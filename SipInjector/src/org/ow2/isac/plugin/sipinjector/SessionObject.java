/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2009 France Telecom
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * CLIF
 *
 * Contact: clif@ow2.org
 */

package org.ow2.isac.plugin.sipinjector;

// System imports
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Vector;
import java.util.Properties;
import java.util.ArrayList;
import java.util.List;
import java.util.Date;
import java.lang.Object;
import java.text.ParseException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

// CLIF imports
import org.ow2.clif.scenario.isac.plugin.SessionObjectAction;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;
import org.ow2.clif.scenario.isac.plugin.DataProvider;
import org.ow2.clif.scenario.isac.exception.IsacRuntimeException;
import java.lang.Error;
import org.ow2.clif.storage.api.ActionEvent;
import org.ow2.clif.scenario.isac.plugin.SampleAction;
import org.ow2.clif.scenario.isac.plugin.ControlAction;
import org.ow2.clif.scenario.isac.plugin.TestAction;
import org.ow2.clif.scenario.isac.util.ParameterParser;

// SIP & SDP imports
import javax.sip.*;
import javax.sip.address.*;
import javax.sip.header.*;
import javax.sip.message.*;
import javax.sdp.*;

import gov.nist.javax.sip.Utils;
import gov.nist.javax.sip.header.WWWAuthenticate;
import gov.nist.javax.sip.header.SIPHeaderNames;
import gov.nist.javax.sip.message.SIPMessage;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

/**
 * Implementation of a session object for plugin ~SipInjector~
 * 
 * @author Remi Druilhe
 */
public class SessionObject implements SessionObjectAction, DataProvider, SampleAction, ControlAction, TestAction, SipListener
{
 private static final Logger logger = Logger.getLogger(SessionObject.class);
	
	static final int TEST_RESPONSECODEIS = 7;
	static final String TEST_RESPONSECODEIS_METHOD = "method";
	static final String TEST_RESPONSECODEIS_NOTCHOICE = "notChoice";
	static final String TEST_RESPONSECODEIS_RESPONSECODE = "responseCode";
	static final int TEST_RESPONSEIS1XX = 8;
	static final String TEST_RESPONSEIS1XX_NOTCHOICE = "notChoice";
	static final int TEST_RESPONSEIS2XX = 9;
	static final String TEST_RESPONSEIS2XX_NOTCHOICE = "notChoice";
	static final int TEST_RESPONSEIS3XX = 10;
	static final String TEST_RESPONSEIS3XX_NOTCHOICE = "notChoice";
	static final int TEST_RESPONSEIS4XX = 11;
	static final String TEST_RESPONSEIS4XX_NOTCHOICE = "notChoice";
	static final int TEST_RESPONSEIS5XX = 12;
	static final String TEST_RESPONSEIS5XX_NOTCHOICE = "notChoice";
	static final int TEST_RESPONSEIS6XX = 13;
	static final String TEST_RESPONSEIS6XX_NOTCHOICE = "notChoice";
	static final int TEST_REQUESTISREGISTER = 14;
	static final String TEST_REQUESTISREGISTER_NOTCHOICE = "notChoice";
	static final int TEST_REQUESTISINVITE = 15;
	static final String TEST_REQUESTISINVITE_NOTCHOICE = "notChoice";
	static final int TEST_REQUESTISACK = 16;
	static final String TEST_REQUESTISACK_NOTCHOICE = "notChoice";
	static final int TEST_REQUESTISBYE = 17;
	static final String TEST_REQUESTISBYE_NOTCHOICE = "notChoice";
	static final int TEST_REQUESTISCANCEL = 18;
	static final String TEST_REQUESTISCANCEL_NOTCHOICE = "notChoice";
	static final int TEST_REQUESTISOPTIONS = 19;
	static final String TEST_REQUESTISOPTIONS_NOTCHOICE = "notChoice";
	static final int TEST_REQUESTISMESSAGE = 20;
	static final String TEST_REQUESTISMESSAGE_NOTCHOICE = "notChoice";
	static final int TEST_REQUESTISSUBSCRIBE = 21;
	static final String TEST_REQUESTISSUBSCRIBE_NOTCHOICE = "notChoice";
	static final int TEST_REQUESTISNOTIFY = 22;
	static final String TEST_REQUESTISNOTIFY_NOTCHOICE = "notChoice";
	static final int TEST_ISTIMEOUT = 23;
	static final String TEST_ISTIMEOUT_NOTCHOICE = "notChoice";
	static final int CONTROL_SETSDP = 3;
	static final String CONTROL_SETSDP_STOPTIME = "stopTime";
	static final String CONTROL_SETSDP_STARTTIME = "startTime";
	static final String CONTROL_SETSDP_OTHERHEADERS = "otherHeaders";
	static final String CONTROL_SETSDP_MEDIAFORMAT = "mediaFormat";
	static final String CONTROL_SETSDP_MEDIAPROTO = "mediaProto";
	static final String CONTROL_SETSDP_MEDIAPORT = "mediaPort";
	static final String CONTROL_SETSDP_MEDIATYPE = "mediaType";
	static final String CONTROL_SETSDP_ATTRIBUTES = "attributeS";
	static final String CONTROL_SETSDP_OWNERADDRESS = "ownerAddress";
	static final String CONTROL_SETSDP_OWNERADDRESSTYPE = "ownerAddressType";
	static final String CONTROL_SETSDP_OWNERNETWORKTYPE = "ownerNetworkType";
	static final String CONTROL_SETSDP_SESSIONVERSION = "sessionVersion";
	static final String CONTROL_SETSDP_SESSIONID = "sessionId";
	static final String CONTROL_SETSDP_OWNERUSERNAME = "ownerUserName";
	static final String CONTROL_SETSDP_ATTRIBUTEV = "attributeV";
	static final int CONTROL_SETBODY = 4;
	static final String CONTROL_SETBODY_VALUE = "value";
	static final int CONTROL_WAITREQUEST = 6;
	static final String CONTROL_WAITREQUEST_TIMEOUT = "timeOut";
	static final String CONTROL_WAITREQUEST_SESSIONID = "sessionId";
	static final int CONTROL_SETGLOBALDEFAULTS = 24;
	static final String CONTROL_SETGLOBALDEFAULTS_SCHEME = "scheme";
	static final String CONTROL_SETGLOBALDEFAULTS_RESPONSE = "response";
	static final String CONTROL_SETGLOBALDEFAULTS_QOP = "qop";
	static final String CONTROL_SETGLOBALDEFAULTS_OPAQUE = "opaque";
	static final String CONTROL_SETGLOBALDEFAULTS_NONCECOUNT = "nonceCount";
	static final String CONTROL_SETGLOBALDEFAULTS_NONCE = "nonce";
	static final String CONTROL_SETGLOBALDEFAULTS_CNONCE = "cNonce";
	static final String CONTROL_SETGLOBALDEFAULTS_ALGORITHM = "algorithm";
	static final String CONTROL_SETGLOBALDEFAULTS_TIMEOUT = "timeOut";
	static final String CONTROL_SETGLOBALDEFAULTS_OTHERHEADERS = "otherHeaders";
	static final String CONTROL_SETGLOBALDEFAULTS_USERAGENT = "userAgent";
	static final String CONTROL_SETGLOBALDEFAULTS_MAXFORWARDS = "maxForwards";
	static final String CONTROL_SETGLOBALDEFAULTS_REMOTEDOMAIN = "remoteDomain";
	static final String CONTROL_SETGLOBALDEFAULTS_IMSIMPU = "imsImpu";
	static final String CONTROL_SETGLOBALDEFAULTS_IMSIMPI = "imsImpi";
	static final String CONTROL_SETGLOBALDEFAULTS_IMSPARAMETER = "imsParameter";
	static final String CONTROL_SETGLOBALDEFAULTS_REMOTEUSERNAME = "remoteUserName";
	static final String CONTROL_SETGLOBALDEFAULTS_REMOTEDISPLAYNAME = "remoteDisplayName";
	static final String CONTROL_SETGLOBALDEFAULTS_REALM = "realm";
	static final String CONTROL_SETGLOBALDEFAULTS_DOMAIN = "domain";
	static final String CONTROL_SETGLOBALDEFAULTS_PASSWORD = "password";
	static final String CONTROL_SETGLOBALDEFAULTS_USERNAME = "userName";
	static final String CONTROL_SETGLOBALDEFAULTS_LOCALDISPLAYNAME = "localDisplayName";
	static final int CONTROL_UPDATEGLOBALDEFAULTS = 30;
	static final String CONTROL_UPDATEGLOBALDEFAULTS_SCHEME = "scheme";
	static final String CONTROL_UPDATEGLOBALDEFAULTS_RESPONSE = "response";
	static final String CONTROL_UPDATEGLOBALDEFAULTS_QOP = "qop";
	static final String CONTROL_UPDATEGLOBALDEFAULTS_OPAQUE = "opaque";
	static final String CONTROL_UPDATEGLOBALDEFAULTS_NONCECOUNT = "nonceCount";
	static final String CONTROL_UPDATEGLOBALDEFAULTS_NONCE = "nonce";
	static final String CONTROL_UPDATEGLOBALDEFAULTS_CNONCE = "cNonce";
	static final String CONTROL_UPDATEGLOBALDEFAULTS_ALGORITHM = "algorithm";
	static final String CONTROL_UPDATEGLOBALDEFAULTS_TIMEOUT = "timeOut";
	static final String CONTROL_UPDATEGLOBALDEFAULTS_OTHERHEADERS = "otherHeaders";
	static final String CONTROL_UPDATEGLOBALDEFAULTS_USERAGENT = "userAgent";
	static final String CONTROL_UPDATEGLOBALDEFAULTS_MAXFORWARDS = "maxForwards";
	static final String CONTROL_UPDATEGLOBALDEFAULTS_REMOTEDOMAIN = "remoteDomain";
	static final String CONTROL_UPDATEGLOBALDEFAULTS_REMOTEUSERNAME = "remoteUserName";
	static final String CONTROL_UPDATEGLOBALDEFAULTS_REMOTEDISPLAYNAME = "remoteDisplayName";
	static final String CONTROL_UPDATEGLOBALDEFAULTS_IMSIMPU = "imsImpu";
	static final String CONTROL_UPDATEGLOBALDEFAULTS_IMSIMPI = "imsImpi";
	static final String CONTROL_UPDATEGLOBALDEFAULTS_IMSPARAMETER = "imsParameter";
	static final String CONTROL_UPDATEGLOBALDEFAULTS_REALM = "realm";
	static final String CONTROL_UPDATEGLOBALDEFAULTS_DOMAIN = "domain";
	static final String CONTROL_UPDATEGLOBALDEFAULTS_PASSWORD = "password";
	static final String CONTROL_UPDATEGLOBALDEFAULTS_USERNAME = "userName";
	static final String CONTROL_UPDATEGLOBALDEFAULTS_LOCALDISPLAYNAME = "localDisplayName";
	static final int CONTROL_INITSIPSTACK = 32;
	static final String CONTROL_INITSIPSTACK_SERVERPORT = "serverPort";
	static final String CONTROL_INITSIPSTACK_TRANSPORT = "transport";
	static final String CONTROL_INITSIPSTACK_SERVERIPADDRESS = "serverIpAddress";
	static final String CONTROL_INITSIPSTACK_PORT = "port";
	static final String CONTROL_INITSIPSTACK_IPADDRESS = "ipAddress";
	static final int CONTROL_REQUEST = 33;
	static final String CONTROL_REQUEST_VIA = "via";
	static final String CONTROL_REQUEST_HOST = "host";
	static final String CONTROL_REQUEST_USERNAME = "userName";
	static final String CONTROL_REQUEST_GENERATEAUTHORIZATION = "generateAuthorization";
	static final String CONTROL_REQUEST_AUTHORIZATION = "authorization";
	static final String CONTROL_REQUEST_ADDTOTAG = "addToTag";
	static final String CONTROL_REQUEST_REQUESTID = "requestId";
	static final String CONTROL_REQUEST_FROMTAG = "fromTag";
	static final String CONTROL_REQUEST_TOTAG = "toTag";
	static final String CONTROL_REQUEST_OTHERHEADERS = "otherHeaders";
	static final String CONTROL_REQUEST_CSEQSEQUENCENUMBER = "cSeqSequenceNumber";
	static final String CONTROL_REQUEST_CONTENTLENGTH = "contentLength";
	static final String CONTROL_REQUEST_CALLID = "callId";
	static final String CONTROL_REQUEST_METHOD = "method";
	static final int CONTROL_SETRESPONSEDEFAULTS = 34;
	static final String CONTROL_SETRESPONSEDEFAULTS_ADDCONTACT = "addContact";
	static final String CONTROL_SETRESPONSEDEFAULTS_OTHERHEADERS = "otherHeaders";
	static final String CONTROL_SETRESPONSEDEFAULTS_CONTENTTYPE = "contentType";
	static final String CONTROL_SETRESPONSEDEFAULTS_EXPIRES = "expires";
	static final String CONTROL_SETRESPONSEDEFAULTS_QVALUE = "qValue";
	static final int CONTROL_RESPONSE = 35;
	static final String CONTROL_RESPONSE_RESPONSEID = "responseId";
	static final String CONTROL_RESPONSE_OTHERHEADERS = "otherHeaders";
	static final String CONTROL_RESPONSE_TOTAG = "toTag";
	static final String CONTROL_RESPONSE_CONTENTLENGTH = "contentLength";
	static final String CONTROL_RESPONSE_REASONPHRASE = "reasonPhrase";
	static final String CONTROL_RESPONSE_STATUSCODE = "statusCode";
	static final int CONTROL_SETMETHODDEFAULTS = 59;
	static final String CONTROL_SETMETHODDEFAULTS_VIA = "via";
	static final String CONTROL_SETMETHODDEFAULTS_CONTENTTYPE = "contentType";
	static final String CONTROL_SETMETHODDEFAULTS_OTHERHEADERS = "otherHeaders";
	static final String CONTROL_SETMETHODDEFAULTS_ADDCONTACT = "addContact";
	static final String CONTROL_SETMETHODDEFAULTS_EXPIRES = "expires";
	static final String CONTROL_SETMETHODDEFAULTS_QVALUE = "qValue";
	static final String CONTROL_SETMETHODDEFAULTS_FROMINTO = "fromInTo";
	static final String CONTROL_SETMETHODDEFAULTS_METHOD = "method";
	static final int CONTROL_DELETESESSION = 60;
	static final String CONTROL_DELETESESSION_SESSIONID = "SessionID";
	static final int SAMPLE_WAITRESPONSE = 0;
	static final String SAMPLE_WAITRESPONSE_TIMEOUT = "timeOut";
	static final String SAMPLE_WAITRESPONSE_SESSIONID = "sessionId";
	
	static final int SAMPLE_WAITRESPONSECODE = 61;
	static final String SAMPLE_WAITRESPONSECODE_NRETRYREQUEST = "nRetryRequest";
	static final String SAMPLE_WAITRESPONSECODE_METHOD = "method";
	static final String SAMPLE_WAITRESPONSECODE_RESPONSECODE = "responseCode";
	static final String SAMPLE_WAITRESPONSECODE_TIMEOUT = "timeOut";
	static final String SAMPLE_WAITRESPONSECODE_SESSIONID = "sessionId";
	// General
	private static final char[] toHex = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'};
	
	// Attributes related to the SIP Communications
	private SipStack sipStack;
	private SipFactory sipFactory;
	private AddressFactory addressFactory;
	private MessageFactory messageFactory;
	private HeaderFactory headerFactory;
	private SipProvider sipProvider;
	private ListeningPoint listeningPoint;

	// Session object's attributes - SIP
	private HashMap<String, HashMap<String, String>> requestTableParam = new HashMap<String, HashMap<String, String>>();
	
	// Buffer SIP
	private LinkedList<MyResponseTable> responseBuffer = new LinkedList<MyResponseTable>();
	private LinkedList<MyRequestTable> requestBuffer = new LinkedList<MyRequestTable>();
	
	// Received SIP message
	private LinkedList<MyResponseTable> receivedResponseTable = new LinkedList<MyResponseTable>();
	private LinkedList<MyRequestTable> receivedRequestTable = new LinkedList<MyRequestTable>();
	
	// Table Session
	private LinkedList<MySessionTable> sessionTable = new LinkedList<MySessionTable>();
	
	// Timeout Constant
	private boolean timeOutUp;
	private static String enable = "enabled";
	
	// Global Variable for doGet
	private Message getMessage;

	/**
	 * Constructor for specimen object.
	 *
	 * @param params : key-value pairs for plugin parameters
	 */
	public SessionObject(Hashtable<String, String> params)
	{
		// SIP Stack
		sipStack = null;
		sipFactory = null;
		addressFactory = null;
		messageFactory = null;
		headerFactory = null;
		sipProvider = null;
		listeningPoint = null;
		
		// Other parameters
		timeOutUp = false;
	}

	/**
	 * Copy constructor (clone specimen object to get session object).
	 *
	 * @param so : specimen object to clone
	 */
	private SessionObject(SessionObject so) 
	{
	}
	
	////////////////////////////////////////
	// SessionObjectAction implementation //
	////////////////////////////////////////

	/**
	 * @see org.ow2.clif.scenario.isac.plugin.SessionObjectAction#createNewSessionObject()
	 */
	public Object createNewSessionObject() {
		return new SessionObject(this);
	}

	/**
	 * @see org.ow2.clif.scenario.isac.plugin.SessionObjectAction#close()
	 */
	public void close()
	{
		try
		{
			if (sipStack != null)
			{
				sipStack.deleteListeningPoint(listeningPoint);
				sipProvider.removeSipListener(this);
				
				while(true)
				{
					try 
					{
						sipStack.deleteSipProvider(sipProvider);
						break;
					} 
					catch (ObjectInUseException e)
					{
						continue;
					}
				}	
			
				sipStack=null;
				sipProvider=null;
				addressFactory=null;
				headerFactory=null;
				messageFactory=null;
				listeningPoint=null;
			}
		}
		catch (Exception ex)
		{
			throw new IsacRuntimeException("SipStack destruction failed : " + ex);
		}
		
		requestTableParam.clear();
		receivedResponseTable.clear();
		receivedRequestTable.clear();
		sessionTable.clear();
		responseBuffer.clear();
		requestBuffer.clear();
	}

	/**
	 * @see org.ow2.clif.scenario.isac.plugin.SessionObjectAction#reset()
	 */
	public void reset() 
	{
	}
	
	/////////////////////////////////
	// DataProvider implementation //
	/////////////////////////////////

	/**
	 * @see org.ow2.clif.scenario.isac.plugin.DataProvider#doGet()
	 */
	public String doGet(String var) 
	{
		String variable[] = var.split("=");
		
		if(variable[0].compareTo("sip_body") == 0)
			return getSipBody();
		else if(variable[0].compareTo("sip_header") == 0)
			return getSipHeader(variable[1]);
		else if(variable[0].compareTo("sdp") == 0)
			return getSdpValue(variable[1]);
		else if(variable[0].compareTo("sip_message") == 0)
			return getMessage.toString();
		else if(variable[0].compareTo("sip_first_line") == 0)
			return ((SIPMessage) getMessage).getFirstLine();
		else
			throw new IsacRuntimeException("Unknown parameter value in ~SipInjector~ ISAC plugin: " + var);
	}
	
	/**
	 * @param sdpLine : the variable you want the value.
	 * @return the value of the sdpLine.
	 */
	public String getSdpValue(String sdpLine)
	{
		String value = "";
		
		if(getMessage.getContent() != null)
		{
			String type[] = sdpLine.split("_", 2);
			byte[] content = ((byte[]) ((SIPMessage) getMessage).getContent());
			String sipBody = new String(content);
			SdpFactory sdpFactory = SdpFactory.getInstance();
			SessionDescription localSession = null;
			
			try
			{
				localSession = sdpFactory.createSessionDescription(sipBody);
				
				logger.log(Level.DEBUG, "localsession addr :" + localSession.getConnection().getAddress());
			}
			catch (SdpParseException e)
			{
				throw new IsacRuntimeException("Unable to create SDP : " + e);
			}

			try
			{
				if(type[0].compareTo("session") == 0)
				{
					String tempValue[] = null;
					
					if(type[1].compareTo("version") == 0)
						tempValue = localSession.getVersion().toString().split("=");
					else if(type[1].compareTo("origin") == 0)
						tempValue = localSession.getOrigin().toString().split("=");
					else if(type[1].compareTo("name") == 0)
						tempValue = localSession.getSessionName().toString().split("=");
					else if(type[1].compareTo("information") == 0)
						tempValue = localSession.getInfo().toString().split("=");
					else if(type[1].startsWith("email") == true)
					{
						String index[] = type[1].split("_");
					
						tempValue = localSession.getEmails(true).get(Integer.valueOf(index[1])).toString().split("=");
					}
					else if(type[1].compareTo("uri") == 0)
						tempValue = localSession.getURI().toString().split("=");
					else if(type[1].startsWith("phone") == true)
					{
						String index[] = type[1].split("_");
					
						tempValue = localSession.getPhones(true).get(Integer.valueOf(index[1])).toString().split("=");
					}
					else if(type[1].compareTo("connection") == 0)
						tempValue = localSession.getConnection().toString().split("=");
					else if(type[1].compareTo("key") == 0)
					
					if(tempValue[1] != null)
						value = tempValue[1];
				}
				else if(type[0].compareTo("time") == 0)
				{
					String tempValue = "";
					
					if(type[1].compareTo("start") == 0)
						tempValue = ((TimeDescription) localSession.getTimeDescriptions(true).get(0)).getTime().getStart().toString();
					else if(type[1].compareTo("stop") == 0)
						tempValue = ((TimeDescription) localSession.getTimeDescriptions(true).get(0)).getTime().getStop().toString();
					
					if(tempValue != null)
						value = tempValue;
				}
				else if(type[0].compareTo("media") == 0)
				{
					String tempValue = "";
					
					if(type[1].compareTo("port") == 0)
						tempValue = String.valueOf(((MediaDescription) localSession.getMediaDescriptions(true).get(0)).getMedia().getMediaPort());
					else if(type[1].compareTo("type") == 0)
						tempValue = ((MediaDescription) localSession.getMediaDescriptions(true).get(0)).getMedia().getMediaType();
					else if(type[1].compareTo("protocol") == 0)
						tempValue = ((MediaDescription) localSession.getMediaDescriptions(true).get(0)).getMedia().getProtocol();
					else if((type[1].compareTo("ip") == 0))
						tempValue = localSession.getConnection().getAddress();
					else if(type[1].startsWith("format") == true)
					{
						String index[] = type[1].split("_");
						
						Vector<String> vectorValue = ((MediaDescription) localSession.getMediaDescriptions(true).get(0)).getMedia().getMediaFormats(true); 
						
						tempValue = vectorValue.get(Integer.valueOf(index[1]));
					}
					
					if(tempValue != null)
						value = tempValue;
				}
			}
			catch (SdpException e) 
			{
				throw new IsacRuntimeException("Unable to get SDP variables : " + e);
			}
		}
		
		return value;
	}
	
	/**
	 * Permit to recover the body from the last message (request or response).
	 * 
	 * @return : the body of the message.
	 */
	public String getSipBody()
	{
		if(getMessage.getContent() != null)
		{
			byte[] content = ((byte[]) ((SIPMessage) getMessage).getContent());
			String sipBody = new String(content);
			
			return(sipBody);
		}

		return "";
	}
	
	/**
	 * Permit to recover some headers from the last message (request or response).
	 * 
	 * @param sipHeader : header to recover.
	 * @return the specified header of the message.
	 */
	public String getSipHeader(String sipHeader)
	{
		String sipHeaderValue = "";

		if(((Header) getMessage.getHeader(sipHeader)).toString().compareTo("") != 0)
		{
			String tempSipHeader[] = ((Header) getMessage.getHeader(sipHeader)).toString().split(": ", 2);
			sipHeaderValue = tempSipHeader[1];
		}
		
		return sipHeaderValue;
	}
	
	
	/////////////////////////////////
	// SampleAction implementation //
	/////////////////////////////////

	/**
	 * @see org.ow2.clif.scenario.isac.plugin.SampleAction#doSample()
	 */
	public ActionEvent doSample(int number, Map params, ActionEvent report) 
	{
		switch (number)
		{
			case SAMPLE_WAITRESPONSECODE:
				waitResponseCode(params, report);
			break;
		case SAMPLE_WAITRESPONSE:
				waitResponse(params, report);
				break;
			default:
				throw new Error("Unable to find this sample in ~SipInjector~ ISAC plugin: " + number);
		}
		
		return report;
	}
	
	private ActionEvent waitResponseCode(Map params, ActionEvent report) {
		HashMap<String, String> globalDefaults = requestTableParam.get("GLOBAL");
		MyResponseTable myResponseTable = new MyResponseTable();
		String localId = (String) params.get(SessionObject.SAMPLE_WAITRESPONSE_SESSIONID);

		logger.log(Level.DEBUG, "begin of waitResponse" + " " + localId);

		MySessionTable mySessionTable = new MySessionTable();
		int indexSession = -1;

		// Previous message
		if(!receivedResponseTable.isEmpty())
		{
			logger.log(Level.DEBUG, "receivedResponseTable Size: " + receivedResponseTable.size()+ " " + localId);
			logger.log(Level.DEBUG, receivedResponseTable.get(0).getMyResponse().getStatusCode() + " " + localId);
			receivedResponseTable.removeFirst();
		}

		// Test if the session has already been created
		if(!sessionTable.isEmpty())
			for(int i=0; i<sessionTable.size(); i++)
				if(sessionTable.get(i).getMySessionId().equals(localId))
				{
					mySessionTable = sessionTable.get(i);
					indexSession = i;
				}

		// Timeout
		long timeOut = 60000;

		if(globalDefaults.get("Timeout").compareTo("") != 0)
			timeOut = Long.parseLong(globalDefaults.get("Timeout"));

		if(((String) params.get(SessionObject.SAMPLE_WAITRESPONSE_TIMEOUT)).compareTo("") != 0)
			timeOut = Long.parseLong((String) params.get(SessionObject.SAMPLE_WAITRESPONSE_TIMEOUT));

		timeOutUp = false;

		// Read the buffer and erase it
		long start = System.currentTimeMillis();

		int awaitedRspCode = Integer.parseInt((String)params.get(SessionObject.SAMPLE_WAITRESPONSECODE_RESPONSECODE));
		String awaitedMethod = (String) params.get(SessionObject.TEST_RESPONSECODEIS_METHOD);

		boolean awaitedRspIsReceived = false;
		String method;

		while (!awaitedRspIsReceived)
		{
			synchronized (this)  
			{	
				if(responseBuffer.isEmpty())
				{	
					try
					{
						logger.log(Level.DEBUG, "wait response" + " " + localId);

						// Waiting for a response
						this.wait(timeOut);
						logger.log(Level.DEBUG, "notified or timeout" + " " + localId);
					}
					catch(Exception e)
					{
						throw new IsacRuntimeException("Buffer writing failed : " + e);
					}
				}
				else
				{
					logger.log(Level.DEBUG, "ResponseBuffer Size: " + responseBuffer.size() + " " + localId);
					logger.log(Level.DEBUG, responseBuffer.get(0).getMyResponse().getStatusCode() + " " + localId);
				}

				if(responseBuffer.isEmpty())
				{	
					//No response was received during the waiting period and time is out.
					timeOutUp = true;

					break;
				}
				else {
					//A msg is received or was already received before the control call. 
					Iterator itr = responseBuffer.iterator(); 
					MyResponseTable current;
					int rspCode = 0;

					while(itr.hasNext())
					{
						logger.log(Level.DEBUG, "rspCode :" + rspCode + " " + localId);

						current = (MyResponseTable) itr.next();
						rspCode= current.getMyResponse().getStatusCode();
						method = ((CSeqHeader) current.getMyResponse().getHeader(SIPHeaderNames.CSEQ)).getMethod();

						logger.log(Level.DEBUG, "awaitedMethod: " + awaitedMethod + ", receivedMethod:" + method + " " + localId);

						boolean isAwaitedMethod = true;

						if (awaitedMethod != null)
						{
							isAwaitedMethod = (awaitedMethod.compareTo(method) == 0);
						}

						if ((rspCode == awaitedRspCode) && isAwaitedMethod)
						{
							myResponseTable = current;
							itr.remove();

							logger.log(Level.DEBUG, "remove rspCode :" + rspCode + " " + localId);

							awaitedRspIsReceived = true;

							break;
						}
						else
						{
							logger.log(Level.DEBUG, "the response I was received was not the one i'm expecting for" + " " + localId);
						}
					}					
				}

				if (!awaitedRspIsReceived)
				{
					long duree = System.currentTimeMillis() - start;
					logger.log(Level.DEBUG, duree);

					if (duree >= timeOut)
					{
						//message will not be sent
						logger.log(Level.DEBUG, "time is out" + " " + localId);

						timeOutUp = true;

						break;
					}

					try
					{
						//lock off
						this.wait(timeOut-duree);
						logger.log(Level.DEBUG, "notified or timeout, msg queue was not empty" + " " + localId);
					} 
					catch (InterruptedException e) 
					{
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				logger.log(Level.DEBUG, "end synchrozone wait rsp" + " " + localId);

			}
		}
		
		if(!timeOutUp)
		{
			receivedResponseTable.addLast(myResponseTable);

			if(indexSession != -1 && ((CallIdHeader) myResponseTable.getMyResponse().getHeader(SIPHeaderNames.CALL_ID)).getCallId().compareTo(mySessionTable.getMySessionCallId()) == 0)
			{
				//TODO Add tag to in the session
				if(indexSession != -1)
					sessionTable.remove(indexSession);
				
				mySessionTable.setMySessionToTag(((ToHeader) myResponseTable.getMyResponse().getHeader(SIPHeaderNames.TO)).getTag());	
				sessionTable.addLast(mySessionTable);
				
				// Make the report
				method = ((CSeqHeader) myResponseTable.getMyResponse().getHeader(SIPHeaderNames.CSEQ)).getMethod();
				logger.log(Level.DEBUG, "CseqMethod:" + method + " " + localId);
				
				long cSeq = ((CSeqHeader) myResponseTable.getMyResponse().getHeader(SIPHeaderNames.CSEQ)).getSeqNumber();
				Long duration = myResponseTable.getMyTime() - mySessionTable.getMySessionTime(method, cSeq);

				report.setDate(mySessionTable.getMySessionTime(method, cSeq));
				report.duration = duration.intValue();
				report.type = "SIP/" + ((CSeqHeader) myResponseTable.getMyResponse().getHeader(SIPHeaderNames.CSEQ)).getMethod();
				report.successful = true;
				report.comment = "From: " + ((FromHeader) myResponseTable.getMyResponse().getHeader(SIPHeaderNames.FROM)).getAddress().toString() 
					+ " | To: " + ((ToHeader) myResponseTable.getMyResponse().getHeader(SIPHeaderNames.TO)).getAddress().toString();
				report.result = myResponseTable.getMyResponse().getStatusCode() + " " + myResponseTable.getMyResponse().getReasonPhrase();
				
				// Export message to be used in doGet
				getMessage = myResponseTable.getMyResponse();
				
				logger.log(Level.DEBUG, "adresse ip :" + getSdpValue("media_ip"));
			}
		}
		else if (timeOutUp)
		{
			//TODO  Manage here repetition of message
			
			// Make the report
			report.setDate(System.currentTimeMillis());
			report.duration = 0;
			report.type = "Timeout";
			report.successful = false;
			report.comment = "Unable to receive response during " + timeOut;
			report.result = "Timeout";
		}
		
		logger.log(Level.DEBUG, "end wait rsp" + " " + localId);
		return report;
	}

	/**
	 * Read the responseBuffer and write responses in receivedResponseTable. It cleans buffer
	 * after having done this operation.
	 * 
	 * @param report : Will contain the result of the accomplished action
	 * @param params : A map of the parameters of the control action
	 * @return The ActionEvent report with the duration, comments, type, successful, date, result
	 */
	
	public ActionEvent waitResponse(Map params, ActionEvent report)
	{
		HashMap<String, String> globalDefaults = requestTableParam.get("GLOBAL");
		MyResponseTable myResponseTable = new MyResponseTable();
		String localId = (String) params.get(SessionObject.SAMPLE_WAITRESPONSE_SESSIONID);
		
		logger.log(Level.DEBUG, "begin of waitResponse" + " " + localId);
		
		MySessionTable mySessionTable = new MySessionTable();
		int indexSession = -1;
		
		if(!receivedResponseTable.isEmpty())
		{
			logger.log(Level.DEBUG, "receivedResponseTable size: " + receivedResponseTable.size()+ " " + localId);
			logger.log(Level.DEBUG, receivedResponseTable.get(0).getMyResponse().getStatusCode() + " " + localId);
			receivedResponseTable.removeFirst();
			}
			
		// Test if the session has already been created
		if(!sessionTable.isEmpty())
			for(int i=0; i<sessionTable.size(); i++)
				if(sessionTable.get(i).getMySessionId().equals(localId))
				{
					mySessionTable = sessionTable.get(i);
					indexSession = i;
				}
		
		// Timeout
		long timeOut = 60000;
		
		if(globalDefaults.get("Timeout").compareTo("") != 0)
			timeOut = Long.parseLong(globalDefaults.get("Timeout"));
		
		if(((String) params.get(SessionObject.SAMPLE_WAITRESPONSE_TIMEOUT)).compareTo("") != 0)
			timeOut = Long.parseLong((String) params.get(SessionObject.SAMPLE_WAITRESPONSE_TIMEOUT));
		
		timeOutUp = false;

		// Read the buffer and erase it
		synchronized (this) 
		{	
			if(responseBuffer.isEmpty())
			{	
				try
				{
					logger.log(Level.DEBUG, "wait response" + " " + localId);
					this.wait(timeOut);
					logger.log(Level.DEBUG, "notified" + " " + localId);
					
				}
				catch(Exception e)
				{
					throw new IsacRuntimeException("Buffer writing failed : " + e);
				}
			}
			else
			{
				logger.log(Level.DEBUG, "responseBuffer size: " + responseBuffer.size() + " " + localId);
				logger.log(Level.DEBUG, responseBuffer.get(0).getMyResponse().getStatusCode() + " " + localId);
			}
			
			if(responseBuffer.isEmpty() == false)
				myResponseTable = responseBuffer.removeFirst();
			else
				timeOutUp = true;
			
			logger.log(Level.DEBUG, "end synchrozone wait rsp" + " " + localId);
		}
		
		if(!timeOutUp)
		{
			receivedResponseTable.addLast(myResponseTable);

			if(indexSession != -1 && ((CallIdHeader) myResponseTable.getMyResponse().getHeader(SIPHeaderNames.CALL_ID)).getCallId().compareTo(mySessionTable.getMySessionCallId()) == 0)
			{
				// Add tag of To in the session
				if(indexSession != -1)
					sessionTable.remove(indexSession);
				
				mySessionTable.setMySessionToTag(((ToHeader) myResponseTable.getMyResponse().getHeader(SIPHeaderNames.TO)).getTag());
				
				sessionTable.addLast(mySessionTable);
				
				// Make the report
				String method = ((CSeqHeader) myResponseTable.getMyResponse().getHeader(SIPHeaderNames.CSEQ)).getMethod();
				logger.log(Level.DEBUG, "CseqMethod:" + method + " " + localId);
			
				long cSeq = ((CSeqHeader) myResponseTable.getMyResponse().getHeader(SIPHeaderNames.CSEQ)).getSeqNumber();

				Long duration = myResponseTable.getMyTime() - mySessionTable.getMySessionTime(method, cSeq);

				report.setDate(mySessionTable.getMySessionTime(method, cSeq));
				report.duration = duration.intValue();
				report.type = "SIP/" + ((CSeqHeader) myResponseTable.getMyResponse().getHeader(SIPHeaderNames.CSEQ)).getMethod();
				report.successful = true;
				report.comment = "From: " + ((FromHeader) myResponseTable.getMyResponse().getHeader(SIPHeaderNames.FROM)).getAddress().toString() 
					+ " | To: " + ((ToHeader) myResponseTable.getMyResponse().getHeader(SIPHeaderNames.TO)).getAddress().toString();
				report.result = myResponseTable.getMyResponse().getStatusCode() + " " + myResponseTable.getMyResponse().getReasonPhrase();
				
				// Export message to be used in doGet
				getMessage = myResponseTable.getMyResponse();
			}
		}
		else
		{
			// Make the report
			report.setDate(System.currentTimeMillis());
			report.duration = 0;
			report.type = "Timeout";
			report.successful = false;
			report.comment = "Unable to receive response during " + timeOut;
			report.result = "Timeout";
		}
		
		logger.log(Level.DEBUG, "end wait rsp" + " " + localId);
		return report;
	}

	
	//////////////////////////////////
	// ControlAction implementation //
	//////////////////////////////////

	/**
	 * @see org.ow2.clif.scenario.isac.plugin.ControlAction#doControl()
	 */
	public void doControl(int number, Map params)
	{
		logger.log(Level.DEBUG, "doControl: " + number + " =" + params);
		switch (number) 
		{
			case CONTROL_DELETESESSION:
				doDeleteSession(params);
			break;
		case CONTROL_SETMETHODDEFAULTS:
				setMethodDefaults(params);
				break;
			case CONTROL_RESPONSE:
				initResponse(params);
				doResponse();
				break;
			case CONTROL_SETRESPONSEDEFAULTS:
				setResponseDefaults(params);
				break;
			case CONTROL_REQUEST:
				initRequest(params);
				doRequest();
				break;
			case CONTROL_INITSIPSTACK:
				initSipStack(params);
				break;
			case CONTROL_UPDATEGLOBALDEFAULTS:
				updateGlobalDefaults(params);
				break;
			case CONTROL_SETGLOBALDEFAULTS:	
				setGlobalDefaults(params);
				break;
			case CONTROL_WAITREQUEST:
				waitRequest(params);
				break;
			case CONTROL_SETBODY:
				setBodyDefaults(params);
				break;
			case CONTROL_SETSDP:
				setSdpDefaults(params);
				break;
			default:
				throw new Error(
					"Unable to find this control in ~SipInjector~ ISAC plugin: "
							+ number);
		}
	}
	
	/**
	 * Initialize SIP Stack with local IP address, local port, server IP address, server port and transport.
	 * Many Stacks can be initialized using CsvProvider plug-in.
	 * 
	 * @param params : A map of the parameters of the control action
	 */
	public void initSipStack(Map params) 
	{
		HashMap<String, String> headerTable = new HashMap<String, String>();

		sipFactory = SipFactory.getInstance();
		sipFactory.setPathName("gov.nist");
		
		Properties properties = new Properties();
		properties.setProperty("javax.sip.STACK_NAME", "SipInjector" 
				+ (String) params.get(SessionObject.CONTROL_INITSIPSTACK_PORT));
		properties.setProperty("javax.sip.AUTOMATIC_DIALOG_SUPPORT", "off");
		properties.setProperty("javax.sip.OUTBOUND_PROXY", 
				(String) params.get(SessionObject.CONTROL_INITSIPSTACK_SERVERIPADDRESS) 
				+ ":" + (String) params.get(SessionObject.CONTROL_INITSIPSTACK_SERVERPORT) + "/"
				+ (String) params.get(SessionObject.CONTROL_INITSIPSTACK_TRANSPORT));
		
		try
		{
			sipStack = sipFactory.createSipStack(properties);
		}
		catch(Exception e)
		{
			throw new IsacRuntimeException("SipStack Init Failed : " + e);
		}
			
		try
		{
			headerFactory = sipFactory.createHeaderFactory();
			messageFactory = sipFactory.createMessageFactory();
			addressFactory = sipFactory.createAddressFactory();

			String ipAddress = (String) params.get(SessionObject.CONTROL_INITSIPSTACK_IPADDRESS);
			int clientPort = Integer.parseInt((String) params.get(SessionObject.CONTROL_INITSIPSTACK_PORT));
			String transport = (String) params.get(SessionObject.CONTROL_INITSIPSTACK_TRANSPORT);

			listeningPoint = sipStack.createListeningPoint(ipAddress, clientPort, transport);
			sipProvider = sipStack.createSipProvider(listeningPoint);

			sipProvider.addSipListener(this);
			sipProvider.setAutomaticDialogSupportEnabled(false);
		}
		catch(Exception e)
		{
			throw new IsacRuntimeException("Factories creation failed : " + e);
		}
		
		if(requestTableParam.containsKey("INIT") == true)
		{
			requestTableParam.remove("INIT");
		}
		
		headerTable.put("Ip_Address", (String) params.get(SessionObject.CONTROL_INITSIPSTACK_IPADDRESS));
		headerTable.put("Port", (String) params.get(SessionObject.CONTROL_INITSIPSTACK_PORT));
		headerTable.put("Server_Ip_Address", (String) params.get(SessionObject.CONTROL_INITSIPSTACK_SERVERIPADDRESS));
		headerTable.put("Server_Port", (String) params.get(SessionObject.CONTROL_INITSIPSTACK_SERVERPORT));
		headerTable.put("Transport", (String) params.get(SessionObject.CONTROL_INITSIPSTACK_TRANSPORT));
		
		requestTableParam.put("INIT", headerTable);
	}
	
	/**
	 * Initialize global parameters for SIP scenario
	 * 
	 * @param params : A map of the parameters of the control action
	 */
	public void setGlobalDefaults(Map params)
	{
		HashMap<String, String> headerTable = new HashMap<String, String>();

		if(requestTableParam.containsKey("GLOBAL") == true)
		{
			requestTableParam.remove("GLOBAL");
		}

		headerTable.put("Local_Display_Name", (String) params.get(SessionObject.CONTROL_SETGLOBALDEFAULTS_LOCALDISPLAYNAME));
		headerTable.put("Local_User_Name", (String) params.get(SessionObject.CONTROL_SETGLOBALDEFAULTS_USERNAME));
		headerTable.put("Password", (String) params.get(SessionObject.CONTROL_SETGLOBALDEFAULTS_PASSWORD));
		headerTable.put("Local_Domain", (String) params.get(SessionObject.CONTROL_SETGLOBALDEFAULTS_DOMAIN));
		headerTable.put("Realm", (String) params.get(SessionObject.CONTROL_SETGLOBALDEFAULTS_REALM));
		headerTable.put("Ims_Parameter", (String) params.get(SessionObject.CONTROL_SETGLOBALDEFAULTS_IMSPARAMETER));
		headerTable.put("Ims_Impi", (String) params.get(SessionObject.CONTROL_SETGLOBALDEFAULTS_IMSIMPI));
		headerTable.put("Ims_Impu", (String) params.get(SessionObject.CONTROL_SETGLOBALDEFAULTS_IMSIMPU));
		headerTable.put("Algorithm", (String) params.get(SessionObject.CONTROL_SETGLOBALDEFAULTS_ALGORITHM));
		headerTable.put("Cnonce", (String) params.get(SessionObject.CONTROL_SETGLOBALDEFAULTS_CNONCE));
		headerTable.put("Nonce", (String) params.get(SessionObject.CONTROL_SETGLOBALDEFAULTS_NONCE));
		headerTable.put("Nonce_Count", (String) params.get(SessionObject.CONTROL_SETGLOBALDEFAULTS_NONCECOUNT));
		headerTable.put("Opaque", (String) params.get(SessionObject.CONTROL_SETGLOBALDEFAULTS_OPAQUE));
		headerTable.put("Qop", (String) params.get(SessionObject.CONTROL_SETGLOBALDEFAULTS_QOP));
		headerTable.put("Response", (String) params.get(SessionObject.CONTROL_SETGLOBALDEFAULTS_RESPONSE));
		headerTable.put("Scheme", (String) params.get(SessionObject.CONTROL_SETGLOBALDEFAULTS_SCHEME));
		headerTable.put("Remote_Display_Name", (String) params.get(SessionObject.CONTROL_SETGLOBALDEFAULTS_REMOTEDISPLAYNAME));
		headerTable.put("Remote_User_Name", (String) params.get(SessionObject.CONTROL_SETGLOBALDEFAULTS_REMOTEUSERNAME));
		headerTable.put("Remote_Domain", (String) params.get(SessionObject.CONTROL_SETGLOBALDEFAULTS_REMOTEDOMAIN));
		headerTable.put("Max-Forwards", (String) params.get(SessionObject.CONTROL_SETGLOBALDEFAULTS_MAXFORWARDS));
		headerTable.put("Timeout", (String) params.get(SessionObject.CONTROL_SETGLOBALDEFAULTS_TIMEOUT));
		headerTable.put("User_Agent", (String) params.get(SessionObject.CONTROL_SETGLOBALDEFAULTS_USERAGENT));
		headerTable.put("Other_Headers", (String) params.get(SessionObject.CONTROL_SETGLOBALDEFAULTS_OTHERHEADERS));
		
		requestTableParam.put("GLOBAL", headerTable);
	}
	
	/**
	 * Update global parameters for SIP scenario
	 * 
	 * @param params : A map of the parameters of the control action
	 */
	public void updateGlobalDefaults(Map params)
	{
		HashMap<String, String> globalUpdate = new HashMap<String, String>();
		
		globalUpdate = requestTableParam.get("GLOBAL");
		
		if(params.get(SessionObject.CONTROL_UPDATEGLOBALDEFAULTS_LOCALDISPLAYNAME) != "")
		{
			globalUpdate.remove("Local_Display_Name");
			globalUpdate.put("Local_Display_Name", (String) params.get(SessionObject.CONTROL_UPDATEGLOBALDEFAULTS_LOCALDISPLAYNAME));
		}
		if(params.get(SessionObject.CONTROL_UPDATEGLOBALDEFAULTS_USERNAME) != "")
		{
			globalUpdate.remove("Local_User_Name");
			globalUpdate.put("Local_User_Name", (String) params.get(SessionObject.CONTROL_UPDATEGLOBALDEFAULTS_USERNAME));
		}
		if(params.get(SessionObject.CONTROL_UPDATEGLOBALDEFAULTS_PASSWORD) != "")
		{
			globalUpdate.remove("Password");
			globalUpdate.put("Password", (String) params.get(SessionObject.CONTROL_UPDATEGLOBALDEFAULTS_PASSWORD));
		}
		if(params.get(SessionObject.CONTROL_UPDATEGLOBALDEFAULTS_DOMAIN) != "")
		{
			globalUpdate.remove("Local_Domain");
			globalUpdate.put("Local_Domain", (String) params.get(SessionObject.CONTROL_UPDATEGLOBALDEFAULTS_DOMAIN));
		}
		if(params.get(SessionObject.CONTROL_UPDATEGLOBALDEFAULTS_REALM) != "")
		{
			globalUpdate.remove("Realm");
			globalUpdate.put("Realm", (String) params.get(SessionObject.CONTROL_UPDATEGLOBALDEFAULTS_REALM));
		}
		if(params.get(SessionObject.CONTROL_UPDATEGLOBALDEFAULTS_IMSPARAMETER) != "")
		{
			globalUpdate.remove("Ims_Parameter");
			globalUpdate.put("Ims_Parameter", (String) params.get(SessionObject.CONTROL_UPDATEGLOBALDEFAULTS_IMSPARAMETER));
		}
		if(params.get(SessionObject.CONTROL_UPDATEGLOBALDEFAULTS_IMSIMPI) != "")
		{
			globalUpdate.remove("Ims_Impi");
			globalUpdate.put("Ims_Impi", (String) params.get(SessionObject.CONTROL_UPDATEGLOBALDEFAULTS_IMSIMPI));
		}
		if(params.get(SessionObject.CONTROL_UPDATEGLOBALDEFAULTS_IMSIMPU) != "")
		{
			globalUpdate.remove("Ims_Impu");
			globalUpdate.put("Ims_Impu", (String) params.get(SessionObject.CONTROL_UPDATEGLOBALDEFAULTS_IMSIMPU));
		}
		if(params.get(SessionObject.CONTROL_UPDATEGLOBALDEFAULTS_ALGORITHM) != "")
		{
			globalUpdate.remove("Algorithm");
			globalUpdate.put("Algorithm", (String) params.get(SessionObject.CONTROL_UPDATEGLOBALDEFAULTS_ALGORITHM));
		}
		if(params.get(SessionObject.CONTROL_UPDATEGLOBALDEFAULTS_CNONCE) != "")
		{
			globalUpdate.remove("Cnonce");
			globalUpdate.put("Cnonce", (String) params.get(SessionObject.CONTROL_UPDATEGLOBALDEFAULTS_CNONCE));
		}
		if(params.get(SessionObject.CONTROL_UPDATEGLOBALDEFAULTS_NONCE) != "")
		{
			globalUpdate.remove("Nonce");
			globalUpdate.put("Nonce", (String) params.get(SessionObject.CONTROL_UPDATEGLOBALDEFAULTS_NONCE));
		}
		if(params.get(SessionObject.CONTROL_UPDATEGLOBALDEFAULTS_NONCECOUNT) != "")
		{
			globalUpdate.remove("Nonce_Count");
			globalUpdate.put("Nonce_Count", (String) params.get(SessionObject.CONTROL_UPDATEGLOBALDEFAULTS_NONCECOUNT));
		}
		if(params.get(SessionObject.CONTROL_UPDATEGLOBALDEFAULTS_OPAQUE) != "")
		{
			globalUpdate.remove("Opaque");
			globalUpdate.put("Opaque", (String) params.get(SessionObject.CONTROL_UPDATEGLOBALDEFAULTS_OPAQUE));
		}
		if(params.get(SessionObject.CONTROL_UPDATEGLOBALDEFAULTS_QOP) != "")
		{
			globalUpdate.remove("Qop");
			globalUpdate.put("Qop", (String) params.get(SessionObject.CONTROL_UPDATEGLOBALDEFAULTS_QOP));
		}
		if(params.get(SessionObject.CONTROL_UPDATEGLOBALDEFAULTS_RESPONSE) != "")
		{
			globalUpdate.remove("Response");
			globalUpdate.put("Response", (String) params.get(SessionObject.CONTROL_UPDATEGLOBALDEFAULTS_RESPONSE));
		}
		if(params.get(SessionObject.CONTROL_UPDATEGLOBALDEFAULTS_SCHEME) != "")
		{
			globalUpdate.remove("Scheme");
			globalUpdate.put("Scheme", (String) params.get(SessionObject.CONTROL_UPDATEGLOBALDEFAULTS_SCHEME));
		}
		if(params.get(SessionObject.CONTROL_UPDATEGLOBALDEFAULTS_REMOTEDISPLAYNAME) != "")
		{
			globalUpdate.remove("Remote_Display_Name");
			globalUpdate.put("Remote_Display_Name", (String) params.get(SessionObject.CONTROL_UPDATEGLOBALDEFAULTS_REMOTEDISPLAYNAME));
		}
		if(params.get(SessionObject.CONTROL_UPDATEGLOBALDEFAULTS_REMOTEUSERNAME) != "")
		{
			globalUpdate.remove("Remote_User_Name");
			globalUpdate.put("Remote_User_Name", (String) params.get(SessionObject.CONTROL_UPDATEGLOBALDEFAULTS_REMOTEUSERNAME));
		}
		if(params.get(SessionObject.CONTROL_UPDATEGLOBALDEFAULTS_REMOTEDOMAIN) != "")
		{
			globalUpdate.remove("Remote_Domain");
			globalUpdate.put("Remote_Domain", (String) params.get(SessionObject.CONTROL_UPDATEGLOBALDEFAULTS_REMOTEDOMAIN));
		}
		if(params.get(SessionObject.CONTROL_UPDATEGLOBALDEFAULTS_MAXFORWARDS) != "")
		{
			globalUpdate.remove("Max-Forwards");
			globalUpdate.put("Max-Forwards", (String) params.get(SessionObject.CONTROL_UPDATEGLOBALDEFAULTS_MAXFORWARDS));
		}
		if(params.get(SessionObject.CONTROL_UPDATEGLOBALDEFAULTS_TIMEOUT) != "")
		{
			globalUpdate.remove("Timeout");
			globalUpdate.put("Timeout", (String) params.get(SessionObject.CONTROL_UPDATEGLOBALDEFAULTS_TIMEOUT));
		}
		if(params.get(SessionObject.CONTROL_UPDATEGLOBALDEFAULTS_USERAGENT) != "")
		{
			globalUpdate.remove("User_Agent");
			globalUpdate.put("User_Agent", (String) params.get(SessionObject.CONTROL_UPDATEGLOBALDEFAULTS_USERAGENT));
		}
		if(params.get(SessionObject.CONTROL_UPDATEGLOBALDEFAULTS_OTHERHEADERS) != "")
		{
			globalUpdate.remove("Other_Headers");
			globalUpdate.put("Other_Headers", (String) params.get(SessionObject.CONTROL_UPDATEGLOBALDEFAULTS_OTHERHEADERS));
		}
		
		requestTableParam.remove("GLOBAL");
		requestTableParam.put("GLOBAL", globalUpdate);
	}
	
	/**
	 * Initialize parameters for each method selected
	 * 
	 * @param params : A map of the parameters of the control action
	 */
	public void setMethodDefaults(Map params)
	{
		HashMap<String, String> headerTable = new HashMap<String, String>();
		
		String method = (String) params.get(SessionObject.CONTROL_SETMETHODDEFAULTS_METHOD);
		
		if(requestTableParam.containsKey(method) == false)
		{
			headerTable.put("From_In_To", (String) params.get(SessionObject.CONTROL_SETMETHODDEFAULTS_FROMINTO));
			headerTable.put("Add_Contact", (String) params.get(SessionObject.CONTROL_SETMETHODDEFAULTS_ADDCONTACT));
			headerTable.put("Qvalue", (String) params.get(SessionObject.CONTROL_SETMETHODDEFAULTS_QVALUE));
			headerTable.put("Expires", (String) params.get(SessionObject.CONTROL_SETMETHODDEFAULTS_EXPIRES));
			headerTable.put("Content-Type", (String) params.get(SessionObject.CONTROL_SETMETHODDEFAULTS_CONTENTTYPE));
			headerTable.put("via", (String) params.get(SessionObject.CONTROL_SETMETHODDEFAULTS_VIA));
			headerTable.put("Other_Default_Headers", (String) params.get(SessionObject.CONTROL_SETMETHODDEFAULTS_OTHERHEADERS));
			
			requestTableParam.put(method, headerTable);
		}
		else
		{
			headerTable = requestTableParam.get(method);
			
			requestTableParam.remove(method);
			
			if(headerTable.containsKey("From_In_To") == true)
				headerTable.remove("From_In_To");
			if(headerTable.containsKey("Content-Type") == true)
				headerTable.remove("Content-Type");
			if(headerTable.containsKey("Add_Contact") == true)
				headerTable.remove("Add_Contact");
			if(headerTable.containsKey("Qvalue") == true)
				headerTable.remove("Qvalue");
			if(headerTable.containsKey("Expires") == true)
				headerTable.remove("Expires");
			if(headerTable.containsKey("via") == true)
				headerTable.remove("via");
			if(headerTable.containsKey("Other_Default_Headers") == true)
				headerTable.remove("Other_Default_Headers");
			
			headerTable.put("From_In_To", (String) params.get(SessionObject.CONTROL_SETMETHODDEFAULTS_FROMINTO));
			headerTable.put("Add_Contact", (String) params.get(SessionObject.CONTROL_SETMETHODDEFAULTS_ADDCONTACT));
			headerTable.put("Qvalue", (String) params.get(SessionObject.CONTROL_SETMETHODDEFAULTS_QVALUE));
			headerTable.put("Expires", (String) params.get(SessionObject.CONTROL_SETMETHODDEFAULTS_EXPIRES));
			headerTable.put("Content-Type", (String) params.get(SessionObject.CONTROL_SETMETHODDEFAULTS_CONTENTTYPE));
			headerTable.put("via", (String) params.get(SessionObject.CONTROL_SETMETHODDEFAULTS_VIA));
			headerTable.put("Other_Default_Headers", (String) params.get(SessionObject.CONTROL_SETMETHODDEFAULTS_OTHERHEADERS));
			
			requestTableParam.put(method, headerTable);
		}
	}
	
	/**
	 * Initialize Body parameters. Need to import FileReader plug-in.
	 * 
	 * @param params : A map of the parameters of the control action
	 */
	public void setBodyDefaults(Map params)
	{
		HashMap<String, String> bodyRequest = new HashMap<String, String>();
		
		bodyRequest.put("Message", (String) params.get(SessionObject.CONTROL_SETBODY_VALUE));
		
		requestTableParam.put("BODY", bodyRequest);
	}
	
	/**
	 * Initialize SDP parameters.
	 * 
	 * @param params : A map of the parameters of the control action 
	 */
	public void setSdpDefaults(Map params)
	{
		HashMap<String, String> bodyRequest = new HashMap<String, String>();
		
		if(requestTableParam.containsKey("SDP") == true)
		{
			requestTableParam.remove("SDP");
		}
		
		bodyRequest.put("Attribute_V", (String) params.get(SessionObject.CONTROL_SETSDP_ATTRIBUTEV));
		bodyRequest.put("Owner_User_Name", (String) params.get(SessionObject.CONTROL_SETSDP_OWNERUSERNAME));
		bodyRequest.put("Session_Id", (String) params.get(SessionObject.CONTROL_SETSDP_SESSIONID));
		bodyRequest.put("Session_Version", (String) params.get(SessionObject.CONTROL_SETSDP_SESSIONVERSION));
		bodyRequest.put("Owner_Network_Type", (String) params.get(SessionObject.CONTROL_SETSDP_OWNERNETWORKTYPE));
		bodyRequest.put("Owner_Address_Type", (String) params.get(SessionObject.CONTROL_SETSDP_OWNERADDRESSTYPE));
		bodyRequest.put("Owner_Address", (String) params.get(SessionObject.CONTROL_SETSDP_OWNERADDRESS));
		bodyRequest.put("Attribute_S", (String) params.get(SessionObject.CONTROL_SETSDP_ATTRIBUTES));
		bodyRequest.put("Start_Time", (String) params.get(SessionObject.CONTROL_SETSDP_STARTTIME));
		bodyRequest.put("Stop_Time", (String) params.get(SessionObject.CONTROL_SETSDP_STOPTIME));
		bodyRequest.put("Media_Type", (String) params.get(SessionObject.CONTROL_SETSDP_MEDIATYPE));
		bodyRequest.put("Media_Port", (String) params.get(SessionObject.CONTROL_SETSDP_MEDIAPORT));
		bodyRequest.put("Media_Proto", (String) params.get(SessionObject.CONTROL_SETSDP_MEDIAPROTO));
		bodyRequest.put("Media_Format", (String) params.get(SessionObject.CONTROL_SETSDP_MEDIAFORMAT));
		bodyRequest.put("Other_Headers", (String) params.get(SessionObject.CONTROL_SETSDP_OTHERHEADERS));
		
		requestTableParam.put("SDP", bodyRequest);
	}
	
	/**
	 * Read the requestBuffer and write requests in receivedRequestTable. It cleans buffer
	 * after having done this operation.
	 * 
	 * @param params : A map of the parameters of the control action
	 */
	public void waitRequest(Map params)
	{
		HashMap<String, String> globalDefaults = requestTableParam.get("GLOBAL");
		MyRequestTable myRequestTable = new MyRequestTable();
		String localId = (String) params.get(SessionObject.CONTROL_WAITREQUEST_SESSIONID);
		
		MySessionTable mySessionTable = new MySessionTable();
		int indexSession = -1;
		
		long timeOut = 60000;
		
		if(globalDefaults.get("Timeout").compareTo("") != 0)
			timeOut = Long.parseLong(globalDefaults.get("Timeout"));
		
		if(((String) params.get(SessionObject.CONTROL_WAITREQUEST_TIMEOUT)).compareTo("") != 0)
			timeOut = Long.parseLong((String) params.get(SessionObject.CONTROL_WAITREQUEST_TIMEOUT));
		
		timeOutUp = false;
		
		// Test if the session has already been created
		if(sessionTable.isEmpty() == false)
			for(int i=0; i<sessionTable.size(); i++)
				if(sessionTable.get(i).getMySessionId().equals(localId))
				{
					mySessionTable = sessionTable.get(i);
					indexSession = i;
				}
		
		// Remove a request from receivedRequestTable
		if(receivedRequestTable.isEmpty() == false)
		{
			receivedRequestTable.removeFirst();
		}

		// Read buffer and erase it
		synchronized (requestBuffer) 
		{
			if(requestBuffer.isEmpty())
			{	
				try
				{
					requestBuffer.wait(timeOut);
				}
				catch(Exception e)
				{
					throw new IsacRuntimeException("Buffer writing failed : " + e);
				}
			}
			
			if(requestBuffer.isEmpty() == false)
			{
				myRequestTable = requestBuffer.removeFirst();
			}
			else
			{
				timeOutUp = true;
			}
		}
		
		if(timeOutUp == false)
		{	
			if(indexSession != -1)
				sessionTable.remove(indexSession);
			
			byte[] content = (byte[])myRequestTable.getMyRequest().getContent();
			
			if (content != null)
			{
				String sipBody = new String(content);
			}
			
			
			logger.log(Level.DEBUG, "Read Request");
			
			String toTag = ((FromHeader) myRequestTable.getMyRequest().getHeader(SIPHeaderNames.FROM)).getTag();
			String fromTag = ((ToHeader) myRequestTable.getMyRequest().getHeader(SIPHeaderNames.TO)).getTag();
			
			String method = myRequestTable.getMyRequest().getMethod();
			String callId = ((CallIdHeader) myRequestTable.getMyRequest().getHeader(SIPHeaderNames.CALL_ID)).getCallId();
			
			mySessionTable.setMySessionId(localId);
			mySessionTable.setMySessionCallId(callId);
			mySessionTable.setMySessionFromTag(fromTag);
			mySessionTable.setMySessionToTag(toTag);
			mySessionTable.setMySessionCallFlow(method, myRequestTable.getMyTime());
			mySessionTable.setMySessionExpires(0);
			
			sessionTable.addLast(mySessionTable);
			receivedRequestTable.add(myRequestTable);
			
			// Export message to be used in doGet
			getMessage = myRequestTable.getMyRequest();
		}
	}
	
	/**
	 * Initialize a request
	 * 
	 * @param params : A map of the parameters of the control action
	 */
	public void initRequest(Map params)
	{
		HashMap<String, String> headerTable = new HashMap<String, String>();
		
		if(requestTableParam.containsKey("REQUEST") == true)
		{
			headerTable = requestTableParam.get("REQUEST");
			
			requestTableParam.remove("REQUEST");
			
			if(headerTable.containsKey("Request_Id") == true)
				headerTable.remove("Request_Id");
			if(headerTable.containsKey("Method") == true)
				headerTable.remove("Method");
			if(headerTable.containsKey("Call-ID") == true)
				headerTable.remove("Call-ID");
			if(headerTable.containsKey("Content-Length") == true)
				headerTable.remove("Content-Length");
			if(headerTable.containsKey("CSeq_Sequence_Number") == true)
				headerTable.remove("CSeq_Sequence_Number");
			if(headerTable.containsKey("From_Tag") == true)
				headerTable.remove("From_Tag");
			if(headerTable.containsKey("Add_To_Tag") == true)
				headerTable.remove("Add_To_Tag");
			if(headerTable.containsKey("To_Tag") == true)
				headerTable.remove("To_Tag");
			if(headerTable.containsKey("Authorization") == true)
				headerTable.remove("Authorization");
			if(headerTable.containsKey("via") == true)
				headerTable.remove("via");
			if(headerTable.containsKey("Other_Headers") == true)
				headerTable.remove("Other_Headers");
		}
		
		headerTable.put("Request_Id", (String) params.get(SessionObject.CONTROL_REQUEST_REQUESTID));
		headerTable.put("Method", (String) params.get(SessionObject.CONTROL_REQUEST_METHOD));
		headerTable.put("User_Name", (String) params.get(SessionObject.CONTROL_REQUEST_USERNAME));
		headerTable.put("Host", (String) params.get(SessionObject.CONTROL_REQUEST_HOST));
		headerTable.put("Call-ID", (String) params.get(SessionObject.CONTROL_REQUEST_CALLID));
		headerTable.put("Content-Length", (String) params.get(SessionObject.CONTROL_REQUEST_CONTENTLENGTH));
		headerTable.put("CSeq_Sequence_Number", (String) params.get(SessionObject.CONTROL_REQUEST_CSEQSEQUENCENUMBER));
		headerTable.put("From_Tag", (String) params.get(SessionObject.CONTROL_REQUEST_FROMTAG));
		headerTable.put("Add_To_Tag", (String) params.get(SessionObject.CONTROL_REQUEST_ADDTOTAG));
		headerTable.put("To_Tag", (String) params.get(SessionObject.CONTROL_REQUEST_TOTAG));
		headerTable.put("via", (String) params.get(SessionObject.CONTROL_REQUEST_VIA));
		headerTable.put("Authorization", (String) params.get(SessionObject.CONTROL_REQUEST_AUTHORIZATION));
		headerTable.put("Generate_Authorization", (String) params.get(SessionObject.CONTROL_REQUEST_GENERATEAUTHORIZATION));
		headerTable.put("Other_Headers", (String) params.get(SessionObject.CONTROL_REQUEST_OTHERHEADERS));
		
		requestTableParam.put("REQUEST", headerTable);
	}
	
	/**
	 * Initialize a response
	 * 
	 * @param params : A map of the parameters of the control action
	 */
	public void initResponse(Map params)
	{
		HashMap<String, String> headerTable = new HashMap<String, String>();
		
		if(requestTableParam.containsKey("RESPONSE") == true)
		{
			headerTable = requestTableParam.get("RESPONSE");
			
			requestTableParam.remove("RESPONSE");
			
			if(headerTable.containsKey("Response_Id") == true)
				headerTable.remove("Response_Id");
			if(headerTable.containsKey("Status_Code") == true)
				headerTable.remove("Status_Code");
			if(headerTable.containsKey("Reason_Phrase") == true)
				headerTable.remove("Reason_Phrase");
			if(headerTable.containsKey("Content-Length") == true)
				headerTable.remove("Content-Length");
			if(headerTable.containsKey("To_Tag") == true)
				headerTable.remove("To_Tag");
			if(headerTable.containsKey("Other_Headers") == true)
				headerTable.remove("Other_Headers");
		}
		
		headerTable.put("Response_Id", (String) params.get(SessionObject.CONTROL_RESPONSE_RESPONSEID));
		headerTable.put("Status_Code", (String) params.get(SessionObject.CONTROL_RESPONSE_STATUSCODE));
		headerTable.put("Reason_Phrase", (String) params.get(SessionObject.CONTROL_RESPONSE_REASONPHRASE));
		headerTable.put("Content-Length", (String) params.get(SessionObject.CONTROL_RESPONSE_CONTENTLENGTH));
		headerTable.put("To_Tag", (String) params.get(SessionObject.CONTROL_RESPONSE_TOTAG));
		headerTable.put("Other_Headers", (String) params.get(SessionObject.CONTROL_RESPONSE_OTHERHEADERS));
		
		requestTableParam.put("RESPONSE", headerTable);
	}
	
	/**
	 * Initialize parameters for each responses
	 * 
	 * @param params : A map of the parameters of the control action
	 */
	public void setResponseDefaults(Map params)
	{
		HashMap<String, String> headerTable = new HashMap<String, String>();
		
		if(requestTableParam.containsKey("SETRESPONSE") == false)
		{
			headerTable.put("Add_Contact", (String) params.get(SessionObject.CONTROL_SETRESPONSEDEFAULTS_ADDCONTACT));
			headerTable.put("Qvalue", (String) params.get(SessionObject.CONTROL_SETRESPONSEDEFAULTS_QVALUE));
			headerTable.put("Expires", (String) params.get(SessionObject.CONTROL_SETRESPONSEDEFAULTS_EXPIRES));
			headerTable.put("Content-Type", (String) params.get(SessionObject.CONTROL_SETRESPONSEDEFAULTS_CONTENTTYPE));
			headerTable.put("Other_Default_Headers", (String) params.get(SessionObject.CONTROL_SETRESPONSEDEFAULTS_OTHERHEADERS));
			
			requestTableParam.put("SETRESPONSE", headerTable);
		}
		else
		{
			headerTable = requestTableParam.get("SETRESPONSE");
			
			requestTableParam.remove("SETRESPONSE");
			
			if(headerTable.containsKey("Content-Type") == true)
				headerTable.remove("Content-Type");
			if(headerTable.containsKey("Add_Contact") == true)
				headerTable.remove("Add_Contact");
			if(headerTable.containsKey("Qvalue") == true)
				headerTable.remove("Qvalue");
			if(headerTable.containsKey("Expires") == true)
				headerTable.remove("Expires");
			if(headerTable.containsKey("Other_Default_Headers") == true)
				headerTable.remove("Other_Default_Headers");
			
			headerTable.put("Add_Contact", (String) params.get(SessionObject.CONTROL_SETRESPONSEDEFAULTS_ADDCONTACT));
			headerTable.put("Qvalue", (String) params.get(SessionObject.CONTROL_SETRESPONSEDEFAULTS_QVALUE));
			headerTable.put("Expires", (String) params.get(SessionObject.CONTROL_SETRESPONSEDEFAULTS_EXPIRES));
			headerTable.put("Content-Type", (String) params.get(SessionObject.CONTROL_SETRESPONSEDEFAULTS_CONTENTTYPE));
			headerTable.put("Other_Default_Headers", (String) params.get(SessionObject.CONTROL_SETRESPONSEDEFAULTS_OTHERHEADERS));
			
			requestTableParam.put("SETRESPONSE", headerTable);
		}
	}
	
	public void doDeleteSession(Map params)
	{
		close();
	}
	
	
	/**
	 * Create and send request using request_id as session identity
	 */
	public void doRequest()
	{	
		HashMap<String, String> initTable = new HashMap<String, String>();
		HashMap<String, String> headerTable = new HashMap<String, String>();
		HashMap<String, String> methodTable = new HashMap<String, String>();
		HashMap<String, String> bodyTable = new HashMap<String, String>();
		HashMap<String, String> globalDefaults = new HashMap<String, String>();
		
		MySessionTable mySessionTable = new MySessionTable();
		int indexSession = -1;
		
		initTable = requestTableParam.get("INIT");
		globalDefaults = requestTableParam.get("GLOBAL");
		headerTable = requestTableParam.get("REQUEST");
		methodTable = requestTableParam.get(headerTable.get("Method"));

		// Test if the session has already been created
		if(sessionTable.isEmpty() == false)
		{
			for(int i=0; i<sessionTable.size(); i++)
			{
				if(sessionTable.get(i).getMySessionId().equals(headerTable.get("Request_Id")))
				{
					mySessionTable = sessionTable.get(i);
					indexSession = i;
				}
			}
		}
		
		// Creation of Call-Id header
		CallIdHeader callIdHeader = null;
		
		if(headerTable.get("Call-ID").compareTo("") == 0)
		{
			if(mySessionTable.getMySessionCallId().compareTo("") != 0)
			{
				try
				{
					callIdHeader = headerFactory.createCallIdHeader(mySessionTable.getMySessionCallId());
				}
				catch(ParseException e)
				{
					throw new IsacRuntimeException("Unable to create CallId Header : " + e);
				}
			}
			else
				callIdHeader = sipProvider.getNewCallId();
		}
		else
		{
			try
			{
				callIdHeader = headerFactory.createCallIdHeader(headerTable.get("Call-ID"));
			}
			catch(ParseException e)
			{
				throw new IsacRuntimeException("Unable to create CallId Header : " + e);
			}
		}

		// Creation of From header
		String fromTag = null;
		SipURI address = null;
		
		try
		{
			address = addressFactory.createSipURI((String) globalDefaults.get("Local_User_Name"), (String) globalDefaults.get("Local_Domain"));
		}
		catch(ParseException e)
		{
			throw new IsacRuntimeException("Unable to create From address : " + e);
		}
		
		String imsParameter = "";
		
		if(!ParameterParser.getCheckBox(globalDefaults.get("Ims_Parameter")).isEmpty())
			imsParameter = ParameterParser.getCheckBox(globalDefaults.get("Ims_Parameter")).get(0);
		
		if(imsParameter.compareTo(enable) ==  0)
		{
			String localImpu[] = globalDefaults.get("Ims_Impu").replaceFirst("sip:", "").split("@");
			String imsUser = localImpu[0];
			String imsHost = localImpu[1];
			
			try
			{
				address = addressFactory.createSipURI(imsUser, imsHost);
			}
			catch(ParseException e)
			{
				throw new IsacRuntimeException("Unable to create From address : " + e);
			}
		}
		
		Address fromNameAddress = addressFactory.createAddress(address);
		
		if((String) globalDefaults.get("Local_Display_Name") != "")
		{
			try
			{
				fromNameAddress.setDisplayName((String) globalDefaults.get("Local_Display_Name"));
			}
			catch(ParseException e)
			{
				throw new IsacRuntimeException("Unable to add From Display Name : " + e);
			}
		}

		fromTag = addTag((String) headerTable.get("From_Tag"));

		if(mySessionTable.getMySessionFromTag().compareTo("") != 0)
			fromTag = mySessionTable.getMySessionFromTag();

		FromHeader fromHeader = null;
		
		try
		{
			fromHeader = headerFactory.createFromHeader(fromNameAddress, fromTag);
		}
		catch(ParseException e)
		{
			throw new IsacRuntimeException("Unable to create From header : " + e);
		}
		
		// Creation of To header
		String toTag = null;

		try
		{
			address = addressFactory.createSipURI((String) globalDefaults.get("Remote_User_Name"), (String) globalDefaults.get("Remote_Domain"));
		}
		catch(ParseException e)
		{
			throw new IsacRuntimeException("Unable to create To address : " + e);
		}
		
		Address toNameAddress = addressFactory.createAddress(address);

		if((String) globalDefaults.get("Remote_Display_Name") != "")
		{
			try
			{
				toNameAddress.setDisplayName((String) globalDefaults.get("Remote_Display_Name"));
			}
			catch(ParseException e)
			{
				throw new IsacRuntimeException("Unable to add To Display Name : " + e);
			}
		}

		String fromInTo = "";
		
		if(!ParameterParser.getCheckBox(methodTable.get("From_In_To")).isEmpty())
			fromInTo = ParameterParser.getCheckBox(methodTable.get("From_In_To")).get(0);
		
		if(fromInTo.compareTo(enable) ==  0)
			toNameAddress = fromNameAddress;
		
		if((mySessionTable.getMySessionToTag() != null) && (mySessionTable.getMySessionToTag().compareTo("") != 0))
			toTag = mySessionTable.getMySessionToTag();

		if(headerTable.get("Add_To_Tag").compareTo("") != 0)
		{
			toTag = addTag((String) headerTable.get("To_Tag"));
		}		
		
		ToHeader toHeader = null;
		
		
		try
		{
			toHeader = headerFactory.createToHeader(toNameAddress, toTag);
		}
		catch(ParseException e)
		{
			throw new IsacRuntimeException("Unable to create To header : " + e);
		}

		// Creation of Request URI
		String requestUriUserName = null;
		
		if(headerTable.get("User_Name").compareTo("") != 0)
			requestUriUserName = (String) headerTable.get("User_Name");
		
		SipURI requestURI = null;
		
		try
		{
			requestURI = addressFactory.createSipURI(requestUriUserName, (String) headerTable.get("Host"));
		}
		catch(ParseException e)
		{
			throw new IsacRuntimeException("Unable to create Request URI : " + e);
		}

		// Creation of Via header	
		ArrayList<ViaHeader> viaHeaders = new ArrayList<ViaHeader>();
		List<List<String>> viaList = ParameterParser.getTable(methodTable.get("via"));
		List<List<String>> viaBranch = ParameterParser.getTable(headerTable.get("via"));
		
		String branch = "";

		if (mySessionTable.getMySessionBranch() != null)
		{
			if(mySessionTable.getMySessionBranch().compareTo("") != 0)
				branch = mySessionTable.getMySessionBranch();
		}
		
		String[] column;
		
		for(int i=0; i<viaList.size(); i++)
		{
			if(viaBranch.size()>0)
			{
				branch = viaBranch.get(i).toString().replace("[", "").replace("]", "");
			}
			column = viaList.get(i).toString().replace("[", "").replace("]", "").split(", ", 7);

			ViaHeader viaHeader = null;
			
			try
			{
				branch = addBranch(branch);
				mySessionTable.setMySessionBranch(branch);
				viaHeader = headerFactory.createViaHeader(column[0], new Integer(column[1]), initTable.get("Transport"), branch);
			}
			catch(ParseException e)
			{
				throw new IsacRuntimeException("Unable to create Via header : " + e);
			}
			catch(InvalidArgumentException e)
			{
				throw new IsacRuntimeException("Invalid Via arguments : " + e);
			}

			// Add optional argument to Via
			try
			{
				if(column[2].compareTo("") != 0)
					viaHeader.setMAddr(column[2]);
				if(column[3].compareTo("") != 0)
					viaHeader.setProtocol(column[3]);
				if(column[4].compareTo("") != 0)
					viaHeader.setReceived(column[4]);
			}
			catch(ParseException e)
			{
				throw new IsacRuntimeException("Unable to add Via arguments : " + e);
			}
			
			try
			{
				if(column[5].compareTo("") != 0)
					viaHeader.setRPort();
				if(column[6].compareTo("") != 0)
					viaHeader.setTTL(Integer.parseInt(column[6]));
			}
			catch(InvalidArgumentException e)
			{
				throw new IsacRuntimeException("Invalid Via arguments : " + e);
			}
			
			viaHeaders.add(viaHeader);
		}

		// Creation of sequence number in CSeq
		long sequenceNumber = mySessionTable.getMySessionCSeq(headerTable.get("Method"));
		
		if(headerTable.get("CSeq_Sequence_Number").compareTo("") != 0)
			sequenceNumber = Long.parseLong((String) headerTable.get("CSeq_Sequence_Number"));

		// Creation of request
		Request request = null;
		
		try
		{
			request = messageFactory.createRequest(requestURI, 
					headerTable.get("Method"), 
					callIdHeader, 
					headerFactory.createCSeqHeader(sequenceNumber, headerTable.get("Method")), 
					fromHeader, 
					toHeader, 
					viaHeaders, 
					headerFactory.createMaxForwardsHeader(Integer.parseInt((String) globalDefaults.get("Max-Forwards"))));
		}
		catch(ParseException e) 
		{
			throw new IsacRuntimeException("Unable to create request : " + e);
		}
		catch(InvalidArgumentException e)
		{
			throw new IsacRuntimeException("Invalid argument : " + e);
		}
		
		// Creation of Contact header	
		String addContact = "";
		
		if(!ParameterParser.getCheckBox(methodTable.get("Add_Contact")).isEmpty())
			addContact = ParameterParser.getCheckBox(methodTable.get("Add_Contact")).get(0);
		
		if(addContact.compareTo(enable) ==  0)
		{
			SipURI contactURI = null;
			
			try
			{
				contactURI = addressFactory.createSipURI((String) globalDefaults.get("Local_User_Name"), (String) initTable.get("Ip_Address"));
			}
			catch(ParseException e)
			{
				throw new IsacRuntimeException("Unable to create Contact address : " + e);
			}
			
			contactURI.setPort(Integer.parseInt(initTable.get("Port")));
			
			Address contactAddress = addressFactory.createAddress(contactURI);
			
			try
			{
				contactAddress.setDisplayName((String) globalDefaults.get("Local_Display_Name"));
			}
			catch(ParseException e)
			{
				throw new IsacRuntimeException("Unable to add Contact Display Name : " + e);
			}
			
			ContactHeader contactHeader = headerFactory.createContactHeader(contactAddress);
		
			// Add optional argument to Contact
			if(methodTable.get("Qvalue").compareTo("") != 0)
			{
				try
				{
					contactHeader.setQValue(Float.parseFloat(methodTable.get("Qvalue")));
				}
				catch(InvalidArgumentException e)
				{
					throw new IsacRuntimeException("Invalid QValue argument : " + e);
				}
			}
			
			if(methodTable.get("Expires").compareTo("") != 0)
			{
				try
				{
					contactHeader.setExpires(Integer.parseInt(methodTable.get("Expires")));
				}
				catch(InvalidArgumentException e)
				{
					throw new IsacRuntimeException("Invalid Expires argument : " + e);
				}
			}
			
			request.addHeader(contactHeader);
		}
		
		// Creation of Content-Type && Content-Length header
		int contentLength = 0;

		if(headerTable.get("Content-Length").compareTo("") != 0)
		{
			contentLength = Integer.parseInt((String) headerTable.get("Content-Length"));
		}
		
		String message;
		
		if(methodTable.get("Content-Type").contains("none") == false)
		{
			String contentTypeValue[] = methodTable.get("Content-Type").split("/");
			ContentTypeHeader contentTypeHeader = null;
			
			try
			{
				contentTypeHeader = headerFactory.createContentTypeHeader(contentTypeValue[0], contentTypeValue[1]);
			}
			catch(ParseException e)
			{
				throw new IsacRuntimeException("Unable to create Content-Type header : " + e);
			}
			
			request.addHeader(contentTypeHeader);
			
			if(methodTable.get("Content-Type").compareTo("application/sdp") == 0)
			{
				try
				{
					message = sdpFunction();
				}
				catch(SdpParseException e)
				{
					throw new IsacRuntimeException("Unable to create SDP body : " + e);
				}
					
				try
				{
					request.setContent(message, contentTypeHeader);
				}
				catch(ParseException e)
				{
					throw new IsacRuntimeException("Unable to add content to the body : " + e);
				}
				
				contentLength = message.length();
			}
			else 
			{
				bodyTable = requestTableParam.get("BODY");
				
				try
				{
					request.setContent(bodyTable.get("Message"), contentTypeHeader);
				}
				catch(ParseException e)
				{
					throw new IsacRuntimeException("Unable to add content to the body : " + e);
				}
				
				contentLength = bodyTable.get("Message").length();
			}
		}

		try
		{
			request.setContentLength(headerFactory.createContentLengthHeader(contentLength));
		}
		catch(InvalidArgumentException e)
		{
			throw new IsacRuntimeException("Invalid Content-Length argument : " + e);
		}

		// Creation of User-Agent header
		if(globalDefaults.get("User_Agent").compareTo("") != 0)
		{
			List<String> userAgent = new ArrayList<String>();
			userAgent.add((String) globalDefaults.get("User_Agent"));
			
			try
			{
				request.addHeader(headerFactory.createUserAgentHeader(userAgent));
			}
			catch(ParseException e)
			{
				throw new IsacRuntimeException("Unable to create User-Agent header : " + e);
			}
		}
		
		// Creation of Authorization header
		if(headerTable.get("Authorization").compareTo("") != 0)
		{
			try
			{
				request.addHeader(authorizationFunction(headerTable.get("Method"), requestURI));
			}
			catch(ParseException e)
			{
				throw new IsacRuntimeException("Unable to create Authorization header : " + e);
			}
			catch(NoSuchAlgorithmException e)
			{
				throw new IsacRuntimeException("Algorithm no available : " + e);
			}
		}
		
		// Creation of other headers from method
		if(headerTable.get("Other_Headers").compareTo("") != 0)
			addRequestHeader((String) headerTable.get("Other_Headers"), request);
		
		// Creation of other headers from setMethodDefault
		if(methodTable.get("Other_Default_Headers").compareTo("") != 0)
			addRequestHeader((String) methodTable.get("Other_Default_Headers"), request);
		
		// Creation of other headers from setGlobalDefault
		if(globalDefaults.get("Other_Headers").compareTo("") != 0)
			addRequestHeader((String) globalDefaults.get("Other_Headers"), request);

		// Send request		
		try
		{
			sipProvider.sendRequest(request);
		}
		catch(SipException e)
		{
			throw new IsacRuntimeException("Unable to send request : " + e);
		}

		if(indexSession != -1)
			sessionTable.remove(indexSession);
		
		mySessionTable.setMySessionId(headerTable.get("Request_Id"));
		mySessionTable.setMySessionCallId(callIdHeader.getCallId());
		mySessionTable.setMySessionFromTag(fromTag);
		mySessionTable.setMySessionToTag(toTag);
		mySessionTable.setMySessionCallFlow(headerTable.get("Method"), System.currentTimeMillis());
		mySessionTable.setMySessionExpires(0);
		
		sessionTable.addLast(mySessionTable);
	}
	
	/**
	 * Create and send response
	 */
	public void doResponse()
	{	
		HashMap<String, String> initTable = new HashMap<String, String>();
		HashMap<String, String> headerTable = new HashMap<String, String>();
		HashMap<String, String> methodTable = new HashMap<String, String>();
		HashMap<String, String> bodyTable = new HashMap<String, String>();
		HashMap<String, String> globalDefaults = new HashMap<String, String>();

		initTable = requestTableParam.get("INIT");
		globalDefaults = requestTableParam.get("GLOBAL");
		headerTable = requestTableParam.get("RESPONSE");
		methodTable = requestTableParam.get("SETRESPONSE");

		if(!receivedRequestTable.isEmpty())
		{	
			MyRequestTable myRequestTable = receivedRequestTable.getFirst();

			// Creation of From header
			Address fromHeaderAddress = ((FromHeader) myRequestTable.getMyRequest().getHeader(SIPHeaderNames.FROM)).getAddress();
			String fromTag = ((FromHeader) myRequestTable.getMyRequest().getHeader(SIPHeaderNames.FROM)).getTag();
			
			FromHeader fromHeader = null;
			
			try
			{
				fromHeader = headerFactory.createFromHeader(fromHeaderAddress, fromTag);
			}
			catch(ParseException e)
			{
				throw new IsacRuntimeException("Unable to create From header : " + e);
			}

			// Creation of To header
			String tempTag = "";

			Address toHeaderAddress = ((ToHeader) myRequestTable.getMyRequest().getHeader(SIPHeaderNames.TO)).getAddress();

			if(((ToHeader) myRequestTable.getMyRequest().getHeader(SIPHeaderNames.TO)).getTag() != null)
				tempTag = ((ToHeader) myRequestTable.getMyRequest().getHeader(SIPHeaderNames.TO)).getTag();

			String toTag = addTag(tempTag);

			if(((String) headerTable.get("To_Tag")).compareTo("") != 0)
				toTag = (String) headerTable.get("To_Tag");

			ToHeader toHeader = null;
			
			try
			{
				toHeader = headerFactory.createToHeader(toHeaderAddress, toTag);
			}
			catch(ParseException e)
			{
				throw new IsacRuntimeException("Unable to create To header : " + e);
			}

			// Creation of Via header
			ArrayList<ViaHeader> viaHeaders = new ArrayList<ViaHeader>();

			ViaHeader viaHeader = (ViaHeader) myRequestTable.getMyRequest().getHeader(SIPHeaderNames.VIA);
			
			viaHeaders.add(viaHeader);

			// Creation of sequence number in CSeq
			long sequenceNumber = ((CSeqHeader) myRequestTable.getMyRequest().getHeader(SIPHeaderNames.CSEQ)).getSeqNumber();

			// Creation of response
			Response response = null;
			
			try
			{
				response = messageFactory.createResponse(Integer.parseInt(headerTable.get("Status_Code")), 
						(CallIdHeader) myRequestTable.getMyRequest().getHeader(SIPHeaderNames.CALL_ID), 
						headerFactory.createCSeqHeader(sequenceNumber, myRequestTable.getMyRequest().getMethod()), 
						fromHeader, 
						toHeader, 
						viaHeaders, 
						(MaxForwardsHeader) myRequestTable.getMyRequest().getHeader(SIPHeaderNames.MAX_FORWARDS));
			}
			catch(ParseException e)
			{
				throw new IsacRuntimeException("Unable to create response : " + e);
			}
			catch(InvalidArgumentException e)
			{
				throw new IsacRuntimeException("Invalid argument : " + e);
			}
			
			try
			{
				response.setReasonPhrase(headerTable.get("Reason_Phrase"));
			}
			catch(ParseException e)
			{
				throw new IsacRuntimeException("Unable to add Reason Phrase : " + e);
			}
			
			// Creation of Contact header
			String addContact = "";
			
			if(!ParameterParser.getCheckBox(methodTable.get("Add_Contact")).isEmpty())
				addContact = ParameterParser.getCheckBox(methodTable.get("Add_Contact")).get(0);
			
			if(addContact.compareTo(enable) == 0)
			{
				SipURI contactURI = null;
				
				try
				{
					contactURI = addressFactory.createSipURI((String) globalDefaults.get("Local_User_Name"), (String) initTable.get("Ip_Address"));
				}
				catch(ParseException e)
				{
					throw new IsacRuntimeException("Unable to add Contact Sip Uri : " + e);
				}
				
				contactURI.setPort(Integer.parseInt(initTable.get("Port")));
				
				Address contactAddress = addressFactory.createAddress(contactURI);
				
				try
				{
					contactAddress.setDisplayName((String) globalDefaults.get("Local_Display_Name"));
				}
				catch(ParseException e)
				{
					throw new IsacRuntimeException("Unable to add Contact Display Name : " + e);
				}
				
				ContactHeader contactHeader = headerFactory.createContactHeader(contactAddress);
				
				// Add optional argument to Contact
				if(methodTable.get("Qvalue").compareTo("") != 0)
				{
					try
					{
						contactHeader.setQValue(Float.parseFloat(methodTable.get("Qvalue")));
					}
					catch(InvalidArgumentException e)
					{
						throw new IsacRuntimeException("Invalid QValue argument : " + e);
					}
				}
				
				if(methodTable.get("Expires").compareTo("") != 0)
				{
					try
					{
						contactHeader.setExpires(Integer.parseInt(methodTable.get("Expires")));
					}
					catch(InvalidArgumentException e)
					{
						throw new IsacRuntimeException("Invalid QValue argument : " + e);
					}
				}
				
				response.addHeader(contactHeader);
			}
			
			// Creation of Content-Type && Content-Length header
			int contentLength = 0;
			
			if(headerTable.get("Content-Length").compareTo("") != 0)
				contentLength = Integer.parseInt((String) headerTable.get("Content-Length"));
			
			if(methodTable.get("Content-Type").contains("none") == false)
			{
				String contentTypeValue[] = methodTable.get("Content-Type").split("/");
				ContentTypeHeader contentTypeHeader = null;
				
				try
				{
					contentTypeHeader = headerFactory.createContentTypeHeader(contentTypeValue[0], contentTypeValue[1]);
				}
				catch(ParseException e)
				{
					throw new IsacRuntimeException("Unable to create Content-Type argument : " + e);
				}
				
				response.addHeader(contentTypeHeader);

				if(methodTable.get("Content-Type").compareTo("application/xml") == 0)
				{
					bodyTable = requestTableParam.get("XML");
					
					try
					{
						response.setContent(bodyTable.get("Message"), contentTypeHeader);
					}
					catch(ParseException e)
					{
						throw new IsacRuntimeException("Unable to add content to the body : " + e);
					}
					
					contentLength = bodyTable.get("Message").length();
				}
				else if(methodTable.get("Content-Type").compareTo("application/sdp") == 0)
				{
					String message;
					
					try
					{
						message = sdpFunction();
					}
					catch(SdpParseException e)
					{
						throw new IsacRuntimeException("Unable to create SDP body : " + e);
					}
						
					try
					{
						response.setContent(message, contentTypeHeader);
					}
					catch(ParseException e)
					{
						throw new IsacRuntimeException("Unable to add content to the body : " + e);
					}
					
					contentLength = message.length();
				}
			}

			try
			{
				response.setContentLength(headerFactory.createContentLengthHeader(contentLength));
			}
			catch(InvalidArgumentException e)
			{
				throw new IsacRuntimeException("Invalid Content-Length argument : " + e);
			}

			// Creation of User-Agent header
			if(globalDefaults.get("User_Agent").compareTo("") != 0)
			{
				List<String> userAgent = new ArrayList<String>();
				userAgent.add((String) globalDefaults.get("User_Agent"));
				
				try
				{
					response.addHeader(headerFactory.createUserAgentHeader(userAgent));
				}
				catch(ParseException e)
				{
					throw new IsacRuntimeException("Unable to create User-Agent header : " + e);
				}	
			}
			
			// Creation of other headers from response
			if(headerTable.get("Other_Headers").compareTo("") != 0)
				addResponseHeader((String) headerTable.get("Other_Headers"), response);
			
			// Creation of other headers from setResponseDefaults
			if(methodTable.get("Other_Default_Headers").compareTo("") != 0)
				addResponseHeader((String) methodTable.get("Other_Default_Headers"), response);
			
			// Creation of other headers from setGlobalDefault
			if(globalDefaults.get("Other_Headers").compareTo("") != 0)
				addResponseHeader((String) globalDefaults.get("Other_Headers"), response);
			
			// Send response
			try
			{
				sipProvider.sendResponse(response);
			}
			catch(SipException e)
			{
				throw new IsacRuntimeException("Unable to send response : " + e);
			}
		}
	}
	
	
	///////////////////////////////
	// TestAction implementation //
	///////////////////////////////
	
	/**
	 * @see org.ow2.clif.scenario.isac.plugin.TestAction#doTest()
	 */
	public boolean doTest(int number, Map params) 
	{
		int code = 0;
		String method;
		boolean result = false;
		String noChoice = null;
		
		switch (number)
		{
			case TEST_ISTIMEOUT:
				if(timeOutUp == true)
				{
					if(((String) params.get(SessionObject.TEST_ISTIMEOUT_NOTCHOICE)).compareTo("") == 0)
						result = true;
					else
						result = false;
				}
				break;
			case TEST_REQUESTISNOTIFY:
				if(receivedRequestTable.isEmpty() == false)
				{
					method = receivedRequestTable.getFirst().getMyRequest().getMethod();

					if(((String) params.get(SessionObject.TEST_REQUESTISNOTIFY_NOTCHOICE)).compareTo("") == 0)
						result = testRequestIsMethod(method, "NOTIFY");
					else
						result = testRequestIsNotMethod(method, "NOTIFY");
				}
				else
				{
					if(((String) params.get(SessionObject.TEST_REQUESTISNOTIFY_NOTCHOICE)).compareTo("") == 0)
						result = false;
					else
						result = true;
				}
				break;
			case TEST_REQUESTISSUBSCRIBE:
				if(receivedRequestTable.isEmpty() == false)
				{
					method = receivedRequestTable.getFirst().getMyRequest().getMethod();
					
					if(((String) params.get(SessionObject.TEST_REQUESTISSUBSCRIBE_NOTCHOICE)).compareTo("") == 0)
						result = testRequestIsMethod(method, "SUBSCRIBE");
					else
						result = testRequestIsNotMethod(method, "SUBSCRIBE");
				}
				else
				{
					if(((String) params.get(SessionObject.TEST_REQUESTISSUBSCRIBE_NOTCHOICE)).compareTo("") == 0)
						result = false;
					else
						result = true;
				}
				break;
			case TEST_REQUESTISMESSAGE:
				if(receivedRequestTable.isEmpty() == false)
				{
					method = receivedRequestTable.getFirst().getMyRequest().getMethod();
					
					if(((String) params.get(SessionObject.TEST_REQUESTISMESSAGE_NOTCHOICE)).compareTo("") == 0)
						result = testRequestIsMethod(method, "MESSAGE");
					else
						result = testRequestIsNotMethod(method, "MESSAGE");
				}
				else
				{
					if(((String) params.get(SessionObject.TEST_REQUESTISMESSAGE_NOTCHOICE)).compareTo("") == 0)
						result = false;
					else
						result = true;
				}
				break;
			case TEST_REQUESTISOPTIONS:
				if(receivedRequestTable.isEmpty() == false)
				{
					method = receivedRequestTable.getFirst().getMyRequest().getMethod();
					
					if(((String) params.get(SessionObject.TEST_REQUESTISOPTIONS_NOTCHOICE)).compareTo("") == 0)
						result = testRequestIsMethod(method, "OPTIONS");
					else
						result = testRequestIsNotMethod(method, "OPTIONS");
				}
				else
				{
					if(((String) params.get(SessionObject.TEST_REQUESTISOPTIONS_NOTCHOICE)).compareTo("") == 0)
						result = false;
					else
						result = true;
				}
				break;
				
			case TEST_REQUESTISCANCEL:
				if(receivedRequestTable.isEmpty() == false)
				{
					method = receivedRequestTable.getFirst().getMyRequest().getMethod();
					
					if(((String) params.get(SessionObject.TEST_REQUESTISCANCEL_NOTCHOICE)).compareTo("") == 0)
						result = testRequestIsMethod(method, "CANCEL");
					else
						result = testRequestIsNotMethod(method, "CANCEL");
					
				}
				else
				{
					if(((String) params.get(SessionObject.TEST_REQUESTISCANCEL_NOTCHOICE)).compareTo("") == 0)
						result = false;
					else
						result = true;
				}
				break;
			case TEST_REQUESTISBYE:			
				noChoice = (String) params.get(SessionObject.TEST_REQUESTISBYE_NOTCHOICE);
				
				if(receivedRequestTable.isEmpty() == false)
				{
					method = receivedRequestTable.getFirst().getMyRequest().getMethod();
					
					if(noChoice.compareTo("") == 0)
						result = testRequestIsMethod(method, "BYE");
					else
						result = testRequestIsNotMethod(method, "BYE");
				}
				else
				{
					if(noChoice.compareTo("") == 0)
						result = false;
					else
						result = true;
				}
				break;
				
			case TEST_REQUESTISACK:
				noChoice = (String) params.get(SessionObject.TEST_REQUESTISACK_NOTCHOICE);
				
				if(receivedRequestTable.isEmpty() == false)
				{
					method = receivedRequestTable.getFirst().getMyRequest().getMethod();
					
					if(noChoice.compareTo("") == 0)
						result = testRequestIsMethod(method, "ACK");
					else
						result = testRequestIsNotMethod(method, "ACK");
				}
				else
				{
					if(noChoice == null)
						result = false;
					else
						result = true;
				}
				break;
				
			case TEST_REQUESTISINVITE:
				noChoice = (String) params.get(SessionObject.TEST_REQUESTISINVITE_NOTCHOICE);
				
				if(receivedRequestTable.isEmpty() == false)
				{
					method = receivedRequestTable.getFirst().getMyRequest().getMethod();
					
					if(noChoice.compareTo("") == 0)
						result = testRequestIsMethod(method, "INVITE");
					else
						result = testRequestIsNotMethod(method, "INVITE");
				}
				else
				{
					if(noChoice.compareTo("") == 0)
						result = false;
					else
						result = true;
				}
				break;
			case TEST_REQUESTISREGISTER:
				if(receivedRequestTable.isEmpty() == false)
				{
					method = receivedRequestTable.getFirst().getMyRequest().getMethod();
					
					if(((String) params.get(SessionObject.TEST_REQUESTISREGISTER_NOTCHOICE)).compareTo("") == 0)
						result = testRequestIsMethod(method, "REGISTER");
					else
						result = testRequestIsNotMethod(method, "REGISTER");
				}
				else
				{
					if(((String) params.get(SessionObject.TEST_REQUESTISREGISTER_NOTCHOICE)).compareTo("") == 0)
						result = false;
					else
						result = true;
				}
				break;
			case TEST_RESPONSEIS6XX:
				if(receivedResponseTable.isEmpty() == false)
				{
					code = receivedResponseTable.getFirst().getMyResponse().getStatusCode();
					
					if(((String) params.get(SessionObject.TEST_RESPONSEIS6XX_NOTCHOICE)).compareTo("") == 0)
						result = testResponseCodeIsNumber(code, 600, 700);
					else
						result = testResponseCodeIsNotNumber(code, 600, 700);
				}
				else
				{
					if(((String) params.get(SessionObject.TEST_RESPONSEIS6XX_NOTCHOICE)).compareTo("") == 0)
						result = false;
					else
						result = true;
				}
				break;
			case TEST_RESPONSEIS5XX:
				if(receivedResponseTable.isEmpty() == false)
				{
					code = receivedResponseTable.getFirst().getMyResponse().getStatusCode();
					
					if(((String) params.get(SessionObject.TEST_RESPONSEIS5XX_NOTCHOICE)).compareTo("") == 0)
						result = testResponseCodeIsNumber(code, 500, 600);
					else
						result = testResponseCodeIsNotNumber(code, 500, 600);
				}
				else
				{
					if(((String) params.get(SessionObject.TEST_RESPONSEIS5XX_NOTCHOICE)).compareTo("") == 0)
						result = false;
					else
						result = true;
				}
				break;
			case TEST_RESPONSEIS4XX:
				if(receivedResponseTable.isEmpty() == false)
				{
					code = receivedResponseTable.getFirst().getMyResponse().getStatusCode();
					
					if(((String) params.get(SessionObject.TEST_RESPONSEIS4XX_NOTCHOICE)).compareTo("") == 0)
						result = testResponseCodeIsNumber(code, 400, 500);
					else
						result = testResponseCodeIsNotNumber(code, 400, 500);
				}
				else
				{
					if(((String) params.get(SessionObject.TEST_RESPONSEIS4XX_NOTCHOICE)).compareTo("") == 0)
						result = false;
					else
						result = true;
				}
				break;
			case TEST_RESPONSEIS3XX:
				if(receivedResponseTable.isEmpty() == false)
				{
					code = receivedResponseTable.getFirst().getMyResponse().getStatusCode();
					
					if(((String) params.get(SessionObject.TEST_RESPONSEIS3XX_NOTCHOICE)).compareTo("") == 0)	
						result = testResponseCodeIsNumber(code, 300, 400);
					else
						result = testResponseCodeIsNotNumber(code, 300, 400);
				}
				else
				{
					if(((String) params.get(SessionObject.TEST_RESPONSEIS3XX_NOTCHOICE)).compareTo("") == 0)
						result = false;
					else
						result = true;
				}
				break;
			case TEST_RESPONSEIS2XX:
				if(receivedResponseTable.isEmpty() == false)
				{
					code = receivedResponseTable.getFirst().getMyResponse().getStatusCode();
					
					if(((String) params.get(SessionObject.TEST_RESPONSEIS2XX_NOTCHOICE)).compareTo("") == 0)
						result = testResponseCodeIsNumber(code, 200, 300);
					else
						result = testResponseCodeIsNotNumber(code, 200, 300);
				}
				else
				{
					if(((String) params.get(SessionObject.TEST_RESPONSEIS2XX_NOTCHOICE)).compareTo("") == 0)
						result = false;
					else
						result = true;
				}
				break;
			case TEST_RESPONSEIS1XX:
				if(receivedResponseTable.isEmpty() == false)
				{
					code = receivedResponseTable.getFirst().getMyResponse().getStatusCode();
					
					if(((String) params.get(SessionObject.TEST_RESPONSEIS1XX_NOTCHOICE)).compareTo("") == 0)
						result = testResponseCodeIsNumber(code, 100, 200);
					else
						result = testResponseCodeIsNotNumber(code, 100, 200);
				}
				else
				{
					if(((String) params.get(SessionObject.TEST_RESPONSEIS1XX_NOTCHOICE)).compareTo("") == 0)
						result = false;
					else
						result = true;
				}
				break;

			case TEST_RESPONSECODEIS:

				boolean tmpResult = true;
				
				if(receivedResponseTable.isEmpty() == false)
				{
					code = receivedResponseTable.getFirst().getMyResponse().getStatusCode();
					int codeExpected = Integer.parseInt((String) params.get(SessionObject.TEST_RESPONSECODEIS_RESPONSECODE));

					result = testResponseCodeIs(code, codeExpected);

					if ((params.get(SessionObject.TEST_RESPONSECODEIS_METHOD) != null) && ((String) params.get(SessionObject.TEST_RESPONSECODEIS_METHOD)).compareTo("") != 0)
					{
						String awaitedCseqMethod,receivedCseqMethod;

						receivedCseqMethod = ((CSeqHeader) receivedResponseTable.getFirst().getMyResponse().getHeader(SIPHeaderNames.CSEQ)).getMethod();
						awaitedCseqMethod = (String) params.get(SessionObject.TEST_RESPONSECODEIS_METHOD);
						tmpResult = (awaitedCseqMethod.compareTo(receivedCseqMethod) == 0);
					}

					if (!(((String) params.get(SessionObject.TEST_RESPONSECODEIS_NOTCHOICE)).compareTo("") == 0))
						result = !(result && tmpResult);
					else
						result = (result && tmpResult);

				}
				else
				{
					if(((String) params.get(SessionObject.TEST_RESPONSECODEIS_NOTCHOICE)).compareTo("") == 0)
						result = false;
					else
						result = true;
				}
				break;
			default:
			throw new Error(
					"Unable to find this test in ~SipInjector~ ISAC plugin: "
							+ number);
		}
		
		return result;
	}
	
	/**
	 * Test if the response code is between min and max
	 * 
	 * @param code : The response status code
	 * @param min : The lower limit of the interval to test
	 * @param max : The upper limit of the interval to test
	 * @return the result of the test
	 */
	public boolean testResponseCodeIsNumber(int code, int min, int max)
	{
		boolean test = false;
		
		if((code >= min) && (code < max))
			test = true;
		
		return test ;
	}
	
	/**
	 * Test if the response code is not between min and max
	 * 
	 * @param code : The response status code
	 * @param min : The lower limit of the interval to test
	 * @param max : The upper limit of the interval to test
	 * @return the result of the test
	 */
	public boolean testResponseCodeIsNotNumber(int code, int min, int max)
	{
		boolean test = true;
		
		if((code >= min) && (code < max))
			test = false;
		
		return test ;
	}
	
	/**
	 * Test if the response code is the one given in parameter
	 * 
	 * @param code : The response status code
	 * @param codeExpected : The status code you want to compare
	 * @return the result of the test
	 */
	public boolean testResponseCodeIs(int code, int codeExpected)
	{
		boolean test = false;
		
		if(code == codeExpected)
			test = true;
		
		return test;
	}
	
	/**
	 * Test if the response code is not the one given in parameter
	 * 
	 * @param code : The response status code
	 * @param codeExpected : The status code you want to compare
	 * @return the result of the test
	 */
	public boolean testResponseCodeIsNot(int code, int codeExpected)
	{
		boolean test = true;
		
		if(code == codeExpected)
			test = false;
		
		return test;
	}
	
	/**
	 * Test if the request method is the one given in parameter
	 *
	 * @param method : The request method
	 * @param methodExpected : The method you want to compare
	 * @return the result of the test
	 */
	public boolean testRequestIsMethod(String method, String methodExpected)
	{
		boolean test = false;
		
		if(method.compareTo(methodExpected) == 0)
			test = true;
		
		return test;
	}
	
	/**
	 * Test if the request method is not the one given in parameter
	 *
	 * @param method : The request method
	 * @param methodExpected : The method you want to compare
	 * @return the result of the test
	 */
	public boolean testRequestIsNotMethod(String method, String methodExpected)
	{
		boolean test = true;
		
		if(method.compareTo(methodExpected) == 0)
			test = false;
		
		return test;
	}
	
	
	////////////////////////////////////////////
	// SipListenner Interface implementation //
	//////////////////////////////////////////
	
	/**
	 * Event-based method called by the SIP stack when a request is received.
	 * Add the request to the end of the buffer.
	 * 
	 * @param requestEvent : the received request object
	 */
	
	public void processRequest(RequestEvent requestEvt)
	{
		// Put request in buffer
		Long receivedTime = System.currentTimeMillis();
		MyRequestTable myRequestTable = new MyRequestTable();
		myRequestTable.setMyTime(receivedTime);
		myRequestTable.setMyRequest(requestEvt.getRequest());

		synchronized (requestBuffer) 
		{
			requestBuffer.addLast(myRequestTable);
			requestBuffer.notify();
		}
	}
	
	/**
	 * Event-based method called by the SIP stack when a response is received.
	 * Add the response to the end of the buffer.
	 * 
	 * @param responseEvent : the received response object
	 */
	public void processResponse(ResponseEvent responseEvt)
	{
		// Put response in buffer
		Long receivedTime = System.currentTimeMillis();
		MyResponseTable myResponseTable = new MyResponseTable();
		myResponseTable.setMyTime(receivedTime);
		myResponseTable.setMyResponse(responseEvt.getResponse());

		synchronized (this) 
		{
			responseBuffer.addLast(myResponseTable);
			this.notify();
		}
	}

	public void processTimeout(TimeoutEvent timeoutEvt) {}

	public void processIOException(IOExceptionEvent ioExceptionEvt) {}
	
	public void processTransactionTerminated(TransactionTerminatedEvent transTermEvt) {}
	
	public void processDialogTerminated(DialogTerminatedEvent dialogTermEvt) {}

	
	///////////////
	// SIP Tools //
	///////////////
	
	/**
	 * Add headers to a request. Used to add optional headers.
	 * 
	 * @param vectorTable : a String with the given headers to add
	 * @param request : the request you want to add headers
	 * @throws ParseException
	 */
	private void addRequestHeader(String vectorTable, Request request)
	{
		List<List<String>> parametersTable = ParameterParser.getTable(vectorTable);

		String[] finalCol;

		for(int i=0; i<parametersTable.size(); i++)
		{
			finalCol = parametersTable.get(i).toString().split(", ", 2);

			finalCol[0] = finalCol[0].substring(1, finalCol[0].length());

			if(request.getHeader(finalCol[0]) == null)
			{
				finalCol[1] = finalCol[1].substring(0, finalCol[1].length() - 1);

				try
				{
					request.addHeader(headerFactory.createHeader(finalCol[0], finalCol[1]));
				}
				catch(ParseException e)
				{
					throw new IsacRuntimeException("Unable to add header : " + e);
				}	
			}
		}
	}
	
	/**
	 * Add headers to a response. Used to add optional headers.
	 * 
	 * @param vectorTable : a String with the given headers to add
	 * @param response : the response you want to add headers
	 * @throws ParseException
	 */
	private void addResponseHeader(String vectorTable, Response response)
	{
		List<List<String>> parametersTable = ParameterParser.getTable(vectorTable);
		
		String[] finalCol;

		for(int i=0; i<parametersTable.size(); i++)
		{
			finalCol = parametersTable.get(i).toString().split(", ", 2);

			finalCol[0] = finalCol[0].substring(1, finalCol[0].length());
			
			if(response.getHeader(finalCol[0]) == null)
			{
				finalCol[1] = finalCol[1].substring(0, finalCol[1].length() - 1);
				
				try
				{
					response.addHeader(headerFactory.createHeader(finalCol[0], finalCol[1]));
				}
				catch(ParseException e)
				{
					throw new IsacRuntimeException("Unable to add header : " + e);
				}
			}
		}
	}
	
	/**
	 * Create the tag of From field and To field
	 * 
	 * @param localTag : the value of the field Tag
	 * @return the final tag (empty or not)
	 */
	private String addTag(String localTag)
	{
		String finalTag;

		if((localTag.compareTo("") == 0) || (localTag == null))
			finalTag = Utils.generateTag();
		else
			finalTag = localTag;
		
		return finalTag;
	}
	
	/**
	 * Creation of Branch
	 * 
	 * @param localBranch : the value of the field Branch
	 * @return the final branch (never empty)
	 */
	private String addBranch(String localBranch)
	{
		String finalBranch;
		
		if(localBranch.compareTo("") == 0)
		{
			finalBranch = Utils.generateBranchId();
		}	
		else
		{
			finalBranch = localBranch;
		}
		
		return finalBranch;
	}
	
	/**
	 * Creation of Authorization header
	 * 
	 * @param method : the method of the request
	 * @return the header Authorization
	 * @throws ParseException
	 * @throws NoSuchAlgorithmException
	 */
	private AuthorizationHeader authorizationFunction(String method, SipURI requestURI) throws ParseException, NoSuchAlgorithmException
	{
		HashMap<String, String> localTable = requestTableParam.get("GLOBAL");
		HashMap<String, String> methodTable = requestTableParam.get("REQUEST");
		String response = "";
		String localUserName = localTable.get("Local_User_Name");
		
		String imsParameter = "";
		
		if(!ParameterParser.getCheckBox(localTable.get("Ims_Parameter")).isEmpty())
			imsParameter = ParameterParser.getCheckBox(localTable.get("Ims_Parameter")).get(0);
		
		if(imsParameter.compareTo(enable) == 0)
			localUserName = localTable.get("Ims_Impi");

		AuthorizationHeader localAuthorization = headerFactory.createAuthorizationHeader(localTable.get("Scheme"));
		
		String generateAuthorization = "";
		
		if(!ParameterParser.getCheckBox(methodTable.get("Generate_Authorization")).isEmpty())
			generateAuthorization = ParameterParser.getCheckBox(methodTable.get("Generate_Authorization")).get(0);
		
		if(receivedResponseTable.isEmpty() == false && generateAuthorization.compareTo(enable) == 0 && localTable.get("Response").compareTo("") == 0)
		{
			Response localResponse = receivedResponseTable.getFirst().getMyResponse();
	
			String nonce = ((WWWAuthenticate) (localResponse.getHeader(SIPHeaderNames.WWW_AUTHENTICATE))).getNonce();
			String cNonce = ((WWWAuthenticate) (localResponse.getHeader(SIPHeaderNames.WWW_AUTHENTICATE))).getCNonce();

			MessageDigest messageDigest = MessageDigest.getInstance(localTable.get("Algorithm"));
			
			// A1
			String A1 = localUserName + ":" + localTable.get("Realm") + ":" + localTable.get("Password") ;
			byte mdbytes[] = messageDigest.digest(A1.getBytes());
			String HA1 = toHexString(mdbytes);
			
			// A2
			String A2 = method.toUpperCase() + ":" + requestURI ;
			mdbytes = messageDigest.digest(A2.getBytes());
			String HA2 = toHexString(mdbytes);
			
			// KD
			String KD = HA1 + ":" + nonce;
			
			if(cNonce != null) 
			{
			   if(cNonce.length()>0)
			   {
				   KD += ":" + cNonce;
				   localAuthorization.setCNonce(cNonce);
			   }
			}
			
			KD += ":" + HA2;
			
			mdbytes = messageDigest.digest(KD.getBytes());
			
			response = toHexString(mdbytes);
			
			localAuthorization.setNonce(nonce);
		}
		
		localAuthorization.setURI(requestURI);

		if(localTable.get("Algorithm").compareTo("") != 0)
			localAuthorization.setAlgorithm(localTable.get("Algorithm"));
		if(localTable.get("Cnonce").compareTo("") != 0)
			localAuthorization.setCNonce(localTable.get("Cnonce"));
		if(localTable.get("Nonce").compareTo("") != 0)
			localAuthorization.setNonce(localTable.get("Nonce"));
		if(localTable.get("Nonce_Count").compareTo("") != 0)
			localAuthorization.setNonceCount(Integer.parseInt(localTable.get("Nonce_Count")));
		if(localTable.get("Opaque").compareTo("") != 0)
			localAuthorization.setOpaque(localTable.get("Opaque"));
		if(localTable.get("Qop").compareTo("") != 0)
			localAuthorization.setQop(localTable.get("Qop"));
		if(localTable.get("Realm").compareTo("") != 0)
			localAuthorization.setRealm(localTable.get("Realm"));
		
		if(generateAuthorization.compareTo(enable) == 0)
		{
			if(localTable.get("Response").compareTo("") != 0)
				response = localTable.get("Response");
		}
		else
		{
			if(localTable.get("Response").compareTo("") != 0)
				response = localTable.get("Response");
		}
		
		localAuthorization.setResponse(response);
		localAuthorization.setUsername(localUserName);

		if(localTable.get("Realm").compareTo("") != 0)
			localAuthorization.setRealm(localTable.get("Realm"));

		return(localAuthorization);
	}
	
	/**
	 * Convert Byte to String 
	 * 
	 * @param b : byte
	 * @return string representation
	 */
	public static String toHexString(byte byteTable[]) 
	{
        int position = 0;
        char[] character = new char[byteTable.length*2];
        
        for (int i=0; i<byteTable.length; i++)
        {
        	character[position++] = toHex[(byteTable[i] >> 4) & 0x0F];
        	character[position++] = toHex[byteTable[i] & 0x0f];
        }
        
        return new String(character);
    }

	/**
	 * Creation of SDP body
	 * 
	 * @return the body to insert into the request or response
	 * @throws SdpParseException
	 */
	
	private String sdpFunction() throws SdpParseException
	{	
		HashMap<String, String> localTable = requestTableParam.get("SDP");
		List<List<String>> parametersTable;
		parametersTable = ParameterParser.getTable(localTable.get("Other_Headers"));
		String mediaFormatFinal = "";

		String sessionId = String.valueOf(SdpFactory.getNtpTime(new Date()));
		String sessionVersion = String.valueOf(SdpFactory.getNtpTime(new Date()));
		String startTime = String.valueOf(SdpFactory.getNtpTime(new Date()));
		String stopTime = String.valueOf(SdpFactory.getNtpTime(new Date()));

		String[] result = localTable.get("Media_Format").split(";");
	    
		for (int i=0; i<result.length; i++)
			mediaFormatFinal = mediaFormatFinal + " " + result[i];

		if(localTable.get("Session_Id").compareTo("") != 0)
			sessionId = localTable.get("Session_Id");
		if(localTable.get("Session_Version").compareTo("") != 0)
			sessionVersion = localTable.get("Session_Version");
		if(localTable.get("Start_Time").compareTo("") != 0)
			startTime = localTable.get("Start_Time");
		if(localTable.get("Stop_Time").compareTo("") != 0)
			stopTime = localTable.get("Stop_Time");
	
		String body = "v=" + localTable.get("Attribute_V") + "\r\n"
					+ "o=" + localTable.get("Owner_User_Name")
					+ " " + sessionId + " " + sessionVersion
					+ " " + localTable.get("Owner_Network_Type")
					+ " " + localTable.get("Owner_Address_Type")
					+ " " + localTable.get("Owner_Address") + "\r\n"
					+ "s=" + localTable.get("Attribute_S") + "\r\n"
					+ "t=" + startTime + " " + stopTime + "\r\n"
					+ "m=" + localTable.get("Media_Type")
					+ " " + localTable.get("Media_Port")
					+ " " + localTable.get("Media_Proto")
					+ mediaFormatFinal + "\r\n";

		String totalCol;
		String finalCol[];
		int i = 0;
		
		if(localTable.get("Other_Headers") != null)
			while(i != parametersTable.size())
			{
				totalCol = parametersTable.get(i).toString();

				finalCol = totalCol.split(", ");
	
				finalCol[0] = finalCol[0].substring(1, finalCol[0].length());

				finalCol[1] = finalCol[1].substring(0, finalCol[1].length() - 1);
					
				body = body + finalCol[0] + "=" + finalCol[1] + "\r\n";
				
				i++;
			}

		return body;
	}
}
