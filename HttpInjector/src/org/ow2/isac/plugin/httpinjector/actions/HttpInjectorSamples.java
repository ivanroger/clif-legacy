/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2004-2006, 2012 France Telecom
 * Copyright (C) 2015-2017 Orange SA
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * Contact: clif@ow2.org
 */

package org.ow2.isac.plugin.httpinjector.actions;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.Map;
import org.htmlparser.NodeFilter;
import org.htmlparser.Parser;
import org.htmlparser.Node;
import org.htmlparser.lexer.nodes.TagNode;
import org.htmlparser.tags.ImageTag;
import org.htmlparser.tags.FrameTag;
import org.htmlparser.tags.ScriptTag;
import org.htmlparser.util.ParserException;
import org.apache.commons.httpclient.Header;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpMethod;
//import org.apache.commons.httpclient.NTCredentials;
import org.apache.commons.httpclient.NameValuePair;
import org.apache.commons.httpclient.URIException;
import org.apache.commons.httpclient.URI;
import org.apache.commons.httpclient.UsernamePasswordCredentials;
import org.apache.commons.httpclient.auth.AuthScope;
import org.apache.commons.httpclient.methods.DeleteMethod;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.methods.HeadMethod;
import org.apache.commons.httpclient.methods.InputStreamRequestEntity;
import org.apache.commons.httpclient.methods.OptionsMethod;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.PutMethod;
import org.apache.commons.httpclient.methods.StringRequestEntity;
import org.apache.commons.httpclient.methods.multipart.FilePart;
import org.apache.commons.httpclient.methods.multipart.MultipartRequestEntity;
import org.apache.commons.httpclient.methods.multipart.Part;
import org.ow2.clif.scenario.isac.exception.IsacRuntimeException;
import org.ow2.clif.scenario.isac.util.ParameterParser;
import org.ow2.clif.storage.api.ActionEvent;
import org.ow2.clif.util.ClifClassLoader;
import org.ow2.isac.plugin.httpinjector.SessionObject;
import org.ow2.isac.plugin.httpinjector.tools.HostConfigurationUtils;
import org.ow2.isac.plugin.httpinjector.tools.HttpStateUtils;
import org.ow2.isac.plugin.httpinjector.tools.ParameterConstants;

/**
 * This class implements the HttpInjector sample methods.
 *
 * @author JC Meillaud
 * @author A Peyrard
 * @author Emmanuel Varoquaux
 * @author Bruno Dillenseger
 */
public class HttpInjectorSamples {
	public static final int	MAXATTEMPT	= 1;

	/**
	 * Executes a previously initialized HttpMethod.
	 *
	 * @param sessionObject
	 *            The sessionObject which handles the httpClient.
	 * @param report
	 *            The ActionReport to update..
	 * @param httpMethod
	 *            the HttpMethod to execute
	 * @param responseVar if not null, the response body is stored as a variable 
	 * with the given name in the session object(@see DataProvider)
	 */
	private static void executeMethod(SessionObject sessionObject,
			ActionEvent report, HttpMethod httpMethod, String responseVar) {
		final HttpClient httpClient = sessionObject.getHttpClient();
		int responseCode;
		Header[] responseHeaders;
		String responseBody;
		String msg;

		if (responseVar != null && responseVar.isEmpty())
		{
			responseVar = null;
		}
		if (responseVar != null)
		{
			sessionObject.clearVariable(responseVar);
		}
		responseCode = -1;
		responseHeaders = null;
		responseBody = null;
		msg = null;
		httpMethod.addRequestHeader("User-Agent", sessionObject.getUserAgent());
		report.setDate(System.currentTimeMillis());
		long start_ns = 0; 
		if (sessionObject.unitIsMicros())
		{
			start_ns = System.nanoTime();
		}
		for (int attempt = 0; responseCode < 0 && attempt < MAXATTEMPT; attempt++) {
			try {
				responseCode = httpClient.executeMethod(httpMethod);
				msg = httpMethod.getStatusLine().toString();
				responseHeaders = httpMethod.getResponseHeaders();
				if (sessionObject.inDepthLoadEnabled() || responseVar != null)
				{
					responseBody = httpMethod.getResponseBodyAsString();
				}
				if (responseVar != null)
				{
					sessionObject.setVariable(responseVar, responseBody);
				}
				if (sessionObject.unitIsMicros())
				{
					report.duration = (int)((System.nanoTime() - start_ns) / 1000);
				}
				else
				{
					report.duration = (int)(System.currentTimeMillis() - report.getDate());
				}
			}
			catch (Exception e) {
				msg = e.getMessage();
			}
			finally {
				httpMethod.releaseConnection();
			}
		}
		sessionObject.setLastStatusCode(responseCode);
		sessionObject.setLastHeader(responseHeaders);
		sessionObject.setLastResponseBody(responseBody);
		report.result = msg;
		if (responseBody != null && sessionObject.inDepthLoadEnabled())
		{
			try
			{
				report.duration += inDepthLoad(sessionObject, responseBody, httpMethod.getURI());
			}
			catch (URIException e) {}
		}
		report.successful = responseCode >= 0 && sessionObject.isSuccessful(responseCode);
	}

	/**
	 * Sets the credentials for a particular host and realm.
	 *
	 * @param sessionObject
	 *            The sessionObject.
	 * @param params
	 *            Map containing all variables.
	 */
	private static void setCredentials(SessionObject sessionObject, Map<String,String> params) {
		String hostAuth, realm;
		String username, password;

		if (
			(hostAuth = params.get(ParameterConstants.HOSTAUTH)) == null
			|| hostAuth.equals(""))
		{
			hostAuth = AuthScope.ANY_HOST;
		}
		if (
			(realm = params.get(ParameterConstants.REALM)) == null
			|| realm.equals(""))
		{
			realm = AuthScope.ANY_REALM;
		}
		username = params.get(ParameterConstants.USERNAME);
		password = params.get(ParameterConstants.PASSWORD);
		if (username != null && !username.equals(""))
		{
			sessionObject.getHttpClient().getState().setCredentials(
				new AuthScope(hostAuth, AuthScope.ANY_PORT, realm),
				new UsernamePasswordCredentials(username, password));
				// new NTCredentials(username, password, null, ""));  // null host is not allowed
		}
	}

	/**
	 * Builds an array of name/value pairs and returns this array,
	 * that will be set into the query string.
	 *
	 * @param inputParameters
	 *            a String containing all the parameters in a serialized way
	 * @return An array of name/value pairs.
	 */
	private static NameValuePair[] setInputParameters(List<String> inputParameters)
	{
		Set<NameValuePair> res = new HashSet<NameValuePair>();
		Iterator<String> paramIter = inputParameters.iterator();

		while (paramIter.hasNext())
		{
			String token = paramIter.next();
			int index = token.indexOf('=');
			res.add(new NameValuePair(token.substring(0, index), token.substring(index+1)));
		}
		return res.toArray(new NameValuePair[res.size()]);
	}


	private static String completeUri(String uri, Map<String,String> params) {
		String scheme = uri.substring(0, 5);
		if (!scheme.equals("http:") && !scheme.equals("https"))
			params.put(ParameterConstants.URI, uri = "http://" + uri);
		return uri;
	}

	/**
	 * Loads frames (recursively) and images included in a given HTML body.
	 * 
	 * @param session the session object.
	 * @param body the HTML text.
	 * @param baseURI the URI of this HTML text (necessary to resolve relative paths).
	 */
	static private long inDepthLoad(SessionObject session, String body, URI baseURI)
	{
		long startTime;
		Parser parser;
		List<Node> nodes = new ArrayList<Node>();
		GetMethod method;
		String rootPath;
		String responseBody;

		parser = Parser.createParser(body);
		try
		{
			if (session.loadFramesEnabled())
			{
				nodes.addAll(Arrays.asList(parser.extractAllNodesThatAre(FrameTag.class)));
				parser.reset();
			}
			if (session.loadImagesEnabled())
			{
				nodes.addAll(Arrays.asList(parser.extractAllNodesThatAre(ImageTag.class)));
				parser.reset();
			}
			if (session.loadJsEnabled())
			{
				nodes.addAll(Arrays.asList(parser.extractAllNodesThatAre(ScriptTag.class)));
				parser.reset();
			}
			if (session.loadCssEnabled())
			{
				nodes.addAll(Arrays.asList(parser.extractAllNodesThatMatch(
					new NodeFilter()
					{
						@Override
						public boolean accept(Node node)
						{
							return
								node instanceof TagNode
								&& ((TagNode)node).getTagName().equalsIgnoreCase("link");
						}
					}).toNodeArray()));
			}
		}
		catch (ParserException e) {
			return 0;
		}
		try {
			rootPath = baseURI.getPath().substring(0, 1 + baseURI.getPath().lastIndexOf('/'));
		}
		catch (URIException e) {
			return 0;
		}
		if (session.unitIsMicros())
		{
			startTime = System.nanoTime();
		}
		else
		{
			startTime = System.currentTimeMillis();
		}
		for (Node node : nodes)
		{
			String path = null;
			if (node instanceof FrameTag)
			{
				path = ((FrameTag)node).getFrameLocation();
			}
			else if (node instanceof ImageTag)
			{
				path = ((ImageTag)node).extractImageLocn();
			}
			else if (node instanceof ScriptTag)
			{
				path = ((ScriptTag)node).getAttribute("src");
			}
			else
			{
				path = ((TagNode)node).getAttribute("href");
			}
			if (path != null)
			{
				if (path.startsWith("http:") || path.startsWith("https:")
					|| path.startsWith("ftp:") || path.startsWith("file:"))
				{
					try
					{
						method = new GetMethod(URLEncoder.encode(path, "UTF-8"));
					}
					catch (UnsupportedEncodingException ex)
					{
						throw new IsacRuntimeException("Exception while encoding " + path + " (GET " + baseURI + ").", ex);
					}
				}
				else
				{
					method = new GetMethod(baseURI.toString());
					if (path.startsWith("/"))
					{
						method.setPath(path);
					}
					else
					{
						method.setPath(rootPath + path);
					}
				}
				try
				{
					session.getHttpClient().executeMethod(method);
				}
				catch (IOException e2)
				{
					continue;
				}
				finally
				{
					method.releaseConnection();
				}
				try
				{
					if (node instanceof FrameTag
						&& (responseBody = method.getResponseBodyAsString()) != null)
					{
						inDepthLoad(session, responseBody, method.getURI());
					}
				}
				catch (Exception ex)
				{
					throw new IsacRuntimeException("Exception while loading frame " + path + " in " + baseURI, ex);
				}
			}
		}
		if (session.unitIsMicros())
		{
			return (System.nanoTime() - startTime) / 1000;
		}
		else
		{
			return System.currentTimeMillis() - startTime;
		}
	}

	/**
	 * Does a HTTP GET.
	 *
	 * @param sessionObject
	 *            The sessionObject which handles the httpClient.
	 * @param report
	 *            The ActionReport to update.
	 * @param params
	 *            A Map containing all the useful variable.
	 * @return the report.
	 */
	public static ActionEvent doGet(SessionObject sessionObject,
			ActionEvent report, Map<String,String> params) {
		final HttpClient httpClient = sessionObject.getHttpClient();
		String uri, parameters, redirect;
		GetMethod method;

		setActionType(report, params, "HTTP GET");
		if (!params.containsKey(ParameterConstants.URI)) {
			report.result = "Undefined URI";
			report.successful = false;
			return report;
		}
		uri = completeUri(params.get(ParameterConstants.URI), params);
		setActionComment(report, params, uri);
		parameters = params.get(ParameterConstants.QUERYPARAMETERS);
		redirect = params.get(ParameterConstants.REDIRECT);
		if (params.containsKey(ParameterConstants.USERNAME) && params.containsKey(ParameterConstants.PASSWORD))
		{
			setCredentials(sessionObject, params);
		}
		method = new GetMethod(uri);
		if (parameters != null && !parameters.equals(""))
		{
			method.setQueryString(setInputParameters(ParameterParser.getCheckBox(parameters)));
		}
		method.setFollowRedirects(
			ParameterParser.getCheckBox(redirect).contains(ParameterConstants.ENABLED));
		httpClient.setState(HttpStateUtils.setHttpState(sessionObject, params));
		try {
			httpClient.setHostConfiguration(HostConfigurationUtils
					.setHostConfiguration(sessionObject, params));
		}
		catch (URIException e) {
			report.result = "Malformed URI";
			report.successful = false;
			return report;
		}
		setHeaders(method, params.get(ParameterConstants.HEADERS));
		executeMethod(sessionObject, report, method, params.get(ParameterConstants.RESPONSE_BODY));
		return report;
	}

	/**
	 * Does a HTTP POST.
	 *
	 * @param sessionObject
	 *            The sessionObject which handles the httpClient.
	 * @param report
	 *            The ActionReport to update.
	 * @param params
	 *            A Map containing all the useful variable.
	 * @return the report.
	 */
	public static ActionEvent doPost(
		SessionObject sessionObject,
		ActionEvent report,
		Map<String,String> params)
	{
		final HttpClient httpClient = sessionObject.getHttpClient();
		String uri, queryParameters, bodyParameters, redirect, request, file;
		PostMethod method;

		setActionType(report, params, "HTTP POST");
		if (!params.containsKey(ParameterConstants.URI)) {
			setActionComment(report, params, "Undefined URI");
			report.successful = false;
			return report;
		}
		uri = completeUri(params.get(ParameterConstants.URI), params);
		setActionComment(report, params, uri);
		queryParameters = params.get(ParameterConstants.QUERYPARAMETERS);
		bodyParameters = params.get(ParameterConstants.BODYPARAMETERS);
		redirect = params.get(ParameterConstants.REDIRECT);
		request = params.get(ParameterConstants.REQUEST);
		file = params.get(ParameterConstants.FILE);
		if (params.containsKey(ParameterConstants.USERNAME) && params.containsKey(ParameterConstants.PASSWORD))
			setCredentials(sessionObject, params);
		method = new PostMethod(uri);
		setHeaders(method, params.get(ParameterConstants.HEADERS));
		if (queryParameters != null && !queryParameters.equals(""))
		{
			method.setQueryString(setInputParameters(ParameterParser.getNField(queryParameters)));
		}
		if (bodyParameters != null && !bodyParameters.equals(""))
		{
			method.setRequestBody(setInputParameters(ParameterParser.getNField(bodyParameters)));
		}
		String contentType = null;
		Header contentTypeHeader = method.getRequestHeader("Content-Type");
		if (contentTypeHeader != null && !contentTypeHeader.toExternalForm().isEmpty())
		{
			contentType = contentTypeHeader.toExternalForm();
		}
		if (file != null && !file.equals(""))
		{
			InputStream fileInputStream;
			if ((fileInputStream = ClifClassLoader.getClassLoader()
					.getResourceAsStream(file)) != null)
			{
				method.setRequestEntity(new InputStreamRequestEntity(fileInputStream, contentType));
			}
			else
			{
				throw new IsacRuntimeException("HttpInjector POST failed: could not read file " + file);
			}
		}
		if (request != null && !request.equals(""))
		{
			try
			{
				method.setRequestEntity(new StringRequestEntity(request, contentType, null));
			}
			catch (Exception ex)
			{
				throw new IsacRuntimeException("Exception when getting parameters for HTTP POST", ex);
			}
		}
		method.setFollowRedirects(
			ParameterParser.getCheckBox(redirect).contains(ParameterConstants.ENABLED));
		httpClient.setState(HttpStateUtils.setHttpState(sessionObject, params));
		try {
			httpClient.setHostConfiguration(HostConfigurationUtils
					.setHostConfiguration(sessionObject, params));
		}
		catch (URIException e)
		{
			report.result = "Malformed URI";
			report.successful = false;
			return report;
		}
		executeMethod(sessionObject, report, method, params.get(ParameterConstants.RESPONSE_BODY));
		return report;
	}

	/**
	 * Does a HTTP MULTIPOST.
	 *
	 * @param sessionObject
	 *            The sessionObject which handles the httpClient.
	 * @param report
	 *            The ActionReport to update.
	 * @param params
	 *            A Map containing all the useful variable.
	 * @return the report.
	 */
	public static ActionEvent doMultiPost(
		SessionObject sessionObject,
		ActionEvent report,
		Map<String,String> params)
	{
		final HttpClient httpClient = sessionObject.getHttpClient();
		String uri, parameters, redirect, files;
		PostMethod method;

		setActionType(report, params, "HTTP MULTIPART POST");
		if (!params.containsKey(ParameterConstants.URI)) {
			setActionComment(report, params, "Undefined URI");
			report.successful = false;
			return report;
		}
		uri = completeUri(params.get(ParameterConstants.URI), params);
		setActionComment(report, params, uri);
		parameters = params.get(ParameterConstants.QUERYPARAMETERS);
		redirect = params.get(ParameterConstants.REDIRECT);
		files = params.get(ParameterConstants.FILES);
		if (params.containsKey(ParameterConstants.USERNAME) && params.containsKey(ParameterConstants.PASSWORD))
			setCredentials(sessionObject, params);
		method = new PostMethod(uri);
		setHeaders(method, params.get(ParameterConstants.HEADERS));
		if (parameters != null && !parameters.equals(""))
			method.setQueryString(setInputParameters(ParameterParser.getNField(parameters)));
		if (files != null && !files.equals(""))
		{
			List<String> fileList = ParameterParser.getNField(files);
			Iterator<String> fileIter = fileList.iterator();
			Part[] parts;
			parts = new Part[fileList.size()];
			int i=0;
			while (fileIter.hasNext())
			{
				String file = fileIter.next();
				try
				{
					File f = new File(file);
					parts[i++] = new FilePart(f.getName(), f);
				}
				catch (Exception ex)
				{
					throw new RuntimeException(uri + " Multipart Post failed:" + ex);
				}
				method.setRequestEntity(new MultipartRequestEntity(parts, method.getParams()));
			}
		}
		method.setFollowRedirects(
			ParameterParser.getCheckBox(redirect).contains(ParameterConstants.ENABLED));
		httpClient.setState(HttpStateUtils.setHttpState(sessionObject, params));
		try {
			httpClient.setHostConfiguration(HostConfigurationUtils
					.setHostConfiguration(sessionObject, params));
		}
		catch (URIException e)
		{
			report.result = "Malformed URI";
			report.successful = false;
			return report;
		}
		executeMethod(sessionObject, report, method, params.get(ParameterConstants.RESPONSE_BODY));
		return report;
	}

	/**
	 * Does a HTTP HEAD.
	 *
	 * @param sessionObject
	 *            The sessionObject which handles the httpClient.
	 * @param report
	 *            The ActionReport to update.
	 * @param params
	 *            A Map containing all the useful variable.
	 * @return the report.
	 */
	public static ActionEvent doHead(SessionObject sessionObject,
			ActionEvent report, Map<String,String> params) {
		final HttpClient httpClient = sessionObject.getHttpClient();
		String uri, parameters, redirect;
		HeadMethod method;

		setActionType(report, params, "HTTP HEAD");
		if (!params.containsKey(ParameterConstants.URI)) {
			setActionComment(report, params, "Undefined URI");
			report.successful = false;
			return report;
		}
		uri = completeUri(params.get(ParameterConstants.URI), params);
		setActionComment(report, params, uri);
		parameters = params.get(ParameterConstants.QUERYPARAMETERS);
		redirect = params.get(ParameterConstants.REDIRECT);
		if (params.containsKey(ParameterConstants.USERNAME) && params.containsKey(ParameterConstants.PASSWORD))
			setCredentials(sessionObject, params);
		method = new HeadMethod(uri);
		setHeaders(method, params.get(ParameterConstants.HEADERS));
		if (parameters != null && !parameters.equals(""))
			method.setQueryString(setInputParameters(ParameterParser.getNField(parameters)));
		method.setFollowRedirects(
			ParameterParser.getCheckBox(redirect).contains(ParameterConstants.ENABLED));

		httpClient.setState(HttpStateUtils.setHttpState(sessionObject, params));
		try {
			httpClient.setHostConfiguration(HostConfigurationUtils
					.setHostConfiguration(sessionObject, params));
		}
		catch (URIException e) {
			report.result = "Malformed URI";
			report.successful = false;
			return report;
		}
		executeMethod(sessionObject, report, method, null);
		return report;
	}

	/**
	 * Does a HTTP OPTIONS.
	 *
	 * @param sessionObject
	 *            The sessionObject which handles the httpClient.
	 * @param report
	 *            The ActionReport to update.
	 * @param params
	 *            A Map containing all the useful variable.
	 * @return the report.
	 */
	public static ActionEvent doOptions(SessionObject sessionObject,
			ActionEvent report, Map<String,String> params) {
		final HttpClient httpClient = sessionObject.getHttpClient();
		String uri, parameters, redirect;
		OptionsMethod method;

		setActionType(report, params, "HTTP OPTIONS");
		if (!params.containsKey(ParameterConstants.URI)) {
			setActionComment(report, params, "Undefined URI");
			report.successful = false;
			return report;
		}
		uri = completeUri(params.get(ParameterConstants.URI), params);
		setActionComment(report, params, uri);
		parameters = params.get(ParameterConstants.QUERYPARAMETERS);
		redirect = params.get(ParameterConstants.REDIRECT);
		if (params.containsKey(ParameterConstants.USERNAME) && params.containsKey(ParameterConstants.PASSWORD))
			setCredentials(sessionObject, params);
		method = new OptionsMethod(uri);
		setHeaders(method, params.get(ParameterConstants.HEADERS));
		if (parameters != null && !parameters.equals(""))
			method.setQueryString(setInputParameters(ParameterParser.getNField(parameters)));
		method.setFollowRedirects(
			ParameterParser.getCheckBox(redirect).contains(ParameterConstants.ENABLED));

		httpClient.setState(HttpStateUtils.setHttpState(sessionObject, params));
		try {
			httpClient.setHostConfiguration(HostConfigurationUtils
					.setHostConfiguration(sessionObject, params));
		}
		catch (URIException e) {
			report.result = "Malformed URI";
			report.successful = false;
			return report;
		}
		executeMethod(sessionObject, report, method, params.get(ParameterConstants.RESPONSE_BODY));
		return report;
	}

	/**
	 * Does a HTTP PUT.
	 *
	 * @param sessionObject
	 *            The sessionObject which handles the httpClient.
	 * @param report
	 *            The ActionReport to update.
	 * @param params
	 *            A Map containing all the useful variable.
	 * @return the report.
	 */
	public static ActionEvent doPut(
		SessionObject sessionObject,
		ActionEvent report,
		Map<String,String> params)
	{
		final HttpClient httpClient = sessionObject.getHttpClient();
		String uri, parameters, redirect, file, fileType;
		PutMethod method;

		setActionType(report, params, "HTTP PUT");
		if (!params.containsKey(ParameterConstants.URI)) {
			setActionComment(report, params, "Undefined URI");
			report.successful = false;
			return report;
		}
		uri = completeUri(params.get(ParameterConstants.URI), params);
		setActionComment(report, params, uri);
		parameters = params.get(ParameterConstants.QUERYPARAMETERS);
		redirect = params.get(ParameterConstants.REDIRECT);
		file = params.get(ParameterConstants.FILE);
		fileType = params.get(ParameterConstants.FILE_TYPE);
		if (params.containsKey(ParameterConstants.USERNAME)
			&& params.containsKey(ParameterConstants.PASSWORD))
		{
			setCredentials(sessionObject, params);
		}
		method = new PutMethod(uri);
		setHeaders(method, params.get(ParameterConstants.HEADERS));
		if (parameters != null && !parameters.equals(""))
		{
			method.setQueryString(setInputParameters(ParameterParser.getNField(parameters)));
		}
		method.setFollowRedirects(
			ParameterParser.getCheckBox(redirect).contains(ParameterConstants.ENABLED));
		if (file != null && !file.equals(""))
		{
			InputStream fileInputStream;
			if ((fileInputStream = ClifClassLoader.getClassLoader().getResourceAsStream(file)) != null)
			{
				if (fileType != null && !fileType.trim().isEmpty())
				{
					method.setRequestEntity(new InputStreamRequestEntity(fileInputStream, fileType.trim()));
				}
				else
				{
					method.setRequestEntity(new InputStreamRequestEntity(fileInputStream));
				}
			}
		}

		httpClient.setState(HttpStateUtils.setHttpState(sessionObject, params));
		try {
			httpClient.setHostConfiguration(HostConfigurationUtils
					.setHostConfiguration(sessionObject, params));
		}
		catch (URIException e) {
			report.result = "Malformed URI";
			report.successful = false;
			return report;
		}
		executeMethod(sessionObject, report, method, params.get(ParameterConstants.RESPONSE_BODY));
		return report;
	}

	/**
	 * Does a HTTP DELETE.
	 *
	 * @param sessionObject
	 *            The sessionObject which handles the httpClient.
	 * @param report
	 *            The ActionReport to update.
	 * @param params
	 *            A Map containing all the useful variable.
	 * @return the report.
	 */
	public static ActionEvent doDelete(SessionObject sessionObject,
			ActionEvent report, Map<String,String> params) {
		final HttpClient httpClient = sessionObject.getHttpClient();
		String uri, parameters, redirect;
		DeleteMethod method;

		setActionType(report, params, "HTTP DELETE");
		if (!params.containsKey(ParameterConstants.URI)) {
			setActionComment(report, params, "Undefined URI");
			report.successful = false;
			return report;
		}
		uri = completeUri(params.get(ParameterConstants.URI), params);
		parameters = params.get(ParameterConstants.QUERYPARAMETERS);
		redirect = params.get(ParameterConstants.REDIRECT);
		if (params.containsKey(ParameterConstants.USERNAME) && params.containsKey(ParameterConstants.PASSWORD))
			setCredentials(sessionObject, params);
		method = new DeleteMethod(uri);
		setHeaders(method, params.get(ParameterConstants.HEADERS));
		if (parameters != null && !parameters.equals(""))
			method.setQueryString(setInputParameters(ParameterParser.getNField(parameters)));
		method.setFollowRedirects(
			ParameterParser.getCheckBox(redirect).contains(ParameterConstants.ENABLED));
		httpClient.setState(HttpStateUtils.setHttpState(sessionObject, params));
		try {
			httpClient.setHostConfiguration(HostConfigurationUtils
					.setHostConfiguration(sessionObject, params));
		}
		catch (URIException e)
		{
			setActionComment(report, params, "Malformed URI");
			report.successful = false;
			return report;
		}

		setActionComment(report, params, uri);
		executeMethod(sessionObject, report, method, params.get(ParameterConstants.RESPONSE_BODY));
		return report;
	}


	/**
	 * Sets headers in the provided HTTP method from the content of a given string.
	 * @param method the target HTTP method object
	 * @param headers a string that must be formatted in a specific way in order to
	 * be parsed as a sequence of header definitions:
	 * "header=header1|value=value1|;header=header2|value=value2...|;header=headerN|value=valueN|;"
	 * special characters | ; = and \ shall be prefixed with \ escape character
	 */
	static void setHeaders(HttpMethod method, String headers)
	{
		if (headers != null)
		{
			List<List<String>> values = ParameterParser.getTable(headers);
			Iterator<List<String>> headerIter = values.iterator();
			while (headerIter.hasNext())
			{
				Iterator<String> entryIter = headerIter.next().iterator();
				String name = entryIter.next();
				method.addRequestHeader(name, entryIter.next());
			}
		}
	}

	static void setActionType(ActionEvent report, Map<String,String> params, String defaultType)
	{
		String actionType = params.get(ParameterConstants.TYPE);
		report.type =
			(actionType == null || actionType.isEmpty()) 
			? defaultType
			: actionType;
	}

	static void setActionComment(ActionEvent report, Map<String,String> params, String defaultComment)
	{
		String actionComment= params.get(ParameterConstants.COMMENT);
		report.comment =
			(actionComment == null || actionComment.isEmpty()) 
			? defaultComment
			: actionComment;
	}
}
