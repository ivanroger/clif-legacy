/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2004 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * CLIF 
 *
 * Contact: clif@ow2.org
 */
package org.ow2.isac.plugin.httpinjector.actions;

import org.apache.commons.httpclient.Header;
import org.ow2.isac.plugin.httpinjector.SessionObject;

/**
 * This class implements the matrix test methods
 * 
 * @author JC Meillaud
 * @author A Peyrard
 */
public class HttpInjectorTests {	
	/**
	 * Simple test to know if the last response was a not found Http Status Code
	 * 
	 * @param sessionObject
	 *            The SessionObject where we get information about the last
	 *            statuscode
	 * @return true if it was a 404 error
	 */

	public static boolean is404Response(SessionObject sessionObject) {
		return (sessionObject.getLastStatusCode() == 404);
	}

	/**
	 * General test to know if the last response Status code is equal to
	 * statusCode
	 * 
	 * @param sessionObject
	 *            The SessionObject where we get information about the last
	 *            statuscode
	 * @param statusCode
	 *            the code
	 * @return true if it was an error equal to statusCode
	 */
	public static boolean isStatusCodeResponse(SessionObject sessionObject,
			int statusCode) {

		return (sessionObject.getLastStatusCode() == statusCode);
	}

	/**
	 * Tests if the latest response contains a given header with a given value
	 * @param sessionObject session object
	 * @param headerType name of the header to look for
	 * @param headerValue the value to compare with the header value
	 * @return true if the header was found among the latest response's headers,
	 * and its value equals (case-insensitive) the given value, false otherwise
	 */
	public static boolean isHeaderValue(
		SessionObject sessionObject,
		String headerType,
		String headerValue)
	{
		Header[] headers = sessionObject.getLastHeader();
		for (int i = 0; i < headers.length; i++)
		{
			if (headers[i].getName().equalsIgnoreCase(headerType))
			{
				return (headers[i].getValue().equals(headerValue));
			}
		}
		return false;
	}

	/**
	 * Tests if the latest response contains a given header
	 * @param sessionObject session object
	 * @param headerType name of the header to look for
	 * @return true if the header was found among the latest response's headers,
	 * false otherwise
	 */
	public static boolean hasHeader(SessionObject sessionObject, String headerType)
	{
		Header[] allHeaders = sessionObject.getLastHeader();
		for (Header header : allHeaders)
		{
			if (header.getName().equalsIgnoreCase(headerType))
			{
				return true;
			}
		}
		return false;
	}
}
