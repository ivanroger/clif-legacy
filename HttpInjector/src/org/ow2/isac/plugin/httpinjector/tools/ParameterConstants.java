/*
* CLIF is a Load Injection Framework
* Copyright (C) 2004, 2006, 2012 France Telecom
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
* Contact: clif@ow2.org
*/

package org.ow2.isac.plugin.httpinjector.tools;

/**
 * This class only holds parameters constants
 * 
 * @author JC Meillaud
 * @author A Peyrard
 * @author Bruno Dillenseger
 */
public class ParameterConstants
{
	public static final int HTTPDEFAULTPORT = 80;
	public static final String EMPTY = "";
	public static final String SEPARATOR = "&";
	public static final String USERAGENT = "useragent";
	public static final String DEFAULTUSERAGENT = "CLIF-ISAC HttpInjector";
	public static final String CUSTOM = "custom";
	public static final String REDIRECT = "redirect";
	public static final String INDEPTHLOAD = "indepthload";
	public static final String ENABLED = "enabled";
	public static final String LOAD_FRAMES = "frames";
	public static final String LOAD_IMAGES = "images";
	public static final String LOAD_CSS = "css";
	public static final String LOAD_JS = "javascript";
	public static final String URI = "uri";
	public static final String HEADERS = "headers";
	public static final String QUERYPARAMETERS = "parameters";
	public static final String BODYPARAMETERS = "bodyparameters";
	public static final String REQUEST = "request";
	public static final String RESPONSE_BODY = "response-body";
	public static final String FILE = "file";
	public static final String FILES = "files";
	public static final String FILE_TYPE = "filetype";
	public static final String RESPONSE_TIME_UNIT = "unit";
	public static final String TYPE = "type";
	public static final String COMMENT = "comment";

	// Test Constants
	public static final String STATUSCODE = "statuscode";
	public static final String HEADERPARAMETERVALUE = "headervalue";
	public static final String HEADERPARAMETERTYPE = "headertype";
	public static final String EQUALITY_OPERATOR = "equality";
	public static final String EQUALITY_EQUAL= "equal";
	public static final String EQUALITY_DIFFERENT = "different";

	// Controls constants
	public static final String CONTROL_SETCONNECTIONTIMEOUT_TIMEOUT = "timeout";
	public static final String CONTROL_SETRESPONSETIMEOUT_TIMEOUT = "timeout";
	public static final String CONTROL_SETERRORRESPONSECODES = "codes";

	// Proxy Configuration
	public static final String PROXY = "proxy";
	public static final int PROXYDEFAULTPORT = 1080;
	public static final String USERNAME = "username";
	public static final String PASSWORD = "password";
	public static final String REALM = "realm";
	public static final String HOSTAUTH = "hostauth";
	public static final String PROXYUSERNAME = "proxyusername";
	public static final String PROXYUSERPASS = "proxyuserpass";
	public static final String PROXYHOST = "proxyhost";
	public static final String PROXYPORT = "proxyport";
	public static final String PREEMPTIVEAUTHENTICATION = "preemptiveauthentication";
	public static final String LOCALADDRESS = "localaddress";

	// Cookie Constants

	public static final String COOKIES = "cookies";
	public static final String COOKIEPOLICY = "cookiepolicy";
	public static final String COOKIEHEADER = "cookieheader";
	public static final int DOMAIN = 0;
	public static final int NAME = 1;
	public static final int VALUE = 2;
	public static final int PATH = 3;
	public static final int EXPIRES = 4;
	public static final int MAXAGE = 5;
	public static final int SECURE = 6;
	public static final String RFC2109 = "rfc2109";
	public static final String RFC2965 = "rfc2965";
	public static final String NETSCAPE = "netscapedraft";
	public static final String COMPATIBILITY = "compatibility";
	public static final String NOCOOKIE = "ignore";
	public static final String SINGLECOOKIEHEADER = "single";
	public static final String MULTIPLECOOKIEHEADERS = "multiple";
}
