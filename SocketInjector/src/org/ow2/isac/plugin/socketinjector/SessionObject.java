/*
* CLIF is a Load Injection Framework
* Copyright (C) 2005, 2006, 2010, 2013 France Telecom
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
* Contact: clif@ow2.org
*/

package org.ow2.isac.plugin.socketinjector;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.util.HashMap;
import java.util.Map;
import org.ow2.clif.scenario.isac.exception.IsacRuntimeException;
import org.ow2.clif.scenario.isac.plugin.DataProvider;
import org.ow2.clif.scenario.isac.plugin.SampleAction;
import org.ow2.clif.scenario.isac.plugin.TestAction;
import org.ow2.clif.scenario.isac.plugin.SessionObjectAction;
import org.ow2.clif.scenario.isac.util.ParameterParser;
import org.ow2.clif.storage.api.ActionEvent;
import org.ow2.clif.supervisor.api.ClifException;
import org.ow2.clif.util.Network;
import org.ow2.clif.scenario.isac.plugin.ControlAction;

/**
 * ISAC plug-in providing a TCP socket injector
 * @author Bruno Dillenseger
 */
public class SessionObject implements SampleAction, TestAction, SessionObjectAction, DataProvider, ControlAction
{
	static final String PLUGIN_UNIT = "unit";
	static final int CONTROL_ACCEPT = 9;
	static final String CONTROL_ACCEPT_TIMEOUT = "timeout";
	static final String CONTROL_ACCEPT_LOCALPORT = "localport";
	static final String CONTROL_ACCEPT_LOCALIP = "localip";
	static final int CONTROL_SETTIMEOUT = 10;
	static final String CONTROL_SETTIMEOUT_TIMEOUT = "timeout";
	static final int TEST_ISOPEN = 7;
	static final String TEST_ISOPEN_OPTIONS = "options";
	static final int TEST_EOF = 11;
	static final String TEST_EOF_OPTIONS = "options";
	static final int TEST_TIMEDOUT = 12;
	static final String TEST_TIMEDOUT_OPTIONS = "options";
	static final int SAMPLE_READBYTES = 0;
	static final String SAMPLE_READBYTES_BUFFERNAME = "buffername";
	static final int SAMPLE_READUTFSTRING = 1;
	static final String SAMPLE_READUTFSTRING_BUFFERNAME = "buffername";
	static final int SAMPLE_WRITEBYTES = 2;
	static final String SAMPLE_WRITEBYTES_DATASTRING = "datastring";
	static final int SAMPLE_WRITEUTFSTRING = 3;
	static final String SAMPLE_WRITEUTFSTRING_DATASTRING = "datastring";
	static final int SAMPLE_CLOSE = 6;
	static final int SAMPLE_CONNECT = 8;
	static final String SAMPLE_CONNECT_TIMEOUT = "timeout";
	static final String SAMPLE_CONNECT_REMOTEPORT = "remoteport";
	static final String SAMPLE_CONNECT_REMOTEIP = "remoteip";
	static final String SAMPLE_CONNECT_LOCALPORT = "localport";
	static final String SAMPLE_CONNECT_LOCALIP = "localip";
	static private final int BUFFER_SIZE = 4096;

	private Socket sock = null;
	private InetAddress localhost;
	private int localport = -1;
	private InetSocketAddress remoteAddress;
	private Map<String,String> buffers = new HashMap<String,String>();
	private byte[] buffer = new byte[BUFFER_SIZE];
	private boolean eofReached = false;
	private boolean timedOut = false;
	/** if false, response times are measured in microseconds instead of milliseconds */
	private boolean unit_ms = true;


	/**
	 * Build a new SessionObject for this plugin
	 * @param params The table containing all the parameters
	 */
	public SessionObject(Map<String,String> params)
		throws ClifException
	{
		String unit = ParameterParser.getRadioGroup((String)params.get(PLUGIN_UNIT));
		unit_ms = (unit == null) || unit.equals("millisecond");
	}


	/**
	 * This constructor is used to clone the specified session object
	 *
	 * @param toClone
	 *            The session object to clone
	 */
	private SessionObject(SessionObject toClone)
	{
		this.unit_ms = toClone.unit_ms;
	}


	private void acceptSocket(Map<String,String> params)
	{
		// definition of local IP address
		String localIpStr = (String)params.get(CONTROL_ACCEPT_LOCALIP);
		if (localIpStr != null && localIpStr.length() > 0)
		{
			try
			{
				localhost = InetAddress.getByAddress(localIpStr, new byte[4]);
			}
			catch (Exception ex)
			{
				throw new IsacRuntimeException("SocketInjector can't accept socket because the specified local IP address is not valid: " + localIpStr, ex);
			}
		}
		else
		{
			localhost = Network.getInetAddress(null);
		}
		if (localhost == null)
		{
			throw new IsacRuntimeException("SocketInjector can't accept socket because no local network interface is configured");
		}
		// definition of local port
		String localPortStr = (String)params.get(CONTROL_ACCEPT_LOCALPORT);
		if (localPortStr != null && localPortStr.length() > 0)
		{
			try
			{
				localport = Integer.parseInt(localPortStr);
			}
			catch (Exception ex)
			{
				throw new IsacRuntimeException("SocketInjector can't accept socket because the specified local port is not valid: " + localPortStr, ex);
			}
		}
		else
		{
			localport = 0;
		}
		// time out definition
		String timeoutStr = (String)params.get(CONTROL_ACCEPT_TIMEOUT);
		int timeout = 0;
		if (timeoutStr != null && timeoutStr.length() > 0)
		{
			try
			{
				timeout = Integer.parseInt(timeoutStr);
			}
			catch (NumberFormatException ex)
			{
				throw new IsacRuntimeException("SocketInjector can't accept socket because the specified timeout is not valid: " + timeoutStr, ex);
			}
		}
		try
		{
			ServerSocket server = new ServerSocket(localport, 0, localhost);
			server.setSoTimeout(timeout);
			sock = server.accept();
		}
		catch (IOException ex)
		{
			throw new IsacRuntimeException("SocketInjector can't create a server socket: ", ex);
		}
	}


	private ActionEvent connectSocket(ActionEvent report, Map<String,String> params)
	{
		// local address setting
		String localIpStr = (String)params.get(SAMPLE_CONNECT_LOCALIP);
		if (localIpStr != null && localIpStr.length() > 0)
		{
			try
			{
				localhost = InetAddress.getByAddress(localIpStr, new byte[4]);
			}
			catch (Exception ex)
			{
				throw new IsacRuntimeException("SocketInjector can't connect socket because the specified local IP address is not valid: " + localIpStr, ex);
			}
		}
		else
		{
			localhost = Network.getInetAddress(null);
		}
		if (localhost == null)
		{
			throw new IsacRuntimeException("SocketInjector can't connect socket because no local network interface is configured");
		}
		String localPortStr = (String)params.get(SAMPLE_CONNECT_LOCALPORT);
		if (localPortStr != null && localPortStr.length() > 0)
		{
			try
			{
				localport = Integer.parseInt(localPortStr);
			}
			catch (Exception ex)
			{
				throw new IsacRuntimeException("SocketInjector can't connect socket because the specified local port is not valid: " + localPortStr, ex);
			}
		}
		else
		{
			localport = 0;
		}
		// target address setting
		String targetHost = (String)params.get(SAMPLE_CONNECT_REMOTEIP);
		if (targetHost == null || targetHost.length() == 0)
		{
			throw new IsacRuntimeException("SocketInjector can't connect socket because no remote host is specified");
		}
		String targetPort = (String)params.get(SAMPLE_CONNECT_REMOTEPORT);
		try
		{
			remoteAddress = new InetSocketAddress(targetHost, Integer.parseInt(targetPort));
		}
		catch (Exception ex)
		{
			throw new IsacRuntimeException("SocketInjector can't connect socket because target port is unspecified or invalid: " + targetPort, ex);
		}
		// get time out value
		String timeoutStr = (String)params.get(SAMPLE_CONNECT_TIMEOUT);
		if (timeoutStr == null || timeoutStr.length() == 0)
		{
			throw new IsacRuntimeException("SocketInjector can't connect socket because no time out is specified");
		}
		int timeout = 0;
		try
		{
			timeout = Integer.parseInt(timeoutStr);
		}
		catch (NumberFormatException ex)
		{
			throw new IsacRuntimeException("SocketInjector can't connect socket because the time out value (" + timeoutStr + ") is not a correct number.");
		}
		// socket creation and connection
		long start_ns = 0;
		report.setDate(System.currentTimeMillis());
		if (! unit_ms)
		{
			start_ns = System.nanoTime();
		}
		try
		{
			sock = new Socket();
			sock.bind(new InetSocketAddress(localhost, localport));
			sock.connect(remoteAddress, timeout);
			if (unit_ms)
			{
				report.duration = (int)(System.currentTimeMillis() - report.getDate());
			}
			else
			{
				report.duration = (int)((System.nanoTime() - start_ns) / 1000);
			}
			report.successful = true;
			report.result = localhost + " connected to " + remoteAddress;
			return report;
		}
		catch (Exception ex)
		{
			sock = null;
			report.successful = false;
			report.comment = "SocketInjector can't connect " + localhost + " to " + remoteAddress;
			report.result = ex.toString();
			return report;
		}
	}


	private void closeSocket()
	{
		if (sock != null)
		{
			try
			{
				sock.close();
				sock = null;
			}
			catch (Exception ex)
			{
				throw new RuntimeException("ISAC SocketInjector could not close socket", ex);
			}
		}
	}


	private ActionEvent closeSocket(ActionEvent report)
	{
		if (sock != null)
		{
			long start_ns = 0;
			if (! unit_ms)
			{
				start_ns = System.nanoTime();
			}
			report.setDate(System.currentTimeMillis());
			try
			{
				sock.close();
				if (unit_ms)
				{
					report.duration = (int)(System.currentTimeMillis() - report.getDate());
				}
				else
				{
					report.duration = (int)((System.nanoTime() - start_ns) / 1000);
				}
				sock = null;
				report.successful = true;
				report.result = localhost + " disconnected from " + remoteAddress;
				return report;
			}
			catch (Exception ex)
			{
				report.successful = false;
				report.comment = "ISAC SocketInjector can't disconnect " + localhost + " from " + remoteAddress;
				report.result = ex.toString();
				return report;
			} 
		}
		else
		{
			report.successful = false;
			report.comment = "ISAC SocketInjector can't close unopen socket connection";
			report.result = "ignored";
			return null;
		}
	}


	private ActionEvent readSocket(ActionEvent report, boolean asBytes, String bufferName)
	{
		eofReached = false;
		timedOut = false;
		try
		{
			int size = 0;
			String data ="";
			long start_ns = 0;
			if (! unit_ms)
			{
				start_ns = System.nanoTime();
			}
			report.setDate(System.currentTimeMillis());
			if (asBytes)
			{
				InputStream in = sock.getInputStream();
				size = in.read(buffer, 0, buffer.length);
				if (bufferName != null)
				{
					data = new String(buffer, 0, size);
				}
			}
			else
			{
				DataInputStream in = new DataInputStream(sock.getInputStream());
				data = in.readUTF();
				if (data == null)
				{
					eofReached = true;
					size = 0;
				}
				else
				{
					size = data.length();
				}
			}
			if (unit_ms)
			{
				report.duration = (int)(System.currentTimeMillis() - report.getDate());
			}
			else
			{
				report.duration = (int)((System.nanoTime() - start_ns) / 1000);
			}
			report.successful = true;
			report.comment = size + " bytes read";
			report.result = "OK";
			if (bufferName != null)
			{
				buffers.put(bufferName, data.toString());
			}
			return report;
		}
		catch (Exception ex)
		{
			report.successful = false;
			report.comment = ex.getMessage();
			report.result = ex.toString();
			if (ex instanceof SocketTimeoutException)
			{
				timedOut = true;
			}
			if (bufferName != null)
			{
				buffers.remove(bufferName);
			}
			return report;
		}
	}


	private ActionEvent writeSocket(ActionEvent report, boolean asBytes, String data)
	{
		try
		{
			long start_ns = 0;
			if (! unit_ms)
			{
				start_ns = System.nanoTime();
			}
			report.setDate(System.currentTimeMillis());
			DataOutputStream out = new DataOutputStream(sock.getOutputStream());
			if (asBytes)
			{
				out.writeBytes(data);
			}
			else
			{
				out.writeUTF(data);
			} 
			out.flush();
			if (unit_ms)
			{
				report.duration = (int)(System.currentTimeMillis() - report.getDate());
			}
			else
			{
				report.duration = (int)((System.nanoTime() - start_ns) / 1000);
			}
			report.successful = true;
			report.comment = data.length() + " chars written";
			report.result = "OK";
			return report;
		}
		catch (Exception ex)
		{
			report.successful = false;
			report.comment = ex.getMessage();
			report.result = ex.toString();
			return report;
		}
	}


	///////////////////////////
	// TestAction interface //
	///////////////////////////


	public boolean doTest(int number, Map<String,String> params)
		throws IsacRuntimeException
	{
		switch (number)
		{
			case TEST_TIMEDOUT:
				if (ParameterParser.getCheckBox((String)params.get(TEST_TIMEDOUT_OPTIONS)).contains("not"))
				{
					return ! timedOut;
				}
				else
				{
					return timedOut;
				}
			case TEST_EOF:
				if (ParameterParser.getCheckBox((String)params.get(TEST_EOF_OPTIONS)).contains("not"))
				{
					return ! eofReached;
				}
				else
				{
					return eofReached;
				}
			case TEST_ISOPEN:
				if (ParameterParser.getCheckBox((String)params.get(TEST_ISOPEN_OPTIONS)).contains("not"))
				{
					return sock == null;
				}
				else
				{
					return sock != null;
				}
			default:
				throw new Error("Fatal error in ISAC's SocketInjector plug-in: unknown test identifier " + number);
		}
	}


	////////////////////////////
	// SampleAction interface //
	////////////////////////////


	public ActionEvent doSample(int number, Map<String,String> params, ActionEvent report)
	{
		switch (number)
		{
			case SAMPLE_CONNECT:
				report.type = "TCP connect";
				return connectSocket(report, params);
			case SAMPLE_READBYTES:
			{
				report.type = "TCP read bytes";
				String bufferName = (String)params.get(SAMPLE_READBYTES_BUFFERNAME);
				if (bufferName != null && buffer.length == 0)
				{
					bufferName = null;
				}
				return readSocket(report, true, bufferName);
			}
			case SAMPLE_READUTFSTRING:
			{
				report.type = "TCP read UTF string";
				String bufferName = (String)params.get(SAMPLE_READUTFSTRING_BUFFERNAME);
				if (bufferName != null && buffer.length == 0)
				{
					bufferName = null;
				}
				return readSocket(report, false, bufferName);
			}
			case SAMPLE_WRITEBYTES:
				report.type = "TCP write bytes";
				return writeSocket(report, true, (String)params.get(SAMPLE_WRITEBYTES_DATASTRING));
			case SAMPLE_WRITEUTFSTRING:
				report.type = "TCP write UTF string";
				return writeSocket(report, false, (String)params.get(SAMPLE_WRITEUTFSTRING_DATASTRING));
			case SAMPLE_CLOSE:
				report.type = "TCP close";
				return closeSocket(report);
			default:
				throw new Error("Fatal error in ISAC's SocketInjector plug-in: unknown sample identifier " + number);
		}
	}
	
	////////////////////////////
	// DataProvider interface //
	////////////////////////////


	public String doGet(String bufferName)
	{
		return buffers.get(bufferName);
	}


	///////////////////////////////////
	// SessionObjectAction interface //
	///////////////////////////////////


	public Object createNewSessionObject()
	{
		return new SessionObject(this);
	}


	public void close()
	{
		closeSocket();
	}


	public void reset()
	{
		closeSocket();
		buffers.clear();
		eofReached = false;
		timedOut = false;
	}


	
	//////////////////////////////////
	// ControlAction implementation //
	//////////////////////////////////


	/**
	 * @see org.ow2.clif.scenario.isac.plugin.ControlAction#doControl()
	 */
	public void doControl(int number, Map<String,String> params)
	{
		switch (number)
		{
			case CONTROL_SETTIMEOUT:
				if (sock == null)
				{
					throw new IsacRuntimeException("SocketInjector can't set read timeout while the socket is not open.");
				}
				else
				{
					String timeoutStr = (String)params.get(CONTROL_ACCEPT_TIMEOUT);
					if (timeoutStr != null && timeoutStr.length() > 0)
					{
						try
						{
							sock.setSoTimeout(Integer.parseInt(timeoutStr));
						}
						catch (Exception ex)
						{
							throw new IsacRuntimeException("SocketInjector can't set this timeout value: " + timeoutStr, ex);
						}
					}
				}
				break;
			case CONTROL_ACCEPT:
				acceptSocket(params);
				break;
			default:
				throw new Error("Unable to find this control in ~SocketInjector~ ISAC plugin: " + number);
		}
	}
}
