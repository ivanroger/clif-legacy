/*
* CLIF is a Load Injection Framework
* Copyright (C) 2004,2012 France Telecom R&D
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
* CLIF 
*
* Contact: clif@ow2.org
*/
package org.ow2.clif.scenario.isac.egui.plugins.nodes;

import java.util.Vector;
import org.ow2.clif.scenario.isac.egui.plugins.PluginManager;

/**
 * This class is an implementation of some methods which will be used to build some 
 * nodes of type 'while' 
 * 
 * @author JC Meillaud
 * @author A Peyrard
 * @author Bruno Dillenseger
 */
public abstract class WhileNode {
     
    /**
     * Create a node description for the while controller
     * @param pluginManager the plugin manager
     * @param pluginName The name of the plugin used
     * @param testName The name of the test used
     * @return The node description for this controler
     */
    protected static NodeDescription createNodeDescription(
            PluginManager pluginManager, String pluginName, String testName) {
        return pluginManager.createNodeDescription(pluginName, Node.WHILE, testName) ;
    }
    
    /**
     * This method return the help of a while node
     * @return The help lines
     */
    public static Vector<String> getHelp() {
        Vector<String> help = new Vector<String>();
        help.add("While loop statement.");
        help.add("The attached condition is checked before executing the first instruction of the attached behavior block.");
        help.add("As long as the condition evaluates to true, the full behavior block is executed once more.");
        help.add("This loop stops when the condition evaluates to false.");
        return help ;
    }
}
