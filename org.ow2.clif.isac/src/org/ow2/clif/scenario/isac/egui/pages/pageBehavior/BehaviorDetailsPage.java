/*
* CLIF is a Load Injection Framework
* Copyright (C) 2005 France Telecom R&D
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
* CLIF $Name: not supported by cvs2svn $
*
* Contact: clif@ow2.org
*/

package org.ow2.clif.scenario.isac.egui.pages.pageBehavior;

import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.forms.AbstractFormPart;
import org.eclipse.ui.forms.IDetailsPage;
import org.eclipse.ui.forms.IFormPart;
import org.eclipse.ui.forms.IManagedForm;
import org.eclipse.ui.forms.events.ExpansionAdapter;
import org.eclipse.ui.forms.events.ExpansionEvent;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.forms.widgets.Section;
import org.ow2.clif.scenario.isac.egui.IsacEditor;
import org.ow2.clif.scenario.isac.egui.ScenarioManager;
import org.w3c.dom.Element;

/**
 * Details page from pattern Master/Details
 * Allow edition of parameters for the selected action
 * in the master part.
 * @author Joan Chaumont
 */
public class BehaviorDetailsPage extends AbstractFormPart implements IDetailsPage {

    private IManagedForm form;
    private ScenarioManager scenario;
    private IsacEditor editor;
    private Element lastNode;
    
    /**
     * Constructor
     * @param editor
     * @param scenario
     */
    public BehaviorDetailsPage (
            IsacEditor editor, ScenarioManager scenario) {
        super();
        this.scenario = scenario;
        this.editor = editor;
        this.lastNode = null;
    }
    
    public void createContents(Composite parent) {
        FormToolkit toolkit = form.getToolkit();
        parent.setLayout(new GridLayout());
        
        /* Create "Properties" section */
        Section section = toolkit.createSection(parent,
                Section.DESCRIPTION | Section.TWISTIE | Section.EXPANDED);
        section.setText("Parameters");
        section.setDescription("Set parameters for the selected primitive");
        toolkit.createCompositeSeparator(section);
        section.setLayoutData(new GridData(GridData.FILL_BOTH));
        section.addExpansionListener(new ExpansionAdapter() {
            public void expansionStateChanged(ExpansionEvent e) {
                form.getForm().reflow(true);
            }
        });
        /* Composite client for injectors and probes properties labels and textfields */
        Composite sectionClient = toolkit.createComposite(section);
        section.setClient(sectionClient);
        sectionClient.setLayout(new GridLayout());
        sectionClient.setLayoutData(new GridData(GridData.FILL_BOTH));
        scenario.getPluginGUIManager().setParentComposite(sectionClient);
        
        toolkit.paintBordersFor(section);
    }

    public void initialize(IManagedForm form) {
        this.form = form;
    }

    public void dispose() {}

    public boolean isDirty() {
        return false;
    }

    public void commit(boolean onSave) {
        scenario.getPluginGUIManager().commit();
    }

    public boolean setFormInput(Object input) {
        return false;
    }

    public void setFocus() {
        form.getForm().setFocus();
    }

    public boolean isStale() {
        return false;
    }

    public void refresh() {
        if(lastNode != null) {
            scenario.getPluginGUIManager().setEditor(editor);
            scenario.getPluginGUIManager().switchPanel(lastNode);
            super.refresh();
        }
    }

    public void selectionChanged(IFormPart part, ISelection selection) {
        commit(false);
        Element node = (Element)((StructuredSelection)selection).getFirstElement();
        if(!node.equals(lastNode)) {
            lastNode = node;
            scenario.getPluginGUIManager().setEditor(editor);
            scenario.getPluginGUIManager().switchPanel(node);
        }
    }
}
