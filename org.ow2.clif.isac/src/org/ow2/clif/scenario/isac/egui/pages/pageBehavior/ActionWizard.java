/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2005, 2009 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * CLIF $Name: not supported by cvs2svn $
 *
 * Contact: clif@ow2.org
 */

package org.ow2.clif.scenario.isac.egui.pages.pageBehavior;

import org.eclipse.jface.viewers.*;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.SashForm;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.List;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.ui.INewWizard;
import org.eclipse.ui.IWorkbench;
import org.ow2.clif.scenario.isac.egui.Icons;
import org.ow2.clif.scenario.isac.egui.model.ModelWriterXIS;
import org.ow2.clif.scenario.isac.egui.plugins.*;
import org.ow2.clif.scenario.isac.egui.plugins.nodes.Node;
import static org.ow2.clif.scenario.isac.egui.util.BehaviorUtil.insertElementInTree;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import java.util.ArrayList;
import java.util.Map;
import java.util.Vector;

/**
 * This class is used for adding an action to a
 * behavior. User can select an imported plug-in
 * to see the list of actions it proposed and then
 * select one action. Else he can select a controller :
 * if, while, nchoice...
 * @author Joan Chaumont
 * @author Florian Francheteau
 */
public class ActionWizard extends Wizard implements INewWizard {

    private Document doc;
    private Element behavior;
    private Element node;
    private Map<String, PluginDescription> plugins;

    private String actionId;
    private String actionType;
    private String actionName;
    private Vector<ParameterDescription> actionParams;

    private TableViewer actionsView;
    private Table types;

    private List help;
    private ActionWizardPage page;
    private ActionPlacement actionPlacement;


    /**
     * Constructor
     * @param doc
     * @param currentNode
     * @param behavior
     * @param plugins
     */
    public ActionWizard(Document doc, Element currentNode,
                        Element behavior, Map<String, PluginDescription> plugins)
    {
        super();
        this.doc = doc;
        this.behavior = behavior;
        this.node = currentNode;
        this.plugins = plugins;

        actionId = actionType = actionName = "";
        actionParams = null;
        actionPlacement = processDefaultPlacement();
        setWindowTitle("Actions");
    }

    /**
     * Constuctor for the action Wizard
     * @param doc             Xml document
     * @param currentNode     Node selected
     * @param behavior        Current behaviour
     * @param plugins         List of plugins
     * @param actionPlacement Placement of the actions
     */
    public ActionWizard(Document doc, Element currentNode,
                        Element behavior, Map<String, PluginDescription> plugins, ActionPlacement actionPlacement)
    {
        super();
        this.doc = doc;
        this.behavior = behavior;
        this.node = currentNode;
        this.plugins = plugins;

        actionId = actionType = actionName = "";
        actionParams = null;
        this.actionPlacement = actionPlacement;

        setWindowTitle("Actions");
    }


    private ActionPlacement processDefaultPlacement()
    {
        if (node == null)
        {
            return ActionPlacement.END;
        } else
        {
            return ActionPlacement.CHILD;
        }
    }


    class TableActionContentProvider implements IStructuredContentProvider, ILabelProvider {

        /* List all actions for a selected plug-in */
        public Object[] getElements(Object inputElement)
        {
            Map<String, PluginDescription> plugins = (Map<String, PluginDescription>) inputElement;

            java.util.List<ActionDescription> c = new ArrayList<ActionDescription>();
            PluginDescription plug = plugins.get(actionId);
            if (plug == null)
            {
                return new Object[0];
            }

            c.addAll(plug.getSamples().values());
            c.addAll(plug.getTimers().values());
            c.addAll(plug.getControls().values());

            return c.toArray();
        }

        public void dispose()
        {
        }

        public void inputChanged(Viewer viewer, Object oldInput, Object newInput)
        {
        }

        public Image getImage(Object element)
        {
            Image image = null;
            try
            {
                if (element instanceof SampleDescription)
                {
                    image = Icons.getImageRegistry().get(Node.SAMPLE);
                } else if (element instanceof TimerDescription)
                {
                    image = Icons.getImageRegistry().get(Node.TIMER);
                } else if (element instanceof ControlDescription)
                {
                    image = Icons.getImageRegistry().get(Node.CONTROL);
                }
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
            return image;
        }

        public String getText(Object element)
        {
            PluginDescription p = plugins.get(actionId);
            return p.getName() + "." + ((ActionDescription) element).getName();
        }

        public void addListener(ILabelProviderListener listener)
        {
        }

        public void removeListener(ILabelProviderListener listener)
        {
        }

        public boolean isLabelProperty(Object element, String property)
        {
            return false;
        }
    }

    class ActionWizardPage extends WizardPage
            implements ISelectionChangedListener, SelectionListener {

        private TableItem itemIf;
        private TableItem itemWhile;
        private TableItem itemNChoice;
        private TableItem itemPreemptive;

        protected ActionWizardPage()
        {
            super("paramWizard");
            setTitle("Add a new action for behavior " + behavior.getAttribute("id"));
        }

        public void createControl(Composite parent)
        {
            SashForm main = new SashForm(parent, SWT.VERTICAL);
            main.setLayout(new GridLayout());
            main.setLayoutData(new GridData(GridData.FILL_BOTH));

            SashForm lists = new SashForm(main, SWT.HORIZONTAL);
            lists.setLayout(new GridLayout());
            lists.setLayoutData(new GridData(GridData.FILL_BOTH));

            /* All plug-ins and controllers */
            types = new Table(lists, SWT.SINGLE | SWT.BORDER);
            for (String pluginId : plugins.keySet())
            {
                TableItem item = new TableItem(types, SWT.NONE);
                item.setText(pluginId);
                item.setImage(Icons.getImageRegistry().get(Node.PLUGINS));
            }
            types.addSelectionListener(this);

            itemIf = addItem(Node.IF);
            itemWhile = addItem(Node.WHILE);
            itemNChoice = addItem(Node.NCHOICE);
            itemPreemptive = addItem(Node.PREEMPTIVE);

            /* Table with all actions of selected plug-in */
            actionsView = new TableViewer(lists, SWT.SINGLE | SWT.BORDER);

            TableActionContentProvider provider = new TableActionContentProvider();
            actionsView.setContentProvider(provider);
            actionsView.setLabelProvider(provider);
            actionsView.addSelectionChangedListener(this);

            lists.setWeights(new int[]{50, 50});

            help = new List(main, SWT.READ_ONLY | SWT.BORDER
                    | SWT.H_SCROLL | SWT.V_SCROLL);
            help.setLayoutData(new GridData(GridData.FILL_BOTH));

            setControl(parent);
        }

        /* adding controllers to table types */
        private TableItem addItem(String node)
        {
            TableItem item = new TableItem(types, SWT.NONE);
            item.setText(node);
            item.setImage(Icons.getImageRegistry().get(node));

            return item;
        }

        /* Allow finish when an action is selected */
        public void widgetSelected(SelectionEvent e)
        {
            if (e.getSource() instanceof Table)
            {
                TableItem item = ((Table) e.getSource()).getSelection()[0];
                actionId = item.getText();

                setPageComplete(isController(item));

                actionsView.setInput(plugins);
                help.removeAll();
            }
        }

        public void widgetDefaultSelected(SelectionEvent e)
        {
        }

        /* Allow finish when an action is selected and display help */
        public void selectionChanged(SelectionChangedEvent e)
        {
            if (e.getSource() instanceof TableViewer)
            {
                IStructuredSelection s =
                        (IStructuredSelection) ((TableViewer) e.getSource()).getSelection();
                ActionDescription desc = ((ActionDescription) s.getFirstElement());
                if (desc == null)
                {
                    actionName = "";
                    actionParams = null;
                    return;
                }

                actionName = desc.getName();
                actionParams = desc.getParams();
                actionType = getActionType(desc);
                setPageComplete(true);
                Vector<String> helpText = desc.getHelp();
                if (helpText != null)
                {
                    help.setItems(helpText.toArray(new String[0]));
                }
            }
        }

        private String getActionType(ActionDescription desc)
        {
            if (desc instanceof ControlDescription)
            {
                return Node.CONTROL;
            } else if (desc instanceof TimerDescription)
            {
                return Node.TIMER;
            }
            return Node.SAMPLE;
        }

        private boolean isController(TableItem item)
        {
            return item.equals(itemIf) || item.equals(itemWhile)
                    || item.equals(itemNChoice) || item.equals(itemPreemptive);
        }
    }

    public void addPages()
    {
        page = new ActionWizardPage();
        addPage(page);
    }

    public boolean performFinish()
    {
        Element newNode = createNewNode();
        insertElementInTree(newNode, node, behavior, actionPlacement);
        ModelWriterXIS.addParams(doc, newNode, actionParams);
        return true;
    }

    /**
     * Create a new node depending of values of actionName
     * and actionId (values of selected action)
     */
    private Element createNewNode()
    {
        Element newElement;
        /* Controllers case */
        if (actionName.equals(""))
        {
            newElement = doc.createElement(actionId);
            if (!actionId.equals(Node.NCHOICE))
            {
                Element condition = doc.createElement(Node.CONDITION);
                condition.setAttribute("use", "");
                condition.setAttribute("name", "");

                newElement.appendChild(condition);
                if (actionId.equals(Node.IF))
                {
                    newElement.appendChild(doc.createElement(Node.THEN));
                }
            }
        } else
        {
            newElement = doc.createElement(actionType);
            newElement.setAttribute("use", actionId);
            newElement.setAttribute("name", actionName);
        }
        return newElement;
    }

    public void init(IWorkbench workbench, IStructuredSelection selection)
    {
    }

}