/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2004, 2005, 2006, 2010 France Telecom
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * CLIF
 *
 * Contact: clif@ow2.org
 */
package org.ow2.clif.scenario.isac.egui.plugins;
import java.io.File;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.Vector;
import org.ow2.clif.supervisor.api.ClifException;
import org.ow2.clif.scenario.isac.egui.FileName;
import org.ow2.clif.scenario.isac.egui.plugins.gui.PluginGUIManager;
import org.ow2.clif.scenario.isac.egui.plugins.nodes.NodeDescription;
import org.ow2.clif.util.ExecutionContext;

/**
 * Manage the list of available ISAC plug-ins
 *
 * @author JC Meillaud
 * @author A Peyrard
 * @author Joan Chaumont
 * @author Bruno Dillenseger
 */
public class PluginManager {

    /* The hash table which will store the plug-ins descriptions */
    private Map<String,PluginDescription> plugins;

    /** Constructor */
    public PluginManager() {
        this.plugins = new TreeMap<String,PluginDescription>();
    }

    /**
     * This method build the table which store the plug-ins descriptions
     * @param pluginGUIManager 
     */
    public void initialisePluginsTable(PluginGUIManager pluginGUIManager)
    {
    	/* search into the directory where the ISAC plug-ins are defined */    	
    	StringBuffer path = new StringBuffer(ExecutionContext.getBaseDir());
    	path.append(File.separator);
    	path.append(FileName.PLUGINS_DIR );
    	path.append(File.separator);
    	
    	/* search into the directory where the ISAC plug-ins are defined */
    	List<File> files = SearchPluginFile.searchPlugins(path.toString(),
    			FileName.PLUGIN_PROPERTIES_FILE);		
		
		/* for each location analyze the plug-ins informations */
    	List<PluginDescription> lsPluginDescription;
		for (File file : files) {
			try {
				lsPluginDescription=PluginDescription.loadPluginDescription(pluginGUIManager, file);
				for (PluginDescription desc : lsPluginDescription)	{
					/* add the plug-in description which has be analyzed */
					this.plugins.put(desc.getName(), desc);
				}
			}
			catch (ClifException ex)
			{
				ex.printStackTrace(System.err);
			}
		}
	}

    /**
	 * Create a new vector with an entry for each actions, parameters values
	 * will be set to null
	 * 
	 * @param type
	 *            The type of the action searched
	 * @return The nodes descriptions
	 */
    public Vector<NodeDescription> createNodesDescriptions(String type) {
        Vector<NodeDescription> result = new Vector<NodeDescription>();
        Iterator<PluginDescription> iter = this.plugins.values().iterator();
        while (iter.hasNext()) {
            PluginDescription temp = iter.next();
            result.addAll(temp.createNodesDescriptions(type));
        }
        return result;
    }

    /**
     * Create a new node description for a selected action of a selected plug-in
     * @param plugin The name of the plug-in
     * @param type The type of the action
     * @param name The name of the action
     * @return The node description for this action
     */
    public NodeDescription createNodeDescription(
            String plugin, String type, String name) {
        if (this.plugins.containsKey(plugin)) {
            PluginDescription temp = this.plugins.get(plugin);
            return temp.createNodeDescription(type, name) ;
        }
        return null;
    }

    /**
     * Create a new vector with an entry for each actions, parameters values
     * will be set to null But only action of the plug-in define in the vector
     * which is given
     * @param pluginsName The names of the plug-ins
     * @param type The type of the action searched
     * @return The nodes descriptions
     */
    public Vector<NodeDescription> createNodesDescriptionsByPlugins(Vector<String> pluginsName,
            String type) {
        Vector<NodeDescription> result = new Vector<NodeDescription>();
        Iterator<PluginDescription> iter = this.plugins.values().iterator();
        while (iter.hasNext()) {
            PluginDescription temp = iter.next();
            if (pluginsName.contains(temp.getName())) {
                result.addAll(temp.createNodesDescriptions(type));
            }
        }
        return result;
    }

    /**
     * This method return the help lines of the action of a selected plugin
     * @param plugin The plug-in name
     * @param type The type of the action
     * @param action The name of the action
     * @return The help lines of the action
     */
    public Vector<String> getPluginActionHelp(String plugin, String type, String action) {
        if (this.plugins.containsKey(plugin)) {
            PluginDescription temp = plugins.get(plugin);
            Vector<String> helpLines = temp.getActionHelp(type, action);
            return helpLines;
        }
        return null;
    }

    /**
     * This method return the GUI key of a specified action of a specified plug-in
     * @param plugin The plug-in name
     * @param type The action type
     * @param action The action name
     * @return The GUI key, or null if the specified plug-in name/type/action could not be found
     */
    public String getPluginActionGUIKey(String plugin, String type, String action) {
        if (this.plugins.containsKey(plugin)) {
            PluginDescription temp = this.plugins.get(plugin);
            return temp.getActionGUIKey(type, action);
        }
        return null ;
    }

    /**
     * @return String[] plug-ins names
     */
    public String[] getPluginsName() {
        return (String[]) plugins.keySet().toArray(new String[0]);
    }
    
    /**
     * Return the plug-in description for a given name
     * @param pluginName the plug-in name
     * @return PluginDescription
     */
    public PluginDescription getDescription(String pluginName) {
        return plugins.get(pluginName);
    }
}