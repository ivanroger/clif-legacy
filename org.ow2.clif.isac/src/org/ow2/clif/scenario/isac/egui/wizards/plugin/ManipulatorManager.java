/*
 * CLIF is a Load Injection Framework Copyright (C) 2006, 2007 France Telecom R&D
 * 
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option) any
 * later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 * CLIF $Name$
 * 
 * Contact: clif@ow2.org
 */
package org.ow2.clif.scenario.isac.egui.wizards.plugin;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Properties;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.Path;
import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.IPackageFragment;
import org.eclipse.jdt.core.IPackageFragmentRoot;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.swt.widgets.Shell;
import org.jdom.Element;
import org.ow2.clif.scenario.isac.egui.FileName;


/**
 * Singleton class which manages model in memory (Java code + XML).
 * This class loads ISAC plugin files in memory, manages manipulation and saves them if user asked.
 * @author Fabrice Rivart
 * @author Bruno Dillenseger
 */
public class ManipulatorManager {
	
	private static ManipulatorManager ref;

	private XMLManipulator xmlManipulator;
	private CodeManipulator codeManipulator;
	
	private IProject project;
	private IJavaProject jProject;
	private Properties oldProperties;
	private Properties newProperties;
	
	// true if singleton has been initialize
	private boolean init = false;
	
	// true if a save has been occured
	private boolean save = false;
	
	// true if singleton has been destroyed
	private boolean destroy = false;
	
	/**
	 * Private constructor.
	 */
	private ManipulatorManager() {
		xmlManipulator = new XMLManipulator();
		codeManipulator = new CodeManipulator();
		oldProperties = new Properties();
		newProperties = new Properties();
	}

	/**
	 * Returns a ManipulatorManager singleton instance.
	 * @return ManipulatorManager singleton instance.
	 */
	public static synchronized ManipulatorManager getInstance() {
        if (ref == null) {
        	ref = new ManipulatorManager();
        }
        return ref;
    }
	
	/**
	 * This method loads ISAC plugin files in memory.
	 * @param arg0 IProject representing the ISAC plugin project.
	 * @param s Shell representing the current shell
	 */
	public void init(IProject arg0, Shell s) throws Exception {
		
		if (!init) {
			project = arg0;
			InputStream is;
			
			// load properties file in memory
			IFile propertiesIFile = project.getFile(FileName.PLUGIN_PROPERTIES_FILE);
			if (propertiesIFile != null) {
				is = propertiesIFile.getContents();
				oldProperties.load(is);
				is = propertiesIFile.getContents();
				newProperties.load(is);
				is.close();
			}
			else {
				throw new Exception("File : " + FileName.PLUGIN_PROPERTIES_FILE + " does not exist.");
			}
			
			// load gui xml file in memory
			String guiFileName = oldProperties.getProperty("plugin.guiFile");
			IFile guiIFile = project.getFile(guiFileName);
			if (guiIFile != null) {
				is = guiIFile.getContents();
				xmlManipulator.loadGui(is);
				is.close();
			}
			else {
				throw new Exception("File : " + guiFileName + " does not exist.");
			}
			
			// load plugin xml file in memory
			String pluginFileName = oldProperties.getProperty("plugin.xmlFile");
			IFile pluginIFile = project.getFile(pluginFileName);
			if (pluginIFile != null) {
				is = pluginIFile.getContents();
				xmlManipulator.loadPlugin(is);
				is.close();
			}
			else {
				throw new Exception("File : " + pluginFileName + " does not exist.");
			}

			// load build xml file in memory
			IFile buildIFile = project.getFile("build.xml");
			if (buildIFile != null) {
				is = buildIFile.getContents();
				xmlManipulator.loadBuild(is);
				is.close();
			}
			
			// load class and package name
			String className = xmlManipulator.getClassName();
			String packageName = xmlManipulator.getPackageName();
			oldProperties.setProperty("class", className);
			newProperties.setProperty("class", className);
			oldProperties.setProperty("package", packageName);
			newProperties.setProperty("package", packageName);
			
			//load java file in memory
			jProject = JavaCore.create(project);
			ICompilationUnit cu = null;
			boolean fileExist = false;
			
			IPackageFragmentRoot [] pfRoots = jProject.getPackageFragmentRoots();
			for (int i = 0; i < pfRoots.length; i++) {
				if (pfRoots[i].getKind() == IPackageFragmentRoot.K_SOURCE && 
					pfRoots[i].getPackageFragment(packageName).exists()) {
					cu = pfRoots[i].getPackageFragment(packageName).getCompilationUnit(className + ".java");
					if (cu.exists()) {
						newProperties.setProperty("source", pfRoots[i].getPath().toString());
						oldProperties.setProperty("source", pfRoots[i].getPath().toString());
						fileExist = true;
					}
				}
			}
			
			if (fileExist) {
				codeManipulator.load(cu, oldProperties.getProperty("plugin.name"), s);
				if (codeManipulator.isDataProvider()) {
					oldProperties.setProperty("provider", "yes");
					newProperties.setProperty("provider", "yes");
				}
				else {
					oldProperties.setProperty("provider", "no");
					newProperties.setProperty("provider", "no");
				}
			}
			else {
				throw new Exception("File : " + className + ".java does not exist.");
			}
			
			// synchronize files
			synchronize();
			
			init = true;
		}
	}
	
	/**
	 * This method adds an action into model.
	 * @param action String representing the action to add (sample, timer, test, control)
	 * @param name String representing action name
	 * @throws Exception if there is an error
	 */
	public void addAction(String action, String name) throws Exception {
		
		int number = xmlManipulator.getMaxNumber();
		xmlManipulator.addAction(action, name, number + 1);
		codeManipulator.addAction(action, name, number + 1);
	}
	
	/**
	 * This method delete an action into model.
	 * @param action String representing the action to add (sample, timer, test, control)
	 * @param name String representing action name
	 * @throws Exception if there is an error
	 */
	public void deleteAction(String action, String name) throws Exception {
		
		String [] params = xmlManipulator.getStringParams(action, name);
		savePluginFiles();
		codeManipulator.removeAction(action, name, params);
		xmlManipulator.deleteAction(action, name);
		savePluginFiles();
	}
	
	/**
	 * This method updates an action into model.
	 * @param action String representing the action to add (sample, timer, test, control)
	 * @param oldName String representing old action name
	 * @param newName String representing new action name
	 * @throws Exception if there is an error
	 */
	public void updateAction(String action, String oldName, String newName) throws Exception {
		
		String [] params = xmlManipulator.getStringParams(action, oldName);
		savePluginFiles();
		codeManipulator.updateAction(action, oldName, newName, params);
		xmlManipulator.updateAction(action, oldName, newName);
		savePluginFiles();
	}
	
	/**
	 * This method move up action into model.
	 * @param action String representing the action to add (sample, timer, test, control)
	 * @param name String representing action name
	 * @throws Exception if there is an error
	 */
	public void moveUpAction(String action, String name) throws Exception {
		xmlManipulator.moveUpAction(action, name);
	}
	
	/**
	 * This method move down action element into model.
	 * @param action String representing the action to add (sample, timer, test, control)
	 * @param name String representing action name
	 * @throws Exception if there is an error
	 */
	public void moveDownAction(String action, String name) throws Exception {
		xmlManipulator.moveDownAction(action, name);
	}
	
	/**
	 * Returns a ArrayList representing string actions.
	 * @param action String representing the action to add (sample, timer, test, control)
	 * @return ArrayList representing string actions
	 * @throws Exception if there is an error
	 */
	public ArrayList<String> getActions(String action) throws Exception {
		return xmlManipulator.getActions(action);
	}
	
	/**
	 * This method adds group element into model.
	 * @param action String representing the action to add (sample, timer, test, control)
	 * @param actionName String representing action name
	 * @param parentPath String representing XPath parent expression 
	 * @param groupName String representing group name
	 * @throws Exception if there is an error
	 */
	public void addGroup(String action, 
						String actionName, 
						String parentPath,
						String groupName) throws Exception {
		xmlManipulator.addGroup(action, actionName, parentPath, groupName);
	}
	
	/**
	 * This method adds parameter element into model.
	 * @param action String representing the action to add (sample, timer, test, control)
	 * @param actionName String representing action name
	 * @param guiPath String representing XPath parent expression in gui file
	 * @param pluginPath String representing XPath parent expression in plugin file
	 * @param paramName String representing parameter name
	 * @throws Exception if there is an error
	 */
	public void addParameter(String action, 
						String actionName, 
						String guiPath,
						String pluginPath,
						String paramName) throws Exception {
		xmlManipulator.addParameter(action, actionName, guiPath, pluginPath, paramName);
		codeManipulator.addActionParameter(action, actionName, paramName);
	}
	
	/**
	 * This method delete group or parameter element into model.
	 * @param action String representing the action to add (sample, timer, test, control)
	 * @param actionName String representing action name
	 * @param guiParentPath String representing XPath parent expression in gui file
	 * @param pluginParentPath String representing XPath parent expression in plugin file
	 * @param guiElementPath String representing XPath element expression in gui file
	 * @param pluginElementPath String representing XPath element expression in plugin file
	 * @param parameterName String representing parameter name
	 * @param type String representing element type (group or param)
	 * @throws Exception if there is an error
	 */
	public void deleteElement(String action, 
						String actionName, 
						String guiParentPath,
						String pluginParentPath,
						String guiElementPath,
						String pluginElementPath,
						String parameterName,
						String type) throws Exception {
		
		savePluginFiles();
		xmlManipulator.deleteElement(action, actionName, guiParentPath,
				pluginParentPath, guiElementPath, pluginElementPath);
		
		if (type.equals("param")) {
			codeManipulator.removeActionParameter(action, actionName, parameterName);
			savePluginFiles();
		}
	}
	
	/**
	 * This method updates group or parameter element name into model.
	 * @param action String representing the action to add (sample, timer, test, control)
	 * @param actionName String representing action name
	 * @param guiElementPath String representing XPath element expression in gui file
	 * @param pluginElementPath String representing XPath element expression in plugin file
	 * @param oldName String representing the old element name
	 * @param newName String representing the new element name
	 * @param type String representing element type (group or param)
	 * @throws Exception if there is an error
	 */
	public void updateElementName(String action, 
						String actionName, 
						String guiElementPath,
						String pluginElementPath,
						String oldName,
						String newName,
						String type) throws Exception {
		
		savePluginFiles();
		xmlManipulator.updateElementName(action, actionName, guiElementPath, pluginElementPath, newName);
		
		if (type.equals("param")) {
			codeManipulator.updateActionParameter(action, actionName, oldName, newName);
			savePluginFiles();
		}
	}
	
	/**
	 * This method moves up parameter in XML file.
	 * @param action String representing the action to add (sample, timer, test, control)
	 * @param actionName String representing action name
	 * @param guiParentPath String representing XPath parent element expression in gui file
	 * @param guiElementPath String representing XPath element expression in gui file
	 * @throws Exception if there is an error
	 */
	public void moveUpParameter(String action, 
						String actionName, 
						String guiParentPath,
						String guiElementPath) throws Exception {
		
		xmlManipulator.moveUpParameter(action, actionName, guiParentPath, guiElementPath);
	}
	
	/**
	 * This method moves down parameter in XML file.
	 * @param action String representing the action to add (sample, timer, test, control)
	 * @param actionName String representing action name
	 * @param guiParentPath String representing XPath parent element expression in gui file
	 * @param guiElementPath String representing XPath element expression in gui file
	 * @throws Exception if there is an error
	 */
	public void moveDownParameter(String action, 
						String actionName, 
						String guiParentPath,
						String guiElementPath) throws Exception {
		
		xmlManipulator.moveDownParameter(action, actionName, guiParentPath, guiElementPath);
	}
	
	/**
	 * This method updates parameter label into model.
	 * @param action String representing the action to add (sample, timer, test, control)
	 * @param actionName String representing action name
	 * @param guiElementPath String representing XPath element expression in gui file
	 * @param label String representing the new parameter label
	 * @throws Exception if there is an error
	 */
	public void setParameterLabel(String action, 
						String actionName, 
						String guiElementPath,
						String label) throws Exception {
		
		xmlManipulator.setParameterLabel(action, actionName, guiElementPath, label);
	}
	
	/**
	 * This method returns parameter label from model.
	 * @param action String representing the action to add (sample, timer, test, control)
	 * @param actionName String representing action name
	 * @param guiElementPath String representing XPath element expression in gui file
	 * @throws Exception if there is an error
	 */
	public String getParameterLabel(String action, 
						String actionName, 
						String guiElementPath) throws Exception {
		
		return xmlManipulator.getParameterLabel(action, actionName, guiElementPath);
	}
	
	/**
	 * This method updates parameter represention in gui file.
	 * @param action String representing the action to add (sample, timer, test, control)
	 * @param actionName String representing action name
	 * @param guiElementPath String representing XPath element expression in gui file
	 * @param type String representing the new parameter representation
	 * @throws Exception if there is an error
	 */
	public void setParameterType(String action, 
						String actionName, 
						String guiElementPath,
						String type) throws Exception {
		
		xmlManipulator.setParameterType(action, actionName, guiElementPath, type);
	}
	
	/**
	 * This method returns parameter represention from gui file.
	 * @param action String representing the action to add (sample, timer, test, control)
	 * @param actionName String representing action name
	 * @param guiElementPath String representing XPath element expression in gui file
	 * @return parameter represention from gui file
	 * @throws Exception if there is an error
	 */
	public String getParameterType(String action, 
						String actionName, 
						String guiElementPath) throws Exception {
		
		return xmlManipulator.getParameterType(action, actionName, guiElementPath);
	}
	
	/**
	 * This method updates parameter field text into model.
	 * @param action String representing the action to add (sample, timer, test, control)
	 * @param actionName String representing action name
	 * @param guiElementPath String representing XPath element expression in gui file
	 * @param text String representing the new parameter field text
	 * @throws Exception if there is an error
	 */
	public void setParameterFieldText(String action, 
						String actionName, 
						String guiElementPath,
						String text) throws Exception {
		
		xmlManipulator.setParameterFieldText(action, actionName, guiElementPath, text);
	}
	
	/**
	 * This method returns parameter field text from model.
	 * @param action String representing the action to add (sample, timer, test, control)
	 * @param actionName String representing action name
	 * @param guiElementPath String representing XPath element expression in gui file
	 * @return parameter represention from gui file
	 * @throws Exception if there is an error
	 */
	public String getParameterFieldText(String action, 
						String actionName, 
						String guiElementPath) throws Exception {
		
		return xmlManipulator.getParameterFieldText(action, actionName, guiElementPath);
	}
	
	/**
	 * This method updates parameter field size into model.
	 * @param action String representing the action to add (sample, timer, test, control)
	 * @param actionName String representing action name
	 * @param guiElementPath String representing XPath element expression in gui file
	 * @throws Exception if there is an error
	 */
	public void setParameterFieldSize(String action, 
						String actionName, 
						String guiElementPath,
						String size) throws Exception {
	
		xmlManipulator.setParameterFieldSize(action, actionName, guiElementPath, size);
	}
	
	/**
	 * This method returns parameter field size from model.
	 * @param action String representing the action to add (sample, timer, test, control)
	 * @param actionName String representing action name
	 * @param guiElementPath String representing XPath element expression in gui file
	 * @return parameter represention from gui file
	 * @throws Exception if there is an error
	 */
	public String getParameterFieldSize(String action, 
						String actionName, 
						String guiElementPath) throws Exception {
		
		return xmlManipulator.getParameterFieldSize(action, actionName, guiElementPath);
	}
	
	/**
	 * This method adds parameter element into model.
	 * @param action String representing the action to add (sample, timer, test, control)
	 * @param actionName String representing action name
	 * @param guiPath String representing XPath parent expression in gui file
	 * @param type String representing representation of element
	 * @param elementName String representing parameter element name
	 * @throws Exception if there is an error
	 */
	public void addParameterElement(String action, 
						String actionName, 
						String guiPath,
						String type,
						String elementName) throws Exception {
		xmlManipulator.addParameterElement(action, actionName, guiPath, type, elementName);
	}
	
	/**
	 * This method deletes parameter element into model.
	 * @param action String representing the action to add (sample, timer, test, control)
	 * @param actionName String representing action name
	 * @param guiPath String representing XPath parent expression in gui file
	 * @param type String representing representation of element
	 * @param elementName String representing parameter element name
	 * @throws Exception if there is an error
	 */
	public void deleteParameterElement(String action, 
						String actionName, 
						String guiPath,
						String type,
						String elementName) throws Exception {
		
		xmlManipulator.deleteParameterElement(action, actionName, guiPath, type, elementName);		
	}
	
	/**
	 * This method updates parameter element into model.
	 * @param action String representing the action to add (sample, timer, test, control)
	 * @param actionName String representing action name
	 * @param guiPath String representing XPath parent expression in gui file
	 * @param type String representing representation of element
	 * @param oldElementName String representing old parameter element name
	 * @param newElementName String representing new parameter element name
	 * @throws Exception if there is an error
	 */
	public void updateParameterElement(String action, 
						String actionName, 
						String guiPath,
						String type,
						String oldElementName,
						String newElementName) throws Exception {
		xmlManipulator.updateParameterElement(action, actionName, guiPath, 
												type, oldElementName, newElementName);
	}
	
	/**
	 * This method moves up parameter element into model.
	 * @param action String representing the action to add (sample, timer, test, control)
	 * @param actionName String representing action name
	 * @param guiPath String representing XPath parent expression in gui file
	 * @param type String representing representation of element
	 * @param elementName String representing parameter element name
	 * @throws Exception if there is an error
	 */
	public void moveUpParameterElement(String action, 
						String actionName, 
						String guiPath,
						String type,
						String elementName) throws Exception {
		
		xmlManipulator.moveUpParameterElement(action, actionName, guiPath, type, elementName);
	}
	
	/**
	 * This method moves down parameter element in gui XML file.
	 * @param action String representing the action to add (sample, timer, test, control)
	 * @param actionName String representing action name
	 * @param guiPath String representing XPath parent expression in gui file
	 * @param type String representing representation of element
	 * @param elementName String representing parameter element name
	 * @throws Exception if there is an error
	 */
	public void moveDownParameterElement(String action, 
						String actionName, 
						String guiPath,
						String type,
						String elementName) throws Exception {
		xmlManipulator.moveDownParameterElement(action, actionName, guiPath, type, elementName);
	}
	
	/**
	 * Returns parameter element list in gui XML file.
	 * @param action String representing the action to add (sample, timer, test, control)
	 * @param actionName String representing action name
	 * @param guiPath String representing XPath parent expression in gui file
	 * @param type String representing representation of element
	 * @return ArrayList of all elements
	 * @throws Exception if there is an error
	 */
	public ArrayList<String> getParameterElements(String action, 
						String actionName, 
						String guiPath,
						String type) throws Exception {
		return xmlManipulator.getParameterElements(action, actionName, guiPath, type);
	}
	
	/**
	 * This method sets default parameter element value to true or false into model.
	 * @param action String representing the action to add (sample, timer, test, control)
	 * @param actionName String representing action name
	 * @param guiPath String representing XPath parent expression in gui file
	 * @param type String representing representation of element
	 * @param elementName String representing parameter element name
	 * @param value String representing the value to set
	 * @throws Exception if there is an error
	 */
	public void setParameterElementDefault(String action, 
						String actionName, 
						String guiPath,
						String type,
						String elementName,
						String value) throws Exception {
		xmlManipulator.setParameterElementDefault(action, actionName, guiPath, type, elementName, value);
	}
	
	/**
	 * Returns default parameter element value from model.
	 * @param action String representing the action to add (sample, timer, test, control)
	 * @param actionName String representing action name
	 * @param guiPath String representing XPath parent expression in gui file
	 * @param type String representing representation of element
	 * @param elementName String representing parameter element name
	 * @return String representing default parameter element value
	 * @throws Exception if there is an error
	 */
	public String getParameterElementDefault(String action, 
						String actionName, 
						String guiPath,
						String type,
						String elementName) throws Exception {
		return xmlManipulator.getParameterElementDefault(action, actionName, guiPath, type, elementName);
	}
	
	/**
	 * Returns a Element representing action params element.
	 * @param action String representing the action to add (sample, timer, test, control)
	 * @param name String representing action name
	 * @return Element representing action params element
	 * @throws Exception if there is an error
	 */
	public Element getParams(String action, String name) throws Exception {
		return xmlManipulator.getParams(action, name);
	}
	
	/**
	 * Returns a String representing action help.
	 * 
	 * @param action String representing the action to add (sample, timer, test,
	 *            control)
	 * @param name String representing action name
	 * @return String representing action help
	 * @throws Exception if there is an error
	 */
	public String getHelp(String action, String name) throws Exception {
		return xmlManipulator.getHelp(action, name);
	}
	
	/**
	 * Set action help into model
	 * @param action String representing the action to add (sample, timer, test, control)
	 * @param name String representing action name
	 * @param help String representing action help
	 * @throws Exception if there is an error
	 */
	public void setHelp(String action, String name, String help) throws Exception {
		xmlManipulator.setHelp(action, name, help);
	}
	
	/**
	 * Returns true if element exists
	 * @param action String representing the action (sample, timer, test, control)
	 * @param name String representing action name
	 * @param elementPath String representing XPath expression of element
	 * @return true if element exists
	 * @throws Exception if there is an error.
	 */
	public boolean isParameterExist(String action, String name, String elementPath) throws Exception {
		return xmlManipulator.isParameterExist(action, name, elementPath);
	}
	
	/**
	 * Sets new plugin name.
	 * @param name String representing plugin name.
	 */
	public void setPluginName(String name) {
		newProperties.setProperty("plugin.name", name);
	}
	
	/**
	 * Returns plugin name.
	 * @return String representing plugin name.
	 */
	public String getPluginName() {
		return oldProperties.getProperty("plugin.name");
	}
	
	/**
	 * Sets new source path.
	 * @param path String representing source path.
	 */
	public void setSource(String path) {
		newProperties.setProperty("source", path);
	}
	
	/**
	 * Returns source path.
	 * @return String representing source path.
	 */
	public String getSource() {
		return oldProperties.getProperty("source");
	}
	
	/**
	 * Sets new package name.
	 * @param name String representing package name.
	 */
	public void setPackageName(String name) {
		newProperties.setProperty("package", name);
	}
	
	/**
	 * Returns package name.
	 * @return String representing package name.
	 */
	public String getPackageName() {
		return oldProperties.getProperty("package");
	}
	
	/**
	 * Sets new class name.
	 * @param name String representing class name.
	 */
	public void setClassName(String name) {
		newProperties.setProperty("class", name);
	}
	
	/**
	 * Returns class name.
	 * @return String representing class name.
	 */
	public String getClassName() {
		return oldProperties.getProperty("class");
	}
	
	/**
	 * Sets new gui file name.
	 * @param name String representing gui file name.
	 */
	public void setGuiFileName(String name) {
		newProperties.setProperty("plugin.guiFile", name);
	}
	
	/**
	 * Returns gui file name.
	 * @return String representing gui file name.
	 */
	public String getGuiFileName() {
		return oldProperties.getProperty("plugin.guiFile");
	}
	
	/**
	 * Sets new plugin file name.
	 * @param name String representing plugin file name.
	 */
	public void setPluginFileName(String name) {
		newProperties.setProperty("plugin.xmlFile", name);
	}
	
	/**
	 * Returns plugin file name.
	 * @return String representing plugin file name.
	 */
	public String getPluginFileName() {
		return oldProperties.getProperty("plugin.xmlFile");
	}
	
	/**
	 * Sets new data provider state ("yes" or "no").
	 * @param provider String representing data provider state.
	 */
	public void setDataProvider(String provider) {
		newProperties.setProperty("provider", provider);
	}
	
	/**
	 * Returns data provider state ("yes" or "no").
	 * @return String representing data provider state ("yes" or "no").
	 */
	public String getDataProvider() {
		return oldProperties.getProperty("provider");
	}
		
	/**
	 * this method saves all modifications on model into xml and Java files.
	 * @throws Exception if there is an error.
	 */
	public void save() throws Exception {
		
		if (!save) {
			
			// save properties file
			savePropertiesFile();
			
			// update plugin name into model
			String newPluginName = newProperties.getProperty("plugin.name");
			String oldPluginName = oldProperties.getProperty("plugin.name");
			
			if (!newPluginName.equals(oldPluginName)) {
				xmlManipulator.setPluginName(newPluginName);
				codeManipulator.updatePluginName(newPluginName);
			}
			
			// update package and class name into model
			String oldJavaFileName = oldProperties.getProperty("class");
			String newJavaFileName = newProperties.getProperty("class");
			String oldPackageName = oldProperties.getProperty("package");
			String newPackageName = newProperties.getProperty("package");
			String newSourceName = newProperties.getProperty("source");
			
			xmlManipulator.setPluginClassName(newPackageName + "." + newJavaFileName);
			xmlManipulator.setGuiObjectName(newJavaFileName);
			xmlManipulator.setSourceDir(newSourceName.substring(project.getName().length() + 1));

			// save XML and properties files
			savePluginFiles();
			
			// update data provider interface
			String newProvider = newProperties.getProperty("provider");
			String oldProvider = oldProperties.getProperty("provider");
			
			if (!newProvider.equals(oldProvider)) {
				
				if (newProvider.equals("yes")) {
					codeManipulator.setDataProvider(true);
				}
				else {
					codeManipulator.setDataProvider(false);
				}
			}
			
			// Update java file
			Path newSourcePath = new Path(newSourceName);
			IPackageFragmentRoot newRoot = jProject.findPackageFragmentRoot(newSourcePath);
			IPackageFragment newFragment = newRoot.createPackageFragment(newPackageName, false, null);
			
			// update package name
			codeManipulator.updatePackage(newPackageName);
			
			ICompilationUnit cu = codeManipulator.getCompilationUnit();
			
			// update class name
			if (!newJavaFileName.equals(oldJavaFileName)) {
				codeManipulator.updateClassName(newJavaFileName);
			}
			
			if (!newPackageName.equals(oldPackageName)) {
				IPackageFragment oldFragment = newRoot.getPackageFragment(oldPackageName);
				cu = oldFragment.getCompilationUnit(newJavaFileName + ".java");
				cu.move(newFragment, null, null, false, null);
				
				if (oldPackageName.equals("org.ow2.isac.plugin.pluginname") &&
					!oldFragment.containsJavaResources()) {
					oldFragment.delete(true, null);
				}
			}
			
			destroy();
			
			save = true;
			destroy = true;
		}
	}
	
	/**
	 * This method saves plugin properties file.
	 * @throws Exception if there is an error.
	 */
	private void savePropertiesFile() throws Exception {
		
		IFile propertiesIFile = project.getFile(FileName.PLUGIN_PROPERTIES_FILE);
		
		StringBuffer sb = new StringBuffer();
		sb.append("plugin.name=");
		sb.append(newProperties.getProperty("plugin.name"));
		sb.append("\n");
		sb.append("plugin.guiFile=");
		sb.append(newProperties.getProperty("plugin.guiFile"));
		sb.append("\n");
		sb.append("plugin.xmlFile=");
		sb.append(newProperties.getProperty("plugin.xmlFile"));
		sb.append("\n");
		
		propertiesIFile.setContents(new ByteArrayInputStream(sb.toString().getBytes()), 
									false, false, null);
		
	}
	
	/**
	 * This method saves xml and properties files.
	 * @throws Exception if there is an error.
	 */
	private void savePluginFiles() throws Exception {
		
		// Update gui file content
		String oldGuiFileName = oldProperties.getProperty("plugin.guiFile");
		String newGuiFileName = newProperties.getProperty("plugin.guiFile");
		IFile oldGuiIFile = project.getFile(oldGuiFileName);
		IFile newGuiIFile = project.getFile(newGuiFileName);
		InputStream is = xmlManipulator.getGuiContent();
		
		if (!newGuiIFile.exists()) {
			oldGuiIFile.delete(IResource.NONE, null);
			newGuiIFile.create(is, IResource.NONE, null);
		}
		else {
			oldGuiIFile.setContents(is, false, false, null);
		}
		is.close();
		
		// Update plugin file content
		String oldPluginFileName = oldProperties.getProperty("plugin.xmlFile");
		String newPluginFileName = newProperties.getProperty("plugin.xmlFile");
		IFile oldPluginFile = project.getFile(oldPluginFileName);
		IFile newPluginIFile = project.getFile(newPluginFileName);
		is = xmlManipulator.getPluginContent();
		
		if (!newPluginIFile.exists()) {
			oldPluginFile.delete(IResource.NONE, null);
			newPluginIFile.create(is, IResource.NONE, null);
		}
		else {
			oldPluginFile.setContents(is, false, false, null);
		}
		is.close();

		// Update build file content
		IFile buildFile = project.getFile("build.xml");
		xmlManipulator.setXMLFiles(newPluginFileName, newGuiFileName);
		is = xmlManipulator.getBuildContent();
		if (!buildFile.exists())
		{
			buildFile.create(is, IResource.NONE, null);
		}
		else
		{
			buildFile.setContents(is, false, false, null);
		}
	}

	/**
	 * This method resets all references.
	 */
	public void destroy() {
		
		if (!destroy) {
			if (xmlManipulator != null) {
				xmlManipulator.reset();
				xmlManipulator = null;
			}
			
			if (codeManipulator != null) {
				codeManipulator.reset();
				codeManipulator = null;
			}
			
			init = false;
			save = false;
			destroy = false;
			ref = null;
		}
	}
	
	/**
	 * This method synchronizes the Java file with the plugin XML file.
	 * Only adds elements in java file.
	 */
	private void synchronize() throws Exception{
		
		ArrayList<String> actions;
		
		// synchronize sample
		String action = "sample";
		actions = xmlManipulator.getActions(action);
		Iterator iter = actions.iterator();
		while (iter.hasNext()) {
			String name = (String) iter.next();
			// add action in Java file
			int number = xmlManipulator.getActionNumber(action, name);
			codeManipulator.addAction(action, name, number);
			
			// add parameters in Java file
			String [] params = xmlManipulator.getStringParams(action, name);
			for (int i = 0; i < params.length; i++) {
				codeManipulator.addActionParameter(action, name, params[i]);
			}
		}
		
		// synchronize timer
		action = "timer";
		actions = xmlManipulator.getActions(action);
		iter = actions.iterator();
		while (iter.hasNext()) {
			String name = (String) iter.next();
			// add action in Java file
			int number = xmlManipulator.getActionNumber(action, name);
			codeManipulator.addAction(action, name, number);
			
			// add parameters in Java file
			String [] params = xmlManipulator.getStringParams(action, name);
			for (int i = 0; i < params.length; i++) {
				codeManipulator.addActionParameter(action, name, params[i]);
			}
		}
		
		// synchronize test
		action = "test";
		actions = xmlManipulator.getActions(action);
		iter = actions.iterator();
		while (iter.hasNext()) {
			String name = (String) iter.next();
			// add action in Java file
			int number = xmlManipulator.getActionNumber(action, name);
			codeManipulator.addAction(action, name, number);
			
			// add parameters in Java file
			String [] params = xmlManipulator.getStringParams(action, name);
			for (int i = 0; i < params.length; i++) {
				codeManipulator.addActionParameter(action, name, params[i]);
			}
		}
		
		// synchronize control
		action = "control";
		actions = xmlManipulator.getActions(action);
		iter = actions.iterator();
		while (iter.hasNext()) {
			String name = (String) iter.next();
			// add action in Java file
			int number = xmlManipulator.getActionNumber(action, name);
			codeManipulator.addAction(action, name, number);
			
			// add parameters in Java file
			String [] params = xmlManipulator.getStringParams(action, name);
			for (int i = 0; i < params.length; i++) {
				codeManipulator.addActionParameter(action, name, params[i]);
			}
		}
		
		// synchronize plugin parameter
		action = "object";
		actions = xmlManipulator.getActions(action);
		iter = actions.iterator();
		while (iter.hasNext()) {
			String name = (String) iter.next();
						
			// add parameters in Java file
			String [] params = xmlManipulator.getStringParams(action, name);
			for (int i = 0; i < params.length; i++) {
				codeManipulator.addActionParameter(action, name, params[i]);
			}
		}
		
		// save modifications
		codeManipulator.getCompilationUnit();
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#clone()
	 */
	public Object clone() throws CloneNotSupportedException {
		throw new CloneNotSupportedException();
	}

}
