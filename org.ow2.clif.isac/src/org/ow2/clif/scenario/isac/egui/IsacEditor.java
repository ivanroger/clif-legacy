/*
* CLIF is a Load Injection Framework
* Copyright (C) 2005, 2013 France Telecom R&D
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
* Contact: clif@ow2.org
*/

package org.ow2.clif.scenario.isac.egui;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jface.text.IDocument;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorSite;
import org.eclipse.ui.IKeyBindingService;
import org.eclipse.ui.INestableKeyBindingService;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.WorkbenchException;
import org.eclipse.wst.sse.core.internal.provisional.IModelStateListener;
import org.eclipse.wst.sse.core.internal.provisional.IStructuredModel;
import org.eclipse.wst.sse.core.internal.provisional.StructuredModelManager;
import org.eclipse.wst.sse.ui.StructuredTextEditor;
import org.eclipse.wst.xml.core.internal.provisional.document.IDOMModel;
import org.eclipse.wst.xml.core.internal.provisional.format.DocumentNodeFormatter;
import org.eclipse.wst.xml.ui.internal.tabletree.XMLMultiPageEditorPart;
import org.ow2.clif.scenario.isac.egui.loadprofile.LoadProfile;
import org.ow2.clif.scenario.isac.egui.loadprofile.LoadProfileView;
import org.ow2.clif.scenario.isac.egui.model.ModelReaderXIS;
import org.ow2.clif.scenario.isac.egui.pages.pageBehavior.BehaviorEditor;
import org.ow2.clif.scenario.isac.egui.pages.pageImport.ImportEditor;
import org.w3c.dom.Document;

/**
 * This class is the editor for ISAC scenario
 * At least four pages (four tab) for this editor.
 * XMLTree, XMLSource, Import and at least one behavior.
 * 
 * @author Joan Chaumont
 * @author Bruno Dillenseger
 */
public class IsacEditor extends XMLMultiPageEditorPart
implements IModelStateListener {

    /* XML source position page */
    private static final int XML_EDITOR_ID = 1;
    /* Import editor position page */
    private static final int IMPORT_EDITOR_ID = 2;
    /* All behavior editor are after this position */
    private static final int BEFORE_BEHAVIORS = 3;
    
    /* Old page is used when a page change is requested */
    private int oldPage = 0;
    
    /* Our editors : all behavior editors are in the list behaviors 
     * Only one editor for import edition */
    private List<BehaviorEditor> behaviors;
    private ImportEditor importEditor;
    
    /* OUR MODEL : MOST IMPORTANT VALUE 
     * all changes are done on this model */
    private IStructuredModel model;
    
    /* Set as true if modification are needed */
    private boolean commit;
    /* Set as true if modification need to be save */
    private boolean dirty;
    
    /* All profiles needed by the profileView */
    private Map<String,LoadProfile> profiles;
    
    /** Simple constructor */
    public IsacEditor() {
        super();
        behaviors = new ArrayList<BehaviorEditor>();
    }
    
    public void init(IEditorSite site, IEditorInput input)
            throws PartInitException {
        super.init(site, input);
        
        /* Change perspective on open */
        IWorkbenchWindow window = site.getWorkbenchWindow();
        try {
            window.getWorkbench().showPerspective(
                    IsacScenarioPerspective.ISAC_SCENARIO_PERSPECTIVE_ID, window);
        } catch (WorkbenchException e) {
            e.printStackTrace();
        }
    }

    protected void createPages() {
        super.createPages();
        /* get model needed by the pages and parse the profiles */
        model = getModel();
        profiles = ModelReaderXIS.getProfiles(getDocument(model));
        
        /* Add all pages : import and behaviors */
        addImportPage();
        addBehaviorPages();
        
        /* Set the profile view */
        loadProfile();
        
        model.addModelStateListener(this);
    }

    private void addImportPage() {
        importEditor = new ImportEditor(new ScenarioManager(), this);
        int index = -1;
        try {
            index = addPage(importEditor, getEditorInput());
        } catch (PartInitException e) {
            e.printStackTrace();
        }
        
        importEditor.setDocument(getDocument());
        setPageText(index, "Import");
    }
    /* For each behavior found in the model it creates another page */
    private void addBehaviorPages() {
        String [] ids = ModelReaderXIS.getBehaviorsId(getDocument(model));
        
        for (int i = 0; i < ids.length; i++) {
            addBehaviorPage(ids[i]);
        }
    }
    /**
     * Create a new page editor for a behavior
     * This function may be called by the add behavior action 
     * in ImportEditor or BehaviorEditor
     * @param id the id of the behavior
     */
    public void addBehaviorPage(String id) {
        BehaviorEditor behaviorEditor = 
            new BehaviorEditor(new ScenarioManager(), this, id, 
                    getPageCount()-BEFORE_BEHAVIORS, getProfile(id));
        int index = -1;
        try {
            index = addPage(behaviorEditor, getEditorInput());
        } catch (PartInitException e) {
            e.printStackTrace();
        }
        
        behaviorEditor.setDocument(getDocument());
        behaviorEditor.setBehaviorPos(index-BEFORE_BEHAVIORS);
        setPageText(index, "Behavior " + id);
        behaviors.add(behaviorEditor);
    }
    /* Return correct model from the input document */
    protected IStructuredModel getModel() {
        IDocument document = getDocument();
        
        IStructuredModel model = null;
        try {
            model = StructuredModelManager.getModelManager().getExistingModelForRead(document);
        }
        finally {
            if (model != null) {
                model.releaseFromRead();
            }
        }
        return model;
    }
    /* Get w3c document from model */
    private static Document getDocument(IStructuredModel model) {
        Document domDoc = null;

        if (model != null && model instanceof IDOMModel) {
            domDoc = ((IDOMModel) model).getDocument();
        }
        return domDoc;
    }
    
    /* Get the document from text editor input */
    private IDocument getDocument() {
        IDocument document = null;
        StructuredTextEditor textEditor = getEditor();
        if (textEditor != null) {
            document = textEditor.getDocumentProvider().getDocument(
                    textEditor.getEditorInput());
        }
        return document;
    }
    
    /* Return the XMLSource editor */
    private StructuredTextEditor getEditor() {
        return ((StructuredTextEditor)this.getEditor(XML_EDITOR_ID));
    }

    /* set profiles */
    private void loadProfile() {
        profiles = ModelReaderXIS.getProfiles(getDocument(model));

        IWorkbenchPage page = getEditorSite().getPage();
        if(page != null){
            LoadProfileView loadView = (LoadProfileView)page.findView(
                    IsacScenarioPerspective.ISAC_SCENARIO_LOADPROFILE_ID);            
            if(loadView != null) {
                loadView.setProfiles(profiles);
            }
        }
    }

    /**
     * Return the profile for a behavior
     * @param behaviorId
     * @return LoadProfile the load profile
     */
    public LoadProfile getProfile(String behaviorId) {
        return profiles.get(behaviorId);
    }

    /**
     * @return HashMap all profiles indexed by behavior id
     */
    public Map<String,LoadProfile> getProfiles() {
        return profiles;
    }

    public boolean isDirty() {
        return super.isDirty() || dirty;
    }

    /**
     * Set the dirty boolean
     * @param d isDirty
     */
    public void setDirty(boolean d) {
        dirty = d;
        firePropertyChange(PROP_DIRTY);
    }

    public void doSave(IProgressMonitor arg0) {
        int page = getActivePage();
        if(page >= BEFORE_BEHAVIORS) {
            BehaviorEditor be = behaviors.get(page - BEFORE_BEHAVIORS);
            be.getScenario().getPluginGUIManager().commit();
        }
        if(page == IMPORT_EDITOR_ID) {
            importEditor.getScenario().getPluginGUIManager().commit();
        }
        
        super.doSave(arg0);
    }

    /**
     * Refresh the import editor or a behavior editor
     * depending on current editor
     */
    public void refresh() {
        int index = getActivePage();
        if(index >= BEFORE_BEHAVIORS) {
            /* find correct behavior editor and refresh */
            BehaviorEditor be = behaviors.get(index - BEFORE_BEHAVIORS);
            be.refresh();
        }
        else if(index == IMPORT_EDITOR_ID) {
            importEditor.refresh();
        }
    }
    
    /* modifications may be commited in this function */
    protected void pageChange(int newPageIndex) {
        /* if file is dirty we must keep it dirty */
        boolean keepDirty = isDirty();
        if(oldPage >= BEFORE_BEHAVIORS) {
            BehaviorEditor be = behaviors.get(oldPage - BEFORE_BEHAVIORS);
            be.getScenario().getPluginGUIManager().commit();
            
            if(!keepDirty) {
                doSave(null);
            }
        }
        else if(oldPage == IMPORT_EDITOR_ID) {
            importEditor.getScenario().getPluginGUIManager().commit();
            
            if(!keepDirty) {
                doSave(null);
            } 
        }
        /* Remove the key bindings if it isn't XMLsource edition */
        if(newPageIndex != 1) {
            IKeyBindingService service = getSite().getKeyBindingService();
            if (service instanceof INestableKeyBindingService) {
                INestableKeyBindingService nestableService = (INestableKeyBindingService) service;
                nestableService.activateKeyBindingService(null);
            }           
        }
        /* Format the XML source */
        if(newPageIndex == 1) {
            DocumentNodeFormatter formatter = new DocumentNodeFormatter();
            if(model != null){
            	formatter.format(((IDOMModel)model).getDocument());
            }
            
            if(!keepDirty) {
                doSave(null);
            }
        }
        
        super.pageChange(newPageIndex);
        refresh();
        
        oldPage = newPageIndex;
    }
    /**
     * Force the model to commit all changes
     */
    public void modelChanged() {
        commit = true;
        model.aboutToChangeModel();
        model.changedModel();
    }
    
    public void modelChanged(IStructuredModel model) {
        if(commit || 
                (getActivePage() >= 0 && getActivePage() < BEFORE_BEHAVIORS)) {
            String[] ids = ModelReaderXIS.getBehaviorsId(getDocument(model));
            
            /* Test if behavior name or number have changed */
            if(ids.length == behaviors.size()) {
                modifyOldBehaviorPage(ids);
            }
            else if(ids.length > behaviors.size()) {
                createNewBehaviorPages(ids);
            }
            else if(ids.length < behaviors.size()) {
                removeOldBehaviorPages(ids);
            }
            
            /* Test if some loadprofile have changed */
            loadProfile();
            Iterator<BehaviorEditor> iter = behaviors.iterator();
            while (iter.hasNext()) {
                BehaviorEditor editor = iter.next();
                editor.setProfile(getProfile(editor.getBehaviorId()));
            }
            commit = false;
        }
    }
    /* If some behaviors have changed their name */
    private void modifyOldBehaviorPage(String[] ids) {
        for (int i = 0; i < ids.length; i++) {
            BehaviorEditor behavior = behaviors.get(i);
            if(!behavior.getBehaviorId().equals(ids[i])) {
                behavior.setBehaviorId(ids[i]);
                setPageText(i+BEFORE_BEHAVIORS, "Behavior " + ids[i]);
            }
        }
    }
    
    /* If some behaviors appeared */
    private void createNewBehaviorPages(String[] ids) {
        int nbBehaviors = behaviors.size();
        int i;
        for(i = 0; i < nbBehaviors; i++) {
            BehaviorEditor behavior = behaviors.get(i);
            behavior.setBehaviorId(ids[i]);
            setPageText(i+BEFORE_BEHAVIORS, "Behavior " + ids[i]);
        }
        
        for(; i < ids.length; i++) {
            addBehaviorPage(ids[i]);
        }
    }
    
    /* If some behaviors disappeared */
    private void removeOldBehaviorPages(String[] ids) {
        for(int i = 0; i < ids.length; i++) {
            BehaviorEditor behavior = behaviors.get(i);
            behavior.setBehaviorId(ids[i]);
            behavior.setBehaviorPos(i);
            setPageText(i+BEFORE_BEHAVIORS, "Behavior " + ids[i]);
        }
        
        while(ids.length < behaviors.size()) {
            int last = behaviors.size()-1;
            behaviors.remove(last);
            oldPage = last - 1;
            removePage(last + BEFORE_BEHAVIORS);
        }
    }

    public void modelAboutToBeChanged(IStructuredModel model) {}
    public void modelDirtyStateChanged(IStructuredModel model, boolean isDirty) {}
    public void modelResourceDeleted(IStructuredModel model) {}
    public void modelResourceMoved(IStructuredModel oldModel, IStructuredModel newModel) {}
    public void modelAboutToBeReinitialized(IStructuredModel structuredModel) {}
    public void modelReinitialized(IStructuredModel structuredModel) {}
}

