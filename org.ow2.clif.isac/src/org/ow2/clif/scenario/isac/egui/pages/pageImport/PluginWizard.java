/*
* CLIF is a Load Injection Framework
* Copyright (C) 2005 France Telecom R&D
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
* CLIF $Name: not supported by cvs2svn $
*
* Contact: clif@ow2.org
*/

package org.ow2.clif.scenario.isac.egui.pages.pageImport;

import org.eclipse.jface.viewers.ILabelProvider;
import org.eclipse.jface.viewers.ILabelProviderListener;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.INewWizard;
import org.eclipse.ui.IWorkbench;
import org.eclipse.wst.xml.core.internal.document.DocumentImpl;
import org.ow2.clif.scenario.isac.egui.Icons;
import org.ow2.clif.scenario.isac.egui.model.ModelWriterXIS;
import org.ow2.clif.scenario.isac.egui.plugins.PluginManager;
import org.ow2.clif.scenario.isac.egui.plugins.nodes.Node;
import org.w3c.dom.Element;

/**
 * This class is used for adding a plug-in to the 
 * plug-ins section.
 * 
 * @author Joan Chaumont
 */
public class PluginWizard extends Wizard implements INewWizard, ISelectionChangedListener {
    
    private PluginWizardPage page;
    
    private DocumentImpl doc;
    private PluginManager pluginsMgr;
    private String selected;
     

    public PluginWizard(DocumentImpl doc, PluginManager pluginsMgr) {
        super();
        this.doc = doc;
        this.pluginsMgr = pluginsMgr;
        setWindowTitle("ISAC Plug-ins");
    }
    
    class TablePluginContentProvider implements IStructuredContentProvider, ILabelProvider {

        public Object[] getElements(Object inputElement) {
            return (Object[])inputElement;
        }

        public void dispose() {}

        public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {}

        public Image getImage(Object element) {
            Image image = null;
            try {
                image = Icons.getImageRegistry().get(Node.USE);
            }
            catch (Exception e) {
                e.printStackTrace(System.out) ;
            }
            return image;
        }

        public String getText(Object element) {
            return (String)element;
        }
        
        public void addListener(ILabelProviderListener listener) {}
        
        public void removeListener(ILabelProviderListener listener) {}
        
        public boolean isLabelProperty(Object element, String property) {
            return false;
        }
    }
    
    class PluginWizardPage extends WizardPage {
        
        TableViewer plugins;
        
        protected PluginWizardPage() {
            super("paramWizard");
            setTitle("Add a new plug-in");
        }
        
        public void createControl(Composite parent) {
            Composite main = new Composite(parent, SWT.NULL);
            main.setLayout(new GridLayout());
            main.setLayoutData(new GridData(GridData.FILL_BOTH));
            
            plugins = new TableViewer(main, SWT.SINGLE | SWT.BORDER);
            
            TablePluginContentProvider provider = new TablePluginContentProvider();
            plugins.setContentProvider(provider);
            plugins.setLabelProvider(provider);
            plugins.setInput(pluginsMgr.getPluginsName());
            
            plugins.getControl().setLayoutData(new GridData(GridData.FILL_BOTH));
            plugins.addSelectionChangedListener(PluginWizard.this);
            
            setControl(main);
        }
    }
    
    public void addPages() {
        page = new PluginWizardPage();
        addPage(page);
    }
    
    public boolean performFinish() {
        org.w3c.dom.Node plugins = doc.getElementsByTagName("plugins").item(0);
        if(plugins == null) {
            plugins = ModelWriterXIS.correctPlugins(doc);
        }
        
        Element newPlugin = doc.createElement(Node.USE);
        newPlugin.setAttribute("id", ImportUtil.generatePluginId(doc, selected));
        newPlugin.setAttribute("name", selected);
        plugins.appendChild(newPlugin);
        ModelWriterXIS.addParams(
        	doc,
        	newPlugin,
        	pluginsMgr.getDescription(selected).getObject().getParams());
        return true;
    }    

    public void init(IWorkbench workbench, IStructuredSelection selection) {}

    public void selectionChanged(SelectionChangedEvent event) {
        IStructuredSelection sel = (IStructuredSelection)((TableViewer)event.getSource()).getSelection();
        selected = (String)sel.getFirstElement();
    }
}
