/*
* CLIF is a Load Injection Framework
* Copyright (C) 2005, 2009 France Telecom R&D
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
* CLIF $Name: not supported by cvs2svn $
*
* Contact: clif@ow2.org
*/

package org.ow2.clif.scenario.isac.egui.util;

import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.ISelectionProvider;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.ow2.clif.scenario.isac.egui.pages.pageBehavior.ActionPlacement;
import static org.ow2.clif.scenario.isac.egui.pages.pageBehavior.ActionPlacement.*;
import static org.ow2.clif.scenario.isac.egui.pages.pageBehavior.BehaviorMasterPage.*;
import org.ow2.clif.scenario.isac.egui.plugins.nodes.Node;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import java.util.ArrayList;
import java.util.List;

/**
 * Some util function for behaviors
 * @author Joan Chaumont
 * @author Florian Francheteau
 */
public class BehaviorUtil {


	/**
	 * Test if an element target is a child of an element
	 * @param target the supposed child
	 * @param id	 id of parent element
	 * @return true	 if element is a parent of target
	 */
	public static boolean isParent(Element target, String id) {
		if (target.toString().equals(id)) {
			return true;
		}

		Element parent = (Element) target.getParentNode();
		while (parent != null && !parent.getNodeName().equals("behaviors")) {
			parent.normalize();
			if (parent.toString().equals(id)) {
				return true;
			}
			parent = (Element) parent.getParentNode();
		}
		return false;
	}

	/**
	 * Test if an element can be add into another one
	 * @param target the selected element where new element will be added
	 * @param toDrop the element to add
	 * @return true	 if toDrop can be added to target
	 */
	public static boolean childrenAllowed(Element target, Element toDrop) {
		String nameTarget = target.getNodeName();
		String nameToDrop = toDrop.getNodeName();
		if (nameTarget.equals("plugins")) {
			return nameToDrop.equals("use");
		}
		if ((nameTarget.equals("choice")) ||
				(nameTarget.equals("then")) ||
				(nameTarget.equals("else"))) {
			return !nameToDrop.equals("then")
					&& !nameToDrop.equals("else")
					&& !nameToDrop.equals("choice");
		} else if (nameToDrop.equals("then")
				|| nameToDrop.equals("else")) {
			return nameTarget.equals("if");
		} else if (nameToDrop.equals("choice")) {
			return nameTarget.equals("nchoice");
		} else if (Node.isPluginNode(nameTarget)) {
			return false;
		} else if (Node.isControllerNode(nameTarget)) {
			if (nameTarget.equals("if")) {
				return nameToDrop.equals("then")
						|| nameToDrop.equals("else");
			} else {
				return !nameToDrop.equals("then")
						&& !nameToDrop.equals("else")
						&& !nameToDrop.equals("choice");
			}
		} else if (nameTarget.equals("nchoice")) {
			return nameToDrop.equals("choice");
		}

		return true;
	}

	/**
	 * Test if the target can posses sibling
	 * @param target  Target element to test
	 * @param newNode Node to insert
	 * @return True if it can have sibling
	 */
	public static boolean siblingAllowed(Element target, Element newNode) {
		if (target == null) {
			return false;
		}
		String nameTarget = target.getNodeName();
		String nameNewTarget = newNode.getNodeName();
		if (nameTarget.equals("choice") && nameNewTarget.equals("choice")) {
			return true;
		}
		return !(nameTarget.equals("else") ||
				nameTarget.equals("then") ||
				nameTarget.equals("choice"));
	}


	/**
	 * Test if the target element can have children
	 * @param target element to test
	 * @return True if allowed
	 */
	public static boolean childrenAllowed(Element target) {
		if (target == null) {
			return false;
		}
		String nameTarget = target.getNodeName();
		return (nameTarget.equals("while") ||
				nameTarget.equals("choice") ||
				nameTarget.equals("preemptive") ||
				nameTarget.equals("then") ||
				nameTarget.equals("else")
		);
	}

	public static boolean isAllowedToMove(Element target) {
		if(target==null){
			return false;
		}
		String nameElement = target.getNodeName();
		return !(nameElement.equals("then") || nameElement.equals("else"));
	}

	/**
	 * Get the add text.<br />
	 * The text can change depending on the selected node and desactivated if there is no possibility
	 * @param sel
	 * @return Text of the button. "" if the button has to be disabled.
	 */
	public static String getAddText(IStructuredSelection sel) {
		Element elt = (Element) sel.getFirstElement();
		if (elt != null) {
			if (elt.getNodeName().equals("if")) {
				boolean present = false;
				NodeList nls = elt.getElementsByTagName("then");
				/* if there is no "then" element */
				if (nls.getLength() == 0) {
					return ADD_THEN;
				}
				for (int i = 0; i < nls.getLength(); i++) {
					/* if element "then" is a child of selected item*/
					if (nls.item(i).getParentNode() == elt) {
						present = true;
					}
				}
				if (!present) {
					return ADD_THEN;
				}
				present = false;
				nls = elt.getElementsByTagName("else");
				/* if there is no "else" element */
				if (nls.getLength() == 0) {
					return ADD_ELSE;
				}
				for (int i = 0; i < nls.getLength(); i++) {
					/* if element "else" is a child of selected item*/
					if (nls.item(i).getParentNode() == elt) {
						present = true;
					}
				}
				if (!present) {
					return ADD_ELSE;
				}
				return "";
			} else if (elt.getNodeName().equals("nchoice")) {
				return ADD_CHOICE;
			} else {
				if (childrenAllowed(elt)) {
					return ADD_CHILD;
				}
			}
		}
		return "";
	}

	/**
	 * Insert a new node in the document dependending on the selected node and the placement required
	 * @param newNode		   Node to add
	 * @param selectedNode	  The selected node. May be null
	 * @param rootBehaviourNode The root behavior element. Used to insert at the begin or at the end
	 * @param actionPlacement   Place to insert the new node
	 */
	public static boolean insertElementInTree(Element newNode, Element selectedNode, Element rootBehaviourNode, ActionPlacement actionPlacement) {
		actionPlacement = validatePlacement(newNode, selectedNode, actionPlacement);
		if (actionPlacement == BEGIN) {
			NodeUtil.insertAtBegin(newNode, rootBehaviourNode);
		} else if (actionPlacement == END) {
			rootBehaviourNode.appendChild(newNode);
		} else if (actionPlacement == CHILD) {
			selectedNode.appendChild(newNode);
		} else if (actionPlacement == BEFORE_NODE) {
			NodeUtil.checkAndInsertBefore(newNode, selectedNode);
		} else if (actionPlacement == AFTER_NODE) {
			NodeUtil.checkAndInsertAfter(newNode, selectedNode);
		} else if (actionPlacement == CANCEL) {
			return false;
		}
		return true;
	}

	/**
	 * Validate and change the action Placement depending on the targeted node
	 * @param nodeToAdd	   Node to insert
	 * @param node			Target node
	 * @param actionPlacement Placement of the node to insert   @return Valide placement to insert a new node
	 * @return Place of the insertion
	 */
	public static ActionPlacement validatePlacement(Element nodeToAdd, Element node, ActionPlacement actionPlacement) {
		if (node == null) {
			if (actionPlacement == BEGIN || actionPlacement == BEFORE_NODE || actionPlacement == CHILD) {
				actionPlacement = BEGIN;
			} else {
				actionPlacement = END;
			}
		}
		if (actionPlacement == CHILD) {
			if (!childrenAllowed(node, nodeToAdd)) {
				if (siblingAllowed(node, nodeToAdd)) {
					actionPlacement = AFTER_NODE;
				} else {
					actionPlacement = CANCEL;
				}
			}
		}
		return actionPlacement;
	}

	/**
	 * If elements are on differents level, filter only element on the same level
	 * @param listElement List of element to filter
	 * @return Liste of element filtered
	 */
	public static List<Element> getSameLevelElement(List<Element> listElement) {
		ArrayList<Element> arrayList = new ArrayList<Element>();
		if (!listElement.isEmpty()) {
			Element firstElement = listElement.get(0);
			for (Element element : listElement) {
				if (element.getParentNode().equals(firstElement.getParentNode())) {
					arrayList.add(element);
				}
			}
		}
		return arrayList;
	}
}
