/*
* CLIF is a Load Injection Framework
* Copyright (C) 2004,2012 France Telecom R&D
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
* Contact: clif@ow2.org
*/
package org.ow2.clif.scenario.isac.egui.plugins.nodes;

import java.util.Vector;

/**
 * This class embeds help information for nodes of type 'preemptive' 
 * 
 * @author JC Meillaud
 * @author A Peyrard
 * @author Bruno Dillenseger
 */
public abstract class PreemptiveNode
{
    /**
     * This method return the help of a preemptive node
     * @return The help lines
     */
    public static Vector<String> getHelp()
    {
        Vector<String> help = new Vector<String>() ;
        help.add("Preemptive statement.");
        help.add("Comes with a condition and a behavior block.");
        help.add("The condition is checked before executing each instruction of the behavior block.");
        help.add("If the condition evaluates to true, the next instruction in the behavior block is executed.");
        help.add("If the condition evaluates to false, all remaining instructions in the behavior block are skipped.");
        return help;
    }
}
