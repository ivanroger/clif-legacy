/*
* CLIF is a Load Injection Framework
* Copyright (C) 2005, 2010 France Telecom R&D
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
* Contact: clif@ow2.org
*/

package org.ow2.clif.scenario.isac.egui.loadprofile;

import java.util.ArrayList;
import java.util.List;

/**
 * A load profile set for a specific behavior.
 * @author Joan Chaumont
 * @author Bruno Dillenseger
 */
public class LoadProfile {

    private String id;
    private List<ProfilePoint> profilesPoints;
    private boolean forceStop;
    
    /**
     * Constructor
     * @param id
     * @param profPoints
     * @param force
     */
    public LoadProfile(String id, List<ProfilePoint> profPoints, boolean force) {
        this.id = id;
        
        this.profilesPoints = new ArrayList<ProfilePoint>();
        int nbPoints = profPoints.size();
        for (int i = 0; i < nbPoints; i++) {
            addPoint(profPoints.get(i));
        }
        
        this.forceStop = force;
    }

    /** @return String id */
    public String getId() {
        return id;
    }

    /** @return ArrayList the list of profilespoints*/
    public List<ProfilePoint> getProfilesPoints() {
        return profilesPoints;
    }
    /** @return boolean forceStop */
    public boolean getForceStop() {
        return forceStop;
    }

    /**
     * Sets the force stop flag
     * @param force if true, means running vUsers may be terminated
     * before full behavior completion, according to the load profile.
     * If false, running vUsers will complete their behavior.
     */
    public void setForceStop(boolean force)
    {
    	forceStop = force;
    }

    /**
     * If the profile have some crenel_xx points we need to add
     * some additional points. 
     * @return int the number of additional points 
     */
    public int getAdditionalPoints() {
        int nbPoints = profilesPoints.size();
        
        int additionals = 0;
        
        for (int i = 0; i < nbPoints; i++) {
            String style = profilesPoints.get(i).rampStyle;
            if(style.toLowerCase().equals("crenel_vh") ||
                    style.toLowerCase().equals("crenel_hv")) {
                additionals++;
            }
        }
        return additionals;
    }

    /**
     * @return int the max value of time for these points
     */
    public int getMaxTime() {
        int nbPoints = profilesPoints.size();
        int max = 0;
        for (int i = 0; i < nbPoints; i++) {
            int time = profilesPoints.get(i).time;
            if(time > max) {
                max = time;
            }
        }
        return max;
    }
    
    /**
     * @return int the max value of population for these points
     */
    public int getMaxPopulation() {
        int nbPoints = profilesPoints.size();
        int max = 0;
        for (int i = 0; i < nbPoints; i++) {
            int pop = profilesPoints.get(i).population;
            if(pop > max) {
                max = pop;
            }
        }
        return max;
    }

    /**
     * Add a new point in this profile
     * @param point the new point
     */
    public void addPoint(ProfilePoint point) {
        int nbPoints = profilesPoints.size();
        for (int i = 0; i < nbPoints; i++) {
            if(point.time < profilesPoints.get(i).time) {
                profilesPoints.add(i, point);
                return;
            }
        }
        profilesPoints.add(point);
    }

    /**
     * Remove a point
     * @param point the point to remove
     */
    public void removePoint(ProfilePoint point) {
        profilesPoints.remove(point);
    }

    /**
     * Check if profile have already a point at this time
     * @param time
     * @return true if a point already exists
     */
    public boolean containsTime(int time) {
        int nbPoints = profilesPoints.size();
        for (int i = 0; i < nbPoints; i++) {
            if(time == profilesPoints.get(i).time) {
                return true;
            } 
        }
        return false;
    }   
    
    /** Remove point at this time 
     * @param time 
     */
    public void removeTime(int time) {
        int nbPoints = profilesPoints.size();
        for (int i = 0; i < nbPoints; i++) {
            if(time == profilesPoints.get(i).time) {
                profilesPoints.remove(i);
                return;
            }
        }
    }

    /**
     * Check if the profile is correct 
     * @return true if there isn't two points at the same time
     */
    public boolean isCorrect() {
        int nbPoints = profilesPoints.size();
        
        for (int i = 0; i < nbPoints-1; i++) {
            if(profilesPoints.get(i).time == profilesPoints.get(i+1).time) {
                return false;
            }
        }
        return true;
    }
    
    /**
     * Check if there is enough point
     * @return true if there is more than 1 point
     * other than time = 0
     */
    public boolean enoughPoints() {
        int nbPoints = profilesPoints.size();
        if(nbPoints < 1) {
            return false;
        }
        else if(profilesPoints.get(0).time == 0 && nbPoints < 2) {
            return false;
        }
        return true;
    }
}
