/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2009 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * CLIF $Name: not supported by cvs2svn $
 *
 * Contact: clif@ow2.org
 */


package org.ow2.clif.scenario.isac.egui.wizards.httpCapture;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import org.ow2.clif.util.ExecutionContext;
import com.bitmechanic.maxq.Config;

/**
 * This class extends from the maxQ Config class. It is used
 * for taking properties from properties file instead of system properties
 * 
 * @author Florian Francheteau
 */

public class ConfigIsac extends Config {
	
	  /**
	   * Simple constructor calling super constructor
	   */
	  private ConfigIsac()
	  {
		  super();
	  }
	  
	  /**
	   * Set up the properties file from maxq.properties file
	   */
	  public static void initConfig()
	  {
		  config = new ConfigIsac();

		  // Retrieve maxq.properties file
		  String propertiesFileName = ExecutionContext.getMaxQPropFile();
		  try {
			  FileInputStream propInputFile = new FileInputStream(propertiesFileName);
			  props = new Properties();
			  props.load(propInputFile);
		  } catch (IOException e) {
			  // We do not want to absorb the exception without reporting
			  // it so we convert it to what Java uses to report Errors.
			  System.err.println("Could not find " + propertiesFileName);
		  }
		  // Load default port.
		  String portStr = props.getProperty("local.proxy.port");
		  if (portStr != null)
			  config.setPort(Integer.parseInt(portStr));
	  }	  

	  /** 
	   * get maxQ configuration and load it if it does not exist
	   */
	  public static Config getConfig()
	  {
	      // if config is null, it means that the Config has not yet been
	      // explicitly initialised, so the caller expects the 'default
	      // behaviour' which is to load it from the properties file.
	      if (config == null){
	    	  ConfigIsac.initConfig();
	      }
	      return config;
	    }
	  
 
	  /** 
	   * remove maxQ configuration
	   */
	  public static void removeConfig(){
		  config=null;
	  }
}
