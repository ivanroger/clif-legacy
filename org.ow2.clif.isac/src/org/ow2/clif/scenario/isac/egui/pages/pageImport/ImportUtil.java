/*
* CLIF is a Load Injection Framework
* Copyright (C) 2005 France Telecom R&D
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
* CLIF $Name: not supported by cvs2svn $
*
* Contact: clif@ow2.org
*/

package org.ow2.clif.scenario.isac.egui.pages.pageImport;

import java.util.ArrayList;
import java.util.List;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

/**
 * Some util function for import editor
 * @author Joan Chaumont
 *
 */
public class ImportUtil {

    /**
     * Generate a plug-in id for a new import
     * @param doc
     * @param pluginName
     * @return String the new id
     */
    public static String generatePluginId(Document doc, String pluginName) {
        NodeList plugins = doc.getElementsByTagName("use");
        int nbPlugins = plugins.getLength();

        List<String> ids = new ArrayList<String>();

        for (int i = 0; i < nbPlugins; i++) {
            ids.add(((Element)plugins.item(i)).getAttribute("id"));
        }
        
        int i = 0;
        String pluginId = pluginName + "_" + i;
        while(ids.contains(pluginId)) {
            pluginId = pluginName + "_" + ++i;
        }
        
        return pluginId;
    }
    
}
