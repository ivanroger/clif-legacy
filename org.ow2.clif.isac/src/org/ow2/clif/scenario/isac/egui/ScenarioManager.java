/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2004 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * CLIF
 *
 * Contact: clif@ow2.org
 */
package org.ow2.clif.scenario.isac.egui;

import org.ow2.clif.scenario.isac.egui.plugins.PluginManager;
import org.ow2.clif.scenario.isac.egui.plugins.gui.PluginGUIManager;

/**
 * This class contains pluginGUIManager and pluginManager 
 * used by each editor 
 *
 * @author JC Meillaud
 * @author A Peyrard
 * @author Joan Chaumont
 */
public class ScenarioManager {
    
	private PluginGUIManager pluginGUIManager;
	private PluginManager pluginManager;

	/**
	 * Build a new Scenario gui editor object, this object is a application
	 * window, which permit the edition of a clif scenario file
	 */
	public ScenarioManager() {
        initManagers();
        pluginManager.initialisePluginsTable(pluginGUIManager);
    }

	/**
	 * Initialise the managers
	 */
	private void initManagers() {
		pluginGUIManager = new PluginGUIManager(this);
		pluginManager = new PluginManager();
	}

    /**
     * @return PluginManager the pluginManager used by this editor
     */
    public PluginManager getPluginManager() {
        return pluginManager;
    }
    
    /**
     * @return PluginGUIManager the pluginGUIManager used by this editor
     */
    public PluginGUIManager getPluginGUIManager() {
        return pluginGUIManager;
    }
}