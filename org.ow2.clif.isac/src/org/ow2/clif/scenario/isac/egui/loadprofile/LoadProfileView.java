/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * CLIF $Name: not supported by cvs2svn $
 *
 * Contact: clif@ow2.org
 */

package org.ow2.clif.scenario.isac.egui.loadprofile;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import org.eclipse.jface.viewers.CheckStateChangedEvent;
import org.eclipse.jface.viewers.CheckboxTableViewer;
import org.eclipse.jface.viewers.ICheckStateListener;
import org.eclipse.jface.viewers.IColorProvider;
import org.eclipse.jface.viewers.ILabelProvider;
import org.eclipse.jface.viewers.ILabelProviderListener;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.SashForm;
import org.eclipse.swt.events.PaintEvent;
import org.eclipse.swt.events.PaintListener;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Table;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IPartListener;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.part.ViewPart;
import org.ow2.clif.scenario.isac.egui.IsacEditor;

/**
 * This view is used in plugin ISAC for displaying
 * profiles
 * @author Joan Chaumont
 *
 */
public class LoadProfileView extends ViewPart 
implements PaintListener, ICheckStateListener, 
IPartListener, ISelectionChangedListener {
    
    private Map<String,Integer> colorsMap;
    
    int[] colors = new int[]{SWT.COLOR_YELLOW, SWT.COLOR_BLUE, SWT.COLOR_RED,
            SWT.COLOR_GREEN, SWT.COLOR_BLACK, SWT.COLOR_CYAN, SWT.COLOR_MAGENTA,
            SWT.COLOR_DARK_YELLOW, SWT.COLOR_GRAY, SWT.COLOR_DARK_BLUE,
            SWT.COLOR_DARK_GREEN, SWT.COLOR_DARK_RED};
    int color = 0;
    
    private Display d;
    private SashForm form;
    
    private Label error; 
    private Graph graph;
    private CheckboxTableViewer profTable;
    
    class ProfileContentProvider 
    implements IStructuredContentProvider, ILabelProvider, IColorProvider {
        
        public Object[] getElements(Object inputElement) {
            HashMap elements = (HashMap)inputElement;
            return elements.keySet().toArray();
        }

        public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {}
        
        public Image getImage(Object element) {
            return null;
        }

        public String getText(Object element) {
            return (String)element;
        }

        public void addListener(ILabelProviderListener listener) {}
        public boolean isLabelProperty(Object element, String property) {
            return false;
        }
        public void removeListener(ILabelProviderListener listener) {}
        public void dispose() {}

        public Color getForeground(Object element) {
            String id = (String)element;
            return d.getSystemColor(colorsMap.get(id).intValue());
        }

        public Color getBackground(Object element) {
            return null;
        }
    }
    
    public void createPartControl(Composite parent) {
        Composite main = new Composite(parent, SWT.NONE);
        main.setLayout(new GridLayout());
        
        /* This error message appears if the current editor
         * is not an ISAC editor */
        error = new Label(main, SWT.NONE);
        error.setText("This view show profiles of a XIS file. Please edit one.");
        error.setVisible(false);
        
        colorsMap = new HashMap<String,Integer>();
        d = parent.getDisplay();
        /* The view is cut in two part : list of profiles and graph */
        form = new SashForm(main, SWT.HORIZONTAL);
        form.setLayout(new GridLayout());
        form.setLayoutData(new GridData(GridData.FILL_BOTH));
        
        /* List of profile composite */
        Composite profileComp = new Composite(form, SWT.NONE | SWT.BORDER);
        profileComp.setBackground(d.getSystemColor(SWT.COLOR_LIST_BACKGROUND));
        profileComp.setLayout(new GridLayout());
        profileComp.setLayoutData(new GridData(GridData.FILL_BOTH));
        
        Label label = new Label(profileComp, SWT.NONE);
        label.setText("Profiles :");
        label.setBackground(d.getSystemColor(SWT.COLOR_LIST_BACKGROUND));
        
        /* CheckTable with all profiles */
        Table table = new Table(profileComp, SWT.CHECK);
        table.setLayoutData(new GridData(GridData.FILL_BOTH));
        profTable = new CheckboxTableViewer(table);
        
        ProfileContentProvider provider = new ProfileContentProvider();
        profTable.setContentProvider(provider);
        profTable.setLabelProvider(provider);
        profTable.addCheckStateListener(this);
        profTable.addSelectionChangedListener(this);
        
        /* Graph part */
        graph = new Graph(form, SWT.NONE | SWT.BORDER);
        graph.setBackground(d.getSystemColor(SWT.COLOR_WHITE));
        graph.addPaintListener(this);
        
        form.setWeights(new int[]{20,80});
        
        IEditorPart edit = getSite().getPage().getActiveEditor();
        if(edit instanceof IsacEditor){
            Map<String,LoadProfile> profiles = ((IsacEditor)edit).getProfiles();
            setProfiles(profiles);
        }
        
        getSite().getPage().addPartListener(this);
    }
    
    /**
     * Set profiles
     * @param profiles
     */
    public void setProfiles(Map<String,LoadProfile> profiles) {
        color = 0;
        Iterator iter = profiles.keySet().iterator();
        while (iter.hasNext()) {
            String id = (String) iter.next();
            color = (color == colors.length - 1)?0:++color;
            colorsMap.put(id, new Integer(colors[color]));
        }
        profTable.setInput(profiles);
        profTable.setAllChecked(true);
        graph.setProfiles(profiles);
        graph.redraw();
    }
    
    /* Get all checked elements for
     * displaying the checked ones */
    private Object[] getProfilesToDisplay() {
        Object[] ids = profTable.getCheckedElements();
        return ids;
    }

    public void setFocus() {}

    public void dispose() {
        getSite().getPage().removePartListener(this);
        super.dispose();
    }

    public void paintControl(PaintEvent e) {
        Object[] ids = getProfilesToDisplay();
        graph.drawGraph(ids, colorsMap);
    }

    /* Redraw correct graph when some graph are checked (or unchecked)*/
    public void checkStateChanged(CheckStateChangedEvent event) {
        Object[] ids = getProfilesToDisplay();
        graph.redraw();
        graph.drawGraph(ids, colorsMap);
    }

    /* These functions change the display of this view if the
     * current editor is an ISAC editor or not */
    public void partActivated(IWorkbenchPart part) {
        IEditorPart editor = getSite().getPage().getActiveEditor();
        if (editor instanceof IsacEditor) {
            IsacEditor e = (IsacEditor) editor;
            setProfiles(e.getProfiles());
            showError(false);
        }
    }

    public void partBroughtToTop(IWorkbenchPart part) {
        IEditorPart editor = getSite().getPage().getActiveEditor();
        if (editor instanceof IsacEditor) {
            IsacEditor e = (IsacEditor) editor;
            setProfiles(e.getProfiles());
            showError(false);
        }
        else {
            showError(true);
        }
    }

    public void partClosed(IWorkbenchPart part) {
        if(!(part instanceof LoadProfileView)) {
            IEditorPart editor = getSite().getPage().getActiveEditor();
            if(editor == null || !(editor instanceof IsacEditor)) {
                profTable.setInput(null);
                showError(true);
            }
        }
    }

    public void partDeactivated(IWorkbenchPart part) {}

    public void partOpened(IWorkbenchPart part) {
        IEditorPart editor = getSite().getPage().getActiveEditor();
        if (editor instanceof IsacEditor) {
            IsacEditor e = (IsacEditor) editor;
            setProfiles(e.getProfiles());
            showError(false);
        }
        else {
            showError(true);
        }
    }

    public void selectionChanged(SelectionChangedEvent event) {
        IStructuredSelection sel = (IStructuredSelection)event.getSelection();
        graph.setPriority((String)sel.getFirstElement());
        graph.redraw();
        Object[] ids = getProfilesToDisplay();
        graph.drawGraph(ids, colorsMap);
    }

    private void showError(boolean show) {
        form.setVisible(!show);
        error.setVisible(show);
    }
    
}
