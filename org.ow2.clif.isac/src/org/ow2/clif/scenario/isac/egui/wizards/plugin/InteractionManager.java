/*
 * CLIF is a Load Injection Framework Copyright (C) 2006 France Telecom R&D
 * 
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option) any
 * later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 * CLIF $Name$
 * 
 * Contact: clif@ow2.org
 */
package org.ow2.clif.scenario.isac.egui.wizards.plugin;

import java.util.ArrayList;
import java.util.Iterator;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Status;
import org.eclipse.jdt.core.IJavaModel;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.IPackageFragmentRoot;
import org.eclipse.jdt.core.JavaConventions;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jdt.core.JavaModelException;
import org.eclipse.jdt.internal.ui.wizards.NewWizardMessages;
import org.eclipse.jdt.internal.ui.wizards.TypedElementSelectionValidator;
import org.eclipse.jdt.internal.ui.wizards.TypedViewerFilter;
import org.eclipse.jdt.ui.JavaElementLabelProvider;
import org.eclipse.jdt.ui.JavaElementSorter;
import org.eclipse.jdt.ui.StandardJavaElementContentProvider;
import org.eclipse.jface.dialogs.DialogPage;
import org.eclipse.jface.dialogs.ErrorDialog;
import org.eclipse.jface.dialogs.InputDialog;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.resource.ImageRegistry;
import org.eclipse.jface.viewers.ILabelProvider;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerFilter;
import org.eclipse.jface.window.Window;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.List;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Spinner;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.TreeItem;
import org.eclipse.ui.dialogs.ElementTreeSelectionDialog;
import org.eclipse.ui.dialogs.PropertyPage;
import org.jdom.Element;
import org.ow2.clif.util.StringHelper;
import org.ow2.clif.scenario.isac.egui.FileName;
import org.ow2.clif.scenario.isac.egui.Icons;
import org.ow2.clif.scenario.isac.egui.IsacScenarioPlugin;

/**
 * This class permits to manage all interactions between user and model.
 * This class constructs the appropriate property or wizard GUI page.
 * @author Fabrice Rivart
 */
public class InteractionManager {

	ManipulatorManager manipulator;

	private ImageRegistry images;
	private Shell parentShell;
	private IProject project;
	private IJavaProject jProject;
	private String actionType;
	private DialogPage page;
	private boolean validated = false;
	private StringBuffer sbOldPackage = new StringBuffer();

	private String warning = "During this action, all modifications will be applied " +
	"and a rename refactoring might be done in your project.\n" +
	"Do you want to continue ?";

	private List lActions;
	private List lElement;
	private Tree tParams;
	private StyledText tHelp;
	private Text tLabel;
	private Combo cType;
	private Text tText;
	private Spinner sSize;

	private Button bAddElement;
	private Button bDelElement;
	private Button bEditElement;
	private Button bUpElement;
	private Button bDownElement;
	private Button bChoiceTrue;
	private Button bChoiceFalse;
	private Button bAddParam;
	private Button bDelParam;
	private Button bEditParam;
	private Button bUpParam;
	private Button bDownParam;
	private Button bExpand;
	private Button bAddGroupParam;
	private Button bDownAction;
	private Button bUpAction;
	private Button bEditAction;
	private Button bDelAction;
	private Button bAddAction;
	private Button checkbox;

	private Label label;
	private Label type;
	private Label lText;
	private Label lSize;
	private Label lChoice;

	private Text tName;
	private Text tSource;
	private Text tPackage;
	private Text tClass;
	private Text tGuiFile;
	private Text tPluginFile;

	private Composite infoType;

	/**
	 * Constructor.
	 * @param page DialogPage where interactions are done
	 * @param project IProject representing the current project where user does modifications
	 * @param actionType String representing action type to manipulate
	 * (sample, test, timer, control, object, plugin)
	 */
	public InteractionManager(DialogPage page, IProject project, String actionType) {

		this.page = page;
		this.parentShell = page.getShell();
		this.project = project;
		this.actionType = actionType;
	}

	/**
	 * This method initializes graphics
	 */
	public void initContents() {
		manipulator = ManipulatorManager.getInstance();

		try {
			manipulator.init(project, parentShell);

			if (actionType.equals("plugin")) {
				jProject = JavaCore.create(project);
				tSource.setText(manipulator.getSource());
				String pluginName = manipulator.getPluginName();
				sbOldPackage.append(StringHelper.convertJavaIdentifier(pluginName));
				tName.setText(pluginName);
				tPackage.setText(manipulator.getPackageName());
				tClass.setText(manipulator.getClassName());
				tGuiFile.setText(manipulator.getGuiFileName());
				tPluginFile.setText(manipulator.getPluginFileName());
				if (manipulator.getDataProvider().equals("yes")) {
					checkbox.setSelection(true);
				}
				else {
					checkbox.setSelection(false);
				}
			}
			else if (actionType.equals("object")) {
				actionSelected();
			}
			else {
				constructActionList(manipulator.getActions(actionType));
			}
		}
		catch (Exception e) {
			catchException(e);
		}
	}

	/**
	 * This method constructs graphics components.
	 * @param arg0 Composite where contents must be created.
	 */
	public Composite createPluginContents(Composite arg0) {

		Composite page = new Composite(arg0, SWT.NONE);
		page.setLayout(new GridLayout(3, false));
		page.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));

		// Create plugin name propertie
		Label lName = new Label(page, SWT.CENTER);
		lName.setText("Plugin name:");
		tName = new Text(page, SWT.BORDER | SWT.SINGLE);
		tName.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		tName.addModifyListener(new ModifyListener() {
            public void modifyText(ModifyEvent e) {
            	completion();
            	validate();
            	manipulator.setPluginName(tName.getText());
            }
        });

		Label lBlank = new Label(page, SWT.CENTER);
		lBlank.setText("");

		Label lSeparator = new Label(page, SWT.CENTER);
		GridData gridData = new GridData();
		gridData.horizontalSpan = 3;
		gridData.verticalSpan = 2;
		lSeparator.setLayoutData(gridData);

		Label line = new Label(page, SWT.SEPARATOR | SWT.HORIZONTAL);
		gridData = new GridData(GridData.FILL_HORIZONTAL);
		gridData.horizontalSpan = 3;
		line.setLayoutData(gridData);

		lSeparator = new Label(page, SWT.CENTER);
		lSeparator.setLayoutData(gridData);

		// Create source property

		Label lSource = new Label(page, SWT.CENTER);
		lSource.setText("Source:");
		tSource = new Text(page, SWT.BORDER | SWT.SINGLE);
		tSource.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		tSource.addModifyListener(new ModifyListener() {
            public void modifyText(ModifyEvent e) {
                validate();
                manipulator.setSource(tSource.getText());

            }
        });

		Button bSource = new Button(page, SWT.PUSH);
		bSource.setText("Browse...");
		bSource.addSelectionListener(new SelectionListener() {
			public void widgetDefaultSelected(SelectionEvent e) {}

			public void widgetSelected(SelectionEvent e) {

				String source = chooseSourceContainer(project);
				if (!source.equals("")) {
					tSource.setText(source);
				}
				validate();
			}
		});


		// Create package property
		Label lPackage = new Label(page, SWT.CENTER);
		lPackage.setText("Package:");
		tPackage = new Text(page, SWT.BORDER | SWT.SINGLE);
		tPackage.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		tPackage.addModifyListener(new ModifyListener() {
            public void modifyText(ModifyEvent e) {
                validate();
                manipulator.setPackageName(tPackage.getText());
            }
        });
		lBlank = new Label(page, SWT.CENTER);

		// Create class property
		Label lClass = new Label(page, SWT.CENTER);
		lClass.setText("Class name:");
		tClass = new Text(page, SWT.BORDER | SWT.SINGLE);
		tClass.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		tClass.addModifyListener(new ModifyListener() {
            public void modifyText(ModifyEvent e) {
                validate();
                manipulator.setClassName(tClass.getText());
            }
        });
		lBlank = new Label(page, SWT.CENTER);

		checkbox = new Button(page, SWT.CHECK | SWT.LEFT);
		checkbox.setText("Implements DataProvider interface");
		checkbox.addSelectionListener(new SelectionListener() {
			public void widgetDefaultSelected(SelectionEvent e) {}

			public void widgetSelected(SelectionEvent e) {
				if (checkbox.getSelection()) {
					manipulator.setDataProvider("yes");
				}
				else {
					manipulator.setDataProvider("no");
				}
			}
		});


		lSeparator = new Label(page, SWT.CENTER);
		lSeparator.setLayoutData(gridData);

		line = new Label(page, SWT.SEPARATOR | SWT.HORIZONTAL);
		gridData = new GridData(GridData.FILL_HORIZONTAL);
		gridData.horizontalSpan = 3;
		line.setLayoutData(gridData);

		lSeparator = new Label(page, SWT.CENTER);
		lSeparator.setLayoutData(gridData);

		// Create gui file property
		Label lGuiFile = new Label(page, SWT.CENTER);
		lGuiFile.setText("GUI file name:");
		tGuiFile = new Text(page, SWT.BORDER | SWT.SINGLE);
		tGuiFile.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		tGuiFile.addModifyListener(new ModifyListener() {
            public void modifyText(ModifyEvent e) {
                validate();
                manipulator.setGuiFileName(tGuiFile.getText());
            }
        });
		lBlank = new Label(page, SWT.CENTER);

		// Create plugin file property
		Label lPluginFile = new Label(page, SWT.CENTER);
		lPluginFile.setText("Plugin file name:");
		tPluginFile = new Text(page, SWT.BORDER | SWT.SINGLE);
		tPluginFile.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		tPluginFile.addModifyListener(new ModifyListener() {
            public void modifyText(ModifyEvent e) {
                validate();
                manipulator.setPluginFileName(tPluginFile.getText());
            }
        });
		lBlank = new Label(page, SWT.CENTER);

		return page;
	}

	/**
	 * This method constructs graphics components.
	 * @param arg0 Composite where contents must be created.
	 */
	public Composite createActionContents(Composite arg0) {

		Composite page = new Composite(arg0, SWT.NONE);
		page.setLayout(new GridLayout(1, true));
		page.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		arg0.setSize(600, 570);

		images = Icons.getImageRegistry();

		//----------------------------------------------------------------------------ACTIONS
		Group gActions = new Group(page, SWT.NONE);
		gActions.setLayout(new GridLayout(12, true));
		GridData gData = new GridData(GridData.FILL_BOTH);
		gData.verticalSpan = 5;
		gActions.setLayoutData(gData);
		if (!actionType.equals("object")) {
			gActions.setText("Names");
		}
		else {
			gActions.setText("Help");
			gActions.setLayout(new GridLayout());
		}

		if (!actionType.equals("object")) {
			bAddAction = new Button(gActions, SWT.PUSH);
			bAddAction.setImage(images.get(FileName.ADD_ICON));
			bAddAction.setToolTipText("Add name");
			gData = new GridData(GridData.FILL_HORIZONTAL);
			bAddAction.setLayoutData(gData);
			bAddAction.addSelectionListener(new SelectionListener() {
				public void widgetDefaultSelected(SelectionEvent e) {}

				public void widgetSelected(SelectionEvent e) {
					InputDialog id =
						new InputDialog(parentShell, "Add " + actionType,
								"Give the " + actionType + " name", null, null);
					id.open();
					String name = id.getValue();

					while (isActionExist(name)) {
						id = new InputDialog(parentShell, "Add " + actionType,
								"This " + actionType + " name already exists, change the name", name, null);
						id.open();
						name = id.getValue();
					}

					if (name != null && !name.equalsIgnoreCase("")) {
						try {
							manipulator.addAction(actionType, name);
							lActions.add(name);
							lActions.setFocus();
							lActions.setSelection(lActions.getItemCount() - 1);
							actionSelected();
						}
						catch (Exception e1) {
							catchException(e1);
						}
					}
				}
			});
		}

		if (!actionType.equals("object")) {
			lActions = new List(gActions, SWT.BORDER | SWT.SINGLE | SWT.V_SCROLL | SWT.H_SCROLL);
			gData = new GridData(GridData.FILL_BOTH);
			gData.verticalSpan = 5;
			gData.horizontalSpan = 3;
			lActions.setLayoutData(gData);

			tHelp = new StyledText(gActions, SWT.WRAP | SWT.MULTI | SWT.BORDER | SWT.V_SCROLL);
			tHelp.setEditable(false);
			gData = new GridData(GridData.FILL_BOTH);
			gData.verticalSpan = 5;
			gData.horizontalSpan = 8;
			tHelp.setLayoutData(gData);
		}
		else {
			tHelp = new StyledText(gActions, SWT.WRAP | SWT.MULTI | SWT.BORDER | SWT.V_SCROLL);
			tHelp.setEditable(false);
			gData = new GridData(GridData.FILL_BOTH);
			gData.minimumHeight = 150;
			gData.verticalSpan = 5;
			gData.horizontalSpan = 11;
			tHelp.setText("Give "+ actionType + " help.");
			tHelp.setLayoutData(gData);
			
			lActions = new List(gActions, SWT.BORDER | SWT.SINGLE | SWT.V_SCROLL | SWT.H_SCROLL);
			gData = new GridData(GridData.FILL_BOTH);
			gData.verticalSpan = 5;
			lActions.setLayoutData(gData);
			lActions.add("object");
			lActions.setSelection(0);
			lActions.setVisible(false);
		}



		lActions.addSelectionListener(new SelectionListener() {
			public void widgetDefaultSelected(SelectionEvent e) {}

			public void widgetSelected(SelectionEvent e) {
				actionSelected();
			}
		});

		tHelp.addModifyListener(new ModifyListener() {

			public void modifyText(ModifyEvent e) {

				if (lActions.getSelectionCount() > 0) {
					String name = lActions.getSelection()[0];
					try {
						manipulator.setHelp(actionType, name, tHelp.getText());
					}
					catch (Exception e1) {
						catchException(e1);
					}
				}
			}
		});

		if (!actionType.equals("object")) {
			bDelAction = new Button(gActions, SWT.PUSH);
			bDelAction.setImage(images.get(FileName.DELETE_ICON));
			bDelAction.setToolTipText("Delete name");
			bDelAction.setEnabled(false);
			gData = new GridData(GridData.FILL_HORIZONTAL);
			bDelAction.setLayoutData(gData);
			bDelAction.addSelectionListener(new SelectionListener() {
				public void widgetDefaultSelected(SelectionEvent e) {}

				public void widgetSelected(SelectionEvent e) {

					if (lActions.getSelectionCount() > 0) {
						String name = lActions.getSelection()[0];
						int index = lActions.getSelectionIndex();

						boolean confirm = MessageDialog.openConfirm(parentShell, "Confirmation", warning);

						if (confirm) {
							try {
								manipulator.deleteAction(actionType, name);
								lActions.remove(name);
								lActions.setSelection(index - 1);
								actionSelected();
								if (lActions.getItemCount() == 0) {
									tHelp.setText("");
								}
							}
							catch (Exception e1) {
								catchException(e1);
							}
						}
					}
				}
			});

			bEditAction = new Button(gActions, SWT.PUSH);
			bEditAction.setImage(images.get(FileName.RENAME_ICON));
			bEditAction.setToolTipText("Change name");
			bEditAction.setEnabled(false);
			gData = new GridData(GridData.FILL_HORIZONTAL);
			bEditAction.setLayoutData(gData);
			bEditAction.addSelectionListener(new SelectionListener() {
				public void widgetDefaultSelected(SelectionEvent e) {}

				public void widgetSelected(SelectionEvent e) {

					if (lActions.getSelectionCount() > 0) {
						String oldName = lActions.getSelection()[0];

						InputDialog id =
							new InputDialog(parentShell, "Modify " + actionType,
									"Edit the " + actionType + " name", oldName, null);
						id.open();
						String newName = id.getValue();

						while (isActionExist(newName)) {
							id = new InputDialog(parentShell, "Modify " + actionType,
									"This " + actionType + " name already exists, change the name", newName, null);
							id.open();
							newName = id.getValue();
						}

						if (newName != null && !newName.equalsIgnoreCase("")) {
							boolean confirm = MessageDialog.openConfirm(parentShell, "Confirmation", warning);

							if (confirm) {
								try {
									manipulator.updateAction(actionType, oldName, newName);
									lActions.setItem(lActions.getSelectionIndex(), newName);
								}
								catch (Exception e1) {
									catchException(e1);
								}
							}
						}
					}
				}
			});

			bUpAction = new Button(gActions, SWT.ARROW | SWT.UP);
			bUpAction.setToolTipText("Move name up");
			bUpAction.setEnabled(false);
			gData = new GridData(GridData.FILL_HORIZONTAL);
			bUpAction.setLayoutData(gData);
			bUpAction.addSelectionListener(new SelectionListener() {
				public void widgetDefaultSelected(SelectionEvent e) {}

				public void widgetSelected(SelectionEvent e) {

					if (lActions.getSelectionCount() > 0) {
						String name = lActions.getSelection()[0];
						int index = lActions.getSelectionIndex();
						try {
							if (index > 0) {
								manipulator.moveUpAction(actionType, name);
								lActions.remove(index);
								lActions.add(name, index - 1);
								lActions.setSelection(index - 1);
							}
						}
						catch (Exception e1) {
							catchException(e1);
						}
					}
				}
			});

			bDownAction = new Button(gActions, SWT.ARROW | SWT.DOWN);
			bDownAction.setToolTipText("Move name down");
			bDownAction.setEnabled(false);
			gData = new GridData(GridData.FILL_HORIZONTAL | GridData.VERTICAL_ALIGN_BEGINNING);
			bDownAction.setLayoutData(gData);
			bDownAction.addSelectionListener(new SelectionListener() {
				public void widgetDefaultSelected(SelectionEvent e) {}

				public void widgetSelected(SelectionEvent e) {

					if (lActions.getSelectionCount() > 0) {
						String name = lActions.getSelection()[0];
						int index = lActions.getSelectionIndex();
						try {
							if (index < lActions.getItemCount() - 1) {
								manipulator.moveDownAction(actionType, name);
								lActions.remove(index);
								lActions.add(name, index + 1);
								lActions.setSelection(index + 1);
							}
						}
						catch (Exception e1) {
							catchException(e1);
						}
					}
				}
			});
		}


		Label lBlank  = new Label(page, SWT.CENTER);
		gData = new GridData(GridData.HORIZONTAL_ALIGN_FILL);
		lBlank.setLayoutData(gData);


		//----------------------------------------------------------------------------PARAMETERS
		Group gParams = new Group(page, SWT.NONE);
		gParams.setLayout(new GridLayout(12, true));
		gData = new GridData(GridData.FILL_BOTH);
		gData.verticalSpan = 7;
		gParams.setLayoutData(gData);
		gParams.setText("Parameters");

		bAddGroupParam = new Button(gParams, SWT.PUSH);
		bAddGroupParam.setImage(images.get(FileName.ADD_GROUP_ICON));
		bAddGroupParam.setToolTipText("Add group");
		bAddGroupParam.setEnabled(false);
		gData = new GridData(GridData.FILL_HORIZONTAL);
		bAddGroupParam.setLayoutData(gData);
		bAddGroupParam.addSelectionListener(new SelectionListener() {
			public void widgetDefaultSelected(SelectionEvent e) {}

			public void widgetSelected(SelectionEvent e) {

				if (lActions.getSelectionCount() > 0 && tParams.getSelectionCount() > 0) {

					String actionName = lActions.getSelection()[0];
					TreeItem parent = tParams.getSelection()[0];
					String parentPath = (String) parent.getData("guixpath");

					InputDialog id = new InputDialog(parentShell, "Add group",
							"Give the group name", null, null);
					id.open();
					String groupName = id.getValue();

					try {
						while (isGroupExist(parent, groupName)) {
							id = new InputDialog(parentShell, "Modify parameter",
									"This parameter name already exists, change the name", groupName, null);
							id.open();
							groupName = id.getValue();
						}
					}
					catch (Exception e1) {
						catchException(e1);
					}

					if (groupName != null && !groupName.equalsIgnoreCase("")) {
						addGroup(parent, groupName);
						parent.setExpanded(true);

						try {
							manipulator.addGroup(actionType, actionName, parentPath, groupName);
						}
						catch (Exception e1) {
							catchException(e1);
						}
					}
				}
			}
		});

		tParams = new Tree(gParams, SWT.BORDER | SWT.SINGLE | SWT.V_SCROLL | SWT.H_SCROLL);
		gData = new GridData(GridData.FILL_BOTH);
		gData.verticalSpan = 7;
		gData.horizontalSpan = 4;
		tParams.setLayoutData(gData);
		tParams.addSelectionListener(new SelectionListener() {
			public void widgetDefaultSelected(SelectionEvent e) {}

			public void widgetSelected(SelectionEvent e) {
				parameterSelected();
			}
        });

		//----------------------------------------------------------------------------GUI INFO
		Composite info = new Composite(gParams, SWT.BORDER);
		gData = new GridData(GridData.FILL_BOTH);
		gData.verticalSpan = 7;
		gData.horizontalSpan = 7;
		info.setLayoutData(gData);
		info.setLayout(new GridLayout(2, false));

		label = new Label(info, SWT.CENTER);
		label.setText("Label:");
		tLabel = new Text(info, SWT.BORDER | SWT.SINGLE);
		tLabel.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		tLabel.addModifyListener(new ModifyListener() {
            public void modifyText(ModifyEvent e) {
            	if (lActions.getSelectionCount() > 0 && tParams.getSelectionCount() > 0) {
            		String actionName = lActions.getSelection()[0];
					TreeItem param = tParams.getSelection()[0];

					String guiElementPath = (String) param.getData("guixpath");
					String label = tLabel.getText();
					try {
						manipulator.setParameterLabel(actionType, actionName, guiElementPath, label);
					}
					catch (Exception e1) {
						catchException(e1);
					}
            	}
            }
        });

		type = new Label(info, SWT.CENTER);
		type.setText("Type:");
		cType = new Combo(info, SWT.READ_ONLY);
		cType.setItems (new String [] {"field", "nfield", "table", "radiobutton", "checkbox", "combo"});
		cType.select(0);
		cType.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		cType.addSelectionListener(new SelectionListener() {
			public void widgetDefaultSelected(SelectionEvent e) {}

			public void widgetSelected(SelectionEvent e) {
				if (lActions.getSelectionCount() > 0 && tParams.getSelectionCount() > 0) {
            		String actionName = lActions.getSelection()[0];
					TreeItem param = tParams.getSelection()[0];

					String guiElementPath = (String) param.getData("guixpath");
					String type = cType.getText();
					lElement.removeAll();
					choiceSelected();
					try {
						manipulator.setParameterType(actionType, actionName, guiElementPath, type);
						refreshGuiInfoType(type);
					}
					catch (Exception e1) {
						catchException(e1);
					}
            	}
			}
        });

		infoType = new Composite(info, SWT.BORDER);
		gData = new GridData(GridData.FILL_BOTH);
		gData.horizontalSpan = 2;
		infoType.setLayoutData(gData);
		infoType.setLayout(new GridLayout(4, false));

		lText = new Label(infoType, SWT.CENTER);
		lText.setText("Text:");
		tText = new Text(infoType, SWT.BORDER | SWT.SINGLE);
		tText.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		tText.addModifyListener(new ModifyListener() {
            public void modifyText(ModifyEvent e) {
            	if (lActions.getSelectionCount() > 0 && tParams.getSelectionCount() > 0) {
            		String actionName = lActions.getSelection()[0];
					TreeItem param = tParams.getSelection()[0];

					String guiElementPath = (String) param.getData("guixpath");
					String text = tText.getText();
					try {
						manipulator.setParameterFieldText(actionType, actionName, guiElementPath, text);
					}
					catch (Exception e1) {
						catchException(e1);
					}
            	}
            }
        });

		lSize = new Label(infoType, SWT.CENTER);
		lSize.setText("Length:");
		sSize = new Spinner(infoType, SWT.BORDER);
		sSize.setMinimum(8);
		sSize.setMaximum(24);
		sSize.addSelectionListener(new SelectionListener() {
			public void widgetDefaultSelected(SelectionEvent e) {}

			public void widgetSelected(SelectionEvent e) {
				if (lActions.getSelectionCount() > 0 && tParams.getSelectionCount() > 0) {
            		String actionName = lActions.getSelection()[0];
					TreeItem param = tParams.getSelection()[0];

					String guiElementPath = (String) param.getData("guixpath");
					String size = String.valueOf(sSize.getSelection());
					try {
						manipulator.setParameterFieldSize(actionType, actionName, guiElementPath, size);
					}
					catch (Exception e1) {
						catchException(e1);
					}
            	}
			}
        });

		bAddElement = new Button(infoType, SWT.PUSH);
		bAddElement.setImage(images.get(FileName.ADD_ICON));
		bAddElement.addSelectionListener(new SelectionListener() {
			public void widgetDefaultSelected(SelectionEvent e) {}

			public void widgetSelected(SelectionEvent e) {
				if (lActions.getSelectionCount() > 0 && tParams.getSelectionCount() > 0) {

					String actionName = lActions.getSelection()[0];
					TreeItem param = tParams.getSelection()[0];
					String guiPath = (String) param.getData("guixpath");
					String type = cType.getText();
					String title;
					String message;
					String exist;
					if (type.equalsIgnoreCase("table")) {
						title = "Add column";
						message = "Give the column name";
						exist = "This column name already exists, change the name";
					}
					else {
						title = "Add choice";
						message = "Give the choice value";
						exist = "This choice value already exists, change the value";
					}

					InputDialog id = new InputDialog(parentShell, title, message, null, null);
					id.open();
					String elementName = id.getValue();

					while (isElementExist(elementName)) {
						id = new InputDialog(parentShell, title, exist, elementName, null);
						id.open();
						elementName = id.getValue();
					}

					if (elementName != null && !elementName.equalsIgnoreCase("")) {
						try {
							manipulator.addParameterElement(actionType, actionName, guiPath,
									type, elementName);
							lElement.add(elementName);
							lElement.setFocus();
							lElement.setSelection(lElement.getItemCount() - 1);
							choiceSelected();
						}
						catch (Exception e1) {
							catchException(e1);
						}
					}
            	}

			}
		});

		lElement = new List(infoType, SWT.BORDER | SWT.SINGLE | SWT.V_SCROLL | SWT.H_SCROLL);
		gData = new GridData(GridData.FILL_BOTH);
		gData.verticalSpan = 5;
		gData.horizontalSpan = 2;
		lElement.setLayoutData(gData);
		lElement.addSelectionListener(new SelectionListener() {
			public void widgetDefaultSelected(SelectionEvent e) {}

			public void widgetSelected(SelectionEvent e) {
				choiceSelected();
			}
		});

		lChoice = new Label(infoType, SWT.CENTER);
		lChoice.setText("Default state:");


		bDelElement = new Button(infoType, SWT.PUSH);
		bDelElement.setImage(images.get(FileName.DELETE_ICON));
		bDelElement.addSelectionListener(new SelectionListener() {
			public void widgetDefaultSelected(SelectionEvent e) {}

			public void widgetSelected(SelectionEvent e) {
				if (lActions.getSelectionCount() > 0 &&
					tParams.getSelectionCount() > 0 &&
					lElement.getSelectionCount() > 0) {

					String actionName = lActions.getSelection()[0];
					TreeItem param = tParams.getSelection()[0];
					String guiPath = (String) param.getData("guixpath");
					String type = cType.getText();
					String elementName = lElement.getSelection()[0];

					if (elementName != null && !elementName.equalsIgnoreCase("")) {
						try {
							manipulator.deleteParameterElement(actionType, actionName, guiPath,
									type, elementName);
							lElement.setSelection(lElement.getSelectionIndex() - 1);
							lElement.remove(elementName);
							choiceSelected();
						}
						catch (Exception e1) {
							catchException(e1);
						}
					}
            	}
			}
		});

		bChoiceTrue = new Button(infoType, SWT.RADIO);
		bChoiceTrue.setText("enabled");
		bChoiceTrue.addSelectionListener(new SelectionListener() {
			public void widgetDefaultSelected(SelectionEvent e) {}

			public void widgetSelected(SelectionEvent e) {
				if (lActions.getSelectionCount() > 0 &&
					tParams.getSelectionCount() > 0 &&
					lElement.getSelectionCount() > 0) {

					String actionName = lActions.getSelection()[0];
					TreeItem param = tParams.getSelection()[0];
					String guiPath = (String) param.getData("guixpath");
					String type = cType.getText();
					String elementName = lElement.getSelection()[0];

					try {
						if (type.equals("radiobutton") || type.equals("combo")) {
							String [] elements = lElement.getItems();
							for (int i = 0; i < elements.length; i++) {
								manipulator.setParameterElementDefault(actionType, actionName, guiPath,
										type, elements[i], "false");
							}
						}

						manipulator.setParameterElementDefault(actionType, actionName, guiPath,
								type, elementName, "true");

					}
					catch (Exception e1) {
						catchException(e1);
					}
	            }
			}
		});

		bEditElement = new Button(infoType, SWT.PUSH);
		bEditElement.setImage(images.get(FileName.RENAME_ICON));
		bEditElement.addSelectionListener(new SelectionListener() {
			public void widgetDefaultSelected(SelectionEvent e) {}

			public void widgetSelected(SelectionEvent e) {
				if (lActions.getSelectionCount() > 0 &&
					tParams.getSelectionCount() > 0 &&
					lElement.getSelectionCount() > 0) {

					String actionName = lActions.getSelection()[0];
					TreeItem param = tParams.getSelection()[0];
					String guiPath = (String) param.getData("guixpath");
					String type = cType.getText();
					String oldElementName = lElement.getSelection()[0];

					String title;
					String message;
					String exist;
					if (type.equalsIgnoreCase("table")) {
						title = "Modify column";
						message = "Edit the column name";
						exist = "This column name already exists, change the name";
					}
					else {
						title = "Modify choice";
						message = "Edit the choice value";
						exist = "This choice value already exists, change the value";
					}

					InputDialog id = new InputDialog(parentShell, title, message, oldElementName, null);
					id.open();
					String newElementName = id.getValue();

					while (isElementExist(newElementName)) {
						id = new InputDialog(parentShell, title, exist, newElementName, null);
						id.open();
						newElementName = id.getValue();
					}

					if (newElementName != null && !newElementName.equalsIgnoreCase("")) {
						try {
							manipulator.updateParameterElement(actionType, actionName, guiPath,
									type, oldElementName, newElementName);
							lElement.setItem(lElement.getSelectionIndex(), newElementName);
						}
						catch (Exception e1) {
							catchException(e1);
						}
					}
	            }
			}
		});


		bChoiceFalse = new Button(infoType, SWT.RADIO);
		bChoiceFalse.setText("disabled");
		bChoiceFalse.setSelection(true);
		bChoiceFalse.addSelectionListener(new SelectionListener() {
			public void widgetDefaultSelected(SelectionEvent e) {}

			public void widgetSelected(SelectionEvent e) {
				if (lActions.getSelectionCount() > 0 &&
					tParams.getSelectionCount() > 0 &&
					lElement.getSelectionCount() > 0) {

					String actionName = lActions.getSelection()[0];
					TreeItem param = tParams.getSelection()[0];
					String guiPath = (String) param.getData("guixpath");
					String type = cType.getText();
					String elementName = lElement.getSelection()[0];

					try {
						manipulator.setParameterElementDefault(actionType, actionName, guiPath,
								type, elementName, "false");
					}
					catch (Exception e1) {
						catchException(e1);
					}
	            }
			}
		});

		bUpElement = new Button(infoType, SWT.ARROW | SWT.UP);
		bUpElement.addSelectionListener(new SelectionListener() {
			public void widgetDefaultSelected(SelectionEvent e) {}

			public void widgetSelected(SelectionEvent e) {
				if (lActions.getSelectionCount() > 0 &&
					tParams.getSelectionCount() > 0 &&
					lElement.getSelectionCount() > 0) {

					String actionName = lActions.getSelection()[0];
					TreeItem param = tParams.getSelection()[0];
					String guiPath = (String) param.getData("guixpath");
					String type = cType.getText();
					String elementName = lElement.getSelection()[0];

					int index = lElement.getSelectionIndex();
					try {
						if (index > 0) {
							manipulator.moveUpParameterElement(actionType, actionName, guiPath,
															type, elementName);
							lElement.remove(index);
							lElement.add(elementName, index - 1);
							lElement.setSelection(index - 1);
						}
					}
					catch (Exception e1) {
						catchException(e1);
					}
				}
			}
		});

		lBlank = new Label(infoType, SWT.CENTER);
		lBlank.setText("");

		bDownElement = new Button(infoType, SWT.ARROW | SWT.DOWN);
		gData = new GridData(GridData.VERTICAL_ALIGN_BEGINNING);
		bDownElement.setLayoutData(gData);
		bDownElement.addSelectionListener(new SelectionListener() {
			public void widgetDefaultSelected(SelectionEvent e) {}

			public void widgetSelected(SelectionEvent e) {
				if (lActions.getSelectionCount() > 0 &&
					tParams.getSelectionCount() > 0 &&
					lElement.getSelectionCount() > 0) {

					String actionName = lActions.getSelection()[0];
					TreeItem param = tParams.getSelection()[0];
					String guiPath = (String) param.getData("guixpath");
					String type = cType.getText();
					String elementName = lElement.getSelection()[0];

					int index = lElement.getSelectionIndex();
					try {
						if (index < lElement.getItemCount() - 1) {
							manipulator.moveDownParameterElement(actionType, actionName, guiPath,
																type, elementName);
							lElement.remove(index);
							lElement.add(elementName, index + 1);
							lElement.setSelection(index + 1);
						}
					}
					catch (Exception e1) {
						catchException(e1);
					}
				}
			}
		});

		lBlank = new Label(infoType, SWT.CENTER);
		lBlank.setText("");



		//----------------------------------------------------------------------------END GUI INFO
		bAddParam = new Button(gParams, SWT.PUSH);
		bAddParam.setImage(images.get(FileName.ADD_PARAM_ICON));
		bAddParam.setToolTipText("Add parameter");
		bAddParam.setEnabled(false);
		gData = new GridData(GridData.FILL_HORIZONTAL);
		bAddParam.setLayoutData(gData);
		bAddParam.addSelectionListener(new SelectionListener() {
			public void widgetDefaultSelected(SelectionEvent e) {}

			public void widgetSelected(SelectionEvent e) {

				if (lActions.getSelectionCount() > 0 && tParams.getSelectionCount() > 0) {

					String actionName = lActions.getSelection()[0];
					TreeItem parent = tParams.getSelection()[0];
					String parentPath = (String) parent.getData("guixpath");


					InputDialog id = new InputDialog(parentShell, "Add parameter",
									"Give the parameter name", null, null);
					id.open();
					String newName = id.getValue();

					try {
						while (isParamExist(actionName, "/params/param[@name='" + newName + "']")) {
							id = new InputDialog(parentShell, "Modify parameter",
									"This parameter name already exists, change the name", newName, null);
							id.open();
							newName = id.getValue();
						}
					}
					catch (Exception e1) {
						catchException(e1);
					}

					if (newName != null && !newName.equalsIgnoreCase("")) {
						addParam(parent, newName);
						parent.setExpanded(true);

						try {
							manipulator.addParameter(actionType, actionName, parentPath, "/params", newName);
						}
						catch (Exception e2) {
							catchException(e2);
						}
					}
				}
			}
		});

		bDelParam = new Button(gParams, SWT.PUSH);
		bDelParam.setImage(images.get(FileName.DELETE_ICON));
		bDelParam.setToolTipText("Delete element");
		bDelParam.setEnabled(false);
		gData = new GridData(GridData.FILL_HORIZONTAL);
		bDelParam.setLayoutData(gData);
		bDelParam.addSelectionListener(new SelectionListener() {
			public void widgetDefaultSelected(SelectionEvent e) {}

			public void widgetSelected(SelectionEvent e) {

				if (lActions.getSelectionCount() > 0 && tParams.getSelectionCount() > 0) {

					String actionName = lActions.getSelection()[0];
					TreeItem item = tParams.getSelection()[0];
					TreeItem parent = item.getParentItem();

					boolean confirm = MessageDialog.openConfirm(parentShell, "Confirmation", warning);

					if (confirm) {

						deleteAll(actionName, item);
						tParams.redraw();
						if (parent == null) {
							tParams.setSelection(new TreeItem []{tParams.getItem(0)});
						}
						else {
							tParams.setSelection(new TreeItem []{parent});
						}
						parameterSelected();
					}
				}
			}
		});

		bEditParam = new Button(gParams, SWT.PUSH);
		bEditParam.setImage(images.get(FileName.RENAME_ICON));
		bEditParam.setToolTipText("Change element name");
		bEditParam.setEnabled(false);
		gData = new GridData(GridData.FILL_HORIZONTAL);
		bEditParam.setLayoutData(gData);
		bEditParam.addSelectionListener(new SelectionListener() {
			public void widgetDefaultSelected(SelectionEvent e) {}

			public void widgetSelected(SelectionEvent e) {

				if (lActions.getSelectionCount() > 0  && tParams.getSelectionCount() > 0) {

					String actionName = lActions.getSelection()[0];
					TreeItem param = tParams.getSelection()[0];

					String guiElementPath = (String) param.getData("guixpath");
					String pluginElementPath = (String) param.getData("pluginxpath");
					String oldName = param.getText();
					String type = (String) param.getData("type");

					try {
						InputDialog id =
							new InputDialog(parentShell, "Modify element",
									"Edit the element name", oldName, null);
						id.open();
						String newName = id.getValue();

						while (isParamExist(actionName, "/params/param[@name='" + newName + "']")  ||
							   isGroupExist(param.getParentItem(), newName)) {
							id = new InputDialog(parentShell, "Modify element",
									"This element name already exists, change the name", newName, null);
							id.open();
							newName = id.getValue();
						}

						boolean confirm = true;

						if (type.equals("param") && newName != null) {

							confirm = MessageDialog.openConfirm(parentShell, "Confirmation", warning);
						}

						if (confirm && newName != null && !newName.equalsIgnoreCase("")) {
							replaceAllName(param, oldName, newName);
							param.setText(newName);
							manipulator.updateElementName(actionType, actionName, guiElementPath,
									pluginElementPath, oldName, newName, type);

						}
					}
					catch (Exception e1) {
						catchException(e1);
					}
				}
			}
		});

		bUpParam = new Button(gParams, SWT.ARROW | SWT.UP);
		bUpParam.setToolTipText("Move element up");
		gData = new GridData(GridData.FILL_HORIZONTAL);
		bUpParam.setLayoutData(gData);
		bUpParam.setEnabled(false);
		bUpParam.addSelectionListener(new SelectionListener() {
			public void widgetDefaultSelected(SelectionEvent e) {}

			public void widgetSelected(SelectionEvent e) {

				if (lActions.getSelectionCount() > 0  && tParams.getSelectionCount() > 0) {

					String actionName = lActions.getSelection()[0];
					TreeItem param = tParams.getSelection()[0];

					String guiParentPath = (String) param.getParentItem().getData("guixpath");
					String guiElementPath = (String) param.getData("guixpath");

					int index = param.getParentItem().indexOf(param);

					try {
						if (index > 0) {
							manipulator.moveUpParameter(actionType, actionName,
													guiParentPath, guiElementPath);

							if (tParams.getItemCount() > 0 && tParams.getItem(0) != null) {
								tParams.getItem(0).dispose();
							}

							TreeItem item = new TreeItem(tParams, SWT.NONE);
							item.setText("- root -");
							item.setData("guixpath", "/params");
							item.setData("pluginxpath", "/params");
							item.setData("type", "root");

							constructTree(manipulator.getParams(actionType, actionName), item);
							expandAll(item);
							TreeItem selection = findItem(item, guiElementPath);
							tParams.setSelection(new TreeItem []{selection});
							parameterSelected();
						}
					}
					catch (Exception e1) {
						catchException(e1);
					}
				}
			}
		});

		bDownParam = new Button(gParams, SWT.ARROW | SWT.DOWN);
		bDownParam.setToolTipText("Move element down");
		gData = new GridData(GridData.FILL_HORIZONTAL);
		bDownParam.setLayoutData(gData);
		bDownParam.setEnabled(false);
		bDownParam.addSelectionListener(new SelectionListener() {
			public void widgetDefaultSelected(SelectionEvent e) {}

			public void widgetSelected(SelectionEvent e) {

				if (lActions.getSelectionCount() > 0  && tParams.getSelectionCount() > 0) {

					String actionName = lActions.getSelection()[0];
					TreeItem param = tParams.getSelection()[0];

					String guiParentPath = (String) param.getParentItem().getData("guixpath");
					String guiElementPath = (String) param.getData("guixpath");

					int index = param.getParentItem().indexOf(param);
					int count = param.getParentItem().getItemCount();

					try {
						if (index < (count - 1)) {
							manipulator.moveDownParameter(actionType, actionName,
														guiParentPath, guiElementPath);

							if (tParams.getItemCount() > 0 && tParams.getItem(0) != null) {
								tParams.getItem(0).dispose();
							}

							TreeItem item = new TreeItem(tParams, SWT.NONE);
							item.setText("- root -");
							item.setData("guixpath", "/params");
							item.setData("pluginxpath", "/params");
							item.setData("type", "root");

							constructTree(manipulator.getParams(actionType, actionName), item);
							expandAll(item);
							TreeItem selection = findItem(item, guiElementPath);
							tParams.setSelection(new TreeItem []{selection});
							parameterSelected();
						}
					}
					catch (Exception e1) {
						catchException(e1);
					}
				}
			}
		});

		bExpand = new Button(gParams, SWT.PUSH);
		bExpand.setToolTipText("Expand element");
		bExpand.setEnabled(false);
		gData = new GridData(GridData.FILL_HORIZONTAL | GridData.VERTICAL_ALIGN_BEGINNING);
		bExpand.setLayoutData(gData);
		bExpand.setImage(images.get(FileName.EXPAND_ICON));
		bExpand.addSelectionListener(new SelectionListener() {
			public void widgetDefaultSelected(SelectionEvent e) {}

			public void widgetSelected(SelectionEvent e) {

				if (lActions.getSelectionCount() > 0 && tParams.getSelectionCount() > 0) {

					TreeItem item = tParams.getSelection()[0];
					expandAll(item);
				}
			}
		});


		refreshGuiInfo("root");
		gActions.pack();
		page.pack();
		arg0.pack();
		return page;
	}

	/**
	 * Recursive method which finds element with guixpath
	 * @param item TreeItem where start search
	 * @param path String Xpath expression
	 */
	private TreeItem findItem(TreeItem item, String path) {

		TreeItem result = null;
		String itemPath = (String) item.getData("guixpath");


		if (itemPath.equals(path)) {
			return item;
		}
		else {
			TreeItem [] children = item.getItems();
			for (int i = 0; i < children.length; i++) {
				result = findItem(children[i], path);
				if (result != null) {
					return result;
				}
			}
		}

		return result;
	}

	/**
	 * Recursive method which replaces element name in XPath expression
	 * @param item TreeItem to replace name
	 * @param oldName String old element name
	 * @param newName String new element name
	 */
	private void replaceAllName(TreeItem item, String oldName, String newName) {

		String newGuiPath = ((String) item.getData("guixpath"))
							.replaceAll("@name='" + oldName + "'", "@name='" + newName + "'");
		String newPluginPath = ((String) item.getData("pluginxpath"))
							.replaceAll("@name='" + oldName + "'", "@name='" + newName + "'");
		item.setData("guixpath", newGuiPath);
		item.setData("pluginxpath", newPluginPath);

		TreeItem [] children = item.getItems();
		for (int i = 0; i < children.length; i++) {
			replaceAllName(children[i], oldName, newName);
		}
	}


	/**
	 * Recursive method which expands item and children.
	 * @param item TreeItem to expand
	 */
	private void expandAll(TreeItem item) {

		item.setExpanded(true);
		TreeItem [] children = item.getItems();
		for (int i = 0; i < children.length; i++) {
			expandAll(children[i]);
		}
	}

	/**
	 * Recursive method which deletes item and children;
	 * @param actionName String representing action name
	 * @param item TreeItem to expand
	 */
	private void deleteAll(String actionName, TreeItem item) {

		try {
			TreeItem [] children = item.getItems();
			for (int i = 0; i < children.length; i++) {
				deleteAll(actionName, children[i]);
			}

			if (!((String) item.getData("type")).equalsIgnoreCase("root")) {
				TreeItem parent = item.getParentItem();
				String guiParentPath = (String) parent.getData("guixpath");
				String pluginParentPath = "/params";
				String guiElementPath = (String) item.getData("guixpath");
				String pluginElementPath = (String) item.getData("pluginxpath");
				manipulator.deleteElement(actionType, actionName, guiParentPath,
						pluginParentPath, guiElementPath, pluginElementPath,
						item.getText(), (String) item.getData("type"));
				item.dispose();
			}
		}
		catch (Exception e1) {
			catchException(e1);
		}
	}

	/**
	 * This method refresh page when an action is selected.
	 */
	private void actionSelected() {
		if (lActions.getSelectionCount() > 0) {
			String name = lActions.getSelection()[0];
			tHelp.setEditable(true);
			try {
				String help = manipulator.getHelp(actionType, name);
				if (help.equalsIgnoreCase("")) {
					tHelp.setText("Give "+ actionType + " help.");
				}
				else {
					tHelp.setText(help);
				}

				if (tParams.getItemCount() > 0 && tParams.getItem(0) != null) {
					tParams.getItem(0).dispose();
				}

				TreeItem item = new TreeItem(tParams, SWT.NONE);
				item.setText("- root -");
				item.setData("guixpath", "/params");
				item.setData("pluginxpath", "/params");
				item.setData("type", "root");

				constructTree(manipulator.getParams(actionType, name), item);
				expandAll(item);
				tParams.setSelection(new TreeItem [] {item});
				parameterSelected();
				tParams.redraw();

				if (!actionType.equals("object")) {
					refreshGuiActionButtons(true);
				}
			}
			catch (Exception e1) {
				catchException(e1);
			}
		}
		else {
			if (tParams.getItemCount() > 0 && tParams.getItem(0) != null) {
				tParams.getItem(0).dispose();
			}
			refreshGuiActionButtons(false);
			parameterSelected();
		}
	}

	/**
	 * This method refresh page when a parameter is selected.
	 */
	private void parameterSelected() {
		if (lActions.getSelectionCount() > 0 && tParams.getSelectionCount() > 0) {

			String actionName = lActions.getSelection()[0];
			TreeItem param = tParams.getSelection()[0];
			String guiPath = (String) param.getData("guixpath");
			String paramType =(String) param.getData("type");

			// Refresh information
			refreshGuiInfo(paramType);
			refreshGuiParameterButtons(paramType);

			try {
				if (paramType.equalsIgnoreCase("param")) {
					tLabel.setText(manipulator.getParameterLabel(actionType, actionName, guiPath));
					String type = manipulator.getParameterType(actionType, actionName, guiPath);
					cType.setText(type);
					refreshGuiInfoType(type);
					if (type.equalsIgnoreCase("field"))
					{
						String text = manipulator.getParameterFieldText(actionType, actionName, guiPath);
						if (text != null)
						{
							tText.setText(text);
						}
						String size = manipulator.getParameterFieldSize(actionType, actionName, guiPath);
						if (size != null)
						{
							sSize.setSelection(Integer.valueOf(size));
						}
					}
					else if (type.equalsIgnoreCase("table") ||
							 type.equalsIgnoreCase("radiobutton") ||
							 type.equalsIgnoreCase("checkbox") ||
							 type.equalsIgnoreCase("combo")) {
						lElement.removeAll();
						constructElementList(
								manipulator.getParameterElements(actionType, actionName, guiPath, type));
					}
				}
			}
			catch (Exception e1) {
				catchException(e1);
			}

		}
		else {
			refreshGuiParameterButtons("nothing");
		}
	}

	/**
	 * This method refresh page when a parameter choice is selected.
	 */
	private void choiceSelected() {
		if (lActions.getSelectionCount() > 0 &&
			tParams.getSelectionCount() > 0 &&
			lElement.getSelectionCount() > 0) {

			String elementName = lElement.getSelection()[0];
			String actionName = lActions.getSelection()[0];
			TreeItem param = tParams.getSelection()[0];
			String guiPath = (String) param.getData("guixpath");
			String type = cType.getText();

			refreshGuiElementButtons(true);

			try {
				if (!type.equals("table")) {
					String value = manipulator.getParameterElementDefault(actionType, actionName, guiPath,
																		type, elementName);
					if (value.equalsIgnoreCase("true")) {
						bChoiceTrue.setSelection(true);
						bChoiceFalse.setSelection(false);
					}
					else {
						bChoiceFalse.setSelection(true);
						bChoiceTrue.setSelection(false);
					}
				}
			}
			catch (Exception e1) {
				catchException(e1);
			}
		}
		else {
			refreshGuiElementButtons(false);
		}
	}

	/**
	 * Recursive method which constructs tree parameters with a root element.
	 * @param root Element representing the params gui element in gui file
	 * @param parent TreeItem representing the parent in tree
	 */
	private void constructTree(Element element, TreeItem parent)
	{
		java.util.List children = null;
		if (element != null && (children = element.getChildren()) != null)
		{
			Iterator itr = children.iterator();
			while(itr.hasNext()) {
				Element child = (Element)itr.next();
				TreeItem item = null;
				if (child.getName().equalsIgnoreCase("group")) {
					item = addGroup(parent, child.getAttributeValue("name"));
				}
				else if (child.getName().equalsIgnoreCase("param")) {
					item = addParam(parent, child.getAttributeValue("name"));
				}
				constructTree(child, item);
			}
		}
	}

	/**
	 * This method constructs action list.
	 * @param elements ArrayList with string action
	 */
	private void constructActionList(ArrayList<String> elements) {

		Iterator itr = elements.iterator();
		while(itr.hasNext()) {
			String element = (String) itr.next();
			lActions.add(element);
		}
		lActions.setSelection(0);
		actionSelected();
	}

	/**
	 * This method constructs parameter element list.
	 * @param elements ArrayList with string parameter element
	 */
	private void constructElementList(ArrayList<String> elements) {

		Iterator itr = elements.iterator();
		while(itr.hasNext()) {
			String element = (String) itr.next();
			lElement.add(element);
		}
		lElement.setSelection(0);
		choiceSelected();

	}

	/**
	 * Returns a TreeItem representing the node added to parent.
	 * @param parent TreeItem representing the parent of new node
	 * @param groupName String representing group name
	 * @return TreeItem representing the node added.
	 */
	private TreeItem addGroup(TreeItem parent, String groupName) {
		TreeItem child = new TreeItem(parent, SWT.NONE);
		String parentPath = (String) parent.getData("guixpath");
		String childPath = parentPath + "/group[@name='" + groupName + "']";
		child.setText(groupName);
		child.setData("type", "group");
		child.setData("guixpath", childPath);
		child.setData("pluginxpath", "/params/group");
		child.setImage(images.get(FileName.GROUP_ICON));
		return child;
	}

	/**
	 * Returns a TreeItem representing the node added to parent.
	 * @param parent TreeItem representing the parent of new node
	 * @param groupName String representing parameter name
	 * @return TreeItem representing the node added.
	 */
	private TreeItem addParam(TreeItem parent, String paramName) {
		TreeItem child = new TreeItem(parent, SWT.NONE);
		String parentPath = (String) parent.getData("guixpath");
		String guiPath = parentPath + "/param[@name='" + paramName + "']";
		String pluginPath = "/params/param[@name='" + paramName + "']";
		child.setText(paramName);
		child.setData("type", "param");
		child.setData("guixpath", guiPath);
		child.setData("pluginxpath", pluginPath);
		child.setImage(images.get(FileName.PARAM_ICON));
		return child;
	}

	/**
	 * Refresh graphic elements (action buttons).
	 * @param enable boolean true if action buttons must be enable else false
	 */
	private void refreshGuiActionButtons(boolean enable) {
		bDelAction.setEnabled(enable);
		bEditAction.setEnabled(enable);
		bUpAction.setEnabled(enable);
		bDownAction.setEnabled(enable);
	}

	/**
	 * Refresh graphic elements (parameter buttons).
	 * @param type String representing element type (nothing, root, group or param)
	 */
	private void refreshGuiParameterButtons(String type) {

		if (type.equalsIgnoreCase("nothing")) {
			bAddGroupParam.setEnabled(false);
			bAddParam.setEnabled(false);
			bDelParam.setEnabled(false);
			bUpParam.setEnabled(false);
			bDownParam.setEnabled(false);
			bExpand.setEnabled(false);
			bEditParam.setEnabled(false);
		}
		else if (type.equalsIgnoreCase("root")) {
			bAddGroupParam.setEnabled(true);
			bAddParam.setEnabled(true);
			bDelParam.setEnabled(true);
			bEditParam.setEnabled(false);
			bUpParam.setEnabled(false);
			bDownParam.setEnabled(false);
			bExpand.setEnabled(true);
		}
		else if (type.equalsIgnoreCase("group")) {
			bAddGroupParam.setEnabled(true);
			bAddParam.setEnabled(true);
			bDelParam.setEnabled(true);
			bEditParam.setEnabled(true);
			bUpParam.setEnabled(true);
			bDownParam.setEnabled(true);
			bExpand.setEnabled(true);
		}
		else if (type.equalsIgnoreCase("param")) {
			bAddGroupParam.setEnabled(false);
			bAddParam.setEnabled(false);
			bDelParam.setEnabled(true);
			bEditParam.setEnabled(true);
			bUpParam.setEnabled(true);
			bDownParam.setEnabled(true);
			bExpand.setEnabled(false);
		}
	}

	/**
	 * Refresh graphic elements (parameter element buttons).
	 * @param enable boolean true if parameter element buttons must be enable else false
	 */
	private void refreshGuiElementButtons(boolean enable) {
		bDelElement.setEnabled(enable);
		bEditElement.setEnabled(enable);
		bUpElement.setEnabled(enable);
		bDownElement.setEnabled(enable);
		bChoiceTrue.setEnabled(enable);
		bChoiceFalse.setEnabled(enable);
	}

	/**
	 * Refresh graphic elements (parameter information).
	 * @param type String representing element type (root, group or param)
	 */
	private void refreshGuiInfo(String type) {

		if (type.equalsIgnoreCase("root")) {
			label.setVisible(false);
			tLabel.setVisible(false);
			this.type.setVisible(false);
			cType.setVisible(false);
			infoType.setVisible(false);
		}
		else if (type.equalsIgnoreCase("group")) {
			label.setVisible(false);
			tLabel.setVisible(false);
			this.type.setVisible(false);
			cType.setVisible(false);
			infoType.setVisible(false);
		}
		else if (type.equalsIgnoreCase("param")) {
			label.setVisible(true);
			tLabel.setVisible(true);
			this.type.setVisible(true);
			cType.setVisible(true);
			infoType.setVisible(true);
		}
	}

	/**
	 * Refresh graphic elements (parameter information).
	 * @param type String representing element type (field, nfield, table, radiobutton, checkbox or combo)
	 */
	private void refreshGuiInfoType(String type) {

		if (type.equalsIgnoreCase("field")) {
			lText.setVisible(true);
			tText.setVisible(true);
			lSize.setVisible(true);
			sSize.setVisible(true);
			bAddElement.setVisible(false);
			bDelElement.setVisible(false);
			bEditElement.setVisible(false);
			bUpElement.setVisible(false);
			bDownElement.setVisible(false);
			lElement.setVisible(false);
			lChoice.setVisible(false);
			bChoiceTrue.setVisible(false);
			bChoiceFalse.setVisible(false);
		}
		else if (type.equalsIgnoreCase("nfield")) {
			lText.setVisible(false);
			tText.setVisible(false);
			lSize.setVisible(false);
			sSize.setVisible(false);
			bAddElement.setVisible(false);
			bDelElement.setVisible(false);
			bEditElement.setVisible(false);
			bUpElement.setVisible(false);
			bDownElement.setVisible(false);
			lElement.setVisible(false);
			lChoice.setVisible(false);
			bChoiceTrue.setVisible(false);
			bChoiceFalse.setVisible(false);
		}
		else if (type.equalsIgnoreCase("table")) {
			lText.setVisible(false);
			tText.setVisible(false);
			lSize.setVisible(false);
			sSize.setVisible(false);
			bAddElement.setVisible(true);
			bAddElement.setToolTipText("Add column");
			bDelElement.setVisible(true);
			bDelElement.setToolTipText("Delete column");
			bEditElement.setVisible(true);
			bEditElement.setToolTipText("Change column name");
			bUpElement.setVisible(true);
			bUpElement.setToolTipText("Move column up");
			bDownElement.setVisible(true);
			bDownElement.setToolTipText("Move column down");
			lElement.setVisible(true);
			lChoice.setVisible(false);
			bChoiceTrue.setVisible(false);
			bChoiceFalse.setVisible(false);
		}
		else if (type.equalsIgnoreCase("radiobutton") ||
				 type.equalsIgnoreCase("checkbox") ||
				 type.equalsIgnoreCase("combo")) {
			lText.setVisible(false);
			tText.setVisible(false);
			lSize.setVisible(false);
			sSize.setVisible(false);
			bAddElement.setVisible(true);
			bAddElement.setToolTipText("Add choice");
			bDelElement.setVisible(true);
			bDelElement.setToolTipText("Delete choice");
			bEditElement.setVisible(true);
			bEditElement.setToolTipText("Change choice name");
			bUpElement.setVisible(true);
			bUpElement.setToolTipText("Move choice up");
			bDownElement.setVisible(true);
			bDownElement.setToolTipText("Move choice down");
			lElement.setVisible(true);
			lChoice.setVisible(true);
			bChoiceTrue.setVisible(true);
			bChoiceFalse.setVisible(true);
		}
	}

	/**
	 * Returns true if name is already present in action list.
	 * @param name String representing action name
	 * @return true if name is already present in action list
	 */
	private boolean isActionExist(String name) {

		String [] elements = lActions.getItems();
		for (int i = 0; i < elements.length; i++) {
			if (elements[i].equals(name)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Returns true if group name is already present in parameter tree.
	 * @param item TreeItem representing the element of tree where search will begin
	 * @param name String representing action name
	 * @return true if group name is already present in parameter tree
	 */
	private boolean isGroupExist(TreeItem item, String name) {

		TreeItem [] elements = item.getItems();
		for (int i = 0; i < elements.length; i++) {
			String type = (String) elements[i].getData("type");
			if (type.equals("group") && elements[i].getText().equals(name)) {
				return true;
			}
		}

		return false;
	}

	/**
	 * Returns true if parameter name is already present in model.
	 * @param name String representing action name
	 * @param elementPath String representing XPath element expression
	 * @return true if parameter name is already present in model
	 */
	private boolean isParamExist(String name, String elementPath) throws Exception {

		return manipulator.isParameterExist(actionType, name, elementPath);
	}

	/**
	 * Returns true if name is already present in paramater element list.
	 * @param name String representing action name
	 * @return true if name is already present in paramater element list
	 */
	private boolean isElementExist(String name) {

		String [] elements = lElement.getItems();
		for (int i = 0; i < elements.length; i++) {
			if (elements[i].equals(name)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * This method open a message error dialog to user, block plugin activity
	 * in setting valide page to false and close the shell.
	 * @param e Exception to catch
	 */
	public void catchException(Exception e) {

		Status status = new Status(Status.ERROR,
									IsacScenarioPlugin.PLUGIN_ID,
									Status.OK,
									"Error during file manipulation.",
									e);

		ErrorDialog diag = new ErrorDialog(parentShell,
											"Plugin error",
											"Problem detects",
											status,
											IStatus.ERROR);
		diag.open();
		e.printStackTrace();
		cancel();
		parentShell.close();
	}


	/**
	 * This method try to apply all modifications into model.
	 */
	public void apply() {
		try {
			manipulator.save();
		}
		catch (Exception e) {
			catchException(e);
		}
	}

	/**
	 * This method performs cancel all modifications into model.
	 */
	public void cancel() {
		manipulator.destroy();
	}

	/* Ensures completion for package field. */
    private void completion() {

    	String s = StringHelper.convertJavaIdentifier(tName.getText());
    	String [] tab = tPackage.getText().split("[.]");

    	if (tab.length > 0) {
	    	String sPackage = tab[tab.length-1];
	    	StringBuffer sb = new StringBuffer();

	    	if (!tName.getText().matches("\\s+") && !sbOldPackage.toString().matches("\\s+")) {

		    	if (s.equalsIgnoreCase("")) {
		    		for (int i = 0; i < tab.length-1; i++) {
		    			sb.append(tab[i]);
		    			if (i != tab.length-2) {
		    				sb.append(".");
		    			}
		    		}
		    	}
		    	else if (sbOldPackage.toString().toLowerCase().equals(sPackage)) {

		    		for (int i = 0; i < tab.length-1; i++) {
		    			sb.append(tab[i]);
		    			sb.append(".");
		    		}
		    		sb.append(s.toLowerCase());
		    	}
		    	else if (sbOldPackage.toString().equals("")){
		    		sb.append(tPackage.getText());
		    		sb.append(".");
		    		sb.append(s.toLowerCase());
		    	}

		    	tPackage.setText(sb.toString());
	    	}
    	}
    	sbOldPackage.replace(0, sbOldPackage.length(), s);
    }

	/* Ensures that both text fields are set. */
    private void validate() {

    	boolean source = false;
    	boolean pluginName = false;
    	boolean packageName = false;
    	boolean className = false;
    	boolean guiFile = false;
    	boolean pluginFile = false;

    	// Validate source property
    	if (tSource != null && !tSource.getText().equals("") ) {
	    	Path sourcePath = new Path(tSource.getText());
	    	try {
	    		IPackageFragmentRoot pfRoot = jProject.findPackageFragmentRoot(sourcePath);
	    		if (pfRoot != null && pfRoot.getKind() == IPackageFragmentRoot.K_SOURCE) {
	    			source = true;
	    		}
			}
			catch (JavaModelException e) {}
    	}

    	// Validate plugin name property
    	pluginName = tName != null &&
    				!tName.getText().replaceAll(" ", "").equalsIgnoreCase("");

    	// Validate class name property
    	packageName = JavaConventions.validatePackageName(tPackage.getText()).isOK();

    	// Validate class name property
    	className = JavaConventions.validateCompilationUnitName(tClass.getText() + ".java").isOK();

    	// Validate gui file property
    	guiFile = tGuiFile != null &&
    			(!tGuiFile.getText().equalsIgnoreCase("")) && tGuiFile.getText().endsWith(".xml");

    	// Validate plugin file property
    	pluginFile = tPluginFile != null &&
    				(!tPluginFile.getText().equalsIgnoreCase("")) && tPluginFile.getText().endsWith(".xml");

    	if(!source) {
    		page.setErrorMessage("Invalid source folder.");
    	}

    	if(!pluginName) {
    		page.setErrorMessage("Invalid plugin name.");
    	}

    	if(!packageName) {
    		page.setErrorMessage("Invalid package name.");
    	}

    	if(!className) {
    		page.setErrorMessage("Invalid class name.");
    	}

    	if(!guiFile) {
    		page.setErrorMessage("Invalid GUI file name.");
    	}

    	if(!pluginFile) {
    		page.setErrorMessage("Invalid plugin file name.");
    	}

    	validated = source && pluginName && packageName && className && guiFile && pluginFile;

    	if(validated) {
    		page.setErrorMessage(null);
    		page.setMessage(" Plugin properties are valid.", DialogPage.INFORMATION);
    	}

    	if (page instanceof WizardPage) {
    		WizardPage newPage = (WizardPage) page;
    		newPage.setPageComplete(validated);
    	}
    	if (page instanceof PropertyPage) {
    		PropertyPage newPage = (PropertyPage) page;
    		newPage.setValid(validated);
    	}
    }

    /**
     * Returns source path selected by user.
     * @param project Iproject init element
     * @return source String representing source path selected by user.
     */
    private String chooseSourceContainer(IProject project) {

		Class[] acceptedClasses= new Class[] { IPackageFragmentRoot.class };
		TypedElementSelectionValidator validator= new TypedElementSelectionValidator(acceptedClasses, false) {
			public boolean isSelectedValid(Object element) {
				try {
					if (element instanceof IPackageFragmentRoot) {
						return (((IPackageFragmentRoot)element).getKind() == IPackageFragmentRoot.K_SOURCE);
					}
					return true;
				} catch (JavaModelException e) {
					catchException(e);
				}
				return false;
			}
		};

		acceptedClasses= new Class[] { IJavaModel.class, IPackageFragmentRoot.class, IJavaProject.class };
		ViewerFilter filter= new TypedViewerFilter(acceptedClasses) {
			public boolean select(Viewer viewer, Object parent, Object element) {
				if (element instanceof IPackageFragmentRoot) {
					try {
						return (((IPackageFragmentRoot)element).getKind() == IPackageFragmentRoot.K_SOURCE);
					} catch (JavaModelException e) {
						catchException(e);
						return false;
					}
				}
				return super.select(viewer, parent, element);
			}
		};

		StandardJavaElementContentProvider provider= new StandardJavaElementContentProvider();
		ILabelProvider labelProvider= new JavaElementLabelProvider(JavaElementLabelProvider.SHOW_DEFAULT);
		ElementTreeSelectionDialog dialog= new ElementTreeSelectionDialog(parentShell, labelProvider, provider);
		dialog.setValidator(validator);
		dialog.setSorter(new JavaElementSorter());
		dialog.setTitle(NewWizardMessages.NewContainerWizardPage_ChooseSourceContainerDialog_title);
		dialog.setMessage(NewWizardMessages.NewContainerWizardPage_ChooseSourceContainerDialog_description);
		dialog.addFilter(filter);
		dialog.setInput(JavaCore.create(project));
		dialog.setInitialSelection(project);

		if (dialog.open() == Window.OK) {
			Object element= dialog.getFirstResult();
			if (element instanceof IPackageFragmentRoot) {
				return ((IPackageFragmentRoot) element).getPath().toString();
			}
			return "";
		}
		return "";
	}

    /**
	 * @param project the project to set
	 */
	public void setProject(IProject project) {
		this.project = project;
	}
}
