/*
* CLIF is a Load Injection Framework
* Copyright (C) 2004 France Telecom R&D
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
* CLIF 
*
* Contact: clif@ow2.org
*/
package org.ow2.clif.scenario.isac.egui.pages.pageBehavior.dnd;

import org.eclipse.swt.dnd.ByteArrayTransfer;
import org.eclipse.swt.dnd.TransferData;
import org.w3c.dom.*;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * This class is used to serialize the scenario node, and to do some transfert
 * when drag & drop operations are used
 *
 * @author JC Meillaud
 * @author A Peyrard
 */

/**
 * Class for serializing behaviorfulltree to/from a byte array
 * @author JC Meillaud & A Peyrard
 */
public class ElementTransfer extends ByteArrayTransfer {

    private static ElementTransfer instance = new ElementTransfer();

    private static final String TYPE_NAME = "Element-transfer-format";
    private static final int TYPEID = registerType(TYPE_NAME);

    private Document doc;

    /**
     * Returns the singleton bft transfer instance.
     * @return ElementTransfer unique instance
     */
    public static ElementTransfer getInstance()
    {
        return instance;
    }

    /**
     * Avoid explicit instantiation
     */
    private ElementTransfer()
    {
    }

    protected List<Element> fromByteArray(byte[] bytes)
    {
        if (bytes == null)
        {
            return null;
        }
        DataInputStream in = new DataInputStream(new ByteArrayInputStream(bytes));
        try
        {
            return readElement(in);

        } catch (IOException e)
        {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Method declared on Transfer.
     * @return A table containing the value of accepted types
     */
    protected int[] getTypeIds()
    {
        return new int[]{TYPEID};
    }

    /**
     * Method declared on Transfer.
     * @return A table containing the name of accepted types
     */
    protected String[] getTypeNames()
    {
        return new String[]{TYPE_NAME};
    }

    /**
     * Method declared on Transfer.
     * @param object
     * @param transferData
     */
    protected void javaToNative(Object object, TransferData transferData)
    {
        byte[] bytes = toByteArray((List<Element>) object);
        if (bytes != null)
        {
            super.javaToNative(bytes, transferData);
        }
    }

    /**
     * Method declared on Transfer.
     */
    protected Object nativeToJava(TransferData transferData)
    {
        byte[] bytes = (byte[]) super.nativeToJava(transferData);
        return fromByteArray(bytes);
    }

    private List<Element> readElement(DataInputStream dataIn) throws IOException
    {
        List<Element> res = new ArrayList<Element>();
        do
        {
            res.add((Element) readSingleElement(dataIn));
        } while (dataIn.available() > 0);
        return res;
    }

    /**
     * Reads and returns a single BehaviorFullTree from the given stream.
     * @param dataIn
     * @return Node created from dataIn
     * @throws IOException
     */
    private Node readSingleElement(DataInputStream dataIn)
            throws IOException
    {

        short type = dataIn.readShort();
        Node node = null;

        String name;
        String value;
        String text;

        switch (type)
        {
            case Node.ELEMENT_NODE:
                name = dataIn.readUTF();
                node = doc.createElement(name);

                int nbAttr = dataIn.readInt();
                for (int i = 0; i < nbAttr; i++)
                {
                    ((Element) node).setAttributeNode((Attr) readSingleElement(dataIn));
                }

                int nbChild = dataIn.readInt();
                for (int i = 0; i < nbChild; i++)
                {
                    Node elt = readSingleElement(dataIn);
                    if (elt != null)
                    {
                        node.appendChild(elt);
                    }
                }
                break;
            case Node.ATTRIBUTE_NODE:
                name = dataIn.readUTF();
                value = dataIn.readUTF();
                node = doc.createAttribute(name);
                node.setNodeValue(value);
                break;
            case Node.COMMENT_NODE:
                text = dataIn.readUTF();
                node = doc.createComment(text);
                break;
            case Node.TEXT_NODE:
                text = dataIn.readUTF();
                node = doc.createTextNode(text);
                break;
            default:
                break;
        }
        return node;
    }

    protected byte[] toByteArray(List<Element> tree)
    {
        ByteArrayOutputStream byteOut = new ByteArrayOutputStream();
        DataOutputStream out = new DataOutputStream(byteOut);

        byte[] bytes = null;

        try
        {
//            out.writeUTF(tree.toString());
            writeElement(tree, out);
            out.close();
            bytes = byteOut.toByteArray();
        } catch (IOException e)
        {
            // do nothing
        }
        return bytes;
    }

    private void writeElement(List<Element> node, DataOutputStream dataOut) throws IOException
    {
        for (Element element : node)
        {
            writeSingleElement(element, dataOut);
        }
    }

    /**
     * Writes the given ScenarioNode to the stream.
     * @param node
     * @param dataOut write node into
     * @throws IOException
     */
    private void writeSingleElement(Node node, DataOutputStream dataOut)
            throws IOException
    {
        doc = node.getOwnerDocument();
        dataOut.writeShort(node.getNodeType());
        switch (node.getNodeType())
        {
            case Node.ELEMENT_NODE:
                dataOut.writeUTF(node.getNodeName());
                NodeList children = node.getChildNodes();
                NamedNodeMap attributes = node.getAttributes();

                int nbChildren = children.getLength();
                int nbAttributes = attributes.getLength();

                dataOut.writeInt(nbAttributes);
                for (int i = 0; i < nbAttributes; i++)
                {
                    Node next = attributes.item(i);
                    writeSingleElement(next, dataOut);
                }

                dataOut.writeInt(nbChildren);
                for (int i = 0; i < nbChildren; i++)
                {
                    Node next = children.item(i);
                    writeSingleElement(next, dataOut);
                }
                break;
            case Node.ATTRIBUTE_NODE:
                Attr attribute = (Attr) node;
                dataOut.writeUTF(attribute.getName());
                dataOut.writeUTF(attribute.getValue());
                break;
            case Node.COMMENT_NODE:
                Comment comment = (Comment) node;
                dataOut.writeUTF(comment.getNodeValue());
                break;
            case Node.TEXT_NODE:
                Text text = (Text) node;
                dataOut.writeUTF(text.getNodeValue());
                break;
            default:
                System.out.println(node.getNodeType());
                break;
        }
    }
}

    