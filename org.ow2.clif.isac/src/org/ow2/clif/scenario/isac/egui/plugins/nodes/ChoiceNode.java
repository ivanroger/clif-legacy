/*
* CLIF is a Load Injection Framework
* Copyright (C) 2004, 2005,2012 France Telecom R&D
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
* Contact: clif@ow2.org
*/
package org.ow2.clif.scenario.isac.egui.plugins.nodes;

import java.util.HashMap;
import java.util.Map;
import java.util.Vector;
import org.ow2.clif.scenario.isac.egui.plugins.gui.ParametersWidgetsNode;
import org.ow2.clif.scenario.isac.egui.plugins.gui.WidgetDescription;

/**
 * This class is an implementation of some methods which will be used to build some
 * nodes of type 'choice'
 *
 * @author JC Meillaud
 * @author A Peyrard
 * @author Bruno Dillenseger
 */
public abstract class ChoiceNode {

    /**
     * Create a node description for a choice
     * @return The node description for this controler
     */
    public static NodeDescription createNodeDescription() {
        /* create a new desc */
        NodeDescription desc = new NodeDescription(Node.CHOICE);
        Map<String,String> values = new HashMap<String,String>();
        values.put("proba","");
        desc.setParams(values);
        return desc ;
    }

    /**
     * Create the parameters widgets tree
     * @return The tree created
     */
    public static ParametersWidgetsNode createParametersWidgetsNode() {
        ParametersWidgetsNode pwn = new ParametersWidgetsNode(null) ;
        /* add the proba parameter field */
        ParametersWidgetsNode child = new ParametersWidgetsNode(new WidgetDescription(WidgetDescription.TEXT_FIELD, "proba",null, null)) ;
        pwn.addChild(child) ;
        return pwn ;
    }

    /**
     * This method return the help of an choice node
     * @return The help lines
     */
    public static Vector<String> getHelp() {
        Vector<String> help = new Vector<String>();
        help.add("Random choice statement.");
        help.add("It is enclosed in an nchoice statement, and comes with an integer weight and a behavior branch.");
        help.add("It defines a possible behavior branch, whose execution probability is ruled by the given weight and the weigths of other branches in the nchoice statement.");
        help.add("Refer to the nchoice statement help for more information.");
        return help ;
    }
}
