/*
* CLIF is a Load Injection Framework
* Copyright (C) 2005,2008,2012 France Telecom R&D
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
* Contact: clif@ow2.org
*/

package org.ow2.clif.scenario.isac.egui.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Vector;
import org.ow2.clif.scenario.isac.egui.loadprofile.LoadProfile;
import org.ow2.clif.scenario.isac.egui.loadprofile.ProfilePoint;
import org.ow2.clif.scenario.isac.egui.plugins.PluginDescription;
import org.ow2.clif.scenario.isac.egui.plugins.PluginManager;
import org.ow2.clif.scenario.isac.egui.plugins.nodes.ChoiceNode;
import org.ow2.clif.scenario.isac.egui.plugins.nodes.IfNode;
import org.ow2.clif.scenario.isac.egui.plugins.nodes.NChoiceNode;
import org.ow2.clif.scenario.isac.egui.plugins.nodes.Node;
import org.ow2.clif.scenario.isac.egui.plugins.nodes.PreemptiveNode;
import org.ow2.clif.scenario.isac.egui.plugins.nodes.WhileNode;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.NodeList;

/**
 * Some function for reading the model
 * @author Joan Chaumont
 * @author Bruno Dillenseger
 *
 */
public class ModelReaderXIS {

    /**
     * Get all behavior ids in a doc
     * @param doc the document
     * @return String[] all ids
     */
    public static String[] getBehaviorsId(Document doc) {
        NodeList behaviors = doc.getElementsByTagName("behavior");
        
        int nbBehaviors = behaviors.getLength();
        String[] ids = new String[nbBehaviors];
        
        for (int i = 0; i < nbBehaviors; i++) {
            Element behavior = (Element)behaviors.item(i);
            ids[i] = behavior.getAttribute("id");
        }
        
        return ids;
    }
    
    /**
     * Get a behavior Element by id
     * @param doc the entire document
     * @param id id of the behavior
     * @return Element the behavior
     */
    public static Element getBehaviorById(Document doc, String id) {
        NodeList behaviors = doc.getElementsByTagName("behavior");
        
        int nbBehaviors = behaviors.getLength();        
        for (int i = 0; i < nbBehaviors; i++) {
            Element behavior = (Element)behaviors.item(i);
            if(behavior.getAttribute("id").equals(id)) {
                return behavior;
            }
        }
        return null;
    }
    
    /**
     * Get all profiles ids in a doc
     * @param doc the document
     * @return String[] all ids
     */
    public static String[] getProfilesId(Document doc) {
        NodeList behaviors = doc.getElementsByTagName("group");
        
        int nbBehaviors = behaviors.getLength();
        String[] ids = new String[nbBehaviors];
        
        for (int i = 0; i < nbBehaviors; i++) {
            ids[i] = behaviors.item(i).getAttributes().getNamedItem("behavior").getNodeValue();
        }
        
        return ids;
    }
    
    /**
     * Get all plugins imported in the document
     * @param doc the entire document
     * @return HashMap of all plugins (id, name)
     */
    public static Map<String,String> getPlugins(Document doc) {
        Map<String,String> plugs = new HashMap<String,String>();
        NodeList plugins = doc.getElementsByTagName("use");
        
        if(plugins != null) {
            int nbPlugins = plugins.getLength();
            for (int i = 0; i < nbPlugins; i++) {
                Element plugin = (Element)plugins.item(i);
                String id = plugin.getAttribute("id");
                String name = plugin.getAttribute("name");
                if(id != null && name != null 
                        && !id.equals("") && !name.equals("")) {
                    plugs.put(id, name);
                }
            }
        }
        return plugs;
    }
    
    /**
     * Get all params of an element
     * @param elt
     * @return HashMap of params (name, value)
     */
    public static Map<String,String> getParams(Element elt) {
        Map<String,String> params = new HashMap<String,String>();
        
        NodeList nodeParams = elt.getElementsByTagName("param");
        int nbParams = nodeParams.getLength();
        
        for (int i = 0; i < nbParams; i++) {
            NamedNodeMap attr = nodeParams.item(i).getAttributes();
            Attr name = (Attr)attr.getNamedItem("name");
            Attr value = (Attr)attr.getNamedItem("value");
            if(value != null && name != null) {
                params.put(name.getNodeValue(), value.getNodeValue());
            }
        }
        
        return params;
    }

    /**
     * Gets the help for an ISAC instruction or an ISAC plug-in primitive
     * @param element
     * @param plugManager
     * @return Vector help
     */
    public static Vector<String> getActionHelp(Element element, PluginManager plugManager)
    {
        Vector<String> help = null;
        String type = element.getNodeName();
       	if (type.equals(Node.IF))
       	{
       		help = IfNode.getHelp();
       	}
       	else if (type.equals(Node.WHILE))
       	{
       		help = WhileNode.getHelp();
       	}
       	else if (type.equals(Node.PREEMPTIVE))
       	{
       		help = PreemptiveNode.getHelp();
       	}
       	else if (type.equals(Node.NCHOICE))
       	{
       		help = NChoiceNode.getHelp();
       	}
       	else if (type.equals(Node.CHOICE))
       	{
       		help = ChoiceNode.getHelp();
        }
        else {
            String plugId = element.getAttribute("use");
            String plugAction = element.getAttribute("name");
            Map<String,String> usedPlugins = getPlugins(element.getOwnerDocument());
            
            Iterator<String> iter = usedPlugins.keySet().iterator();
            while (iter.hasNext() && help == null)
            {
                String id = iter.next();
                PluginDescription plug = null;
                if(id.equals(plugId))
                {
                    plug = plugManager.getDescription((String)usedPlugins.get(id));
                    
                    /* Test all node kind. Ugly way */
                    help = plug.getActionHelp(Node.SAMPLE, plugAction);
                    if (help == null)
                    {
                        help = plug.getActionHelp(Node.TIMER, plugAction);
                        if (help == null)
                        {
                            help = plug.getActionHelp(Node.TEST, plugAction);
                            if (help == null)
                            {
                            	help = plug.getActionHelp(Node.CONTROL, plugAction);
                            }
                        }
                    }
                }
            }
        }
        return help;
    }
    
    /**
     * Get the help of a giving plugin
     * @param element
     * @param plugManager
     * @return Vector help
     */
    public static Vector<String> getPluginHelp(Element element, PluginManager plugManager) {
        Vector<String> help = null;
        String plugName = element.getAttribute("name");

        PluginDescription plug = plugManager.getDescription(plugName);
        if(plug != null) {
            help = plug.getObject().getHelp();
        }
        return help;
    }
    
    /**
     * Get all profiles in the document
     * @param doc
     * @return HashMap (id, LoadProfile)
     */
    public static Map<String,LoadProfile> getProfiles(Document doc) {
        String[] ids = getBehaviorsId(doc);
        
        Map<String,LoadProfile> profiles = new HashMap<String,LoadProfile>();
        NodeList profileNodes = doc.getElementsByTagName("group");
        
        int nbProfile = profileNodes.getLength();
        for (int i = 0; i < nbProfile; i++) {
            /* a profile is correct if he has a corresponding behavior */
            boolean correctProfile = false;
            Element p = (Element)profileNodes.item(i);
            String id = 
                p.getAttributes().getNamedItem("behavior").getNodeValue();
            for (int j = 0; j < ids.length; j++) {
                if(id.equals(ids[j])) {
                    correctProfile = true;
                    break;
                }
            }

            /* if profile is correct parse it */
            if(correctProfile) {
                /* get all ramps for this profile */
                NodeList ramps = p.getElementsByTagName("ramp");
                int nbSegments = ramps.getLength();
                
                List<ProfilePoint> profPoints = new ArrayList<ProfilePoint>();
                
                for (int j = 0; j < nbSegments; j++) {
                    Element ramp = (Element)ramps.item(j);
                    NodeList points = ramp.getElementsByTagName("point");
                    if(points.getLength() != 2) {
                        continue;
                    }
                    /* a ramp is made of 2 points */
                    for(int k = 0; k < 2; k++) {
                        Element pt = (Element)points.item(k);
                        ProfilePoint point = new ProfilePoint(
                                Integer.parseInt(pt.getAttribute("x")),
                                Integer.parseInt(pt.getAttribute("y")), 
                                ramp.getAttribute("style"));
                        
                        ProfilePoint tmp = null;
                        int nbPoints = profPoints.size();
                        /* Don't add an existing point */
                        for (int index = 0; index < nbPoints; index++) {
                            tmp = profPoints.get(index);
                            if(tmp.time == point.time 
                                    && tmp.population == point.population) {
                                break;
                            }
                        }
                        if(tmp == null || !(tmp.time == point.time) || !(tmp.population == point.population)) {
                            profPoints.add(point);
                        }
                    }
                }
                Boolean force = Boolean.valueOf(
                        p.getAttributes().getNamedItem("forceStop").getNodeValue());
                
                LoadProfile profile = 
                    new LoadProfile(id, profPoints, force.booleanValue());
                profiles.put(id, profile);
            }
        }
        
        return profiles;
    }
}
