/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2004, 2006 France Telecom
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * CLIF 
 *
 * Contact: clif@ow2.org
 */
package org.ow2.clif.scenario.isac.egui;

/**
 * Define the file names for the scenario modules
 * 
 * @author JC Meillaud
 * @author A Peyrard
 * @author Bruno Dillenseger
 */
public class FileName {
	// version of the editor
	public static final String VERSION = "0.3a";

	// TEMP Behaviors File
	public static final String TEMP_BEHAVIORS = "#temp#.xml";

	// Icons definition
	public static final String ICONS_DIR = "icons/";
	public static final String EXIT_ICON = ICONS_DIR + "close.gif";
	public static final String ABOUT_ICON = ICONS_DIR + "about.gif";
	public static final String NEW_ICON = ICONS_DIR + "new.gif";
	public static final String OPEN_ICON = ICONS_DIR + "open.gif";
	public static final String SAVE_ICON = ICONS_DIR + "save.gif";
	public static final String SAVE_AS_ICON = ICONS_DIR + "save_as.gif";
	public static final String HELP_SHOW_ICON = ICONS_DIR + "help_show.png";
	public static final String HELP_HIDE_ICON = ICONS_DIR + "help_hide.png";
	public static final String UP_ARROW_ICON = ICONS_DIR + "up_arrow.gif";
	public static final String DOWN_ARROW_ICON = ICONS_DIR + "down_arrow.gif";
	public static final String DELETE_ICON = ICONS_DIR + "delete.gif";
	public static final String COPY_ICON = ICONS_DIR + "copy.gif";
	public static final String CUT_ICON = ICONS_DIR + "cut.gif";
	public static final String PASTE_ICON = ICONS_DIR + "paste.gif";
	public static final String ADD_ICON = ICONS_DIR + "add.gif";
	public static final String EXPAND_ICON = ICONS_DIR + "expand.gif";
	public static final String COLLAPSE_ICON = ICONS_DIR + "collapse.gif";
	public static final String GENERATE_ICON = ICONS_DIR + "generate.gif";
	public static final String FULL_ICON = ICONS_DIR + "full.gif";
	public static final String SPLIT_ICON = ICONS_DIR + "split.gif";
	public static final String RENAME_ICON = ICONS_DIR + "rename.png";
	public static final String ADD_GROUP_ICON = ICONS_DIR + "addgroup.png";
	public static final String ADD_PARAM_ICON = ICONS_DIR + "addparam.png";
	public static final String GROUP_ICON = ICONS_DIR + "group.png";
	public static final String PARAM_ICON = ICONS_DIR + "param.png";

	// icon for behaviors tree nodes 
	public static final String BEHAVIOR_ICON = ICONS_DIR + "behavior.gif";
	public static final String IF_ICON = ICONS_DIR + "if.gif";
	public static final String NCHOICE_ICON = ICONS_DIR + "nchoice.gif";
	public static final String PLUGINS_ICON = ICONS_DIR + "plugins.gif";
	public static final String SAMPLE_ICON = ICONS_DIR + "injector.png";
	public static final String TIMER_ICON = ICONS_DIR + "timer.gif";
	public static final String CONTROL_ICON = ICONS_DIR + "sample.gif";
	public static final String USE_ICON = ICONS_DIR + "use.gif";
	public static final String WHILE_ICON = ICONS_DIR + "generate.gif";
	public static final String CONDITION_ICON = ICONS_DIR + "condition.png";
	public static final String THEN_ICON = ICONS_DIR + "forward.gif";
	public static final String CHOICE_ICON = ICONS_DIR + "choice.png";
	public static final String PREEMPTIVE_ICON = ICONS_DIR + "preemptive.gif";

	// drawing icons
	public static final String DRAW_NOTHING_ICON = ICONS_DIR + "arrow.png";
	public static final String ADDPOINT_ICON = ICONS_DIR + "addpoint.png";
	public static final String DELPOINT_ICON = ICONS_DIR + "delpoint.png";
	public static final String MAXIMIZEVIEW_ICON = ICONS_DIR + "maximizeview.png";
	public static final String NORMALVIEW_ICON = ICONS_DIR + "normalview.png";

	// load profile icons
	public static final String LINE_ICON = ICONS_DIR + "line.png";
	public static final String HV_ICON = ICONS_DIR + "hv.png";
	public static final String VH_ICON = ICONS_DIR + "vh.png";

	// plugins definition
	public static final String PLUGINS_DIR = "lib/ext";
	public static final String PLUGIN_PROPERTIES_FILE = "isac-plugin.properties";
	public static final String PLUGIN_TEMPLATES_DIR = "templates/";
	public static final String PLUGIN_TEMPLATES_BUILD_FILE = "build.xml.template";
	public static final String PLUGIN_TEMPLATES_SESSIONOBJECT_FILE = "SessionObject.java.template";
	public static final String PLUGIN_TEMPLATES_PROPERTIES_FILE = "isac-plugin.properties.template";
	public static final String PLUGIN_TEMPLATES_GUI_FILE = "gui.xml.template";
	public static final String PLUGIN_TEMPLATES_PLUGIN_FILE = "plugin.xml.template";
	public static final String PLUGIN_EXTENSION = ".jar";
}
