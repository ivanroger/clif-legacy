/*
* CLIF is a Load Injection Framework
* Copyright (C) 2004 France Telecom R&D
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
* CLIF 
*
* Contact: clif@ow2.org
*/
package org.ow2.clif.scenario.isac.egui.plugins.nodes;

import java.util.Vector;

import org.ow2.clif.scenario.isac.egui.plugins.gui.ParametersWidgetsNode;

/**
 * This class implements some statics methods which will be used with none plugins nodes
 * @author JC Meillaud
 * @author A Peyrard
 */
public class NonePluginNode {

    /**
     * Help getter
     * @param type The type of the node 
     * @return The help of the node given
     */
    public static Vector<String> getHelp(String type) {
        if (Node.NCHOICE.equals(type)) {
            return NChoiceNode.getHelp() ;
        }
        if (Node.CHOICE.equals(type)) {
            return ChoiceNode.getHelp() ;
        }
        // default
        return null ;
    }
    
    /**
     * Create a new node description of the given type
     * @param type The type
     * @return The node description
     */
    public static NodeDescription createNodeDescription(String type) {
        if (Node.CHOICE.equals(type)) {
            return ChoiceNode.createNodeDescription() ;
        }
        if (Node.BEHAVIOR.equals(type)) {
            return BehaviorNode.createNodeDescription() ;
        }
        if (Node.BEHAVIORS.equals(type)) {
            return BehaviorsNode.createNodeDescription() ;
        }
        // default
        return new NodeDescription(type) ;
    }
    
    /**
     * Create the parameters widgets tree of a none plugin node
     * @param type The type of the node
     * @return the tree created
     */
    public static ParametersWidgetsNode createParametersWidgetsNode(String type) {
        if (Node.BEHAVIOR.equals(type))
            return BehaviorNode.createParametersWidgetsNode() ;
        if (Node.CHOICE.equals(type))
            return ChoiceNode.createParametersWidgetsNode() ;
        // default
        return null ;
    }
    
}
