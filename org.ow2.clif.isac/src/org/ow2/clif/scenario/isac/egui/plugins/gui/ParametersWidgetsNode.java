/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2004 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * CLIF 
 *
 * Contact: clif@ow2.org
 */
package org.ow2.clif.scenario.isac.egui.plugins.gui;

import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;
import org.ow2.clif.scenario.isac.egui.plugins.ParameterDescription;
import org.ow2.clif.scenario.isac.egui.plugins.nodes.Node;

/**
 * This class is a node of the parameters widgets trees
 * 
 * @author JC Meillaud
 * @author A Peyrard
 */
public class ParametersWidgetsNode {
	private WidgetDescription widget;
	private ParametersWidgetsNode parent;
	private Vector<ParametersWidgetsNode> children;

	/**
	 * Build a new parameters widget node
	 * @param wd The widget description
	 */
	public ParametersWidgetsNode(WidgetDescription wd) {
		this.widget = wd;
		this.parent = null;
		this.children = new Vector<ParametersWidgetsNode>();
	}

	/**
	 * @return Returns the parent.
	 */
	public ParametersWidgetsNode getParent() {
		return parent;
	}
	/**
	 * @param parent The parent to set.
	 */
	public void setParent(ParametersWidgetsNode parent) {
		this.parent = parent;
	}

	/**
	 * Add a new child to this node
	 * 
	 * @param child The child to be added
	 */
	public void addChild(ParametersWidgetsNode child) {
		this.children.add(child);
	}

	/**
	 * @return Returns the children.
	 */
	public Vector getChildren() {
		return children;
	}

	/**
	 * Create a new parameters widgets node
	 * @param ids The table containing all the parameters values
     * @param params a vector of all params
	 * @param type The type of the action to create the tree
	 * @param plugin The name of the plugin
	 * @return The default parameters widgets node created
	 */
	public static ParametersWidgetsNode createParametersWidgetsNode(Vector ids,
			Vector params, String type, String plugin) {
        
		ParametersWidgetsNode root = new ParametersWidgetsNode(null);

        Enumeration parametersNames = null;
        if(params != null) {
            parametersNames = params.elements();
        }
        
		while (parametersNames.hasMoreElements()) {
			ParameterDescription param = 
                (ParameterDescription) parametersNames.nextElement();
            
			/* build a new description for this params 
             * only if it is not the "id" parameter */
			if (!param.getName().equals("id")) {
				WidgetDescription desc = new WidgetDescription(
						WidgetDescription.TEXT_FIELD, param.getName(),null, null);
				root.addChild(new ParametersWidgetsNode(desc));
			} 
            else {
				if (Node.isPluginNode(type)) {
					if (Node.USE.equals(type)) {
						/* add a new field to set the id value */
						WidgetDescription desc = new WidgetDescription(
								WidgetDescription.TEXT_FIELD, "id",null, null);
						root.addChild(new ParametersWidgetsNode(desc));
					} 
                    else {
						Map<String,Object> paramsTable = new HashMap<String,Object>();
						paramsTable.put("choices", ids);
						WidgetDescription desc = new WidgetDescription(
								WidgetDescription.COMBO, "id",null, paramsTable);
						root.addChild(new ParametersWidgetsNode(desc));
					}
				} else {
					WidgetDescription desc = new WidgetDescription(
							WidgetDescription.TEXT_FIELD, param.getName(),null, null);
					root.addChild(new ParametersWidgetsNode(desc));
				}
			}
		}
		return root;
	}

	/**
	 * @return Returns the widget.
	 */
	public WidgetDescription getWidget() {
		return widget;
	}

	public String toString() {
		String result = "";
		if (this.widget != null) {
            result = "Widget : \n" + this.widget.toString();
        }
		if (this.children != null) {
			result = result + "this ParametersWidgetsNode has "
					+ this.children.size() + " childrens;\n";
			for (int i = 0; i < children.size(); i++) {
				result = result.concat(((ParametersWidgetsNode) this.children
						.elementAt(i)).toString());
			}
		}
		return result;
	}
}