/*
* CLIF is a Load Injection Framework
* Copyright (C) 2004 France Telecom R&D
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
* CLIF 
*
* Contact: clif@ow2.org
*/
package org.ow2.clif.scenario.isac.egui.plugins;

import java.util.Vector;

import org.ow2.clif.scenario.isac.egui.plugins.nodes.NodeDescription;
/**
 * Interface which define method which must be implemented by action description object 
 *   
 * @author JC Meillaud
 * @author A Peyrard
 */
public interface ActionDescription {
	/**
	 * Method which update the node description for this action
	 * The node have been initialised with the rigth type, we initialise the other fields
	 * @param desc The node description 
	 */
	public void createNodeDescription(NodeDescription desc) ;
	/**
	 * The GUIKey Setter, this key is the reference in the table which store
	 *  the parameters definitions interfaces
	 * @param key The key
	 */
	public void setGUIKey(String key) ;
	/**
	 * GUIKey getter
	 * @return The key
	 */
	public String getGUIKey() ;
    /**
     * Name getter
     * @return The name
     */
    public String getName() ;
    /**
     * Help getter
     * @return The help
     */
    public Vector<String> getHelp();

    /**
     * Get action parameters.
     * @return the action parameters as a vector of parameter description objects
     */
    public Vector<ParameterDescription> getParams();
}
