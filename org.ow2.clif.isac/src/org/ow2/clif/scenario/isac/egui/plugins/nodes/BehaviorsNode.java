/*
* CLIF is a Load Injection Framework
* Copyright (C) 2004 France Telecom R&D
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
* CLIF 
*
* Contact: clif@ow2.org
*/
package org.ow2.clif.scenario.isac.egui.plugins.nodes;

import java.util.Vector;


/**
 * Some methods use for the 'behaviors' nodes
 * 
 * @author JC Meillaud
 * @author A Peyrard
 */
public class BehaviorsNode {

    /**
     * Help behaviors node getter
     * @return The help lines
     */
    public static Vector getHelp() {
        return null ;
    }
    
    /**
     * Create a 'behaviors' node description
     * @return The node description created
     */
    public static NodeDescription createNodeDescription() {
        return new NodeDescription(Node.BEHAVIORS) ;
    }
}
