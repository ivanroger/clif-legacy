/*
* CLIF is a Load Injection Framework
* Copyright (C) 2004, 2005 France Telecom R&D
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
* Contact: clif@ow2.org
*/

package org.ow2.clif.util.gui;


import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JDesktopPane;
import javax.swing.JInternalFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;


/**
 * Generic alert window (JInternalFrame) for Swing JDesktopPane
 * @author Bruno Dillenseger
 */
public class GuiAlert extends JInternalFrame implements ActionListener
{
	private static final long serialVersionUID = 464014749410732128L;

	protected JButton okBtn;


	public GuiAlert(JDesktopPane desktop, String title, String message)
	{
		super(title, true, true);
		Container pane = getContentPane();
		pane.setLayout(new BorderLayout());
		if (message != null)
		{
			pane.add(BorderLayout.CENTER, new JScrollPane(new JTextArea(message, 5, 40)));
		}
		JPanel buttonPnl = new JPanel();
		buttonPnl.add(okBtn = new JButton("OK"));
		okBtn.addActionListener(this);
		pane.add(BorderLayout.SOUTH, buttonPnl);
		desktop.add(this);
	}


	public void alert()
	{
		setVisible(true);
		pack();
	}


	@Override
	public void actionPerformed(ActionEvent e)
	{
		this.dispose();
	}
}
