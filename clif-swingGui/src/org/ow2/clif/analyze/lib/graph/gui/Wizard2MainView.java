/*
* CLIF is a Load Injection Framework
* Copyright (C) 2011, 2014 France Telecom R&D
* Copyright (C) 2016-2017 Orange SA
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
* Contact: clif@ow2.org
*/

package org.ow2.clif.analyze.lib.graph.gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.SortedSet;
import java.util.Vector;
import javax.swing.AbstractButton;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.DefaultCellEditor;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import org.ow2.clif.analyze.lib.graph.FieldFilterAndOp;
import org.ow2.clif.analyze.lib.graph.gui.ReportManager.Phases;
import org.ow2.clif.analyze.lib.report.FieldsValuesFilter;
import org.ow2.clif.analyze.lib.report.FieldsValuesFilter.LogicalEnum;
import org.ow2.clif.analyze.lib.report.LogAndDebug;
import org.ow2.clif.analyze.lib.report.Section;
import org.ow2.clif.analyze.lib.report.Section.ChartRepresentation;
import org.ow2.clif.util.gui.SwingHelper;


@SuppressWarnings("serial")
public class Wizard2MainView extends JPanel implements ActionListener {

	// Use for the columns in the filter table
	static private final int FILTER_TABLE_FIELD = 0;
	static private final int FILTER_TABLE_OPERATOR = 1;
	static private final int FILTER_TABLE_VALUE = 2;
	static private final int FILTER_TABLE_DELETE = 3;

	JFrame frame;
	ReportManager control;
	String name = "Filternig View";
	JPanel panel = new JPanel();
	Phases next;
	DataAccess	data;


	public Wizard2MainView(ReportManager _control, JFrame _frame)
	{
		frame = _frame;
		control = _control;
		data = control.getDataAccess();
		frame.setTitle("Step 2/2 - Dataset options");
		frame.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		init();
		frame.pack();
	}


	private void init() {

		jTableFilter = new javax.swing.JTable();

		modelTableFilter = new DefaultTableModel(
			new Object[][] {},
			new String[] { "Field", "Operator", "Value", "Delete" })
			{
				Class<?>[] types = new Class[]
				{
					java.lang.String.class,
					java.lang.String.class,
					java.lang.String.class,
					java.lang.String.class
				};
				boolean[] canEdit = new boolean[] { true, true, true, false };

				public Class<?> getColumnClass(int columnIndex)
				{
					return types[columnIndex];
				}

				public boolean isCellEditable(int rowIndex, int columnIndex)
				{
					return canEdit[columnIndex];
				}
			};
		clearTableFilter();
		guiObjectsCreation();
		guiObjectsAssembly();
		guiObjectsInitialization();
		guiObjectsListeners();
		dataInit();
	}


	private void dataInit() {
		if (null == control.w2View){
			return;				// while Wizard2MainView Constructor is being called
		}
		// filtering times
		updateStartFilterAtFirstEvent(true);
		updateEndFilterAtLastEvent(true);
		// statistical analysis
		data.wizardDataset.setPerformStatAnalysis(true);
		data.wizardDataset.setkParam(5.0);
		data.wizardDataset.setPercentPointsKeptParam(95.0);
		// draw options
		data.wizardDataset.setPerformDraw(true);
		data.wizardDataset.setDrawRaw(true);
	}


	void updateGuiFromWizardDataset() {
		// filter start time
		jcbStartFilterAtFirstEvent.setSelected(data.wizardDataset.isStartFilterAtFirstEvent());
		updateStartFilterAtFirstEvent(data.wizardDataset.isStartFilterAtFirstEvent());
		updateFilterStartTime(data.wizardDataset.getFilterStartTime());
		// filter end time
		updateEndFilterAtLastEvent(data.wizardDataset.isEndFilterAtLastEvent());
		updateFilterEndTime(data.wizardDataset.getFilterEndTime());
		// perform filtering
		jcbPerformStAn.setSelected(data.wizardDataset.isPerformStatAnalysis());
		updatePerformStatAnalysis(data.wizardDataset.isPerformStatAnalysis());
		// cleaning
		updateStatisticsCleaning();
		// perform draw
		jrbNoChart.setSelected(!data.wizardDataset.isPerformDraw());
		updatePerformDraw(data.wizardDataset.isPerformDraw());

		// times verifications
		w2TimeVerifications();

		// Draw Options
		jrbNoChart.setSelected(!data.wizardDataset.isPerformDraw());
		jrbTimeBased.setSelected(data.wizardDataset.isPerformTimeChart());
		jcbScatterPlot.setSelected(data.wizardDataset.isDrawRaw());
		jrbHistogram.setSelected(data.wizardDataset.isPerformHistogram());
		jtfSlices.setText(String.valueOf(data.wizardDataset.getSlicesNb()));
		jrbQuantiles.setSelected(data.wizardDataset.isPerformQuantiles());
		jtfQuantiles.setText(String.valueOf(data.wizardDataset.getQuantiles()));

		jcbMovingMin.setSelected(data.wizardDataset.isDrawMovingMin());
		jcbMovingMax.setSelected(data.wizardDataset.isDrawMovingMax());
		jcbMovingAverage.setSelected(data.wizardDataset.isDrawMovingAverage());
		jcbMovingMedian.setSelected(data.wizardDataset.isDrawMovingMedian());

		jcbMovingStdDev.setSelected(data.wizardDataset.isDrawMovingStdDev());
		jcbMovingThroughput.setSelected(data.wizardDataset.isDrawMovingThroughput());

		jtfTimeWindow.setText(Integer.toString(data.wizardDataset.getTimeWindow()));
		jtfStep.setText(Integer.toString(data.wizardDataset.getStep()));

		updateDrawOptions();
		updateStatus();
	}


	private void guiObjectsCreation() {

		//  --- jpMainFilter_
		mainFilterPanel = new JPanel();
		//  ----------- jpMainFilteringLeft 
		jpMainFilteringLeft = new TitledPanel("Event Selection");
		//  ----------------------- jtfFilterStartTime 
		jtfFilterStartTime = new JTextField(8);
		jtfFilterStartTime.setMinimumSize(jtfFilterStartTime.getPreferredSize());
		//  ----------------------- jcbStartFilterAtFirstEvent 
		jcbStartFilterAtFirstEvent = new JCheckBox();
		//  ----------------------- jtfFilterEndTime 
		jtfFilterEndTime = new JTextField(8);
		jtfFilterEndTime.setMinimumSize(jtfFilterEndTime.getPreferredSize());
		//  ----------------------- jcbEndFilterAtLastEvent 
		jcbEndFilterAtLastEvent = new JCheckBox();
		//  --------------- jScrollFilterTable 
		jScrollFilterTable = new JScrollPane();
		//  --------------- jpAddAndLogic 
		jpAddAndLogic = new JPanel();
		//  ------------------- jbAddFilter 
		jbAddFilter = new JButton();
		//  ------------------- jpLogic 
		jpLogic = new JPanel();
		//  ----------------------- jlLinkingFilter 
		jlLinkingFilter = new JLabel();
		//  ----------------------- jrbAnd 
		//  ----------------------- jrbOr 
		jrbAnd = new JRadioButton("AND");
		jrbOr = new JRadioButton("OR");
		//  ----------- jpMainFilterRight 
		jpMainFilterRight = new JPanel();
		//  ------- jspCenter 
		jspCenter = new JSplitPane(
			JSplitPane.HORIZONTAL_SPLIT,
			jpMainFilteringLeft,
			jpMainFilterRight);
		jspCenter.setResizeWeight(1);
		//  --------------- jpStatAnalysis 
		jpStatAnalysis = new TitledPanel("Statistical Analysis");
		//  ------------------- jpStAnChoose 
		jpStAnChoose = new JPanel();
		//  ----------------------- jcbPerformStAn 
		jcbPerformStAn = new JCheckBox();
		//  ----------------------- jcomboChooseField 
		jcomboChooseField = new JComboBox<String>();
		//  --------------- jpCleaning_ 
		jpCleaning_ = new JPanel();
		//  ----------------------- jpKCleaningDescription 
		jpKCleaningDescription = new JPanel();
		//  --------------------------- jlCleaningKDescription 
		jlCleaningKDescription = new JLabel();
		//  ----------------------- jpCleaningK 
		jpCleaningK = new JPanel();
		//  --------------------------- jtfCleaningK 
		jtfCleaningK = new JTextField(20);
		//  --------------------------- jlCleaningKValue 
		jlCleaningKValue = new JLabel();
		//  ----------------------- jpCleaningPercentPointsKept 
		jpCleaningPercentPointsKept = new JPanel();
		//  --------------------------- jlCleaningPercentPointsKept1 
		jlCleaningPercentPointsKept1 = new JLabel();
		//  --------------------------- jtfCleaningPercentPointsKept 
		jtfCleaningPercentPointsKept = new JTextField(3);
		//  --------------------------- jlCleaningPercentPointsKept2 
		jlCleaningPercentPointsKept2 = new JLabel();
		//  ----------------------- jcbDrawRaw
		jcbDrawRaw = new JCheckBox();
		//  --------------- jpButtons 
		jpButtons = new JPanel();
		//  ------------------- jbPrevious 
		jbPrevious = new JButton();
		//  ------------------- jbCancel 
		jbCancel = new JButton();
		//  ------------------- jbNext 
		jbNext = new JButton();
		//  ------- jpStatusFilter 
		jpStatusFilter = new JPanel();
		//  ----------- jlW2StatusLine 
		jlW2StatusLine = new JLabel("status");
	}


	private void guiObjectsAssembly()
	{
		frame.add(mainFilterPanel);
		//  --- jpMainFilter_
		mainFilterPanel.setLayout(new BorderLayout());
		mainFilterPanel.add(jspCenter, BorderLayout.CENTER);
		mainFilterPanel.add(jpStatusFilter, BorderLayout.SOUTH);
		
		//  ----------- jpMainFilteringLeft

		jpMainFilteringLeft.setLayout(new GridBagLayout());
		GridBagConstraints gbc = new GridBagConstraints();
		gbc.fill = GridBagConstraints.HORIZONTAL;
		gbc.weightx = 1;

		// event selection: start time
		gbc.gridx = 0;
		gbc.gridy = 0;
		gbc.anchor = GridBagConstraints.EAST;
		jpMainFilteringLeft.add(new JLabel("Start time (ms)"), gbc);

		gbc.gridx++;
		gbc.anchor = GridBagConstraints.WEST;
		jpMainFilteringLeft.add(jtfFilterStartTime, gbc);

		gbc.gridx++;
		jpMainFilteringLeft.add(jcbStartFilterAtFirstEvent, gbc);

		// event selection: end time
		gbc.gridx = 0;
		gbc.gridy++;
		gbc.anchor = GridBagConstraints.EAST;
		jpMainFilteringLeft.add(new JLabel("End time (ms)"), gbc);

		gbc.gridx++;
		gbc.anchor = GridBagConstraints.WEST;
		jpMainFilteringLeft.add(jtfFilterEndTime, gbc);

		gbc.gridx++;
		jpMainFilteringLeft.add(jcbEndFilterAtLastEvent, gbc);

		// fields values filters table
		gbc.gridx = 0;
		gbc.gridy++;
		gbc.gridwidth = 3;
		gbc.weighty = 1;
		gbc.fill = GridBagConstraints.BOTH;
		jpMainFilteringLeft.add(jScrollFilterTable, gbc);

		// buttons area for fields values filters
		gbc.gridy++;
		gbc.weighty = 0;
		gbc.fill = GridBagConstraints.HORIZONTAL;
		jpAddAndLogic.setLayout(new FlowLayout());
		jpAddAndLogic.add(jbAddFilter);
		jpAddAndLogic.add(jpLogic);
		jpLogic.add(jlLinkingFilter);
		jpLogic.add(jrbAnd);
		jpLogic.add(jrbOr);
		jpMainFilteringLeft.add(jpAddAndLogic, gbc);

		// jpMainFilterRight

		jpMainFilterRight.setLayout(new BoxLayout(jpMainFilterRight, BoxLayout.Y_AXIS));
		jpMainFilterRight.add(jpStatAnalysis);

//		jpMainFilterRight.add(jpCleaning_);		//  TODO to add when implemented 

		jpDrawOptions = datasetDrawOptionsCreation();
		jpMainFilterRight.add(jpDrawOptions, BorderLayout.CENTER);
		jpMainFilterRight.add(jpButtons, BorderLayout.SOUTH);
		//  --------------- jpStatAnalysis 
		FlowLayout jpChooseAFieldLayout = (FlowLayout) jpStatAnalysis.getLayout();
		jpChooseAFieldLayout.setAlignment((int) LEFT_ALIGNMENT);
		jpStatAnalysis.add(jpStAnChoose, BorderLayout.NORTH);
		//  ------------------- jpStAnChoose 
		jpStAnChoose.setLayout(new BoxLayout(jpStAnChoose, BoxLayout.X_AXIS));
		jpStAnChoose.add(jcbPerformStAn);
		jpStAnChoose.add(jcomboChooseField);
		//  ----------------------- jcbPerformStAn 
		//  ----------------------- jcomboChooseField 
		//  --------------- jpCleaning_ 
		jpCleaning_.setLayout(new BoxLayout(jpCleaning_, BoxLayout.Y_AXIS));
		jpCleaning_.add(jpKCleaningDescription);
		jpCleaning_.add(jpCleaningK);
		jpCleaning_.add(jpCleaningPercentPointsKept);
		//  ----------------------- jpKCleaningDescription 
		jpKCleaningDescription.add(jlCleaningKDescription);
		//  --------------------------- jlCleaningKDescription 
		//  ----------------------- jpCleaningK 
		jpCleaningK.add(jlCleaningKValue);
		jpCleaningK.add(jtfCleaningK);
		//  --------------------------- jtfCleaningK 
		//  --------------------------- jlCleaningKValue 
		//  ----------------------- jpCleaningPercentPointsKept 
		jpCleaningPercentPointsKept.add(jlCleaningPercentPointsKept1);
		jpCleaningPercentPointsKept.add(jtfCleaningPercentPointsKept);
		jpCleaningPercentPointsKept.add(jlCleaningPercentPointsKept2);
		//  --------------------------- jlCleaningPercentPointsKept1 
		//  --------------------------- jtfCleaningPercentPointsKept 
		//  --------------------------- jlCleaningPercentPointsKept2 

		//  --------------- jpButtons 
		jpButtons.setLayout(new BoxLayout(jpButtons, BoxLayout.LINE_AXIS));
		jbPrevious.setAlignmentY(BOTTOM_ALIGNMENT);
		jpButtons.add(jbPrevious);
		jbCancel.setAlignmentY(BOTTOM_ALIGNMENT);
		jpButtons.add(jbCancel);
		jbNext.setAlignmentY(BOTTOM_ALIGNMENT);
		jpButtons.add(jbNext);
		//  ------------------- jbPrevious 
		//  ------------------- jbCancel 
		//  ------------------- jbNext 
		//  ------- jpStatusFilter 
		jpStatusFilter.add(jlW2StatusLine);
		//  ----------- jlW2StatusLine 

		// radio-button functional assembly
		ButtonGroup jbgAndOr = new ButtonGroup();
		jbgAndOr.add(jrbAnd);
		jbgAndOr.add(jrbOr);
	}


	private Component datasetDrawOptionsCreation()
	{
		TitledPanel panel = new TitledPanel("Draw Options");
		ButtonGroup bgChartRepresentation = new ButtonGroup();
		panel.setLayout(new GridBagLayout());
		GridBagConstraints gbc = new GridBagConstraints();
		gbc.fill = GridBagConstraints.HORIZONTAL;
		gbc.weightx = 1;

		// row 1, col 1: no chart radio button
		gbc.gridx = 0;
		gbc.gridy = 0;
		gbc.weighty = 1;
		gbc.anchor = GridBagConstraints.NORTHWEST;
		jrbNoChart = new JRadioButton("no chart");
		bgChartRepresentation.add(jrbNoChart);
		jrbNoChart.addActionListener(this);
		panel.add(jrbNoChart, gbc);

		// row 2, col 1: time-based radio button
		gbc.gridy++;
		gbc.anchor = GridBagConstraints.SOUTHWEST;
		jrbTimeBased = new JRadioButton("time-based");
		bgChartRepresentation.add(jrbTimeBased);
		jrbTimeBased.addActionListener(this);
		panel.add(jrbTimeBased, gbc);

		// row 3, col 1: scatter plot check-box
		gbc.gridy++;
		gbc.anchor = GridBagConstraints.NORTHEAST;
		jcbScatterPlot = new JCheckBox("scatter plot");
		jcbScatterPlot.setHorizontalTextPosition(SwingConstants.LEFT);
		jcbScatterPlot.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				updateDrawRaw(((AbstractButton) evt.getSource()).getModel().isSelected());
			}
		});
		panel.add(jcbScatterPlot, gbc);

		// row 4, col 1: histogram radio button
		gbc.gridy++;
		gbc.anchor = GridBagConstraints.SOUTHWEST;
		jrbHistogram = new JRadioButton("histogram");
		bgChartRepresentation.add(jrbHistogram);
		jrbHistogram.addActionListener(this);
		panel.add(jrbHistogram, gbc);

		// row 5, col 1: histogram slices label
		gbc.gridy++;
		gbc.anchor = GridBagConstraints.WEST;
		gbc.fill = GridBagConstraints.NONE;
		panel.add(jlSlices = new JLabel("Number of slices"), gbc);

		// row 5, col 2: histogram slices text field
		gbc.gridx++;
		jtfSlices = new JTextField(6);
		jtfSlices.setMinimumSize(jtfSlices.getPreferredSize());
		panel.add(jtfSlices, gbc);

		// row 6, col 1: quantiles radio button
		gbc.gridx = 0;
		gbc.gridy++;
		gbc.fill = GridBagConstraints.HORIZONTAL;
		gbc.anchor = GridBagConstraints.SOUTHWEST;
		jrbQuantiles = new JRadioButton("quantiles");
		bgChartRepresentation.add(jrbQuantiles);
		jrbQuantiles.addActionListener(this);
		panel.add(jrbQuantiles, gbc);

		// row 7, col 1: quantiles label
		gbc.gridy++;
		gbc.fill = GridBagConstraints.NONE;
		gbc.anchor = GridBagConstraints.WEST;
		panel.add(jlQuantiles = new JLabel("Number of quantiles"), gbc);

		// row 7, col 2: quantiles text field
		gbc.gridx++;
		gbc.weightx = 0;
		jtfQuantiles = new JTextField(6);
		jtfQuantiles.setMinimumSize(jtfQuantiles.getPreferredSize());
		panel.add(jtfQuantiles, gbc);

		// row 2-3, col 2: moving statistics sub-panel
		gbc.gridy = 1;
		gbc.weightx = 1;
		gbc.fill = GridBagConstraints.BOTH;
		gbc.anchor = GridBagConstraints.NORTHWEST;
		gbc.gridheight = 2;
		jpMovingOptions = new TitledPanel("Moving Statistics");		
		panel.add(jpMovingOptions, gbc);
 		jpMovingOptions.setLayout(new GridBagLayout());

		// moving statistics parameters
		gbc.gridx = 0;
		gbc.gridy = 0;
		gbc.gridheight = 1;
		jpMovingOptions.add(new JLabel("Time window (ms)"), gbc);

		gbc.gridy++;
		jpMovingOptions.add(new JLabel("Time step (ms)"), gbc);

		gbc.gridx++;
		gbc.gridy = 0;
		jtfTimeWindow = new JTextField(6);
		jtfTimeWindow.setMinimumSize(jtfTimeWindow.getPreferredSize());
		jtfTimeWindow.setName("timeWindow");
		jtfTimeWindow.addActionListener(this);
		jpMovingOptions.add(jtfTimeWindow, gbc);

		gbc.gridy++;
		jtfStep = new JTextField(6);
		jtfStep.setMinimumSize(jtfStep.getPreferredSize());
		jtfStep.setName("step");
		jtfStep.addActionListener(this);
		jpMovingOptions.add(jtfStep, gbc);

		// moving min
		gbc.gridx = 0;
		gbc.gridy++;
		gbc.gridwidth = 2;
		jcbMovingMin = new JCheckBox("min");
		jcbMovingMin.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				updateDrawMovingMin(((AbstractButton) evt.getSource()).getModel().isSelected());
			}
		});
		jpMovingOptions.add(jcbMovingMin, gbc);

		// moving max
		gbc.gridy++;
		jcbMovingMax = new JCheckBox("max");
		jcbMovingMax.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				updateDrawMovingMax(((AbstractButton) evt.getSource()).getModel().isSelected());
			}
		});
		jpMovingOptions.add(jcbMovingMax, gbc);

		// moving average
		gbc.gridy++;
		jcbMovingAverage = new JCheckBox("average");
		jcbMovingAverage.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				updateDrawMovingAverage(((AbstractButton) evt.getSource()).getModel().isSelected());
			}
		});
		jpMovingOptions.add(jcbMovingAverage, gbc);

		// moving median
		gbc.gridy++;
		jcbMovingMedian = new JCheckBox("median");
		jcbMovingMedian.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				updateDrawMovingMedian(((AbstractButton) evt.getSource()).getModel().isSelected());
			}
		});
		jpMovingOptions.add(jcbMovingMedian, gbc);

		// moving standard deviation
		gbc.gridy++;
		jcbMovingStdDev = new JCheckBox("standard deviation");
		jcbMovingStdDev.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				updateDrawMovingStdDev(((AbstractButton) evt.getSource()).getModel().isSelected());
			}
		});
		jpMovingOptions.add(jcbMovingStdDev, gbc);

		// moving throughput
		gbc.gridy++;
		jcbMovingThroughput = new JCheckBox("throughput");
		jcbMovingThroughput.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				updateDrawMovingThroughput(((AbstractButton) evt.getSource()).getModel().isSelected());
			}
		});
		jpMovingOptions.add(jcbMovingThroughput, gbc);

		return panel;
	}


	private void guiObjectsInitialization() {
		/////////////////// GUI objects initialization
		//
		//  --- jpMainFilter_
		//  ------- jpCenter 
		//  ----------- jpMainFilteringLeft 
		//  --------------- jpFilterTime 
		//  ------------------- jpFilterStartTime 
		//  ----------------------- jtfFilterStartTime 
		//  ----------------------- jcbStartFilterAtFirstEvent 
		jcbStartFilterAtFirstEvent.setSelected(true);
		//		updateStartFilterAtFirstEvent(true);
		//		data.currentDsm.setStartFilterAtFirstEvent(true);

		//  ------------------- jpFilterEndTime 
		//  ----------------------- jlFilterEndTime 
		//  ----------------------- jcbEndFilterAtLastEvent
		//		updateEndFilterAtLastEvent(true);
		jTableFilter.setModel(modelTableFilter);
		jScrollFilterTable.setViewportView(jTableFilter);
		//  --------------- jScrollFilterTable 
		//  --------------- jpAddAndLogic 
		//  ------------------- jbAddFilter 
		jbAddFilter.setText("Add Filter");
		//  ------------------- jpLogic 
		enableFilterLogic(false);
		//  ----------------------- jlLinkingFilter 
		jlLinkingFilter.setText("Logical operator");
		//  ----------------------- jrbAnd 
		jrbAnd.setSelected(true);
		//  ----------------------- jrbOr 
		//  ----------- jpMainFilterRight 
		//  --------------- jpStatAnalysis 
		//  ------------------- jpStAnChoose 
		//  ----------------------- jcbPerformStAn 
		jcbPerformStAn.setText("Perform statistic analysis for field");
		//		jcbPerformStAn.setSelected(true);
		//  ----------------------- jcomboChooseField 
		//		updatejcomboChooseField();
		//  --------------- jpCleaning_ 
		//  ----------------------- jpKCleaningDescription 
		//  --------------------------- jlCleaningKDescription 
		jlCleaningKDescription.setText("Discard values such than |value-average| > K * std_deviation.");
		jlCleaningKDescription.setEnabled(false);
		//  ----------------------- jpCleaningK 
		//  --------------------------- jlCleaningKValue 
		jlCleaningKValue.setText("K      ");
		//  --------------------------- jtfCleaningK 
		jtfCleaningK.setEnabled(false); // unimplemented yet
		//  ----------------------- jpCleaningPercentPointsKept 
		//  --------------------------- jlCleaningPercentPointsKept1 
		jlCleaningPercentPointsKept1.setText("keep at least ");
		//  --------------------------- jtfCleaningPercentPointsKept 
		//  --------------------------- jlCleaningPercentPointsKept2 
		jlCleaningPercentPointsKept2.setText(" % points (Not Implemented Yet)");
		//  ----------------------- jcbDrawRaw 
		jcbDrawRaw.setText("Raw");
		jcbDrawRaw.setSelected(true);
		//  --------------- jpButtons 
		//  ------------------- jbPrevious 
		jbPrevious.setText("Previous");
		//  ------------------- jbCancel 
		jbCancel.setText("Cancel");
		//  ------------------- jbNext 
		jbNext.setText("Finish");
		//  ------- jpStatusFilter 
		//  ----------- jlW2StatusLine 
		jlW2StatusLine.setText("status line ...");
	}



	private void guiObjectsListeners() {
		/////////////////// GUI objects listeners
		//
		//  --- jpMainFilter_
		//  ------- jpCenter 
		//  ----------- jpMainFilteringLeft 
		//  --------------- jpFilterTime 
		//  ------------------- jlFilterTimeInterval 
		//  ------------------- jpFilterStartTime 
		//  ----------------------- jlFilterStartTime 
		//  ----------------------- jtfFilterStartTime 
		jtfFilterStartTime.addKeyListener(new KeyListener() {
			public void keyPressed(KeyEvent evt) {
				// JTextField modification
				jtfFilterStartTime.setBackground(Color.YELLOW);
				if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
					// put focus on the parent
					jtfFilterStartTime.setBackground(Color.WHITE);
					updateFilterStartTime(Long.parseLong(jtfFilterStartTime.getText()));
					w2TimeVerifications();
					jtfFilterStartTime.getParent().requestFocus();
				}
			}
			public void keyReleased(KeyEvent e) {}
			public void keyTyped(KeyEvent e) {}
		});

		jtfFilterStartTime.addFocusListener(new FocusListener() {
			public void focusGained(FocusEvent e) {}

			public void focusLost(FocusEvent e) {
				jtfFilterStartTime.setBackground(Color.WHITE);
				try{
					updateFilterStartTime(Long.parseLong(jtfFilterStartTime.getText()));
				}
				catch (Exception ex) {
					if (jcbStartFilterAtFirstEvent.isSelected()){
						updateFilterStartTime(data.wizardDataset.getFirstEventTime());	// not a number but will take first event time 
					}else{
						;		//	... just ignore it...
					}
				}
				jtfFilterStartTime.getParent().requestFocus();
			} // focusLost
		});

		//  ----------------------- jcbStartFilterAtFirstEvent 
		jcbStartFilterAtFirstEvent.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				updateStartFilterAtFirstEvent(jcbStartFilterAtFirstEvent.isSelected());
				updateFilterStartTime(data.wizardDataset.getFirstEventTime());
			}
		});

		jcbStartFilterAtFirstEvent.addMouseListener(new MouseListener() {
			public void mouseClicked(MouseEvent e) {updateStatus();}
			public void mouseEntered(MouseEvent e) {jlW2StatusLine.setText("Start statistic alaysis at first event");}
			public void mouseExited(MouseEvent e)  {updateStatus();}
			public void mousePressed(MouseEvent e) {updateStatus();}
			public void mouseReleased(MouseEvent e){updateStatus();}
		});
		//  ------------------- jpFilterEndTime 
		//  ----------------------- jtfFilterEndTime 
		jtfFilterEndTime.addKeyListener(new KeyListener() {
			public void keyPressed(KeyEvent evt) {
				// JTextField modification
				jtfFilterEndTime.setBackground(Color.YELLOW);
				if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
					// put focus on the parent
					//					jtfFilterEndTime.setBackground(Color.WHITE);
					updateFilterEndTime(Long.parseLong(jtfFilterEndTime.getText()));
					w2TimeVerifications();
					jtfFilterEndTime.getParent().requestFocus();
				}
			}
			public void keyReleased(KeyEvent e) {}
			public void keyTyped(KeyEvent e) {}
		});

		jtfFilterEndTime.addFocusListener(new FocusListener() {
			public void focusGained(FocusEvent e) {}

			public void focusLost(FocusEvent e) {
				jtfFilterEndTime.setBackground(Color.WHITE);
				try{
					updateFilterEndTime(Long.parseLong(jtfFilterEndTime.getText()));
				}
				catch (Exception ex) {
					if (jcbEndFilterAtLastEvent.isSelected()){
						updateFilterEndTime(data.wizardDataset.getLastEventTime());	// not a number but will take first event time 
					}else{
						;		//	... just ignore it...
					}
				}
				jtfFilterEndTime.getParent().requestFocus();
			} // focusLost
		});
		//  ----------------------- jtfFilterEndTime 
		//  ----------------------- jcbEndFilterAtLastEvent 
		jcbEndFilterAtLastEvent.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				updateEndFilterAtLastEvent(jcbEndFilterAtLastEvent.isSelected());
				updateFilterEndTime(data.wizardDataset.getLastEventTime());
			}
		});

		jcbEndFilterAtLastEvent.addMouseListener(new MouseListener() {
			public void mouseClicked(MouseEvent e) {}

			public void mouseEntered(MouseEvent e) {
				jlW2StatusLine.setText("End statistic alaysis at last event");
			}

			public void mouseExited(MouseEvent e) {
				updateStatus();
			}

			public void mousePressed(MouseEvent e)  {updateStatus();}
			public void mouseReleased(MouseEvent e) {updateStatus();}
		});


		jTableFilter.addMouseListener(new MouseListener() {
			public void mouseClicked(MouseEvent e) {
				int viewRow = jTableFilter.getSelectedRow();
				int viewCol = jTableFilter.getSelectedColumn();
				int rowCount;
				if (viewCol == FILTER_TABLE_DELETE) {
					modelTableFilter.removeRow(viewRow);
					rowCount = jTableFilter.getRowCount();
					enableFilterLogic(rowCount > 1);
				}
			}
			public void mouseEntered(MouseEvent e) {}
			public void mouseExited(MouseEvent e) {}
			public void mousePressed(MouseEvent e) {}
			public void mouseReleased(MouseEvent e) {}
		});

		//  --------------- jpAddAndLogic 
		//  ------------------- jbAddFilter 
		jbAddFilter.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				jbAddFilterActionPerformed(evt);
			}
		});

		//  ------------------- jpLogic 
		//  ----------------------- jlLinkingFilter 
		//  ----------------------- jrbAnd 
		jrbAnd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				data.wizardDataset.getFieldsValuesFilter().setLogicalOp(LogicalEnum.AND);
			}
		});

		//  ----------------------- jrbOr 
		jrbOr.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				data.wizardDataset.getFieldsValuesFilter().setLogicalOp(LogicalEnum.OR);
			}
		});

		//  ----------- jpMainFilterRight 
		//  --------------- jpStatAnalysis 
		//  ------------------- jpStAnChoose 
		//  ----------------------- jcbPerformStAn 
		jcbPerformStAn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				updatePerformStatAn();
			}
		});

		//  ----------------------- jcomboChooseField 
		jcomboChooseField.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				if (null != jcomboChooseField.getSelectedItem()){
					updateField((String) jcomboChooseField.getSelectedItem());
				}
			}

		});
		//  --------------- jpCleaning_ 
		//  ----------------------- jpKCleaningDescription 
		//  --------------------------- jlCleaningKDescription 
		//  ----------------------- jpCleaningK 
		//  --------------------------- jtfCleaningK 
		//  --------------------------- jlCleaningKValue 
		//  ----------------------- jpCleaningPercentPointsKept 
		//  --------------------------- jlCleaningPercentPointsKept1 
		//  --------------------------- jtfCleaningPercentPointsKept 
		//  --------------------------- jlCleaningPercentPointsKept2 
		//  --------------- jpDrawOptions_ 
		//  ------------------- jcbPerformDraw 

		//  ------------------- jpDatasetDrawOptions 
		//  ----------------------- jcbDrawRaw 
		jcbDrawRaw.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				updateDrawRaw(jcbDrawRaw.isSelected());
			}
		});
		//  ------------------- jpSpareRight 
		//  --------------- jpButtons 
		//  ------------------- jbPrevious 
		jbPrevious.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				jbPreviousActionPerformed(evt);
			}
		});

		//  ------------------- jbCancel 
		jbCancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				jbCancelActionPerformed(evt);
			}
		});

		//  ------------------- jbNext 
		jbNext.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				jbFinishActionPerformed(evt);
			}
		});
	}


	private void updatePerformStatAn() {
		if (null == control.w2View){
			return;				// while Wizard2MainView Constructor is being called
		}
		updatePerformStatAnalysis(jcbPerformStAn.isSelected());
	}


	private void updatePerformStatAnalysis(boolean _performStatAnalysis) {
		if (null == control.w2View){
			return;				// while Wizard2MainView Constructor is being called
		}
		// update w1Dataset
		data.wizardDataset.setPerformStatAnalysis(_performStatAnalysis);
		data.wizardDataset.setPerformDraw(_performStatAnalysis && data.wizardDataset.isPerformDraw());

		// update gui
		jcbPerformStAn.setSelected(data.wizardDataset.isPerformStatAnalysis());
		jrbNoChart.setSelected(!data.wizardDataset.isPerformDraw());
		updatejcomboChooseField();
		jcomboChooseField.setEnabled(data.wizardDataset.isPerformStatAnalysis());
		jrbNoChart.setEnabled(data.wizardDataset.isPerformStatAnalysis());
	}


	private void updateField(String field) {
		LogAndDebug.trace(" wizardDataset: adding field \"" + field + "\"");
		data.wizardDataset.addField(field);
		updateStatus();
	}

	///////////////////////////
	/**
	 * @param FilterStartTime
	 * 
	 * updates w1Dataset.filterStartTime according to w1Dataset.StartFilterAtFirstEvent
	 * then updates gui
	 */	
	private void updateFilterStartTime(long filterStartTime) {
		if (null == control.w2View){
			return;				// while Wizard2MainView Constructor is being called
		}

		// update w1Dataset
		if (data.wizardDataset.isStartFilterAtFirstEvent()){
			// should data.currentDsm.get here only when initializing Wizard2MainView
			data.wizardDataset.setFilterStartTime(data.wizardDataset.getFirstEventTime());
		}else{
			data.wizardDataset.setFilterStartTime(filterStartTime);
		}
		w2TimeVerifications();
	}



	/**
	 * @param b boolean: value to set on StartFilterAtFirstEvent
	 * 
	 * updates currentDsm.StartFilterAtFirstEvent
	 * updates Gui according with filterStartTime and FirstEventTime (form data.currentGsm)
	 */
	private void updateStartFilterAtFirstEvent(boolean b) {
		if (null == control.w2View){
			return;				// while Wizard2MainView Constructor is being called
		}
		// update w1Dataset
		if (null == data.wizardDataset){
			return;
		}				// during ReportManager constructor (constructing)
		data.wizardDataset.setStartFilterAtFirstEvent(b);
		updateFilterStartTime(data.wizardDataset.getFirstEventTime());

		// update gui
		String tfText;
		b = data.wizardDataset.isStartFilterAtFirstEvent(); // to make sure that StartFilterAtFirstEvent has been updated
		if (b){
			tfText = "<first event>";
			tfText = String.valueOf(data.wizardDataset.getFirstEventTime());

		}else{ 
			tfText = String.valueOf(data.wizardDataset.getFilterStartTime());
		}
		jtfFilterStartTime.setText(tfText);
		jtfFilterStartTime.setEnabled(!b);
		//		jcbStartFilterAtFirstEvent.setText("First event (" + String.valueOf(data.currentDsm.getFirstEventTime()) + " ms)");
		jcbStartFilterAtFirstEvent.setText("First event");
		jcbStartFilterAtFirstEvent.setSelected(b);
	}


	/////////////////////////////

	/**
	 * @param FilterEndTime
	 * 
	 * updates w1Dataset.filterEndTime according to w1Dataset.EndFilterAtLastEvent
	 * then updates gui
	 */	
	private void updateFilterEndTime(long filterEndTime) {
		if (null == control.w2View){
			return;				// while Wizard2MainView Constructor is being called
		}

		// update w1Dataset
		if (data.wizardDataset.isEndFilterAtLastEvent()){
			// should data.currentDsm.get here only when initializing Wizard2MainView
			data.wizardDataset.setFilterEndTime(data.wizardDataset.getLastEventTime());
		}else{
			data.wizardDataset.setFilterEndTime(filterEndTime);
		}
		w2TimeVerifications();
	}



	/**
	 * @param b boolean: value to set on EndFilterAtLastEvent
	 * 
	 * updates w1Dataset.EndFilterAtLastEvent
	 * updates Gui according with filterEndTime and LastEventTime (form data.currentGsm)
	 */
	private void updateEndFilterAtLastEvent(boolean b) {
		if (null == control.w2View){
			return;				// while Wizard2MainView Constructor is being called
		}
		// update w1Dataset
		data.wizardDataset.setEndFilterAtLastEvent(b);
		updateFilterEndTime(data.wizardDataset.getLastEventTime());

		// update gui
		String tfText;
		b = data.wizardDataset.isEndFilterAtLastEvent(); // to make sure that EndFilterAtLastEvent has been updated
		if (b){
			//			tfText = "<last event>";
			tfText = String.valueOf(data.wizardDataset.getLastEventTime());
		}else{ 
			tfText = String.valueOf(data.wizardDataset.getFilterEndTime());
		}
		jtfFilterEndTime.setText(tfText);
		jtfFilterEndTime.setEnabled(!b);
		//		jcbEndFilterAtLastEvent.setText("Last event (" + String.valueOf(data.currentDsm.getLastEventTime()) + " ms)");
		jcbEndFilterAtLastEvent.setText("Last event");
		jcbEndFilterAtLastEvent.setSelected(b);
	}


	///////////////////////////

	private void updatejcomboChooseField() {
		if (null == control.w2View){
			return;				// while Wizard2MainView Constructor is being called
		}
		if (null == data.wizardDataset){
			return;
		}
		if (0 == data.wizardDataset.getDatasources().size()){
			return;
		}
		jcomboChooseField.removeAllItems();
		for (String label : data.getFieldLabelsWithoutDate(data.wizardDataset))
		{
			jcomboChooseField.addItem(label);
		}
		if (data.wizardDataset.getEventType().equals(
			org.ow2.clif.storage.api.ActionEvent.EVENT_TYPE_LABEL))
		{
			jcomboChooseField.setSelectedItem("duration");
		}
		updateStatus();
	}


	// does nothing (not implemented)
	private void updateStatisticsCleaning() {  // related to kCleaning and PercentPointsKept
		if (null == control.w2View){
			return;				// while Wizard2MainView Constructor is being called
		}
	}


	private void updatePerformDraw(boolean _performDraw) {
		if (null == control.w2View){
			return;				// while Wizard2MainView Constructor is being called
		}
		// update w1Dataset
		data.wizardDataset.setPerformDraw(_performDraw);
		updateDrawOptions();
	}


	protected void updateDrawRaw(boolean value) {
		LogAndDebug.trace("("+value+")");
		if (null == control.w2View){return;}	// while Wizard2MainView Constructor is being called
		data.wizardDataset.setDrawRaw(value);
	}


	private void updateDrawMovingMin(boolean value) {
		//		LogAndDebug.trace("("+value+")");
		if (null == control.w2View){return;}	// while Wizard2MainView Constructor is being called
		data.wizardDataset.setDrawMovingMin(value);
	}

	private void updateDrawMovingMax(boolean value) {
		//		LogAndDebug.trace("("+value+")");
		if (null == control.w2View){return;}	// while Wizard2MainView Constructor is being called
		data.wizardDataset.setDrawMovingMax(value);
	}

	private void updateDrawMovingAverage(boolean value) {
		//		LogAndDebug.trace("("+value+")");
		if (null == control.w2View){return;}	// while Wizard2MainView Constructor is being called
		data.wizardDataset.setDrawMovingAverage(value);
	}

	private void updateDrawMovingMedian(boolean value) {
		//		LogAndDebug.trace("("+value+")");
		if (null == control.w2View){return;}	// while Wizard2MainView Constructor is being called
		data.wizardDataset.setDrawMovingMedian(value);
	}

	private void updateDrawMovingStdDev(boolean value) {
		//		LogAndDebug.trace("("+value+")");
		if (null == control.w2View){return;}	// while Wizard2MainView Constructor is being called
		data.wizardDataset.setDrawMovingStdDev(value);
	}

	private void updateDrawMovingThroughput(boolean value) {
		//		LogAndDebug.trace("("+value+")");
		if (null == control.w2View){return;}	// while Wizard2MainView Constructor is being called
		data.wizardDataset.setDrawMovingThroughput(value);
	}


	private void updateStatus() {
		if (null == control.w2View){
			return;				// while Wizard2MainView Constructor is being called
		}
		String msg = " ";
		if (null == data.wizardDataset){
			msg += data.wizardDataset.getDatasetType() + " " + data.wizardDataset.getName();
		}
		jlW2StatusLine.setText(msg);
	}


	private void updateDrawOptions()
	{
		if (jrbNoChart.isSelected())
		{
			enableDrawOptions(false, false, false);
		}
		else
		{
			enableDrawOptions(
				jrbTimeBased.isSelected(),
				jrbHistogram.isSelected(),
				jrbQuantiles.isSelected());
		}
	}


	private void enableDrawOptions(boolean timeBased, boolean histogram, boolean quantiles)
	{
		SwingHelper.setEnabled(jpMovingOptions, timeBased);
		jcbScatterPlot.setEnabled(timeBased);
		jlSlices.setEnabled(histogram);
		jtfSlices.setEnabled(histogram);
		jlQuantiles.setEnabled(quantiles);
		jtfQuantiles.setEnabled(quantiles);
	}


	/**
	 * w2TimesVerification()
	 * colors background on RED for those times that are incorrect
	 */
	private void w2TimeVerifications() {
		boolean enableFinish = true;

		if (data.wizardDataset.getFirstEventTime() > data.wizardDataset.getLastEventTime()){
			enableFinish = false;	// Houston, we have a problem....
			LogAndDebug.err("*** ERROR *** data corrupted\n" +
					"	firstEventTime ("+data.wizardDataset.getFirstEventTime()+") > lastEventTime ("+data.wizardDataset.getLastEventTime() + ")\n" +
			"	impossible to go further.");
			jbNext.setEnabled(enableFinish);
			return;
		}
		jbNext.setEnabled(enableFinish);

		// filterStartTime < lasteventTime
		if (data.wizardDataset.getFilterStartTime() > data.wizardDataset.getLastEventTime()){
			jtfFilterStartTime.setBackground(Color.RED);
			LogAndDebug.err("*** ERROR *** filterStartTime ("+data.wizardDataset.getFilterStartTime()+") > lastEventTime("+data.wizardDataset.getLastEventTime()+")");
		}else{
			if (data.wizardDataset.isStartFilterAtFirstEvent()){
				//				updateStartFilterAtFirstEvent(data.w1Dataset.isStartFilterAtFirstEvent());
			}else{
				jtfFilterStartTime.setBackground(Color.WHITE);
			}
		}

		// filterStartTime ? filterEndTime
		if (data.wizardDataset.getFilterStartTime() > data.wizardDataset.getFilterEndTime()){
			jtfFilterEndTime.setBackground(Color.RED);
			LogAndDebug.err("*** ERROR *** filterStartTime ("+data.wizardDataset.getFilterStartTime()+") > filterEndTime("+data.wizardDataset.getFilterEndTime()+")");
		}else{
			if (data.wizardDataset.isEndFilterAtLastEvent()){
				//				updateEndFilterAtLastEvent(data.currentDsm.isStartDrawAtFilterStartTime());
			}else{
				jtfFilterEndTime.setBackground(Color.WHITE);
			}
		}


		updateDrawGuiEnables();
	}


	private void updateDrawGuiEnables(){
		// enable performStatAnalysis
		this.jcbPerformStAn.setEnabled(true);

		// disable all
		this.jrbNoChart.setEnabled(false);

		// enable if needed

		if (!data.wizardDataset.isStartFilterAtFirstEvent())	{this.jtfFilterStartTime.setEnabled(true);}
		if (!data.wizardDataset.isEndFilterAtLastEvent())		{this.jtfFilterEndTime.setEnabled(true);}
		if (data.wizardDataset.isPerformStatAnalysis() )		{this.jrbNoChart.setEnabled(true);}
	}


	// listeners
	@Override
	public void actionPerformed(ActionEvent event) {
		Object source = event.getSource();

		String className = source.getClass().getSimpleName();
		if (className.equals("JTextField")){
			actionJTextFieldPerformed(event);
		}else if (className.equals("JCheckBox")){
			actionJCheckBoxPerformed(event);
		}else if (className.equals("JRadioButton")){
			actionJRadioButtonPerformed(event);
		}else{
			throw new Error("actionPerformed: cannot recognize " + className + " object.");
		}	
	}


	private void actionJRadioButtonPerformed(ActionEvent event)
	{
		updateDrawOptions();
	}


	private void actionJTextFieldPerformed(ActionEvent event) {
		Object source = event.getSource();
		String commandName = ((JTextField) source).getName();
		String text = ((JTextField) source).getText();

		LogAndDebug.trace("You have writen on \""+ commandName +"\" ... ");
		if(source == jtfStep) {
			data.wizardDataset.setStep(Integer.parseInt(text));
		}else if(commandName.equals("timeWindow")){
			data.wizardDataset.setTimeWindow(Integer.parseInt(text));
		}else if(commandName.equals("step")) {
			data.wizardDataset.setStep(Integer.parseInt(text));
		}else if(commandName.equals("maxPointsNumber")) {
			data.wizardDataset.setMaxPointsNb(Integer.parseInt(text));
		} else {
			LogAndDebug.trace(".... unknown command");
		}
		LogAndDebug.trace(" <= \""+ text +"\". ");
	}


	private void actionJCheckBoxPerformed(ActionEvent event) {
		Object source = event.getSource();
		String commandName = ((AbstractButton) source).getName();
//		boolean value = ((AbstractButton) source).isSelected();

		LogAndDebug.trace("actionJCheckBoxPerformed: You have clicked on \""+ commandName +"\" ... ");
		LogAndDebug.log(".... unknown command");
		return;
	}



	//   Methods called by listeners
	private void jbPreviousActionPerformed(ActionEvent evt) {
//		data.wizardDataset.clearDatasources();
		control.nextState(Phases.WIZARD_1_SELECTION);
	}


	private void jbCancelActionPerformed(ActionEvent evt) {
		data.wizardDataset = null;
		control.nextState(Phases.REPORT_MANAGER);
	}


	private void jbFinishActionPerformed(ActionEvent evt) {
		if (0 == data.wizardDataset.getDatasources().size()){
			LogAndDebug.err("ERROR: dataset has no datasources...");
			return;
		}
		if (updateModelFromGui())
		{
			control.nextState(Phases.REPORT_MANAGER);
		}
	}
	

	/**
	 * update model while verifying correct values
	 * @return false if there is an error or missing value
	 *         true if all seems OK
	 */
	private boolean updateModelFromGui(){
		// filter start and end times
		data.wizardDataset.setStartFilterAtFirstEvent(jcbStartFilterAtFirstEvent.isSelected());
		data.wizardDataset.setEndFilterAtLastEvent(jcbEndFilterAtLastEvent.isSelected());
		data.wizardDataset.setFilterStartTime(DataAccess.longParse(jtfFilterStartTime.getText(), data.wizardDataset.getFilterStartTime()));
		data.wizardDataset.setFilterEndTime(DataAccess.longParse(jtfFilterEndTime.getText(), data.wizardDataset.getFilterEndTime()));
		
		// fields values filter		
		FieldsValuesFilter fvf = checkFilterTable();
		data.wizardDataset.setFieldsValuesFilter(fvf);
		if (null == data.wizardDataset.getFieldsValuesFilter())
		{
			JOptionPane.showMessageDialog(
				this,
				"Malformed filter. Please fix it",
				"Warning",
				JOptionPane.WARNING_MESSAGE);
			return false;
		}
		try
		{
			// statistic analysis
			data.wizardDataset.setPerformStatAnalysis(jcbPerformStAn.isSelected());
			data.wizardDataset.addField((String) jcomboChooseField.getSelectedItem());
	
			// draw options
			data.wizardDataset.setPerformDraw(! jrbNoChart.isSelected());
			data.wizardDataset.setPerformTimeChart(jrbTimeBased.isSelected());		
			data.wizardDataset.setDrawRaw(jcbScatterPlot.isSelected());
			data.wizardDataset.setTimeWindow(Integer.parseInt(jtfTimeWindow.getText()));
			data.wizardDataset.setStep(Integer.parseInt(jtfStep.getText()));
			data.wizardDataset.setDrawMovingMin(jcbMovingMin.isSelected());
			data.wizardDataset.setDrawMovingMax(jcbMovingMax.isSelected());
			data.wizardDataset.setDrawMovingAverage(jcbMovingAverage.isSelected());
			data.wizardDataset.setDrawMovingMedian(jcbMovingMedian.isSelected());
			data.wizardDataset.setDrawMovingStdDev(jcbMovingStdDev.isSelected());
			data.wizardDataset.setDrawMovingThroughput(jcbMovingThroughput.isSelected());
			data.wizardDataset.setPerformHistogram(jrbHistogram.isSelected());
			data.wizardDataset.setSlicesNb(Integer.parseInt(jtfSlices.getText()));
			data.wizardDataset.setPerformQuantiles(jrbQuantiles.isSelected());
			data.wizardDataset.setQuantiles(Integer.parseInt(jtfQuantiles.getText()));
			return true;
		}
		catch (NumberFormatException ex)
		{
			JOptionPane.showMessageDialog(
				this,
				"Incorrect integer value. Please fix it.",
				"Warning",
				JOptionPane.WARNING_MESSAGE);
			return false;
		}
	}


	/**
	 * Check is the table is filled
	 * 
	 * @return true is the filterTable is correct(all fields are filled
	 */
	public FieldsValuesFilter checkFilterTable() {
		FieldsValuesFilter filterList;
		try {
			// check if all the cells are filled : OK
			int rowCount = modelTableFilter.getRowCount();
			int colCount = modelTableFilter.getColumnCount();
			for (int i = 0; i < rowCount; i++) {
				// don't take the last column which is delete column
				for (int j = 0; j < colCount - 1; j++) {
					if ((modelTableFilter.getValueAt(i, j).toString()).equals("")) {
						return null;
					}
				}
			}

			// ADD values in filter list
			filterList = new FieldsValuesFilter();
			LogicalEnum logOp = jrbOr.isSelected()? LogicalEnum.OR : LogicalEnum.AND;
			filterList.setLogicalOp(logOp);

			for (int i = 0; i < rowCount; i++) {
				String keyword = modelTableFilter.getValueAt(i, FILTER_TABLE_VALUE).toString();
				String operator = modelTableFilter.getValueAt(i, FILTER_TABLE_OPERATOR).toString();
				String field = modelTableFilter.getValueAt(i, FILTER_TABLE_FIELD).toString();
				FieldFilterAndOp ffo = new FieldFilterAndOp(field, keyword, operator);
				filterList.addExpression(ffo);
			}
			return filterList;
			// else NO GOOD
		}
		catch (Exception e) {
			LogAndDebug.err(" *** Exception: " + e);
			return null;
		}
	}


	private void jbAddFilterActionPerformed(ActionEvent evt) {
		Vector<String> addSel = new Vector<String>();
		addSel.add("");
		addSel.add("");
		addSel.add("");
		addSel.add("");
		modelTableFilter.addRow(addSel);
		initFilterRendererAndEditor();
		enableFilterLogic(modelTableFilter.getRowCount() > 1);
	}

	private void enableFilterLogic(boolean enable)
	{
		for (Component comp : jpLogic.getComponents())
		{
			comp.setEnabled(enable);
		}
	}


	// Initialization
	private void initFilterRendererAndEditor() {
		TableColumnModel myColumnModel = jTableFilter.getColumnModel();
		TableColumn myTableColumn;
		myTableColumn = myColumnModel.getColumn(FILTER_TABLE_FIELD);
		JComboBox<String> fieldComboBox = new JComboBox<String>();
		try {
			// get the first blade event
			if (data.wizardDataset.getDatasources().size() > 0){
				SortedSet<String> fields = data.getFieldLabels(data.wizardDataset);
				for (String label : fields) {
					fieldComboBox.addItem(label);
				}
			}

		}
		catch (Exception e) {
			LogAndDebug.err(
					"SimpleDataset.java / initFilterRendererAndEditor\n" + "Exception: "+ e);
		}
		fieldComboBox.addItemListener(new java.awt.event.ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				if (e.getStateChange() == 1) {
					// LogAndDebug.trace("item state changed" + e.getItem().toString());
				}
			}
		});

		myTableColumn.setCellEditor(new DefaultCellEditor(fieldComboBox));
		myTableColumn = myColumnModel.getColumn(FILTER_TABLE_OPERATOR);
		JComboBox<String> operatorComboBox = new JComboBox<String>(FieldsValuesFilter.OPERATORS);
		operatorComboBox.addItemListener(new java.awt.event.ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				if (e.getStateChange() == 1) {
					// LogAndDebug.trace("item state changed" + e.getItem().toString());
				}
			}
		});
		myTableColumn.setCellEditor(new DefaultCellEditor(operatorComboBox));
		myTableColumn = myColumnModel.getColumn(FILTER_TABLE_DELETE);
		myTableColumn.setCellRenderer(new TableCellRenderer() {
			public Component getTableCellRendererComponent(
					JTable table, 
					Object value,
					boolean isSelected, 
					boolean hasFocus, 
					int row, 
					int column) {
				JLabel jlDelete = new JLabel();
				jlDelete.setIcon(data.DELETE_ICON);
				jlDelete.setOpaque(true);
				jlDelete.setHorizontalAlignment(JLabel.CENTER);
				if (isSelected) {
					Color bgrd = table.getSelectionBackground();
					jlDelete.setBackground(bgrd);
				} else {
					jlDelete.setBackground(Color.WHITE);
				}
				jlDelete.setBorder(null);
				return jlDelete;
			}
		});
		myTableColumn.setPreferredWidth(FILTER_TABLE_DELETE);
	}


	/**
	 * initializing currenDsm depends on previous state:
	 *  - when "Wizard step 1": copy currentDs on w1Dataset and pre-initialization of some fields
	 *  - when"Report Manager": copy selected dataset on w1Dataset
	 * @param previousState
	 */
	public void initializeFrom(Phases previousState) {
		switch(previousState){
		case INIT:
		case REPORT_MANAGER:
			dataInit();
			jbPrevious.setEnabled(true);
			jbNext.setEnabled(true);
			break;
		case WIZARD_1_SELECTION:
			//			data.createw1Dataset();
			preInitw1Dataset();
			clearTableFilter();
			jbPrevious.setEnabled(true);
			jbNext.setEnabled(true);
			break;
		case WIZARD_2_FILTERING:
			break;
		case EXIT:
		default:
			;
		}
	}


	private void clearTableFilter()
	{
		while(modelTableFilter.getRowCount() > 0){
			modelTableFilter.removeRow(0);
		}
	}


	/**
	 *  default configuration of a new w1Dataset after Wizard's step 1
	 */
	private void preInitw1Dataset() {
		data.updateWizardDataset();
		data.wizardDataset.setFilterStartTime(data.wizardDataset.getFirstEventTime());
		data.wizardDataset.setStartFilterAtFirstEvent(true);
		data.wizardDataset.setFilterEndTime(data.wizardDataset.getLastEventTime());
		data.wizardDataset.setEndFilterAtLastEvent(true);
		data.wizardDataset.setPerformStatAnalysis(true);
		data.wizardDataset.setDrawRaw(true);
		data.wizardDataset.setMaxPointsNb(-1);
		data.wizardDataset.setTimeWindow(10000);
		data.wizardDataset.setStep(10000);

		Section section = data.wizardDataset.getSection();
		if (section == null)
		{
			data.wizardDataset.setPerformDraw(true);
			data.wizardDataset.setPerformTimeChart(true);
			data.wizardDataset.setSlicesNb(20);
			data.wizardDataset.setQuantiles(20);
		}
		else
		{
			ChartRepresentation chartType = section.getChartRepresentation();
			data.wizardDataset.setPerformDraw(chartType != null);
			data.wizardDataset.setSlicesNb(section.getSlicesNb());
			data.wizardDataset.setQuantiles(section.getQuantilesNb());
			if (chartType != null)
			{
				data.wizardDataset.setPerformTimeChart(chartType.equals(ChartRepresentation.TIME_BASED));
				data.wizardDataset.setPerformHistogram(chartType.equals(ChartRepresentation.HISTOGRAM));
				data.wizardDataset.setPerformQuantiles(chartType.equals(ChartRepresentation.QUANTILE));
			}
		}
		updateGuiFromWizardDataset();
	}


	/////////////////// GUI objects declaration
	//

	//  --- jpMainFilter_
	private JPanel mainFilterPanel;
	//  ------- jpCenter 
	private JSplitPane jspCenter;
	//  ----------- jpMainFilteringLeft 
	private TitledPanel jpMainFilteringLeft;
	//  ----------------------- jtfFilterStartTime 
	private JTextField jtfFilterStartTime;
	//  ----------------------- jcbStartFilterAtFirstEvent 
	private JCheckBox jcbStartFilterAtFirstEvent;
	//  ----------------------- jtfFilterEndTime 
	private JTextField jtfFilterEndTime;
	//  ----------------------- jcbEndFilterAtLastEvent 
	private JCheckBox jcbEndFilterAtLastEvent;
	//  --------------- jScrollFilterTable 
	private JScrollPane jScrollFilterTable;
	//  --------------- jpAddAndLogic 
	private JPanel jpAddAndLogic;
	//  ------------------- jbAddFilter 
	private JButton jbAddFilter;
	//  ------------------- jpLogic 
	private JPanel jpLogic;
	//  ----------------------- jlLinkingFilter 
	private JLabel jlLinkingFilter;
	//  ----------------------- jrbAnd 
	private JRadioButton jrbAnd;
	//  ----------------------- jrbOr 
	private JRadioButton jrbOr;
	//  ----------- jpMainFilterRight 
	private JPanel jpMainFilterRight;
	//  --------------- jpStatAnalysis 
	private TitledPanel jpStatAnalysis;
	//  ------------------- jpStAnChoose 
	private JPanel jpStAnChoose;
	//  ----------------------- jcbPerformStAn 
	private JCheckBox jcbPerformStAn;
	//  ----------------------- jcomboChooseField 
	private JComboBox<String> jcomboChooseField;
	//  --------------- jpCleaning_ 
	private JPanel jpCleaning_;
	//  ----------------------- jpKCleaningDescription 
	private JPanel jpKCleaningDescription;
	//  --------------------------- jlCleaningKDescription 
	private JLabel jlCleaningKDescription;
	//  ----------------------- jpCleaningK 
	private JPanel jpCleaningK;
	//  --------------------------- jtfCleaningK 
	private JTextField jtfCleaningK;
	//  --------------------------- jlCleaningKValue 
	private JLabel jlCleaningKValue;
	//  ----------------------- jpCleaningPercentPointsKept 
	private JPanel jpCleaningPercentPointsKept;
	//  --------------------------- jlCleaningPercentPointsKept1 
	private JLabel jlCleaningPercentPointsKept1;
	//  --------------------------- jtfCleaningPercentPointsKept 
	private JTextField jtfCleaningPercentPointsKept;
	//  --------------------------- jlCleaningPercentPointsKept2 
	private JLabel jlCleaningPercentPointsKept2;
	//  --------------- jpDrawOptions_ 
	private Component jpDrawOptions;
	//  ----------------------- 
	private JCheckBox jcbDrawRaw;
	//  --------------- jpButtons 
	private JPanel jpButtons;
	//  ------------------- jbPrevious 
	private JButton jbPrevious;
	//  ------------------- jbCancel 
	private JButton jbCancel;
	//  ------------------- jbNext 
	private JButton jbNext;
	//  ------- jpStatusFilter 
	private JPanel jpStatusFilter;
	//  ----------- jlW2StatusLine 
	private JLabel jlW2StatusLine;

	private JTable jTableFilter;
	private DefaultTableModel modelTableFilter;
	//	private JList jlChooseField;

	private JPanel jpMovingOptions;
	private JRadioButton jrbNoChart;
	private JRadioButton jrbTimeBased;
	private JRadioButton jrbHistogram;
	private JRadioButton jrbQuantiles;
	private JCheckBox jcbScatterPlot;
	private JCheckBox jcbMovingMin;
	private JCheckBox jcbMovingMax;
	private JCheckBox jcbMovingAverage;
	private JCheckBox jcbMovingMedian;
	private JCheckBox jcbMovingStdDev;
	private JCheckBox jcbMovingThroughput;
	private JLabel jlSlices;
	private JLabel jlQuantiles;
	private JTextField jtfSlices;
	private JTextField jtfQuantiles;
	private JTextField jtfTimeWindow;
	private JTextField jtfStep;
}
