/*
* CLIF is a Load Injection Framework
* Copyright (C) 2011 France Telecom R&D
* Copyright (C) 2016 Orange SA
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
* Contact: clif@ow2.org
*/

package org.ow2.clif.analyze.lib.graph.util;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;
import java.util.SortedSet;
import java.util.TreeSet;
import org.ow2.clif.storage.api.BladeDescriptor;
import org.ow2.clif.storage.api.BladeEvent;
import org.ow2.clif.storage.api.BladeFilter;
import org.ow2.clif.storage.api.EventFilter;
import org.ow2.clif.storage.api.LifeCycleEvent;
import org.ow2.clif.storage.api.StorageRead;
import org.ow2.clif.storage.api.TestDescriptor;
import org.ow2.clif.storage.api.TestFilter;
import org.ow2.clif.storage.lib.util.NameBladeFilter;
import org.ow2.clif.supervisor.api.ClifException;

/**
 * Wraps and adds some utility methods to a StorageRead implementation.
 * 
 * @author Bruno Dillenseger
 * @author Tomas Perez Segovia
 */
public class StorageReadHelper implements StorageRead
{
	static private final int LIFECYCLE_RUNNING_ROW = 3;

	private StorageRead srItf;


	/**
	 * Creates a new front-end on top of the provided StorageRead
	 * back-end implementation, providing extra utility methods.
	 * @param backend
	 */
	public StorageReadHelper(StorageRead backend)
	{
		srItf = backend;
	}

	///////////////////////////
	// StorageRead interface //
	///////////////////////////

	@Override
	public TestDescriptor[] getTests(TestFilter filter)
	throws ClifException
	{
		return srItf.getTests(filter);
	}

	@Override
	public BladeDescriptor[] getTestPlan(String testName, BladeFilter filter)
	throws ClifException
	{
		return srItf.getTestPlan(testName, filter);
	}

	@Override
	public Properties getBladeProperties(String testName, String bladeId)
	throws ClifException
	{
		return srItf.getBladeProperties(testName, bladeId);
	}

	@Override
	public String[] getEventFieldLabels(
		String testName,
		String bladeId,
		String eventTypeLabel)
	{
		return srItf.getEventFieldLabels(testName, bladeId, eventTypeLabel);
	}

	@Override
	public Serializable getEventIterator(
		String testName,
		String bladeId,
		String eventTypeLabel,
		EventFilter filter)
	throws ClifException
	{
		return srItf.getEventIterator(testName, bladeId, eventTypeLabel, filter);
	}

	@Override
	public BladeEvent[] getNextEvents(Serializable iteratorKey, int count)
	throws ClifException
	{
		return srItf.getNextEvents(iteratorKey, count);
	}

	@Override
	public void closeEventIterator(Serializable iteratorKey)
	{
		srItf.closeEventIterator(iteratorKey);
	}

	@Override
	public BladeEvent[] getEvents(
		String testName,
		String bladeId,
		String eventTypeLabel,
		EventFilter filter,
		long fromIndex,
		int count)
	throws ClifException
	{
		return srItf.getEvents(testName, bladeId, eventTypeLabel, filter, fromIndex, count);
	}

	@Override
	public long countEvents(
		String testName,
		String bladeId,
		String eventTypeLabel,
		EventFilter filter)
	throws ClifException
	{
		return srItf.countEvents(testName, bladeId, eventTypeLabel, filter);
	}

	////////////////////////////////////////////////
	// extra utility methods based on StorageRead //
	////////////////////////////////////////////////

	/**
	 * Get the tests' names.
	 * 
	 * @return an array that contains the tests' names
	 */
	public SortedSet<String> getTestsNames()
	{
		SortedSet<String> testsNames = null;
		try
		{
			TestDescriptor[] tests = getTests(null);
			testsNames = new TreeSet<String>();
			for (TestDescriptor desc : tests)
			{
				testsNames.add(desc.getName());
			}
		}
		catch (ClifException e)
		{
			e.printStackTrace(System.err);
		}
		return testsNames;
	}


	public SortedSet<String> getBladesIds(String testName)
	{
		SortedSet<String> bladesIds = null;
		try
		{
			BladeDescriptor[] blades = getTestPlan(testName, null);
			bladesIds = new TreeSet<String>();
			for (BladeDescriptor desc : blades)
			{
				bladesIds.add(desc.getId());
			}
		}
		catch (ClifException e)
		{
			e.printStackTrace(System.err);
		}
		return bladesIds;
	}


	public Object[][] getBladeEventsValues(
		BladeEvent[] bladeEventList,
		String xFieldLabel, String yFieldLabel)
	{
		Object[][] values = null;
		ArrayList<Object> valTemp1 = new ArrayList<Object>();
		ArrayList<Object> valTemp2 = new ArrayList<Object>();
		values = new Object[2][];
		for (int i = 0; i < bladeEventList.length; i++)
		{
			valTemp1.add(bladeEventList[i].getFieldValue(xFieldLabel));
			valTemp2.add(bladeEventList[i].getFieldValue(yFieldLabel));
		}
		values[0] = valTemp1.toArray();
		values[1] = valTemp2.toArray();

		return values;
	}


	public BladeEvent[] getBladeEvents(
		String testName,
		String bladeId,
		String eventTypeLabel,
		Object object)
	{
		try
		{
			//get all the blade events.with no filter(null) from index 0 and all the value since count is -1 
			BladeEvent[] bladeEventList = getEvents(testName, bladeId, eventTypeLabel, null, 0, -1);
			return bladeEventList;
		} 
		catch (ClifException e)
		{
			e.printStackTrace(System.err);
		}
		return null;
	}


	public long getLifecycleCompleted(String testName, String bladeId)
	{
		long time = 0;
		try
		{
			BladeEvent[] events = getEvents(testName, bladeId, LifeCycleEvent.EVENT_TYPE_LABEL, null, -1, 1);
			time = events[0].getDate();
		}
		catch (ClifException e)
		{
			e.printStackTrace(System.err);
		}
		return time;
	}


	public long getLifecycleRunning(String testName, String bladeId)
	{
		long time = 0;
		try{
			BladeEvent[] events = getEvents(
				testName,
				bladeId,
				LifeCycleEvent.EVENT_TYPE_LABEL,
				null,
				LIFECYCLE_RUNNING_ROW,
				1);
			time = events[0].getDate();
		}
		catch (ClifException e)
		{
			e.printStackTrace(System.err);
		}
		return time;
	}


	public Object[][] getAllFieldsValues(
		String testName,
		String bladeId,
		String eventTypeLabel,
		String xFieldLabel, 
		String yFieldLabel,
		int start,
		int end,
		int count)
	{
		Object[][] values = null;
		ArrayList<Object> valTemp1 = new ArrayList<Object>();
		ArrayList<Object> valTemp2 = new ArrayList<Object>();
		try
		{
			if (count<0)
			{
				count = (int)countEvents(testName, bladeId, eventTypeLabel, null);
			}
			values = new Object[2][];
			BladeEvent[] events = getEvents(testName, bladeId, eventTypeLabel, null, 0, count);
			for (int i = 0; i < events.length; i++)
			{
				valTemp1.add(events[i].getFieldValue(xFieldLabel));
				valTemp2.add(events[i].getFieldValue(yFieldLabel));
			}
			values[0] = valTemp1.toArray();
			values[1] = valTemp2.toArray();
		}
		catch (ClifException e)
		{
			e.printStackTrace(System.err);
		}
		return values;
	}


	public SortedSet<String> getEventsTypeLabels(String testName, String bladeId)
	{
		SortedSet<String> eventsTypeLabels= null;
		try
		{
			BladeDescriptor blade = getTestPlan(testName, new NameBladeFilter(bladeId))[0];
			eventsTypeLabels = new TreeSet<String>(); 
			eventsTypeLabels.addAll(Arrays.asList(blade.getEventTypeLabels()));
		}
		catch (ClifException e)
		{
			e.printStackTrace(System.err);
		}
		return eventsTypeLabels;
	}


	public long getMinTimeFor(String testName, String bladeId)
	{
		long time = 0;
		try
		{
			BladeEvent[] events = getEvents(testName, bladeId, LifeCycleEvent.EVENT_TYPE_LABEL, null, 0, 1);
			time = events[0].getDate();
		}
		catch (ClifException e)
		{
			e.printStackTrace(System.err);
		}
		return time;
	}


	public long getMaxTimeFor(String testName, String bladeId)
	{
		long time = 0;
		try
		{
			BladeEvent[] events = getEvents(testName, bladeId, LifeCycleEvent.EVENT_TYPE_LABEL, null, -1, 1);
			time = events[0].getDate();
		}
		catch (ClifException e)
		{
			e.printStackTrace(System.err);
		}
		return time;
	}


	public List<BladeEvent> getBladeOneEventValues(
		String testName,
		String bladeId,
		String eventTypeLabel)
	{
		try
		{
			BladeEvent[] bladeEventArray = getBladeEvents(testName, bladeId, eventTypeLabel, null);
			return Arrays.asList(bladeEventArray);
		}
		catch (Exception e)
		{
			e.printStackTrace(System.err);
		}
		return null;
	}
}
