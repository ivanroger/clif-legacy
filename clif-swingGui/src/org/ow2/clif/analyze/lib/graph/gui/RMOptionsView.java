/*
* CLIF is a Load Injection Framework
* Copyright (C) 2011 France Telecom R&D
* Copyright (C) 2014, 2016-2017 Orange SA
* 
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
* Contact: clif@ow2.org
*/

package org.ow2.clif.analyze.lib.graph.gui;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.SortedSet;
import java.util.Vector;
import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.ItemEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import javax.swing.AbstractButton;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.DefaultCellEditor;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.TitledBorder;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.event.MenuListener;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import org.ow2.clif.analyze.lib.graph.FieldFilterAndOp;
import org.ow2.clif.analyze.lib.report.Dataset;
import org.ow2.clif.analyze.lib.report.Datasource;
import org.ow2.clif.analyze.lib.report.FieldsValuesFilter;
import org.ow2.clif.analyze.lib.report.FieldsValuesFilter.LogicalEnum;
import org.ow2.clif.analyze.lib.report.LogAndDebug;
import org.ow2.clif.analyze.lib.report.Report;
import org.ow2.clif.analyze.lib.report.Section;
import org.ow2.clif.analyze.lib.report.Section.ChartRepresentation;
import org.ow2.clif.analyze.lib.report.Statistics;
import org.ow2.clif.util.gui.SwingHelper;


/**
 * @author Tomas Perez Segovia
 * @author Bruno Dillenseger
 *
 * Second (middle) part of ReportManager View
 */

public class RMOptionsView extends JPanel implements ActionListener {

	private static final long serialVersionUID = 6569469478240042554L;


	boolean useStatisticalFiltering = false;
	
	// Use for the columns in the filter table
	private final int FILTER_TABLE_FIELD = 0;
	private final int FILTER_TABLE_OPERATOR = 1;
	private final int FILTER_TABLE_VALUE = 2;
	private final int FILTER_TABLE_DELETE = 3;

	private final String FILTER_START_TIME = "Filter Start Time";
	private final String FILTER_END_TIME    = "Filter End Time";
	private final String FIRST_EVENT = "First event";
	private final String FILTER_START_AT_FIRST_EVENT = "Filter Start at first event";
	private final String LAST_EVENT    = "Last event";
	private final String FILTER_END_AT_LAST_EVENT    = "Filter end at last event";
	private final String ADD_FIELD_FILTER = "Add Filter";

	private final String PERCENT_POINTS_KEPT = "Percent Points Kept";
	private final String CLEANING_K = "Cleainig k";

	private final String TIME_WINDOW = "timeWindow";
	private final String STEP = "step";

	private final String SLICES_NUMBER = "number of slices";
	private final String QUANTILES_NUMBER = "number of quantiles";
//	private final String MAX_POINTS_NUMBER = "maxPointsNumber";

	private final String REPORT = "Report";
	private final String SECTION = "Section";
	private final String DATASET = "Dataset";

	private final String REPORT_TITLE = "reportTitle";
	private final String SECTION_TITLE = "sectionTitle";
	private final String DATASET_TITLE = "datasetTitle";

	private final String APPLY = "Apply";

	private final String UNINITALIZED_LABEL = "<uninitialized>";

	// color for unimplemented functionalities
	final Color UNIMPLEMENTED_FOREGROUND_COLOR = new Color(107, 108, 153);	// BLUE-GRAY //
	final Color UNIMPLEMENTED_BACKGROUD_COLOR = new Color(192,192,192); 	// lightgrey //



	private LogicalEnum logicalOp;	// for multiple filter : logical operator between them


	private ReportManager control;
	private static DataAccess data;
//	private JInternalFrame frame;
	private static ReportManagerView rmView;
	//	private exportFormats exportFormat = exportFormats.HTML_FORMAT;

	// buttons
//	private JButton jbExport;

	// selected objects
	Report report = null;
//	Section selectedSection = null;
	Dataset selectedChart = null;
	Dataset selectedDataset = null;
	Datasource selectedDatasetDescriptor = null;
	Object selectedObject = null;


	// Constructors //

	private RMOptionsView() {
		super();
	}


	public RMOptionsView(ReportManager _control, ReportManagerView _rmView, DataAccess _data, JInternalFrame _frame) {
		this();
		data = _data;
		control = _control;
//		frame = _frame;
		rmView = _rmView;
		setLayout(new CardLayout());

		add(reportCardCreation(REPORT_OPTIONS),   REPORT_OPTIONS);
		add(sectionCardCreation(SECTION_OPTIONS), SECTION_OPTIONS);
		add(datasetCardCreation(DATASET_OPTIONS), DATASET_OPTIONS);
	}


	// report Card
	private JPanel reportCardCreation(String _name) {
		reportCard = new JPanel();
		reportCard.setName(_name);
		reportCard.setLayout(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();

		// Report Options
		c.gridx = 0;
		c.gridy = 0;
		c.weightx = 1;
		c.weighty = 1;
		c.fill = GridBagConstraints.BOTH;
		reportCard.add(reportPropertiesCreation(), c);

		// Report Commands
		c.gridx++;
		c.fill = GridBagConstraints.BOTH;
		reportCard.add(reportCommandsCreation(), c);

		return reportCard;
	}


	private Component reportPropertiesCreation() {
		reportProperties = new TitledPanel(REPORT_OPTIONS);

		reportTitleTextField = new JTextField(data.report.getTitle());
		setBorder(reportTitleTextField, "Title");
		reportTitleTextField.addActionListener(this);

		reportCommentTextArea = new JTextArea();
		reportCommentTextArea.setLineWrap(true);   
		reportCommentTextArea.setText(data.report.getComment());
		reportCommentTextArea.getDocument().addDocumentListener(new ReportCommentListener());
		
		reportCommentScrollPane = new JScrollPane(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS,   
				ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		reportCommentScrollPane.setViewportView(reportCommentTextArea);   
		setBorder(reportCommentScrollPane, "Comments");

		reportProperties.setLayout(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();

		c.gridx = 0;
		c.gridy = 0;
		c.weightx = 1;
		c.weighty = 0;
		c.fill = GridBagConstraints.HORIZONTAL;
		reportProperties.add(reportTitleTextField, c);

		c.gridy++;
		c.weighty = 1;
		c.fill = GridBagConstraints.BOTH;
		reportProperties.add(reportCommentScrollPane, c);		
		
		return reportProperties;	
	}

	private Component reportCommandsCreation()
	{
		JPanel panel = new JPanel();
		panel.setLayout(new BoxLayout(panel, BoxLayout.PAGE_AXIS));

		// Button "Add dataset to new section"
		addDatasetIntoNewSectionButton = new JButton();
		addDatasetIntoNewSectionButton.setLayout(
			new BoxLayout(
				addDatasetIntoNewSectionButton,
				BoxLayout.PAGE_AXIS));
		addDatasetIntoNewSectionButton.add(new JLabel("Add dataset"));
		addDatasetIntoNewSectionButton.add(new JLabel("to new section"));
		addDatasetIntoNewSectionButton.setName("addDatasetIntoNewSection");
		addDatasetIntoNewSectionButton.addActionListener(this);
		panel.add(Box.createVerticalGlue());
		panel.add(addDatasetIntoNewSectionButton);

		// Button "Export report"
		exportButton = new JButton();
		exportButton.setLayout(
			new BoxLayout(
				exportButton,
				BoxLayout.PAGE_AXIS));
		exportButton.add(new JLabel("Export report"));
		exportButton.add(new JLabel("to HTML"));
		exportButton.setName("exportButton");
		exportButton.addActionListener(this);
		panel.add(Box.createVerticalGlue());
		panel.add(exportButton);
		panel.add(Box.createVerticalGlue());
		return panel;
	}


/* TODO: uncomment when other export formats than HTML are available
	private Component reportExportFormatCreation() {		// TODO on linux width is too small
		JPanel reportExportFormatPanel = new JPanel();
		Dimension preferredSize = new Dimension(100, 100);
		reportExportFormatPanel.setPreferredSize(preferredSize );
		setBorder(reportExportFormatPanel, "Export Format");
		reportExportFormatPanel.setLayout(new BoxLayout(reportExportFormatPanel, BoxLayout.Y_AXIS));

		ButtonGroup exportFormatGroup = new ButtonGroup();

		htmlRadioButton = new JRadioButton("HTML");
		htmlRadioButton.setName("htmlRadioButton");
		reportExportFormatPanel.add(htmlRadioButton);
		exportFormatGroup.add(htmlRadioButton);
		htmlRadioButton.addActionListener(this);

		xmlRadioButton  = new JRadioButton("XML");
		xmlRadioButton.setName("xmlRadioButton");
		reportExportFormatPanel.add(xmlRadioButton);
		exportFormatGroup.add(xmlRadioButton);
		xmlRadioButton.addActionListener(this);

		txtRadioButton  = new JRadioButton("TXT");
		txtRadioButton.setName("txtRadioButton");
		reportExportFormatPanel.add(txtRadioButton);
		exportFormatGroup.add(txtRadioButton);
		txtRadioButton.addActionListener(this);

		htmlRadioButton.setSelected(true);

		return reportExportFormatPanel;
	}
*/
/* TODO: uncomment when other image formats than PNG are available
	private Component reportImageFormatCreation() {		// TODO on linux width is too small
		JPanel reportImageFormatPanel = new JPanel();
		Dimension preferredSize = new Dimension(100, 100);
		reportImageFormatPanel.setPreferredSize(preferredSize );
		setBorder(reportImageFormatPanel, "Image Format");
		reportImageFormatPanel.setLayout(new BoxLayout(reportImageFormatPanel, BoxLayout.Y_AXIS));

		pngRadioButton = new JRadioButton("PNG");
		jpgRadioButton = new JRadioButton("JPG");
		svgRadioButton = new JRadioButton("SVG");

		reportImageFormatPanel.add(pngRadioButton);
		reportImageFormatPanel.add(jpgRadioButton);
		reportImageFormatPanel.add(svgRadioButton);

		ButtonGroup chartFormatGroup = new ButtonGroup();
		chartFormatGroup.add(pngRadioButton);
		chartFormatGroup.add(jpgRadioButton);
		chartFormatGroup.add(svgRadioButton);

		pngRadioButton.addActionListener(this);
		jpgRadioButton.addActionListener(this);
		svgRadioButton.addActionListener(this);

		pngRadioButton.setSelected(true);
		return reportImageFormatPanel;
	}
*/
/*	private Component reportMacroPanelCreation() {
		JPanel reportMacroPanel = new JPanel();
//		setBorder(reportMacroPanel, "MACRO");
//		reportMacroPanel.setLayout(new BoxLayout(reportMacroPanel, BoxLayout.Y_AXIS));
//		loadMacroButton = new JButton("Load Macro");
//		loadMacroButton.setFont(new java.awt.Font("Dialog", 0, 12));
//		loadMacroButton.setName("loadMacroButton");
//		loadMacroButton.addActionListener(this);
//
//		saveMacroButton = new JButton("Save Macro");
//		saveMacroButton.setFont(new java.awt.Font("Dialog", 0, 12));
//		saveMacroButton.setName("saveMacroButton");
//		saveMacroButton.addActionListener(this);
//
//		reportMacroPanel.add(loadMacroButton);
//		reportMacroPanel.add(saveMacroButton);
//
		return reportMacroPanel;
	}

	private Component reportLoadPanelCreation() {
		JPanel reportLoadPanel = new JPanel();
		setBorder(reportLoadPanel, "Load Report");
		reportLoadPanel.setLayout(new BoxLayout(reportLoadPanel, BoxLayout.Y_AXIS));
		loadReportButton = new JButton("Load Report");
		loadReportButton.setFont(new java.awt.Font("Dialog", 0, 12));
		loadReportButton.setName("loadReportButton");
		loadReportButton.addActionListener(this);

		reportLoadPanel.add(loadReportButton);

		return reportLoadPanel;
	}
*/	
	// section Card
	private JPanel sectionCardCreation(String _name) {
		sectionCard = new JPanel();
		sectionCard.setName(_name);
		sectionCard.setLayout(new BoxLayout(sectionCard, BoxLayout.LINE_AXIS));
		sectionCard.add(sectionPropertiesCreation());
		sectionCard.add(sectionChartOptionsCreation());
		sectionCard.add(sectionCommandsCreation());

		return sectionCard;
	}


	private Component sectionPropertiesCreation()
	{
		sectionProperties = new TitledPanel(SECTION_OPTIONS);
		sectionProperties.setLayout(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();

		// section title
		sectionTitleTextField = new JTextField(20);
		sectionTitleTextField.setName(SECTION_TITLE);
		setBorder(sectionTitleTextField, "Title");
		sectionTitleTextField.addActionListener(this);
		c.gridx = 0;
		c.gridy = 0;
		c.weightx = 1;
		c.weighty = 0;
		c.fill = GridBagConstraints.HORIZONTAL;
		sectionProperties.add(sectionTitleTextField, c);

		// section comments
		sectionCommentTextArea = new JTextArea();
		sectionCommentTextArea.setLineWrap(true);
		sectionCommentTextArea.getDocument().addDocumentListener(new SectionCommentListener());
		sectionCommentScrollPane = new JScrollPane(
			ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS,
			ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		sectionCommentScrollPane.setViewportView(sectionCommentTextArea);   
		setBorder(sectionCommentScrollPane, "Comments");
		c.gridy++;
		c.weighty = 1;
		c.fill = GridBagConstraints.BOTH;
		sectionProperties.add(sectionCommentScrollPane, c);

		return sectionProperties;
	}


	private Component sectionCommandsCreation()
	{
		JPanel panel = new JPanel();
		panel.setLayout(new BoxLayout(panel, BoxLayout.PAGE_AXIS));
		panel.add(Box.createVerticalGlue());

		// button "Add dataset"
		addDatasetToSectionButton = new JButton("Add dataset");
		addDatasetToSectionButton.setName("addDatasetToSection");
		addDatasetToSectionButton.addActionListener(this);
		panel.add(addDatasetToSectionButton);
		panel.add(Box.createVerticalGlue());
		
		// button "Delete section"
		sectionRemoveCommandButton = new JButton("Delete section");
		sectionRemoveCommandButton.setName("sectionRemoveCommandButton");
		sectionRemoveCommandButton.addActionListener(this);
		panel.add(sectionRemoveCommandButton);
		panel.add(Box.createVerticalGlue());

		return panel;
	}


	private Component sectionChartOptionsCreation()
	{
		sectionChartOptionsPanel = new TitledPanel("Chart options");
		sectionChartOptionsPanel.setLayout(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();
		c.fill = GridBagConstraints.HORIZONTAL;
		c.anchor = GridBagConstraints.WEST;
		Insets noInsets = new Insets(0, 0, 0, 0);
		Insets insetsTop10 = new Insets(10, 0, 0, 0);

		// radio button group: Time-based/Histogram/Quantiles
		timeBasedRadioButton = new JRadioButton("Time-based");
		histogramRadioButton = new JRadioButton("Histogram");
		quantileRadioButton = new JRadioButton("Quantiles");
		timeBasedRadioButton.addActionListener(this);
		histogramRadioButton.addActionListener(this);
		quantileRadioButton.addActionListener(this);
		ButtonGroup chartTypeGroup = new ButtonGroup();
		chartTypeGroup.add(timeBasedRadioButton);
		chartTypeGroup.add(histogramRadioButton);
		chartTypeGroup.add(quantileRadioButton);
		timeBasedRadioButton.setSelected(true);

		// "Time-based" radio button
		c.gridx = 0;
		c.gridy = 0;
		sectionChartOptionsPanel.add(timeBasedRadioButton, c);

		// time interval restriction
		c.gridy++;
		sectionFromTimeLabel = new JLabel("from time (ms)");
		sectionChartOptionsPanel.add(sectionFromTimeLabel, c);

		c.gridx++;
		sectionFromTimeTextField = new JTextField(8);
		sectionFromTimeTextField.setMinimumSize(sectionFromTimeTextField.getPreferredSize());
		sectionChartOptionsPanel.add(sectionFromTimeTextField, c);

		c.gridx++;
		sectionFromTimeDefaultCheckBox = new JCheckBox("auto", true);
		sectionFromTimeDefaultCheckBox.setHorizontalTextPosition(SwingConstants.LEFT);
		sectionFromTimeDefaultCheckBox.addActionListener(this);
		sectionChartOptionsPanel.add(sectionFromTimeDefaultCheckBox, c);

		c.gridx = 0;
		c.gridy++;
		sectionToTimeLabel = new JLabel("to time (ms)");
		sectionChartOptionsPanel.add(sectionToTimeLabel, c);

		c.gridx++;
		sectionToTimeTextField = new JTextField(8);
		sectionToTimeTextField.setMinimumSize(sectionToTimeTextField.getPreferredSize());
		sectionChartOptionsPanel.add(sectionToTimeTextField, c);

		c.gridx++;
		sectionToTimeDefaultCheckBox = new JCheckBox("auto", true);
		sectionToTimeDefaultCheckBox.setHorizontalTextPosition(SwingConstants.LEFT);
		sectionToTimeDefaultCheckBox.addActionListener(this);
		sectionChartOptionsPanel.add(sectionToTimeDefaultCheckBox, c);

		// "Histogram" radio button
		c.gridx = 0;
		c.gridy++;
		c.insets = insetsTop10;
		sectionChartOptionsPanel.add(histogramRadioButton, c);

		// "Number of slices" label
		c.gridy++;
		c.insets = noInsets;
		slicesNumberLabel = new JLabel(SLICES_NUMBER);
		sectionChartOptionsPanel.add(slicesNumberLabel, c);

		// "Number of slices" text field
		c.gridx++;
		slicesNumberTextField = new JTextField(4);
		slicesNumberTextField.setMinimumSize(slicesNumberTextField.getPreferredSize());
		slicesNumberTextField.setName(SLICES_NUMBER);
		slicesNumberTextField.addActionListener(this);
		sectionChartOptionsPanel.add(slicesNumberTextField, c);

		// "Quantiles" radio button
		c.gridx = 0;
		c.gridy++;
		c.insets = insetsTop10;
		sectionChartOptionsPanel.add(quantileRadioButton, c);

		// "Quantiles" label
		c.gridy++;
		c.insets = noInsets;
		quantilesNumberLabel = new JLabel(QUANTILES_NUMBER);
		sectionChartOptionsPanel.add(quantilesNumberLabel, c);

		// "Quantiles" text field
		c.gridx++;
		quantilesNumberTextField = new JTextField(4);
		quantilesNumberTextField.setMinimumSize(quantilesNumberTextField.getPreferredSize());
		quantilesNumberTextField.setName(QUANTILES_NUMBER);
		quantilesNumberTextField.addActionListener(this);
		sectionChartOptionsPanel.add(quantilesNumberTextField, c);

		// "Apply" button
		c.gridx = 0;
		c.gridy++;
		c.gridwidth = 3;
		c.fill = GridBagConstraints.NONE;
		c.anchor = GridBagConstraints.CENTER;
		c.insets = insetsTop10;
		sectionApplyButton = new JButton(APPLY);
		sectionApplyButton.setName("sectionApplyButton");
		sectionApplyButton.addActionListener(this);
		sectionChartOptionsPanel.add(sectionApplyButton, c);

		return sectionChartOptionsPanel;
	}


	private void updateSectionChartOptions()
	{
		if (timeBasedRadioButton.isSelected())
		{
			updateChartOptionsActivation(true, false,  false);
		}
		else if (histogramRadioButton.isSelected())
		{
			updateChartOptionsActivation(false, true, false);
		}
		else if (quantileRadioButton.isSelected())
		{
			updateChartOptionsActivation(false, false, true);
		}
	}


	private void updateChartOptionsActivation(boolean timeBased, boolean histogram, boolean quantiles)
	{
		sectionFromTimeLabel.setEnabled(timeBased);
		sectionFromTimeDefaultCheckBox.setEnabled(timeBased);
		sectionFromTimeTextField.setEnabled(
			timeBased
			&& ! sectionFromTimeDefaultCheckBox.isSelected());
		sectionToTimeLabel.setEnabled(timeBased);
		sectionToTimeDefaultCheckBox.setEnabled(timeBased);
		sectionToTimeTextField.setEnabled(
			timeBased
			&& ! sectionToTimeDefaultCheckBox.isSelected());

		slicesNumberLabel.setEnabled(histogram);
		slicesNumberTextField.setEnabled(histogram);

		quantilesNumberLabel.setEnabled(quantiles);
		quantilesNumberTextField.setEnabled(quantiles);
	}


	// dataset Card
	private Component datasetCardCreation(String _name) {
		datasetCard = new JTabbedPane();	// JPanel();
		datasetCard.setName(_name);
		setBorder(datasetCard, DATASET);

		datasetCard.addTab("Global Options",          datasetGlobalOptionsTabCreation());
		datasetCard.addTab("Datasources",             datasetDatasourcesTabCreation());
		datasetCard.addTab("Time Filtering",          datasetTimeFilteringTabCreation());
		datasetCard.addTab("Fields Values Filtering", datasetFieldsValuesFilteringTabCreation());
		
		if (useStatisticalFiltering){
			datasetCard.addTab("Statistical Filtering",   datasetStatisticalFilteringTabCreation());
		}
		datasetCard.addTab("Chart Options",datasetDrawOptionsTabCreation());
		return datasetCard;
	}


	private Component datasetGlobalOptionsTabCreation()
	{
		JPanel panel = new JPanel();
		panel.setLayout(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();

		// Dataset options
		c.gridx = 0;
		c.gridy = 0;
		c.weightx = 1;
		c.weighty = 1;
		c.gridheight = 2;
		c.fill = GridBagConstraints.BOTH;
		c.anchor = GridBagConstraints.CENTER;
		c.insets = new Insets(5, 5, 5, 5);
		panel.add(datasetPropertiesCreation(), c);

		// Dataset statistics
		c.gridx++;
		c.weightx = 0.5;
		c.gridheight = 1;
		panel.add(datasetStatisticsPanelCreation(), c);

		// Remove Dataset button
		datasetRemoveCommandButton = new JButton("Remove Dataset");
		datasetRemoveCommandButton.setName("datasetRemoveCommandButton");
		datasetRemoveCommandButton.addActionListener(this);
		c.gridy++;
		c.weightx = 0;
		c.weighty = 0;
		c.fill = GridBagConstraints.NONE;
		panel.add(datasetRemoveCommandButton, c);

		return panel;
	}
	

	private Component datasetPropertiesCreation()
	{
		JPanel panel = new JPanel();
		panel.setLayout(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();

		// Title text field
		datasetTitleTextField = new JTextField(20);
		datasetTitleTextField.setName(DATASET_TITLE);
		datasetTitleTextField.addActionListener(this);
		c.gridx = 0;
		c.gridy = 0;
		c.weightx = 1;
		c.weighty = 0;
		c.fill = GridBagConstraints.HORIZONTAL;
		setBorder(datasetTitleTextField, "Title");
		panel.add(datasetTitleTextField, c);

		// Comment text area
		datasetCommentTextArea = new JTextArea();
		datasetCommentTextArea.setLineWrap(true);
		datasetCommentTextArea.getDocument().addDocumentListener(new DatasetCommentListener());
		datasetCommentScrollPane = new JScrollPane(
			ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS,
			ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		datasetCommentScrollPane.setViewportView(datasetCommentTextArea);
		setBorder(datasetCommentScrollPane, "Comments");
		c.gridy++;
		c.weighty = 1;
		c.fill = GridBagConstraints.BOTH;
		panel.add(datasetCommentScrollPane, c);

		// Apply button
		datasetGlobalOptionsApplyButton = new JButton("Apply");
		datasetGlobalOptionsApplyButton.addActionListener(this);
		c.gridy++;
		c.weightx = 0;
		c.weighty = 0;
		c.fill = GridBagConstraints.NONE;
		panel.add(datasetGlobalOptionsApplyButton, c);
		guiObjectsListeners();
		return panel;
	}


	private JScrollPane datasetStatisticsPanelCreation()
	{
		JScrollPane datasetStatsScrollPane = new JScrollPane(
			ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED,   
			ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);

		datasetStatsTextArea = new JTextArea();
		datasetStatsTextArea.setLineWrap(true);
		datasetStatsTextArea.setToolTipText("Statistics for this dataset.");
		datasetStatsTextArea.setEditable(false);

		datasetStatsScrollPane.setViewportView(datasetStatsTextArea);   
		setBorder(datasetStatsScrollPane, "Statistics");
		return datasetStatsScrollPane;
	}


	private Component datasetDatasourcesTabCreation() {
		datasetDatasourcesTab = new JPanel();
		datasetDatasourcesTab.setLayout(new BoxLayout(datasetDatasourcesTab, BoxLayout.PAGE_AXIS));
		datasetDatasourcesTab.add(datasetDatasourcesPanelCreation());
		datasetDatasourcesTab.add(datasetDatasourcesApplyButton = new JButton("Apply"));
		datasetDatasourcesApplyButton.addActionListener(this);
		return datasetDatasourcesTab;
	}


	private Component datasetDrawOptionsTabCreation()
	{
		JPanel drawOptionsTab = new JPanel();
		drawOptionsTab = new JPanel();
		drawOptionsTab.setLayout(new BoxLayout(drawOptionsTab, BoxLayout.LINE_AXIS));

		// filler before plot options
		drawOptionsTab.add(Box.createHorizontalGlue());

		// plot options
		JPanel plotOptions = new TitledPanel("Plot type");
		plotOptions.setLayout(new GridLayout(5, 1));

		ButtonGroup plotOptionsGrp = new ButtonGroup();

		datasetNoChartRadio = new JRadioButton("no chart", false);
		datasetNoChartRadio.addActionListener(this);
		plotOptionsGrp.add(datasetNoChartRadio);
		plotOptions.add(datasetNoChartRadio);

		datasetTimeBasedRadio = new JRadioButton("time-based", false);
		datasetTimeBasedRadio.addActionListener(this);
		plotOptionsGrp.add(datasetTimeBasedRadio);
		plotOptions.add(datasetTimeBasedRadio);

		datasetRawCheckBox = new JCheckBox("scatter plot");
		datasetRawCheckBox.setHorizontalTextPosition(SwingConstants.LEFT);
		plotOptions.add(datasetRawCheckBox);

		datasetHistogramRadio = new JRadioButton("histogram", false);
		datasetHistogramRadio.addActionListener(this);
		plotOptionsGrp.add(datasetHistogramRadio);
		plotOptions.add(datasetHistogramRadio);

		datasetquantilesRadio = new JRadioButton("quantiles", false);
		datasetquantilesRadio.addActionListener(this);
		plotOptionsGrp.add(datasetquantilesRadio);
		plotOptions.add(datasetquantilesRadio);

		drawOptionsTab.add(plotOptions);

		// filler between scatter plot options and moving statistics options
		drawOptionsTab.add(Box.createHorizontalGlue());

		// moving statistics
		datasetMovingOptionsPanel = new TitledPanel("Moving statistics");
		datasetMovingOptionsPanel.setLayout(new GridBagLayout());
		GridBagConstraints gbc = new GridBagConstraints();
		gbc.fill = GridBagConstraints.HORIZONTAL;
		gbc.anchor = GridBagConstraints.WEST;
		gbc.weightx = 0;

		gbc.gridx = 0;
		gbc.gridy = 0;
		datasetMovingOptionsPanel.add(new JLabel("Time window (ms)"), gbc);

		gbc.gridx++;
		timeWindowTextField = new JTextField(6);
		timeWindowTextField.setMinimumSize(timeWindowTextField.getPreferredSize());
		timeWindowTextField.setName(TIME_WINDOW);
		timeWindowTextField.addActionListener(this);
		datasetMovingOptionsPanel.add(timeWindowTextField, gbc);

		gbc.gridx = 0;
		gbc.gridy++;
		datasetMovingOptionsPanel.add(new JLabel("Time step (ms)"), gbc);

		gbc.gridx++;
		stepTextField = new JTextField(6);
		stepTextField.setMinimumSize(stepTextField.getPreferredSize());
		stepTextField.setName(STEP);
		stepTextField.addActionListener(this);
		datasetMovingOptionsPanel.add(stepTextField, gbc);

		gbc.gridx = 0;
		gbc.gridy++;
		gbc.gridwidth = 2;
		datasetMovingMinCheckBox = new JCheckBox("min");
		datasetMovingOptionsPanel.add(datasetMovingMinCheckBox, gbc);
		gbc.gridy++;
		datasetMovingMaxCheckBox = new JCheckBox("max");
		datasetMovingOptionsPanel.add(datasetMovingMaxCheckBox, gbc);
		gbc.gridy++;
		datasetMovingAverageCheckBox = new JCheckBox("average");
		datasetMovingOptionsPanel.add(datasetMovingAverageCheckBox, gbc);
		gbc.gridy++;
		datasetMovingMedianCheckBox = new JCheckBox("median");
		datasetMovingOptionsPanel.add(datasetMovingMedianCheckBox, gbc);
		gbc.gridy++;
		datasetMovingStdDevCheckBox = new JCheckBox("standard deviation");
		datasetMovingOptionsPanel.add(datasetMovingStdDevCheckBox, gbc);
		gbc.gridy++;
		datasetMovingThroughputCheckBox = new JCheckBox("throughput");
		datasetMovingOptionsPanel.add(datasetMovingThroughputCheckBox, gbc);

		drawOptionsTab.add(datasetMovingOptionsPanel);

		// filler between moving statistics options and redraw button
		drawOptionsTab.add(Box.createHorizontalGlue());

		// redraw button
		JPanel redrawPanel = new JPanel();
		redrawButton = new JButton("Redraw");
		redrawButton.setName("redrawButton");
		redrawButton.addActionListener(this);
		redrawPanel.setLayout(new BoxLayout(redrawPanel, BoxLayout.PAGE_AXIS));
		redrawPanel.add(Box.createVerticalGlue());
		redrawPanel.add(redrawButton);
		redrawPanel.add(Box.createVerticalGlue());
		drawOptionsTab.add(redrawPanel);

		// filler after button
		drawOptionsTab.add(Box.createHorizontalGlue());

		return drawOptionsTab;
	}


	private JPanel datasetTimeFilteringTabCreation() {
		JPanel panel = new JPanel();
		JLabel jlFilterStartTime = new JLabel("From time (ms)");

		filterStartTimeTextField = new JTextField(8);
		filterStartTimeTextField.addActionListener(this);
		filterStartTimeTextField.setName(FILTER_START_TIME);

		startFilterAtFirstEventCheckBox = new JCheckBox();
		startFilterAtFirstEventCheckBox.setText(FIRST_EVENT);
		startFilterAtFirstEventCheckBox.setName(FILTER_START_AT_FIRST_EVENT);
		startFilterAtFirstEventCheckBox.setSelected(true);
		startFilterAtFirstEventCheckBox.addActionListener(this);

		filterStartTimeTextField.setEnabled(!startFilterAtFirstEventCheckBox.isSelected());

		JLabel jlFilterEndTime = new JLabel("To time (ms)");

		filterEndTimeTextField = new JTextField(8);
		filterEndTimeTextField.addActionListener(this);
		filterEndTimeTextField.setName(FILTER_END_TIME);

		endFilterAtLastEventCheckBox = new JCheckBox();
		endFilterAtLastEventCheckBox.setText(LAST_EVENT);
		endFilterAtLastEventCheckBox.setName(FILTER_END_AT_LAST_EVENT);
		endFilterAtLastEventCheckBox.setSelected(true);
		endFilterAtLastEventCheckBox.addActionListener(this);

		filterEndTimeTextField.setEnabled(!endFilterAtLastEventCheckBox.isSelected());

		timeFilterApplyButton = new JButton(APPLY);
		timeFilterApplyButton.setName("timeFilterApplyButton");
		timeFilterApplyButton.addActionListener(this);

		panel.setLayout(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();
		c.insets = new Insets(5, 5, 5, 5); // 5-pixel margins on all sides

		// start time
		c.gridx = 0;
		c.gridy = 0;
		c.anchor = GridBagConstraints.LINE_START;
		panel.add(jlFilterStartTime, c);

		c.gridx ++;
		panel.add(filterStartTimeTextField, c);

		c.gridx ++;
		panel.add(startFilterAtFirstEventCheckBox, c);

		// end time
		c.gridx = 0;
		c.gridy++;
		panel.add(jlFilterEndTime, c);

		c.gridx ++;
		panel.add(filterEndTimeTextField, c);

		c.gridx ++;
		panel.add(endFilterAtLastEventCheckBox, c);

		c.gridx ++;
		c.gridy = 0;
		c.gridheight = 2;
		c.anchor = GridBagConstraints.EAST;
		panel.add(timeFilterApplyButton, c);
		c.gridheight = 1;

		c.gridy = 2;
		c.gridx = 0;
		c.gridwidth = 3;
		timeFilteredEventsNumberLabel = new JLabel(UNINITALIZED_LABEL);
		timeFilteredEventsNumberLabel.setBorder(BorderFactory.createLineBorder(Color.black));
		panel.add(timeFilteredEventsNumberLabel, c);

		return panel;
	}


	private Component datasetFieldsValuesFilteringTabCreation() {
		JPanel jpFiledsFilterPanel = new JPanel();
		eventsFilterEventsNumberLabel = new JLabel(UNINITALIZED_LABEL);
		jpFiledsFilterPanel.add(eventsFilterEventsNumberLabel);

		addFilterInFilterValuesFilteringButton = new JButton(ADD_FIELD_FILTER);
		addFilterInFilterValuesFilteringButton.setName("addFilterButton");
		addFilterInFilterValuesFilteringButton.addActionListener(this);


		fieldValuesFilteringApplyButton = new JButton(APPLY);
		fieldValuesFilteringApplyButton.setName("fieldValuessFilteringApplyButton");
		fieldValuesFilteringApplyButton.addActionListener(this);

		JLabel jlLinkingFilter = new JLabel("Logical operator :");

		andRadioButton = new JRadioButton("AND");
		andRadioButton.addActionListener(this);

		andRadioButton.setSelected(true);

		orRadioButton = new JRadioButton("OR");
		orRadioButton.addActionListener(this);

		orRadioButton.setText("OR");

		// radio-button functional assembly
		ButtonGroup jbgAndOr = new ButtonGroup();
		jbgAndOr.add(andRadioButton);
		jbgAndOr.add(orRadioButton);

		eventsFilterEventsNumberLabel = new JLabel(UNINITALIZED_LABEL);
		jpLogic = new JPanel();
		jpLogic.add(jlLinkingFilter);
		jpLogic.add(andRadioButton);
		jpLogic.add(orRadioButton);
		jpLogic.setVisible(false);
		setBorder(jpLogic, null);

		JPanel jpAddAndLogic = new JPanel();

		// GridBagLayout
		jpAddAndLogic.setLayout(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();

		c.gridx = 0;
		c.gridy = 0;
		c.weightx = 0.1;
		c.insets = new Insets(10,10,10,10);		// bottom, left, right, top
		c.anchor = GridBagConstraints.LINE_START;
		jpAddAndLogic.add(addFilterInFilterValuesFilteringButton, c);

		c.gridx ++;
		jpAddAndLogic.add(jpLogic);

		c.gridx ++;
		c.insets = new Insets(10,10,10,10);		// bottom, left, right, top
		jpAddAndLogic.add(fieldValuesFilteringApplyButton);

		c.gridx = 0;
		c.gridy ++;
		c.gridwidth = 3;
		c.anchor = GridBagConstraints.CENTER;
		eventsFilterEventsNumberLabel.setBorder(BorderFactory.createLineBorder(Color.black));
		jpAddAndLogic.add(eventsFilterEventsNumberLabel, c);

		// table
		filterTable = new javax.swing.JTable();
		datasetFieldsValulesFilterTableModel = new DefaultTableModel(
				new Object[][] {}, new String[] {"Field", "Operator", "Argument", "Delete" })
		
		{		private static final long serialVersionUID = 6560949182092675199L;
			Class<?>[] types = new Class[] { java.lang.String.class, java.lang.String.class,
					java.lang.String.class, java.lang.String.class };
			boolean[] canEdit = new boolean[] { true, true, true, false };
			public Class<?> getColumnClass(int columnIndex) {
				return types[columnIndex];
			}
			public boolean isCellEditable(int rowIndex, int columnIndex) {
				return canEdit[columnIndex];
			}
		};
	datasetFieldsValulesFilterTableModel.addTableModelListener(filterTable);		
		filterTable.addMouseListener(new MouseListener() {
			public void mouseClicked(MouseEvent e) {
				int viewRow = filterTable.getSelectedRow();
				int viewCol = filterTable.getSelectedColumn();
				int rowCount;
				if (viewCol == FILTER_TABLE_DELETE) {
					LogAndDebug.tracep("    removing row " + viewRow + ".");
					datasetFieldsValulesFilterTableModel.removeRow(viewRow);
					rowCount = filterTable.getRowCount();
					if (rowCount < 2) {
						jpLogic.setVisible(false);

					} else {
						jpLogic.setVisible(true);
					}
					LogAndDebug.tracep(" jpLogic.setVisible(" + jpLogic.isVisible() + ")");
				}
			
			}
			public void mouseEntered(MouseEvent e) {}
			public void mouseExited(MouseEvent e) {	}
			public void mousePressed(MouseEvent e) {}
			public void mouseReleased(MouseEvent e) {}
		});
		
	//	filterTable.addFocusListener(this);
		
		filterTable.setModel(datasetFieldsValulesFilterTableModel);
		JScrollPane jScrollFilterTable = new JScrollPane();
		jScrollFilterTable.setViewportView(filterTable);
	
		JPanel fieldsFilteringTab = new JPanel();
		fieldsFilteringTab.setLayout(new BorderLayout());
		fieldsFilteringTab.add(jScrollFilterTable, BorderLayout.CENTER);
		fieldsFilteringTab.add(jpAddAndLogic, BorderLayout.SOUTH);

		return fieldsFilteringTab;
	}

	private void jbAddFilterActionPerformed(String field, String operator, String value) {
		LogAndDebug.tracep("()");
		if (null == field)   {field = "";}
		if (null == operator){operator = "";}
		if (null == value)   {value = "";}
		Vector<String> addSel = new Vector<String>();
		addSel.add(field);
		addSel.add(operator);
		addSel.add(value);
		addSel.add("");
		datasetFieldsValulesFilterTableModel.addRow(addSel);
		initFieldsValuesFilterRendererAndEditor();
		if (datasetFieldsValulesFilterTableModel.getRowCount() > 1) {
			jpLogic.setVisible(true);
			if (logicalOp == LogicalEnum.AND) {
				andRadioButton.setSelected(true);
				LogAndDebug.trace("() andRadioButton.setSelected: " + andRadioButton.isSelected());
			} else if (logicalOp == LogicalEnum.OR) {
				orRadioButton.setSelected(true);
				LogAndDebug.trace("() orRadioButton: " + orRadioButton.isSelected());
			} else {
				// select and by default
				andRadioButton.setSelected(true);
				LogAndDebug.trace("() andRadioButton.setSelected: " + andRadioButton.isSelected());
			}
		}
		LogAndDebug.tracem("()");
	}


	// Initialization
	private void initFieldsValuesFilterRendererAndEditor() {
		LogAndDebug.tracep("()");
		TableColumnModel myColumnModel = filterTable.getColumnModel();
		TableColumn myTableColumn;
		myTableColumn = myColumnModel.getColumn(FILTER_TABLE_FIELD);
		if (null != selectedDataset){
			updateFieldComboBox(selectedDataset);
		}
		myTableColumn.setCellEditor(new DefaultCellEditor(fieldComboBox));
		myTableColumn = myColumnModel.getColumn(FILTER_TABLE_OPERATOR);
		JComboBox<String> operatorComboBox = new JComboBox<String>();

		for (String operator : FieldsValuesFilter.OPERATORS) {
			operatorComboBox.addItem(operator);
		}
		operatorComboBox.addItemListener(new java.awt.event.ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				if (e.getStateChange() == 1) {
					LogAndDebug.trace("() operatorComboBox: item state changed: \"" + e.getItem().toString() + "\"");
				}
			}
		});
		myTableColumn.setCellEditor(new DefaultCellEditor(operatorComboBox));
		myTableColumn = myColumnModel.getColumn(FILTER_TABLE_DELETE);
		myTableColumn.setCellRenderer(new TableCellRenderer() {
			public Component getTableCellRendererComponent(
					JTable table, 
					Object value,
					boolean isSelected, 
					boolean hasFocus, 
					int row, 
					int column) {
				JLabel jlDelete = new JLabel();
				jlDelete.setIcon(data.DELETE_ICON);
				jlDelete.setOpaque(true);
				jlDelete.setHorizontalAlignment(JLabel.CENTER);
				if (isSelected) {
					Color bgrd = table.getSelectionBackground();
					jlDelete.setBackground(bgrd);
				} else {
					jlDelete.setBackground(Color.WHITE);
				}
				jlDelete.setBorder(null);
				return jlDelete;
			}
		});
		myTableColumn.setPreferredWidth(FILTER_TABLE_DELETE);
		LogAndDebug.tracem("()");
	}


	/**
	 * update model from gui
	 * @param dataset
	 */
	private void updateDatasetTimeFilteringParamsFromGui(Dataset dataset)
	{
		LogAndDebug.trace("("+LogAndDebug.toText(dataset)+")");
		timeFilterSetStartAtFirstEvent(dataset, dataset.isStartFilterAtFirstEvent());
		timeFilterSetEndAtLastEvent(dataset, dataset.isEndFilterAtLastEvent());
	}


	/**
	 * update model from gui
	 * Check if datasetFieldsValulesFilterTableModel is correctly filled and update Dataset model
	 * @param dataset
	 */
	private void updateDatasetFieldsValuesFilteringParamsFromGui(Dataset dataset)
	{
		LogAndDebug.trace("("+LogAndDebug.toText(dataset)+")");
		try {

			// check if all the cells are filled : OK
			int colCount = datasetFieldsValulesFilterTableModel.getColumnCount();
			int rowCount = datasetFieldsValulesFilterTableModel.getRowCount();
			for (int i = 0; i < rowCount; i++) {
				// don't take the last column which is delete column
				for (int j = 0; j < colCount - 1; j++) {
					String cellsContent = datasetFieldsValulesFilterTableModel.getValueAt(i, j).toString();
					if (cellsContent.equals("")) {
					dataset.setFieldsValuesFilter(null);
						LogAndDebug.trace("("+LogAndDebug.toText(dataset)+")  cell["+i+", "+j+"] is empty.");
						return;
					}
				}
			}


// ADD values in filter list

			FieldsValuesFilter fieldsValuesFilter = new FieldsValuesFilter();
			for (int i = 0; i < rowCount; i++) {
				String value    = datasetFieldsValulesFilterTableModel.getValueAt(i, FILTER_TABLE_VALUE).toString();
				String operator = datasetFieldsValulesFilterTableModel.getValueAt(i, FILTER_TABLE_OPERATOR).toString();
				String field    = datasetFieldsValulesFilterTableModel.getValueAt(i, FILTER_TABLE_FIELD).toString();
				FieldFilterAndOp ffo = new FieldFilterAndOp(field, value, operator);
				fieldsValuesFilter.addExpression(ffo);
				LogAndDebug.trace("   adding expression \"" + field +"\" \"" + operator +"\" \"" + value +"\" \".");
			}

			// Logical operator
			fieldsValuesFilter.setLogicalOp(LogicalEnum.AND);
			if (orRadioButton.isSelected()){
				fieldsValuesFilter.setLogicalOp(LogicalEnum.OR);
			}
			dataset.setFieldsValuesFilter(fieldsValuesFilter);
		}
		catch (Exception e) {
			LogAndDebug.err(" *** Exception: " + e);
			dataset.setFieldsValuesFilter(null);
		}
	}


	/**
	 * update model from gui
	 */
	private void updateDatasetStatisticalFilteringParamsFromGui(Dataset dataset)
	{
		Double d = DataAccess.doubleParse(cleaningKTextField.getText(), dataset.getkParam());
		dataset.setkParam(d);

		d = DataAccess.doubleParse(cleaningPercentPointsKeptTextField.getText(), dataset.getPercentPointsKeptParam());
		dataset.setPercentPointsKeptParam(d);
	}


	private Component datasetStatisticalFilteringTabCreation() {	// TODO implement statistical filtering
		JLabel jlCleaningKDescription = new JLabel();
		jlCleaningKDescription.setText("Discard values such than |value-average| > K * std_deviation.");
		jlCleaningKDescription.setForeground(UNIMPLEMENTED_FOREGROUND_COLOR);
		jlCleaningKDescription.setEnabled(false);

		cleaningKTextField = new JTextField(20);
		cleaningKTextField.setName(CLEANING_K);
		cleaningKTextField.setForeground(UNIMPLEMENTED_FOREGROUND_COLOR);
		cleaningKTextField.setText("...");
		cleaningKTextField.setEnabled(true); 
		cleaningKTextField.addActionListener(this);

		JLabel jlCleaningPercentPointsKept1 = new JLabel();
		cleaningPercentPointsKeptTextField = new JTextField(3);
		cleaningPercentPointsKeptTextField.setName(PERCENT_POINTS_KEPT);
		JLabel jlCleaningPercentPointsKept2 = new JLabel();

		JPanel jpKCleaningDescription = new JPanel();
		jpKCleaningDescription.add(jlCleaningKDescription);

		JLabel jlCleaningKValue = new JLabel("K      ");
		jlCleaningKValue.setText("K      ");
		jlCleaningKValue.setForeground(UNIMPLEMENTED_FOREGROUND_COLOR);

		JPanel jpCleaningK = new JPanel();
		jpCleaningK.add(jlCleaningKValue);
		jpCleaningK.add(cleaningKTextField);

		jlCleaningPercentPointsKept1.setText("keep at least ");
		cleaningPercentPointsKeptTextField.setText("...");
		jlCleaningPercentPointsKept2.setText(" % points (Not Implemented Yet)");

		cleaningPercentPointsKeptTextField.addActionListener(this);

		jlCleaningPercentPointsKept1.setForeground(UNIMPLEMENTED_FOREGROUND_COLOR);
		cleaningPercentPointsKeptTextField.setForeground(UNIMPLEMENTED_FOREGROUND_COLOR);
		jlCleaningPercentPointsKept2.setForeground(UNIMPLEMENTED_FOREGROUND_COLOR);
		jlCleaningPercentPointsKept1.setEnabled(false);
		cleaningPercentPointsKeptTextField.setEnabled(true);		// TODO  unimplemented
		jlCleaningPercentPointsKept2.setEnabled(false);

		JPanel jpCleaningPercentPointsKept = new JPanel();
		jpCleaningPercentPointsKept.add(jlCleaningPercentPointsKept1);
		jpCleaningPercentPointsKept.add(cleaningPercentPointsKeptTextField);
		jpCleaningPercentPointsKept.add(jlCleaningPercentPointsKept2);

		JPanel jpCleaning_ = new JPanel();
		jpCleaning_.setBorder(javax.swing.BorderFactory.createTitledBorder(null,
				"Cleaning (Not Implemented Yet)",
				javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION,
				javax.swing.border.TitledBorder.DEFAULT_POSITION,
				new java.awt.Font("Dialog", 1, 11), UNIMPLEMENTED_FOREGROUND_COLOR));

		jpCleaning_.setLayout(new BoxLayout(jpCleaning_, BoxLayout.Y_AXIS));
		jpCleaning_.add(jpKCleaningDescription);
		jpCleaning_.add(jpCleaningK);
		jpCleaning_.add(jpCleaningPercentPointsKept);


		JPanel panel = new TitledPanel("Statistics Filtering Options");
		panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
		
		panel.add(jpCleaning_);	


		statsFilterEventsNumberLabel = new JLabel(UNINITALIZED_LABEL);
		panel.add(statsFilterEventsNumberLabel);

		panel.setAlignmentX(Component.LEFT_ALIGNMENT);
		return panel;
	}


	private Component datasetDatasourcesPanelCreation() {
		datasourcesTableModel = new DatasourcesTableModel();
		datasourcesJTable = new JTable(datasourcesTableModel);
		datasourcesJTable.putClientProperty("terminateEditOnFocusLost", true);
		datasourcesJTable.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
		datasetDatasourcesScrollPane = new JScrollPane(datasourcesJTable);
		return datasetDatasourcesScrollPane;
	}


	private void updateDatasourcesTableModel(Dataset dataset){
		datasourcesTableModel.removeAll();
		for (Datasource datasource : dataset.getDatasources()){
			datasourcesTableModel.addDatasource(datasource);
		}
	}


	//  gui ancillary methods
	private void displayReport() {
		LogAndDebug.tracep();
		CardLayout cl = (CardLayout)(this.getLayout());
		cl.show(this, REPORT_OPTIONS);
		LogAndDebug.tracem();
	}


	private void displaySection() {
		LogAndDebug.tracep();
		CardLayout cl = (CardLayout)(this.getLayout());
		cl.show(this, SECTION_OPTIONS);
		LogAndDebug.tracem();
	}
	
	
	private void displayDataset() {
		LogAndDebug.tracep();
		CardLayout cl = (CardLayout)(this.getLayout());
		cl.show(this, DATASET_OPTIONS);
		LogAndDebug.tracem();
	}

	
	// selections
	// select tab "Report Options"
	public void selectReport(Report report, int depth) {
		LogAndDebug.tracep("(" + LogAndDebug.toText(report)+", "+depth +")");
		updateReportCard(report);
		displayReport();
		LogAndDebug.tracem("(" + LogAndDebug.toText(report)+", "+depth +")");
	}


	private void updateReportCard(Report report2) {
		reportTitleTextField.setText(data.report.getTitle());
		reportCommentTextArea.setText(data.report.getComment());
/*		switch(data.report.getExportFormat()){
		case HTML_FORMAT:
			htmlRadioButton.setSelected(true);
			break;
		case XML_FORMAT:
			xmlRadioButton.setSelected(true);
			break;
		case TXT_FORMAT:
			txtRadioButton.setSelected(true);
			break;
		default:
			htmlRadioButton.setSelected(true);
		}

		switch(data.report.getImageFormat()){
		case PNG_FORMAT:
			pngRadioButton.setSelected(true);
			break;
		case JPG_FORMAT:
			jpgRadioButton.setSelected(true);
			break;
		case SVG_FORMAT:
			svgRadioButton.setSelected(true);
			break;
		default:
			pngRadioButton.setSelected(true);
			break;
		}
*/	}


	public void selectSection(Section sec, int depth) {
		LogAndDebug.tracep("(" + LogAndDebug.toText(sec)+", "+depth +")");
		updateSectionCard(sec);
		displaySection();
		LogAndDebug.tracem("(" + LogAndDebug.toText(sec)+", "+depth +")");
	}


	/**
	 * Updates GUI from selected section model (data)
	 * @param sec: selected section
	 */
	private void updateSectionCard(Section sec)
	{
		sectionTitleTextField.setText(sec.getTitle());
		sectionCommentTextArea.setText(sec.getComment());
		slicesNumberTextField.setText(Integer.toString(sec.getSlicesNb()));
		quantilesNumberTextField.setText(Integer.toString(sec.getQuantilesNb())); 

		if (sec.getChartRepresentation() != null)
		{
			switch (sec.getChartRepresentation()){
			case TIME_BASED:
				timeBasedRadioButton.setSelected(true);
				break;
			case HISTOGRAM:
				histogramRadioButton.setSelected(true);
				break;
			case QUANTILE:
				quantileRadioButton.setSelected(true);
				break;
			}
		}
		if (sec.getDrawStartTime() == -1)
		{
			sectionFromTimeDefaultCheckBox.setSelected(true);
			if (sec.getChart() != null)
			{
				sectionFromTimeTextField.setText(String.valueOf(sec.getChart().getDefaultStartTime()));
			}
			else
			{
				sectionFromTimeTextField.setText("-1");
			}
		}
		else
		{
			sectionFromTimeDefaultCheckBox.setSelected(false);
			sectionFromTimeTextField.setText(String.valueOf(sec.getDrawStartTime()));
		}
		if (sec.getDrawEndTime() == -1)
		{
			sectionToTimeDefaultCheckBox.setSelected(true);
			if (sec.getChart() != null)
			{
				sectionToTimeTextField.setText(String.valueOf(sec.getChart().getDefaultEndTime()));
			}
			else
			{
				sectionToTimeTextField.setText("-1");
			}
		}
		else
		{
			sectionToTimeDefaultCheckBox.setSelected(false);
			sectionToTimeTextField.setText(String.valueOf(sec.getDrawEndTime()));
		}
		updateSectionChartOptions();
	}


	public void selectDataset(Dataset dataset, int depth) {
		LogAndDebug.tracep("(" + LogAndDebug.toText(dataset)+", "+ depth +")");
		updateDatasetCardGuiFromModel(dataset);
		displayDataset();
		LogAndDebug.tracem("(" + LogAndDebug.toText(dataset)+", "+ depth +")");
	}


	// update methods
	/**
	 * update dataset card (gui) from model when changing selection (Report, Section or Dataset)
	 * @param dataset
	 */
	private void updateDatasetCardGuiFromModel(Dataset dataset) {
		LogAndDebug.tracep("(" + LogAndDebug.toText(dataset) +")");

		// update Global Options Tab
		datasetTitleTextField.setText(dataset.getTitle());
		datasetTitleTextField.setBackground(Color.WHITE);

		datasetCommentTextArea.setText(dataset.getComment());
		datasetCommentTextArea.setBackground(Color.WHITE);

		// update Statistical Text Area		
		StringBuffer txt = new StringBuffer("");
		for(Statistics statistics : dataset.getStatistics()){
			txt.append(statistics.exportToTxt(0));
		}
		datasetStatsTextArea.setText(txt.toString());

		// update Datasources Tab
		updateDatasourcesTab(dataset);

		// update filtering global data
		Statistics stats;
		switch(dataset.getDatasetType()){
		case SIMPLE_DATASET:
		case AGGREGATE_DATASET:
			stats = dataset.getStatistics().get(0);
			timeFilteredEventsNumberLabel.setText(formatTimeFilteredEventsNumber(stats));
			eventsFilterEventsNumberLabel.setText(formatEventsFilteredNumber(stats));
			if (useStatisticalFiltering){
				statsFilterEventsNumberLabel.setText(formatStatsFilteredNumber(stats));
			}
			break;
		case MULTIPLE_DATASET:
			for(Datasource datasource : dataset.getDatasources()){
				stats = datasource.getStatistics();
				timeFilteredEventsNumberLabel.setText(formatTimeFilteredEventsNumber(stats));
				eventsFilterEventsNumberLabel.setText(formatEventsFilteredNumber(stats));
				if (useStatisticalFiltering){
					statsFilterEventsNumberLabel.setText(formatStatsFilteredNumber(stats));
				}
			}
			break;
		default:
		}

		//  update Time Filtering Tab
		filterStartTimeTextField.setText(Long.toString(dataset.getFilterStartTime()));
		timeFilterSetStartAtFirstEvent(dataset, dataset.isStartFilterAtFirstEvent());
		filterEndTimeTextField.setText(Long.toString(dataset.getFilterEndTime()));
		timeFilterSetEndAtLastEvent(dataset, dataset.isEndFilterAtLastEvent());

		updateFieldComboBox(dataset); 
		
		String field;
		String operator;
		String value;
		// clearing TableModel
		while(datasetFieldsValulesFilterTableModel.getRowCount() >0){
			datasetFieldsValulesFilterTableModel.removeRow(0);
		}
		
		// adding filter expressions
		filterTable.setModel(datasetFieldsValulesFilterTableModel);
		FieldsValuesFilter fieldsValuesFilter = dataset.getFieldsValuesFilter();
		for(FieldFilterAndOp ffo : fieldsValuesFilter.getFilterList()){
			field = ffo.getField();
			operator = ffo.getOperator();
			value = ffo.getValue();
			jbAddFilterActionPerformed(field, operator, value);
		}
		// 
		switch (fieldsValuesFilter.getLogicalOp()){
		case OR:
			orRadioButton.setSelected(true);
			break;
		case AND:
		default:
			andRadioButton.setSelected(true);
		}

		// update Chart Options Tab
		datasetNoChartRadio.setSelected(!dataset.isPerformDraw());
		datasetTimeBasedRadio.setSelected(dataset.isPerformTimeChart());
		if (dataset.isPerformTimeChart())
		{
			datasetRawCheckBox.setEnabled(true);
			SwingHelper.setEnabled(datasetMovingOptionsPanel, true);
		}
		else
		{
			datasetRawCheckBox.setEnabled(false);
			SwingHelper.setEnabled(datasetMovingOptionsPanel, false);
		}
		datasetRawCheckBox.setSelected(dataset.isDrawRaw());
		datasetRawCheckBox.setEnabled(dataset.isPerformDraw() && dataset.isPerformTimeChart());
		datasetHistogramRadio.setSelected(dataset.isPerformHistogram());
		datasetquantilesRadio.setSelected(dataset.isPerformQuantiles());
		datasetMovingMinCheckBox.setSelected(dataset.isDrawMovingMin());
		datasetMovingMaxCheckBox.setSelected(dataset.isDrawMovingMax());
		datasetMovingAverageCheckBox.setSelected(dataset.isDrawMovingAverage());
		datasetMovingMedianCheckBox.setSelected(dataset.isDrawMovingMedian());
		datasetMovingStdDevCheckBox.setSelected(dataset.isDrawMovingStdDev());
		datasetMovingThroughputCheckBox.setSelected(dataset.isDrawMovingThroughput());
		timeWindowTextField.setText(Integer.toString(dataset.getTimeWindow()));
		stepTextField.setText(Integer.toString(dataset.getStep()));
	}


	private void updateFieldComboBox(Dataset dataset)
	{
		if (null == fieldComboBox){
			fieldComboBox = new JComboBox<String>();
		}
		try {
			// get the first blade event
			if (dataset.getDatasources().size() > 0){
				SortedSet<String> fields = data.getFieldLabels(dataset);
				fieldComboBox.removeAllItems();
				for (String label : fields)
				{
					fieldComboBox.addItem(label);
				}
			}
		}
		catch (Exception e) {
			LogAndDebug.err("()  Exception: "+ e);
		}
	}


	private String formatTimeFilteredEventsNumber(Statistics stats) {
		return formatFilteredEventsNumber(stats.getOriginalEventsNumber(), stats.getTimeFilteredEventsNumber(), "Time");
	}


	private String formatEventsFilteredNumber(Statistics stats) {
		return formatFilteredEventsNumber(stats.getTimeFilteredEventsNumber(), stats.getFiledsValueFilteredEventsNumber(), "Events values");
	}


	private String formatStatsFilteredNumber(Statistics stats) {
		return formatFilteredEventsNumber(stats.getFiledsValueFilteredEventsNumber(), stats.getStatsFilteredEventsNumber(), "Statistics");
	}

	
	private String formatFilteredEventsNumber(int before, int after, String filterName){
		int discarded = before - after;
		return filterName + " filter: " + before + " events, " + discarded + " discarded, " + after + " remaining";
	}


	private void updateDatasourcesTab(Dataset dataset) {
		updateDatasourcesTableModel(dataset);
	}


	public void updateOptions() {		// TODO change the name to updateSelectionClass
		LogAndDebug.tracep("()");
		Object object = rmView.getSelectedObject();
		String className = object.getClass().getSimpleName();
		if (className.equals(REPORT)){
			selectReport((Report) object, 1);
		}else if(className.equals(SECTION)){
			selectSection((Section) object, 1);
		}else if (className.equals(DATASET)){
			selectDataset((Dataset) object, 1);
		}else{
			LogAndDebug.trace("().... object of class \"" + className + " not recognized.");
		}
		LogAndDebug.tracem("()");
	}


	public void updateDatasetChartOptionsFromGui()
	{
		Dataset dataset = rmView.getSelectedDataset();
		if (dataset != null)
		{
			dataset.setPerformDraw(!datasetNoChartRadio.isSelected());
			dataset.setPerformTimeChart(datasetTimeBasedRadio.isSelected());
			dataset.setDrawRaw(datasetRawCheckBox.isSelected());
			dataset.setPerformHistogram(datasetHistogramRadio.isSelected());
			dataset.setPerformQuantiles(datasetquantilesRadio.isSelected());
			dataset.setTimeWindow(
				DataAccess.integerParse(
					timeWindowTextField.getText(),
					dataset.getTimeWindow()));
			dataset.setStep(
				DataAccess.integerParse(
					stepTextField.getText(),
					dataset.getStep()));
			dataset.setDrawMovingMin(datasetMovingMinCheckBox.isSelected());
			dataset.setDrawMovingMax(datasetMovingMaxCheckBox.isSelected());
			dataset.setDrawMovingAverage(datasetMovingAverageCheckBox.isSelected());
			dataset.setDrawMovingMedian(datasetMovingMedianCheckBox.isSelected());
			dataset.setDrawMovingStdDev(datasetMovingStdDevCheckBox.isSelected());
			dataset.setDrawMovingThroughput(datasetMovingThroughputCheckBox.isSelected());
		}
	}


	private void exportReport()
	{
		switch (data.report.getExportFormat())
		{
		case HTML_FORMAT:
			rmView.exportReportToHtml();
			break;
/* TODO: uncomment when export methods are rewritten
		case XML_FORMAT:
			rmView.exportReportToXml();
			break;
		case TXT_FORMAT:
			rmView.exportReportToTxt();
			break;
*/		default:
			rmView.exportReportToHtml();
			break;
		}
	}

	
	private void setBorder(Component panel, String title){
		TitledBorder titleBorder;
		titleBorder = BorderFactory.createTitledBorder(title);
		((JComponent) panel).setBorder(titleBorder);
	}

/*
	private void setChartFormatPNG(){
		data.report.setImageFormat(imageFormats.PNG_FORMAT);
		LogAndDebug.log(" Export chart format set to " + data.report.getImageFormat() + ".");
	}


	private void setChartFormatJPG(){
		data.report.setImageFormat(imageFormats.JPG_FORMAT);
		LogAndDebug.log(" Export chart format set to " + data.report.getImageFormat() + ".");
	}


	private void setChartFormatSVG(){
		data.report.setImageFormat(imageFormats.SVG_FORMAT);
		LogAndDebug.log(" Export chart format set to " + data.report.getImageFormat() + ".");
	}

	
	private void setExportFormatHTML(){
		data.report.setExportFormat(exportFormats.HTML_FORMAT);
		LogAndDebug.log(" Export format set to " + data.report.getExportFormat() + ".");
	}

	
	private void setExportFormatXML(){
		data.report.setExportFormat(exportFormats.XML_FORMAT);
		LogAndDebug.log(" Export format set to " + data.report.getExportFormat() + ".");
	}
	

	private void setExportFormatTXT(){
		data.report.setExportFormat(exportFormats.TXT_FORMAT);
		LogAndDebug.log(" Export format set to " + data.report.getExportFormat() + ".");
	}
*/
	/**
	private void saveMacro(){
		rmView.saveMacro();
//		LogAndDebug.unimplemented();
	}

	// OK 
	private void loadMacro(){	
		rmView.loadMacro();
	//	LogAndDebug.unimplemented();
	}
	
	private void loadReport(){	
		rmView.loadReport();
	//	LogAndDebug.unimplemented();
	}
	**/

	// Time filtering
	@SuppressWarnings("unused")
	private void timeFilterUpdateStartTime(String text) {
		Dataset dataset = rmView.getSelectedDataset();
		long l = dataset.getFilterStartTime();
		try{
			l = Long.parseLong(text);
		}
		catch (NumberFormatException e) {
			LogAndDebug.trace("("+text+")  *** NumberFormatException: "+ e);
		}
		dataset.setFilterStartTime(l);
	}


	@SuppressWarnings("unused")
	private void timeFilterUpdateEndTime(String text) {
		Dataset dataset = rmView.getSelectedDataset();
		long l = dataset.getFilterEndTime();
		try{
			l = Long.parseLong(text);
		}
		catch (NumberFormatException e) {
			LogAndDebug.trace("("+text+")  *** NumberFormatException: "+ e);
		}
		dataset.setFilterEndTime(l);
	}

	
	public void timeFilterSetStartAtFirstEvent(Dataset dataset, boolean b) {
		startFilterAtFirstEventCheckBox.setSelected(b);
		if (b){
			timeFilterSetStartAtFirstEvent(dataset);
		}else{
			timeFilterUnsetStartAtFirstEvent(dataset);
		}
	}

	
	public void timeFilterSetStartAtFirstEvent(Dataset dataset) {
		LogAndDebug.tracep("()");
//		Dataset dataset = rmView.getSelectedDataset();
		if (null == dataset){
			LogAndDebug.err("() dataset == null.  Selected object= " + LogAndDebug.toText(rmView.getSelectedObject()));
			LogAndDebug.tracem("()");
			return;
		}
		dataset.setStartFilterAtFirstEvent(true);
		dataset.setFilterStartTime(dataset.getFirstEventTime());
		filterStartTimeTextField.setEditable(!startFilterAtFirstEventCheckBox.isSelected());
		filterStartTimeTextField.setEnabled(!startFilterAtFirstEventCheckBox.isSelected());
		filterStartTimeTextField.setText(Long.toString(dataset.getFilterStartTime()));
		LogAndDebug.tracem("()");
	}

	
	public void timeFilterUnsetStartAtFirstEvent(Dataset dataset) {
		LogAndDebug.tracep("()");
//		Dataset dataset = rmView.getSelectedDataset();
		dataset.setStartFilterAtFirstEvent(false);
		dataset.setFilterStartTime(DataAccess.longParse(filterStartTimeTextField.getText(), dataset.getFilterStartTime()));
		filterStartTimeTextField.setEditable(!startFilterAtFirstEventCheckBox.isSelected());
		filterStartTimeTextField.setEnabled(!startFilterAtFirstEventCheckBox.isSelected());
		LogAndDebug.tracem("()");
	}


	public void timeFilterSetEndAtLastEvent(Dataset dataset, boolean b) {
		endFilterAtLastEventCheckBox.setSelected(b);
		if (b){
			timeFilterSetEndAtLastEvent(dataset);
		}else{
			timeFilterUnsetEndAtLastEvent(dataset);
		}
	}


	public void timeFilterSetEndAtLastEvent(Dataset dataset) {
		LogAndDebug.tracep("()");
//		Dataset dataset = rmView.getSelectedDataset();
		dataset.setEndFilterAtLastEvent(true);
		dataset.setFilterEndTime(dataset.getLastEventTime());
		filterEndTimeTextField.setText(Long.toString(dataset.getFilterEndTime()));
		filterEndTimeTextField.setEditable(!endFilterAtLastEventCheckBox.isSelected());
		filterEndTimeTextField.setEnabled(!endFilterAtLastEventCheckBox.isSelected());
		LogAndDebug.tracem("()");
	}

	
	public void timeFilterUnsetEndAtLastEvent(Dataset dataset) {
		LogAndDebug.tracep("()");
//		Dataset dataset = rmView.getSelectedDataset();
		dataset.setEndFilterAtLastEvent(false);
		dataset.setFilterEndTime(DataAccess.longParse(filterEndTimeTextField.getText(), dataset.getFilterEndTime()));
		filterEndTimeTextField.setText(Long.toString(dataset.getFilterEndTime()));
		filterEndTimeTextField.setEditable(!endFilterAtLastEventCheckBox.isSelected());
		filterEndTimeTextField.setEnabled(!endFilterAtLastEventCheckBox.isSelected());
		LogAndDebug.tracem("()");
	}


	/**
	 *  update filtering after parameters modification: updates all (dataset) tabs
	 */
	@SuppressWarnings("unused")
	private void updateFiltering() {
		LogAndDebug.tracep("updateFiltering"+"()");
		Dataset dataset = rmView.getSelectedDataset();
		updateDatasetTimeFilteringParamsFromGui(dataset);
		updateDatasetFieldsValuesFilteringParamsFromGui(dataset);
		updateDatasetStatisticalFilteringParamsFromGui(dataset);
		rmView.updateFiltering(dataset);
		rmView.setSelectedDataset(dataset, 1);
		LogAndDebug.tracem("()");
	}


	private void updateDatasourcesAliases(Dataset dataset)
	{
		Iterator<Datasource> dataSourceIter = dataset.getDatasources().iterator();
		int i=0;
		while (dataSourceIter.hasNext())
		{
			Datasource source = dataSourceIter.next();
			source.setAlias(datasourcesTableModel.getValueAt(i++, 0));
		}
		updateSectionCard(dataset.getSection());
		rmView.updateSectionDisplay(dataset.getSection());
		rmView.selectDataset(dataset);
	}


	/**
	 * update filtering (and chart) after modification of time filtering values
	 * must also update FieldsValuesFiltering and StatisticalFiltering.
	 */
	private void updateTimeFiltering(){
		Dataset dataset = rmView.getSelectedDataset();
		updateDatasetTimeFilteringParamsFromGui(dataset);
		rmView.updateFiltering(dataset);
		updateSectionCard(dataset.getSection());
		rmView.updateSectionDisplay(dataset.getSection());
		rmView.selectDataset(dataset);
	}


	/**
	 * update filtering (and chart) after modification of fieldsValuesFiltering attributes
	 * must also update StatisticalFiltering.
	 * @param dataset 
	 */
	private void updateFieldsValuesFiltering(Dataset dataset) {
		updateDatasetFieldsValuesFilteringParamsFromGui(dataset);
//		updateDatasetStatisticalFilteringParamsFromGui(dataset);	
		//  TODO uncomment when implementing StatisticalFiltering
		// TODO limit update only to fieldsValuesFilteringfTab and StatisticalFiltering
		updateSectionCard(dataset.getSection());
		rmView.updateSectionDisplay(dataset.getSection());
		rmView.unselectObject();		// TODO is this necessary?
		rmView.selectDataset(dataset);
	}


	public void updateSectionFromGui(Section sec)
	{
		sec.setTitle(sectionTitleTextField.getText());
		sec.setComment(sectionCommentTextArea.getText());
		if (timeBasedRadioButton.isSelected())
		{
			sec.setChartRepresentation(ChartRepresentation.TIME_BASED);
			if (sectionFromTimeDefaultCheckBox.isSelected())
			{
				sec.setDrawStartTime(-1);
			}
			else
			{
				sec.setDrawStartTime(Long.parseLong(sectionFromTimeTextField.getText()));
			}
			if (sectionToTimeDefaultCheckBox.isSelected())
			{
				sec.setDrawEndTime(-1);
			}
			else
			{
				sec.setDrawEndTime(Long.parseLong(sectionToTimeTextField.getText()));
			}
		}
		else if (histogramRadioButton.isSelected())
		{
			sec.setChartRepresentation(ChartRepresentation.HISTOGRAM);
		}
		else if (quantileRadioButton.isSelected())
		{
			sec.setChartRepresentation(ChartRepresentation.QUANTILE);
		}
		sec.setSlicesNb(DataAccess.integerParse(slicesNumberTextField.getText(), sec.getSlicesNb()));
		sec.setQuantilesNb(DataAccess.integerParse(quantilesNumberTextField.getText(), sec.getQuantilesNb()));
	}


	// listener
	@Override
	public void actionPerformed(ActionEvent event) {
		Object source = event.getSource();

		String className = source.getClass().getSimpleName();
		if (className.equals("AbstractButton")){
			actionButtonPerformed(event);
		}else if (className.equals("JTextField")){
			actionJTextFieldPerformed(event);
		}else if (className.equals("JCheckBox")){
			actionJCheckBoxPerformed(event);
		}else if (className.equals("JRadioButton")){
			actionJRadioButtonPerformed(event);
		}else if (className.equals("JButton")){
			actionButtonPerformed(event);
		}else{
			LogAndDebug.log("actionPerformed: cannot recognize " + className + " object.");
		}	
	}


	public void actionButtonPerformed(ActionEvent event) {
		Object source = event.getSource();
		String commandName = ((AbstractButton) source).getText();

		Object currentObject = rmView.getSelectedObject();
		String className = currentObject.getClass().getSimpleName();

		LogAndDebug.trace("You have clicked on \""+ ((AbstractButton)source).getName()
				+ "\" ("+ commandName +")  current selection: "+LogAndDebug.toText(currentObject));

		if (className.equals("Report")){	
			if (source == addDatasetIntoNewSectionButton){
				control.addDatasetInNewSection();
			}else if(source == exportButton) {
				exportReport();
//			} else if(source == saveMacroButton) {
//				saveMacro();
//			} else if(source == loadMacroButton) {				
//				loadMacro();
//			} else if(source == loadReportButton) {				
//				loadReport();
			} else {
				LogAndDebug.log(".... unknown command");
			}
		}else if (className.equals("Section")){
			Section sec = (Section) currentObject;
			if (source == addDatasetToSectionButton){
				control.addDataset(sec);
			}else if (source == sectionRemoveCommandButton){
				control.removeSection(sec);
			} else if(source == sectionApplyButton) {
				rmView.updateSectionDisplay(sec);
				rmView.selectSection(sec, 2);
			} else {
				LogAndDebug.log(".... unknown command");
			}
		}
		else if (className.equals("Dataset"))
		{
			Dataset dataset = (Dataset)currentObject;
			if(source == redrawButton)
			{
				Section sec = dataset.getSection();
				updateSectionCard(sec);
				ChartRepresentation sectionChartType = sec.getChartRepresentation();
				ChartRepresentation plotType = null;
				if (datasetTimeBasedRadio.isSelected())
				{
					plotType = ChartRepresentation.TIME_BASED;
				}
				else if (datasetHistogramRadio.isSelected())
				{
					plotType = ChartRepresentation.HISTOGRAM;
				}
				else if (datasetquantilesRadio.isSelected())
				{
					plotType = ChartRepresentation.QUANTILE;
				}
				boolean cancel = false;
				if (plotType != null && plotType != sectionChartType)
				{
					int reply = JOptionPane.showConfirmDialog(
						this,
						"Changing the plot type will apply globally to current section",
						"Plot type change warning",
						JOptionPane.OK_CANCEL_OPTION,
						JOptionPane.WARNING_MESSAGE);
					if (reply == JOptionPane.OK_OPTION)
					{
						sec.setChartRepresentation(plotType);
						updateSectionCard(sec);
					}
					else
					{
						cancel = true;
					}
				}
				if (! cancel)
				{
					updateDatasetChartOptionsFromGui();
					rmView.updateSectionDisplay(sec);
					rmView.unselectObject();
					rmView.selectDataset(dataset);
				}
			} else if (source == timeFilterApplyButton){
				updateTimeFiltering();
			} else if (source == addFilterInFilterValuesFilteringButton){
				jbAddFilterActionPerformed(null, null, null);
			} else if (source == fieldValuesFilteringApplyButton){
				updateFieldComboBox(dataset); 
				
				// adding filter expressions				
	
				int colCount = datasetFieldsValulesFilterTableModel.getColumnCount();

				int rowCount = datasetFieldsValulesFilterTableModel.getRowCount();
				for (int i = 0; i < rowCount; i++) {
					// don't take the last column which is delete column
					for (int j = 0; j < colCount - 1; j++) {
						String cellsContent = datasetFieldsValulesFilterTableModel.getValueAt(i, j).toString();
						if (cellsContent.equals("")) {
							dataset.setFieldsValuesFilter(null);
							LogAndDebug.trace("("+LogAndDebug.toText(dataset)+")  cell["+i+", "+j+"] is empty.");
							return;
						}
					}
				}
//				if (null == dataset.getFieldsValuesFilter()) {
//					LogAndDebug.err(" ERROR: Filtering table is not correctly filled...");
//					JOptionPane.showMessageDialog(this, "Malformed filter. Please correct it",
//							"Warning", JOptionPane.WARNING_MESSAGE);				
//				}
//				else {
				updateFieldsValuesFiltering(dataset);
//				}
			}else if (source == datasetRemoveCommandButton){
				updateSectionCard(dataset.getSection());
				control.removeDataset(dataset);
			}else if (source == datasetGlobalOptionsApplyButton){
				Section sec = dataset.getSection();
				dataset.setName(datasetTitleTextField.getText());
				rmView.updateSectionDisplay(sec);
				rmView.unselectObject();		// TODO is this necessary?
				rmView.selectDataset(dataset);
			}
			else if (source == datasetDatasourcesApplyButton)
			{
				updateDatasourcesAliases(dataset);
			}
			else {
				LogAndDebug.log(".... unknown command");
			}
		}else{
			LogAndDebug.log("() .... unknown selected object \""+ className + "\"");
		}
	}


	private void actionJRadioButtonPerformed(ActionEvent event)
	{
		Object source = event.getSource();
		Object currentObject = rmView.getSelectedObject();
		String className = currentObject.getClass().getSimpleName();

		if (className.equals("Section"))
		{
			Section section = (Section) rmView.getSelectedObject();
			if (source == timeBasedRadioButton)
			{
				section.setChartRepresentation(ChartRepresentation.TIME_BASED);
				updateSectionChartOptions();
			}
			else if (source == histogramRadioButton)
			{
				section.setChartRepresentation(ChartRepresentation.HISTOGRAM);
				updateSectionChartOptions();
			}
			else if (source == quantileRadioButton)
			{
				section.setChartRepresentation(ChartRepresentation.QUANTILE);
				updateSectionChartOptions();
			} 
		}
		else if (className.equals("Dataset"))
		{
			Dataset dataset = (Dataset) rmView.getSelectedObject();
			if (source == orRadioButton)
			{
				logicalOp = LogicalEnum.OR;
				dataset.setLogicalOp(LogicalEnum.OR);
			}
			else if (source == andRadioButton)
			{
				logicalOp = LogicalEnum.AND;
				dataset.setLogicalOp(LogicalEnum.AND);
			}
			else if (source == datasetTimeBasedRadio)
			{
				datasetRawCheckBox.setEnabled(true);
				SwingHelper.setEnabled(datasetMovingOptionsPanel, true);
			}
			else
			{
				datasetRawCheckBox.setEnabled(false);
				SwingHelper.setEnabled(datasetMovingOptionsPanel, false);
			}
		}
	}


	private void actionJTextFieldPerformed(ActionEvent event) {
		Object source = event.getSource();
		String commandName = ((JTextField) source).getName();
		String text = ((JTextField) source).getText();
		Object currentObject = rmView.getSelectedObject();
		String className = currentObject.getClass().getSimpleName();

		guiObjectsListeners();
		if (className.equals(REPORT)){
			if (source == reportTitleTextField) {
				rmView.getReport().setTitle(text);
				setselectedObject(null, 1);
				rmView.updateReportTree();
			}else{
				LogAndDebug.trace(".... unknown command for selected object: "+LogAndDebug.toText(currentObject));
			}
		}else if (className.equals(SECTION)){
			Section sec = rmView.getSelectedSection();
			if (source == slicesNumberTextField){
				sec.setSlicesNb(DataAccess.integerParse(text, sec.getSlicesNb()));
				rmView.updateSectionDisplay(sec);
				rmView.selectSection(sec, 2);
			}else if (source == quantilesNumberTextField){
				sec.setQuantilesNb(DataAccess.integerParse(text, sec.getQuantilesNb()));
				rmView.updateSectionDisplay(sec);
				rmView.selectSection(sec, 2);
			}else if (source == sectionTitleTextField){
				sec.setTitle(text);
				rmView.updateSectionDisplay(sec);
			}else{
				LogAndDebug.trace(".... unknown command for selected object: "+LogAndDebug.toText(currentObject));
			}
		}else if (className.equals(DATASET)){
			Dataset dataset = rmView.getSelectedDataset();
			if (source == timeWindowTextField){
				dataset.setTimeWindow(DataAccess.integerParse(text, dataset.getTimeWindow()));
				timeWindowTextField.setText(Integer.toString(dataset.getTimeWindow()));
			}else if(source == stepTextField) {
				dataset.setStep(DataAccess.integerParse(text, dataset.getStep()));
				stepTextField.setText(Integer.toString(dataset.getStep()));
			}else if(source == datasetTitleTextField) {
				dataset.setName(text);
				Section sec = dataset.getSection();
				rmView.updateSectionDisplay(sec);
				rmView.unselectObject();
				rmView.selectDataset(dataset);
			}else if(source == filterStartTimeTextField) {
				dataset.setFilterStartTime(DataAccess.longParse(text, dataset.getFilterStartTime()));
				timeFilterSetStartAtFirstEvent(dataset, dataset.isStartFilterAtFirstEvent());
			}else if(source == filterEndTimeTextField) {
				dataset.setFilterEndTime(DataAccess.longParse(text, dataset.getFilterEndTime()));
				timeFilterSetEndAtLastEvent(dataset, dataset.isEndFilterAtLastEvent());
			}else if(source == maxPointsNumberTextField) {
				dataset.setMaxPointsNb(DataAccess.integerParse(text, dataset.getMaxPointsNb()));
				maxPointsNumberTextField.setText(Integer.toString(dataset.getMaxPointsNb()));
			}
			else {
				LogAndDebug.trace(".... unknown command for selected object: "+LogAndDebug.toText(currentObject));
			}
		}else{
			LogAndDebug.trace("()  unknown selected object: \""+className+"\"");
		}
		LogAndDebug.tracem("(\"" + commandName + "\", " + text +") " + className);
	}


	private void actionJCheckBoxPerformed(ActionEvent event) {
		Object source = event.getSource();
		String commandName = ((AbstractButton) source).getName();
		boolean value = ((AbstractButton) source).isSelected();
		LogAndDebug.trace("actionJCheckBoxPerformed: You have clicked on \""+ commandName +"\" ..."+value);

		Object currentObject = rmView.getSelectedObject();
		String className = currentObject.getClass().getSimpleName();

		if (className.equals(SECTION))
		{
			Section sec = rmView.getSelectedSection();
			if (source == sectionFromTimeDefaultCheckBox)
			{
				if (sectionFromTimeDefaultCheckBox.isSelected())
				{
					sec.setDrawStartTime(-1);
					sectionFromTimeTextField.setEnabled(false);
					sectionFromTimeTextField.setText(String.valueOf(sec.getChart().getDefaultStartTime()));
				}
				else
				{
					sec.setDrawStartTime(sec.getChart().getDefaultStartTime());
					sectionFromTimeTextField.setEnabled(true);
				}
			}
			else if (source == sectionToTimeDefaultCheckBox)
			{
				if (sectionToTimeDefaultCheckBox.isSelected())
				{
					sec.setDrawEndTime(-1);
					sectionToTimeTextField.setEnabled(false);
					sectionToTimeTextField.setText(String.valueOf(sec.getChart().getDefaultEndTime()));
				}
				else
				{
					sec.setDrawEndTime(sec.getChart().getDefaultEndTime());
					sectionToTimeTextField.setEnabled(true);
				}
			}
		}
		else if (className.equals(DATASET))
		{
			Dataset dataset = rmView.getSelectedDataset();
			if (commandName.equals(FILTER_START_AT_FIRST_EVENT)){
				timeFilterSetStartAtFirstEvent(dataset, startFilterAtFirstEventCheckBox.isSelected());
				filterStartTimeTextField.setEnabled(!startFilterAtFirstEventCheckBox.isSelected());
			}else if (commandName.equals(FILTER_END_AT_LAST_EVENT)){
				timeFilterSetEndAtLastEvent(dataset, endFilterAtLastEventCheckBox.isSelected());
				filterEndTimeTextField.setEditable(!endFilterAtLastEventCheckBox.isSelected());
			}else{
				LogAndDebug.trace(".... unknown command for selected object: "+LogAndDebug.toText(currentObject));
			}
		}else {
			LogAndDebug.trace("()  unknown selected object: \""+className+"\"");
		}
	}


	////  Tables related classes
	// StatsTableModel
	public class StatsTableModel extends AbstractTableModel {

		/**
		 * 
		 */
		private static final long serialVersionUID = -2245120521189918667L;
		private String[][] statsTable;
		private final int rows = 16;
		private int columns;
		//		private final String headers[];

		public void SparseTableModel() {
			columns = selectedDataset.getDatasources().size() + 1;
			statsTable = new String[rows][columns];

			int col = 0;
			int line = 0;
			statsTable[col][line++] = new String("Dataset definition");
			statsTable[col][line++] = "Alias";
			statsTable[col][line++] = "Dataset Type";
			statsTable[col][line++] = "Test name";
			statsTable[col][line++] = "Blade";
			statsTable[col][line++] = "Event";
			statsTable[col][line++] = "Field";
			statsTable[col][line++] = "Time Interval";
			statsTable[col][line++] = "Filter";
			statsTable[col][line++] = "Throughput";
			statsTable[col][line++] = "Minimum";
			statsTable[col][line++] = "Maximum";
			statsTable[col][line++] = "Average";
			statsTable[col][line++] = "Median";
			statsTable[col][line++] = "Std deviation";
			statsTable[col][line++] = "Nb. values";

			if (null == selectedDataset){
				;
			}else if (selectedDataset.isPerformStatAnalysis()){
				// perform statistics; fields to display: dataset (descriptors), throughput, nb of values, filter, time interval, min, max, average, median, std dev
				;
			}else{	// no statistics; fields to display: dataset (descriptors), throughput, nb of values
				;
			}
		}

		
		public int getColumnCount() {
			return columns;
		}

		public int getRowCount() {
			return rows;
		}

		
		public String getColumnName(int _col) {
			return statsTable[0][_col];
		}

		
		public Object getValueAt(int _row, int _col) {
			return statsTable[_row][_col];
		}

		
		public void setValueAt(String value, int _row, int _col) {
			if ((rows < 0) || (columns < 0)) {
				throw new IllegalArgumentException("Invalid row/column setting");
			}
			if ((_row < rows) && (_col < columns)) {
				statsTable[_row][_col] = value;
			}

		}
	}		//	 class StatsTableModel


	// class DatasourcesTableModel
	public class DatasourcesTableModel extends AbstractTableModel {

		private static final long serialVersionUID = 2054423862357399601L;
		private final String[] headers = {"alias", "test name", "blade id", "event type", "field"};	
		// TODO add  "remove" column
		private List<Datasource> datasourcesList = new ArrayList<Datasource>();

		public DatasourcesTableModel() {
			super();
			datasourcesList = new ArrayList<Datasource>();
		}

		public DatasourcesTableModel(Dataset dataset) {
			super();
			LogAndDebug.tracep("(" + LogAndDebug.toText(dataset) + ")");
			datasourcesList = new ArrayList<Datasource>();
			for(Datasource dsource : dataset.getDatasources()){
				LogAndDebug.trace(" adding " + LogAndDebug.toText(dsource) + "...");
				addDatasource(dsource);
			}
			LogAndDebug.tracem("(" + LogAndDebug.toText(dataset) + ")");
		}

		@Override
		public String getColumnName(int columnIndex) {
			return headers[columnIndex];
		}

		@Override
		public int getRowCount() {
			if (null == datasourcesList) return 0;
			return datasourcesList.size();
		}

		@Override
		public int getColumnCount() {
			return headers.length;
		}

		/**
		 * Only the first column is editable (set an alias instead of canonical name)
		 */
		@Override
		public boolean isCellEditable(int row, int col)
		{
			return col == 0;
		}

		@Override
		public void setValueAt(Object value, int rowIndex, int columnIndex)
		{
			String alias = value.toString().trim();
			if (alias.isEmpty())
			{
				alias = null;
			}
			datasourcesList.get(rowIndex).setAlias(alias);
		}

		@Override
		public String getValueAt(int rowIndex, int columnIndex) {
			switch(columnIndex){
			case 0:
				return datasourcesList.get(rowIndex).getName();
			case 1:
				return datasourcesList.get(rowIndex).getTestName();
			case 2:
				return datasourcesList.get(rowIndex).getBladeId();
			case 3:
				return datasourcesList.get(rowIndex).getEventType();
			case 4:
				return datasourcesList.get(rowIndex).getField();
			default:
				return null; // should never occur
			}
		}

		public void addDatasource(Datasource dsource) {
			datasourcesList.add(dsource);
			fireTableRowsInserted(getRowCount() -1, getColumnCount() -1);
		}

		public void removeDatasource(int rowIndex) {
			datasourcesList.remove(rowIndex);
			fireTableRowsDeleted(rowIndex, rowIndex);
		}

		public void removeAll(){
			if (null == datasourcesList){
				datasourcesList = new Dataset().getDatasources();
			}
			for (int i = datasourcesList.size()-1; i >=0; i--){
				removeDatasource(i);
			}
		}
	}	// class DatasourcesTableModel



	private void guiObjectsListeners() {
		/////////////////// GUI objects listeners

		datasetCommentTextArea.addFocusListener(new FocusListener() {
			public void focusGained(FocusEvent e) {}

			public void focusLost(FocusEvent e) {
				datasetCommentTextArea.setBackground(Color.WHITE);
				try{
//					updateFilterStartTime(Long.parseLong(datasetCommentTextArea.getText()));
					// TODO Auto-generated method stub
					LogAndDebug.trace((e.isTemporary() ? " (temporary):" : ":")
							+  e.getComponent().getClass().getName()
							+ " \""
							+  e.getComponent().getName()
							+ " \"; Opposite component: " 
							+ (e.getOppositeComponent() != null ?
									e.getOppositeComponent().getClass().getName() : "null")
					); 
					String componentName = e.getComponent().getName();
					if (null != componentName){
						if (componentName.equals(REPORT_TITLE)){
							data.report.setTitle(reportTitleTextField.getText());
						}else if (componentName.equals(SECTION_TITLE)){
							Section sec = rmView.getSelectedSection();
							if (null == sec){
								LogAndDebug.trace("A section title  has been pointed but not selected");
							}else{
								sec.setTitle(sectionTitleTextField.getText());
							}
						}else if (componentName.equals(DATASET_TITLE)){
							Dataset dataset = rmView.getSelectedDataset();
							dataset.setName(datasetTitleTextField.getText());
							rmView.updateReportTree();
						}else{
							LogAndDebug.unimplemented("() for component \""+componentName+"\"");
						}
					}				
				}
				catch (Exception ex) {
					if (datasetCommentTextArea.isFocusOwner()){
//						datasetCommentTextArea(data.wizardDataset.getFirstEventTime());	// not a number but will take first event time 
					}else{
						;		//	... just ignore it...
					}
				}
				datasetCommentTextArea.getParent().requestFocus();
			} // focusLost
		});


	}	

	
	/// swing components ///
	
//////////////_____________/////////////
///////////// report card  /////////////
//////////////_____________/////////////
	private JPanel reportCard; 
	private JPanel reportProperties;
	private JTextField reportTitleTextField;
	private JTextArea reportCommentTextArea;
	private JScrollPane reportCommentScrollPane = new JScrollPane();

/*	private JRadioButton htmlRadioButton;
	private JRadioButton xmlRadioButton;
	private JRadioButton txtRadioButton;

	private JRadioButton pngRadioButton;
	private JRadioButton jpgRadioButton;
	private JRadioButton svgRadioButton;
*/
	private JButton exportButton;

//	private JButton saveMacroButton;
//	private JButton loadMacroButton;
	
//	private JButton loadReportButton;
	
	private JButton addDatasetIntoNewSectionButton;
	private JButton addDatasetToSectionButton;
	private JButton sectionRemoveCommandButton;


	// section card
	private JPanel sectionCard; 
	private JPanel sectionProperties;
	private JPanel sectionChartOptionsPanel;
	private JTextField sectionTitleTextField = new JTextField("<uninitialized section title>", 30);
	private JTextArea sectionCommentTextArea = new JTextArea("<uninitialized section comment>");
	private JScrollPane sectionCommentScrollPane = new JScrollPane();
	private JLabel sectionFromTimeLabel;
	private JTextField sectionFromTimeTextField;
	private JCheckBox sectionFromTimeDefaultCheckBox;
	private JLabel sectionToTimeLabel;
	private JTextField sectionToTimeTextField;
	private JCheckBox sectionToTimeDefaultCheckBox;
	private JTextField quantilesNumberTextField;
	private JTextField maxPointsNumberTextField;
	private JTextField slicesNumberTextField;

	JLabel slicesNumberLabel;
	JLabel quantilesNumberLabel;

//	private JRadioButton jrbNoChart;
	private JRadioButton timeBasedRadioButton;
	private JRadioButton histogramRadioButton;
	private JRadioButton quantileRadioButton;
	private JButton sectionApplyButton;

//////////////_____________/////////////
//////////////dataset card /////////////
//////////////_____________/////////////
	private JTabbedPane datasetCard; 

	// dataset global options Tab
	private JTextField datasetTitleTextField;
	private JTextArea datasetCommentTextArea;
	private JScrollPane datasetCommentScrollPane;
	private JButton datasetGlobalOptionsApplyButton;
	private JButton datasetRemoveCommandButton;

/////////////// dataset statistics/////////////
	private JTextArea   datasetStatsTextArea = new JTextArea("<uninitalized dataset comment>");


/////////////// datasources Tab/////////////
//	TableModel datasources;
	private JScrollPane datasetDatasourcesScrollPane;
	DatasourcesTableModel datasourcesTableModel;
	JTable datasourcesJTable;
	private JPanel datasetDatasourcesTab;
	private JButton datasetDatasourcesApplyButton;

// Fields Values Filtering
/////////////// dataset filtering tabs/////////////

	// Time filtering Tab  (these are the attributs to update)
	JTextField filterStartTimeTextField;
	JTextField filterEndTimeTextField;
	JCheckBox startFilterAtFirstEventCheckBox;
	JCheckBox endFilterAtLastEventCheckBox;
	JLabel    timeFilteredEventsNumberLabel;

	// Fields Values Filtering Tab
	JComboBox<String> fieldComboBox;
	JTable filterTable;
	DefaultTableModel datasetFieldsValulesFilterTableModel;
	JPanel jpLogic = null;
	JRadioButton andRadioButton;
	JRadioButton orRadioButton;
	JLabel eventsFilterEventsNumberLabel;

	// Statistical Filtering Tab
	JTextField cleaningKTextField;
	JTextField cleaningPercentPointsKeptTextField;
	JLabel statsFilterEventsNumberLabel;

	// dataset Chart options
	private JPanel datasetMovingOptionsPanel;
	private JRadioButton datasetNoChartRadio;
	private JRadioButton datasetTimeBasedRadio;
	private JRadioButton datasetHistogramRadio;
	private JRadioButton datasetquantilesRadio;
	private JCheckBox datasetRawCheckBox;
	private JCheckBox datasetMovingMinCheckBox;
	private JCheckBox datasetMovingMaxCheckBox;
	private JCheckBox datasetMovingAverageCheckBox;
	private JCheckBox datasetMovingMedianCheckBox;
	private JCheckBox datasetMovingStdDevCheckBox;
	private JCheckBox datasetMovingThroughputCheckBox;
	private JTextField timeWindowTextField;
	private JTextField stepTextField;

	JButton redrawButton;
	JButton timeFilterApplyButton;
	JButton fieldValuesFilteringApplyButton;
	JButton addFilterInFilterValuesFilteringButton;

/////////////// Card names/////////////
	String REPORT_OPTIONS  = "Report Options";
	String SECTION_OPTIONS = "Section Options";
	String CHART_OPTIONS   = "Chart Options";
	String DATASET_OPTIONS = "Dataset Options";
	String STATISTICS      = "Statistics";

/////////////// menus listeners/////////////
	MenuListener menuFileListnener;
	MenuListener menuEditListnener;
	MenuListener menuHelpListnener;



	public void setselectedObject(Object currentObj, int depth) {
		if (currentObj == selectedObject){
			return;
		}
		String className = currentObj.getClass().getSimpleName();

		if (className.equals(REPORT)){
			selectReport((Report) currentObj, depth);
		}else if(className.equals(SECTION)){
			selectSection((Section) currentObj, depth);
		}else if (className.equals(DATASET)){
			selectDataset((Dataset) currentObj, depth);
		}
	}


	class ReportCommentListener implements DocumentListener {
		String newline = "\n";

		public void insertUpdate(DocumentEvent e) {
			Report report = rmView.getReport();
			if (null != report){
				String text = reportCommentTextArea.getText();
				report.setComment(text);
				LogAndDebug.trace("(e)  "+ text.length() + " characters. ("+text+")");
			}
		}
		public void removeUpdate(DocumentEvent e) {
			Report report = rmView.getReport();
			if (null != report){
				String text = reportCommentTextArea.getText();
				report.setComment(text);
				LogAndDebug.trace("(e)  "+ text.length() + " characters. ("+text+")");
			}
		}
		public void changedUpdate(DocumentEvent e) {
			LogAndDebug.trace("(e)");
			//Plain text components do not fire these events
		}
	}


	class SectionCommentListener implements DocumentListener {
		String newline = "\n";

		public void insertUpdate(DocumentEvent e) {
			Section section = rmView.getSelectedSection();
			if (null != section){
				String text = sectionCommentTextArea.getText();
				section.setComment(text);
				LogAndDebug.trace("(e)  "+ text.length() + " characters. ("+text+")");
			}
		}
		public void removeUpdate(DocumentEvent e) {
			Section section = rmView.getSelectedSection();
			if (null != section){
				String text = sectionCommentTextArea.getText();
				section.setComment(text);
				LogAndDebug.trace("(e)  "+ text.length() + " characters. ("+text+")");
			}
		}
		public void changedUpdate(DocumentEvent e) {
			LogAndDebug.trace("(e)");
			//Plain text components do not fire these events
		}
	}


	class DatasetCommentListener implements DocumentListener {
		String newline = "\n";

		public void insertUpdate(DocumentEvent e) {
			Dataset dataset = rmView.getSelectedDataset();
			if (null != dataset){
				String text = datasetCommentTextArea.getText();
				dataset.setComment(text);
				LogAndDebug.trace("(e)  "+ text.length() + " characters. ("+text+")");
			}
		}
		public void removeUpdate(DocumentEvent e) {
			Dataset dataset = rmView.getSelectedDataset();
			if (null != dataset){
				String text = datasetCommentTextArea.getText();
				dataset.setComment(text);
				LogAndDebug.trace("(e)  "+ text.length() + " characters. ("+text+")");
			}
		}
		public void changedUpdate(DocumentEvent e) {
			LogAndDebug.trace("(e)");
			//Plain text components do not fire these events
		}
	}
}
