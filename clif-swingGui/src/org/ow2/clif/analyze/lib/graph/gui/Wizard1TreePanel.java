/*
* CLIF is a Load Injection Framework
* Copyright (C) 2011 France Telecom R&D
* Copyright (C) 2016 Orange SA
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
* Contact: clif@ow2.org
*/

package org.ow2.clif.analyze.lib.graph.gui;

import java.awt.Component;
import java.util.ArrayList;
import java.util.SortedSet;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTree;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeCellRenderer;
import javax.swing.tree.TreePath;
import javax.swing.tree.TreeSelectionModel;
import org.ow2.clif.analyze.lib.graph.gui.DataAccess.W1SelectedDataset;
import org.ow2.clif.analyze.lib.report.Datasource;
import org.ow2.clif.analyze.lib.report.LogAndDebug;
import org.ow2.clif.analyze.lib.report.Dataset.DatasetType;
import org.ow2.clif.storage.api.BladeDescriptor;
import org.ow2.clif.storage.lib.util.NameBladeFilter;
import org.ow2.clif.supervisor.api.ClifException;

@SuppressWarnings("serial")
public class Wizard1TreePanel extends JScrollPane{

	JTree jTree1;
	Wizard1MainView w1MainView;
	DataAccess data; 
	JFrame frame;
	DefaultMutableTreeNode availableDatasetsRoot;
	JPanel jpTree;
	W1SelectedDataset availableDs ;
	DefaultTreeCellRenderer renderer;	
	
	public Wizard1TreePanel(Wizard1MainView _mainView) {
		super();
		w1MainView = _mainView;
		frame = w1MainView.frame;
		data = w1MainView.getDataAccess();
		data.loadIcons();
		refresh();
	}

	public void refresh()
	{
		availableDatasetsRoot = data.getTestsTreeNode();
		renderer = new MyTreeRenderer();
		jTree1 = new JTree(availableDatasetsRoot);
		jTree1.setCellRenderer(renderer);
		initTree();

		setViewportView(jTree1);
		
		jTree1.addTreeSelectionListener(new TreeSelectionListener() {
			@Override
			public void valueChanged(TreeSelectionEvent evt) {
				jTreeValueChanged(evt);
			}
		});
	}

	private void jTreeValueChanged(TreeSelectionEvent evt) {
//		LogAndDebug.tracep("()");
		jUpdateTree(evt);
		frame.repaint();
//		LogAndDebug.tracem("()");
	}

	
	private void jUpdateTree(TreeSelectionEvent evt) {
		TreeSelectionModel curentSelModel = jTree1.getSelectionModel();

		// modify the status bar
		LogAndDebug.log(evt.getPath().toString());
		
		w1MainView.clearCurrentDs();

		//  update from tree selection
		ArrayList<SortedSet<String>> eventTypesLists = new ArrayList<SortedSet<String>>();

		TreePath[] treePathArray = curentSelModel.getSelectionPaths();
		if (curentSelModel != null && treePathArray != null){
			boolean allLeaf = true;		// verify if all selections are bladeIds
			for (int i = 0; i < treePathArray.length; i++){
				if (3 != treePathArray[i].getPathCount()){
					allLeaf = false;
					break;
				}
			}
			if (!curentSelModel.isSelectionEmpty() && allLeaf){
				for (TreePath tp : treePathArray){
					String testName = tp.getPathComponent(1).toString(); 
					String bladeName = tp.getPathComponent(2).toString();
					
					SortedSet<String> eventTypes = data.getEventTypes(testName, bladeName);
					w1MainView.tableUpdate(testName, bladeName);
					Datasource dd = new Datasource(testName, bladeName);
					LogAndDebug.trace(" adding datasource to wizardDataset: " + testName +"-"+bladeName);
					data.wizardDataset.addDatasource(dd);
					eventTypesLists.add(eventTypes);
				}
				w1MainView.updateEvGroupCB(eventTypesLists);
				w1MainView.updateStatus();
				w1MainView.tableUpdate();
				return;
			}
		}
		w1MainView.jpEvent.removeAll();
		w1MainView.jbNext.setEnabled(false);	// should be done within updateTable() ?
		w1MainView.tableUpdate();
		w1MainView.updateStatus();
//		LogAndDebug.tracem("() -> no selection ?");
	}		//	jUpdateTree()


	
	class MyTreeRenderer extends DefaultTreeCellRenderer {		// called when resizing the window
		@Override
		public Component getTreeCellRendererComponent(JTree tree, Object value,
				boolean sel, boolean expanded, boolean leaf, int row,
				boolean hasFocus)
		{
			super.getTreeCellRendererComponent(tree, value, sel, expanded, leaf, row, hasFocus);
//			LogAndDebug.tracep("(... " + value + " ...)");

			DefaultMutableTreeNode node = (DefaultMutableTreeNode) value;
			// condition to change the leaf
			if (node.isLeaf() && node.getParent() != null) {
				String testName = node.getParent().toString();
				String bladeName = node.toString();
				NameBladeFilter filter = new NameBladeFilter(bladeName);
//				LogAndDebug.log("getTreeCellRendererComponent(..) selected bladeName: \""+bladeName+"\"");

				BladeDescriptor[] bladeDescriptorArray = null;
				try {
					bladeDescriptorArray = data.getBladeDescriptor(testName, filter);
				} catch (ClifException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
//				LogAndDebug.log("* bladeDescriptorArray= \""+bladeDescriptorArray.toString()+"\"");
				if (bladeDescriptorArray.length > 0){
					// get only one blade descriptobladeDescriptorArray.r
					String bladeClass = bladeDescriptorArray[0].getClassname();

					if (bladeClass.equalsIgnoreCase("IsacRunner")){
						setIcon(data.INJECTOR_ICON);
					} else {
						setIcon(data.PROBE_ICON);
					}
				}else{
					LogAndDebug.log("*** bladeDescriptorArray.length == 0 ");
				}
			}
//			LogAndDebug.tracem("(...)");
			return this;
		}
	}
	
	

	void updateTreeSelectionMode(DatasetType dsType) {
		switch (dsType) {
		case SIMPLE_DATASET:
			jTree1.getSelectionModel().setSelectionMode( 			// single selection possible
					TreeSelectionModel.SINGLE_TREE_SELECTION);
			break;
		case MULTIPLE_DATASET:
			jTree1.getSelectionModel().setSelectionMode( 			// multiple selection possible
					TreeSelectionModel.DISCONTIGUOUS_TREE_SELECTION);
			break;
		case AGGREGATE_DATASET:
			jTree1.getSelectionModel().setSelectionMode( 			// multiple selection possible
					TreeSelectionModel.DISCONTIGUOUS_TREE_SELECTION);
			break;
		default:
			LogAndDebug.log("WARNING TREE SELECTION");
		}
	}


	void initTree(){
//		LogAndDebug.tracep("()");
		TreeSelectionModel curentSelModel = jTree1.getSelectionModel();
		curentSelModel.clearSelection();
//		LogAndDebug.tracem("()");
	}
}