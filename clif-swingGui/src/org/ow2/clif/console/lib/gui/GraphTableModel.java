/*
* CLIF is a Load Injection Framework
* Copyright (C) 2003 France Telecom R&D
* Copyright (C) 2003 INRIA
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
* Contact: clif@ow2.org
*/
package org.ow2.clif.console.lib.gui;

import java.util.Vector;
import javax.swing.table.DefaultTableModel;

/**
 * @author Julien Buret
 * @author Nicolas Droze
 */
public class GraphTableModel extends DefaultTableModel {
	private static final long serialVersionUID = 9133182696460112168L;
	public int rows;
	private int id;
	private Vector<Object> injectors = new Vector<Object>();
	private Boolean value;

	public GraphTableModel(String[] cname) {
		super(cname, 0);
	}

	/**
	 * Sets an ID for this table model. (Used in the panel that represents two
	 * graphics in the same panel)
	 * @param id
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the identifier of this table model
	 */
	public int getId() {
		return id;
	}

	/**
	 * Adds an injector to this table model list.
	 * @param injectorName The name of the injector to add
	 */
	public void addInjector(String injectorName) {
		Object[] arg = new Object[3];
		arg[0] = new Boolean(false);
		arg[1] = new Boolean(true);
		arg[2] = injectorName;
		addRow(arg);
	}

	/**
	 * Necessary to display boolean value as a checkbox in the table.
	 * @see javax.swing.table.TableModel#getColumnClass(int)
	 */
	@Override
	public Class<? extends Object> getColumnClass(int c) {
		return getValueAt(0, c).getClass();
	}

	/**
	 * @return the names of every host in this table model.
	 */
	public Object[] getAllInjectors() {
		injectors.removeAllElements();
		rows = getRowCount();

		for (int i = 0; i < rows; i++) {
			injectors.add(getValueAt(i, 2));
		}

		return injectors.toArray();
	}

	/**
	 * @return the host names whose display checkbox is checked
	 */
	public Object[] getInjectorsToDisplay() {
		injectors.removeAllElements();
		rows = getRowCount();

		for (int i = 0; i < rows; i++) {
			value = (Boolean) getValueAt(i, 0);
			if (value.booleanValue())
				injectors.add(getValueAt(i, 2));
		}

		return injectors.toArray();
	}

	/**
	 * @return the host names whose collect checkbox is checked
	 */
	public Object[] getInjectorsToCollect() {
		injectors.removeAllElements();
		rows = getRowCount();

		for (int i = 0; i < rows; i++) {
			value = (Boolean) getValueAt(i, 1);
			if (value.booleanValue())
				injectors.add(getValueAt(i, 2));
		}

		return injectors.toArray();
	}

	/**
	 * Information about the editable cell of the table.
	 * @see javax.swing.table.TableModel#isCellEditable(int, int)
	 */
	@Override
	public boolean isCellEditable(int rowIndex, int columnIndex) {
		if (columnIndex == 2)
			return false;
		else
			return true;
	}

}