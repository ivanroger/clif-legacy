/*
* CLIF is a Load Injection Framework
* Copyright (C) 2004 France Telecom R&D
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
* Contact: clif@ow2.org
*/

package org.ow2.clif.console.lib.gui;

import org.ow2.clif.util.Version;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.Container;
import java.awt.BorderLayout;


/**
 * "About CLIF" information window - displays CLIF logo, version number and compilation time.
 * @author Bruno Dillenseger
 */
public class GuiAboutDialog extends JDialog implements ActionListener
{
	private static final long serialVersionUID = -7302160189198550591L;


	public GuiAboutDialog(JFrame frame)
	{
		super(frame, "About CLIF...", true);
		Container pane = getContentPane();
		pane.setLayout(new BorderLayout());
		pane.add(BorderLayout.CENTER, new JLabel(new ImageIcon(GuiAboutDialog.class.getClassLoader().getResource("icons/logo_clif_60px.gif"))));
		pane.add(BorderLayout.NORTH, new JLabel("CLIF is a Load Injection Framework"));
		pane.add(BorderLayout.SOUTH, new JLabel(Version.getVersion()));
		JButton okBtn = new JButton("OK");
		pane.add(BorderLayout.EAST, okBtn);
		okBtn.addActionListener(this);
		this.addWindowListener(new GuiAboutDialog.WindowCloser());
		pack();
	}


	@Override
	public void actionPerformed(ActionEvent e)
	{
		this.dispose();
	}


	class WindowCloser extends WindowAdapter
	{
		@Override
		public void windowClosing(WindowEvent e)
		{
			GuiAboutDialog.this.dispose();
		}
	}
}
