/*
* CLIF is a Load Injection Framework
* Copyright (C) 2003, 2004 France Telecom R&D
* Copyright (C) 2003 INRIA
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
* Contact: clif@ow2.org
*/

package org.ow2.clif.console.lib.gui;

import org.ow2.clif.supervisor.api.BladeState;
import org.ow2.clif.console.lib.ClifDeployDefinition;
import java.awt.BorderLayout;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.util.Map;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Arrays;
import java.math.BigInteger;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JButton;
import javax.swing.DefaultCellEditor;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;


/**
 * @author Julien Buret
 * @author Nicolas Droze
 * @author Bruno Dillenseger
 */
public class GuiPanelBladeState
	extends JPanel
	implements ActionListener, ListSelectionListener
{
	private static final long serialVersionUID = -3449711521182645002L;
	static protected final String ADD_CMD = "add";
	static protected final String REMOVE_CMD = "remove";
	static protected final String CLEAR_CMD = "clear";
	static protected final int ID_COL = 0;
	static protected final int SERVER_COL = 1;
	static protected final int ROLE_COL = 2;
	static protected final int CLASS_COL = 3;
	static protected final int ARGUMENT_COL = 4;
	static protected final int COMMENT_COL = 5;
	static protected final int STATE_COL = 6;
	static protected final String[] cname = {
		"Blade id",
		"Server",
		"Role",
		"Blade class",
		"Blade argument",
		"Comment",
		"State" };


	protected BigInteger nextBladeId = BigInteger.ZERO;
	protected JFrame frame;
	protected InjectorStateTableModel tModel = new InjectorStateTableModel(cname);
	protected JTable table = new JTable(tModel);
	protected List<String> servers = Arrays.asList(new String[0]);
	protected JButton addBtn = new JButton("Add a blade");
	protected JButton removeBtn = new JButton("Remove a blade");
	protected JButton clearBtn = new JButton("Remove all blades");
	protected JPanel buttonPnl = new JPanel();
	private boolean editable = true;


	/**
	 * The constructor with no initial hosts.
	 */
	public GuiPanelBladeState(JFrame frame)
	{
		this.frame = frame;
		setLayout(new BorderLayout());
		JScrollPane scrollPane = new JScrollPane(table);
		table.getSelectionModel().addListSelectionListener(this);

		TableColumn serverCol = table.getColumnModel().getColumn(ROLE_COL);
		serverCol.setCellEditor(
			new DefaultCellEditor(
				new JComboBox<String>(new String[] { "injector", "probe" } )));

		addBtn.setActionCommand(ADD_CMD);
		addBtn.addActionListener(this);
		buttonPnl.add(addBtn);

		removeBtn.setActionCommand(REMOVE_CMD);
		removeBtn.setEnabled(false);
		removeBtn.addActionListener(this);
		buttonPnl.add(removeBtn);

		clearBtn.setActionCommand(CLEAR_CMD);
		clearBtn.addActionListener(this);
		buttonPnl.add(clearBtn);

		add(BorderLayout.NORTH, buttonPnl);
		add(BorderLayout.CENTER, scrollPane);
		setAvailableServers(new String[0]);
	}


	public void setAvailableServers(String[] servers)
	{
		this.servers = Arrays.asList(servers);
		if (table.isEditing())
		{
			table.getCellEditor(table.getEditingRow(), table.getEditingColumn()).stopCellEditing();
		}
		for (int i = 0 ; i < tModel.getRowCount() ; ++i)
		{
			if (! this.servers.contains(tModel.getValueAt(i, SERVER_COL)))
			{
				tModel.setValueAt("", i, SERVER_COL);
			}
		}
		TableColumn serverCol = table.getColumnModel().getColumn(SERVER_COL);
		serverCol.setCellEditor(new DefaultCellEditor(new JComboBox<String>(servers)));
	}


	/**
	 * Adds a blade at the bottom of the table
	 * @param id The blade identifier
	 * @param def the deployment definition, or null if no deployment is defined
	 */
	private void addBlade(String id, ClifDeployDefinition def)
	{
		insertBlade(id, def, -1);
	}


	/**
	 * Inserts a blade at the specified row index of the table.
	 * @param id the blade identifier
	 * @param def the deployment definition, or null if no deployment is defined
	 * @param row the insertion index in the table for the new row. A negative value
	 * results in adding the new row at the bottom of the table.
	 */
	protected void insertBlade(String id, ClifDeployDefinition def, int row)
	{
		Object[] values = new Object[GuiPanelBladeState.cname.length];
		if (tModel.getBladeRow(id) == -1)
		{
			values[ID_COL] = id;
		}
		else
		{
			while (tModel.getBladeRow(nextBladeId.toString()) != -1)
			{
				nextBladeId = nextBladeId.add(BigInteger.ONE);
			}
			values[ID_COL] = nextBladeId.toString();
			nextBladeId = nextBladeId.add(BigInteger.ONE);
		}
		values[STATE_COL] = BladeState.UNDEPLOYED;
		values[SERVER_COL] = "";
		if (def != null)
		{
			if (def.isProbe())
			{
				String probeName = def.getContext().get("insert");
				probeName = probeName.substring(0, probeName.lastIndexOf('.'));
				values[CLASS_COL] = probeName.substring(1 + probeName.lastIndexOf('.'));
				values[ROLE_COL] = "probe";
			}
			else
			{
				values[CLASS_COL] = def.getContext().get("insert");
				values[ROLE_COL] = "injector";
			}
			values[ARGUMENT_COL] = (def.getArgument() == null ? "" : def.getArgument());
			values[COMMENT_COL] = (def.getComment() == null ? "" : def.getComment());
			if (servers.contains(def.getServerName()))
			{
				values[SERVER_COL] = def.getServerName();
			}
		}
		else
		{
			values[CLASS_COL] = "";
			values[ARGUMENT_COL] = "";
			values[COMMENT_COL] = "";
			values[ROLE_COL] = "";
		}
		if (row < 0)
		{
			tModel.addRow(values);
		}
		else
		{
			tModel.insertRow(row, values);
		}
	}


	public void setBladeState(String bladeId, BladeState state)
	{
		tModel.setBladeValue(bladeId, state, STATE_COL);
	}


	public void setTestPlan(Map<String,ClifDeployDefinition> testPlan)
	{
		clear();
		Iterator<Map.Entry<String,ClifDeployDefinition>> iter = testPlan.entrySet().iterator();
		while (iter.hasNext())
		{
			Map.Entry<String,ClifDeployDefinition> entry = iter.next();
			addBlade(entry.getKey(), entry.getValue());
		}
	}


	public Map<String,ClifDeployDefinition> getTestPlan()
	{
		Map<String,ClifDeployDefinition> testPlan = new HashMap<String,ClifDeployDefinition>();
		for (int i = 0 ; i < tModel.getRowCount() && testPlan != null ; ++i)
		{
			if (((String)tModel.getValueAt(i, SERVER_COL)).trim().equals(""))
			{
				testPlan = null;
				table.setEditingRow(i);
				table.editCellAt(i, SERVER_COL);
			}
			else if (((String)tModel.getValueAt(i, CLASS_COL)).trim().equals(""))
			{
				testPlan = null;
				table.setEditingRow(i);
				table.editCellAt(i, CLASS_COL);
			}
			else if (((String)tModel.getValueAt(i, ID_COL)).trim().equals(""))
			{
				testPlan = null;
				table.setEditingRow(i);
				table.editCellAt(i, ID_COL);
			}
			else
			{
				Map<String,String> context = new HashMap<String,String>();
				boolean isProbe = tModel.getValueAt(i, ROLE_COL).equals("probe");
				if (isProbe)
				{
					context.put(
						"insert",
						"org.ow2.clif.probe."
						+ tModel.getValueAt(i, CLASS_COL)
						+ ".Insert");
					context.put(
						"datacollector",
						"org.ow2.clif.probe."
						+ tModel.getValueAt(i, CLASS_COL)
						+ ".DataCollector");
				}
				else
				{
					context.put(
						"insert",
						tModel.getValueAt(i, CLASS_COL).toString());
					context.put(
						"datacollector",
						"org.ow2.clif.datacollector.lib.InjectorDataCollector");
				}
				testPlan.put(
					(String)tModel.getValueAt(i, ID_COL),
					new ClifDeployDefinition(
						(String)tModel.getValueAt(i, SERVER_COL),
						"org.ow2.clif.server.lib.Blade",
						context,
						(String)tModel.getValueAt(i, ARGUMENT_COL),
						(String)tModel.getValueAt(i, COMMENT_COL),
						isProbe));
			}
		}
		return testPlan == null || testPlan.size() == 0 ? null : testPlan;
	}


	public boolean isDeployable()
	{
		int n = tModel.getRowCount();
		boolean answer = n != 0;
		while (answer && n-- > 0)
		{
			if (((String)tModel.getValueAt(n, SERVER_COL)).trim().equals("")
				|| ((String)tModel.getValueAt(n, CLASS_COL)).trim().equals("")
				|| ((String)tModel.getValueAt(n, ID_COL)).trim().equals(""))
			{
				answer = false;
			}
		}
		return answer;
	}


	public boolean isEmpty()
	{
		return tModel.getRowCount() == 0;
	}


	/**
	 * Removes all lines from the table
	 */
	protected void clear()
	{
		if (table.isEditing())
		{
			table.getCellEditor(table.getEditingRow(), table.getEditingColumn()).stopCellEditing();
		}
		table.clearSelection();
		table.removeAll();
		int n = tModel.getRowCount();
		while (n-- > 0)
		{
			tModel.removeRow(n);
		}
	}


	/**
	 * Enables or disable testplan edition
	 */
	public void setEditable(boolean enabled)
	{
		editable = enabled;
		addBtn.setEnabled(enabled);
		removeBtn.setEnabled(enabled && table.getSelectedRow() != -1 );
		clearBtn.setEnabled(enabled);
		tModel.setEditable(enabled);
	}


	//////////////////////////////
	// interface ActionListener //
	//////////////////////////////


	@Override
	public void actionPerformed(ActionEvent evt)
	{
		if (evt.getActionCommand().equals(ADD_CMD))
		{
			insertBlade(nextBladeId.toString(), null, table.getSelectedRow());
			nextBladeId = nextBladeId.add(BigInteger.ONE);
		}
		else if (evt.getActionCommand().equals(REMOVE_CMD))
		{
			tModel.removeRow(table.getSelectedRow());
		}
		else if (evt.getActionCommand().equals(CLEAR_CMD))
		{
			if (new GuiClearConfirm(frame).ask())
			{
				clear();
			}
		}
	}


	/////////////////////////////////////
	// interface ListSelectionListener //
	/////////////////////////////////////


	@Override
	public synchronized void valueChanged(ListSelectionEvent evt)
	{
		if (table.getSelectedRow() == -1)
		{
			removeBtn.setEnabled(false);
		}
		else
		{
			removeBtn.setEnabled(editable);
		}
	}
}


/**
 * This class perform operations upon the injector state panel,
 * like retrieving a given type of host,...
 */
class InjectorStateTableModel extends DefaultTableModel
{
	private static final long serialVersionUID = -4727123547120277673L;

	boolean editable = true;


	public InjectorStateTableModel(String[] cname)
	{
		super(cname, 0);
	}


	public void setBladeValue(String bladeId, Object value, int col)
	{
		setValueAt(value, getBladeRow(bladeId), col);
	}


	int getBladeRow(String bladeId)
	{
		for (int i = 0 ; i < getRowCount() ; ++i)
		{
			if(getValueAt(i, GuiPanelBladeState.ID_COL).equals(bladeId))
			{
				return i;
			}
		}
		return -1;
	}


	public void setEditable(boolean editable)
	{
		this.editable = editable;
	}


	/**
	 * Return true if the specified cell is editable.
	 * @see javax.swing.table.TableModel#isCellEditable(int, int)
	 */
	@Override
	public boolean isCellEditable(int rowIndex, int columnIndex)
	{
		if (columnIndex == GuiPanelBladeState.STATE_COL)
		{
			return false;
		}
		else
		{
			return editable;
		}
	}
}