/*
* CLIF is a Load Injection Framework
* Copyright (C) 2005 France Telecom R&D
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
* Contact: clif@ow2.org
*/
package org.ow2.clif.console.lib.gui;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JProgressBar;
import org.ow2.clif.deploy.ClifAppFacade;
import org.ow2.clif.storage.api.CollectListener;

/**
 * Monitor window for test results collect.
 * 
 * @author Bruno Dillenseger
 */
public class GuiCollectDialog extends JDialog implements ActionListener, CollectListener, Runnable
{
	private static final long serialVersionUID = -4846825162387287365L;
	protected JButton button;
 	protected JProgressBar bar;
 	protected JLabel comment;
 	protected String value = null;
 	protected boolean completed = false;
 	protected boolean canceled = false;
	protected long fullsize = -1;
	protected long bladesize = -1;
	protected long progress = -1;
	protected ClifAppFacade clifApp;
	protected Thread collectThr;


	GuiCollectDialog(JFrame frame, ClifAppFacade clifApp)
	{
		super(frame, "Test results collect", true);
		this.clifApp = clifApp;
		Container pane = getContentPane();
		pane.setLayout(new BorderLayout());
		pane.add(BorderLayout.NORTH, bar = new JProgressBar());
		bar.setMinimum(0);
		bar.setMaximum(100);
		bar.setValue(0);
		pane.add(BorderLayout.CENTER, comment = new JLabel("Waiting for data collect to start..."));
		pane.add(BorderLayout.SOUTH, button = new JButton("cancel"));
		button.addActionListener(this);
		addWindowListener(new WindowCloser());
	}


	void go()
	{
		collectThr = new Thread(this);
		collectThr.start();
		pack();
		setVisible(true);
	}


	////////////////////////
	// Runnable interface //
	////////////////////////
	
	
	@Override
	public void run()
	{
		clifApp.collect(null, this);
	}
	
	//////////////////////////////
	// ActionListener interface //
	//////////////////////////////


	@Override
	public void actionPerformed(ActionEvent e)
	{
		if (e.getSource() == button)
		{
			if (! completed)
			{
				canceled = true;
			}
			dispose();
		}
	}


	///////////////////////////////
	// CollectListener interface //
	///////////////////////////////


	@Override
	public void collectStart(String testId, long size)
	{
		setTitle("Collecting test results for test " + testId);
		fullsize = size;
		progress = 0;
		bladesize = 0;
		comment.setText("starting collecting " + size + " bytes for test " + testId);
		pack();
	}


	@Override
	public void bladeCollectStart(String bladeId, long size)
	{
		progress += bladesize;
		bladesize = size;
		comment.setText("starting collecting " + size + " bytes from blade " + bladeId);
	}


	@Override
	public void progress(String bladeId, long done)
	{
		comment.setText(
			"progress: " + done + "/" + bladesize + " for blade " + bladeId
			+ ", " + (progress + done) + "/" + fullsize + " for full collect");
		bar.setValue((int)((100 * progress + done) / fullsize));
	}


	@Override
	public void done()
	{
		progress += bladesize;
		comment.setText("done " + progress + "/" + fullsize);
		bar.setValue((int)((100 * progress) / fullsize));
		completed = true;
		button.setText("close");
	}


	@Override
	public boolean isCanceled()
	{
		return canceled;
	}


	@Override
	public boolean isCanceled(String bladeId)
	{
		return false;
	}


	/**
	 * Inner class for treating window closing as collect cancelation
	 */
	class WindowCloser extends WindowAdapter
	{
		@Override
		public void windowClosing(WindowEvent e)
		{
			canceled = true;
			GuiCollectDialog.this.dispose();
		}
	}
}
