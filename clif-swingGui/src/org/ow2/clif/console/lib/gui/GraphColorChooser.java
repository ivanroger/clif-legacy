/*
* CLIF is a Load Injection Framework
* Copyright (C) 2003,2004 France Telecom R&D
* Copyright (C) 2003 INRIA
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
* Contact: clif@ow2.org
*/

package org.ow2.clif.console.lib.gui;

import java.awt.Color;
import java.util.Vector;

/**
 * This class handles colors for the graphics. It provide a new color available
 * within a defined list of colors.
 * @author Julien Buret
 * @author Nicolas Droze
 * @author Bruno Dillenseger
 */
public class GraphColorChooser
{
	private Vector<Color> colors = new Vector<Color>();
	private int currentColor = -1;


	public GraphColorChooser()
	{
		initColors();
	}

	/**
	 * Initialize a list of available colors.
	 */
	private void initColors()
	{
		colors.add(Color.BLUE);
		colors.add(Color.RED);
		colors.add(Color.GREEN);
		colors.add(Color.CYAN);
		colors.add(Color.MAGENTA);
		colors.add(Color.PINK);
		colors.add(Color.ORANGE);
		colors.add(Color.YELLOW);
		colors.add(Color.GRAY);
		colors.add(Color.DARK_GRAY);
		colors.add(Color.LIGHT_GRAY);
	}

	/**
	 * Return the next color available.
	 * @return the next available color. Once all colors have been used,
	 * it loops on the same color sequence.
	 */
	public Color getNextColor() {
		currentColor++;
		if (currentColor == colors.size())
			currentColor = 0;
		return colors.elementAt(currentColor);
	}
}