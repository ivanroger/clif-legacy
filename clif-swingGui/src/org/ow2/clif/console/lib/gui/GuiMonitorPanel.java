/*
* CLIF is a Load Injection Framework
* Copyright (C) 2004 France Telecom R&D
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
* Contact: clif@ow2.org
*/

package org.ow2.clif.console.lib.gui;


import java.awt.event.ContainerEvent;
import java.awt.event.ContainerListener;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import javax.swing.JTabbedPane;
import org.ow2.clif.console.lib.ClifDeployDefinition;
import org.ow2.clif.deploy.ClifAppFacade;


/**
 *
 * @author Bruno Dillenseger
 */
public class GuiMonitorPanel
	extends JTabbedPane
	implements ContainerListener
{
	private static final long serialVersionUID = 5471570251390652582L;
	
	static private final String INJECTOR_LABEL = "injector";


	/**
     * Constructor
	 * @param testPlan
	 * @param clifApp
	 */
	public GuiMonitorPanel(Map<String,ClifDeployDefinition> testPlan, ClifAppFacade clifApp)
	{
		super();
		Map<String,Map<String,ClifDeployDefinition>> bladeKinds = new HashMap<String,Map<String,ClifDeployDefinition>>();
		Iterator<Map.Entry<String,ClifDeployDefinition>> tpIter = testPlan.entrySet().iterator();
		while (tpIter.hasNext())
		{
			Map.Entry<String,ClifDeployDefinition> entry = tpIter.next();
			ClifDeployDefinition def = entry.getValue();
			String key;
			if (def.isProbe())
			{
				key = def.getContext().get("insert");
				key = key.substring(0, key.lastIndexOf('.'));
				key = key.substring(1 + key.lastIndexOf('.'));
			}
			else
			{
				key = INJECTOR_LABEL;
			}
			Map<String,ClifDeployDefinition> blades = bladeKinds.get(key);
			if (blades == null)
			{
				blades = new HashMap<String,ClifDeployDefinition>();
				bladeKinds.put(key, blades);
			}
			blades.put(entry.getKey(), entry.getValue());
		}
		Iterator<Map.Entry<String,Map<String,ClifDeployDefinition>>> bkIter = bladeKinds.entrySet().iterator();
		while (bkIter.hasNext())
		{
			Map.Entry<String,Map<String,ClifDeployDefinition>> entry = bkIter.next();
			String arbitraryBladeId = entry.getValue().keySet().iterator().next();
			GuiMonitorCard monitorCard = new GuiMonitorCard(
				entry.getValue(),
				clifApp.getStatLabels(arbitraryBladeId),
				clifApp);
			this.addContainerListener(monitorCard);
			add(monitorCard, entry.getKey());
		}
	}


	/////////////////////////////////
	// interface ContainerListener //
	/////////////////////////////////


	@Override
	public void componentAdded(ContainerEvent ev)
	{
	}


	@Override
	public void componentRemoved(ContainerEvent ev)
	{
		removeAll();
	}
}
