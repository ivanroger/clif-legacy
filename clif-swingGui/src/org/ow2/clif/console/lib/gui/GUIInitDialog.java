/*
* CLIF is a Load Injection Framework
* Copyright (C) 2003 France Telecom R&D
* Copyright (C) 2003 INRIA
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
* Contact: clif@ow2.org
*/

package org.ow2.clif.console.lib.gui;

import javax.swing.*;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.*;


/**
 *
 * @author Bruno Dillenseger
 */
public class GUIInitDialog extends JDialog implements ActionListener
{
	private static final long serialVersionUID = -2950660154256210111L;
	static protected int last_id = 0;
 	protected JButton okBtn;
 	protected JTextField idFld;
 	protected String value = null;


	GUIInitDialog(JFrame frame)
	{
		super(frame, "Please enter test unique identifier", true);
		Container pane = getContentPane();
		pane.setLayout(new BorderLayout());
		pane.add(BorderLayout.NORTH, new JLabel("new test id:"));
		pane.add(BorderLayout.CENTER, idFld = new JTextField("test#" + last_id, 20));
		pane.add(BorderLayout.SOUTH, okBtn = new JButton("OK"));
		okBtn.addActionListener(this);
		this.addWindowListener(new WindowCloser());
	}


	public String ask()
	{
		pack();
		setVisible(true);
		return value;
	}


	@Override
	public void actionPerformed(ActionEvent e)
	{
		if (e.getSource() == okBtn)
		{
			++last_id;
			value = idFld.getText();
		}
		this.dispose();
	}

	class WindowCloser extends WindowAdapter
	{
		@Override
		public void windowClosing(WindowEvent e)
		{
			GUIInitDialog.this.dispose();
		}
	}
}
