/*
* CLIF is a Load Injection Framework
* Copyright (C) 2003, 2004 France Telecom R&D
* Copyright (C) 2003 INRIA
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
* Contact: clif@ow2.org
*/
package org.ow2.clif.console.lib.gui;

import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import org.ow2.clif.deploy.ClifAppFacade;

/**
 * This is the action invoked to retrieve a ActionStat.
 * The method actionPerformed of this class is invoked by the timer each
 * interval of time.
 * @author Julien Buret
 * @author Nicolas Droze
 */
final class ActionTestReport extends AbstractAction {
	private static final long serialVersionUID = -1297534481209922177L;

	private Object[] injectors;
	private GraphTableModel tModel = null;
	private Graph graph;
	private int time;
	private int totalTime = 0;
	private long lastEvent = 0;
	private long diff;
	private ClifAppFacade clifApp;

	/**
	 * @param suis
	 * @param graph
	 * @param tModel
	 */
	public ActionTestReport(
		ClifAppFacade clifApp,
		Graph graph,
		GraphTableModel tModel) {
		this.clifApp = clifApp;
		this.graph = graph;
		this.tModel = tModel;
	}


	/**
	 * Set the interval of time between each ActionStat
	 * @param time
	 */
	public void setTime(int time) {
		this.time = time;
	}

	public void reset() {
		totalTime = 0;
		lastEvent = 0;
	}

	/**
	 * Get the statistics from the blades and update the graph
	 */
	@Override
	public void actionPerformed(ActionEvent e)
	{
		if (lastEvent != 0) {
			diff =
				new Long((System.currentTimeMillis() - lastEvent) / 1000)
					.longValue();
			if (diff > time) {
				// The refresh interval has changed before the end of timer period
				// So we have to update the difference of time on the graph
				totalTime += (diff - time);
			}
		}

		totalTime += time;
		// This line update the X Axis. It is necessary if no data is collected.
		graph.updateXAxis(totalTime);

		// Get all injectors to collect data from
		injectors = tModel.getInjectorsToCollect();

		for (int i = 0; i < injectors.length; i++)
		{
			long[] stats = clifApp.getStats((String) injectors[i]);
			if (stats != null)
			{
				for (int j = 0 ; j < stats.length ; ++j)
				{
					graph.addPoint(
						(String) injectors[i],
						j,
						totalTime,
						stats[j]);
				}
			}
		}
		lastEvent = System.currentTimeMillis();

		// Remove the graph and redraw the injectors to display
		injectors = tModel.getAllInjectors();
		for (int i = 0; i < injectors.length; i++) {
			graph.removePointsFromDisplay((String) injectors[i]);
		}

		injectors = tModel.getInjectorsToDisplay();
		for (int i = 0; i < injectors.length; i++) {
			graph.addPointsOnDisplay((String) injectors[i]);
		}

		graph.updateGraph();
	}
}