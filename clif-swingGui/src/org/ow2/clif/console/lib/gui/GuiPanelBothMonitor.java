/*
* CLIF is a Load Injection Framework
* Copyright (C) 2003, 2004 France Telecom R&D
* Copyright (C) 2003 INRIA
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
* Contact: clif@ow2.org
*/
package org.ow2.clif.console.lib.gui;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.ContainerEvent;
import java.awt.event.ContainerListener;
import java.util.Iterator;
import java.util.Map;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.Timer;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import org.ow2.clif.console.lib.ClifDeployDefinition;
import org.ow2.clif.datacollector.lib.InjectorDataCollector;
import org.ow2.clif.deploy.ClifAppFacade;

/**
 * @author Julien Buret
 * @author Nicolas Droze
 * @author Bruno Dillenseger
 */
public class GuiPanelBothMonitor
	extends JPanel
	implements ActionListener, ComponentListener, ContainerListener, TableModelListener
{
	private static final long serialVersionUID = 2186674914675171760L;

	private JTextField intervalField = new JTextField("1", 4);
	private JTextField timeframeField = new JTextField(4);

	private JButton refreshButton = new JButton("Set/Draw");
	private JButton stopButton = new JButton("Stop");
	private JButton resetButton = new JButton("Reset");

	private JPanel panelInjectors = new JPanel();
	private JPanel panelHosts = new JPanel();
	private JPanel panelGraph = new JPanel();
	private JPanel panelControl = new JPanel();
	private JPanel panelTop = new JPanel();
	private JPanel panelMain = new JPanel();

	private JComboBox<String> viewComboInjectors = null;

	private ActionTestReport updateTestReport = null;
	private Timer testReportTimer = null;

	private String[] cname = { "Display", "Collect", "Blade" };

	private GraphTableModel tModelInjectors = new GraphTableModel(cname);
	private JTable tableInjectors = new JTable(tModelInjectors);

	private JScrollPane scrollPaneInjectors = new JScrollPane(tableInjectors);

	private Graph graphTestReport = null;

	private GridBagLayout gb = new GridBagLayout();
	private GridBagConstraints c = new GridBagConstraints();

	private JSplitPane splitPane = null;

	private GraphCellRenderer cellRendererInjector;

	private int lastNbPoints;
	private int nbPoints;
	private int interval;

	/**
	 * The constructor that will initialize the internal panels.
	 * @param testPlan 
	 * @param clifApp 
	 */
	public GuiPanelBothMonitor(Map<String,ClifDeployDefinition> testPlan, ClifAppFacade clifApp)
	{
		tModelInjectors.setId(1);

		tModelInjectors.addTableModelListener(this);
		this.setBackground(Color.white);
		this.setLayout(new GridLayout(1, 1));
		String arbitraryBladeId = (String)testPlan.keySet().iterator().next();
		graphTestReport = new Graph(new String[0], InjectorDataCollector.LABELS.length, true);
		createInjectorsPanel(clifApp, arbitraryBladeId);
		createHostsPanel();
		createGraphPanel();
		createTopPanel();
		createControlPanel();
		createMainPanel();

		Iterator<String> iter = testPlan.keySet().iterator();
		while (iter.hasNext())
		{
			String name = iter.next();
			graphTestReport.addInjector(name);
			cellRendererInjector =
				new GraphCellRenderer(graphTestReport.allHosts);
			tableInjectors.getColumnModel().getColumn(2).setCellRenderer(
				cellRendererInjector);
			tModelInjectors.addInjector(name);
		}
		add(panelMain);
		addComponentListener(this);
		// Initialize the action that will update the report
		updateTestReport =
			new ActionTestReport(clifApp, graphTestReport, tModelInjectors);
	}

	/**
	 * Create the panel that represents the list of all the injectors,
	 * with the associated combo box.
	 * @param clifApp 
	 * @param bladeId 
	 */
	private void createInjectorsPanel(ClifAppFacade clifApp, String bladeId)
	{
		String[] comboElements = clifApp.getStatLabels(bladeId);
		viewComboInjectors = new JComboBox<String>(comboElements);
		viewComboInjectors.setSelectedIndex(0);
		graphTestReport.setView(viewComboInjectors.getSelectedIndex());

		panelInjectors.setLayout(gb);

		c.gridx = 0;
		c.gridy = 0;
		c.gridheight = 1;
		c.gridwidth = 1;
		c.weightx = 100;
		c.weighty = 100;
		c.fill = GridBagConstraints.BOTH;
		c.anchor = GridBagConstraints.NORTH;

		scrollPaneInjectors.setSize(100, 50);
		panelInjectors.add(scrollPaneInjectors, c);

		c.gridx = 0;
		c.gridy = 1;
		c.gridheight = 1;
		c.gridwidth = 1;
		c.weightx = 100;
		c.weighty = 0;
		c.fill = GridBagConstraints.NONE;
		c.anchor = GridBagConstraints.WEST;

		viewComboInjectors.addActionListener(this);
		panelInjectors.add(viewComboInjectors, c);
	}


	/**
	 * Create an intermediate panel which contains both panels: monitor panel
	 * and injector panel.
	 */
	private void createHostsPanel() {
		panelHosts.setLayout(gb);

		c.gridx = 0;
		c.gridy = 0;
		c.gridheight = 1;
		c.gridwidth = 1;
		c.weightx = 100;
		c.weighty = 100;
		c.fill = GridBagConstraints.BOTH;
		c.anchor = GridBagConstraints.NORTH;
		panelHosts.add(panelInjectors, c);
	}

	/**
	 * Create the control panel which contains the control buttons for the graph.
	 */
	private void createControlPanel() {
		panelControl.setLayout(gb);

		// common constraints for every element
		c.gridheight = 1;
		c.gridwidth = 1;
		c.weightx = 100;
		c.weighty = 100;
		c.gridy = 0;

		// drawing timeframe setting
		c.gridx = 0;
		c.fill = GridBagConstraints.NONE;
		c.anchor = GridBagConstraints.EAST;
		panelControl.add(new JLabel("Drawing timeframe:"), c);

		c.gridx = 1;
		c.fill = GridBagConstraints.HORIZONTAL;
		panelControl.add(timeframeField, c);

		c.gridx = 2;
		c.fill = GridBagConstraints.NONE;
		c.anchor = GridBagConstraints.WEST;
		panelControl.add(new JLabel("sec."), c);

		// polling period setting
		c.gridx = 3;
		c.fill = GridBagConstraints.NONE;
		c.anchor = GridBagConstraints.EAST;
		panelControl.add(new JLabel("Polling period:"), c);

		c.gridx = 4;
		c.fill = GridBagConstraints.HORIZONTAL;
		panelControl.add(intervalField, c);

		c.gridx = 5;
		c.fill = GridBagConstraints.NONE;
		c.anchor = GridBagConstraints.WEST;
		panelControl.add(new JLabel("sec."), c);

		// all buttons are centered, and should not be resized
		c.fill = GridBagConstraints.NONE;
		c.anchor = GridBagConstraints.CENTER;

		// set/draw button
		c.gridx = 6;
		refreshButton.setActionCommand("REFRESH");
		refreshButton.addActionListener(this);
		panelControl.add(refreshButton, c);

		// stop button
		c.gridx = 7;
		stopButton.setActionCommand("STOP");
		stopButton.addActionListener(this);
		stopButton.setEnabled(false);
		panelControl.add(stopButton, c);

		// reset button
		c.gridx = 8;
		resetButton.setActionCommand("RESET");
		resetButton.addActionListener(this);
		panelControl.add(resetButton, c);
	}

	/**
	 * Create an intermediate panel which contains the host panel and the graphs panel.
	 */
	private void createTopPanel() {
		panelTop.setLayout(gb);

		splitPane =
			new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, panelHosts, panelGraph);
		splitPane.setDividerLocation(panelHosts.getMinimumSize().width);
		splitPane.setOneTouchExpandable(true);

		c.gridx = 0;
		c.gridy = 0;
		c.gridheight = 1;
		c.gridwidth = 1;
		c.weightx = 100;
		c.weighty = 100;
		c.fill = GridBagConstraints.BOTH;
		c.anchor = GridBagConstraints.NORTH;
		panelTop.add(splitPane, c);
	}

	/**
	 * Create an intermediate panel which contains the injector graph
	 * and the monitor graph.
	 */
	private void createGraphPanel() {
		panelGraph.setLayout(gb);

		c.gridx = 0;
		c.gridy = 0;
		c.gridheight = 1;
		c.gridwidth = 1;
		c.weightx = 100;
		c.weighty = 100;
		c.fill = GridBagConstraints.BOTH;
		c.anchor = GridBagConstraints.NORTH;
		panelGraph.add(graphTestReport, c);
	}

	/**
	 * Create the main panel which is the assembly of all the intermediate panels.
	 */
	private void createMainPanel() {
		panelMain.setLayout(gb);

		c.gridx = 0;
		c.gridy = 0;
		c.gridheight = 1;
		c.gridwidth = 1;
		c.weightx = 100;
		c.weighty = 100;
		c.fill = GridBagConstraints.BOTH;
		c.anchor = GridBagConstraints.NORTH;
		panelMain.add(panelTop, c);

		c.gridx = 0;
		c.gridy = 1;
		c.gridheight = 1;
		c.gridwidth = 1;
		c.weightx = 100;
		c.weighty = 0;
		c.fill = GridBagConstraints.HORIZONTAL;
		c.anchor = GridBagConstraints.CENTER;
		panelMain.add(panelControl, c);
	}


	/////////////////////////////////
	// interface ComponentListener //
	/////////////////////////////////


	@Override
	public void componentHidden(ComponentEvent e)
	{
	}


	@Override
	public void componentMoved(ComponentEvent e)
	{
	}


	@Override
	public void componentResized(ComponentEvent e)
	{
		if (timeframeField.getText().length() == 0)
		{
			timeframeField.setText(String.valueOf(graphTestReport.getGraphAreaWidth()));
		}
	}


	@Override
	public void componentShown(ComponentEvent e)
	{
	}


	/////////////////////////////////
	// interface ContainerListener //
	/////////////////////////////////


	@Override
	public void componentAdded(ContainerEvent ev)
	{
	}


	@Override
	public void componentRemoved(ContainerEvent ev)
	{
		if (ev.getChild() == this && testReportTimer != null)
		{
			testReportTimer.removeActionListener(updateTestReport);
			testReportTimer.stop();
		}
	}


	//////////////////////////////
	// interface ActionListener //
	//////////////////////////////

	/**
	 * Method invoked when the user clicks a button.
	 * The action to perform is dispatched according to the button command.
	 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
	 */
	@Override
	public void actionPerformed(ActionEvent e)
	{
		Object source = e.getSource();
		if (source == this.refreshButton) // Refresh button
		{
			resetButton.setEnabled(false);
			try
			{
				interval = Integer.parseInt(intervalField.getText());
				nbPoints = 1 + Integer.parseInt(timeframeField.getText()) / interval;
				if (nbPoints != lastNbPoints)
				{
					lastNbPoints = nbPoints;
					graphTestReport.setNbPoints(lastNbPoints);
				}
				// Launch a thread that will get the report of all injectors every "interval" seconds
				updateTestReport.setTime(interval);
				if (testReportTimer == null)
				{
					testReportTimer = new Timer(interval * 1000, updateTestReport);
				}
				else
				{
					testReportTimer.stop();
					testReportTimer.setDelay(interval * 1000);
				}
				testReportTimer.start();
				stopButton.setEnabled(true);
			}
			catch (Exception ex)
			{
				System.err.println("unexpected non-integer values in period fields");
			}
		}
	 	else if (source == this.stopButton) // Stop button
	 	{
			if (testReportTimer != null)
			{
				testReportTimer.stop();
			}
			stopButton.setEnabled(false);
			resetButton.setEnabled(true);
		}
		else if (source == this.resetButton) // Reset button
		{
			updateTestReport.reset();
			graphTestReport.setView(viewComboInjectors.getSelectedIndex());
			graphTestReport.clear();
		}
		else if (source == this.viewComboInjectors) // Blade list
		{
			// Inform the graph that the view has changed
			graphTestReport.setView(viewComboInjectors.getSelectedIndex());
			graphTestReport.removeAllPointsFromDisplay(
				tModelInjectors.getInjectorsToDisplay());
			graphTestReport.addAllPointsOnDisplay(
				tModelInjectors.getInjectorsToDisplay());
			graphTestReport.updateGraph();
		}
	}


	//////////////////////////////////
	// interface TableModelListener //
	//////////////////////////////////


	/**
	 * This method is called each time an event occured in the injector table
	 * It is necessary for an immediate graphics update when checking/unchecking display box.
	 * @see javax.swing.event.TableModelListener#tableChanged(javax.swing.event.TableModelEvent)
	 */
	@Override
	public void tableChanged(TableModelEvent e) {

		// We handle only the UPDATE event, ie the checkbox state has changed
		if (e.getType() == TableModelEvent.UPDATE) {
			int row = e.getFirstRow();
			int column = e.getColumn();
			Boolean data;

			// Injectors table
			if (((GraphTableModel) e.getSource()).getId() == 1) {
				// We get the new value of the checkbox
				data = (Boolean) tModelInjectors.getValueAt(row, column);
				// Display column
				if (column == 0) {
					/* Depending on the checkbox value, we inform the graph to
					 * display/remove the graph of the injector
					 */
					if (data.booleanValue()) {
						graphTestReport.addPointsOnDisplay(
							(String) tModelInjectors.getValueAt(
								row,
								column + 2));
						graphTestReport.updateGraph();
					} else {
						graphTestReport.removePointsFromDisplay(
							(String) tModelInjectors.getValueAt(
								row,
								column + 2));
						graphTestReport.updateGraph();
					}
				}
			}
		}
	}
}