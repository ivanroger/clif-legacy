/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2004, 2008 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * Contact: clif@ow2.org
 */
package org.ow2.clif.maven.plugin;

import org.apache.maven.plugin.MojoExecutionException;

/**
 * Deploy a testplan.<p/>
 *
 * This command does not return.
 *
 * @author Julien Coste
 * @goal deploy
 * @requiresProject false
 */
public class DeployMojo
    extends AbstractDeployClifMojo
{

    @Override
    protected void printMojoInfo()
    {
        printInfo( "Deploy Clif test plan", this.testplan );
        printInfo( "Testplan name", this.testplanName );
        printInfo( "Report directory", this.reportDirectory.getAbsolutePath() );
        printInfo( "Clean Report directory", this.cleanReportDirBefore );
    }

    @Override
    protected void doExecute()
        throws MojoExecutionException
    {
        cleanReportDirIfNecessary();

        JavaCommand cmd = getCommand();
        cmd.systemProperty( "clif.filestorage.dir", reportDirectory.getAbsolutePath() )
           .systemProperty( "clif.codeserver.path", testplan.getParent() )
           .arg( this.testplanName )
           .arg( testplan.getAbsolutePath() )
           .execute();
    }
}