/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2004, 2008 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * Contact: clif@ow2.org
 */
package org.ow2.clif.maven.plugin;

import org.apache.commons.lang.StringUtils;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.codehaus.plexus.util.Os;
import org.codehaus.plexus.util.cli.CommandLineException;
import org.codehaus.plexus.util.cli.CommandLineTimeOutException;
import org.codehaus.plexus.util.cli.CommandLineUtils;
import org.codehaus.plexus.util.cli.Commandline;
import org.codehaus.plexus.util.cli.StreamConsumer;

import java.io.File;
import java.io.FileReader;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Properties;

/**
 * Abstract class for all related Clif mojos
 *
 * @author Julien Coste
 */
public abstract class AbstractClifMojo
    extends AbstractMojo
{
    /**
     * Log prefix used to report logs from Clif
     */
    private static final String CLIF_LOG_PREFIX = " [Clif] ";

    private static final int LABEL_PRINT_SIZE = 25;

    /**
     * The path to the clif installation. The recommended way to set this is
     * by specifying a property clif.home in the pom.xml or settings.xml.
     *
     * @parameter expression="${clif.home}"
     * @required
     */
    protected File clifhome;

    /**
     * Clif properties file
     *
     * @parameter expression="${clif.props}"
     */
    protected File clifProps;

    /**
     * Extra JVM arguments that are passed to Clif
     * <p>
     * Can be set from command line using '-Dclif.extraJvmArgs=...'
     * </p>
     *
     * @parameter expression="${clif.extraJvmArgs}" default-value=""
     */
    protected String extraJvmArgs;

    /**
     * Option to specify the jvm (or path to the java executable) to use with the forking scripts. For the default, the
     * jvm will be the same as the one used to run Maven.
     *
     * @parameter expression="${clif.jvm}"
     */
    protected File jvm;

    /**
     * Forked process execution timeOut. Usefull to avoid maven to hang in continuous integration server.
     *
     * @parameter expression="${clif.timeout}"
     */
    protected int timeOut;

    /**
     * A plexus-util StreamConsumer to redirect messages to plugin log
     */
    protected StreamConsumer out = new StreamConsumer()
    {
        public void consumeLine( String line )
        {
            getLog().info( CLIF_LOG_PREFIX + line );
        }
    };

    /**
     * A plexus-util StreamConsumer to redirect errors to plugin log
     */
    protected StreamConsumer err = new StreamConsumer()
    {
        public void consumeLine( String line )
        {
            getLog().error( CLIF_LOG_PREFIX + line );
        }
    };

    /**
     * System-properties to the launched jvm.
     *
     * @parameter
     */
    protected Map<String, String> sysproperties = new HashMap<String, String>();

    /**
     * Miscellaneous options to be added to the jvm launching.
     *
     * @parameter
     */
    protected Map<String, String> options = new HashMap<String, String>();


    /**
     * Checks parameters of the mojo.<p/>
     * If any parameter is invalid a {@link MojoExecutionException} should be thrown.
     *
     * @throws MojoExecutionException if a parameter is invalid
     */
    protected void checkParameters()
        throws MojoExecutionException
    {
        if ( this.clifhome == null )
        {
            throw new MojoExecutionException( "clifhome property not set" );

        }
        checkClifInstallation();
    }

    /**
     * Return the name of the Clif Java classname to run.<p/>
     *
     * @return Java classname to run
     */
    public abstract String getJavaClassname();


    /**
     * Return a {@link JavaCommand} initialize with systemproperties and options passed to the mojo.<br/>
     * The default classpath and java.library.path are set.
     *
     * @return JavaCommand initialized
     * @throws MojoExecutionException if error during parsing of clif.props file
     */
    protected JavaCommand getCommand()
        throws MojoExecutionException
    {
        JavaCommand cmd = new JavaCommand( getJavaClassname() );
        cmd.workingDirectory( this.clifhome )
           .systemProperty( this.sysproperties )
           .environment( this.options )
           .withinClasspath( getClifClasspath() )
           .extraJvmArg( getClifParameters() )
           .systemProperty( "java.library.path", getJavaLibraryPath() );
        return cmd;
    }

    /**
     * Check if clifHome points to a correct Clif Installation.
     *
     * @throws MojoExecutionException if clif installation is invalid
     */
    protected void checkClifInstallation()
        throws MojoExecutionException
    {
        getLog().info( "Checking Clif installation at " + this.clifhome );
        if ( !this.clifhome.exists() )
        {
            throw new MojoExecutionException( "clifhome directory doesn't exist : " + this.clifhome );
        }

        checkFilePresent( this.clifhome, "lib/clif-core.jar" );
        checkFilePresent( this.clifhome, "etc/clif.props" );

        getLog().info( " --> Clif installation seems valid" );
    }

    /**
     * Check if a {@link File} is present in a directory
     *
     * @param baseDir Directory where to search
     * @param file    File to search
     * @throws MojoExecutionException if the searched file doesn't exist
     */
    private void checkFilePresent( File baseDir, String file )
        throws MojoExecutionException
    {
        File fileToCheck = new File( baseDir, file );
        if ( !fileToCheck.exists() )
        {
            throw new MojoExecutionException( "Invalid Clif installation : file [" + file + "] doesn't exist" );
        }
    }

    /**
     * Build the value of the java.library.path variable.<p>
     * This path id depend of the os and the os architecture.
     *
     * @return the java.library.path value
     */
    protected String getJavaLibraryPath()
    {
        String clifOsName = "";
        if ( Os.isFamily( Os.FAMILY_WINDOWS ) )
        {
            clifOsName = "windows";
        }
        else if ( Os.isFamily( Os.FAMILY_UNIX ) && !Os.isFamily( Os.FAMILY_MAC ) )
        {
            clifOsName = Os.OS_NAME;
        }
        else if ( Os.isFamily( Os.FAMILY_MAC ) )
        {
            clifOsName = "macosx";
        }

        File lewysLib = new File( clifhome, "lib/lewys/" + clifOsName + "/" + Os.OS_ARCH );
        File libExt = new File( clifhome, "lib/ext" );

        String javaLibrayPath = lewysLib.getAbsolutePath() + File.pathSeparator + libExt.getAbsolutePath();
        getLog().debug( "java.libray.path=" + javaLibrayPath );
        return javaLibrayPath;
    }

    /**
     * Build the classpath that Clif needs to work properly.<p>
     * This classpath includes :
     * <ul>
     * <li>All files in etc/</li>
     * <li>The etc directory</li>
     * <li>All jars in lib/</li>
     * <li>All jars in lib/ext/</li>
     * </ul>
     *
     * @return the clif classpath
     */
    protected File[] getClifClasspath()
    {
        List<File> files = new ArrayList<File>();

        FilenameFilter jarFilter = new FilenameFilter()
        {
            public boolean accept( File dir, String name )
            {
                return name.endsWith( ".jar" );
            }
        };

        File etcDir = new File( clifhome, "etc" );
        files.addAll( getFiles( etcDir, null ) );
        files.add( etcDir );
        files.addAll( getFiles( new File( clifhome, "lib" ), jarFilter ) );
        files.addAll( getFiles( new File( clifhome, "lib/ext" ), jarFilter ) );
        return files.toArray( new File[files.size()] );
    }

    /**
     * Returns {@link File}s that are in the <code>dir</code> directory that match the optional <code>filter</code>.<p/>
     *
     * @param dir    Directory used to list files
     * @param filter Optional filename filter
     * @return Collection of files that are in the passed directory and that matched the filter
     */
    private Collection<? extends File> getFiles( File dir, FilenameFilter filter )
    {
        return Arrays.asList( dir.listFiles( filter ) );
    }

    /**
     * Get the clif.parameter property as a list of String
     *
     * @return List of clif parameters
     * @throws MojoExecutionException if an error occured when reading Clif properties file.
     */
    protected List<String> getClifParameters()
        throws MojoExecutionException
    {
        Properties clifProperties = new Properties();
        FileReader reader = null;
        try
        {
            if ( clifProps != null )
            {
                reader = new FileReader( clifProps );
            }
            else
            {
                reader = new FileReader( new File( clifhome, "etc/clif.props" ) );
            }
            clifProperties.load( reader );
        }
        catch ( IOException e )
        {
            throw new MojoExecutionException( "Error reading Clif properties file : " + clifProps );
        }
        finally
        {
            if ( reader != null )
            {
                try
                {
                    reader.close();
                }
                catch ( Exception ignored )
                {
                }
            }
        }

        String clifParameters = clifProperties.getProperty( "clif.parameters" );
        List<String> parameters = new ArrayList<String>();
        if ( clifParameters != null )
        {
            parameters.addAll( Arrays.asList( clifParameters.split( " " ) ) );
        }
        return parameters;
    }

    /**
     * Get the java command to execute by analysing the <code>jvm</code> attribut.<p>
     * If the <code>jvm</code> attribut is empty, it use the default jvm (java.home).<p>
     *
     * @return Java command to run
     * @throws MojoExecutionException if specified jvm doesn't exist
     */
    protected String getJavaCommand()
        throws MojoExecutionException
    {
        String javaCommand;
        if ( this.jvm == null )
        {
            // use the same JVM as the one used to run Maven (the "java.home" one)
            jvm = new File( System.getProperty( "java.home" ) );
        }

        // does-it exists ? is-it a directory or a path to a java executable ?
        if ( !jvm.exists() )
        {
            throw new MojoExecutionException(
                "the configured jvm " + jvm + " doesn't exists, please check your environnement" );
        }
        if ( jvm.isDirectory() )
        {
            // it's a directory we construct the path to the java executable
            javaCommand = jvm.getAbsolutePath() + File.separator + "bin" + File.separator + "java";
        }
        else
        {
            javaCommand = jvm.getAbsolutePath();
        }

        getLog().debug( "use jvm " + javaCommand );
        return javaCommand;
    }

    /**
     * Returns Jvm args as a list of String.
     *
     * @return List of all JVM args
     */
    private List<String> getJvmArgs()
    {
        List<String> extra = new ArrayList<String>();
        if ( this.extraJvmArgs != null )
        {
            extra.addAll( Arrays.asList( this.extraJvmArgs.split( " " ) ) );
        }
        return extra;
    }

    /**
     * Prints informations concerning the Mojo.<p/>
     * First prints commons informations.<br/>
     * Then prints Mojo specific informations
     */
    protected void printInfo()
    {
        printInfo( "Clif home", this.clifhome );
        printInfo( "JVM", this.jvm );
        printInfo( "Extra JVM Args", this.extraJvmArgs );
        printInfo( "System properties", this.sysproperties );
        printInfo( "Environment variables", this.options );
        if ( this.timeOut > 0 )
        {
            printInfo( "TimeOut (in seconds)", this.timeOut );
        }
        printMojoInfo();
    }

    /**
     * Print specific mojo informations.<p/>
     * Each Mojo can print specific informations to inform the user
     */
    protected abstract void printMojoInfo();

    /**
     * Print the content of a map.<p/>
     *
     * @param label     Label defining the map
     * @param mapValues Map to print
     */
    protected void printInfo( String label, Map<?, ?> mapValues )
    {
        boolean first = true;
        if ( mapValues != null )
        {
            for ( Map.Entry<?, ?> entry : mapValues.entrySet() )
            {
                if ( first )
                {
                    first = false;
                    printInfo( label, entry.getKey() + "=" + entry.getValue() );
                }
                else
                {
                    printInfo( "", entry.getKey() + "=" + entry.getValue() );
                }
            }
        }
    }

    /**
     * Print the content of an object.<p/>
     *
     * @param label Label defining the object
     * @param value Object to print
     */
    protected void printInfo( String label, Object value )
    {
        if ( value != null )
        {
            String formattedLabel = StringUtils.rightPad( label, LABEL_PRINT_SIZE );
            getLog().info( formattedLabel + ": " + value.toString() );
        }
    }

    public final void execute()
        throws MojoExecutionException
    {
        checkParameters();
        printInfo();
        try
        {
            doExecute();
        }
        catch ( MojoExecutionException meo )
        {
            throw meo;
        }
        catch ( Exception e )
        {
            throw new MojoExecutionException( e.getMessage(), e );
        }
    }

    protected abstract void doExecute()
        throws MojoExecutionException;

    /**
     * Create a command to execute using builder pattern
     */
    public class JavaCommand
    {
        private String className;

        private List<File> classpath = new LinkedList<File>();

        private List<String> args = new ArrayList<String>();

        private Map<String, String> systemProperties = new HashMap<String, String>();

        private List<String> extraJvmArgs = new ArrayList<String>();

        private Properties env = new Properties();

        private File workingDirectory;

        /**
         * Constructs a {@link JavaCommand} with a java classname
         *
         * @param className Java classname to run
         */
        public JavaCommand( String className )
        {
            this.className = className;
        }

        /**
         * Add {@link File}s to the classpath of the {@link JavaCommand}.<p>
         *
         * @param path Files to add to the classpath
         * @return JavaCommand modified (builder pattern
         */
        public LaunchMojo.JavaCommand withinClasspath( File... path )
        {
            classpath.addAll( Arrays.asList( path ) );
            return this;
        }

        /**
         * Set the working directory of the {@link JavaCommand}.<p>
         *
         * @param workDir Working directory
         * @return JavaCommand modified (builder pattern
         */
        public LaunchMojo.JavaCommand workingDirectory( File workDir )
        {
            workingDirectory = workDir;
            return this;
        }

        /**
         * Add an argument to the  {@link JavaCommand}.<p>
         *
         * @param arg Argument to add
         * @return JavaCommand modified (builder pattern
         */
        public LaunchMojo.JavaCommand arg( String arg )
        {
            args.add( arg );
            return this;
        }

        /**
         * Add a system property to the {@link JavaCommand}.<p>
         *
         * @param name  Property name
         * @param value Property value
         * @return JavaCommand modified (builder pattern
         */
        public LaunchMojo.JavaCommand systemProperty( String name, String value )
        {
            systemProperties.put( name, value );
            return this;
        }

        /**
         * Add a system properties to the {@link JavaCommand}.<p>
         *
         * @param properties Properties to add
         * @return JavaCommand modified (builder pattern
         */
        public LaunchMojo.JavaCommand systemProperty( Map<String, String> properties )
        {
            systemProperties.putAll( properties );
            return this;
        }

        /**
         * Add extra JVM args to the {@link JavaCommand}.<p>
         *
         * @param args Args to add
         * @return JavaCommand modified (builder pattern
         */
        public LaunchMojo.JavaCommand extraJvmArg( List<String> args )
        {
            extraJvmArgs.addAll( args );
            return this;
        }


        /**
         * Add a set of environment variables to the {@link JavaCommand}.<p>
         *
         * @param options Maps containing the environment variables
         * @return JavaCommand modified (builder pattern
         */
        public LaunchMojo.JavaCommand environment( Map<String, String> options )
        {
            if ( options != null )
            {
                for ( Map.Entry<String, String> entry : options.entrySet() )
                {
                    environment( entry.getKey(), entry.getValue() );
                }
            }
            return this;
        }

        /**
         * Add an environment variable to the {@link JavaCommand}.<p>
         *
         * @param name  Variable name
         * @param value Variable value
         * @return JavaCommand modified (builder pattern
         */
        public LaunchMojo.JavaCommand environment( String name, String value )
        {
            env.setProperty( name, value );
            return this;
        }

        /**
         * Execute the {@link JavaCommand}.<p>
         *
         * @throws MojoExecutionException if any error is detected
         */
        public void execute()
            throws MojoExecutionException
        {
            List<String> command = new ArrayList<String>();
            command.addAll( getJvmArgs() );
            command.addAll( extraJvmArgs );
            command.add( "-classpath" );
            List<String> path = new ArrayList<String>( classpath.size() );
            for ( File file : classpath )
            {
                path.add( file.getAbsolutePath() );
            }
            command.add( StringUtils.join( path.iterator(), File.pathSeparator ) );
            if ( systemProperties != null )
            {
                for ( Map.Entry<String, String> entry : systemProperties.entrySet() )
                {
                    if ( StringUtils.isEmpty( entry.getValue() ) )
                    {
                        command.add( "-D" + entry.getKey() );
                    }
                    else
                    {
                        command.add( "-D" + entry.getKey() + "=" + entry.getValue() );
                    }
                }
            }
            command.add( className );
            command.addAll( args );

            try
            {
                String[] arguments = command.toArray( new String[command.size()] );

                // On windows, the default Shell will fall into command line length limitation issue
                // On Unixes, not using a Shell breaks the classpath (NoClassDefFoundError:
                // com/google/gwt/dev/Compiler).
                Commandline cmd =
                    Os.isFamily( Os.FAMILY_WINDOWS ) ? new Commandline( new JavaShell() ) : new Commandline();
                if ( this.workingDirectory != null )
                {
                    cmd.setWorkingDirectory( this.workingDirectory );
                }
                cmd.setExecutable( getJavaCommand() );
                cmd.addArguments( arguments );
                if ( env != null )
                {
                    for ( Map.Entry entry : env.entrySet() )
                    {
                        getLog().debug( "add env " + entry.getKey() + " with value " + entry.getValue() );
                        cmd.addEnvironment( (String) entry.getKey(), (String) entry.getValue() );
                    }
                }
                getLog().debug( "Execute command :\n" + cmd.toString() );
                int status;
                if ( timeOut > 0 )
                {
                    status = CommandLineUtils.executeCommandLine( cmd, out, err, timeOut );
                }
                else
                {
                    status = CommandLineUtils.executeCommandLine( cmd, out, err );
                }

                if ( status != 0 )
                {
                    throw new ForkedProcessExecutionException(
                        "Command [[\n" + cmd.toString() + "\n]] failed with status " + status );
                }
            }
            catch ( CommandLineTimeOutException e )
            {
                if ( timeOut > 0 )
                {
                    throw new MojoExecutionException(
                        "Forked JVM has been killed on time-out after " + timeOut + " seconds" );
                }
                throw new MojoExecutionException( "Time-out on command line execution :\n" + command, e );
            }
            catch ( CommandLineException e )
            {
                throw new MojoExecutionException( "Failed to execute command line :\n" + command, e );
            }
        }
    }
}
