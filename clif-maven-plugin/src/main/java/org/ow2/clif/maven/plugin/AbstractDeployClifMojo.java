/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2004, 2008 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * Contact: clif@ow2.org
 */
package org.ow2.clif.maven.plugin;

import org.apache.commons.io.FileUtils;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.project.MavenProject;

import java.io.File;
import java.io.IOException;

/**
 * Abstract class for Mjos that deploy a test plan
 *
 * @author Julien Coste
 */
public abstract class AbstractDeployClifMojo
    extends AbstractBatchCmdClifMojo
{
    /**
     * The archetype project to execute the integration tests on.
     *
     * @parameter expression="${project}"
     * @required
     * @readonly
     */
    private MavenProject project;

    /**
     * The testplan
     *
     * @parameter expression="${clif.testplan}"
     * @required
     */
    protected File testplan;

    /**
     * The report directory used by Clif
     *
     * @parameter expression="${clif.reportDirectory}" default-value="${project.build.directory}/clif-reports"
     * @required
     */
    protected File reportDirectory;

    /**
     * Should the plugin delete the content of the report directory before running Clif.
     * Default value is true.
     *
     * @parameter expression="${clif.cleanReportDirBefore}" default-value="true"
     */
    protected boolean cleanReportDirBefore = true;

    @Override
    protected void checkParameters()
        throws MojoExecutionException
    {
        super.checkParameters();
        if ( this.testplan == null )
        {
            throw new MojoExecutionException( "testplan property not set" );
        }
        if ( this.reportDirectory == null )
        {
            throw new MojoExecutionException( "reportDirectory property not set" );
        }
        checkReportDirectory();
    }

    /**
     * If the mojo is launched without a project, force the reportDirectory to "clif-reports"
     */
    protected void checkReportDirectory()
    {
        if ( project != null && project.getBasedir() == null )
        {
            this.reportDirectory = new File( "clif-reports" );
        }
    }

    /**
     * Erase the report directory if the <code>cleanReportDirBefore</code> property is set to true.
     *
     * @throws org.apache.maven.plugin.MojoExecutionException
     *          if report directory deletion is impossible
     */
    protected void cleanReportDirIfNecessary()
        throws MojoExecutionException
    {

        if ( this.cleanReportDirBefore && this.reportDirectory.exists() )
        {
            try
            {
                FileUtils.forceDelete( this.reportDirectory );
            }
            catch ( IOException ioe )
            {
                getLog().error( "Unable to delete report directory", ioe );
                throw new MojoExecutionException( "Unable to delete report directory : " + this.reportDirectory );
            }
        }
    }


}
