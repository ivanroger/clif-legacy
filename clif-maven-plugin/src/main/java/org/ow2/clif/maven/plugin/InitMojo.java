/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2004, 2008 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * Contact: clif@ow2.org
 */
package org.ow2.clif.maven.plugin;

import org.apache.maven.plugin.MojoExecutionException;

/**
 * Initialize the test run.<p/>
 * Initializes all probes and injectors in a deployed test plan, 
 * or just a subset of them when mentioned. 
 * The target deployed test plan is designated by its name.
 *
 * @author Julien Coste
 * @goal init
 * @requiresProject false
 */
public class InitMojo
    extends AbstractBatchCmdClifMojo
{

    /**
     * The test run id to use
     *
     * @parameter expression="${clif.testrun.id}"
     * @required
     */
    protected String testrunId;

    @Override
    protected void printMojoInfo()
    {
        printInfo( "Initializing test plan", this.testplanName );
        printInfo( "Test run id", this.testrunId );
    }

    @Override
    protected void doExecute()
        throws MojoExecutionException
    {
        JavaCommand cmd = getCommand();
        cmd.arg( this.testplanName ).arg( this.testrunId ).execute();
    }
}
