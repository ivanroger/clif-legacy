/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2004, 2008 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * Contact: clif@ow2.org
 */
package org.ow2.clif.maven.plugin;

/**
 * Collects results generated by the probes and injectors of the given deployed test plan.<p/>
 * Collecting is optional, i.e. the user may not collect results s/he is not interested in.
 * Injectors and probes must be terminated prior to this command.
 *
 * @author Julien Coste
 * @goal collect
 * @requiresProject false
 */
public class CollectMojo
    extends AbstractBatchCmdClifMojo
{

    @Override
    protected void printMojoInfo()
    {
        printInfo( "Collecting test plan", this.testplanName );
    }

}
