/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2004, 2008 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * Contact: clif@ow2.org
 */
package org.ow2.clif.maven.plugin;

import org.apache.maven.plugin.MojoExecutionException;

/**
 * Run a Registry and a default CLIF server named "local host" on one of your nodes.<p/>
 * This command does not return.
 *
 * @author Julien Coste
 * @goal server
 * @requiresProject false
 */
public class ServerMojo
    extends AbstractClifMojo
{

    /**
     * The server name to use
     *
     * @parameter expression="${clif.server.name}"
     */
    protected String serverName;

    @Override
    protected void printMojoInfo()
    {
       if (this.serverName == null)
       {
           printInfo( "Server name", "Using default server name" );
       }
        else
       {
           printInfo( "Server name", this.serverName);
       }
    }

    public void doExecute()
        throws MojoExecutionException
    {
        JavaCommand cmd = getCommand();
        if (this.serverName != null)
        {
            cmd.arg( this.serverName );
        }
        cmd.execute();
    }

    public final String getJavaClassname()
    {
        return "org.ow2.clif.server.lib.ClifServerImpl";
    }
}
