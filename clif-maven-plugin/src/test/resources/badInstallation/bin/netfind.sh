#!/bin/sh
# Prints to standard output the local IP address from
# which the provided host IP address or name is reachable.
# Keeps mute if no local address routes to the target host.
# 2 arguments expected:
# $1 gives the target host IP address or name,
# $2 gives the ping timeout in s for reachability assessment
# of each local address.

ip route | grep src | while read line
do
	network=`echo $line | cut -d' ' -f1`
	address=`echo $line | cut -d' ' -f9`
	if ping -q -I $address -c 1 -W $2 $1 >/dev/null
	then
		echo "network.address=$network"	
		exit 0
	fi
done
