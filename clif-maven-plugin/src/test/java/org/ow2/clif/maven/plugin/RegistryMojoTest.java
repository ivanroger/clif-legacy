package org.ow2.clif.maven.plugin;

import org.apache.maven.plugin.MojoExecutionException;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * @author Julien Coste
 */
public class RegistryMojoTest
    extends AbstractClifMojoTest
{
    BufferStreamConsumer bscOut = new BufferStreamConsumer();
    BufferStreamConsumer bscErr = new BufferStreamConsumer();


    @Test
    public void testDoExecute()
        throws Exception
    {
        RegistryMojo mojo = new RegistryMojo();
        mojo.err = bscErr;
        mojo.out = bscOut;

        mojo.clifhome = getFileResource( "goodInstallation" );
        mojo.timeOut = DEFAULT_TIMEOUT;

        try
        {
        mojo.execute();
            fail();
        }
        catch (MojoExecutionException mee)
        {
            assertEquals( "Forked JVM has been killed on time-out after 10 seconds", mee.getMessage());
            System.out.println("out = "+ bscOut.getOutput());
            assertTrue( bscOut.getOutput().contains("Fractal registry is ready") );
        }
    }
}
