/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2004, 2008 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * Contact: clif@ow2.org
 */
package org.ow2.clif.maven.plugin;

import org.apache.maven.plugin.MojoExecutionException;
import org.junit.Test;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

/**
 * @author Julien Coste
 */
public class LaunchMojoTest
    extends AbstractClifMojoTest
{

    @Test
    public void testGetClifClasspath()
    {

        AbstractClifMojo mojo = new LaunchMojo();

        mojo.clifhome = getFileResource( "goodInstallation" );
        File[] files = mojo.getClifClasspath();
        assertNotNull( files );
        List<String> names = new ArrayList<String>();

        for ( File file : files )
        {
            names.add( file.getName() );
        }

        assertTrue( "etc directory not found in classpath", names.contains( "etc" ) );
        assertTrue( "clif.props not found in classpath", names.contains( "clif.props" ) );
        assertTrue( "clif-core.jar not found in classpath", names.contains( "clif-core.jar" ) );
        assertTrue( "imap.jar not found in classpath", names.contains( "imap.jar" ) );

    }


    @Test
    public void testGetJavaLibraryPath()
    {

        AbstractClifMojo mojo = new LaunchMojo();
        mojo.clifhome = getFileResource( "goodInstallation" );
        String javaLibraryPath = mojo.getJavaLibraryPath();
        assertNotNull( javaLibraryPath );
        System.out.println( javaLibraryPath );

    }

    @Test
    public void testGetJavaClassname()
        throws Exception
    {

        LaunchMojo mojo = new LaunchMojo();
        assertEquals( "Bad java classname", "org.ow2.clif.console.lib.batch.LaunchCmd", mojo.getJavaClassname() );
    }

    // ###########################################################################
    // ## Test methods for checkClifInstallation()                              ##
    // ###########################################################################

    @Test
    public void testCheckClifInstallation()
        throws Exception
    {
        AbstractClifMojo mojo = new LaunchMojo();
        mojo.clifhome = getFileResource( "goodInstallation" );

        mojo.checkClifInstallation();

    }

    @Test
    public void testCheckClifInstallation_badInstallationNotExist()
        throws Exception
    {
        AbstractClifMojo mojo = new LaunchMojo();
        mojo.clifhome = new File( "/unknown/" );

        try
        {
            mojo.checkClifInstallation();
            fail();
        }
        catch ( MojoExecutionException mee )
        {
            assertTrue( "Bad message [" + mee.getMessage() + "]", mee.getMessage().contains( "clifhome" ) );
        }
    }

    @Test
    public void testCheckClifInstallation_badInstallationNotValid()
        throws Exception
    {
        AbstractClifMojo mojo = new LaunchMojo();
        mojo.clifhome = getFileResource( "badInstallation" );

        try
        {
            mojo.checkClifInstallation();
            fail();
        }
        catch ( MojoExecutionException mee )
        {
            assertTrue( "Bad message [" + mee.getMessage() + "]",
                        mee.getMessage().contains( "Invalid Clif installation" ) );
        }
    }

    // ###########################################################################
    // ## Test methods for execute()                                            ##
    // ###########################################################################

    @Test
    public void testExecuteWithTimeout()
        throws Exception
    {

        LaunchMojo mojo = new LaunchMojo();
        mojo.clifhome = getFileResource( "goodInstallation" );
        mojo.reportDirectory = new File( "target/clif-reports" );
        mojo.testplan = getFileResource( "dummy/dummy.ctp" );
        mojo.testplanName = "dummy";
        mojo.timeOut = 3;
        try
        {
            mojo.execute();
            fail();
        }
        catch ( MojoExecutionException mee )
        {
            assertTrue( "Bad message [" + mee.getMessage() + "]",
                        mee.getMessage().contains( "Forked JVM has been killed" ) );
        }
    }

    @Test
    public void testExecute()
        throws Exception
    {

        LaunchMojo mojo = new LaunchMojo();
        mojo.clifhome = getFileResource( "goodInstallation" );
        mojo.reportDirectory = new File( "target/clif-reports" );
        mojo.testplan = getFileResource( "dummy/dummy.ctp" );
        mojo.testplanName = "dummy";

        mojo.execute();
    }

    @Test
    public void testExecuteWithBadJvm()
    {

        LaunchMojo mojo = new LaunchMojo();
        mojo.clifhome = getFileResource( "goodInstallation" );
        mojo.jvm = getFileResource( "fakeJVM/bin" );
        mojo.reportDirectory = new File( "target/clif-reports" );
        mojo.testplan = getFileResource( "dummy/dummy.ctp" );
        mojo.testplanName = "dummy";

        try
        {
            mojo.execute();
            fail();
        }
        catch ( MojoExecutionException mee )
        {
            System.out.println( mee.getMessage() );
            assertTrue( "Bad message [" + mee.getMessage() + "]",
                        mee.getMessage().contains( "Failed to execute command line" ) );
        }
    }

    @Test
    public void testExecuteWithBadTestPlan()
    {

        LaunchMojo mojo = new LaunchMojo();
        mojo.clifhome = getFileResource( "goodInstallation" );
        mojo.reportDirectory = new File( "target/clif-reports" );
        mojo.testplan = new File( "unknown.ctp" );
        mojo.testplanName = "dummy";

        try
        {
            mojo.execute();
            fail();
        }
        catch ( MojoExecutionException mee )
        {
            System.out.println( mee.getMessage() );
            assertTrue( "Bad message [" + mee.getMessage() + "]", mee.getMessage().contains( "failed with status" ) );
        }
    }

    @Test
    public void testExecuteFullConfig()
        throws Exception
    {

        LaunchMojo mojo = new LaunchMojo();
        mojo.clifhome = getFileResource( "goodInstallation" );
        mojo.reportDirectory = new File( "target/clif-reports" );
        mojo.testplan = getFileResource( "dummy/dummy.ctp" );
        mojo.testplanName = "dummy";
        mojo.sysproperties.put( "sysprop1", "value1" );
        mojo.sysproperties.put( "sysprop2", "value2" );
        mojo.options.put( "option1", "value1" );
        mojo.options.put( "option2", "value2" );
        mojo.extraJvmArgs = "-Dtest=value";
        mojo.execute();
    }

    // ###########################################################################
    // ## Test methods for getJavaCommand()                                     ##
    // ###########################################################################

    @Test
    public void testGetJavaCommand_NoJvmSupplied()
        throws Exception
    {
        AbstractClifMojo mojo = new LaunchMojo();
        mojo.jvm = null;
        String javaCommand = mojo.getJavaCommand();
        assertNotNull( "javaCommand null", javaCommand );
        assertTrue( "No java executable [" + javaCommand + "]", javaCommand.endsWith( "java" ) );
    }

    @Test
    public void testGetJavaCommand_GoodDirJvmSupplied()
        throws Exception
    {
        AbstractClifMojo mojo = new LaunchMojo();
        mojo.jvm = getFileResource( "fakeJVM" );
        String javaCommand = mojo.getJavaCommand();
        assertNotNull( "javaCommand null", javaCommand );
        assertTrue( "No java executable [" + javaCommand + "]", javaCommand.endsWith( "java" ) );
    }

    @Test
    public void testGetJavaCommand_GoodExecJvmSupplied()
        throws Exception
    {
        AbstractClifMojo mojo = new LaunchMojo();
        mojo.jvm = getFileResource( "fakeJVM/bin/java" );
        String javaCommand = mojo.getJavaCommand();
        assertNotNull( "javaCommand null", javaCommand );
        assertTrue( "No java executable [" + javaCommand + "]", javaCommand.endsWith( "java" ) );
    }

    @Test( expected = MojoExecutionException.class )
    public void testGetJavaCommand_UnknownDirJvmSupplied()
        throws Exception
    {
        AbstractClifMojo mojo = new LaunchMojo();
        mojo.jvm = new File( "/unknown" );
        mojo.getJavaCommand();

    }

}
