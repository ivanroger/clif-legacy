/*
* CLIF is a Load Injection Framework
* Copyright (C) 2009-2011 France Telecom R&D
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
* Contact: clif@ow2.org
*/

package org.ow2.clif.deploy;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.Observable;
import java.util.Observer;
import org.ow2.clif.console.lib.egui.ClifConsolePlugin;
import org.ow2.clif.storage.api.AlarmEvent;
import org.ow2.clif.util.ExecutionContext;
import org.ow2.clif.util.FractalAdl;
import org.objectweb.fractal.api.control.BindingController;
import org.objectweb.fractal.api.control.ContentController;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.adl.FactoryFactory;
import org.objectweb.fractal.adl.ADLException;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.IllegalLifeCycleException;
import org.objectweb.fractal.util.Fractal;

/**
 * @author Bruno Dillenseger
 * @author Florian Francheteau
*/
public class ClifAnalyzerAppFacade extends Observable implements Observer
{
	/** Fractal ADL definition file of the CLIF application to deploy */
	static final String CLIF_APPLICATION = "org.ow2.clif.console.lib.egui.ClifAnalyzerApp";


	protected Component clifAnalyzerApp;
	protected Component storage;
	protected Component analyzer;
	
	protected Map<String,Component> components = new HashMap<String,Component>();
	protected BindingController storageBc;
	protected BindingController analyzerBc;
	protected ContentController clifAnalyzerAppCc;
	
	
	private static ClifAnalyzerAppFacade clifAnalyzerAppFacade = new ClifAnalyzerAppFacade("ClifRCPconsole", CLIF_APPLICATION);
	
	
	/**
	 * Create new ClifAnalyzerAppFacade with all components of a ClifApplication : 
	 * storage, analyzer...
	 * @param testName the name in the Registry to be associated with the
	 * resulting Clif Application.  
	 * @param appDefinition fully-qualified Fractal ADL definition file of the
	 * CLIF application to instantiate.
	 * @throws Error
	 */
	private ClifAnalyzerAppFacade(String testName, String appDefinition) throws Error
	{
		File clifPropsFile = new File(ExecutionContext.getBaseDir() + File.separator + ExecutionContext.PROPS_PATH);
		updateProperties(clifPropsFile);
		// Create Fractal Components
		try
		{
			clifAnalyzerApp = (Component)FactoryFactory
				.getFactory(FractalAdl.CLIF_BACKEND)
				.newComponent(appDefinition, null);
			setClifAnalyzerAppComponents();
			Fractal.getNameController(clifAnalyzerApp).setFcName(testName);
			
		}
		catch (Exception ex)
		{
			throw new Error("Could not create CLIF deployer: bad component configuration", ex);
		}
	}
	

	/**
	 * Get unique instance of ClifAnalyzerAppFacade
	 * @return clifAnalyzerAppFacade
	 */
	public static ClifAnalyzerAppFacade getInstance()
	{
		return clifAnalyzerAppFacade;
	}
	
	
	/**
	 * Set clifAnalyzerApp sub-components
	 * @throws ADLException
	 * @throws NoSuchInterfaceException
	 * @throws IllegalLifeCycleException
	 */
	private void setClifAnalyzerAppComponents()
		throws ADLException, NoSuchInterfaceException, IllegalLifeCycleException
	{
		Component[] subComp = Fractal.getContentController(clifAnalyzerApp).getFcSubComponents();
	
		for (int i=0 ; i<subComp.length ; ++i)
		{
			components.put(Fractal.getNameController(subComp[i]).getFcName(), subComp[i]);
			if (Fractal.getNameController(subComp[i]).getFcName().equals("storage"))
			{
				storage = subComp[i];
			}
			else if (Fractal.getNameController(subComp[i]).getFcName().equals("analyzer"))
			{
				analyzer = subComp[i];
			}
		}
		
		storageBc = Fractal.getBindingController(storage);
		analyzerBc = Fractal.getBindingController(analyzer);
		clifAnalyzerAppCc = Fractal.getContentController(clifAnalyzerApp);
	}
	
	/**
	 * Get a component of the clifApplication by his Fractal name
	 * @param name the name of the component to find
	 * @return the reference of a component contained by the Clif Application whose name equals the
	 * String passed as parameter, or null if there is no such component.
	 */
	public Component getComponentByName(String name)
	{
		return components.get(name);
	}
	
	/**
	 * Get clifAnalyzerApp component
	 * @return Returns the clifAnalyzerApp component.
	 */
	public Component getClifAnalyzerApp() {
		return clifAnalyzerApp;
	}
	
	
    public void update(Observable supervisor, Object observation) {
	    if(countObservers() == 0 && observation instanceof AlarmEvent)
	    {
	        AlarmEvent alarm = (AlarmEvent)observation;
			if (alarm.argument != null)
			{
				if (alarm.argument instanceof Throwable)
				{
				    ((Throwable)alarm.argument).printStackTrace();
				    System.exit(1);
				}
			}
	    }
		setChanged();
		notifyObservers(observation);
	}


    static public void updateProperties(File clifProps)
    {
        try
        {
        	ClifConsolePlugin.getDefault().setProperties(clifProps);
        }
        catch (Exception ex)
        {
        	System.err.println("Can't set system properties from file " + clifProps);
        	ex.printStackTrace(System.err);
        }
    }
}
