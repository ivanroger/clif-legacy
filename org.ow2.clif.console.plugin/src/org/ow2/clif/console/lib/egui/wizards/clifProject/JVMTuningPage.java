/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2009 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * Contact: clif@ow2.org
 */

package org.ow2.clif.console.lib.egui.wizards.clifProject;

import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.widgets.Composite;
import org.ow2.clif.console.lib.egui.ClifConsolePlugin;

/**
 * Fifth page of the New CLIF project Wizard. 
 * Used to tune JVM heap size
 * 
 * @author Florian Francheteau
 */
public class JVMTuningPage extends WizardPage{

	private InteractionManager interaction;

	/**
	 * Constructor
	 */
	public JVMTuningPage() {
		super("wizardPage");
		setTitle("JVM Tuning");
		setDescription("JVM properties will not be used " +
				"automatically after restarting CLIF RCP Console. " +
				"For this, please enter them through command line " +
				"when starting CLIF RCP Console." +
				"These properties are only used to be written to " +
				ClifConsolePlugin.CLIF_PROPS +
				"file for sharing with other CLIF users");
	}

	/**
	 * Creates and initializes controls through InterationManager
	 */
	public void createControl(Composite parent) {
		interaction = new InteractionManager(this,null);
		Composite comp = interaction.createJVMContents(parent);
		interaction.initializeJVMParam();
		setControl(comp);
	}	

	public String getXms(){
		return interaction.getXms();
	}
	public String getXmx(){
		return interaction.getXmx();
	}
}
