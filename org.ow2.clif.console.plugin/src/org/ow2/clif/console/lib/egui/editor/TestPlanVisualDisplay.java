/*
 * CLIF is a Load Injection Framework
 * Copyright 2005 Manuel Azema, Tsirimiaina Andrianavonimiarina Jaona
 * Copyright (C) 2009 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * CLIF $Name: not supported by cvs2svn $
 *
 * Contact: clif@ow2.org
 */

package org.ow2.clif.console.lib.egui.editor;

import org.eclipse.jface.viewers.*;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CTabFolder;
import org.eclipse.swt.custom.CTabItem;
import org.eclipse.swt.events.MouseListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.*;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.forms.IManagedForm;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.ow2.clif.console.lib.ClifDeployDefinition;
import org.ow2.clif.console.lib.egui.ClifConsolePlugin;
import org.ow2.clif.console.lib.egui.editor.exceptions.BadBladePropertiesException;
import org.ow2.clif.console.lib.egui.editor.exceptions.ExistingBladeIdException;
import org.ow2.clif.supervisor.api.BladeState;
import org.ow2.clif.supervisor.api.ClifException;
import org.ow2.clif.util.ExecutionContext;

import java.util.*;
import java.util.List;

/**
 * Tab Folder that containts one tab for each kind of blade.
 * All blades of the same class are grouped together.<br/>
 * The blades are displayed in a table with 7 columns:
 * <ul>
 * <li>Blade id</li>
 * <li>Deploy server name</li>
 * <li>Blade role (injector or probe)</li>
 * <li>Blade class</li>
 * <li>Blade scenario argument</li>
 * <li>Comment</li>
 * <li>Execution state</li>
 * </ul>
 * A check box and the last state column are optionnal.
 * Use the withCheck and withState constructor boolean to show/hide them.<br/>
 * A selection changed listener can be added in the constructor too.
 *
 * @author Manuel AZEMA
 */
public class TestPlanVisualDisplay extends CTabFolder {
	
	/* The display test plan. */
	private Map<String, ClifDeployDefinition> testPlan;

	/* The display tables viewer. */
	private Map<String, TableViewer> tables;

	/* If withState, state of every blade. */
	private Map<String, BladeState> bladeStates;

	/* Auto blade id for new blade. */
	private int blade_id = 0;

	/* True if check box. */
	private boolean withCheck;

	/* True if state column. */
	private boolean withState;

	/* The managed form. */
	private IManagedForm managedForm;

	/* The editor input. */
	private IEditorInput editorInput;

	/* Listener use by every table viewer. */
	private ISelectionChangedListener selectionChangedListener;
	private MouseListener checkListener;
	private SelectionListener selectionListener;

	/**
	 * Blade id provider. Provide id same blade class blades.
	 *
	 * @author Manuel AZEMA
	 */
	class BladeIdContentProvider implements IStructuredContentProvider {

		/**
		 * Blades class provides.
		 */
		private String bClass;

		/**
		 * Construct a new provider that display <i>bClass</i> blades.
		 *
		 * @param bClass blades class provides
		 */
		public BladeIdContentProvider(String bClass) {
			this.bClass = bClass;
		}

		/**
		 * Return the blade class provide.
		 *
		 * @return the blade class provide
		 */
		public String getBladeClass() {
			return bClass;
		}

		/*
				 * org.eclipse.jface.viewers.IStructuredContentProvider methods
				 * @see org.eclipse.jface.viewers.IStructuredContentProvider
				 */

		public Object[] getElements(Object inputElement) {
			List<String> ids = new ArrayList<String>();
			Iterator<Map.Entry<String, ClifDeployDefinition>> it = testPlan.entrySet().iterator();
			//Return bClass blades
			while (it.hasNext()) {
				Map.Entry<String, ClifDeployDefinition> entry = it.next();
				String id = entry.getKey();
				ClifDeployDefinition def = entry.getValue();
				String bClassTest = def.getClassName();

				if (bClass.equals(bClassTest)) {
					ids.add(id);
				}
			}
			return ids.toArray();
		}

		public void dispose() {
		}

		public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
		}
	}

	/**
	 * Display blade properties in columns.
	 *
	 * @author Manuel AZEMA
	 * @author Joan Chaumont
	 * @author Florian Francheteau
	 */
	class BladeLabelProvider extends LabelProvider implements ITableLabelProvider, IColorProvider {

		/**
		 * Get blade properties with its id in the test plan displayed
		 * and provide for each column.
		 */
		public String getColumnText(Object obj, int index) {
			String id = obj.toString();
			ClifDeployDefinition def = testPlan.get(id);
			if (index == TestPlanEditor.INDEX_COL_ID) {
				return id;
			} else if (index == TestPlanEditor.INDEX_COL_SERVER) {
				return def.getServerName();
			} else if (index == TestPlanEditor.INDEX_COL_ROLE) {
				return def.isProbe() ? "probe" : "injector";
			} else if (index == TestPlanEditor.INDEX_COL_CLASS) {
				if (def.isProbe()) {
					String probeName = def.getContext().get("insert");
					probeName = probeName.substring(0, probeName.lastIndexOf('.'));
					return probeName.substring(1 + probeName.lastIndexOf('.'));
				} else {
					return def.getContext().get("insert").toString();
				}
			} else if (index == TestPlanEditor.INDEX_COL_ARGUMENT) {
				return def.getArgument();
			} else if (index == TestPlanEditor.INDEX_COL_COMMENT) {
				return def.getComment();
			} else if (index == TestPlanEditor.INDEX_COL_STATE) {
				if (bladeStates != null && bladeStates.get(id) != null) {
					return bladeStates.get(id).toString();
				} else {
					return BladeState.UNDEPLOYED.toString();
				}
			} else {
				return obj.toString();
			}
		}

		/**
		 * Maybe we'll can use different image for probe and injector for instance.
		 * Not used here.
		 */
		public Image getColumnImage(Object obj, int index) {
			return null;
		}

		public Color getForeground(Object element) {
			if (element == null || element.toString().equals("")) {
				return getDisplay().getSystemColor(SWT.COLOR_RED);
			}
			ClifDeployDefinition def = testPlan.get(element.toString());

			try {
				String serverName = def.getServerName();
				List<String> serversList = Arrays.asList(ClifConsolePlugin.getDefault().getRegistry(false).getServers());

				if (serversList.indexOf(serverName) == -1) {
					return getDisplay().getSystemColor(SWT.COLOR_RED);
				}
			}
			catch (ClifException ex) {
				ex.printStackTrace(System.err);
				return getDisplay().getSystemColor(SWT.COLOR_RED);
			}
			if (def.getServerName() == null || def.getServerName().equals("")
					|| def.getClassName() == null || def.getClassName().equals("")) {
				return getDisplay().getSystemColor(SWT.COLOR_RED);
			}
			return null;
		}

		public Color getBackground(Object element) {
			return null;
		}
	}

	/**
	 * Create a tab folder that display in tab each blade sort by class.
	 *
	 * @param parent				   the composite parent
	 * @param testPlan				 the display test plan
	 * @param managedForm			  the managed form
	 * @param editorInput			  the editor input
	 * @param style					the tab folde style
	 * @param selectionChangedListener default listener for all table viewer (can be null)
	 * @param checkListener			listener used when check box are modified
	 * @param withCheck				true if check box is diplayed in the first column
	 * @param withState				true if state column is diplayed as the last column
	 */
	public TestPlanVisualDisplay(Composite parent, Map<String, ClifDeployDefinition> testPlan,
								 final IManagedForm managedForm, IEditorInput editorInput, int style,
								 ISelectionChangedListener selectionChangedListener,
								 MouseListener checkListener,
								 boolean withCheck, boolean withState) {
		super(parent, style);

		this.testPlan = testPlan;
		this.managedForm = managedForm;
		this.editorInput = editorInput;

		this.selectionChangedListener = selectionChangedListener;
		this.checkListener = checkListener;

		this.withCheck = withCheck;
		this.withState = withState;

		GridData gd = new GridData(GridData.FILL_BOTH);
		gd.heightHint = 20;
		gd.widthHint = 100;
		this.setLayoutData(gd);

		FormToolkit toolkit = managedForm.getToolkit();
		toolkit.adapt(this);

		selectionListener = new SelectionListener() {
			public void widgetSelected(SelectionEvent e) {
				String bClass = ((TestPlanVisualDisplay) e.widget).getSelection().getText();
				TableViewer viewer = (TableViewer) tables.get(bClass);

				if (viewer != null) {
					viewer.setSelection(new StructuredSelection(viewer.getElementAt(0)));
				}
			}

			public void widgetDefaultSelected(SelectionEvent e) {
			}
		};

		this.addSelectionListener(selectionListener);

		createTabByClass();
	}


	/*
		 * Graphics methods
		 */

	/**
	 * Create one tab by blade class.
	 * Add blades in the same tab if they have the same class.
	 * Injectors are in the same tab.
	 */
	public void createTabByClass() {
		tables = new HashMap<String, TableViewer>();

		Iterator<Map.Entry<String, ClifDeployDefinition>> it = testPlan.entrySet().iterator();
		while (it.hasNext()) {
			//Get id and class for each blade
			Map.Entry<String, ClifDeployDefinition> entry = it.next();
			ClifDeployDefinition def = entry.getValue();
			String bClass = def.getClassName();

			//Test if another blade exists with the same class
			TableViewer viewer = (TableViewer) tables.get(bClass);
			//If false, create a new blade class tab
			if (viewer == null) {
				//Create a new tab
				addTab(bClass);
			}
		}
	}

	/**
	 * Add a new tab that display <i>bClass</i> blades.
	 *
	 * @param bClass blade class name
	 */
	private void addTab(String bClass) {
		FormToolkit toolkit = managedForm.getToolkit();

		/* Create a new tab */
		CTabItem tabBladeClass = new CTabItem(this, SWT.BORDER | SWT.FLAT);
		tabBladeClass.setText(bClass);

		/* Create table with or without check box in the first column */
		final Table table = isWithCheck() ? toolkit.createTable(this, SWT.FULL_SELECTION | SWT.CHECK) : toolkit.createTable(this, SWT.FULL_SELECTION);

		if (checkListener != null) {
			table.addMouseListener(checkListener);
		}

		GridLayout layout = new GridLayout(1, true);
		layout.verticalSpacing = 0;
		layout.marginHeight = 0;
		layout.marginWidth = 0;
		table.setLayout(layout);
		GridData gd = new GridData(GridData.FILL_BOTH);
		gd.heightHint = 20;
		gd.widthHint = 100;
		table.setLayoutData(gd);

		table.setHeaderVisible(true);

		/* Create columns */

		/* Id column */
		TableColumn col = new TableColumn(table, SWT.LEFT);
		col.setText(TestPlanEditor.colName[TestPlanEditor.INDEX_COL_ID]);
		col.setWidth(50);

		/* Server name column */
		col = new TableColumn(table, SWT.LEFT);
		col.setText(TestPlanEditor.colName[TestPlanEditor.INDEX_COL_SERVER]);
		col.setWidth(160);

		/* Blade role column */
		col = new TableColumn(table, SWT.LEFT);
		col.setText(TestPlanEditor.colName[TestPlanEditor.INDEX_COL_ROLE]);
		col.setWidth(60);

		/* Blade class column */
		col = new TableColumn(table, SWT.LEFT);
		col.setText(TestPlanEditor.colName[TestPlanEditor.INDEX_COL_CLASS]);
		col.setWidth(100);

		/* Argument column */
		col = new TableColumn(table, SWT.LEFT);
		col.setText(TestPlanEditor.colName[TestPlanEditor.INDEX_COL_ARGUMENT]);
		col.setWidth(120);

		/* Comment column */
		col = new TableColumn(table, SWT.LEFT);
		col.setText(TestPlanEditor.colName[TestPlanEditor.INDEX_COL_COMMENT]);
		col.setWidth(50);

		/* If withState, state column */
		if (isWithState()) {
			col = new TableColumn(table, SWT.LEFT);
			col.setText(TestPlanEditor.colName[TestPlanEditor.INDEX_COL_STATE]);
			col.setWidth(80);
		}

		/* Create table viewer, and add content and label provider */
		TableViewer viewer = new TableViewer(table);
		BladeIdContentProvider bcip = new BladeIdContentProvider(bClass);
		viewer.setContentProvider(bcip);
		viewer.setLabelProvider(new BladeLabelProvider());
		viewer.setInput(editorInput);
		if (selectionChangedListener != null) {
			viewer.addSelectionChangedListener(selectionChangedListener);
		}
		tables.put(bClass, viewer);

		tabBladeClass.setControl(table);

		/* If the new tab is the only tab in this TabOrder,
				 * the tab is selected */
		if (this.getItemCount() == 1) {
			/* Show the first tab */
			this.setSelection(0);
		}

		table.addListener(SWT.EraseItem, new Listener() {
			public void handleEvent(Event event) {
				if ((event.detail & SWT.SELECTED) == 0) return; /* item not selected */

				int clientWidth = table.getClientArea().width;
				GC gc = event.gc;
				gc.fillRectangle(0, event.y, clientWidth, event.height);

				try {
					List<String> serversList = Arrays.asList(ClifConsolePlugin.getDefault().getRegistry(true).getServers());
					int itemIndex = table.getSelectionIndex();
					TableItem item = table.getItem(itemIndex);
					String serverName = item.getText(TestPlanEditor.INDEX_COL_SERVER);

					if (serversList.indexOf(serverName) == -1) {
						gc.setForeground(getDisplay().getSystemColor(SWT.COLOR_RED));
					} else {
						gc.setForeground(getDisplay().getSystemColor(SWT.COLOR_LIST_SELECTION_TEXT));
					}
				}
				catch (ClifException ex) {
					ex.printStackTrace(System.err);
					gc.setForeground(getDisplay().getSystemColor(SWT.COLOR_RED));
				}
				event.detail &= ~SWT.SELECTED;
			}
		});


	}

	/**
	 * Remove the tab that display <i>bClass</i> blades.
	 *
	 * @param bClass the blade class
	 */
	private void removeTab(String bClass) {
		/* Remove table viewer from tables list */
		tables.remove(bClass);

		/* Remove tab from TabOrder */
		CTabItem[] tabs = this.getItems();
		for (int i = 0; i < tabs.length; i++) {
			if (tabs[i].getText().equals(bClass)) {
				tabs[i].dispose();
				break;
			}
		}
	}

	/**
	 * Remove all tabs.
	 */
	public void removeAllTab() {
		/* Remove table viewer from tables list */
		Object[] bClasses = tables.keySet().toArray();
		for (int i = 0; i < bClasses.length; i++) {
			tables.remove(bClasses[i]);
		}

		/* Remove tab from TabOrder */
		CTabItem[] tabs = this.getItems();
		for (int i = 0; i < tabs.length; i++) {
			tabs[i].dispose();
		}
	}

	/**
	 * Return true if check box is diplayed in the first column.
	 *
	 * @return true if check box is diplayed in the first column
	 */
	public boolean isWithCheck() {
		return withCheck;
	}

	/**
	 * Return true if state column is diplayed as the last column.
	 *
	 * @return true if state column is diplayed as the last column
	 */
	public boolean isWithState() {
		return withState;
	}

	/**
	 * Refresh all table viewer in each tabs.
	 */
	public void refresh() {
		Iterator<String> it = tables.keySet().iterator();
		while (it.hasNext()) {
			String bClass = it.next();
			refresh(bClass);

			if (tables.get(bClass).getTable().getItemCount() == 0) {
				removeTab(bClass);
				it = tables.keySet().iterator();
			}
		}
	}

	/**
	 * Refresh <i>bClass</i> table viewer.
	 *
	 * @param bClass blade class name
	 */
	private void refresh(String bClass) {
		TableViewer viewer = (TableViewer) tables.get(bClass);

		if (selectionChangedListener != null) {
			viewer.removeSelectionChangedListener(selectionChangedListener);
			viewer.refresh();
			viewer.addSelectionChangedListener(selectionChangedListener);
		} else {
			viewer.refresh();
		}
	}

	/* Methods for execution test. */

	/**
	 * If withCheck, select all blades in every tabs.
	 */
	public void selectAll() {
		if (isWithCheck()) {
			Iterator<TableViewer> it = tables.values().iterator();
			while (it.hasNext()) {
				TableViewer viewer = it.next();
				Table table = viewer.getTable();
				for (int i = 0; i < table.getItemCount(); i++) {
					table.getItem(i).setChecked(true);
				}
			}
		}
	}

	/**
	 * If withCheck, deselect all blades in every tabs.
	 */
	public void deselectAll() {
		if (isWithCheck()) {
			Iterator<TableViewer> it = tables.values().iterator();
			while (it.hasNext()) {
				TableViewer viewer = it.next();
				Table table = viewer.getTable();
				for (int i = 0; i < table.getItemCount(); i++) {
					table.getItem(i).setChecked(false);
				}
			}
		}
	}

	/**
	 * Return a new test plan with the selected blades.<br/>
	 * Return all test plan if not with ckeck.
	 *
	 * @return the test plan with the selected blades
	 */
	public String[] selectedTestPlan() {
		Map<String, ClifDeployDefinition> selectedTestPlan = new HashMap<String, ClifDeployDefinition>();
		Iterator<TableViewer> it = tables.values().iterator();
		while (it.hasNext()) {
			TableViewer viewer = it.next();
			Table table = viewer.getTable();
			for (int i = 0; i < table.getItemCount(); i++) {
				/* If blade id is checked, add to the selected blades test plan */
				if (table.getItem(i).getChecked()) {
					String id = table.getItem(i).getText(TestPlanEditor.INDEX_COL_ID);
					selectedTestPlan.put(id, testPlan.get(id));
				}
			}
		}
		if (selectedTestPlan.isEmpty()) {
			return null;
		}
		String[] selectedTestPlanId = new String[selectedTestPlan.size()];
		selectedTestPlanId = (String[]) selectedTestPlan.keySet().toArray(selectedTestPlanId);

		return selectedTestPlanId;
	}

	/**
	 * Return id of the selected blade
	 *
	 * @return String id of the blade
	 */
	public String getSelectedBlade() {
		String name = this.getSelection().getText();
		TableViewer viewer = (TableViewer) tables.get(name);

		StructuredSelection s = (StructuredSelection) viewer.getSelection();
		return (String) s.getFirstElement();
	}

	public List<String> getCheckedBlades() {
		List<String> checkedBlades = new ArrayList<String>();
		if (isWithCheck()) {
			Iterator<TableViewer> it = tables.values().iterator();
			while (it.hasNext()) {
				TableViewer viewer = (TableViewer) it.next();
				Table table = viewer.getTable();
				for (int i = 0; i < table.getItemCount(); i++) {
					if (table.getItem(i).getChecked()) {
						checkedBlades.add(table.getItem(i).getText());
					}
				}
			}
		}
		return checkedBlades;
	}

	/**
	 * Select the right blade in table.
	 *
	 * @param bladeId			  blade's id to select
	 * @param removeSelectListener true if a selectlistener has been assigned
	 *                             to this blade in viewer needed to avoid recurrent setSelection
	 */
	public void setSelection(String bladeId, boolean removeSelectListener) {
		ClifDeployDefinition def = testPlan.get(bladeId);
		String bClass = def.getClassName();

		TableViewer viewer = (TableViewer) tables.get(bClass);
		refresh(bClass);

		/* Select new blade */
		CTabItem[] tabs = getItems();
		for (int i = 0; i < tabs.length; i++) {
			if (tabs[i].getText().equals(bClass))
				setSelection(tabs[i]);

		}

		if (removeSelectListener) {
			viewer.removeSelectionChangedListener(selectionChangedListener);
			viewer.setSelection(new StructuredSelection(String.valueOf(bladeId)), true);
			viewer.addSelectionChangedListener(selectionChangedListener);
		} else viewer.setSelection(new StructuredSelection(String.valueOf(bladeId)), true);
	}


	/**
	 * Init blade state to UNDEPLOYED state.
	 */
	public void initBladeStates() {
		if (isWithState()) {
			bladeStates = new HashMap<String, BladeState>();
			for (String s : testPlan.keySet()) {
				bladeStates.put(s, BladeState.UNDEPLOYED);
			}
			refresh();
		}
	}

	/**
	 * Modify the state of the id blade.
	 *
	 * @param id	the id blade
	 * @param state the new state
	 */
	public void setBladeState(String id, BladeState state) {
		if (isWithState()) {
			bladeStates.put(id, state);
			refresh();
		}
	}

	/**
	 * Return the global state of all selected blades.
	 *
	 * @return the global state of all selected blades
	 */
	public BladeState globalState() {
		if (isWithState()) {
			return BladeState.getGlobalState(bladeStates.values());
		} else {
			return BladeState.UNDEPLOYED;
		}
	}


	/* Methods for test plan edition. */

	/**
	 * Add an injector or a probe in the opened tab (with the right blade class).
	 * If test plan is empty, a cpu probe is added (so a cpu tab).
	 *
	 * @return Map the new test plan
	 */
	public Map<String, ClifDeployDefinition> addBlade() {
		this.removeSelectionListener(selectionListener);
		String newId = getNewId();

		String bClass = "";
		String serverName = ExecutionContext.DEFAULT_SERVER;
		boolean isProbe = true;
		Map<String, String> context = new HashMap<String, String>();

		context.put("insert", "org.ow2.clif.probe."
				+ bClass + ".Insert");
		context.put("datacollector", "org.ow2.clif.probe."
				+ bClass + ".DataCollector");

		/* Add the new blade definition to the test plan */
		testPlan.put(newId,
				new ClifDeployDefinition(serverName,
						"org.ow2.clif.server.lib.Blade",
						context, "", "", isProbe));

		if (!tables.containsKey(bClass)) {
			addTab(bClass);
		}

		/* Select new blade */
		TableViewer viewer = (TableViewer) tables.get(bClass);
		refresh(bClass);
		setSelection(newId, true);
		viewer.setSelection(new StructuredSelection(String.valueOf(newId)));

		this.addSelectionListener(selectionListener);
		return testPlan;
	}

	/**
	 * Modify the specified blade with these new properties.
	 * Adapt tabs display.
	 *
	 * @param bladeId	id of the modify blade
	 * @param newBladeId new blade id (can be the same as bladeId)
	 * @param serverName the CLIF server name
	 * @param bClass	 the blade class (not empty)
	 * @param argument   the blade definition argument
	 * @param comment	the blade definition comment
	 * @param isProbe	true if probe, else injector
	 * @return the new blade id
	 * @throws ExistingBladeIdException	if new blade id already exists
	 * @throws BadBladePropertiesException if id, role or class are not defined
	 */
	public Map<String, ClifDeployDefinition> modifyBlade(String bladeId, String newBladeId, String serverName, String bClass, String argument, String comment, boolean isProbe)
			throws ExistingBladeIdException, BadBladePropertiesException {
		this.removeSelectionListener(selectionListener);
		/* Check if new blade id is valid (if changed id already exists) */
		if (!bladeId.equals(newBladeId)) {
			if (testPlan.get(newBladeId) != null) {
				throw new ExistingBladeIdException(newBladeId);
			}
		}

		/* Get blade actual definition */
		ClifDeployDefinition def = testPlan.get(bladeId);

		/* Get old blade class */
		String oldBladeClass = def.getClassName();

		/* Delete the old blade definition from the test plan */
		testPlan.remove(bladeId);

		/*Change blade properties in the test plan */
		Map<String, String> context = new HashMap<String, String>();
		if (isProbe) {
			context.put("insert", "org.ow2.clif.probe."
					+ bClass + ".Insert");
			context.put("datacollector", "org.ow2.clif.probe."
					+ bClass + ".DataCollector");
		} else {
			context.put("insert", bClass);
			context.put("datacollector",
					"org.ow2.clif.datacollector.lib.InjectorDataCollector");
		}

		/* Add the new blade definition to the test plan */
		testPlan.put(newBladeId,
				new ClifDeployDefinition(serverName,
						"org.ow2.clif.server.lib.Blade",
						context, argument, comment, isProbe));

		String tabBladeClass = isProbe ? bClass : "injector";

		/* If blade class change, tabs could change */
		if (!oldBladeClass.equals(tabBladeClass)) {
			TableViewer viewer = (TableViewer) tables.get(oldBladeClass);
			int count = viewer.getTable().getItemCount();

			/* If the blade is the only one of this class, the class tab is removed */
			if (count == 1) {
				removeTab(oldBladeClass);
			} else {
				refresh(oldBladeClass);
			}
			/* If there is not blade of this new class, new class tab is added */
			if (tables.get(tabBladeClass) == null) {
				addTab(tabBladeClass);
			}
		}

		setSelection(newBladeId, bladeId.equals(newBladeId));

		this.addSelectionListener(selectionListener);
		return testPlan;
	}


	/**
	 * Remove the selected blade in the opened tab.
	 *
	 * @return Map the new test plan
	 */
	public Map<String, ClifDeployDefinition> removeSelectedBlade() {
		if (testPlan.size() != 0) {
			String bClass = this.getSelection().getText();
			TableViewer viewer = (TableViewer) tables.get(bClass);
			IStructuredSelection selection = (IStructuredSelection) viewer.getSelection();
			if (!selection.isEmpty()) {
				String removedBladeId = selection.getFirstElement().toString();
				testPlan.remove(removedBladeId);
				int count = viewer.getTable().getItemCount();

				/* If the blade is the only one of this class,
								 * the class tab is removed */
				if (count == 1) {
					removeTab(bClass);
				} else {
					refresh(bClass);
				}
			}
		}
		return testPlan;
	}

	/**
	 * Remove all blades in the test plan.
	 *
	 * @return Map the new test plan
	 */
	public Map<String, ClifDeployDefinition> removeAllBlades() {
		/* Remove from test plan */
		Object[] ids = testPlan.keySet().toArray();
		for (int i = 0; i < ids.length; i++) {
			testPlan.remove(ids[i]);
		}
		/* Remove from TabOrder */
		CTabItem[] bClasses = this.getItems();
		for (int i = 0; i < bClasses.length; i++) {
			/* Remove table viewer from tables list */
			tables.remove(bClasses[i].getText());

			/* Remove tab from TabOrder */
			bClasses[i].dispose();
		}

		return testPlan;
	}

	/**
	 * Get test plan
	 *
	 * @return Map the test plan
	 */
	public Map<String, ClifDeployDefinition> getTestPlan() {
		return testPlan;
	}


	/**
	 * Set test plan
	 *
	 * @param testPlan The testPlan to set.
	 */
	public void setTestPlan(Map<String, ClifDeployDefinition> testPlan) {
		this.testPlan = testPlan;
	}

	/**
	 * Test if args are correct
	 *
	 * @return boolean true if args are correct
	 */
	public boolean isTestPlanCorrect() {
		Iterator<ClifDeployDefinition> iter = testPlan.values().iterator();
		while (iter.hasNext()) {
			ClifDeployDefinition def = iter.next();
			if (def.getServerName() == null || def.getServerName().equals("")
					|| def.getClassName() == null || def.getClassName().equals("")) {
				return false;
			}
		}
		return true;
	}

	/* Create new id */

	private String getNewId() {
		String newId = String.valueOf(blade_id++);
		while (testPlan.get(newId) != null) {
			newId = String.valueOf(blade_id++);
		}
		return newId;
	}
}

