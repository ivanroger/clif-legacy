/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * CLIF $Name: not supported by cvs2svn $
 *
 * Contact: clif@ow2.org
 */

package org.ow2.clif.console.lib.egui.editor;

import java.util.HashMap;
import java.util.Map;
import java.util.Observable;
import org.ow2.clif.console.lib.ClifDeployDefinition;

/**
 * Test plan observed by different views (TreeView and Editor).
 * Each action done on treeview or editor is set in this class 
 * and notified to all observers.
 *  
 * @author Joan Chaumont
 */
public class TestPlanObservable extends Observable {

    private Map<String,ClifDeployDefinition> testPlan;
    private String selectId;
    
    /**
     * Simple construtor.
     */
    public TestPlanObservable () {
        testPlan = new HashMap<String, ClifDeployDefinition>();
        selectId = "";
    }
    
    /**
     * Construtor with a testplan
     * @param testPlan the test plan observable by different views
     */
    public TestPlanObservable(Map<String, ClifDeployDefinition> testPlan) {
        this.testPlan = testPlan;
    }
    
    /**
     * Get the observable test plan
     * @return Returns the testPlan.
     */
    public Map<String, ClifDeployDefinition> getTestPlan() {
        return testPlan;
    }
    
    /**
     * Set observable test plan
     * @param testPlan The testPlan to set.
     */
    public void setTestPlan(Map<String, ClifDeployDefinition> testPlan) {
        this.testPlan = testPlan;
        changeAndNotify(null);
    }
    
    /**
     *  Removes the mapping for this key from this test plan
     *  and notify observers
     * @param key key whose mapping is to be removed from the map.
     */
    public void remove(String key) {
        testPlan.remove(key);
        changeAndNotify(null);
    }
    
    /**
     *  Associates the specified value with the specified key in this testplan
     *  and notify observers
     * @param newId id with which the specified clif def is to be associated.
     * @param definition definition to be associated with the specified key.
     */
    public void put(String newId, ClifDeployDefinition definition) {
        testPlan.put(newId, definition);
        changeAndNotify(null);
    }
    
    /**
     * Get selected id in the test plan
     * @return Returns the selectId.
     */
    public String getSelectId() {
        return selectId;
    }
    
    /**
     * Set selected of this test plan
     * @param selectId The selectId to set.
     */
    public void setSelectId(String selectId) {
        this.selectId = selectId;
        changeAndNotify(selectId);
    }
    
    private void changeAndNotify(String o) {
        setChanged();
        super.notifyObservers(o);
    }
    
}
