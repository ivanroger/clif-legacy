/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2009 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * CLIF $Name: not supported by cvs2svn $
 *
 * Contact: clif@ow2.org
 */

package org.ow2.clif.console.lib.egui.wizards;

import org.eclipse.core.resources.IContainer;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.Path;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.*;
import org.eclipse.ui.dialogs.ContainerSelectionDialog;

import java.io.File;

/**
 * Unique page for report wizard. Used to open the swing gui.
 * @author Anthonin Bonnefoy
 */
public class LaunchSwingAnalyzePage extends WizardPage {

	private ISelection selection;
	private Text reportDirectoryText;

	/**
	 * Constructor for LaunchSwingAnalyzePage
	 * @param selection
	 */
	public LaunchSwingAnalyzePage(ISelection selection) {
		super("wizardPage");
		setTitle("Open Gui Report");
		setDescription("Choose project report to open.");
		this.selection = selection;
	}

	/**
	 * Creates controls
	 */
	public void createControl(Composite parent) {
		Composite container = new Composite(parent, SWT.NULL);
		GridLayout layout = new GridLayout();
		container.setLayout(layout);
		layout.numColumns = 3;
		layout.verticalSpacing = 9;
		layout.horizontalSpacing = 10;

		// Container
		Label label = new Label(container, SWT.NULL);
		label.setText("&Report directory:");
		reportDirectoryText = new Text(container, SWT.BORDER | SWT.SINGLE);
		GridData gd = new GridData(GridData.FILL_HORIZONTAL);
		reportDirectoryText.setLayoutData(gd);
		reportDirectoryText.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent e) {
				dialogChanged();
			}
		});
		Button button = new Button(container, SWT.PUSH);
		button.setText("Browse...");
		button.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				handleBrowse();
			}
		});

		initialize();
		setControl(container);
	}

	/**
	 * Initializes fields.
	 */
	private void initialize() {
		if (selection != null && !selection.isEmpty() && selection instanceof IStructuredSelection) {
			IStructuredSelection ssel = (IStructuredSelection) selection;
			if (ssel.size() > 1) return;
			Object obj = ssel.getFirstElement();
			if (obj instanceof IResource) {
				IContainer container;
				if (obj instanceof IContainer)
					container = (IContainer) obj;
				else
					container = ((IResource) obj).getParent();
				reportDirectoryText.setText(container.getFullPath().toString().replace('/',File.separatorChar ) + File.separatorChar + "report");
			}
		}
		dialogChanged();
	}

	/**
	 * Uses the standard container selection dialog to
	 * choose the new value for the container field.
	 */
	private void handleBrowse() {
		ContainerSelectionDialog dialog =
				new ContainerSelectionDialog(
						getShell(),
						ResourcesPlugin.getWorkspace().getRoot(),
						false,
						"Select new report directory");
		if (dialog.open() == ContainerSelectionDialog.OK) {
			Object[] result = dialog.getResult();
			if (result.length == 1) {
				reportDirectoryText.setText(((Path) result[0]).toOSString());
			}
		}
	}

	/**
	 * Ensures that both text fields are set and valid.
	 * Container name is valid if it exists.
	 */
	private void dialogChanged() {
		String containerName = getReportDirectoryName();
		if (containerName.length() == 0) {
			updateStatus("Directory must be specified");
			return;
		}
		//Test if container exist
		IWorkspaceRoot root = ResourcesPlugin.getWorkspace().getRoot();
		IResource resource = root.findMember(new Path(containerName));

		if (resource == null || !resource.exists() || !(resource instanceof IContainer)) {
			updateStatus("Invalid directory");
			return;
		}
		updateStatus(null);
	}

	/**
	 * Show error message and allow "Finish" action or not.
	 * @param message the message to display if error
	 */
	private void updateStatus(String message) {
		setErrorMessage(message);
		setPageComplete(message == null);
	}

	/**
	 * Return the container name.
	 * @return the container name
	 */
	public String getReportDirectoryName() {
		return reportDirectoryText.getText();
	}

}