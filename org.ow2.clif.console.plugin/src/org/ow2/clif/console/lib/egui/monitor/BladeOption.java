/*
 * CLIF is a Load Injection Framework
 * Copyright 2005 Manuel Azema, Tsirimiaina Andrianavonimiarina Jaona
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * CLIF $Name: not supported by cvs2svn $
 *
 * Contact: clif@ow2.org
 */

package org.ow2.clif.console.lib.egui.monitor;

import java.util.Date;

/**
 * This class represents blade's options
 * 
 * @author Tsirimiaina ANDRIANAVONIMIARINA JAONA
 * @author Joan Chaumont
 */
public class BladeOption {
    
    /* true/false if checkBox is checked/unchecked */
    private boolean booldisplay;
    private boolean boolcollect;
    private String bladeId;
    
    /* Used for time settings */
    private Date begin;
    private long current;
    
    /**
     * Constructor.
     * @param display
     * @param collect
     * @param bladeId
     */
    public BladeOption(boolean display, boolean collect, String bladeId){
        this.booldisplay = display;
        this.boolcollect = collect;
        this.bladeId = bladeId;
        this.begin = null;
        current = 0;
    }
    
    /**
     * Get the blade id
     * @return String blade id
     */
    public String getBladeId() {
        return bladeId;
    }
    
    /**
     * Set blade id
     * @param bladeId the new blade id
     */
    public void setBladeId(String bladeId) {
        this.bladeId = bladeId;
    }
    
    /**
     * Test if the blade must be collected
     * @return boolean true if blade must be collected
     */
    public boolean isBoolcollect() {
        return boolcollect;
    }
    
    /**     
     * Set if the blade must be collected
     * @param boolcollect
     */
    public void setBoolcollect(boolean boolcollect) {
        this.boolcollect = boolcollect;
    }
    
    /**
     * Test if the blade must be displayed
     * @return boolean true if blade must be display
     */
    public boolean isBooldisplay() {
        return booldisplay;
    }
    
    /**
     * Set if the blade must be displayed
     * @param booldisplay
     */
    public void setBooldisplay(boolean booldisplay) {
        this.booldisplay = booldisplay;
    }
    
    /**
     * Begin the timer for these blade
     */
    public void beginTimer () {
        begin = new Date();
    }
    
    /**
     * Get ellapsed time for this blade
     * @return String the ellapsed time
     */
    public String getEllapsedTime() {
        long milisecond;
        if(begin != null) {
            Date end = new Date();
            milisecond = end.getTime()-begin.getTime() + current;
        }
        else {
            milisecond = current;
        }
        
        return FormatTime.getTime(milisecond);
    }
    
    /**
     * Suspend the timer for these blade
     */
    public void suspendTimer () {
        if(begin == null) {
            return;
        }
        Date end = new Date();
        current = end.getTime()-begin.getTime() + current;
        begin = null;
    }
    
    public String toString(){
        return "[Display: "+ isBoolcollect() + " ; " +
        "Collect: " + isBooldisplay() + " ; " +
        "BladeId: "+ getBladeId() +"]";
    }
}
