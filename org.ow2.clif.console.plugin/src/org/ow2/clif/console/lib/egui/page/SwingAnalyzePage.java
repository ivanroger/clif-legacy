package org.ow2.clif.console.lib.egui.page;

import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.part.ViewPart;
import org.ow2.clif.analyze.lib.graph.AnalyzerImpl;


/**
 * Page for swing analyzer component
 */
public class SwingAnalyzePage extends ViewPart {
//	private Frame container;


	@Override
	public void createPartControl(Composite parent) {
//		Composite swtAwtComponent = new Composite(parent, SWT.EMBEDDED);
//		container = SWT_AWT.new_Frame(swtAwtComponent);
	}

	@Override
	public void setFocus() {
	}

	public void load(String reportDirectoryName) {
		System.setProperty("clif.filestorage.dir", reportDirectoryName.substring(1));
		try {
			AnalyzerImpl.init(false);
            AnalyzerImpl.frame.setVisible(true);
            AnalyzerImpl.frame.toFront();
//			container.add(AnalyzerImpl.quickAnalyzer);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
