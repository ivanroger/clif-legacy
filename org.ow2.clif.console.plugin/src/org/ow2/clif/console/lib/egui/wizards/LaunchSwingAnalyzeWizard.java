/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2009 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * CLIF $Name: not supported by cvs2svn $
 *
 * Contact: clif@ow2.org
 */

package org.ow2.clif.console.lib.egui.wizards;

import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.Path;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.ui.*;
import org.ow2.clif.console.lib.egui.ClifConsolePerspective;
import org.ow2.clif.console.lib.egui.page.SwingAnalyzePage;

/**
 * Main class for Report Wizard.<br />
 * Used to start the swing gui with the report chosen.
 *
 * @author Anthonin Bonnefoy
 */
public class LaunchSwingAnalyzeWizard extends Wizard implements INewWizard {

	private LaunchSwingAnalyzePage mainPage;
	private ISelection selection;
	private IWorkbenchWindow window;

	/**
	 * Constructor
	 */
	public LaunchSwingAnalyzeWizard() {
		super();
		setNeedsProgressMonitor(true);
		setHelpAvailable(false);
		setWindowTitle("Open Swing Report");
	}

	/**
	 * Adding pages to the wizard.
	 */
	public void addPages() {
		mainPage = new LaunchSwingAnalyzePage(selection);
		addPage(mainPage);
	}

	/**
	 * Checks if directory exist.
	 */
	public boolean performFinish() {
		SwingAnalyzePage swingAnalyzePage;
		IWorkspaceRoot root = ResourcesPlugin.getWorkspace().getRoot();
		IResource resource = root.findMember(new Path(mainPage.getReportDirectoryName()));
		if (resource == null || !resource.exists() || !(resource instanceof IResource)) {
			MessageDialog.openError(
					getShell(),
					"CLIF ISAC Plug_in",
					"Directory \"" + mainPage.getReportDirectoryName() + "\" does not exist.");
			return false;
		}

		try {
			IWorkbenchPage activePage = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();
			swingAnalyzePage = (SwingAnalyzePage) activePage.findView(ClifConsolePerspective.ID_SWING_VIEW);
			if (swingAnalyzePage == null) {
				activePage.showView(ClifConsolePerspective.ID_SWING_VIEW);
				swingAnalyzePage = (SwingAnalyzePage) activePage.findView(ClifConsolePerspective.ID_SWING_VIEW);				
			}
		} catch (PartInitException e) {
			e.printStackTrace();
			return false;
		}
		swingAnalyzePage.load(mainPage.getReportDirectoryName());
		return true;
	}

	public void init(IWorkbench workbench, IStructuredSelection selection) {
		this.selection = selection;
		this.window = workbench.getActiveWorkbenchWindow();
	}
}
