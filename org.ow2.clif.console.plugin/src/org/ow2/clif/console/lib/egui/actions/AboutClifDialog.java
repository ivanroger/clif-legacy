/*
 * CLIF is a Load Injection Framework
 * Copyright 2005 Manuel Azema, Tsirimiaina Andrianavonimiarina Jaona
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * CLIF $Name: not supported by cvs2svn $
 *
 * Contact: clif@ow2.org
 */

package org.ow2.clif.console.lib.egui.actions;

import org.eclipse.jface.action.IAction;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.ui.INewWizard;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.IWorkbenchWindowActionDelegate;
import org.ow2.clif.util.Version;
import org.ow2.clif.console.lib.egui.ClifConsolePlugin;

/**
 * Create an information dialog about CLIF.
 * 
 * @author Manuel AZEMA
 */
public class AboutClifDialog implements IWorkbenchWindowActionDelegate {
    
    class AboutWizard extends Wizard implements INewWizard {
        
        private AboutWizardPage page;
        
        /**
         * Wizard constructor
         */
        public AboutWizard() {
            super();
            setWindowTitle("About Clif...");
            setDefaultPageImageDescriptor(ClifConsolePlugin.imageDescriptorFromPlugin(
                    ClifConsolePlugin.PLUGIN_ID,"icons/logo_clif_100px.gif"));
        }
        
        public boolean performFinish() {
            return true;
        }
        
        public void init(IWorkbench workbench, IStructuredSelection selection) {}
        
        public void addPages() {
            page = new AboutWizardPage();
            addPage(page);
        }
    }
    
    class AboutWizardPage extends WizardPage {
        
        protected AboutWizardPage() {
            super("wizardCLIFPage");
            setTitle("CLIF console Plugin for Eclipse");
            setDescription(Version.getVersion());
        }
        
        public void createControl(Composite parent) {
            Label label = new Label(parent,SWT.NULL);
            label.setText("Clif is a Load Injection Framework - http://clif.ow2.org/");
            setControl(parent);
        }
    }
    
    /**
     * Window object to provide parent shell for the message dialog.
     */
    private IWorkbenchWindow window;
    
    /**
     * The constructor.
     */
    public AboutClifDialog() {
    }
    
    /**
     * Open a dialog with some informations about CLIF.
     * 
     * @param action the action proxy that handles the presentation portion
     * 		of the action
     */
    public void run(IAction action) {
        WizardDialog ab = new WizardDialog(window.getShell(), new AboutWizard());
        ab.setPageSize(10,10);
        ab.open();
    }
    
    /**
     * @see  org.eclipse.ui.IActionDelegate#selectionChanged(org.eclipse.jface.action.IAction, org.eclipse.jface.viewers.ISelection)
     */
    public void selectionChanged(IAction action, ISelection selection) {
    }
    
    /**
     * @see IWorkbenchWindowActionDelegate#dispose()
     */
    public void dispose() {
    }
    
    /**
     * Save window object in order to
     * be able to provide parent shell for the message dialog.
     * 
     * @see IWorkbenchWindowActionDelegate#init(org.eclipse.ui.IWorkbenchWindow)
     */
    public void init(IWorkbenchWindow window) {
        this.window = window;
    }
}
