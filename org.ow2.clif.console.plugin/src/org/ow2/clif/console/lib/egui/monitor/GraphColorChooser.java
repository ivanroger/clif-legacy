/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * CLIF $Name: not supported by cvs2svn $
 *
 * Contact: clif@ow2.org
 */

package org.ow2.clif.console.lib.egui.monitor;

import org.eclipse.swt.SWT;

/**
 * Color provider (provides an array of relevant colors)
 * @author Joan Chaumont
 * @author Bruno Dillenseger
 */
public class GraphColorChooser {
    
    /** Array of colors*/
    public static int[] COLORS = {
    	SWT.COLOR_BLUE,
    	SWT.COLOR_RED, 
        SWT.COLOR_BLACK,
        SWT.COLOR_CYAN,
    	SWT.COLOR_DARK_GREEN,
        SWT.COLOR_DARK_YELLOW,
        SWT.COLOR_DARK_GRAY,
        SWT.COLOR_DARK_MAGENTA,
        SWT.COLOR_DARK_RED,
        SWT.COLOR_DARK_BLUE,
        SWT.COLOR_DARK_CYAN
   };
}
