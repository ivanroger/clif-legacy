/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2009, 2010 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * Contact: clif@ow2.org
 */

package org.ow2.clif.console.lib.egui.wizards.clifProject;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IProjectDescription;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.IExecutableExtension;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.OperationCanceledException;
import org.eclipse.core.runtime.SubProgressMonitor;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.ui.INewWizard;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.actions.WorkspaceModifyOperation;
import org.eclipse.ui.dialogs.WizardNewProjectCreationPage;
import org.eclipse.ui.wizards.newresource.BasicNewProjectResourceWizard;
import org.ow2.clif.console.lib.egui.ClifConsolePlugin;


/**
 * Main class for New CLIF project Wizard. Used to create a CLIF 
 * project and to add CLIF properties
 * 
 * @author Florian Francheteau
 * @author Bruno Dillenseger
 */
public class NewClifProjectWizard extends Wizard implements INewWizard,
IExecutableExtension {

	/*
	 * Use the WizardNewProjectCreationPage, which is provided by the Eclipse
	 * framework.
	 */
	private WizardNewProjectCreationPage mainPage;
	private MainParametersPage mainParamPage;
	private AdvancedParametersPage advParamPage;
	private JVMTuningPage jvmTuningPage;
	private IsacTuningPage isacTuningPage;
	private NetworkPage configDeploymentPage;
	private CustomPage customPropsPage;

	private IConfigurationElement config;

	private IProject project;
	private IProjectDescription description;
	
	private File clifPropsLocal;

	/**
	 * Constructor
	 */
	public NewClifProjectWizard() {
		super();
		setWindowTitle("New CLIF Project");
	}

	/**
	 * Adds wizard pages. The project is created when the second page 
	 * is visible
	 */
	public void addPages() {
		mainPage = new WizardNewProjectCreationPage(
				"New CLIF Project");
		mainPage.setTitle("Create a new CLIF Project");
		mainPage.setDescription("Choose project location.");
		addPage(mainPage);

		mainParamPage = new MainParametersPage(){
			// need to override to react to changes on second page
			public void setVisible(boolean visible) {
				if (visible){
					try {
						createProject();
						mainParamPage.setProject(project);
						configDeploymentPage.setProject(project);
						initializeStatsReportText(project.getFullPath());
					} catch (InvocationTargetException e) {
						e.printStackTrace();
						performCancel();
					} catch (InterruptedException e) {	
						e.printStackTrace();
						performCancel();
					}
				}
				super.setVisible(visible);
			}
		};
		addPage(mainParamPage);
		configDeploymentPage = new NetworkPage();
		addPage(configDeploymentPage);
		advParamPage = new AdvancedParametersPage();
		addPage(advParamPage);
		jvmTuningPage = new JVMTuningPage();
		addPage(jvmTuningPage);
		isacTuningPage = new IsacTuningPage();
		addPage(isacTuningPage);
		customPropsPage = new CustomPage();
		addPage(customPropsPage);
	}

	/**
	 * Creates CLIF project without CLIF files, folders and properties
	 */
	private void createProject() throws InvocationTargetException, InterruptedException{
		project = mainPage.getProjectHandle();
		IPath locationPath = mainPage.getLocationPath();

		if (!project.isOpen()) {
			description = project.getWorkspace().newProjectDescription(project.getName());
			if (!mainPage.useDefaults()) {
				description.setLocation(locationPath);
			}
			WorkspaceModifyOperation op = new WorkspaceModifyOperation() {
				protected void execute(IProgressMonitor monitor)
				throws CoreException {
					monitor.beginTask("", 2000);
					project.create(description, new SubProgressMonitor(monitor, 1000));
					if (monitor.isCanceled()) {
						throw new OperationCanceledException();
					}
					project.open(IResource.BACKGROUND_REFRESH, new SubProgressMonitor(
							monitor, 1000));
					monitor.done();
				}
			};
			getContainer().run(true, true, op);
		}
	}

	
	/**
	 * Creates files, folders and write CLIF properties when user clicks
	 * on finish button.
	 */
	public boolean performFinish() {
		mainParamPage.getInteractionManager().setInitialized(false);
		if (project==null){
			try {
				createProject();
				mainParamPage.setProject(project);
				configDeploymentPage.setProject(project);
				mainParamPage.initializeStatsReportText(project.getFullPath());
			} catch (InvocationTargetException e) {
				e.printStackTrace();
				performCancel();
				return false;
			} catch (InterruptedException e) {	
				e.printStackTrace();
				performCancel();
				return false;
			}
		}
		createClifProperties();
		BasicNewProjectResourceWizard.updatePerspective(config);
		mainParamPage.getInteractionManager().apply();
		
		return true;
	}

	/**
	 * If project is created then, this methods delete the project when
	 * user clicks on Cancel button
	 */
	public boolean performCancel() {
		mainParamPage.getInteractionManager().setInitialized(false);
		try {
			if (mainPage.isPageComplete() && mainPage.getProjectHandle().isOpen()) {
				mainPage.getProjectHandle().delete(true, true, null);
			}
		}
		catch (CoreException e) {
			e.printStackTrace();
			return false;
		}
		return super.performCancel();
	}
	
	/**
	 * Creates clif.props file. One file is created in the project folder.
	 * The other is created on directory of commons resources in the workspace.
	 * This method does not complete clif.props files
	 */
	private void createClifProperties(){
		try {
			clifPropsLocal = new File(project.getLocation().append(ClifConsolePlugin.CLIF_PROPS).toOSString());
			clifPropsLocal.createNewFile();
		} catch (IOException e) {
			MessageDialog.openError(
				getShell(),
				"CLIF ISAC Plug_in",
				"Cannot create file " + ClifConsolePlugin.CLIF_PROPS);
		}
	}

	public void init(IWorkbench workbench, IStructuredSelection selection) {
		setNeedsProgressMonitor(true);
	}

	public void setInitializationData(IConfigurationElement config,
			String propertyName, Object data) throws CoreException {
	}
	
	/**
	 * Gets the created project
	 * @return created project
	 */
	public IProject getProject(){
		return project;
	}
}
