/*
* CLIF is a Load Injection Framework
* Copyright (C) 2005, 2008 France Telecom R&D
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
* CLIF $Name: not supported by cvs2svn $
*
* Contact: clif@objectweb.org
*/

package org.ow2.clif.console.lib.egui.monitor;

import java.text.DateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

/**
 * Format given time in millisecond in hour minute second
 * @author Joan Chaumont
 * @author Bruno Dillenseger
 *
 */
public class FormatTime {
    
    /**
     * Format given time in milisecond in hour minute second
     * @param milisecond
     * @return String time
     */
    public static String getTime(long milisecond) {
        Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("GMT")); // get the GMT calendar
        cal.clear(); // clear all fields
        
        cal.setTime(new Date(milisecond));
        String hour =
        	(cal.get(Calendar.HOUR) == 0)
        	? ""
        	: cal.get(Calendar.HOUR) + ":";
        if(hour.length() == 2)
        {
            hour = "0" + hour;
        }
        String minute =
        	(cal.get(Calendar.MINUTE) == 0 && hour.equals(""))
        	? ""
        	: cal.get(Calendar.MINUTE) + "'";
        if(minute.length() == 2)
        {
            minute = "0" + minute;
        }
        String second = cal.get(Calendar.SECOND) + "\"";
        if(second.length() == 2)
        {
            second = "0" + second;
        }
        return hour + minute + second;    }
    
    /**
     * Get real time Date
     * @return date in String
     */
    public static String getDate(){
        Calendar cal = Calendar.getInstance(TimeZone.getDefault(),Locale.US); // default TimeZONE
        cal.get(Calendar.DAY_OF_YEAR);
        return format(cal);
    }
    
    /**
     * Get real time Date
     * @param millis 
     * @return date in String
     */
    public static String getDate(long millis){
        Calendar cal = Calendar.getInstance(TimeZone.getDefault(),Locale.US); // default TimeZONE
        cal.clear();
        cal.setTime(new Date(millis));
        return format(cal); 
    }

    /**
     * @param cal
     * @return date formatted in String day+time 
     */
    private static String format(Calendar cal) {
        String day = DateFormat.getDateInstance(DateFormat.SHORT, Locale.getDefault()).format(cal.getTime());
        String time = DateFormat.getTimeInstance(DateFormat.MEDIUM, Locale.getDefault()).format(cal.getTime());
        return day + " " + time;
    }
    
}
