/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2009, 2010, 2011, 2013 France Telecom R&D
 * Copyright (C) 2016 Orange SA
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * Contact: clif@ow2.org
 */

package org.ow2.clif.console.lib.egui.wizards.clifProject;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.Properties;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.dialogs.ErrorDialog;
import org.eclipse.jface.dialogs.IInputValidator;
import org.eclipse.jface.dialogs.InputDialog;
import org.eclipse.jface.preference.PreferencePage;
import org.eclipse.jface.resource.ImageRegistry;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CCombo;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.FontData;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.DirectoryDialog;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.List;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.dialogs.ContainerSelectionDialog;
import org.ow2.clif.console.lib.egui.ClifConsolePlugin;
import org.ow2.clif.util.ConfigFileManager;
import org.ow2.clif.util.ExecutionContext;
import org.ow2.clif.util.Network;
import org.ow2.clif.util.StringHelper;

/**
 * Class used by wizard and properties pages. Since there are many common
 * features between wizard and properties pages, this class grouped them.
 * If offers the possibility of creating and initializing controls, checking
 * whether the pages are filled correctly, returning filled fields, 
 * writing CLIF properties files and applying changes.
 * 
 * @author Florian Francheteau
 * @author Bruno Dillenseger
 */
public class InteractionManager {

	private Shell parentShell;
	private IProject project;
	private WizardPage wizardPage;
	private PreferencePage prefPage;

	private IPath containerPath;
	private static boolean initialized=false;
	private static boolean applied=false;

	// main settings
	private static Text reportText;
	private Button browseReport;
	private static Text statsText;
	private Button browseStats;
	private static String initReport="report";
	private static String initStats="stats";
	private static List classes;
	private static String initClasses="";

	// Network settings
	private static String initNetworkAddress = "localhost";
	private static CCombo networkAddressCombo;
	private static Button remoteRegistryHost;
	private static Text registryHostText;
	private static String initRegistryHost = null;
	private static String initRegistryPort="1234";
	private static Text registryPortText;	
	private static String initCodeServerPort="1357";
	private static Text codeServerPortText;
	private static Button sharedCodeServer;
	private static boolean initSharedCodeServer = false;

	// Advanced settings
	private static Combo fileStorageCleanCombo;
	private static Text fileStorageDelayText;
	private static Text fileStorageMaxPendingText;
	private static String initFileStorageClean="";
	private static String initFileStorageDelay="";
	private static String initFileStorageMaxPending="";
	private static Button globalTimeBtn;
	private static boolean initGlobalTime;

	private static Text xmsText;
	private static Text xmxText;
	private static String initXms="";
	private static String initXmx="";

	private static Text isacThreadText;
	private static Text groupPeriodText;
	private static Text schedulerPeriodText;
	private static Text jobDelayText;
	private static String initIsacThread="";
	private static String initGroupPeriod="";
	private static String initSchedulerPeriod="";
	private static String initJobDelay="";

	private static List customProps;
	private static java.util.List<String> initCustomProps = new ArrayList<String>();

	/**
	 * Constructor for a wizard page
	 * @param page the wizard page
	 * @param project the project created
	 */
	public InteractionManager(WizardPage page, IProject project) {

		this.wizardPage = page;
		this.parentShell = page.getShell();
		this.project = project;
		applied=false;
	}

	/**
	 * Constructor for a properties page
	 * @param page the properties page
	 * @param project the project where user wants to check or change properties
	 */
	public InteractionManager(PreferencePage page, IProject project) {

		this.prefPage = page;
		this.parentShell = page.getShell();
		this.project = project;
		applied=false;
	}

	//------------------------------------------------------------MAIN CONFIGURATION

	/**
	 * Creates contents of main page
	 */
	public Composite createMainContents(Composite parent){
		Composite container = new Composite(parent, SWT.NULL);
		GridLayout layout = new GridLayout();
		container.setLayout(layout);
		container.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		layout.numColumns = 4;
		layout.verticalSpacing = 9;
		layout.horizontalSpacing = 10;

		//Report Label
		Label label = new Label(container, SWT.NULL);
		label.setText("Directory for &raw measures:");

		//Report Text
		reportText = new Text(container, SWT.BORDER | SWT.SINGLE);
		GridData gd = new GridData(GridData.FILL_HORIZONTAL);
		gd.horizontalSpan = 2;
		reportText.setLayoutData(gd);
		reportText.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent e) {
				mainDialogChanged();
			}
		});

		//Browse Button
		browseReport = new Button(container, SWT.PUSH);
		browseReport.setText("Browse...");
		browseReport.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				handleReportBrowse();
			}
		});

		//Stats Label
		label = new Label(container, SWT.NULL);
		label.setText("Directory for monitoring &statistics:");

		//Stats Text
		statsText = new Text(container, SWT.BORDER | SWT.SINGLE);
		gd = new GridData(GridData.FILL_HORIZONTAL);
		gd.horizontalSpan = 2;
		statsText.setLayoutData(gd);
		statsText.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent e) {
				mainDialogChanged();
			}
		});

		//Browse Button
		browseStats = new Button(container, SWT.PUSH);
		browseStats.setText("Browse...");
		browseStats.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				handleStatsBrowse();
			}
		});

		// code server path settings

		label = new Label(container, SWT.NONE);
		label.setText("Code server path");

		// get images for buttons
		ImageRegistry imageRegistry = new ImageRegistry();
		imageRegistry.put("addproject", ClifConsolePlugin.imageDescriptorFromPlugin(
			ClifConsolePlugin.PLUGIN_ID,"icons/addproject.png"));
		imageRegistry.put("addexternal", ClifConsolePlugin.imageDescriptorFromPlugin(
			ClifConsolePlugin.PLUGIN_ID,"icons/addexternal.png"));
		imageRegistry.put("delete", ClifConsolePlugin.imageDescriptorFromPlugin(
			ClifConsolePlugin.PLUGIN_ID,"icons/delete.gif"));

		/* Composite with 2 columns :
		 * path : Composite with 2 buttons + list*/
		Composite field3 = new Composite(container, SWT.NULL);
		field3.setLayout(new GridLayout(2, false));

		/* Composite with 2 buttons */
		Composite buttons = new Composite(field3, SWT.NULL);
		FillLayout bLayout = new FillLayout(SWT.VERTICAL);
		buttons.setLayout(bLayout);

		Button addProject = new Button(buttons, SWT.PUSH);
		addProject.setImage(imageRegistry.get("addproject"));
		addProject.setToolTipText("Add project path");
		addProject.addListener(SWT.Selection, new Listener(){
			public void handleEvent(Event event) {

				ContainerSelectionDialog dialog = 
					new ContainerSelectionDialog(parentShell, null, false, "Select folder to add to classpath.");

				dialog.showClosedProjects(false);
				dialog.open();
				Object [] array = dialog.getResult();

				if (dialog.getResult() != null) {
					for (int i = 0; i < array.length; i++)
					{
						classes.add(array[i].toString().substring(1));
					}
					mainDialogChanged();
				}
			}
		});

		Button addExternal = new Button(buttons, SWT.PUSH);
		addExternal.setImage(imageRegistry.get("addexternal"));
		addExternal.setToolTipText("Add external path");
		addExternal.addListener(SWT.Selection, new Listener(){
			public void handleEvent(Event event) {
				String nomFichier;
				DirectoryDialog dialog = new DirectoryDialog(parentShell, 
						SWT.OPEN);
				nomFichier = dialog.open();
				if ((nomFichier != null) && (nomFichier.length() != 0)){       
					classes.add(nomFichier);
					mainDialogChanged();
				}
			}
		});

		Button remove = new Button(buttons, SWT.PUSH);
		remove.setImage(imageRegistry.get("delete"));
		remove.setToolTipText("Remove path");
		remove.addListener(SWT.Selection, new Listener(){
			public void handleEvent(Event event) {
				classes.remove(classes.getSelectionIndices());
				mainDialogChanged();
			}
		});

		Button up = new Button(buttons, SWT.ARROW | SWT.UP);
		up.setToolTipText("Move path up");
		up.addListener(SWT.Selection, new Listener(){
			public void handleEvent(Event event) {

				if (classes.getSelectionCount() > 0) {
					String name = classes.getSelection()[0];
					int index = classes.getSelectionIndex();

					if (index > 0) {
						classes.remove(index);
						classes.add(name, index - 1);
						classes.setSelection(index - 1);
					}
				}
			}
		});

		Button down = new Button(buttons, SWT.ARROW | SWT.DOWN);
		down.setToolTipText("Move path down");
		down.addListener(SWT.Selection, new Listener(){
			public void handleEvent(Event event) {
				if (classes.getSelectionCount() > 0) {
					String name = classes.getSelection()[0];
					int index = classes.getSelectionIndex();
					if (index < classes.getItemCount() - 1) {
						classes.remove(index);
						classes.add(name, index + 1);
						classes.setSelection(index + 1);
					}
				}
			}
		});

		classes = new List(field3, SWT.BORDER | SWT.SINGLE | SWT.V_SCROLL | SWT.H_SCROLL |SWT.FILL);
		gd = new GridData(SWT.FILL,SWT.FILL,true,true);
		classes.setLayoutData(gd);    

		return container;
	}

	/**
	 * Initializes contents of main page
	 */
	public void initializeMainContents()
	{
		// directory for saving raw measures
		Path initReportPath=new Path(initReport);
		String report = initReportPath.toOSString();
		if (report.startsWith("//"))
		{
			report = report.substring(1);
		}
		reportText.setText(report);

		// directory for saving monitoring statistics
		Path initStatsPath=new Path(initStats);
		String stats = initStatsPath.toOSString();
		if (stats.startsWith("//"))
		{
			stats = stats.substring(1);
		}
		statsText.setText(stats);

		// code server path
		classes.removeAll();
		String[] paths = initClasses.replaceAll("\"","").split(";");
		for (String pathElement : paths)
		{
			classes.add(pathElement);
		}
		mainDialogChanged();
	}

	/**
	 * Uses the standard container selection dialog to choose 
	 * the new value for the container field. 
	 */
	private void handleReportBrowse() {
		ContainerSelectionDialog dialog = new ContainerSelectionDialog(
				parentShell, project.getWorkspace().getRoot(), false,
		"Select new report directory container");
		if (dialog.open() == ContainerSelectionDialog.OK) {
			Object[] result = dialog.getResult();
			if (result.length == 1) {
				containerPath=((Path) result[0]);
				reportText.setText(containerPath.toOSString().substring(1));
			}
		}
	}

	/**
	 * Uses the standard container selection dialog to choose 
	 * the new value for the container field. 
	 */
	private void handleStatsBrowse() {
		ContainerSelectionDialog dialog = new ContainerSelectionDialog(
				parentShell, project.getWorkspace().getRoot(), false,
		"Select new stats directory container");
		if (dialog.open() == ContainerSelectionDialog.OK) {
			Object[] result = dialog.getResult();
			if (result.length == 1) {
				containerPath=((Path) result[0]);
				statsText.setText(containerPath.toOSString().substring(1));
			}
		}
	}

	/**
	 * Ensures that both main text fields are correctly filled. 
	 */
	private void mainDialogChanged() {
		//Ensures that report directory name is correct
		if (getReportDir().length() == 0) {
			updateStatus("Report directory must be specified");
			return;
		}else if (getReportDir().startsWith("//")) {
			updateStatus("Report directory must be relative");
			return;
		}
		//Ensures that statistics directory name is correct
		if (getStatsDir().length() == 0) {
			updateStatus("Stats directory must be specified");
			return;
		}else if (getStatsDir().startsWith("//")) {
			updateStatus("Stats directory must be relative");
			return;
		}
		//Ensures that class path is correct 
		if (classes.getItemCount()<=0) {
			updateStatus("At least one path element should be specified");
			return;
		}
		updateStatus(null);
	}
	
	/**
	 * Initialize values for report and stats directories 
	 * @param projectPath path of the project containing those values
	 */
	public void initializeStatsReportText(IPath projectPath){
		initReport="report";
		initStats="stats";
		String report = projectPath.append(initReport).toOSString().substring(1);
		String stats = projectPath.append(initStats).toOSString().substring(1);
		reportText.setText(report);
		statsText.setText(stats);
	}

	public String getReportDir(){
		if (reportText!=null){
			if (!reportText.isDisposed()){
				return reportText.getText();
			}
		}
		return initReport;
	}
	
	public String getStatsDir(){
		if (statsText!=null){
			if (!statsText.isDisposed()){
				return statsText.getText();		
			}
		}
		return initStats;
	}

	/**
	 * Get the code server class path
	 * @return the list of paths separated by ;
	 */
	private String getCodeServerPath()
	{
		String result = initClasses;
		if (classes != null && !classes.isDisposed())
		{
			String[] paths = classes.getItems();
			StringBuilder pathSB = new StringBuilder();
			for (String pathElement : paths)
			{
				pathSB.append(pathElement).append(';');
			}
			if (pathSB.length() > 0)
			{
				pathSB.deleteCharAt(pathSB.length() - 1);
			}
			result = pathSB.toString();
		}
		return result;
	}


	/////////////////////////////////////
	// management of custom properties //
	/////////////////////////////////////


	/**
	 * Creates contents of custom properties page
	 * @return the custom properties page Composite
	 */
	public Composite createCustomContents(Composite parent)
	{
		Composite all = new Composite(parent, SWT.NULL);
		all.setLayout(new GridLayout());
		all.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));

		ImageRegistry imageRegistry = new ImageRegistry();
		imageRegistry.put("addproperty", ClifConsolePlugin.imageDescriptorFromPlugin(
			ClifConsolePlugin.PLUGIN_ID,"icons/addproject.png"));
		imageRegistry.put("edit", ClifConsolePlugin.imageDescriptorFromPlugin(
			ClifConsolePlugin.PLUGIN_ID,"icons/rename.png"));
		imageRegistry.put("delete", ClifConsolePlugin.imageDescriptorFromPlugin(
			ClifConsolePlugin.PLUGIN_ID,"icons/delete.gif"));

		Label label = new Label(all, SWT.NONE);
		label.setText("Custom system properties");

		/* Composite with 2 columns : Composite with buttons + List */
		Composite field = new Composite(all, SWT.NULL);
		field.setLayout(new GridLayout(2, false));

		/* Composite with buttons */
		Composite buttons = new Composite(field, SWT.NULL);
		FillLayout bLayout = new FillLayout(SWT.VERTICAL);
		buttons.setLayout(bLayout);

		// button for adding a property
		Button addProp = new Button(buttons, SWT.PUSH);
		addProp.setImage(imageRegistry.get("addproperty"));
		addProp.setToolTipText("Add system property");
		addProp.addListener(SWT.Selection, new Listener() {
			public void handleEvent(Event event)
			{
				InputDialog dialog = new InputDialog(
					parentShell,
					"Custom Java system property",
					"property=value",
					"=",
					new IInputValidator() {
						public String isValid(String prop)
						{
							return prop.contains("=") ? null : "Please use this format: property=value";
						}
					});
				dialog.open();
				if (dialog.getReturnCode() == InputDialog.OK)
				{
					customProps.add(dialog.getValue());
				}
			}
		});

		// button for editing a property
		Button editProp = new Button(buttons, SWT.PUSH);
		editProp.setImage(imageRegistry.get("edit"));
		editProp.setToolTipText("Change property");
		editProp.addListener(SWT.Selection, new Listener() {
			public void handleEvent(Event event)
			{
				if (customProps.getSelectionCount() > 0)
				{
					int index = customProps.getSelectionIndex();
					InputDialog dialog = new InputDialog(
						parentShell,
						"Modify a custom system property",
						"property=value",
						customProps.getItem(index),
						new IInputValidator() {
							public String isValid(String prop)
							{
								return prop.contains("=") ? null : "Please use this format: property=value";
							}
						});
					dialog.open();
					if (dialog.getReturnCode() == InputDialog.OK)
					{
						customProps.setItem(index, dialog.getValue());
					}
				}
			}
		});

		// button for removing a property
		Button remove = new Button(buttons, SWT.PUSH);
		remove.setImage(imageRegistry.get("delete"));
		remove.setToolTipText("Remove system properties");
		remove.addListener(SWT.Selection, new Listener() {
			public void handleEvent(Event event)
			{
				customProps.remove(customProps.getSelectionIndices());
			}
		});

		// button for moving a property up in the list
		Button up = new Button(buttons, SWT.ARROW | SWT.UP);
		up.setToolTipText("Move property up");
		up.addListener(SWT.Selection, new Listener() {
			public void handleEvent(Event event)
			{
				if (customProps.getSelectionCount() > 0)
				{
					String name = customProps.getSelection()[0];
					int index = customProps.getSelectionIndex();

					if (index > 0) {
						customProps.remove(index);
						customProps.add(name, index - 1);
						customProps.setSelection(index - 1);
					}
				}
			}
		});

		// button for moving a property down in the list
		Button down = new Button(buttons, SWT.ARROW | SWT.DOWN);
		down.setToolTipText("Move property down");
		down.addListener(SWT.Selection, new Listener(){
			public void handleEvent(Event event)
			{
				if (customProps.getSelectionCount() > 0)
				{
					String name = customProps.getSelection()[0];
					int index = customProps.getSelectionIndex();
					if (index < customProps.getItemCount() - 1)
					{
						customProps.remove(index);
						customProps.add(name, index + 1);
						customProps.setSelection(index + 1);
					}
				}
			}
		});

		// list of properties
		customProps = new List(field, SWT.BORDER | SWT.SINGLE | SWT.V_SCROLL | SWT.H_SCROLL |SWT.FILL);
		GridData gd = new GridData(SWT.FILL,SWT.FILL,true,true);
		customProps.setLayoutData(gd);
		customProps.addMouseListener(new MouseAdapter() {
			public void mouseDoubleClick(MouseEvent event)
			{
				if (customProps.getSelectionCount() > 0)
				{
					int index = customProps.getSelectionIndex();
					InputDialog dialog = new InputDialog(
						parentShell,
						"Modify a custom system property",
						"property=value",
						customProps.getItem(index),
						new IInputValidator() {
							public String isValid(String prop)
							{
								return prop.contains("=") ? null : "Please use this format: property=value";
							}
						});
					dialog.open();
					if (dialog.getReturnCode() == InputDialog.OK)
					{
						customProps.setItem(index, dialog.getValue());
					}
				}
			}
		});
		return(all);
	}

	/**
	 * Initializes contents of custom properties page
	 */
	public void initializeCustom()
	{
		for (String prop : initCustomProps)
		{
			customProps.add(prop);
		}
	}

	/**
	 * @return list of system properties in the form prop=value
	 */
	public String[] getCustomProperties()
	{
		if (customProps != null && ! customProps.isDisposed())
		{
			return customProps.getItems();
		}
		else
		{
			return initCustomProps.toArray(new String[initCustomProps.size()]);
		}
	}

	//----------------------------------------------------------ADVANCED CONFIGURATION

	/**
	 * Creates contents of advanced page
	 */
	public Composite createAdvancedContents(Composite parent){
		Composite container = new Composite(parent, SWT.NULL);
		GridLayout layout = new GridLayout();
		container.setLayout(layout);
		container.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		layout.numColumns = 2;
		layout.verticalSpacing = 9;
		layout.horizontalSpacing = 10;

		// File storage clean Label
		Label label = new Label(container, SWT.NULL);
		label.setText("File Storage &Cleaning policy:");

		// File storage clean Combo
		fileStorageCleanCombo = new Combo(container, SWT.READ_ONLY | SWT.BORDER);
		fileStorageCleanCombo.add("none");
		fileStorageCleanCombo.add("auto");
		GridData gd = new GridData(GridData.FILL_HORIZONTAL);
		fileStorageCleanCombo.setLayoutData(gd);

		// File storage delay Label
		label = new Label(container, SWT.NULL);
		label.setText("File Storage &Delay (s):");

		// File storage delay Text
		fileStorageDelayText = new Text(container, SWT.BORDER | SWT.SINGLE);
		gd = new GridData(GridData.FILL_HORIZONTAL);
		fileStorageDelayText.setLayoutData(gd);
		fileStorageDelayText.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent e) {
				advancedDialogChanged();
			}
		});

		// File storage max pending Label
		label = new Label(container, SWT.NULL);
		label.setText("File Storage &Maximum pending events:");

		// File storage max pending Text
		fileStorageMaxPendingText = new Text(container, SWT.BORDER | SWT.SINGLE);
		gd = new GridData(GridData.FILL_HORIZONTAL);
		fileStorageMaxPendingText.setLayoutData(gd);
		fileStorageMaxPendingText.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent e) {
				advancedDialogChanged();
			}
		});

		// global time switch label
		label = new Label(container, SWT.NULL);
		label.setText("Assuming &global time support (NTP or the like):");

		// global time switch check box
		globalTimeBtn = new Button(container, SWT.CHECK);
		gd = new GridData(GridData.BEGINNING);
		globalTimeBtn.setLayoutData(gd);
		globalTimeBtn.addSelectionListener(
			new SelectionListener()
			{
				public void widgetDefaultSelected(SelectionEvent evt)
				{
					widgetSelected(evt);
				}

				public void widgetSelected(SelectionEvent evt)
				{
					advancedDialogChanged();
				}
			});

		return (container);
	}

	/**
	 * Initializes contents of advanced page
	 */
	public void initializeAdvanced() {
		fileStorageDelayText.setText(initFileStorageDelay);
		fileStorageCleanCombo.setText(initFileStorageClean);
		fileStorageMaxPendingText.setText(initFileStorageMaxPending);
		globalTimeBtn.setSelection(initGlobalTime);
	}

	/**
	 * Ensures that both advanced text fields are correctly filled. 
	 */
	private void advancedDialogChanged() {
		String str = getFileStorageDelay();
		//Ensures that data collector delay host is correct 
		if (str.length() > 0){
			if (!verifyNumber(str, "File Storage write delay")){
				return;
			}
		}
		str = getFileStorageMaxPending();
		if (str.length() > 0){
			if (!verifyNumber(str, "File Storage maximum number of pending events")){
				return;
			}
		}
		updateStatus(null);
	}

	public String getFileStorageClean(){
		if (fileStorageCleanCombo != null){
			if (! fileStorageCleanCombo.isDisposed()){
				return fileStorageCleanCombo.getText();
			}
		}
		return initFileStorageClean;
	}

	public String getFileStorageDelay(){
		if (fileStorageDelayText!=null){
			if (!fileStorageDelayText.isDisposed()){
				return fileStorageDelayText.getText();
			}
		}
		return initFileStorageDelay;
	}

	public String getFileStorageMaxPending(){
		if (fileStorageMaxPendingText!=null){
			if (!fileStorageMaxPendingText.isDisposed()){
				return fileStorageMaxPendingText.getText();
			}
		}
		return initFileStorageMaxPending;
	}

	public String getGlobalTimeSupport()
	{
		if (globalTimeBtn != null
			&& ! globalTimeBtn.isDisposed())
		{
			return globalTimeBtn.getSelection() ? "true" : "false";
		}
		else
		{
			return initGlobalTime ? "true" : "false";
		}
	}

	//----------------------------------------------------------JVM CONFIGURATION

	/**
	 * Creates contents of JVM page
	 */
	public Composite createJVMContents(Composite parent){
		Composite container = new Composite(parent, SWT.NULL);
		GridLayout layout = new GridLayout();
		container.setLayout(layout);
		container.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		layout.numColumns = 2;
		layout.verticalSpacing = 9;
		layout.horizontalSpacing = 10;

		//Xms Label
		Label label = new Label(container, SWT.NULL);
		label.setText("Xm&s:");

		//Xms Text
		xmsText = new Text(container, SWT.BORDER | SWT.SINGLE);
		GridData gd = new GridData(GridData.BEGINNING);
		gd.widthHint=50;
		xmsText.setLayoutData(gd);
		xmsText.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent e) {
				jvmDialogChanged();
			}
		});

		//Xmx Label
		label = new Label(container, SWT.NULL);
		label.setText("Xm&x:");

		//Xmx Text
		xmxText = new Text(container, SWT.BORDER | SWT.SINGLE);
		gd = new GridData(GridData.BEGINNING);
		gd.widthHint=50;
		xmxText.setLayoutData(gd);
		xmxText.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent e) {
				jvmDialogChanged();
			}
		});
		return(container);
	}

	/**
	 * Initializes contents of JVM page
	 */
	public void initializeJVMParam() {
		xmsText.setText(initXms);
		xmxText.setText(initXmx);
	}

	/**
	 * Ensures that both JVM text fields are correctly filled. 
	 */
	private void jvmDialogChanged() {
		String xms = getXms();
		String xmx = getXmx();
		//Ensures that Xms is correct 
		if (xms.length() > 0){
			if (xms.length()<2){
				updateStatus("Xms value must be formated like xm or xk or xg where x is a number");
				return;
			}
			try {
				if ((!xms.endsWith("m"))&&(!xms.endsWith("k"))&&(!xms.endsWith("g"))){
					updateStatus("Xms value must be formated like xm or xk or xg where x is a number");
					return;
				}
				Integer.parseInt(xms.substring(0,xms.length()-1));
			}catch(NumberFormatException e){
				updateStatus("Xms value must be formated like xm or xk or xg where x is a number");
				return;
			}
		}
		//Ensures that Xmx is correct 
		if (xmx.length() > 0){
			if (xmx.length()<2){
				updateStatus("Xmx value must be formated like xm or xk or xg where x is a number");
				return;
			}
			try {
				Integer.parseInt(xmx.substring(0,xmx.length()-1));
				if ((!xmx.endsWith("m"))&&(!xmx.endsWith("k"))&&(!xmx.endsWith("g"))){
					updateStatus("Xmx value must be formated like xm or xk or xg where x is a number");
					return;
				}
			}catch(NumberFormatException e){
				updateStatus("Xmx value must be formated like xm or xk or xg where x is a number");
				return;
			}
			updateStatus(null);
		}
	}

	public String getXms(){
		if (xmsText!=null){
			if (!xmsText.isDisposed()){
				return xmsText.getText();
			}
		}
		return initXms;
	}
	public String getXmx(){
		if (xmxText!=null){
			if (!xmxText.isDisposed()){
				return xmxText.getText();
			}
		}
		return initXmx;
	}	

	//----------------------------------------------------------ISAC CONFIGURATION

	/**
	 * Creates contents of ISAC page
	 */
	public Composite createISACContents(Composite parent){
		Composite container = new Composite(parent, SWT.NULL);
		GridLayout layout = new GridLayout();
		container.setLayout(layout);
		container.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		layout.numColumns = 2;
		layout.verticalSpacing = 9;
		layout.horizontalSpacing = 10;

		//ISAC Thread Label
		Label label = new Label(container, SWT.NULL);
		label.setText("&Thread pool size");

		//ISAC Thread Text
		isacThreadText = new Text(container, SWT.BORDER | SWT.SINGLE);
		GridData gd = new GridData(SWT.BEGINNING);
		gd.widthHint=50;
		isacThreadText.setLayoutData(gd);
		isacThreadText.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent e) {
				isacDialogChanged();
			}
		});

		//Group period Label
		label = new Label(container, SWT.NULL);
		label.setText("&Group period");

		//Group period Text
		groupPeriodText = new Text(container, SWT.BORDER | SWT.SINGLE);
		gd = new GridData(SWT.BEGINNING);
		gd.widthHint=50;
		groupPeriodText.setLayoutData(gd);
		groupPeriodText.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent e) {
				isacDialogChanged();
			}
		});

		//Scheduler period Label
		label = new Label(container, SWT.NULL);
		label.setText("&Scheduler period");

		//Scheduler period Text
		schedulerPeriodText = new Text(container, SWT.BORDER | SWT.SINGLE);
		gd = new GridData(SWT.BEGINNING);
		gd.widthHint=50;
		schedulerPeriodText.setLayoutData(gd);
		schedulerPeriodText.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent e) {
				isacDialogChanged();
			}
		});

		//Job delay Label
		label = new Label(container, SWT.NULL);
		label.setText("&Job delay");

		//Job delay Text
		jobDelayText = new Text(container, SWT.BORDER | SWT.SINGLE);
		gd = new GridData(SWT.BEGINNING);
		gd.widthHint=50;
		jobDelayText.setLayoutData(gd);
		jobDelayText.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent e) {
				isacDialogChanged();
			}
		});

		return(container);
	}

	/**
	 * Initializes contents of ISAC page
	 */
	public void initializeISACParam() {
		isacThreadText.setText(initIsacThread);
		groupPeriodText.setText(initGroupPeriod);
		schedulerPeriodText.setText(initSchedulerPeriod);
		jobDelayText.setText(initJobDelay);
	}

	/**
	 * Ensures that both ISAC text fields are set. 
	 */
	private void isacDialogChanged() {
		String isacThread = getIsacThread();
		String groupPeriod = getGroupPeriod();
		String schedulerPeriod = getSchedulerPeriod();
		String jobDelay = getJobDelay();
		//Ensures that isacThread is correct 
		if (isacThread.length() > 0){
			if (!verifyNumber(isacThread, "Isac Thread")){
				return;
			}
		}
		if (groupPeriod.length() > 0){
			//Ensures that groupPeriod is correct 
			if (!verifyNumber(groupPeriod, "Group Period")){
				return;
			}
		}
		if (schedulerPeriod.length() > 0){
			//Ensures that schedulerPeriod is correct 
			if (!verifyNumber(schedulerPeriod, "Scheduler Period")){
				return;
			}
		}
		if (jobDelay.length() > 0){
			//Ensures that jobDelay is correct 
			if (!verifyNumber(jobDelay, "Job Delay")){
				return;
			}
		}
		updateStatus(null);
	}

	public String getIsacThread(){
		if (isacThreadText!=null){
			if (!isacThreadText.isDisposed()){
				return isacThreadText.getText();
			}
		}
		return initIsacThread;
	}

	public String getGroupPeriod(){
		if (groupPeriodText!=null){
			if (!groupPeriodText.isDisposed()){
				return groupPeriodText.getText();
			}
		}
		return initGroupPeriod;
	}

	public String getSchedulerPeriod(){
		if (schedulerPeriodText!=null){
			if (!schedulerPeriodText.isDisposed()){
				return schedulerPeriodText.getText();
			}
		}
		return initSchedulerPeriod;
	}

	public String getJobDelay(){
		if (jobDelayText!=null){
			if (!jobDelayText.isDisposed()){
				return jobDelayText.getText();
			}
		}
		return initJobDelay;
	}


	////////////////////////////////////
	// management of Network settings //
	////////////////////////////////////

	/**
	 * Creates contents of Network page
	 */
	public Composite createNetworkContents(Composite parent)
	{
		Composite all = new Composite(parent, SWT.NULL);
		all.setLayout(new GridLayout());
		all.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));

		/* Composite with 2 columns:
		 * Network address: Label + Combo list
		 * External registry option: Label + check box Button
		 * External registry address: Label + Text field
		 * Registry port number: Label + Text field
		 * Code server port number: Label + Text field */
		Composite field2 = new Composite(all, SWT.NULL);
		GridLayout layout2 = new GridLayout();
		field2.setLayout(layout2);
		layout2.numColumns = 2;
		layout2.verticalSpacing = 9;
		GridData gd = new GridData(GridData.FILL_HORIZONTAL);
		field2.setLayoutData(gd);

		// local network address choice
		Label label = new Label(field2, SWT.NULL);
		label.setText("Choose network address*:");
		networkAddressCombo = new CCombo(field2, SWT.BORDER | SWT.SINGLE);
		networkAddressCombo.setEditable(false);
		gd = new GridData(GridData.FILL_HORIZONTAL);
		networkAddressCombo.setLayoutData(gd);
		networkAddressCombo.addModifyListener(
			new ModifyListener() {
				public void modifyText(
						ModifyEvent e) {
					networkDialogChanged();
				}
			});

		// external registry settings
		label = new Label(field2, SWT.NULL);
		label.setText("Use remote registry");
		remoteRegistryHost = new Button(field2, SWT.CHECK);
		gd = new GridData(GridData.BEGINNING);
		remoteRegistryHost.setLayoutData(gd);
		remoteRegistryHost.addSelectionListener(
			new SelectionListener()
			{
				public void widgetDefaultSelected(SelectionEvent evt)
				{
					widgetSelected(evt);
				}

				public void widgetSelected(SelectionEvent evt)
				{
					registryHostText.setEnabled(remoteRegistryHost.getSelection());
					networkDialogChanged();
				}
			});

		label = new Label(field2, SWT.NULL);
		label.setText("Remote registry network address:");
		registryHostText = new Text(field2, SWT.BORDER | SWT.SINGLE);
		gd = new GridData(GridData.FILL_HORIZONTAL);
		registryHostText.setLayoutData(gd);
		registryHostText.addModifyListener(
			new ModifyListener() {
				public void modifyText(
						ModifyEvent e) {
					networkDialogChanged();
				}
			});

		// registry port number
		label = new Label(field2, SWT.NULL);
		label.setText("Registry port:");
		registryPortText = new Text(field2, SWT.BORDER | SWT.SINGLE);
		gd = new GridData(GridData.BEGINNING);
		gd.widthHint=50;
		registryPortText.setLayoutData(gd);
		registryPortText.addModifyListener(
			new ModifyListener() {
				public void modifyText(ModifyEvent e) {
					networkDialogChanged();
				}
			});

		// code server port number
		label = new Label(field2, SWT.NULL);
		label.setText("Code server port:");            
		codeServerPortText = new Text(field2, SWT.BORDER | SWT.SINGLE);
		gd = new GridData(GridData.BEGINNING);
		gd.widthHint=50;
		codeServerPortText.setLayoutData(gd);
		codeServerPortText.addModifyListener(
			new ModifyListener() {
				public void modifyText(
						ModifyEvent e) {
					networkDialogChanged();
				}
			});

		// shared code server switch
		label = new Label(field2, SWT.NULL);
		label.setText("Shared code server**");
		sharedCodeServer = new Button(field2, SWT.CHECK);
		gd = new GridData(GridData.BEGINNING);
		sharedCodeServer.setLayoutData(gd);

		// separator
		Label lSeparator = new Label(all, SWT.CENTER);
		lSeparator.setText("");
		Label line = new Label(all, SWT.SEPARATOR | SWT.HORIZONTAL);
		gd = new GridData(GridData.FILL_HORIZONTAL);
		line.setLayoutData(gd);

		// restart warning message
		label = new Label(all, SWT.NULL);
		FontData fd = label.getFont().getFontData()[0];
		label.setFont(new Font(parent.getDisplay(), fd.getName(), fd.getHeight(), SWT.ITALIC));
		label.setText(
			"* \"localhost\" is only valid for a test plan without remote CLIF server.\n"
			+ "Note: changing this value requires to quit and restart.");

		// shared code server information
		label = new Label(all, SWT.NULL);
			fd = label.getFont().getFontData()[0];
			label.setFont(new Font(parent.getDisplay(), fd.getName(), fd.getHeight(), SWT.ITALIC));
			label.setText(
				"** enables multiple simultaneous deployments, provided they have the same code server path.\n");

		return(all);
	}

	/**
	 * Initializes contents of Network page
	 */
	public void initializeNetwork() {
		codeServerPortText.setText(initCodeServerPort);
		registryPortText.setText(initRegistryPort);
		sharedCodeServer.setSelection(initSharedCodeServer);
		if (initRegistryHost == null)
		{
			remoteRegistryHost.setSelection(false);
			registryHostText.setEnabled(false);
			registryHostText.setText("");
		}
		else
		{
			remoteRegistryHost.setSelection(true);
			registryHostText.setEnabled(true);
			registryHostText.setText(initRegistryHost);
		}
		InetAddress[] addr = Network.getInetAddresses();
		int selectionIndex = 0;
		networkAddressCombo.removeAll();
		networkAddressCombo.add("localhost");
		networkAddressCombo.select(0);
		for (InetAddress addrElement : addr)
		{
			try
			{
				networkAddressCombo.add(
					addrElement.getHostAddress()
					+ " - "
					+ NetworkInterface.getByInetAddress(addrElement).getDisplayName());
				++selectionIndex;
				if (addrElement.getHostAddress().equals(initNetworkAddress))
				{
					networkAddressCombo.select(selectionIndex);
				}
			}
			catch (SocketException e)
			{
				e.printStackTrace(System.err);
			}
		}
		if(networkAddressCombo.getItemCount() == 0)
		{
			networkDialogChanged();
		}
	}

	/**
	 * Ensures that both text fields are correctly filled. 
	 */
	public void networkDialogChanged()
	{
		//Ensures that registry host is correct 
		if (getRegistryHost().length() == 0){
			updateStatus("Remote registry host must be specified or disabled");
			return;
		}
		//Ensures that registry port number is correct 
		if (getRegistryPort().length() == 0) {
			updateStatus("Registry port number must be specified");
			return;
		}
		if (!verifyPort(getRegistryPort(), "Registry Port")){
			return;
		}
		//Ensures that code server port number is correct 
		if (getCodeServerPort().length() == 0) {
			updateStatus("Code Server port number must be specified");
			return;
		}
		if (!verifyPort(getCodeServerPort(), "Code Server Port")){
			return;
		}
		updateStatus(null);
	}

	public String getRegistryHost()
	{
		if (remoteRegistryHost != null
			&& ! remoteRegistryHost.isDisposed()
			&& remoteRegistryHost.getSelection())
		{
			if (registryHostText != null && ! registryHostText.isDisposed())
			{
				return registryHostText.getText();
			}
			else
			{
				return initRegistryHost;
			}
		}
		else
		{
			return getNetworkAddress();
		}
	}

	public String getNetworkAddress()
	{
		if (networkAddressCombo != null &&  !networkAddressCombo.isDisposed())
		{
			return networkAddressCombo.getText().split(" ")[0];
		}
		else
		{
			return initNetworkAddress;
		}
	}

	public String getCodeServerPort() {
		if (codeServerPortText!=null){
			if (!codeServerPortText.isDisposed()){
				return codeServerPortText.getText();
			}
		}
		return initCodeServerPort;
	}

	public String getRegistryPort() {
		if (registryPortText!=null){
			if (!registryPortText.isDisposed()){
				return registryPortText.getText();
			}
		}
		return initRegistryPort;
	}

	public boolean getSharedCodeServer()
	{
		if (sharedCodeServer != null && !sharedCodeServer.isDisposed())
		{
			return sharedCodeServer.getSelection();
		}
		else
		{
			return initSharedCodeServer;
		}
	}
	

	//----------------------------------------------------------COMMON FEATURES	

	/**
	 * Initializes all properties by reading clif.opts or clif.props file.
	 * The file is either read from current project or from the plug-in's
	 * resources.
	 * Detailed process:
	 * <ol>
	 *   <li>try to load clif.opts from the project's directory</li>
	 *   <li>try to load clif.props from the project's directory</li>
	 *   <li>load clif.opts from the plug-in's resources</li>
	 * </ol>
	 */
	public void initialize()
	{
		File clifOptsFile = null;
		File clifPropsFile = null;
		if (project != null)
		{
			// project already exists (project properties, not wizard)
			clifOptsFile = project.getLocation().append(ClifConsolePlugin.CLIF_OPTS).toFile();
			clifPropsFile = project.getLocation().append(ClifConsolePlugin.CLIF_PROPS).toFile();
		}
		if (project == null
			|| !(initializeFromOptsFile(clifOptsFile) || initializeFromPropsFile(clifPropsFile)))
		{
			clifOptsFile = new Path(ExecutionContext.getBaseDir(), ExecutionContext.OPTS_PATH).toFile();
			initializeFromOptsFile(clifOptsFile);
		}
	}

	/**
	 * Try to load CLIF properties file according to clif.props format
	 * 
	 * @param clifPropsFile the file to load CLIF properties from
	 * @return true if properties could be loaded from the file, false otherwise
	 */
	private boolean initializeFromPropsFile(File clifPropsFile)
	{
		boolean done = false;
		if (clifPropsFile != null && clifPropsFile.canRead())
		{
			FileInputStream clifProps;
			Properties properties;
			try
			{
				clifProps = new FileInputStream(clifPropsFile);
				properties = new Properties();
				properties.load(clifProps);
				String allParam = (String) properties.get("clif.parameters");
				String[] parameters = allParam.split(" ");
				initCustomProps.clear();
				initRegistryHost = null;
				initSharedCodeServer = false;
				for (String parameter : parameters)
				{
					if (parameter.startsWith("-Dclif.filestorage.dir"))
					{
						String[] property = parameter.split("=");
						initReport = property[1].replace("\"", "");
					}
					else if (parameter.startsWith("-Dclif.eclipse.statdir"))
					{
						String[] property = parameter.split("=");
						initStats = property[1].replace("\"", "");
					}
					else if (parameter.startsWith("-Dclif.codeserver.path"))
					{
						String[] property = parameter.split("=");
						property[1] = property[1].replace("\"", "");
						initClasses = property[1];
					}
					else
					{
						initializeParameter(parameter);
					}
				}
				initNetworkAddress = ClifConsolePlugin.getDefault().getNetworkAddress();
				initialized = true;
				done = true;
			}
			catch (Exception ex)
			{
				ex.printStackTrace(System.err);
			}
		}
		return done;
	}


	/**
	 * Transforms a path relative to current project's directory
	 * to a path relative to the Eclipse workspace
	 * There are 3 cases:
	 * <ul>
	 *   <li>An absolute path remains unchanged;</li>
	 *   <li>For a relative path starting with "..", ".." is discarded;</li> 
	 *   <li>For a relative path not starting with "..", the project's name
	 *   is concatenated at the beginning of the path.</li>
	 * </ul>   
	 * @param pathStr String representation of the project-relative,
	 * or absolute path
	 * @return the workspace-relative path or the unchanged absolute path
	 */
	private String getPathPropertyFromOption(String pathStr)
	{
		Path path = new Path(pathStr);
		if (path.isAbsolute() || project == null)
		{
		}
		else if (path.segment(0).equals(".."))
		{
			pathStr = path.removeFirstSegments(1).append(project.getName()).toOSString();
		}
		else
		{
			pathStr = new Path(project.getName()).append(path).toOSString();
		}
		return pathStr;
	}

	/**
	 * Try to load CLIF options file according to clif.optsformat
	 * 
	 * @param clifOptsFile the file to load CLIF options from
	 * @return true if options could be loaded from the file, false otherwise
	 */
	private boolean initializeFromOptsFile(File clifOptsFile)
	{
		boolean done = false;
		if (clifOptsFile != null && clifOptsFile.canRead())
		{
			try
			{
				ConfigFileManager optsFileMgr = new ConfigFileManager(clifOptsFile);
				optsFileMgr.load(false);
				java.util.List<String> options = optsFileMgr.getOptions();
				initCustomProps.clear();
				initRegistryHost = null;
				initSharedCodeServer = false;
				for (String option : options)
				{
					if (option.startsWith("-Dclif.filestorage.dir"))
					{
						String[] property = option.split("=");
						initReport = getPathPropertyFromOption(property[1].replace("\"", ""));
					}
					else if (option.startsWith("-Dclif.eclipse.statdir"))
					{
						String[] property = option.split("=");
						initStats = getPathPropertyFromOption(property[1].replace("\"", ""));
					}
					else if (option.startsWith("-Dclif.codeserver.path"))
					{
						String[] property = option.split("=");
						String[] pathElements = property[1].replace("\"", "").split(";");
						StringBuilder pathProp = new StringBuilder();
						for (String path : pathElements)
						{
							pathProp.append(getPathPropertyFromOption(path)).append(';');
						}
						if (pathProp.length() > 0)
						{
							pathProp.deleteCharAt(pathProp.length() - 1);
						}
						initClasses = pathProp.toString();
					}
					else
					{
						initializeParameter(option);
					}
				}
				initNetworkAddress = ClifConsolePlugin.getDefault().getNetworkAddress();
				initialized = true;
				done = true;
			}
			catch (Exception ex)
			{
				ex.printStackTrace(System.err);
			}
		}
		return done;
	}

	private void initializeParameter(String parameter)
	{
		if (parameter.startsWith("-Dfractal.registry.host"))
		{
			String[] property = parameter.split("=");
			initRegistryHost = property[1];
		}
		else if (parameter.startsWith("-Dfractal.registry.port"))
		{
			String[] property = parameter.split("=");
			initRegistryPort = property[1];
		}
		else if (parameter.startsWith("-Dclif.codeserver.port"))
		{
			String[] property = parameter.split("=");
			initCodeServerPort = property[1];
		}
		else if (parameter.startsWith("-Dclif.codeserver.shared"))
		{
			initSharedCodeServer = StringHelper.isEnabled(parameter.split("=")[1]);
		}
		else if (parameter.startsWith("-Dclif.filestorage.clean"))
		{
			String[] property = parameter.split("=");
			initFileStorageClean = property[1];
		}
		else if (parameter.startsWith("-Dclif.filestorage.delay_s"))
		{
			String[] property = parameter.split("=");
			initFileStorageDelay = property[1];
		}
		else if (parameter.startsWith("-Dclif.filestorage.maxpending"))
		{
			String[] property = parameter.split("=");
			initFileStorageMaxPending = property[1];
		}
		else if (parameter.startsWith("-Dclif.globaltime"))
		{
			String[] property = parameter.split("=");
			initGlobalTime = StringHelper.isEnabled(property[1]);
		}
		else if (parameter.startsWith("-Xms"))
		{
			initXms = parameter.substring(4);
		}
		else if (parameter.startsWith("-Xmx"))
		{
			initXmx = parameter.substring(4);
		}
		else if (parameter.startsWith("-Dclif.isac.threads"))
		{
			String[] property = parameter.split("=");
			initIsacThread = property[1];
		}
		else if (parameter.startsWith("-Dclif.isac.groupperiod"))
		{
			String[] property = parameter.split("=");
			initGroupPeriod = property[1];
		}
		else if (parameter.startsWith("-Dclif.isac.schedulerperiod"))
		{
			String[] property = parameter.split("=");
			initSchedulerPeriod = property[1];
		}
		else if (parameter.startsWith("-Dclif.isac.jobdelay"))
		{
			String[] property = parameter.split("=");
			initJobDelay = property[1];
		}
		else if (parameter.startsWith("-D")
			&& !parameter.startsWith("-Djulia.")
			&& !parameter.startsWith("-Dfractal.provider")
			&& !parameter.startsWith("-Dclif.filestorage.host")
			&& !parameter.startsWith("-Dclif.codeserver.host")
			&& !parameter.startsWith("-Djonathan.connectionfactory.host"))
		{
			initCustomProps.add(parameter.substring("-D".length()));
		}
	}

	/**
	 * Applies all modifications on CLIF properties files. 
	 */
	public void apply()
	{
		if (! applied)
		{
			// directory for measures
			Path reportPath = new Path(getReportDir());
			File reportDir;
			if (reportPath.isAbsolute())
			{
				reportDir = reportPath.toFile();
			}
			else
			{
				reportDir = project.getWorkspace()
					.getRoot()
					.getProject(reportPath.segment(0))
					.getFolder(reportPath.removeFirstSegments(1))
					.getLocation()
					.toFile();
			}
			if (! reportDir.exists())
			{
				reportDir.mkdirs();
			}
			// directory for monitoring statistics
			Path statsPath = new Path(getStatsDir());
			File statsDir;
			if (statsPath.isAbsolute())
			{
				statsDir = statsPath.toFile();
			}
			else
			{
				statsDir = project.getWorkspace()
					.getRoot()
					.getProject(statsPath.segment(0))
					.getFolder(statsPath.removeFirstSegments(1))
					.getLocation()
					.toFile();
			}
			if (! statsDir.exists())
			{
				statsDir.mkdirs();
			}
			// set network address and save it for next Console start 
			ClifConsolePlugin.getDefault().setNetworkAddress(getNetworkAddress());
			// write clif.props file
			File clifPropsLocal = new File(project.getLocation().append(ClifConsolePlugin.CLIF_PROPS).toOSString());
			writeClifProperties(clifPropsLocal);
			// write clif.opts file
			File clifOptsLocal = new File(project.getLocation().append(ClifConsolePlugin.CLIF_OPTS).toOSString());
			writeClifOptions(clifOptsLocal);
			try
			{
				ClifConsolePlugin.getDefault().setProperties(clifPropsLocal);
		        project.refreshLocal(IProject.DEPTH_INFINITE, null);
	        }
			catch (Exception e)
			{
		        e.printStackTrace(System.err);
	        }
			applied = true;
		}
	}

	/**
	 * Write all properties in clif properties file by checking 
	 * all filled fields.
	 */
	private void writeClifProperties(File clifFile){
		BufferedWriter outputFile;
		String statDir=getStatsDir();
		statDir = statDir.replaceAll("\\\\","/");
		String reportDir=getReportDir();
		reportDir = reportDir.replaceAll("\\\\","/");
		try {
			outputFile = new BufferedWriter(new FileWriter(clifFile));
			outputFile.write("clif.parameters \\\n");
			outputFile.write("-Dclif.codeserver.path=" + getCodeServerPath() + " \\\n");
			outputFile.write("-Dclif.codeserver.port=" + getCodeServerPort() + " \\\n");
			outputFile.write("-Dclif.codeserver.shared=" + getSharedCodeServer() + " \\\n");
			outputFile.write("-Dclif.eclipse.statdir=" + statDir + " \\\n");
			if (getFileStorageClean().length()>0){
				outputFile.write("-Dclif.filestorage.clean=");
				outputFile.write((getFileStorageClean()+" \\\n"));
			}
			if (getFileStorageDelay().length()>0){
				outputFile.write("-Dclif.filestorage.delay_s=");
				outputFile.write((getFileStorageDelay()+" \\\n"));
			}
			outputFile.write("-Dclif.filestorage.dir=" + reportDir + " \\\n");
			if (getFileStorageMaxPending().length()>0){
				outputFile.write("-Dclif.filestorage.maxpending=");
				outputFile.write((getFileStorageMaxPending()+" \\\n"));
			}
			outputFile.write("-Dclif.globaltime=" + getGlobalTimeSupport() + " \\\n");
			if (getGroupPeriod().length()>0){
				outputFile.write("-Dclif.isac.groupperiod=");
				outputFile.write((getGroupPeriod()+" \\\n"));
			}
			if (getJobDelay().length()>0){
				outputFile.write("-Dclif.isac.jobdelay=");
				outputFile.write((getJobDelay()+" \\\n"));
			}
			if (getSchedulerPeriod().length()>0){
				outputFile.write("-Dclif.isac.schedulerperiod=");
				outputFile.write((getSchedulerPeriod()+" \\\n"));
			}
			if (getIsacThread().length()>0){
				outputFile.write("-Dclif.isac.threads=");
				outputFile.write((getIsacThread()+" \\\n"));
			}
			outputFile.write("-Dfractal.provider=org.objectweb.fractal.julia.Julia \\\n");
			if (! getRegistryHost().equals(getNetworkAddress()))
			{
				outputFile.write("-Dfractal.registry.host=" + getRegistryHost() + " \\\n");
			}
			outputFile.write("-Dfractal.registry.port=" + getRegistryPort() + " \\\n");
			outputFile.write("-Djulia.config=etc/julia.cfg \\\n");
			outputFile.write("-Djulia.loader=org.objectweb.fractal.julia.loader.DynamicLoader \\\n");
			for (String prop : getCustomProperties())
			{
				outputFile.write("-D" + prop + " \\\n");
			}
			if (getXms().length()>0){
				outputFile.write("-Xms");
				outputFile.write((getXms()+" "));
			}
			if (getXmx().length()>0){
				outputFile.write("-Xmx");
				outputFile.write((getXmx()+" \\\n"));
			}
			outputFile.write("-server");
			outputFile.close();			
		} catch (IOException e) {
			catchException(e);
		}
	}


	/**
	 * Transforms a path relative to the Eclipse workspace
	 * to a path relative to current project's directory.
	 * There are 3 cases:
	 * <ul>
	 *   <li>An absolute path remains unchanged;</li>
	 *   <li>For a relative path starting with current project's name,
	 *   the first path element (i.e. the project name) is replaced by .;</li> 
	 *   <li>For a relative path starting with another project's name,
	 *   ../ is concatenated at the beginning of the path.</li>
	 * </ul>   
	 * @param pathStr String representation of the workspace-relative,
	 * or absolute path
	 * @return the project-relative path or the unchanged absolute path
	 */
	private String getPathOptionFromProperty(String pathStr)
	{
		Path path = new Path(pathStr);
		if (path.isAbsolute())
		{
			pathStr = path.toPortableString();
		}
		else if (path.segment(0).equals(project.getName()))
		{
			pathStr = path.removeFirstSegments(1).toPortableString();
			if (pathStr.isEmpty())
			{
				pathStr = ".";
			}
		}
		else
		{
			pathStr = new Path("..").append(path).toPortableString();
		}
		return pathStr;
	}


	/**
	 * Write all properties to clif options file by checking 
	 * all filled fields.
	 */
	private void writeClifOptions(File clifOptFile)
	{
		BufferedWriter outputFile;
		String statDir = getPathOptionFromProperty(getStatsDir().replaceAll("\\\\","/"));
		String reportDir = getPathOptionFromProperty(getReportDir().replaceAll("\\\\","/"));
		StringBuilder codeServerPath = new StringBuilder();
		for (String path : getCodeServerPath().split(";"))
		{
			codeServerPath.append(getPathOptionFromProperty(path)).append(";");
		}
		codeServerPath.deleteCharAt(codeServerPath.length() - 1);
		try
		{
			outputFile = new BufferedWriter(new FileWriter(clifOptFile));
			outputFile.write("-Dclif.codeserver.path=" + codeServerPath.toString() + "\n");
			outputFile.write("-Dclif.codeserver.port=" + getCodeServerPort() + "\n");
			outputFile.write("-Dclif.codeserver.shared=" + getSharedCodeServer() + "\n");
			outputFile.write("-Dclif.eclipse.statdir=" + statDir + "\n");
			if (getFileStorageClean().length() > 0)
			{
				outputFile.write("-Dclif.filestorage.clean=");
				outputFile.write((getFileStorageClean() + "\n"));
			}
			if (getFileStorageDelay().length() > 0)
			{
				outputFile.write("-Dclif.filestorage.delay_s=");
				outputFile.write((getFileStorageDelay() + "\n"));
			}
			outputFile.write("-Dclif.filestorage.dir=" + reportDir + "\n");
			if (getFileStorageMaxPending().length() > 0)
			{
				outputFile.write("-Dclif.filestorage.maxpending=");
				outputFile.write((getFileStorageMaxPending() + "\n"));
			}
			outputFile.write("-Dclif.globaltime=" + getGlobalTimeSupport() + "\n");
			if (getGroupPeriod().length() > 0)
			{
				outputFile.write("-Dclif.isac.groupperiod=");
				outputFile.write((getGroupPeriod() + "\n"));
			}
			if (getJobDelay().length() > 0)
			{
				outputFile.write("-Dclif.isac.jobdelay=");
				outputFile.write((getJobDelay() + "\n"));
			}
			if (getSchedulerPeriod().length() > 0)
			{
				outputFile.write("-Dclif.isac.schedulerperiod=");
				outputFile.write((getSchedulerPeriod() + "\n"));
			}
			if (getIsacThread().length() > 0)
			{
				outputFile.write("-Dclif.isac.threads=");
				outputFile.write((getIsacThread() + "\n"));
			}
			if (! getRegistryHost().equals(getNetworkAddress()))
			{
				outputFile.write("-Dfractal.registry.host=" + getRegistryHost() + "\n");
			}
			outputFile.write("-Dfractal.registry.port=" + getRegistryPort() + "\n");
			for (String prop : getCustomProperties())
			{
				outputFile.write("-D" + prop + "\n");
			}
			if (getXms().length() > 0)
			{
				outputFile.write("-Xms");
				outputFile.write((getXms() + "\n"));
			}
			if (getXmx().length() > 0)
			{
				outputFile.write("-Xmx");
				outputFile.write((getXmx() + "\n"));
			}
			outputFile.write("-server");
			outputFile.close();			
		}
		catch (IOException e)
		{
			catchException(e);
		}
	}

	/**
	 * Verify if the argument is a valid port number
	 * @param number the argument to check
	 * @param label type of this argument
	 * @return boolean true if number is a valid port number
	 */
	private boolean verifyPort(String number, String label){
		if (number.length() == 0) {
			updateStatus(label+" number must be specified");
			return false;
		}
		try {
			int port = Integer.parseInt(number);
			if (port<0 || port>65536){
				updateStatus(label+" number must be valid");
				return false;
			}
		}
		catch(NumberFormatException e){
			updateStatus(label+" number must be an integer");
			return false;
		}
		return true;
	}
	
	/**
	 * Verify if the argument is a valid number
	 * @param number the argument to check
	 * @param label type of this argument
	 * @return true if number is valid
	 */
	private boolean verifyNumber(String number, String label){
		if (number.length() == 0) {
			updateStatus(label+" must be specified");
			return false;
		}
		try {
			Integer.parseInt(number);
		}
		catch(NumberFormatException e){
			updateStatus(label+" must be an integer");
			return false;
		}
		return true;

	}

	/** 
	 * Updates wizard status message
	 */ 
	private void updateStatus(String message) {
		boolean valid=true;
		if (message!=null){
			valid=false;
		}
		if (wizardPage!=null){
			wizardPage.setErrorMessage(message);
			wizardPage.setPageComplete(valid);
		}else if(prefPage!=null){
			prefPage.setErrorMessage(message);
			prefPage.setValid(valid);
		}
	}

	/**
	 * set Project and fill initClasses
	 * @param project
	 */
	public void setProject(IProject project){
		this.project=project;
		initClasses=project.getFullPath().toOSString().substring(1);
		classes.removeAll();
		classes.add(initClasses);
	}

	public boolean getInitialized(){
		return initialized;
	}

	public void setInitialized(boolean init){
		initialized=init;
	}

	/**
	 * Open a message error dialog to user, block plug-in activity
	 * in setting valid page to false and close the shell.
	 * @param e Exception to catch
	 */
	public void catchException(Exception e)
	{
		e.printStackTrace(System.err);
		Status status = new Status(Status.ERROR, 
				"CLIF Project",
				Status.OK,
				"Error during file manipulation.",
				e);
		ErrorDialog diag = new ErrorDialog(parentShell, 
				"Plugin error", 
				"Problem detects", 
				status, 
				IStatus.ERROR);	
		diag.open();
		parentShell.close();
	}
}
