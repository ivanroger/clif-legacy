/*
 * CLIF is a Load Injection Framework
 * Copyright 2005 Manuel Azema, Tsirimiaina Andrianavonimiarina Jaona
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * CLIF $Name: not supported by cvs2svn $
 *
 * Contact: clif@ow2.org
 */
package org.ow2.clif.console.lib.egui.monitor;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CTabFolder;
import org.eclipse.swt.custom.CTabItem;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.forms.widgets.ScrolledForm;
import org.eclipse.ui.part.ViewPart;
import org.ow2.clif.console.lib.ClifDeployDefinition;
import org.ow2.clif.deploy.ClifAppFacade;

/**
 * 
 * @author Tsirimiaina ANDRIANAVONIMIARINA JAONA
 * @author Joan Chaumont
 * @author Bruno Dillenseger
 */
public class MonitorView extends ViewPart {
    
    private FormToolkit toolkit;
    
    /* ScrolledForm where tabFolderPrincipal will be added. */
    private ScrolledForm form;
    
    /* used to sort execution by tabs */
    private CTabFolder tabFolderPrincipal; 
    
    /**
     * Constructor.
     */
    public MonitorView() {
        super();
    }
    
    /**
     * This callback allow us
     * to create the viewer and initialize it.
     */
    public void createPartControl(Composite parent) {
        toolkit = new FormToolkit(parent.getDisplay());
        form = toolkit.createScrolledForm(parent);
        
        GridLayout layout = new GridLayout(1, true);
        layout.verticalSpacing = 0;
        layout.marginHeight  = 0;
        layout.marginWidth   = 0;
        form.getBody().setLayout(layout);
        form.getBody().setLayoutData(new GridData(GridData.FILL_BOTH));
        
        /* TabFolder -> Each execution is a tabItem */
        tabFolderPrincipal = new CTabFolder(form.getBody(), SWT.NONE);
        GridLayout layout1 = new GridLayout(1, true);
        layout1.verticalSpacing = 0;
        layout1.marginHeight = 0;
        layout1.marginWidth = 0;
        tabFolderPrincipal.setLayout(layout1);
        tabFolderPrincipal.setLayoutData(new GridData(GridData.FILL_BOTH));
    }
    
    /**
     * @see org.eclipse.ui.part.WorkbenchPart#setFocus()
     */
    public void setFocus() { }

    /**
     * Adds a new execution tab. <p>
     * Each tab has an unique id
     * @param id id of the new tab
     * @return id of the new tab
     */
    private String addTab(String id){
        CTabItem tabItemExecution = new CTabItem(tabFolderPrincipal, SWT.CLOSE);
        tabItemExecution.setText(id);
        tabFolderPrincipal.setSelection(tabFolderPrincipal.getItems().length - 1);
        return id;
    }
    
    /**
     * Create a new execution tab
     * @param testPlan TestPlan which contains only selected blades
     * @param testId test id and new tab's name
     * @param clifApp the clif app used for this test
     */
    public void addExecutionTab(Map testPlan, String testId, ClifAppFacade clifApp){
        this.addTab(testId);
        /* Get list of class name
         * Each name will be a ctabItem */
        ArrayList listClass = bladesTabName(testPlan);
        
        /* Step 1 : create TabItemFolderPrincipal with its items 
         * and add it to last added item of tabFolderPrincipal*/ 
        CTabItem tabi = this.getLastAddedTabItemExec();
        TabItemFolderPrincipal tabif = new TabItemFolderPrincipal(tabFolderPrincipal, SWT.NONE, clifApp, testPlan);
        
        tabif.createTabsByClass(listClass); //add new CTabItems
        tabif.setSelection(0);
        
        /* Step2 : fill Tables of each TableGraphComposite */
        Iterator iter = testPlan.entrySet().iterator();
        while(iter.hasNext()) {
            /* Get id and class for each blade */
            Map.Entry entry = (Map.Entry) iter.next();
            String id = (String) entry.getKey();
            ClifDeployDefinition def = (ClifDeployDefinition)entry.getValue();
            String bClass = def.getClassName();
            tabif.fillCTabItem(id, bClass);
        }
        
        tabi.setControl(tabif);
    }
    
    /**
     * Returns the list of class name for a testplan. 
     * @param selectedTestPlan
     * @return an arrayList with ctabitems title
     */
    public ArrayList bladesTabName(Map selectedTestPlan){
        ArrayList listClass = new ArrayList();
        boolean injectOk = false;
        
        Iterator it = selectedTestPlan.entrySet().iterator();
        while(it.hasNext()) {
            /* Get id and class for each blade */
            Map.Entry entry = (Map.Entry)it.next();
            ClifDeployDefinition def = (ClifDeployDefinition)entry.getValue();
            String bClass = def.getClassName();
            if(!listClass.contains(bClass)){
                /* if def is probe, add class name */
                if(def.isProbe()){
                    listClass.add(bClass);
                }
                /* Set injectOk to true because injector 
                 * class is added only one time*/
                else if(!injectOk) {
                    listClass.add(bClass);
                    injectOk = true;
                }
            }
        } 
        return listClass;
    }
    
    /**
     * Gets last tabItemExec added
     * @return CTabItem the last added tab item
     */
    public CTabItem getLastAddedTabItemExec(){
        CTabItem[] tabs = tabFolderPrincipal.getItems();
        int last = tabs.length;
        if (tabs.length == 0){
            System.out.println("MonitorView getLastAddedTabItem() : NULL");
            return null;
        }
        return (CTabItem) tabs[last - 1];		
    }
}