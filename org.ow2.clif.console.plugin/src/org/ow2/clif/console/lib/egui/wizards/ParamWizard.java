/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2005, 2009 France Telecom R&D
 * Copyright (C) 2015 Orange
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * Contact: clif@ow2.org
 */

package org.ow2.clif.console.lib.egui.wizards;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.SortedMap;
import java.util.TreeMap;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.INewWizard;
import org.eclipse.ui.IWorkbench;
import org.ow2.clif.supervisor.api.ClifException;
import org.ow2.clif.console.lib.egui.ClifConsolePlugin;
import org.ow2.clif.deploy.ClifAppFacade;

/**
 * @author Joan Chaumont
 * @author Florian Francheteau
 */
public class ParamWizard extends Wizard implements INewWizard {
    
    /**
	*	The different parameters with their values to display
	*/
    private SortedMap<String,List<String>> parameters;
    
    /**
	*	Names and text fields for parameters which are displayed in text fields
	*/
    private Map<String,Text> parametersTextFields;
    
    /**
	*	Names and combos for parameters which are displayed in combo
	*/
    private Map<String,Combo> parametersComboFields;
    
    /**
	*	CLIF application facade
	*/
    private ClifAppFacade clifApp;
    
    /**
	*	a String composed of blades identifiers
	*/
    private String ids;
    
    /**
	*	List of blades identifiers
	*/
    private List<String> bladesIds;
    
    /**
	*	Wizard Page
	*/
    private ParamWizardPage page;
    
    /**
     * Construct parameters of Wizard Page. This constructor is used when the parameters of a 
     * single blade have to be displayed
     * 
     * @param params	Parameters (identifiers and values) of the selected blade
     * @param clifApp 	CLIF application facade
     * @param bladeId 	Identifier of the selected blade
     */
    public ParamWizard(Map<String,Serializable> params, ClifAppFacade clifApp, String bladeId) {
        super();
        
        parameters = new TreeMap<String,List<String>>();
        parametersTextFields = new HashMap<String,Text>();
        parametersComboFields = new HashMap<String,Combo>();
        
        for (Entry<String,Serializable> param : params.entrySet()) {
        	List<String> paramValue=new ArrayList<String>();
        	paramValue.add(param.getValue().toString());
            parameters.put(param.getKey(),paramValue);
        }

        this.clifApp = clifApp;
        this.ids = bladeId;
        
        bladesIds = new ArrayList<String>();
        bladesIds.add(ids);
        
        setWindowTitle("Change parameters of blade " + ids);
        setDefaultPageImageDescriptor(ClifConsolePlugin.imageDescriptorFromPlugin(
                ClifConsolePlugin.PLUGIN_ID,"icons/logo_clif_100px.gif"));
    }
    
    /**
     * Construct parameters of Wizard Page. This constructor is used when the parameters of several 
     * blades have to be displayed
     * 
     * @param params		Common parameters (identifiers and different values) of selected blades
     * @param clifApp 		CLIF application facade
     * @param bladesIds 	List of Identifier of selected blades
     * @param ids			String composed of blades identifiers
     */
    public ParamWizard(Map<String,List<String>> params, ClifAppFacade clifApp, List<String> bladesIds, String ids) {
        super();
        
        parameters.putAll(params);
        parametersTextFields = new HashMap<String,Text>();
        parametersComboFields = new HashMap<String,Combo>();

        this.clifApp = clifApp;
        this.bladesIds = bladesIds; 
        this.ids=ids;
        
        setWindowTitle("Change parameters of blades : " + ids);
        setDefaultPageImageDescriptor(ClifConsolePlugin.imageDescriptorFromPlugin(
                ClifConsolePlugin.PLUGIN_ID,"icons/logo_clif_100px.gif"));
    }
    
    class ParamWizardPage extends WizardPage {
        
        /**
         * Display a Parameters Wizard Page
         */
        protected ParamWizardPage() {
            super("paramWizard");
            setTitle("Change parameters of blade(s) : ");
            setDescription(ids);
        }
        
        public void createControl(Composite parent) {
        	Composite field2 =
                new Composite(parent, SWT.NULL);
            GridLayout layout2 = new GridLayout();
            field2.setLayout(layout2);
            layout2.numColumns = 2;
            layout2.verticalSpacing = 9;
            
            for (Entry<String,List<String>> paramType : parameters.entrySet()) {
                String name = (String) paramType.getKey();
                List<String> paramValues = paramType.getValue();
                
                Label label = new Label(field2,SWT.NULL);
                label.setText(name + " :");
                GridData gd = new GridData(GridData.FILL_HORIZONTAL);
                
                List <String> compareValues = new ArrayList<String>();
                Boolean equalValues=true;
                compareValues.add(paramValues.get(0));
                
                for (String paramValue : paramValues){
                	if (!compareValues.contains(paramValue)){
                		compareValues.add(paramValue);
                		equalValues=false;
                	}
                }
                /* if all values are the same, then a Text Field is displayed */
                if (equalValues){
               	 	Text text = new Text(field2, SWT.BORDER | SWT.SINGLE);
               	 	text.setText(compareValues.get(0));
               	 	text.setLayoutData(gd);
               	 	parametersTextFields.put(name,text);
               	/* if there are different values, then a Combo is displayed */	
                }else {
                    Combo combo = new Combo(field2, SWT.BORDER);
                 	combo.add("Keep current values",0);
                    for (String compareValue : compareValues){
                    	combo.add(compareValue);
                    }
                    combo.select(0);
                	combo.setLayoutData(gd);
                	parametersComboFields.put(name,combo);
                }   
            }
            setControl(parent);
        }
    }
    
    public void addPages() {
        page = new ParamWizardPage();
        addPage(page);
    }
    
    public boolean performFinish() {
    	/* collecting all parameters displayed in text fields */
    	for (Entry<String,Text> parameterTextField : parametersTextFields.entrySet()) {
            String name = parameterTextField.getKey();
            String value = parameterTextField.getValue().getText();
            /* changing values of selected blades */
            for (String bladeId : bladesIds){
            	try {
            		clifApp.changeParameter(bladeId, name, value);
            	}catch (ClifException e) {
            		page.setErrorMessage("Invalid Argument : " + name);
            		return false;
            	}
            }
        }
    	/* collecting all parameters displayed in combos */
    	for (Entry<String,Combo> parameterComboField : parametersComboFields.entrySet()) {
            String name = parameterComboField.getKey();
            String value = parameterComboField.getValue().getText();
            if (!value.equals("Keep current values")){
            	/* changing values of selected blades */
            	for (String bladeId : bladesIds){
            		try {
            			clifApp.changeParameter(bladeId, name, value);
            		}catch (ClifException e) {
            			page.setErrorMessage("Invalid Argument : " + name);
            			return false;
            		}
            	}
            }
        }
        return true;
    }    
    
    public void init(IWorkbench workbench, IStructuredSelection selection) {}
}
