/*
 * CLIF is a Load Injection Framework
 * Copyright 2005 Manuel Azema, Tsirimiaina Andrianavonimiarina Jaona
 * Copyright (C) 2009, 2010 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * Contact: clif@ow2.org
 */

package org.ow2.clif.console.lib.egui.editor;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jface.dialogs.IPageChangedListener;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.dialogs.MessageDialogWithToggle;
import org.eclipse.jface.dialogs.PageChangedEvent;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IFileEditorInput;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.dialogs.SaveAsDialog;
import org.eclipse.ui.forms.editor.FormEditor;
import org.eclipse.ui.forms.editor.FormPage;
import org.eclipse.ui.forms.editor.IFormPage;
import org.ow2.clif.console.lib.ClifDeployDefinition;
import org.ow2.clif.console.lib.TestPlanReader;
import org.ow2.clif.console.lib.TestPlanWriter;
import org.ow2.clif.console.lib.egui.ClifConsolePlugin;
import org.ow2.clif.supervisor.api.ClifException;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Map;

/**
 * Multi-Page editor for CLIF Test Plans.
 * This editor has 2 views:
 * <ul>
 * <li>page 0 : blades sort by class.
 * <li>page 1 : test commands.
 * </ul>
 * @author Manuel AZEMA
 * @author Florian Francheteau
 * @author Bruno Dillenseger
 */
public class TestPlanEditor extends FormEditor
{
    /**
     * Edition page use for editing a test plan
     */
    public static final int POS_PAGE_EDITION = 0;
    protected static final String ID_PAGE_EDITION = "testPlanEditor.pageEdition";
    protected TestPlanMasterEditPage editPage;

    /**
     * Test page used when running a test plan.
     */
    private static final int POS_PAGE_TEST = 1;
    private static final String ID_PAGE_TEST = "testPlanEditor.pageTest";
    private static TestPlanTestPage testPage;
    private static TestPlanEditor deployedTestPlanEditor;
    private static String deployedTest = null;

    public static final int INDEX_COL_ID = 0;
    public static final int INDEX_COL_SERVER = 1;
    public static final int INDEX_COL_ROLE = 2;
    public static final int INDEX_COL_CLASS = 3;
    public static final int INDEX_COL_ARGUMENT = 4;
    public static final int INDEX_COL_COMMENT = 5;
    public static final int INDEX_COL_STATE = 6;
    public static final String[] colName = {
            "Id", "Server", "Role", "Class", "Arguments", "Comment", "State"};

    /**
     * Sets the file name of the currently deployed test plan
     * @param testName file name of deployed test plan
     */
    public static void setDeployedTest(String testName)
    {
        deployedTest = testName;        
    }

    private IFile file;
    private File clifPropsFile;
    private Map<String, ClifDeployDefinition> testPlan;

    /**
     * True if test plan has been modified
     */
    private boolean isDirty;


    public TestPlanEditor() {
        super();
        isDirty = false;
        this.addPageChangedListener(new IPageChangedListener() {
            public void pageChanged(PageChangedEvent arg0) {
                Object page = arg0.getSelectedPage();
                if (page instanceof TestPlanTestPage) {
                    if (editPage.hasChanged()) {
                        testPage.setChanged();
                    }
                }
                if (page instanceof FormPage) {
                    editPage.setChanged(false);
                }
            }
        });
    }

    /**
     * Create a blades edition page sort by class.
     */
    public void createPageEdition() {
        Shell s = getEditorSite().getShell();

        try {
            /* Load test plan fileremoveBlade */
            IFileEditorInput ifei = (IFileEditorInput) getEditorInput();
            file = ifei.getFile();
            testPlan = TestPlanReader.readFromProp(file.getContents());
            clifPropsFile = new File(file.getProject().getLocation().append(ClifConsolePlugin.CLIF_PROPS).toOSString());

            /* Create page. */
            FormPage tp = new FormPage(this, ID_PAGE_EDITION, "Edit");
            tp.createPartControl(this.getContainer());

            /* Create and add editPage */
            editPage = new TestPlanMasterEditPage(tp, testPlan, clifPropsFile);
            addPage(tp);
            editPage.createContent(tp.getManagedForm());

        } catch (PartInitException e) {
            MessageDialog.openError(s, "Part init exception", e.getMessage());
        } catch (IOException e) {
            MessageDialog.openError(s, "I/O exception", e.getMessage());
        } catch (CoreException e) {
            MessageDialog.openError(s, "Core exception", e.getMessage());
        }
    }

    /**
     * Create a test plan execution commands.
     */
    public void createPageTest()
    	throws ClifException
    {
    	if (testPage != null)
    	{
    		if (! testPage.isIdle())
    		{
    			MessageDialogWithToggle confirm = MessageDialogWithToggle.openInformation(
    				getSite().getShell(),
    				"Hazardous deployment",
    				"A test plan is currently deployed and not idle.\nYou should stop it before deploying.",
    				"confirm hazardous deployment",
    				false,
    				null,
    				null);
    			if (! confirm.getToggleState())
    			{
    				throw new ClifException("canceled");
    			}
    		}
    		testPage.dispose();
    		testPage = null;
    		if (deployedTestPlanEditor.getPageCount() > POS_PAGE_TEST)
    		{
    			deployedTestPlanEditor.removePage(POS_PAGE_TEST);
    		}
    		deployedTestPlanEditor = null;
    	}
       	try
       	{
		   	testPage = new TestPlanTestPage(this, editPage.getTestPlan(), ID_PAGE_TEST, "Test");
		   	deployedTestPlanEditor = this;
		    addPage(testPage);
			setActivePage(POS_PAGE_TEST);
		}
       	catch (PartInitException e)
       	{
		    MessageDialog.openError(
		    	getEditorSite().getShell(),
		    	"Could not create Test tab for deployed test plan " + deployedTest,
		    	e.getMessage());
		}
    }


    ////////////////////////
    // FormEditor methods //
    ////////////////////////


    /**
     * Add always editPage and add testPage if registry is started and this
     * test plan is deployed.
     */
    protected void addPages()
    {
        createPageEdition();
        try
        {
            ClifConsolePlugin.getDefault().getRegistry(true);
            if (deployedTest != null && deployedTest.equals(file.getName()))
            {
            	createPageTest();
            }
        }
        catch (ClifException ex)
        {
            ex.printStackTrace(System.err);
        }
        setPartName(getEditorInput().getName());
    }

    /**
     * Save Test Plan with Java Properties format.
     */
    public void doSave(IProgressMonitor monitor) {
        Shell s = getEditorSite().getShell();
        try {
            //Save last test plan modification
            for (int i = 0; i < pages.size(); i++) {
                Object o = pages.elementAt(i);

                if (o instanceof IFormPage) {
                    IFormPage fp = (IFormPage) o;
                    if (fp.getId().equals(ID_PAGE_EDITION)) {
                        fp.doSave(monitor);
                    }
                }
            }
            // Write file
            ByteArrayOutputStream output = new ByteArrayOutputStream();
            TestPlanWriter.write2prop(output, testPlan);

            ByteArrayInputStream input = new ByteArrayInputStream(output.toByteArray());
            file.setContents(input, true, true, monitor);
            output.close();

            setDirty(false);
        } catch (CoreException e) {
            MessageDialog.openError(s, "Core exception", e.getMessage());
        } catch (FileNotFoundException e) {
            MessageDialog.openError(s, "File not found exception", e.getMessage());
        } catch (IOException e) {
            MessageDialog.openError(s, "I/O exception", e.getMessage());
        }
    }

    /**
     * Save Test Plan in new file and change Test Plan title.
     */
    public void doSaveAs() {
        SaveAsDialog dialog = new SaveAsDialog(getSite().getShell());
        IEditorInput input = getEditorInput();

        IFile original = (input instanceof IFileEditorInput) ? ((IFileEditorInput) input).getFile() : null;
        if (original != null) {
            dialog.setOriginalFile(original);
        }

        dialog.open();
        if (dialog.getResult() == null) {
            return;
        }

        IPath path = dialog.getResult();
        if (path == null) {
            return;
        }

        IWorkspace workspace = ResourcesPlugin.getWorkspace();
        IFile dest = workspace.getRoot().getFile(path);

        if (dest.equals(file)) {
            doSave(null);
            return;
        }
        if (!dest.exists()) {
            try {
                dest.create(null, true, null);
            } catch (CoreException e) {
                MessageDialog.openError(getSite().getShell(),
                        "CLIF Console Plug-in",
                        "Save as problem : cannot create the file");
            }
        }
        IFile currentFile = file;
        file = dest;
        doSave(null);
        file = currentFile;
    }

    /**
     * SaveAs is supported for this editor
     * @return true if SaveAs is supported
     */
    public boolean isSaveAsAllowed() {
        return true;
    }

    /**
     * Test if the edition page needs to be save.
     * @return edition page dirty state
     */
    public boolean isDirty() {
        return isDirty;
    }

    /**
     * Set the editor dirty state.
     * @param isDirty the dirty state true if editor needs to be saved.
     */
    public void setDirty(boolean isDirty) {
        this.isDirty = isDirty;
        editorDirtyStateChanged();

        if (testPage != null) {
            testPage.refresh();
        }
    }

    /**
     * Test if the edition page is editable.
     * @return edition page dirty state
     */
    public boolean isEditable() {
        return editPage.isEditable();
    }

    /**
     * Get edit page
     * @return TestPlanMasterEditPage the editPage.
     */
    public TestPlanMasterEditPage getEditPage() {
        return editPage;
    }

	/**
	 * Set the editor editable state
	 * @param isEditable the editable state
	 */
	public void setEditable(boolean isEditable) {
		editPage.setEditable(isEditable);
	}
}
