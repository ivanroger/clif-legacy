/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2009 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * Contact: clif@ow2.org
 */

package org.ow2.clif.console.lib.egui.wizards.clifProject;

import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.widgets.Composite;

/**
 * Fourth page of the New CLIF project Wizard. 
 * Used to choose advanced properties like data collector delay, 
 * jonathan connection factory host and file storage host
 * 
 * @author Florian Francheteau
 */
public class AdvancedParametersPage extends WizardPage{

	private InteractionManager interaction;

	/**
	 * Constructor
	 */
	public AdvancedParametersPage() {
		super("wizardPage");
		setTitle("Advanced parameters");
		setDescription("CAUTION: quit and start again the CLIF console for changes to be effective.");
	}

	/**
	 * Creates and initializes controls through InterationManager
	 */
	public void createControl(Composite parent) {
		interaction = new InteractionManager(this,null);
		Composite comp = interaction.createAdvancedContents(parent);
		interaction.initializeAdvanced();
		setControl(comp);
	}	
}
