#include "jni_Autotest.h"
#include <stdio.h>
#include <unistd.h>
#include <sys/time.h>

#ifdef WIN32
#include <windows.h>
static jdouble s_scaleFactor;
#endif

JNIEXPORT jint JNICALL
JNI_OnLoad (JavaVM * vm, void * reserved)
{
#ifdef WIN32
	LARGE_INTEGER counterFrequency;
	QueryPerformanceFrequency(&counterFrequency);
	s_scaleFactor = counterFrequency.QuadPart / 1000.0;
#endif
	return JNI_VERSION_1_2;
}


/*
	Sleeps during the provided duration (in ms) and sets the duration attribute
	of the ActionEvent argument in micro-seconds.
	Note that the usleep() method used for the sleep purpose is obsolete and may not
	be Multi-Thread safe.
*/
JNIEXPORT jobject JNICALL Java_jni_Autotest_native_1action(JNIEnv *env, jobject obj, jobject arg)
{
	// counts the number of native calls (just for fun...)
	static long n = 0;

	jint duration_us;
	jclass cls;
	jfieldID fid;
	jlong arg_sleep_ms;

	// get initial time
#ifdef WIN32
	LARGE_INTEGER counterReading;
	QueryPerformanceCounter(&counterReading);
	long startTime = (counterReading.QuadPart / s_scaleFactor) * 1000;
#else
	struct timeval tv1, tv2;
	gettimeofday(&tv1, NULL);
#endif

	// get sleep duration argument and sleep...
	cls = (*env)->GetObjectClass(env, obj);
	fid = (*env)->GetFieldID(env, cls, "arg_sleep_ms", "J");
	arg_sleep_ms = (*env)->GetLongField(env, obj, fid);
	usleep((unsigned long) 1000*arg_sleep_ms);
	printf("native sleep %ldms, call#%ld\n", (long)arg_sleep_ms, n++);
	fflush(stdout);

	// get current time ant compute actual sleep duration
#ifdef WIN32
	QueryPerformanceCounter(&counterReading);
	duration_us = (jint)((counterReading.QuadPart / s_scaleFactor) * 1000 - startTime);
#else
	gettimeofday(&tv2, NULL);
	duration_us = (jint)((tv2.tv_sec - tv1.tv_sec) * 1000000 + tv2.tv_usec - tv1.tv_usec);
#endif

	// set duration attribute in micro-seconds	
	cls = (*env)->GetObjectClass(env, arg);
	fid = (*env)->GetFieldID(env, cls, "duration", "I");
	(*env)->SetIntField(env, arg, fid, duration_us);

	return arg;
}
