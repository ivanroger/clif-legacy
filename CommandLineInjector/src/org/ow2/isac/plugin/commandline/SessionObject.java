/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2009 France Telecom
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * CLIF
 *
 * Contact: clif@ow2.org
 */

package org.ow2.isac.plugin.commandline;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.Error;
import java.util.Map;
import org.ow2.clif.scenario.isac.plugin.SessionObjectAction;
import org.ow2.clif.storage.api.ActionEvent;
import org.ow2.clif.supervisor.api.ClifException;
import org.ow2.clif.scenario.isac.plugin.SampleAction;
import org.ow2.clif.scenario.isac.exception.IsacRuntimeException;
import org.ow2.clif.scenario.isac.plugin.DataProvider;

/**
 * Implementation of a session object for plugin ~CommandLineInjector~
 * 
 * @author Colette Vincent
 * @author Rémi Druilhe
 * @author Bruno Dillenseger 
 */
public class SessionObject implements SessionObjectAction, SampleAction, DataProvider 
{
	static final int SAMPLE_EXECUTE = 0;
	static final String SAMPLE_EXECUTE_ITERATION = "iteration";
	static final String SAMPLE_EXECUTE_COMMENT = "comment";
	static final String SAMPLE_EXECUTE_COMMAND = "command";
	// names of variables provided by this plug-in
	static final String GET_STDOUT = "stdout";
	static final String GET_STDERR = "stderr";
	static final String GET_RETCODE = "retcode";

	// to keep the outputs and return code of the latest command-line execution
	protected String stdout = "";
	protected String stderr = "";
	protected String retcode = "";


	///////////////////////////////
	// sessions objects creation //
	///////////////////////////////


	/**
	 * To create specimen session objects
	 */
	public SessionObject(Hashtable<String, String> params) {}


	/**
	 * Copy constructor (clone a specimen object to get a session object)
	 * 
	 * @param so specimen object to clone
	 */
	private SessionObject(SessionObject so) {}


	////////////////////////////////////////
	// SessionObjectAction implementation //
	////////////////////////////////////////


	/**
	 * @see org.ow2.clif.scenario.isac.plugin.SessionObjectAction#createNewSessionObject()
	 */
	public Object createNewSessionObject() 
	{
		return new SessionObject(this);
	}


	/**
	 * @see org.ow2.clif.scenario.isac.plugin.SessionObjectAction#close()
	 */
	public void close() {}


	/**
	 * @see org.ow2.clif.scenario.isac.plugin.SessionObjectAction#reset()
	 */
	public void reset()
	{
		stdout = "";
		stderr = "";
		retcode = "";
	}


	/////////////////////////////////
	// SampleAction implementation //
	/////////////////////////////////


	/**
	 * @see org.ow2.clif.scenario.isac.plugin.SampleAction#doSample()
	 */
	public ActionEvent doSample(int number, Map<String,String> params, ActionEvent report) 
	{
		switch (number) 
		{
			case SAMPLE_EXECUTE:
				List<String> commandLine = new ArrayList<String>();
				final String commandLineStr = (String)params.get(SAMPLE_EXECUTE_COMMAND);
				try 
				{
					Runtime runtime = Runtime.getRuntime();
					if (System.getProperty("os.name").toLowerCase().startsWith("windows"))
					{
						commandLine.add("cmd.exe");
						commandLine.add("/c");
						commandLine.add("\"" + commandLineStr + "\"");
					}
					else // assuming Linux or Unix
					{
						commandLine.add("/bin/sh");
						commandLine.add("-c");
						commandLine.add(commandLineStr);
					}
					report.setDate(System.currentTimeMillis());	
					final Process process = runtime.exec(commandLine.toArray(new String[commandLine.size()]));
					// standard output reading
					new Thread() 
					{
						public void run() 
						{
							try 
							{
								BufferedReader reader = new BufferedReader(
									new InputStreamReader(process.getInputStream()));
								String streamline = "";
								try
								{
									stdout = "";
									
									while ((streamline = reader.readLine()) != null) 
										stdout += streamline+System.getProperty("line.separator");								
								} 
								finally 
								{
									reader.close();
								}
							} 
							catch(IOException e) 
							{
								throw new IsacRuntimeException(
									"Problem in reading standard output while executing command \"" + commandLineStr + "\"" ,
									e);
							}
						}
					}.start();	
					// standard error output reading				 
					new Thread()
					{		 
						public void run()
						{
							try
							{
								BufferedReader reader = new BufferedReader(new InputStreamReader(process.getErrorStream()));
								String streamline = "";
								try
								{
									stderr="";
									
									while ((streamline = reader.readLine()) != null)						
										stderr += streamline + System.getProperty("line.separator");
								}
								finally
								{
									reader.close();
								}
							}
							catch(IOException e)
							{
								throw new IsacRuntimeException(
									"Problem in reading standard error output while executing command \"" + commandLineStr + "\"" ,
									e);
							}
						}
					}.start();
					// wait until the end of command execution
					process.waitFor();
					// get return code of command execution
					int result = process.exitValue();
					retcode = String.valueOf(result);
					// fill sample report
					report.duration = (int) (System.currentTimeMillis() - report.getDate());
					report.type = "COMMAND LINE";
					report.comment = (String)params.get(SAMPLE_EXECUTE_COMMENT);
					report.iteration = Long.parseLong((String)params.get(SAMPLE_EXECUTE_ITERATION));
					report.result = retcode;
					report.successful = (result == 0);
				}
				catch (Exception ex)
				{
					throw new IsacRuntimeException(
						"Problem while executing command \"" + commandLineStr + "\"" ,
						ex);
				}
				break;
			default:
				throw new Error("Unable to find this sample in ~CommandLineInjector~ ISAC plugin: "	+ number);
		}
		return report;
	}


	/////////////////////////////////
	// DataProvider implementation //
	/////////////////////////////////


	/**
	 * @see org.ow2.clif.scenario.isac.plugin.DataProvider#doGet()
	 */
	public String doGet(String var) 
	{
		if (var.equals(GET_STDOUT))
		{
			return stdout;
		}
		else if (var.equals(GET_STDERR))
		{
			return stderr;
		}
		else if (var.equals(GET_RETCODE))
		{
			return retcode;
		}
		else
		{
			throw new IsacRuntimeException("Unknown variable in ~CommandLineInjector~ ISAC plugin: " + var);
		}
	}
}
