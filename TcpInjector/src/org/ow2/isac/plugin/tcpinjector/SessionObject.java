package org.ow2.isac.plugin.tcpinjector;

import org.ow2.clif.scenario.isac.plugin.SessionObjectAction;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Map;
import org.ow2.clif.scenario.isac.plugin.DataProvider;
import org.ow2.clif.scenario.isac.plugin.TestAction;
import org.ow2.clif.scenario.isac.exception.IsacRuntimeException;
import org.ow2.clif.scenario.isac.plugin.ControlAction;
import org.ow2.clif.storage.api.ActionEvent;
import org.ow2.clif.scenario.isac.plugin.SampleAction;
import org.ow2.clif.scenario.isac.util.ParameterParser;
import org.ow2.clif.util.Network;

/**
 * Implementation of a session object for plugin ~TcpInjector~
 */
public class SessionObject implements SessionObjectAction, DataProvider,TestAction, ControlAction, SampleAction {

	static final String PLUGIN_UNIT = "unit";
	static final String PLUGIN_CHARSET = "charset";
	static final int SAMPLE_READBYTES = 5;
	static final String SAMPLE_READBYTES_BUFFERNAME = "buffername";
	static final int SAMPLE_READUTFSTRING = 6;
	static final String SAMPLE_READUTFSTRING_BUFFERNAME = "buffername";
	static final int SAMPLE_READSTRINGLINE = 7;
	static final String SAMPLE_READSTRINGLINE_BUFFERNAME = "buffername";
	static final int SAMPLE_WRITEBYTES = 8;
	static final String SAMPLE_WRITEBYTES_DATASTRING = "datastring";
	static final int SAMPLE_WRITEUTFSTRING = 9;
	static final String SAMPLE_WRITEUTFSTRING_DATASTRING = "datastring";
	static final int SAMPLE_WRITESTRINGLINE = 10;
	static final String SAMPLE_WRITESTRINGLINE_DATASTRING = "datastring";
	static final int SAMPLE_SENDREQUESTLINE = 11;
	static final String SAMPLE_SENDREQUESTLINE_DATASTRING = "datastring";
	static final String SAMPLE_SENDREQUESTLINE_BUFFERNAME = "buffername";
	static final int CONTROL_ACCEPT = 3;
	static final String CONTROL_ACCEPT_TIMEOUT = "timeout";
	static final String CONTROL_ACCEPT_LOCALPORT = "localport";
	static final String CONTROL_ACCEPT_LOCALIP = "localip";
	static final int CONTROL_SETTIMEOUT = 4;
	static final String CONTROL_SETTIMEOUT_TIMEOUT = "timeout";
	static final int CONTROL_CONNECT = 12;
	static final String CONTROL_CONNECT_TIMEOUT = "timeout";
	static final String CONTROL_CONNECT_REMOTEPORT = "remoteport";
	static final String CONTROL_CONNECT_REMOTEIP = "remoteip";
	static final String CONTROL_CONNECT_LOCALPORT = "localport";
	static final String CONTROL_CONNECT_LOCALIP = "localip";
	static final int CONTROL_CLOSE = 13;
	static final int TEST_ISOPEN = 0;
	static final String TEST_ISOPEN_OPTIONS = "options";
	static final int TEST_EOF = 1;
	static final String TEST_EOF_OPTIONS = "options";
	static final int TEST_TIMEDOUT = 2;
	static final String TEST_TIMEDOUT_OPTIONS = "options";
	static final String OLD0_PLUGIN_UNIT = "unit";
	static private final int BUFFER_SIZE=4096;
	
	private Socket sock = null;
	private PrintWriter out = null;
	private BufferedReader in = null;
	private InetAddress localhost;
	private int localport = -1;
	private InetSocketAddress remoteAddress;
	private Map<String,String> buffers = new HashMap<String,String>();
	private byte[] buffer = new byte[BUFFER_SIZE];
	private boolean eofReached = false;
	private boolean timedOut = false;
	/** if false, response times are measured in microseconds instead of milliseconds */
	private boolean unit_ms = true;
	private String charset;

	/**
	 * Constructor for specimen object.
	 *
	 * @param params key-value pairs for plugin parameters
	 */
	public SessionObject(Map<String,String> params) {
		
		charset = params.get(PLUGIN_CHARSET);
		if(charset == null || charset.trim().isEmpty()){
			charset = Charset.defaultCharset().name();
		}
		
		String unit = ParameterParser.getRadioGroup((String)params.get(OLD0_PLUGIN_UNIT));
		unit_ms = (unit == null) || unit.equals("millisecond");

	}

	/**
	 * Copy constructor (clone specimen object to get session object).
	 *
	 * @param so specimen object to clone
	 */
	private SessionObject(SessionObject so) {

		this.unit_ms = so.unit_ms;
		charset = so.charset;
		
	}

	
	
	
	
	private void acceptSocket(Map<String,String> params)
	{
		// definition of local IP address
		String localIpStr = (String)params.get(CONTROL_ACCEPT_LOCALIP);
		if (localIpStr != null && localIpStr.length() > 0)
		{
			try
			{
				localhost = InetAddress.getByAddress(localIpStr, new byte[4]);
			}
			catch (Exception ex)
			{
				throw new IsacRuntimeException("TcpInjector can't accept socket because the specified local IP address is not valid: " + localIpStr, ex);
			}
		}
		else
		{
			localhost = Network.getInetAddress(null);
		}
		if (localhost == null)
		{
			throw new IsacRuntimeException("TcpInjector can't accept socket because no local network interface is configured");
		}
		// definition of local port
		String localPortStr = (String)params.get(CONTROL_ACCEPT_LOCALPORT);
		if (localPortStr != null && localPortStr.length() > 0)
		{
			try
			{
				localport = Integer.parseInt(localPortStr);
			}
			catch (Exception ex)
			{
				throw new IsacRuntimeException("TcpInjector can't accept socket because the specified local port is not valid: " + localPortStr, ex);
			}
		}
		else
		{
			localport = 0;
		}
		// time out definition
		String timeoutStr = (String)params.get(CONTROL_ACCEPT_TIMEOUT);
		int timeout = 0;
		if (timeoutStr != null && timeoutStr.length() > 0)
		{
			try
			{
				timeout = Integer.parseInt(timeoutStr);
			}
			catch (NumberFormatException ex)
			{
				throw new IsacRuntimeException("TcpInjector can't accept socket because the specified timeout is not valid: " + timeoutStr, ex);
			}
		}
		try
		{
			ServerSocket server = new ServerSocket(localport, 0, localhost);
			server.setSoTimeout(timeout);
			sock = server.accept();
		}
		catch (IOException ex)
		{
			throw new IsacRuntimeException("TcpInjector can't create a server socket: ", ex);
		}
	}


	private void connectSocket(Map<String,String> params)
	{
		// local address setting
		String localIpStr = (String)params.get(CONTROL_CONNECT_LOCALIP);
		if (localIpStr != null && localIpStr.length() > 0)
		{
			try
			{
				localhost = InetAddress.getByAddress(localIpStr, new byte[4]);
			}
			catch (Exception ex)
			{
				throw new IsacRuntimeException("TcpInjector can't connect socket because the specified local IP address is not valid: " + localIpStr, ex);
			}
		}
		else
		{
			localhost = Network.getInetAddress(null);
		}
		if (localhost == null)
		{
			throw new IsacRuntimeException("TcpInjector can't connect socket because no local network interface is configured");
		}
		String localPortStr = (String)params.get(CONTROL_CONNECT_LOCALPORT);
		if (localPortStr != null && localPortStr.length() > 0)
		{
			try
			{
				localport = Integer.parseInt(localPortStr);
			}
			catch (Exception ex)
			{
				throw new IsacRuntimeException("TcpInjector can't connect socket because the specified local port is not valid: " + localPortStr, ex);
			}
		}
		else
		{
			localport = 0;
		}
		// target address setting
		String targetHost = (String)params.get(CONTROL_CONNECT_REMOTEIP);
		if (targetHost == null || targetHost.length() == 0)
		{
			throw new IsacRuntimeException("TcpInjector can't connect socket because no remote host is specified");
		}
		String targetPort = (String)params.get(CONTROL_CONNECT_REMOTEPORT);
		try
		{
			remoteAddress = new InetSocketAddress(targetHost, Integer.parseInt(targetPort));
		}
		catch (Exception ex)
		{
			throw new IsacRuntimeException("TcpInjector can't connect socket because target port is unspecified or invalid: " + targetPort, ex);
		}
		// get time out value
		String timeoutStr = (String)params.get(CONTROL_CONNECT_TIMEOUT);
		if (timeoutStr == null || timeoutStr.length() == 0)
		{
			throw new IsacRuntimeException("TcpInjector can't connect socket because no time out is specified");
		}
		int timeout = 0;
		try
		{
			timeout = Integer.parseInt(timeoutStr);
		}
		catch (NumberFormatException ex)
		{
			throw new IsacRuntimeException("TcpInjector can't connect socket because the time out value (" + timeoutStr + ") is not a correct number.");
		}
		// socket creation and connection
				
		try
		{
			sock = new Socket();
			sock.bind(new InetSocketAddress(localhost, localport));
			sock.connect(remoteAddress, timeout);
			
			InputStreamReader inStream = new InputStreamReader(sock.getInputStream());
			in = new BufferedReader(inStream);
			out = new PrintWriter(sock.getOutputStream());
						
		}
		catch (Exception ex)
		{
			sock = null;
			throw new IsacRuntimeException("TcpInjector can't connect " + localhost + " to " + remoteAddress,ex);
			
		}
	}
	
	private void closeSocket()
	{
		if (sock != null)
		{
			try
			{
				sock.close();
				in = null;
				out = null;
				sock = null;
			}
			catch (Exception ex)
			{
				throw new RuntimeException("ISAC TcpInjector could not close socket", ex);
			}
		}
	}
	
	
//	private ActionEvent connectSocket(ActionEvent report, Map<String,String> params)
//	{
//		// local address setting
//		String localIpStr = (String)params.get(SAMPLE_CONNECT_LOCALIP);
//		if (localIpStr != null && localIpStr.length() > 0)
//		{
//			try
//			{
//				localhost = InetAddress.getByAddress(localIpStr, new byte[4]);
//			}
//			catch (Exception ex)
//			{
//				throw new IsacRuntimeException("TcpInjector can't connect socket because the specified local IP address is not valid: " + localIpStr, ex);
//			}
//		}
//		else
//		{
//			localhost = Network.getInetAddress(null);
//		}
//		if (localhost == null)
//		{
//			throw new IsacRuntimeException("TcpInjector can't connect socket because no local network interface is configured");
//		}
//		String localPortStr = (String)params.get(SAMPLE_CONNECT_LOCALPORT);
//		if (localPortStr != null && localPortStr.length() > 0)
//		{
//			try
//			{
//				localport = Integer.parseInt(localPortStr);
//			}
//			catch (Exception ex)
//			{
//				throw new IsacRuntimeException("TcpInjector can't connect socket because the specified local port is not valid: " + localPortStr, ex);
//			}
//		}
//		else
//		{
//			localport = 0;
//		}
//		// target address setting
//		String targetHost = (String)params.get(SAMPLE_CONNECT_REMOTEIP);
//		if (targetHost == null || targetHost.length() == 0)
//		{
//			throw new IsacRuntimeException("TcpInjector can't connect socket because no remote host is specified");
//		}
//		String targetPort = (String)params.get(SAMPLE_CONNECT_REMOTEPORT);
//		try
//		{
//			remoteAddress = new InetSocketAddress(targetHost, Integer.parseInt(targetPort));
//		}
//		catch (Exception ex)
//		{
//			throw new IsacRuntimeException("TcpInjector can't connect socket because target port is unspecified or invalid: " + targetPort, ex);
//		}
//		// get time out value
//		String timeoutStr = (String)params.get(SAMPLE_CONNECT_TIMEOUT);
//		if (timeoutStr == null || timeoutStr.length() == 0)
//		{
//			throw new IsacRuntimeException("TcpInjector can't connect socket because no time out is specified");
//		}
//		int timeout = 0;
//		try
//		{
//			timeout = Integer.parseInt(timeoutStr);
//		}
//		catch (NumberFormatException ex)
//		{
//			throw new IsacRuntimeException("TcpInjector can't connect socket because the time out value (" + timeoutStr + ") is not a correct number.");
//		}
//		// socket creation and connection
//		long start_ns = 0;
//		report.setDate(System.currentTimeMillis());
//		if (! unit_ms)
//		{
//			start_ns = System.nanoTime();
//		}
//		try
//		{
//			sock = new Socket();
//			sock.bind(new InetSocketAddress(localhost, localport));
//			sock.connect(remoteAddress, timeout);
//			if (unit_ms)
//			{
//				report.duration = (int)(System.currentTimeMillis() - report.getDate());
//			}
//			else
//			{
//				report.duration = (int)((System.nanoTime() - start_ns) / 1000);
//			}
//			
//			InputStreamReader inStream = new InputStreamReader(sock.getInputStream());
//			in = new BufferedReader(inStream);
//			out = new PrintWriter(sock.getOutputStream());
//			
//			report.successful = true;
//			report.result = localhost + " connected to " + remoteAddress;
//			return report;
//		}
//		catch (Exception ex)
//		{
//			sock = null;
//			report.successful = false;
//			report.comment = "TcpInjector can't connect " + localhost + " to " + remoteAddress;
//			report.result = ex.toString();
//			return report;
//		}
//	}
	
//	private ActionEvent closeSocket(ActionEvent report)
//	{
//		if (sock != null)
//		{
//			long start_ns = 0;
//			if (! unit_ms)
//			{
//				start_ns = System.nanoTime();
//			}
//			report.setDate(System.currentTimeMillis());
//			try
//			{
//				sock.close();
//				if (unit_ms)
//				{
//					report.duration = (int)(System.currentTimeMillis() - report.getDate());
//				}
//				else
//				{
//					report.duration = (int)((System.nanoTime() - start_ns) / 1000);
//				}
//				in = null;
//				out = null;
//				sock = null;
//				report.successful = true;
//				report.result = localhost + " disconnected from " + remoteAddress;
//				return report;
//			}
//			catch (Exception ex)
//			{
//				report.successful = false;
//				report.comment = "ISAC TcpInjector can't disconnect " + localhost + " from " + remoteAddress;
//				report.result = ex.toString();
//				return report;
//			} 
//		}
//		else
//		{
//			report.successful = false;
//			report.comment = "ISAC TcpInjector can't close unopen socket connection";
//			report.result = "ignored";
//			return null;
//		}
//	}


	private ActionEvent readSocket(ActionEvent report, boolean asBytes, String bufferName)
	{
		eofReached = false;
		timedOut = false;
		try
		{
			int size = 0;
			String data ="";
			long start_ns = 0;
			if (! unit_ms)
			{
				start_ns = System.nanoTime();
			}
			report.setDate(System.currentTimeMillis());
			if (asBytes)
			{
				InputStream in = sock.getInputStream();
				size = in.read(buffer, 0, buffer.length);
				if (bufferName != null)
				{
					data = new String(buffer, 0, size);
				}
			}
			else
			{
				DataInputStream in = new DataInputStream(sock.getInputStream());
				data = in.readUTF();
				if (data == null)
				{
					eofReached = true;
					size = 0;
				}
				else
				{
					size = data.length();
				}
			}
			if (unit_ms)
			{
				report.duration = (int)(System.currentTimeMillis() - report.getDate());
			}
			else
			{
				report.duration = (int)((System.nanoTime() - start_ns) / 1000);
			}
			report.successful = true;
			report.comment = size + " bytes read";
			report.result = "OK";
			if (bufferName != null)
			{
				buffers.put(bufferName, data.toString());
			}
			return report;
		}
		catch (Exception ex)
		{
			report.successful = false;
			report.comment = ex.getMessage();
			report.result = ex.toString();
			if (ex instanceof SocketTimeoutException)
			{
				timedOut = true;
			}
			if (bufferName != null)
			{
				buffers.remove(bufferName);
			}
			return report;
		}
	}


	private ActionEvent writeSocket(ActionEvent report, boolean asBytes, String data)
	{
		try
		{
			long start_ns = 0;
			if (! unit_ms)
			{
				start_ns = System.nanoTime();
			}
			report.setDate(System.currentTimeMillis());
			DataOutputStream out = new DataOutputStream(sock.getOutputStream());
			if (asBytes)
			{
				out.writeBytes(data);
			}
			else
			{
				out.writeUTF(data);
			} 
			out.flush();
			if (unit_ms)
			{
				report.duration = (int)(System.currentTimeMillis() - report.getDate());
			}
			else
			{
				report.duration = (int)((System.nanoTime() - start_ns) / 1000);
			}
			report.successful = true;
			report.comment = data.length() + " chars written";
			report.result = "OK";
			return report;
		}
		catch (Exception ex)
		{
			report.successful = false;
			report.comment = ex.getMessage();
			report.result = ex.toString();
			return report;
		}
	}
	
	private ActionEvent readSocketLine(ActionEvent report, String bufferName)
	{
		eofReached = false;
		timedOut = false;
		try
		{
			// int size = 0;
			String data ="";
			long start_ns = 0;
			if (! unit_ms)
			{
				start_ns = System.nanoTime();
			}
			report.setDate(System.currentTimeMillis());
			if(in == null){
				InputStreamReader inStream = new InputStreamReader(sock.getInputStream());
				in = new BufferedReader(inStream);
			}
			
			//reading
			data = in.readLine();
			if (unit_ms)
			{
				report.duration = (int)(System.currentTimeMillis() - report.getDate());
			}
			else
			{
				report.duration = (int)((System.nanoTime() - start_ns) / 1000);
			}
			report.successful = true;
			report.comment = data + ": line read";
			report.result = "OK";
			if (bufferName != null)
			{
				buffers.put(bufferName, data.toString());
			}
			return report;
		}
		catch (Exception ex)
		{
			report.successful = false;
			report.comment = ex.getMessage();
			report.result = ex.toString();
			if (ex instanceof SocketTimeoutException)
			{
				timedOut = true;
			}
			if (bufferName != null)
			{
				buffers.remove(bufferName);
			}
			return report;
		}
	}
	
	
	private ActionEvent writeSocketLine(ActionEvent report, String data)
	{
		try
		{
			long start_ns = 0;
			if (! unit_ms)
			{
				start_ns = System.nanoTime();
			}
			report.setDate(System.currentTimeMillis());
			if(out == null){
				out = new PrintWriter(sock.getOutputStream());
			}
			
			out.println(data);
			out.flush();
			if (unit_ms)
			{
				report.duration = (int)(System.currentTimeMillis() - report.getDate());
			}
			else
			{
				report.duration = (int)((System.nanoTime() - start_ns) / 1000);
			}
			report.successful = true;
			report.comment = data + " line written";
			report.result = "OK";
			return report;
		}
		catch (Exception ex)
		{
			report.successful = false;
			report.comment = ex.getMessage();
			report.result = ex.toString();
			return report;
		}
	}
	
	private ActionEvent sendRequestLine(ActionEvent report, String data, String bufferName)
	{
		try
		{
			long start_ns = 0;
			String response ="";
			if (! unit_ms)
			{
				start_ns = System.nanoTime();
			}
			report.setDate(System.currentTimeMillis());
			if(out == null){
				out = new PrintWriter(sock.getOutputStream());
			}
			if(in == null){
				InputStreamReader inStream = new InputStreamReader(sock.getInputStream());
				in = new BufferedReader(inStream);
			}
			//writing					
			out.println(data);
			out.flush();
			//reading
			response = in.readLine();
			
			if (unit_ms)
			{
				report.duration = (int)(System.currentTimeMillis() - report.getDate());
			}
			else
			{
				report.duration = (int)((System.nanoTime() - start_ns) / 1000);
			}
			report.successful = true;
			if(unit_ms){
				report.comment = data + " line write "+response+" Line read and time is millisecond ";
			}
			else{
				report.comment = data + " line write "+response+" Line read and time is microsecond ";
			}
			report.result = "OK";
			if (bufferName != null)
			{
				buffers.put(bufferName, response.toString());
			}
			return report;
		}
		catch (Exception ex)
		{
			report.successful = false;
			report.comment = ex.getMessage();
			report.result = ex.toString();
			return report;
		}
	}

	
	

	////////////////////////////////////////
	// SessionObjectAction implementation //
	////////////////////////////////////////

	/**
	 * @see org.ow2.clif.scenario.isac.plugin.SessionObjectAction#createNewSessionObject()
	 */
	public Object createNewSessionObject() {
		return new SessionObject(this);
	}

	/**
	 * @see org.ow2.clif.scenario.isac.plugin.SessionObjectAction#close()
	 */
	public void close() {
		closeSocket();
	}

	/**
	 * @see org.ow2.clif.scenario.isac.plugin.SessionObjectAction#reset()
	 */
	public void reset() {
		
		closeSocket();
		buffers.clear();
		eofReached = false;
		timedOut = false;

	}
	/////////////////////////////////
	// DataProvider implementation //
	/////////////////////////////////

	/**
	 * @see org.ow2.clif.scenario.isac.plugin.DataProvider#doGet()
	 */
	public String doGet(String bufferName)
	{
		return buffers.get(bufferName);
	}

	
	///////////////////////////////
	// TestAction implementation //
	///////////////////////////////

	/**
	 * @see org.ow2.clif.scenario.isac.plugin.TestAction#doTest()
	 */
	public boolean doTest(int number, Map<String, String> params) {
		switch (number) {
		case TEST_TIMEDOUT:
			if (ParameterParser.getCheckBox((String)params.get(TEST_TIMEDOUT_OPTIONS)).contains("not"))
			{
				return ! timedOut;
			}
			else
			{
				return timedOut;
			}
		case TEST_EOF:
			if (ParameterParser.getCheckBox((String)params.get(TEST_EOF_OPTIONS)).contains("not"))
			{
				return ! eofReached;
			}
			else
			{
				return eofReached;
			}
		case TEST_ISOPEN:
			if (ParameterParser.getCheckBox((String)params.get(TEST_ISOPEN_OPTIONS)).contains("not"))
			{
				return sock == null;
			}
			else
			{
				return sock != null;
			}
		default:
			throw new Error(
					"Unable to find this test in ~TcpInjector~ ISAC plugin: "
							+ number);
		}
	}

	
	//////////////////////////////////
	// ControlAction implementation //
	//////////////////////////////////

	/**
	 * @see org.ow2.clif.scenario.isac.plugin.ControlAction#doControl()
	 */
	public void doControl(int number, Map<String, String> params) {
		switch (number) {
		case CONTROL_CLOSE:
			closeSocket();
			break;
		case CONTROL_CONNECT:
			connectSocket(params);
			break;
		case CONTROL_SETTIMEOUT:
			if (sock == null)
			{
				throw new IsacRuntimeException("TcpInjector can't set read timeout while the socket is not open.");
			}
			else
			{
				String timeoutStr = (String)params.get(CONTROL_ACCEPT_TIMEOUT);
				if (timeoutStr != null && timeoutStr.length() > 0)
				{
					try
					{
						sock.setSoTimeout(Integer.parseInt(timeoutStr));
					}
					catch (Exception ex)
					{
						throw new IsacRuntimeException("TcpInjector can't set this timeout value: " + timeoutStr, ex);
					}
				}
			}
			break;
		case CONTROL_ACCEPT:
			acceptSocket(params);
			break;
		default:
			throw new Error(
					"Unable to find this control in ~TcpInjector~ ISAC plugin: "
							+ number);
		}
	}

	
	/////////////////////////////////
	// SampleAction implementation //
	/////////////////////////////////

	/**
	 * @see org.ow2.clif.scenario.isac.plugin.SampleAction#doSample()
	 */
	public ActionEvent doSample(int number, Map<String, String> params,
			ActionEvent report) {
		switch (number) {
		case SAMPLE_SENDREQUESTLINE:
		{
			report.type = "TCP read string line";
			String bufferName = (String)params.get(SAMPLE_SENDREQUESTLINE_BUFFERNAME);
			if (bufferName != null && buffer.length == 0)
			{
				bufferName = null;
			}
			return sendRequestLine(report, new String(params.get(SAMPLE_SENDREQUESTLINE_DATASTRING).getBytes(), Charset.forName(charset)), bufferName);
		}
		case SAMPLE_WRITESTRINGLINE:
			report.type = "TCP write string line";
			return writeSocketLine(report, new String(params.get(SAMPLE_WRITESTRINGLINE_DATASTRING).getBytes(), Charset.forName(charset)));
		case SAMPLE_WRITEUTFSTRING:
			report.type = "TCP write UTF string";
			return writeSocket(report, false, new String(params.get(SAMPLE_WRITEUTFSTRING_DATASTRING).getBytes(), Charset.forName(charset)));
		case SAMPLE_WRITEBYTES:
			report.type = "TCP write bytes";
			return writeSocket(report, true, new String(params.get(SAMPLE_WRITEBYTES_DATASTRING).getBytes(), Charset.forName(charset)));
		case SAMPLE_READSTRINGLINE:
		{
			report.type = "TCP read string line";
			String bufferName = (String)params.get(SAMPLE_READSTRINGLINE_BUFFERNAME);
			if (bufferName != null && buffer.length == 0)
			{
				bufferName = null;
			}
			return readSocketLine(report, bufferName);
		}
		case SAMPLE_READUTFSTRING:
		{
			report.type = "TCP read UTF string";
			String bufferName = (String)params.get(SAMPLE_READUTFSTRING_BUFFERNAME);
			if (bufferName != null && buffer.length == 0)
			{
				bufferName = null;
			}
			return readSocket(report, false, bufferName);
		}
		case SAMPLE_READBYTES:
		{
			report.type = "TCP read bytes";
			String bufferName = (String)params.get(SAMPLE_READBYTES_BUFFERNAME);
			if (bufferName != null && buffer.length == 0)
			{
				bufferName = null;
			}
			return readSocket(report, true, bufferName);
		}
		default:
			throw new Error(
					"Unable to find this sample in ~TcpInjector~ ISAC plugin: "
							+ number);
		}
		
	}

}
