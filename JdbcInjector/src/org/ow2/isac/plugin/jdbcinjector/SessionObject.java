/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2004-2007, 2013 France Telecom R&D
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 * Contact: clif@ow2.org
 */

package org.ow2.isac.plugin.jdbcinjector;

import org.ow2.clif.scenario.isac.plugin.SessionObjectAction;
import java.util.Hashtable;
import java.util.Map;
import org.ow2.clif.scenario.isac.plugin.ControlAction;
import org.ow2.clif.scenario.isac.exception.IsacRuntimeException;
import org.ow2.clif.storage.api.ActionEvent;
import org.ow2.clif.scenario.isac.plugin.SampleAction;
import org.ow2.clif.scenario.isac.plugin.DataProvider;
import org.ow2.clif.scenario.isac.util.ParameterParser;
import org.ow2.clif.util.ClifClassLoader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.HashMap;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.ow2.isac.plugin.jdbcinjector.tools.*;


/**
 * Implementation of a session object for plugin ~Jdbc_1.0~
 * @author Pierre Crepieux 18/04/2007
 * @author Bruno Dillenseger
 */
public class SessionObject implements SessionObjectAction, ControlAction, SampleAction, DataProvider {

	static final int SAMPLE_EXECUTEQUERY = 7;
	static final String SAMPLE_EXECUTEQUERY_RESULTSETTYPES = "resultSetTypes";
	static final String SAMPLE_EXECUTEQUERY_USERESULTSET = "useResultSet";
	static final String SAMPLE_EXECUTEQUERY_SQLSTMT = "sqlStmt";
	static final int SAMPLE_EXECUTEUPDATE = 8;
	static final String SAMPLE_EXECUTEUPDATE_SQLSTMT = "sqlStmt";
	static final int SAMPLE_PREPARESTATEMENT = 9;
	static final String SAMPLE_PREPARESTATEMENT_RESULTSETTYPES = "resultSetTypes";
	static final String SAMPLE_PREPARESTATEMENT_USERESULTSET = "useResultSet";
	static final String SAMPLE_PREPARESTATEMENT_PLACEHOLDERS = "placeHolders";
	static final String SAMPLE_PREPARESTATEMENT_SQLSTMT = "sqlStmt";
	static final String SAMPLE_PREPARESTATEMENT_PREPAREDSTMTNAME = "preparedStmtName";
	static final int SAMPLE_EXECUTE = 10;
	static final String SAMPLE_EXECUTE_PLACEHOLDERSVALUES = "placeHoldersValues";
	static final String SAMPLE_EXECUTE_EXECUTEMETHOD = "executeMethod";
	static final String SAMPLE_EXECUTE_OBJECT = "object";
	static final String SAMPLE_EXECUTE_PREPAREDSTMTNAME = "preparedStmtName";
	static final int SAMPLE_CONNECT = 11;
	static final int SAMPLE_COMMIT = 12;
	static final int SAMPLE_ROLLBACK = 13;
	static final int CONTROL_SET_AUTOCOMMIT = 1;
	static final String CONTROL_SET_AUTOCOMMIT_AUTOCOMMIT = "autocommit";
	static final int CONTROL_SET_ISOLATIN_LEVEL = 2;
	static final String CONTROL_SET_ISOLATIN_LEVEL_ISOLATIONLEVEL = "isolationLevel";
	static final String PLUGIN_BASE_NAME = "base_name";
	static final String PLUGIN_PASSWD = "passwd";
	static final String PLUGIN_LOGIN = "login";
	static final String PLUGIN_DIRECTJDBC = "directJdbc";
	static final String PLUGIN_PORT = "port";
	static final String PLUGIN_HOST = "host";
	static final String PLUGIN_SERVERTYPE = "serverType";

	//pattern used by the data provider to identify which column 
	//to return
	private static final Pattern	pattern		= Pattern.compile("\\[(\\d+)\\]");
	
	private String login 		= 	null;
	private String passwd 		= 	null;
	private int serverType 		=	-1;
	private String url 			= 	null;
	private String baseUrl 		= 	null;
	private String driver 		= 	null ;
	private String baseName 	= 	null;
	private String host 		= 	null ;
	private String port 		= 	null;
	
	//each SessionObject holds a connection to the database
	//be careful when designing your scenario !
	private Connection connection = null;
	
	//each SessionObject may have multiple 
	//PreparedStatement. They are stored in a 
	//HashMap
	private Map preparedStmtMap = null;
	
	//the provided data are stored in this array
	//to be improved
	private String	resultSet[]=new String[8]; 
	
	/**
	 * Constructor for specimen object.
	 *
	 * @param params key-value pairs for plugin parameters
	 */
	public SessionObject(Hashtable params) {
		List<String> directDriver = ParameterParser.getCheckBox((String)params.get(PLUGIN_DIRECTJDBC));
		this.serverType = ( (Integer)ServerTypes.toServerMap.get(
			ParameterParser.getCombo((String)params.get(PLUGIN_SERVERTYPE)))
		).intValue();
		
		this.url = (String)ServerTypes.toUrlMap.get(this.serverType);
		this.driver = (String)ServerTypes.toDriverMap.get(this.serverType);
		
		this.baseName = (String) params.get(PLUGIN_BASE_NAME);
		this.host = (String) params.get(PLUGIN_HOST);
		this.port = (String) params.get(PLUGIN_PORT);
		this.login = (String) params.get(PLUGIN_LOGIN);
		this.passwd = (String) params.get(PLUGIN_PASSWD);
		
		switch(this.serverType) {
		case ServerTypes.SOLID:
			this.baseUrl = this.url + "//" + this.host + ":" + this.port;
			break;
		case ServerTypes.TIMESTEN:
			if (directDriver.contains("enabled")){
				this.baseUrl = this.url + "direct:"+"dsn=" + this.baseName;
			}else{
				this.baseUrl = this.url + "client:"+"dsn=" + this.baseName;
			}
			break;
		default:
			this.baseUrl = this.url + "//" + this.host + ":" + this.port+"/" + this.baseName;
		break;
		}
		
		//It seems the specimen doesn't require 
		//the HashMap to be allocated. It will
		//be done by the copy constructor
		//this.preparedStmtMap = new HashMap(8);
		
		//there'is no need to initialize the connection for the specimen
		//but in some case, it may highlight a connection problem from the
		//begining ...
		//this.initJDBCConnection();
}

	/**
	 * Copy constructor (clone specimen object to get session object).
	 *
	 * @param so specimen object to clone
	 */
	private SessionObject(SessionObject so) {
		try{
			// copy references of the attributes values
			this.serverType = so.serverType;
			this.driver = so.driver;
			this.url = so.url;
			this.baseName = so.baseName;
			this.host = so.host;
			this.port = so.port;
			this.baseUrl = so.baseUrl;
			this.login = so.login;
			this.passwd = so.passwd;
			
			//our clone needs its own Prepared Statement Map
			//as this is connection dependent.
			this.preparedStmtMap = new HashMap(8);
			
			// init the connections
			initJDBCConnection();
		}catch(Exception e){
			throw new IsacRuntimeException("error in copy constructor for "+this.baseUrl,e);
		}
	}

	/**
	 * Init a new jdbc connection to the base
	 */
	private void initJDBCConnection() {
		// check if the driver class is reachable
		try {
			ClifClassLoader.getClassLoader().loadClass(this.driver);
		} catch (ClassNotFoundException cnfe) {
			throw new IsacRuntimeException( this.driver + " not found", cnfe);
		} catch (Exception e) {
			throw new IsacRuntimeException("Can't instantiate JDBC driver", e);
		}
		// create the connection on this base
		try {
			Thread.currentThread().setContextClassLoader(ClifClassLoader.getClassLoader());
			this.connection = DriverManager.getConnection(this.baseUrl,this.login,this.passwd);
			this.connection.setAutoCommit(true);
		} catch (Exception e) {
			e.printStackTrace();
			throw new IsacRuntimeException("Could not connect to " + this.baseUrl, e);
		}
	}
	
	/**
	 * This method permit to close the sql connection
	 */
	private void closeConnection() {
		try {
			connection.close();
		} catch (SQLException se) {
			throw new IsacRuntimeException("Unable to close the jdbc connection",se);
		}
	}
	
	private void addPreparedStmt(String name, PreparedStatementDefinition pstmt){
		preparedStmtMap.put(name,pstmt);
	}
	
	private PreparedStatementDefinition getPreparedStatement(String name){
		return (PreparedStatementDefinition)preparedStmtMap.get(name);
	}
	
	private ActionEvent ExecuteQuery(Map params, ActionEvent report) {
		String query = (String)params.get(SAMPLE_EXECUTEQUERY_SQLSTMT) ;
		ArrayList resultSetAtomizers = new ArrayList();
		
		long t1=0, t2=0, duration=0;

		ResultSet rs = null;
		Statement stmt = null;
		
		report.type = "executeQuery";
		report.setDate(System.currentTimeMillis()) ;
		try {
			//not obvious wether or not the createStatement should be measured
			t1 = System.nanoTime() ;
			stmt = connection.createStatement();
			rs = stmt.executeQuery(query);
			t2 = System.nanoTime();
			
			duration=(t2-t1)/1000;
			report.duration = (int)duration;
			report.successful = true ;
			report.comment="\""+query+"\"";
			
			//Gives the choice whether or not the resultset is retrieved
			List<String> parse_result = ParameterParser.getCheckBox(
				(String)params.get(SAMPLE_EXECUTEQUERY_USERESULTSET));
			if (parse_result.contains("enabled")){
				//At first, we analyze information given by ISAC scenario
				//in order to know what kind of data the resultSet should
				//provide
				Iterator<String> types = ParameterParser.getNField(
					(String)params.get(SAMPLE_EXECUTEQUERY_RESULTSETTYPES)).iterator();
				int jdbcType =0;
				String type;
				while (types.hasNext()){
					type=types.next();
					jdbcType=((Integer)JdbcTypes.toJdbcMap.get(type)).intValue();
					//for the moment only INTEGER and VARCHAR are supported 
					//in resultset
					switch (jdbcType){
					case java.sql.Types.INTEGER:
						resultSetAtomizers.add(new RsInteger());
						break;
					case java.sql.Types.VARCHAR:
						resultSetAtomizers.add(new RsVarchar());
						break;
					}
				}
				rs.next();
				//There is a "cleaner way of doing this ...
				int i=0;
				for (Iterator it = resultSetAtomizers.iterator (); it.hasNext (); ) {
					resultSet[i]=((ResultSetAtomizer)(it.next())).get(rs,i+1);
					i++;
				}
				//For the moment only, first column is used as the result
				//We'll see later what is the best choice
				report.result=resultSet[0];
				rs.close();
			}
			stmt.close();
		}
		catch (Exception se) {
			report.comment = "Unable to executeQuery "+se;
			report.successful = false ;
			System.err.println(se);
		}
		return report;
	}
	
	private ActionEvent ExecuteUpdate(Map params, ActionEvent report) {
		String query = (String)params.get(SAMPLE_EXECUTEUPDATE_SQLSTMT) ;
		long t1=0, t2=0, duration=0;
		int s = -1;
		
		// set the report for this action
		report.type = "executeUpdate";
		report.comment="\""+query+"\"";
		
		try {
			report.setDate(System.currentTimeMillis()) ;
			t1 = System.nanoTime();
			Statement stmt = connection.createStatement();
			s = stmt.executeUpdate(query);
			t2 = System.nanoTime();
			stmt.close();
			duration=(t2-t1)/1000;
			report.duration = (int)duration;
			report.successful = true ;
			report.result = s;
		}
		catch (Exception se) {
			report.comment = "Unable to executeUpdate "+se;
			report.successful = false ;
		}
		return report;
	}
	
	private ActionEvent PrepareStatement(Map params, ActionEvent report) {
		String name = (String)params.get(SAMPLE_PREPARESTATEMENT_PREPAREDSTMTNAME);
		String statement = (String)params.get(SAMPLE_PREPARESTATEMENT_SQLSTMT);
		List<String> arguments = ParameterParser.getNField(
			(String)params.get(SAMPLE_PREPARESTATEMENT_PLACEHOLDERS)); 
		List<String> parse_result = ParameterParser.getCheckBox(
			(String)params.get(SAMPLE_PREPARESTATEMENT_USERESULTSET));
		List<String> types = ParameterParser.getNField(
			(String)params.get(SAMPLE_PREPARESTATEMENT_RESULTSETTYPES));
		
		report.type = "prepareStatement";
		long t1=0, t2=0, duration=0;
		
		PreparedStatementDefinition psd = new PreparedStatementDefinition( name );
		psd.setStatement(statement);
		
		//We retrieve the place holder types for our prepared statement
		Iterator<String> argumentIter = arguments.iterator();
		while (argumentIter.hasNext()){
			psd.addParam(argumentIter.next());
		} 
		
		//We retrieve the type of each column contained in the resultset
		//This is done even if PARSE_PREPAREDSTATEMENT_RESULTSET is 
		//disabled. In future, we could retrieve the resulset per
		//execute call
		Iterator<String> typeIter = types.iterator();
		while (typeIter.hasNext()){
			psd.addResult(typeIter.next());
		}
		
		//Just keep track of this
		//After executing the preparedStatement in another
		//sample we'll get back this info to know if
		//we have to retrieve the resultset
		if (parse_result.contains("enabled")){
			psd.parseResultSetOn();
		}
		
		try{
			report.setDate(System.currentTimeMillis()) ;
			
			t1 = System.nanoTime();
			PreparedStatement ps = connection.prepareStatement( statement );
			t2 = System.nanoTime();
			
			duration=(t2-t1)/1000;
			report.duration = (int)duration;
			report.successful = true ;
			report.comment=name;
			report.result="\""+psd.toString()+"\"";
			psd.setPreparedStatement(ps);
			this.addPreparedStmt(name , psd);
		}catch (Exception se) {
			report.comment = psd.toString()+"/"+se;
			report.successful = false ;
		}
		return report;
	}
	
	private ActionEvent Exec( Map params, ActionEvent report) {
		
		String name =(String)params.get(SAMPLE_EXECUTE_PREPAREDSTMTNAME) ; 
		String executionType = ParameterParser.getCombo(
			(String)params.get(SAMPLE_EXECUTE_EXECUTEMETHOD));
		List<String> values = ParameterParser.getNField(
			(String)params.get(SAMPLE_EXECUTE_PLACEHOLDERSVALUES));
		
		PreparedStatementDefinition psd = null;
		PreparedStatement ps = null;
		
		report.type="ExecPrepStmt";
		long t1=0, t2=0, duration=0;
		
		psd = (PreparedStatementDefinition)getPreparedStatement(name);
		if( psd == null ){	
			report.setDate(System.currentTimeMillis());
			report.duration = 0;
			report.successful = false ;
			report.comment="no prepared statement definition found: "+name;
			return report;
		}
		
		ps = psd.getPreparedStatement();
		if( ps == null ){
			report.setDate(System.currentTimeMillis());
			report.duration = 0;
			report.successful = false ;
			report.comment="no prepared statement found: "+name;
			return report;
		}
		
		//Set arguments of the prepared statement
		int capacity = psd.getCapacity();
		Iterator<String> valueIter = values.iterator();
		try{
			for(int i=0;i<capacity;i++){
				psd.setPlaceHolder(i,valueIter.next());
			}
		}catch (Exception se) {
			report.setDate(System.currentTimeMillis());
			report.duration = 0;
			report.successful = false ;
			report.comment = "Unable to set query object "+se;
			return report;
		}
		
		ResultSet rs=null;
		try{
			if( executionType.compareTo("Execute") == 0 ){
				report.setDate(System.currentTimeMillis());
				t1 = System.nanoTime();
				ps.execute();          
				t2 = System.nanoTime();
				duration=(t2-t1)/1000;
				report.duration = (int)duration;
				report.comment=name;
			}else if( executionType.compareTo("ExecuteQuery") == 0 ){
				report.setDate(System.currentTimeMillis());
				t1 = System.nanoTime();
				rs=ps.executeQuery();     
				t2 = System.nanoTime();
				duration=(t2-t1)/1000;
				report.duration = (int)duration;
				report.comment=name;
			}else if ( executionType.compareTo("ExecuteUpdate") == 0 ){
				report.setDate(System.currentTimeMillis());
				t1 = System.nanoTime();
				ps.executeUpdate();    
				t2 = System.nanoTime();
				duration=(t2-t1)/1000;
				report.duration = (int)duration;
				report.comment=name;
			}                               
			report.successful = true ;
			if (psd.getParseResultSetState() == true){
				rs.next();
				ArrayList resultTypes=psd.getResultTypes();
				int i=0;
				for (Iterator it = resultTypes.iterator (); it.hasNext (); ) {
					resultSet[i]=((ResultSetAtomizer)(it.next())).get(rs,i+1);
					i++;
				}
			}
		}catch(SQLException se) {
			report.successful = false;
			report.comment = executionType + " " + se;
		}
		return report;
	}
	
	private ActionEvent Commit(ActionEvent report) {
		report.type = "commit";
		long t1=0, t2=0, duration=0;
		try{
			report.setDate(System.currentTimeMillis());
			t1 = System.nanoTime();
			this.connection.commit();
			t2 = System.nanoTime();
			duration=(t2-t1)/1000;
			report.duration = (int)duration;
		}catch(SQLException sqle){
			report.successful = false;
			report.duration = 0;
		}
		report.successful = true;
		return report;
	}
	
	private ActionEvent Connect(ActionEvent report)
	{
		report.type = "connect";
		report.setDate(System.currentTimeMillis());
		long start_ns = System.nanoTime();
		try
		{
			initJDBCConnection();
			report.duration = (int)((System.nanoTime() - start_ns) / 1000);
			report.successful = true;
		}
		catch(Exception e)
		{
			report.successful = false;
		}
		return report;
	}
	
	private ActionEvent Rollback(ActionEvent report) {
		report.type = "rollback";
		long t1=0,t2=0,duration=0;
		try{
			report.setDate(System.currentTimeMillis());
			t1 = System.nanoTime();
			this.connection.rollback();
			t2 = System.nanoTime();
			duration=(t2-t1)/1000;
			report.duration = (int)duration;
		}catch(SQLException sqle){
			report.successful = false;
			report.duration = 0;
		}
		report.successful = true;
		return report;
	}
	
	private void SetAutoCommit(boolean value){
		try{
			this.connection.setAutoCommit(value);
		}catch(SQLException sqle){
			throw new IsacRuntimeException("unable to setAutoCommit", sqle);
		}
	}
	
	private void SetTxIsolation(int value){
		try{
			this.connection.setTransactionIsolation(value);
		}catch(SQLException sqle){
			System.err.println(sqle.toString());
			throw new IsacRuntimeException("unable to setTxIsolation", sqle);
		}
	}
	////////////////////////////////////////
	// SessionObjectAction implementation //
	////////////////////////////////////////

	/**
	 * @see org.ow2.clif.scenario.isac.plugin.SessionObjectAction#createNewSessionObject()
	 */
	public Object createNewSessionObject() {
		return new SessionObject(this);
	}

	/**
	 * @see org.ow2.clif.scenario.isac.plugin.SessionObjectAction#close()
	 */
	public void close() {
		this.closeConnection() ;
	}

	/**
	 * @see org.ow2.clif.scenario.isac.plugin.SessionObjectAction#reset()
	 */
	public void reset() {

	}

	
	//////////////////////////////////
	// ControlAction implementation //
	//////////////////////////////////

	/**
	 * @see org.ow2.clif.scenario.isac.plugin.ControlAction#doControl()
	 */
	public void doControl(int number, Map params) {
		try{
		switch (number) {
		case CONTROL_SET_ISOLATIN_LEVEL:{
			String requiredLevel = ParameterParser.getCombo(
				(String)params.get(CONTROL_SET_ISOLATIN_LEVEL_ISOLATIONLEVEL));
			int levelValue;
			levelValue=((Integer)IsolationLevel.toJdbcMap.get(requiredLevel)).intValue();
			SetTxIsolation(levelValue);
			return;
		}
			case CONTROL_SET_AUTOCOMMIT:{
				String requiredAutocommit = ParameterParser.getRadioGroup(
					(String)params.get(CONTROL_SET_AUTOCOMMIT_AUTOCOMMIT));
				if("on".compareTo(requiredAutocommit)==0){
					SetAutoCommit(true);
				}else{
					SetAutoCommit(false);
				}			
				return;
			}
			default:
			throw new Error("Unable to find this control in ~Jdbc_1.0~ ISAC plugin: " + number);
		}
		}catch(Exception e){
			throw new IsacRuntimeException(e + "thrown while processing control in ~Jdbc_1.0~ ISAC plugin: " + number, e);
		}
	}

	
	/////////////////////////////////
	// SampleAction implementation //
	/////////////////////////////////

	/**
	 * @see org.ow2.clif.scenario.isac.plugin.SampleAction#doSample()
	 */
	public ActionEvent doSample(int number, Map params, ActionEvent report) {
		switch (number) {
		case SAMPLE_ROLLBACK:
			return Rollback(report);
			case SAMPLE_COMMIT:
				return Commit(report);
			case SAMPLE_CONNECT:
				return Connect(report);
			case SAMPLE_EXECUTE:
				return Exec( params,report );
			case SAMPLE_PREPARESTATEMENT:
				return PrepareStatement( params, report );
			case SAMPLE_EXECUTEUPDATE:
				return ExecuteUpdate( params,report );
			case SAMPLE_EXECUTEQUERY:
				return ExecuteQuery( params,report );
			default:
			throw new Error("Unable to find this sample in ~Jdbc_1.0~ ISAC plugin: " + number);
		}
		//throw new IsacRuntimeException("No action defined for this sample in ~Jdbc_1.0~ ISAC plugin: " + number);
	}

	
	/////////////////////////////////
	// DataProvider implementation //
	/////////////////////////////////

	/**
	 * @see org.ow2.clif.scenario.isac.plugin.DataProvider#doGet()
	 */
	public String doGet(String var) {
		try{
		Matcher m;
		int i;
		m = pattern.matcher(var);
		return m.matches()
		&& (i = Integer.parseInt(m.group(1))) < resultSet.length ? resultSet[i]
		                                                               : null;
		}catch(Exception e){
		throw new IsacRuntimeException("Unknown parameter value in ~Plugin Name~ ISAC plugin: " + var,e);
		}
	}

}
