/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2004 France Telecom R&D
 *
 * Contact: clif@ow2.org
 *
 * @author P Crepieux 18/04/2007
 */

package org.ow2.isac.plugin.jdbcinjector.tools;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class PhInteger implements PlaceHolder{
	public void set(PreparedStatement ps,int rank,String value){
		try{
			ps.setObject( rank,new Integer(value),java.sql.Types.INTEGER );
		}catch(SQLException sqle){
			
		}
	}
	public String toString(){
		return "INTEGER";
	}
}
