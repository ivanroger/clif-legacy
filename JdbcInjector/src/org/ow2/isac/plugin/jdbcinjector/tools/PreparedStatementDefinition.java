/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2004 France Telecom R&D
 *
 * Contact: clif@ow2.org
 *
 * @author P Crepieux 18/04/2007
 */

package org.ow2.isac.plugin.jdbcinjector.tools;

import java.sql.PreparedStatement;
import java.util.ArrayList;
import java.util.Iterator;


public class PreparedStatementDefinition{
	
	String _name = null;
	String _stmt = null ;
	boolean parseResultSet=false;
	PreparedStatement _ps = null; 
	ArrayList _paramList = new ArrayList ();
	ArrayList _resultTypes = new ArrayList();
	int capacity=0;
	int resultset_capacity=0;
	
	public PreparedStatementDefinition(String name){
		_name = name;
	}
	
	public void setPreparedStatement(PreparedStatement ps){
		_ps = ps;
	}
	
	public PreparedStatement getPreparedStatement(){
		return(_ps);
	}
	
	public void setName(String name){
		_name = name;
	}
	
	public void setStatement(String stmt){
		_stmt = stmt;
	}
	 
	public void addParam(String type){
		int t = ((Integer)JdbcTypes.toJdbcMap.get(type)).intValue();
		switch (t){
		case java.sql.Types.INTEGER:
			_paramList.add(new PhInteger());
			break;
		case java.sql.Types.VARCHAR:
			_paramList.add(new PhVarchar());
			break;
		}
	}
	
	public void addResult(String type){
		int t = ((Integer)JdbcTypes.toJdbcMap.get(type)).intValue();
		switch (t){
		case java.sql.Types.INTEGER:
			_resultTypes.add(new RsInteger());
			break;
		case java.sql.Types.VARCHAR:
			_resultTypes.add(new RsVarchar());
			break;
		}
	}
	
	public void setPlaceHolder(int i, String value){
		getParam(i).set(_ps,i+1,value);
	}
	
	public int getResultSetCapacity(){
		return _resultTypes.size();
	}
	
	public ArrayList getResultTypes(){
		return _resultTypes;
	}
	
	public PlaceHolder getParam(int index){
		return ( (PlaceHolder)_paramList.get(index) );
	}
	
	public int getCapacity(){
		return _paramList.size();
	}
	
	public void parseResultSetOn(){
		parseResultSet=true;
	}
	
	public void parseResultSetOff(){
		parseResultSet=false;
	}
	
	public boolean getParseResultSetState(){
		System.err.println(parseResultSet);
		return parseResultSet;
	}
	
	public String toString(){
		String s;
		s=_stmt + " with ( ";
		for (Iterator it = _paramList.iterator (); it.hasNext (); ) {
		    s+=it.next().toString()+" ";
		}
		s+=" )";
		return s;
	}
}