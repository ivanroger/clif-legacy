/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2004 France Telecom R&D
 *
 * Contact: clif@ow2.org
 *
 * @author P Crepieux 18/04/2007
 */

package org.ow2.isac.plugin.jdbcinjector.tools;
import java.sql.ResultSet;
import java.sql.SQLException;

public class RsInteger implements ResultSetAtomizer{
	public String get(ResultSet rs, int rank){
		try{
			return new Integer(rs.getInt(rank)).toString();
		}catch(SQLException sqle){
			System.err.println(sqle);
			return null;
		}
		
	}

}
