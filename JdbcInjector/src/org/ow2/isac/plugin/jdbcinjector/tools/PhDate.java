/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2004 France Telecom R&D
 *
 * Contact: clif@ow2.org
 *
 * @author P Crepieux 18/04/2007
 */

package org.ow2.isac.plugin.jdbcinjector.tools;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.ow2.clif.scenario.isac.exception.IsacRuntimeException;

public class PhDate implements PlaceHolder {
	private static SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yy HH:mm:ss");
	
	public void set(PreparedStatement ps, int rank, String value){
		Date d;
		try{
			d=formatter.parse(new String(value));
		}catch(ParseException pe){
			throw new IsacRuntimeException("date format error");
		}
		try{
			ps.setObject(rank,new Timestamp(d.getTime()), java.sql.Types.TIMESTAMP);
		}catch(SQLException sqle){
		}
	}
}
