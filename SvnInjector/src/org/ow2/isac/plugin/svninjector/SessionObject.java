/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2016 Orange SA
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * Contact: clif@ow2.org
 */
package org.ow2.isac.plugin.svninjector;

import org.ow2.isac.plugin.svninjector.CheckoutListener;
import org.ow2.clif.scenario.isac.plugin.SessionObjectAction;
import java.util.Map;
import org.ow2.clif.storage.api.ActionEvent;
import org.ow2.clif.scenario.isac.plugin.SampleAction;
import org.ow2.clif.scenario.isac.exception.IsacRuntimeException;
import org.tmatesoft.svn.core.auth.ISVNAuthenticationManager;
import org.tmatesoft.svn.core.internal.io.fs.FSRepositoryFactory;
import org.tmatesoft.svn.core.internal.io.dav.DAVRepositoryFactory;
import org.tmatesoft.svn.core.internal.io.svn.SVNRepositoryFactoryImpl;
import org.tmatesoft.svn.core.SVNURL;
import org.tmatesoft.svn.core.io.SVNRepository;
import org.tmatesoft.svn.core.io.SVNRepositoryFactory;
import org.tmatesoft.svn.core.wc.SVNRevision;
import org.tmatesoft.svn.core.wc.SVNWCUtil;
import org.tmatesoft.svn.core.SVNException;
import org.ow2.clif.scenario.isac.plugin.ControlAction;

/**
 * Implementation of a session object for plug-in ~SvnInjector~.
 * Enables check-out and commit operations with an SVN repository, through a
 * number of ISAC controls and samples.
 * When adding a file for an upcoming commit, the file content is
 * generated on the fly, thus enabling committing numerous and big files,
 * without handling heavy data sets.
 * CAUTION: due to the embedded SVN client library, this session object does not
 * support multiple simultaneous virtual users, but a single one.
 *  
 * @author Bruno Dillenseger
 */
public class SessionObject implements SessionObjectAction, SampleAction, ControlAction
{
	static final int CONTROL_ADDDIR = 1;
	static final String CONTROL_ADDDIR_PATH = "path";
	static final int CONTROL_ADDFILE = 2;
	static final String CONTROL_ADDFILE_SIZE = "size";
	static final String CONTROL_ADDFILE_NAME = "name";
	static final int CONTROL_SETDIR = 4;
	static final String CONTROL_SETDIR_PATH = "path";
	static final int CONTROL_CONNECT = 5;
	static final String CONTROL_CONNECT_PASSWORD = "password";
	static final String CONTROL_CONNECT_LOGIN = "login";
	static final String CONTROL_CONNECT_URL = "url";
	static final int CONTROL_CLOSE = 6;
	static final int SAMPLE_CHECKOUT = 0;
	static final String SAMPLE_CHECKOUT_COMMENT = "comment";
	static final String SAMPLE_CHECKOUT_TYPE = "type";
	static final String SAMPLE_CHECKOUT_PATH = "path";
	static final int SAMPLE_COMMIT = 3;
	static final String SAMPLE_COMMIT_COMMENT = "comment";
	static final String SAMPLE_COMMIT_TYPE = "type";
	static final String SAMPLE_COMMIT_MESSAGE = "message";
	static
	{
		//Set up connection protocols support:
		//http:// and https://
		DAVRepositoryFactory.setup();
		//svn://, svn+xxx:// (svn+ssh:// in particular)
		SVNRepositoryFactoryImpl.setup();
		//file:///
		FSRepositoryFactory.setup();
	}

	SVNRepository repo = null;
	Committer commit = null;

	/**
	 * Constructor for specimen object.
	 * Does nothing.
	 * @param params no plug-in import parameter
	 */
	public SessionObject(Map<String,String> params)
	{
	}

	/**
	 * Copy constructor (clone specimen object to get session object).
	 * Does nothing.
	 *
	 * @param so specimen object to clone
	 */
	private SessionObject(SessionObject so)
	{
	}


	/////////////////////////////
	// private utility methods //
	/////////////////////////////


	/**
	 * Changes current directory for further file/directory addition
	 * in next commit.
	 * @param path the new current directory, given as a full path
	 * relative to the repository path.
	 * Prior to this call, the directory path must have been added already.
	 */
	private void setDir(String path)
	{
		if (commit == null)
		{
			commit = new Committer(repo);
		}
		commit.setDir(path, false);
	}


	/**
	 * Adds a new directory to next commit, and
	 * makes this directory the current directory for further
	 * file/directory addition in next commit.
	 * @param path the new directory path to add, given as a full path
	 * relative to the repository path.
	 */
	private void addDir(String path)
	{
		if (commit == null)
		{
			commit = new Committer(repo);
		}
		commit.setDir(path, true);
	}


	/**
	 * Adds a new file to next commit, in current directory.
	 * @param name the name of the new file to add to current path
	 * @param size the file size in bytes
	 */
	private void addFile(String name, Long size)
	{
		if (commit == null)
		{
			commit = new Committer(repo);
		}
		commit.addFile(name, size);
	}


	////////////////////////////////////////
	// SessionObjectAction implementation //
	////////////////////////////////////////

	/**
	 * Just calls the copy constructor.
	 * @see org.ow2.clif.scenario.isac.plugin.SessionObjectAction#createNewSessionObject()
	 */
	public Object createNewSessionObject()
	{
		return new SessionObject(this);
	}

	/**
	 * Closes the possibly open connection with an SVN repository.
	 * @see org.ow2.clif.scenario.isac.plugin.SessionObjectAction#close()
	 */
	public void close()
	{
		if (repo != null)
		{
			repo.closeSession();
		}
		repo = null;
		commit = null;
	}

	/**
	 * @see #close()
	 * @see org.ow2.clif.scenario.isac.plugin.SessionObjectAction#reset()
	 */
	public void reset()
	{
		close();
	}

	
	/////////////////////////////////
	// SampleAction implementation //
	/////////////////////////////////

	/**
	 * @see org.ow2.clif.scenario.isac.plugin.SampleAction#doSample()
	 */
	public ActionEvent doSample(int number, Map<String, String> params, ActionEvent report)
	throws IsacRuntimeException
	{
		if (repo == null)
		{
			throw new IsacRuntimeException("No SVN operation can be performed before connecting to a repository.");
		}
		String comment;
		switch (number)
		{
			case SAMPLE_COMMIT:
				try
				{
					if (commit == null)
					{
						throw new IsacRuntimeException("Nothing to commit");
					}
					report.setDate(System.currentTimeMillis());
					commit.commit(params.get(SAMPLE_COMMIT_MESSAGE));
					report.duration = (int)(System.currentTimeMillis() - report.getDate());
					report.result = commit.getCommittedFiles() + " files committed " + commit.getWrittenBytes() + " bytes written";
					report.successful = true;
				}
				catch (SVNException ex)
				{
					ex.printStackTrace(System.out);
					report.successful = false;
					report.result = ex.getMessage();
				}
				finally
				{
					commit = null;
				}
				report.type = params.get(SAMPLE_COMMIT_TYPE);
				comment = params.get(SAMPLE_COMMIT_COMMENT);
				report.comment =
					comment == null || comment.length() == 0
					? repo.getLocation().toString()
					: comment;
				break;
			case SAMPLE_CHECKOUT:
				CheckoutListener col = new CheckoutListener();
				String path = params.get(SAMPLE_CHECKOUT_PATH);
				if (path.startsWith("."))
				{
					path = path.substring(1);
				}
				if (path.startsWith("/"))
				{
					path = path.substring(1);
				}
				try
				{
					report.setDate(System.currentTimeMillis());
					repo.checkout(SVNRevision.HEAD.getNumber(), path, true, col);
					report.duration = (int)(System.currentTimeMillis() - report.getDate());
					report.result = col.getFiles() + " files " + col.getBytes() + " bytes read";
					report.successful = true;
				}
				catch (SVNException ex)
				{
					ex.printStackTrace(System.out);
					report.successful = false;
					report.result = col.getFiles() + " files " + col.getBytes() + " bytes read";
				}
				report.type = params.get(SAMPLE_CHECKOUT_TYPE);
				comment = params.get(SAMPLE_CHECKOUT_COMMENT);
				report.comment =
					comment == null || comment.length() == 0
					? repo.getLocation().toString() + "/" + path
					: comment;
				break;
			default:
				throw new IsacRuntimeException(
					"Unable to find this sample in ~SvnInjector~ ISAC plugin: " + number);
		}
		return report;
	}

	
	//////////////////////////////////
	// ControlAction implementation //
	//////////////////////////////////


	/**
	 * @see org.ow2.clif.scenario.isac.plugin.ControlAction#doControl()
	 */
	public void doControl(int number, Map<String, String> params)
	{
		switch (number)
		{
			case CONTROL_CLOSE:
				close();
				break;
			case CONTROL_CONNECT:
				close();
				try
				{
					String url = params.get(CONTROL_CONNECT_URL);
					if (!url.endsWith("/"))
					{
						url += "/";
					}
					SVNURL svnurl = SVNURL.parseURIEncoded(url);
					repo = SVNRepositoryFactory.create(svnurl);
					String login = params.get(CONTROL_CONNECT_LOGIN);
					if (login != null && !login.isEmpty())
					{
						ISVNAuthenticationManager authMgr = SVNWCUtil.createDefaultAuthenticationManager(
							login,
							params.get(CONTROL_CONNECT_PASSWORD));
						repo.setAuthenticationManager(authMgr);
					}
				}
				catch (SVNException e)
				{
					repo = null;
					throw new IsacRuntimeException("Incorrect SVN server URL \"" + params.get(CONTROL_CONNECT_URL) + "\".", e);
				}
				break;
			case CONTROL_SETDIR:
				if (repo == null)
				{
					throw new IsacRuntimeException("No SVN operation can be performed before connecting to a repository.");
				}
				setDir(params.get(CONTROL_SETDIR_PATH));
				break;
			case CONTROL_ADDFILE:
				if (repo == null)
				{
					throw new IsacRuntimeException("No SVN operation can be performed before connecting to a repository.");
				}
				addFile(params.get(CONTROL_ADDFILE_NAME), Long.valueOf(params.get(CONTROL_ADDFILE_SIZE)));
				break;
			case CONTROL_ADDDIR:
				if (repo == null)
				{
					throw new IsacRuntimeException("No SVN operation can be performed before connecting to a repository.");
				}
				addDir(params.get(CONTROL_ADDDIR_PATH));
				break;
			default:
				throw new Error(
					"Unable to find this control in ~SvnInjector~ ISAC plugin: "
					+ number);
		}
	}
}
