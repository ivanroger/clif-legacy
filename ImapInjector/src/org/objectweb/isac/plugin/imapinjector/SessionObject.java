/*
* CLIF is a Load Injection Framework
* Copyright (C) 2009 Red Hat
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
* Contact: clif@ow2.org
*/
package org.objectweb.isac.plugin.imapinjector;

import org.ow2.clif.scenario.isac.plugin.SessionObjectAction;
import java.util.Hashtable;
import java.lang.Error;
import java.util.Map;
import java.util.Properties;
import org.ow2.clif.storage.api.ActionEvent;
import org.ow2.clif.scenario.isac.plugin.SampleAction;
import org.ow2.clif.scenario.isac.exception.IsacRuntimeException;
import javax.mail.*;
import javax.mail.search.*;

/**
 * Implementation of a session object for plugin ~ImapInjector~
 * @author Mark Sechrest
 */
public class SessionObject implements SessionObjectAction, SampleAction {

	static final int SAMPLE_CONNECT = 0;
	static final String SAMPLE_CONNECT_PORT = "port";
	static final String SAMPLE_CONNECT_HOSTNAME = "hostname";
	static final String SAMPLE_CONNECT_PASSWORD = "password";
	static final String SAMPLE_CONNECT_USERNAME = "username";
	static final int SAMPLE_LOGOFF = 1;
	static final int SAMPLE_SELECT = 2;
	static final String SAMPLE_SELECT_NAME = "name";
	static final int SAMPLE_SEARCH = 3;
	static final String SAMPLE_SEARCH_CRITERIA = "criteria";
	static final int SAMPLE_FETCH = 4;
	static final String SAMPLE_FETCH_MESSAGEID = "messageID";
	static final int SAMPLE_MOVE = 5;
	static final String SAMPLE_MOVE_MAILBOX = "mailbox";
	static final String SAMPLE_MOVE_MESSAGEID = "messageID";
	static final String PLUGIN_PORT = "port";
	static final String PLUGIN_HOSTNAME = "hostname";

	private Properties props;
	private Session imapSession;
	private Store imapStore;
	private Folder imapFolder;
	/**
	 * Constructor for specimen object.
	 *
	 * @param params key-value pairs for plugin parameters
	 */
	public SessionObject(Hashtable params) {
	// create a properties object
		 props = new Properties();
	// set the intial properties
		String value = (String)params.get(PLUGIN_HOSTNAME);
		if (value != null && value.length() > 0) {
			try {
				props.put("mail.imaps.host", value);
			} catch (Exception ex) {
				throw new IsacRuntimeException ("Specified hostname is invalid:" + value, ex);
			}
		} else {
			throw new IsacRuntimeException ("Hostname cannot be empty.");
		}
		value = (String)params.get(PLUGIN_PORT);
		if (value != null && value.length() > 0) {
			try {
				props.put("mail.imaps.port", value);
			} catch (Exception ex) {
				throw new IsacRuntimeException ("Specified port is invalid:" + value, ex);
			}
		} else {
			throw new IsacRuntimeException ("Port cannot be empty.");
		}
	// create a session
		props.setProperty("mail.store.protocol", "imaps");
		imapSession = Session.getInstance(props);
	}

	/**
	 * Copy constructor (clone specimen object to get session object).
	 *
	 * @param so specimen object to clone
	 */
	private SessionObject(SessionObject so) {
		this.props = so.props;
		this.imapSession = so.imapSession;
	}


	////////////////////////////////////////
	// SessionObjectAction implementation //
	////////////////////////////////////////

	/**
	 * @see org.ow2.clif.scenario.isac.plugin.SessionObjectAction#createNewSessionObject()
	 */
	public Object createNewSessionObject() {
		return new SessionObject(this);
	}

	/**
	 * @see org.ow2.clif.scenario.isac.plugin.SessionObjectAction#close()
	 */
	public void close() {

	}

	/**
	 * @see org.ow2.clif.scenario.isac.plugin.SessionObjectAction#reset()
	 */
	public void reset() {

	}

	
	/////////////////////////////////
	// SampleAction implementation //
	/////////////////////////////////

	/**
	 * @see org.ow2.clif.scenario.isac.plugin.SampleAction#doSample()
	 */
	public ActionEvent doSample(int number, Map params, ActionEvent report) {
		switch (number) {
		case SAMPLE_MOVE:
			return doMove(number, params, report);
		case SAMPLE_FETCH:
			return doFetch(number, params, report);
		case SAMPLE_SEARCH:
			return doSearch(number, params, report);
		case SAMPLE_SELECT:
			return doSelect(number, params, report);
		case SAMPLE_LOGOFF:
			return doLogoff(number, params, report);
		case SAMPLE_CONNECT:
			return doConnect(number, params, report);
		default:
			throw new Error("Unable to find this sample in ~ImapInjector~ ISAC plugin: " + number);
		}
//		throw new IsacRuntimeException("No action defined for this sample in ~ImapInjector~ ISAC plugin: " + number);
	}
	private ActionEvent doConnect(int number, Map params, ActionEvent report){
		long timeStart = 0;
		boolean overrideHost = false;
		boolean overridePort = false;
		String user = (String)params.get(SAMPLE_CONNECT_USERNAME);
		String pass = (String)params.get(SAMPLE_CONNECT_PASSWORD);
		String host = (String)params.get(SAMPLE_CONNECT_HOSTNAME);
		String port = (String)params.get(SAMPLE_CONNECT_PORT);
		report.type = "CONNECT";
		report.setDate(System.currentTimeMillis());
		report.result = null;
		
		if (host != null && host.length() > 0)
		{ //overriding the host name. Kill the current connection and update the props.
			if ( imapStore != null && imapStore.isConnected())
			{
				try {
					    imapStore.close();
				    } catch (Exception e) {
				    System.err.println("Problem closing store while setting host=" + host + ".");
					e.printStackTrace();
				    }
			}
			try {
					props.setProperty("mail.imaps.host", host);
					overrideHost = true;
				} catch (Exception ex) {
					throw new IsacRuntimeException ("Specified hostname is invalid:" + host, ex);
				}
		}
		if (port != null && port.length() > 0)
		{ //overriding the port. Kill the current connection and update the props.
			if ( imapStore != null && imapStore.isConnected())
			{
				try {
					    imapStore.close();
				    } catch (Exception e) {
					e.printStackTrace();
				    }
			}
			try {
					props.put("mail.imaps.port", port);
					overridePort = true;
				} catch (Exception ex) {
					throw new IsacRuntimeException ("Specified hostname is invalid:" + host, ex);
				}
		}						
		if (overrideHost)
		{
			while(imapSession.getProperty("mail.imaps.host") != host)
			{
				imapSession = Session.getInstance(props);
			}
		}
		if (overridePort)
		{
			while(imapSession.getProperty("mail.imaps.port") != port)
			{
				imapSession = Session.getInstance(props);
			}
		}
		
		report.comment = "Connecting to " + imapSession.getProperty("mail.imaps.host") + ":" +
              imapSession.getProperty("mail.imaps.port") + "as" + user + "/" + pass;
		try {
				imapStore = imapSession.getStore();
				timeStart = System.currentTimeMillis();	
				imapStore.connect(user, pass);
		} catch (NoSuchProviderException e) {
			System.err.println("Invalid provider" + imapSession.getProperty("mail.store.protocol"));
			report.result = e.toString();
		} catch (AuthenticationFailedException afe) {
			System.err.println("Unable to log on with user=" + user + ", password=" + pass);
			report.result = afe.toString();
		} catch (Exception e){
			e.printStackTrace();
			report.result = e.toString();
		}
		if(report.result == null){
			report.result = "OK";
			report.successful = true;
		} else {
			report.comment = "Connection failed";
			report.successful = false;
		}
		if(timeStart > 0) {
			report.duration = (int)(System.currentTimeMillis() - timeStart);
		} else {
			report.duration = 0;
		}
		return report;
	}
	
	private ActionEvent doSelect(int number, Map params, ActionEvent report){
		long timeStart = 0;
		String folderName = (String)params.get(SAMPLE_SELECT_NAME);
		report.type = "SELECT";
		report.setDate(System.currentTimeMillis());
		report.result = null;
		report.comment = "Selected " + folderName;
		
		if(!imapStore.isConnected())
		{
			report.duration = 0;
			report.successful = false;
			report.result = "Aborted";
			report.comment = "No connection";
			return report;
		}
		try {
				timeStart = System.currentTimeMillis();
				imapFolder = imapStore.getFolder(folderName);
				imapFolder.open(Folder.READ_WRITE);
		} catch (MessagingException me) {
			report.result = me.toString();
		} catch (Exception e) {
			e.printStackTrace();
			report.result = e.toString();
		}
		if(report.result == null){
			report.result = "OK";
			report.successful = true;
		} else {
			report.comment = "Unable to get folder" + folderName;
			report.successful = false;
		}
		if(timeStart > 0){
			report.duration = (int)(System.currentTimeMillis() - timeStart);
		} else {
			report.duration = 0;
		}
		return report;
	}
	
	private ActionEvent doFetch(int number, Map params, ActionEvent report){
		long timeStart = 0;
		int messageID = Integer.valueOf((String)params.get(SAMPLE_FETCH_MESSAGEID));
		Message imapMessage = null;
		report.type = "FETCH";
		report.setDate(System.currentTimeMillis());
		report.result = null;
		report.comment = "Retrieved message# " + messageID;
		
		if(!imapStore.isConnected())
		{
			report.duration = 0;
			report.successful = false;
			report.result = "Aborted";
			report.comment = "No connection";
			return report;
		}
		try {
				timeStart = System.currentTimeMillis();
				imapMessage = imapFolder.getMessage(messageID);
		} catch (MessagingException me) {
			report.result = me.toString();
		} catch (Exception e) {
			e.printStackTrace();
			report.result = e.toString();
		}
		if(report.result == null){
			report.result = "OK";
			report.successful = true;
		} else {
			report.comment = "Unable to get message" + messageID;
			report.successful = false;
		}
		if(timeStart > 0){
			report.duration = (int)(System.currentTimeMillis() - timeStart);
		} else {
			report.duration = 0;
		}
		if(imapMessage != null){
			imapMessage = null;
		}
		return report;
	}
	private ActionEvent doSearch(int number, Map params, ActionEvent report){
		long timeStart = 0;
		SearchTerm imapSearchTerm = new BodyTerm((String)params.get(SAMPLE_SEARCH_CRITERIA));
		Message imapMessage[] = null;
		report.type = "SEARCH";
		report.setDate(System.currentTimeMillis());
		report.result = null;
		
		if(!imapStore.isConnected())
		{
			report.duration = 0;
			report.successful = false;
			report.result = "Aborted";
			report.comment = "No connection";
			return report;
		}
		
		try {
				timeStart = System.currentTimeMillis();
				imapMessage = imapFolder.search(imapSearchTerm);
				report.comment = "Found " + imapMessage.length + " messages";
				imapMessage = null;
		} catch (MessagingException me) {
			report.result = me.toString();
		} catch (Exception e) {
			e.printStackTrace();
			report.result = e.toString();
		}
		if(report.result == null){
			report.result = "OK";
			report.successful = true;
		} else{
			report.comment = "Search for " + (String)params.get(SAMPLE_SEARCH_CRITERIA) + "failed.";
			report.successful = false;
		}
		if(timeStart > 0){
			report.duration = (int)(System.currentTimeMillis() - timeStart);
		} else {
			report.duration = 0;
		}
		return report;
	}
	private ActionEvent doLogoff(int number, Map params, ActionEvent report){
		long timeStart = 0;	
		report.type = "LOGOFF";
		report.setDate(System.currentTimeMillis());
		report.result = null;
		report.comment = "Logged off";
		
		if(!imapStore.isConnected())
		{
			report.duration = 0;
			report.successful = false;
			report.result = "Aborted";
			report.comment = "No connection";
			return report;
		}
		
		try {
				timeStart = System.currentTimeMillis();
				imapFolder.close(false);
				imapStore.close();	
		} catch (MessagingException me) {
			report.result = me.toString();
		} catch (Exception e) {
			e.printStackTrace();
			report.result = e.toString();
		}
		if(report.result == null){
			report.result = "OK";
			report.successful = true;
		} else{
			report.comment = "Problem closing the folder, or the store.";
			report.successful = false;
		}
		if(timeStart > 0){
			report.duration = (int)(System.currentTimeMillis() - timeStart);
		} else {
			report.duration = 0;
		}
		return report;
	}
	private ActionEvent doMove(int number, Map params, ActionEvent report){
		long timeStart = 0;
		long timeStop = 0;
		int messageID = Integer.valueOf((String)params.get(SAMPLE_MOVE_MESSAGEID));
		Message imapMessage = null;
		Folder targetFolder = null;
		String folderName = (String)params.get(SAMPLE_MOVE_MAILBOX);
		
		report.type = "MOVE";
		report.setDate(System.currentTimeMillis());
		report.result = null;
		report.comment = "Moved message " + messageID + ".";
		
		if(!imapStore.isConnected())
		{
			report.duration = 0;
			report.successful = false;
			report.result = "Aborted";
			report.comment = "No connection";
			return report;
		}
		
		try {
				timeStart = System.currentTimeMillis();
				targetFolder = imapStore.getFolder(folderName);
				targetFolder.open(Folder.READ_WRITE);
				imapMessage = imapFolder.getMessage(messageID);
				Message ImapMessages[] = imapFolder.getMessages(messageID, messageID);
				imapFolder.copyMessages(ImapMessages, targetFolder);
				imapMessage.setFlag(Flags.Flag.DELETED, true);
				timeStop = System.currentTimeMillis();
		} catch (MessagingException me) {
			report.result = me.toString();
		} catch (Exception e) {
			report.result = e.toString();
			e.printStackTrace();
		} finally {
		// Now undo the move/copy by clearing the DELETED flag on the original,
		// setting DELETED on the copy, and expunging to target folder
			if(report.result != null){
				report.comment = "Failed to copy message " + messageID + " to folder " + folderName;
			}
			try{
				imapMessage.setFlag(Flags.Flag.DELETED, false);
				if (!targetFolder.isOpen())
				{
					targetFolder = imapStore.getFolder(folderName);
					targetFolder.open(Folder.READ_WRITE);
				}
				imapMessage = targetFolder.getMessage(messageID);
				imapMessage.setFlag(Flags.Flag.DELETED, true);
				targetFolder.close(true);
				imapMessage = null;
			} catch (Exception e) {
				report.result = e.toString();
				report.comment = report.comment + " A problem occured during clean up.";
			}
		}
		if(report.result == null){
			report.result = "OK";
			report.successful = true;
		} else {
			report.successful = false;
		}
		if((timeStart > 0) && (timeStop > 0)){
			report.duration = (int)(timeStop - timeStart);
		} else {
			report.duration = 0;
		}
		return report;
	}
}
