**************************************
* README RtpInjector                 *
**************************************
* CLIF is a Load Injection Framework *
* Copyright (C) 2009 France Telecom  *
* Contact: clif@ow2.org              *
**************************************

If you want to compile the RtpInjector be sure you have the latest version of Ant (min v1.7.1).

The field "File To Stream" is like : /directory/to/my/file/file.format