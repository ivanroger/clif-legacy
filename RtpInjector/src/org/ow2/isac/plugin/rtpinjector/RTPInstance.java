package org.ow2.isac.plugin.rtpinjector;

import java.net.InetAddress;
import java.util.LinkedList;

import org.ow2.clif.probe.rtp.RTCPPacket;
import org.ow2.clif.probe.rtp.RTPListener;
import org.ow2.clif.probe.rtp.RTPSession;
import org.ow2.clif.probe.rtp.RTPStats;
import org.ow2.clif.scenario.isac.exception.IsacRuntimeException;

public class RTPInstance 
{
	// General
	public InetAddress localIp;
	public Integer localPort;
	public InetAddress remoteIp;
	public Integer remotePort;
	
	//pierre
	public String sipSession;
	public RTPSession rtpSession;
	
	//static??
	public boolean enableRtcp = false;
	public boolean enableBye = false;
	
	// RTP
	public Integer timeout;
	public Integer durationPacket = 20;
	
	// RTCP
	public String reason;
	public Long timeInterval = null;
	public LinkedList<RTCPPacket> rtcpTemplate = new LinkedList<RTCPPacket>();
	public Long lastRtcpTime = System.currentTimeMillis();
	
	public void sendRtcpPacket(RTPListener rtpListener, byte[] data)
	{
		//set up remote and local port
		rtpListener.sendPacket(data, remoteIp, remotePort + 1, localPort + 1);
	}
	
	/**
	 * Send a RTP packet.
	 * 
	 * @param data : the packet.
	 * @param remoteAddress : InetAddress of the remote client.
	 * @param remotePort : the port of the remote client.
	 * @param localPort : the port on which the packet is sent.
	 */
	public void sendRtpPacket(RTPListener rtpListener, byte[] data)
	{	
		rtpListener.sendPacket(data, remoteIp, remotePort, localPort);
		
		try
		{
			Thread.currentThread().sleep(durationPacket);
		}
		catch(InterruptedException e)
		{
			throw new IsacRuntimeException("Unable to wait " + durationPacket + " ms : " + e);
		}
	}
	
}
