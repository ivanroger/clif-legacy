package org.ow2.isac.plugin.jmsinjector;

import java.util.Hashtable;
import java.util.Map;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.MessageConsumer;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.jms.TextMessage;
import javax.naming.Context;
import javax.naming.InitialContext;

import org.ow2.clif.scenario.isac.exception.IsacRuntimeException;
import org.ow2.clif.scenario.isac.plugin.DataProvider;
import org.ow2.clif.scenario.isac.plugin.SampleAction;
import org.ow2.clif.scenario.isac.plugin.SessionObjectAction;
import org.ow2.clif.storage.api.ActionEvent;
import org.ow2.clif.supervisor.api.ClifException;

/**
 * Implementation of a session object for plugin ~JMSInjector~
 */
public class SessionObject implements SessionObjectAction, DataProvider, SampleAction {

	static final int SAMPLE_START_PRODUCER = 0;
	static final int SAMPLE_START_CONSUMER = 1;
	
	static final String PLUGIN_NBMSG = "nbMsg";
	static final String PLUGIN_CONSWEIGHT = "consWeight";
	static final String PLUGIN_PRODWEIGHT = "prodWeight";
	static final String PLUGIN_MESSAGESIZE = "messageSize";
	static final String PLUGIN_CLUSTERQUEUENAME = "clusterQueueName";
	static final String PLUGIN_CONNECTIONFACTORYNAME = "connectionFactoryName";
	static final String PLUGIN_JNDIHOST = "jndiHost";
	static final String PLUGIN_JNDIPORT = "jndiPort";
	
	private String jndiHost;
	private String jndiPort;
	private int nbMsg;
    /*private int sleep;
    private int nbMsgSleep;*/
    private int consWeight;
    private String clusterQueueName;
    private String connectionFactoryName;
    private int messageSize;
    private int prodWeight;
	
	/**
	 * Constructor for specimen object.
	 *
	 * @param params key-value pairs for plugin parameters
	 */
	public SessionObject(Map<String,String> params) throws ClifException{
		String value = (String)params.get(PLUGIN_NBMSG);
		/*System.out.println("nbMsg : "+value);
		if (value != null && value.length() > 0)
		{
			try
			{
				nbMsg = Integer.parseInt(value);
			}
			catch (Exception ex)
			{
				throw new ClifException("ISAC can't get nbMsg because the specified nbMsg is not valid: " + value, ex);
			}
		}
		else
		{
			throw new ClifException("ISAC can't get nbMsg because the specified nbMsg is not valid: " + value);
		}	

		value = (String)params.get(PLUGIN_CONSWEIGHT);
		System.out.println("consWeight : "+value);
		if (value != null && value.length() > 0)
		{
			try
			{
				consWeight = Integer.parseInt(value);
			}
			catch (Exception ex)
			{
				throw new ClifException("ISAC can't get consWeight because the specified consWeight is not valid: " + value, ex);
			}
		}
		else
		{
			throw new ClifException("ISAC can't get consWeight because the specified consWeight is not valid: " + value);
		}	

		value = (String)params.get(PLUGIN_PRODWEIGHT);
		System.out.println("prodWeight : "+value);
		if (value != null && value.length() > 0)
		{
			try
			{
				prodWeight = Integer.parseInt(value);
			}
			catch (Exception ex)
			{
				throw new ClifException("ISAC can't get prodWeight because the specified prodWeight is not valid: " + value, ex);
			}
		}
		else
		{
			throw new ClifException("ISAC can't get prodWeight because the specified prodWeight is not valid: " + value);
		}*/

		value = (String)params.get(PLUGIN_CLUSTERQUEUENAME);
		System.out.println("clusterQueueName : "+value);
		if (value != null && value.length() > 0)
		{
			try
			{
				clusterQueueName = value;
			}
			catch (Exception ex)
			{
				throw new ClifException("ISAC can't get clusterQueueName because the specified clusterQueueName is not valid: " + value, ex);
			}
		}
		else
		{
			throw new ClifException("ISAC can't get clusterQueueName because the specified clusterQueueName is not valid: " + value);
		}
		
		value = (String)params.get(PLUGIN_CONNECTIONFACTORYNAME);
		System.out.println("connectionFactoryName : "+value);
		if (value != null && value.length() > 0)
		{
			try
			{
				connectionFactoryName = value;
			}
			catch (Exception ex)
			{
				throw new ClifException("ISAC can't get connectionFactoryName because the specified connectionFactoryName is not valid: " + value, ex);
			}
		}
		else
		{
			throw new ClifException("ISAC can't get connectionFactoryName because the specified connectionFactoryName is not valid: " + value);
		}
		
		value = (String)params.get(PLUGIN_MESSAGESIZE);
		System.out.println("messageSize : "+value);
		if (value != null && value.length() > 0)
		{
			try
			{
				messageSize = Integer.parseInt(value);
			}
			catch (Exception ex)
			{
				throw new ClifException("ISAC can't get messageSize because the specified messageSize is not valid: " + value, ex);
			}
		}
		else
		{
			throw new ClifException("ISAC can't get messageSize because the specified messageSize is not valid: " + value);
		}
		
		value = (String)params.get(PLUGIN_JNDIHOST);
		System.out.println("jndiHost : "+value);
		if (value != null && value.length() > 0)
		{
			try
			{
				jndiHost = value;
			}
			catch (Exception ex)
			{
				throw new ClifException("ISAC can't get jndiHost because the specified jndiHost is not valid: " + value, ex);
			}
		}
		else
		{
			throw new ClifException("ISAC can't get jndiHost because the specified jndiHost is not valid: " + value);
		}
		
		value = (String)params.get(PLUGIN_JNDIPORT);
		System.out.println("jndiPort : "+value);
		if (value != null && value.length() > 0)
		{
			try
			{
				jndiPort = value;
			}
			catch (Exception ex)
			{
				throw new ClifException("ISAC can't get jndiPort because the specified jndiPort is not valid: " + value, ex);
			}
		}
		else
		{
			throw new ClifException("ISAC can't get jndiPort because the specified jndiPort is not valid: " + value);
		}
	}

	/**
	 * Copy constructor (clone specimen object to get session object).
	 *
	 * @param so specimen object to clone
	 */
	private SessionObject(SessionObject so) {
		this.nbMsg = so.nbMsg;
		/*this.sleep = so.sleep;
		this.nbMsgSleep = so.nbMsgSleep;*/
		this.consWeight = so.consWeight;
		this.clusterQueueName = so.clusterQueueName;
		this.connectionFactoryName =  so.connectionFactoryName;
		this.messageSize = so.messageSize;
		this.prodWeight = so.prodWeight;
		this.jndiHost = so.jndiHost;
		this.jndiPort =  so.jndiPort;
	}


	////////////////////////////////////////
	// SessionObjectAction implementation //
	////////////////////////////////////////

	/**
	 * @see org.ow2.clif.scenario.isac.plugin.SessionObjectAction#createNewSessionObject()
	 */
	public Object createNewSessionObject() {
		return new SessionObject(this);
	}

	/**
	 * @see org.ow2.clif.scenario.isac.plugin.SessionObjectAction#close()
	 */
	public void close() {

	}

	/**
	 * @see org.ow2.clif.scenario.isac.plugin.SessionObjectAction#reset()
	 */
	public void reset() {

	}

	
	/////////////////////////////////
	// DataProvider implementation //
	/////////////////////////////////

	/**
	 * @see org.ow2.clif.scenario.isac.plugin.DataProvider#doGet()
	 */
	public String doGet(String var) {
		throw new IsacRuntimeException("Unknown parameter value in ~JMSInjector~ ISAC plugin: " + var);
	}

	public ActionEvent doSample(int number, Map<String,String> params, ActionEvent report) {
		switch (number) {
			case SAMPLE_START_CONSUMER:
				return doStartConsumer(number,params,report);
			case SAMPLE_START_PRODUCER:
				return doStartProducer(number,params,report);
			default:
			throw new Error("Unable to find this sample in ~JMSInjector~ ISAC plugin: " + number);
		}
	}
	
	public ActionEvent doStartConsumer(int number, Map<String,String> params, ActionEvent report) {
        try {
        	consWeight = new Integer((String)params.get("consWeight")).intValue();
        	nbMsg = new Integer((String)params.get("nbMsg")).intValue();
            if (consWeight == 0) consWeight = 1;
            prodWeight = 0;
            
        	System.setProperty("joram.clusterQueue", clusterQueueName);
        	System.setProperty("joram.prodWeight", "" + prodWeight);
        	System.setProperty("joram.consWeight", "" + consWeight);
        	System.setProperty("registry.hostname", jndiHost);

            System.out.println();
            System.out.println("Consumes " + nbMsg + " messages on the cluster queue " + clusterQueueName);
            //System.out.println("sleep = " + sleep + ", nbMsgSleep = " + nbMsgSleep);
            System.out.println("prodWeight = " + prodWeight + ", consWeight = " + consWeight);
            
            Hashtable<String, String> env = new Hashtable<String, String>();
            env.put("java.naming.factory.initial", "fr.dyade.aaa.jndi2.client.NamingContextFactory");
            env.put("java.naming.factory.host", jndiHost);
            env.put("java.naming.factory.port", jndiPort);
            
            Context ictx = new InitialContext(env);
//            Object obj = ictx.lookup(clusterQueueName);
//            System.out.println("ClusterQueue Class : " + obj.getClass() + " " + obj.getClass().getClassLoader());
//            ClassLoader objCl = obj.getClass().getClassLoader();
//            for(;objCl != null;objCl = objCl.getParent()){
//            	System.out.println(objCl);
//            }
//            System.out.println("Object clusterQueueu dasn Clif " + obj.toString());
//            System.out.println("" + clusterQueueName + " = " + obj);
//            ClassLoader cl = Destination.class.getClassLoader();
//            System.out.println("Destination Class : " + Destination.class + " " + cl + " parent = " + cl.getParent());
            Destination clusterQueue = (Destination) ictx.lookup(clusterQueueName);
//            ConnectionFactory ccf = (ConnectionFactory) ictx.lookup("ccf" + clusterQueueName);
            ConnectionFactory ccf = (ConnectionFactory) ictx.lookup(connectionFactoryName);
            ictx.close();

            System.out.println("RMIRegistry for manager on " + System.getProperty("registry.hostname"));
            Connection cnx = ccf.createConnection("user","user");
            Session sess = cnx.createSession(true,Session.SESSION_TRANSACTED);
            //Session sess = cnx.createSession(false,Session.AUTO_ACKNOWLEDGE);
            MessageConsumer recv = sess.createConsumer(clusterQueue);

            long firstTime = System.currentTimeMillis();
            System.out.println("First time = " + firstTime);

            cnx.start();
            
            System.out.println("location = " + System.getProperty("location"));
            
            String loc = System.getProperty("location");

            int i;
            for (i = 0; i < nbMsg; i++) {
            	recv.receive();
            	try {
            		sess.commit();
            	} catch (Exception e) {
            		System.out.println("Failed on Message number " + i);
            		e.printStackTrace();
            	}
            	//System.out.println("Message " + i +": " + msg.getText());
            	// We keep a constant thoughput
            	/*if (sleep > 0 && (i % nbMsgSleep) == 0) {
            		Thread.sleep(sleep);
            	}*/
            }
            long lastTime = System.currentTimeMillis();
            System.out.println("Last time = " + lastTime);
            float average = ((lastTime - firstTime) * 1.0f) / nbMsg;
            System.out.println("Average time between each reading: " + average);
            System.out.println("Message number" + i + " read");
            System.out.println("location = " + System.getProperty("location"));
            
            System.setProperty("joram.clusterQueue", clusterQueueName);
        	System.setProperty("joram.prodWeight", "" + prodWeight);
        	System.setProperty("joram.consWeight", "" + consWeight);
        	System.setProperty("location", "" + loc);
            
            cnx.close();
            
    	    report.successful = true;
    	    report.comment = "Consumer succeded";
    	    System.out.println("Consumer succeded");                
	    } catch( Exception e ) {
         	report.successful = false;
        	report.result = e.toString();
        	report.comment = "ISAC JMSInjector error occured";
        	System.out.println("ISAC JMSInjector error occured. ");
        	e.printStackTrace();
	    }

		return report;
	}

	public ActionEvent doStartProducer(int number, Map<String,String> params, ActionEvent report) {
        try {
        	prodWeight = new Integer((String)params.get("prodWeight")).intValue();
        	nbMsg = new Integer((String)params.get("nbMsg")).intValue();
            if (prodWeight == 0) prodWeight = 1;
            consWeight = 0;
            
        	System.setProperty("joram.clusterQueue", clusterQueueName);
        	System.setProperty("joram.prodWeight", "" + prodWeight);
        	System.setProperty("joram.consWeight", "" + consWeight);
        	System.setProperty("registry.hostname", jndiHost);

            System.out.println();
            System.out.println("Produces " + nbMsg + " messages on the cluster queue " + clusterQueueName);
            //System.out.println("sleep = " + sleep + ", nbMsgSleep = " + nbMsgSleep);
            System.out.println("prodWeight = " + prodWeight + ", consWeight = " + consWeight);
            
            Hashtable<String, String> env = new Hashtable<String, String>();
            env.put("java.naming.factory.initial", "fr.dyade.aaa.jndi2.client.NamingContextFactory");
            env.put("java.naming.factory.host", jndiHost);
            env.put("java.naming.factory.port", jndiPort);
            
            Context ictx = new InitialContext(env);
            
            Destination clusterQueue = (Destination) ictx.lookup(clusterQueueName);
//            System.out.println("" + clusterQueueName + " = " + clusterQueue);
            ConnectionFactory ccf = (ConnectionFactory) ictx.lookup(connectionFactoryName);
//            ConnectionFactory ccf = (ConnectionFactory) ictx.lookup("ccf" + clusterQueueName);
            ictx.close();

            Connection cnx = ccf.createConnection("user","user");
            Session sess = cnx.createSession(true,Session.SESSION_TRANSACTED);
            //Session sess = cnx.createSession(false,Session.AUTO_ACKNOWLEDGE);
            MessageProducer producer = sess.createProducer(clusterQueue);
            
            TextMessage msg = (TextMessage) sess.createTextMessage();
            long firstTime = System.currentTimeMillis();
            System.out.println("location = " + System.getProperty("location"));
            System.out.println("First time = " + firstTime);
            
            String loc = System.getProperty("location");

            int i;
            for (i = 0; i < nbMsg; i++) {
            	msg.setText("Location: " + System.getProperty("location") + ", Test number " + i + new String(new char[messageSize - 30]));
            	producer.send(msg);
            	try {
            		sess.commit();
            	} catch (Exception e) {
            		System.out.println("Failed on Message number " + i);
            		e.printStackTrace();
            	}
            	// We keep a constant thoughput
            	/*if (sleep > 0 && (i % nbMsgSleep) == 0) {
            		Thread.sleep(sleep);
            	}*/
            }
            long lastTime = System.currentTimeMillis();
            System.out.println("Last time = " + lastTime);
            float average = ((lastTime - firstTime) * 1.0f) / nbMsg;
            System.out.println("Average time between each writing: " + average);
            System.out.println("Message number " + i + " writed");
            
            
            System.setProperty("joram.clusterQueue", clusterQueueName);
        	System.setProperty("joram.prodWeight", "" + prodWeight);
        	System.setProperty("joram.consWeight", "" + consWeight);
        	System.setProperty("location", "" + loc);
            
            cnx.close();

    	    report.successful = true;
    	    report.comment = "Producer succeded";
    	    System.out.println("Producer succeded");            
            
            //printSearchResult(searchResults);
	        
	    } catch( Exception e ) {
         	report.successful = false;
        	report.result = e.toString();
        	report.comment = "ISAC JMSInjector error occured";
        	System.out.println("ISAC JMSInjector error occured. ");
        	e.printStackTrace();
        }

		return report;
	}

}
