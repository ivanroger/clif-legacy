/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2013 France Telecom
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * See the "license.txt" provided along with this library or
 * the web site http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html for more details 
 * 
 * Contact : clif@ow2.org
 */
package org.ow2.isac.plugin.dnsprovider;

public class WeightedElement<T> {
	private T element = null;
	private int weight = 0;

	public WeightedElement(T element, int weight) {
		this.element = element;
		this.weight = weight;
	}

	public T getElement() {
		return this.element;
	}

	public int getWeight() {
		return this.weight;
	}

	public void setWeight(int weight) {
		this.weight = weight;
	}
}
