/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2013 France Telecom
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * See the "license.txt" provided along with this library or
 * the web site http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html for more details 
 * 
 * Contact : clif@ow2.org
 */
package org.ow2.isac.plugin.dnsprovider;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class represents a table of <T> weighted objects
 * 
 * @author yplo6403
 * 
 * @param <T>
 */
public class WeightedTable<T> {
	String title = null;
	int index = 0;
	int total = 0;
	List<T> elts = null;
	List<WeightedElement<T>> table = null;

	private final static Logger logger = LoggerFactory.getLogger(WeightedTable.class.getSimpleName());

	/**
	 * Created a weighted table from a map entry
	 * 
	 * @param map
	 *            the key is the object and the value is the weight
	 * @param title
	 *            the name of the table
	 */
	public WeightedTable(Map<T, Integer> map, String title) {
		this.title = title;
		this.table = new ArrayList<WeightedElement<T>>();
		this.index = 0;
		for (T key : map.keySet()) {
			try {
				int weight = Integer.valueOf(map.get(key));
				this.table.add(new WeightedElement<T>(key, weight));
			} catch (NumberFormatException e) {
				logger.error("Data: {} has invalid weight {}", key, map.get(key));
			}
		}
		switch (this.table.size()) {
			case 0 :
				logger.error("Weitghted table is empty !");
				throw new IllegalArgumentException("Cannot load weighted table '" + title + "'");
			case 1 :
				this.table.get(0).setWeight(1);
				this.total = 1;
				break;
			default :
				int[] tableWeight = new int[this.table.size()];
				int i = 0;
				for (WeightedElement<T> we : this.table) {
					tableWeight[i++] = we.getWeight();
				}
				int gcd = GreatestCommonDivisor.greatestCommonDivisor(tableWeight);
				for (WeightedElement<T> we : this.table) {
					we.setWeight(we.getWeight() / gcd);
					this.total += we.getWeight();
				}
				break;
		}
		this.elts = new ArrayList<T>();
		int i = 0;
		for (WeightedElement<T> weightedElement : this.table) {
			for (int j = 0; j < weightedElement.getWeight(); j++) {
				this.elts.add(i++, weightedElement.getElement());
			}
		}
		// randomizes entries of the table
		Collections.shuffle(this.elts);
	}

	/**
	 * @return
	 */
	public T getNext() {
		this.index = ((this.index + 1) % this.total);
		return this.elts.get(this.index);
	}

	public void display() {
		for (WeightedElement<T> elt : this.table) {
			logger.debug(this.title + " '{}' has weight '{}'", elt.getElement(), elt.getWeight());
		}
	}

	public int getTotal() {
		return this.total;
	}

}
