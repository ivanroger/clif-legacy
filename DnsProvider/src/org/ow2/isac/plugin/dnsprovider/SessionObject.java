/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2013 France Telecom
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * See the "license.txt" provided along with this library or
 * the web site http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html for more details 
 * 
 * Contact : clif@ow2.org
 */

package org.ow2.isac.plugin.dnsprovider;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.math.BigInteger;
import java.net.Inet4Address;
import java.net.Inet6Address;
import java.net.InetAddress;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.ow2.clif.scenario.isac.exception.IsacRuntimeException;
import org.ow2.clif.scenario.isac.plugin.ControlAction;
import org.ow2.clif.scenario.isac.plugin.DataProvider;
import org.ow2.clif.scenario.isac.plugin.SessionObjectAction;
import org.ow2.clif.util.ClifClassLoader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Implementation of a session object for plugin ~DnsProvider~
 * 
 * <p>
 * This plugin generates input data for the DnsInjector plugin. <br>
 * There are 6 main computed attributes:
 * <ul>
 * <li>domain : the target domain name</li>
 * <li>query : the query type</li>
 * <li>ip : the IP source</li>
 * <li>rd : the Recursion Desired flag (1/0)</li>
 * <li>udp : the transport protocol UDP (1) vs TCP (0)</li>
 * <li>dnssec : use DNSSEC (1) or not (0)</li>
 * </ul>
 * Some optional computed attributes to test reverser DNS:
 * <ul>
 * <li>ip6rev : an IPv6 address in nibble reverse dotted format</li>
 * <li>ip6for : the same IPv6 address in forward undotted format</li>
 * <li>ip4rev : an IPv4 address in reverser format
 * </ul>
 * 
 * @author Philippe Lemordant
 * 
 */
public class SessionObject implements SessionObjectAction, ControlAction, DataProvider {

	static final String PLUGIN_IP4REVERSE = "IP4reverse";
	static final String PLUGIN_IP6REVERSE = "IP6reverse";
	static final String PLUGIN_DNSSEC_ARG = "dnssec_arg";
	static final String PLUGIN_UDP_ARG = "udp_arg";
	static final String PLUGIN_RD_FLAG_ARG = "rd_flag_arg";
	static final String PLUGIN_ZONE_ARG = "zone_arg";
	static final String PLUGIN_INDEX_LENGTH_ARG = "index_length_arg";
	static final int CONTROL_NEXT = 0;
	static final String PLUGIN_IP_SRC_ARG = "ip_src_arg";
	static final String PLUGIN_QTYPE_ARG = "qtype_arg";
	static final String PLUGIN_LAST_INDEX_DOMAIN_ARG = "last_index_domain_arg";
	static final String PLUGIN_FIRST_INDEX_DOMAIN_ARG = "first_index_domain_arg";
	static final String PLUGIN_FORMAT_ARG = "format_arg";

	private static long domainIndex = 0;
	private static long startDomainIndex = 0;
	private static long endDomainIndex = -1;
	private static String domainFormat = null;
	private static int indexLength = 6;

	private final static Logger logger = LoggerFactory.getLogger(SessionObject.class.getSimpleName());

	private static WeightedTable<String> ipSrcTable = null;
	private static WeightedTable<String> queryTypeTable = null;
	private static WeightedTable<String> zoneTable = null;
	private static WeightedTable<String> dnsSecTable = null;
	private static WeightedTable<String> flagRdTable = null;
	private static WeightedTable<String> udpTable = null;
	private static WeightedTable<BigInteger> ip6revTable = null;
	private static WeightedTable<Integer> ip4revTable = null;

	private static Object syncObject = new Object();
	private String domainName;
	private String ipSource;
	private String queryType;
	private String dnssec;
	private String rd;
	private String udp;

	private BigInteger ip6rev = null;
	private Integer ip4rev = null;
	static int counterGet = 0;
	static BigInteger counterGetBI = BigInteger.ZERO;
	private static final char[] hex = "0123456789abcdef".toCharArray();

	/**
	 * Constructor for specimen object.
	 * 
	 * @param params
	 *            key-value pairs for plugin parameters
	 */
	public SessionObject(Map<String, String> params) {
		String zoneFileLocation = null;
		String qtypeFileLocation = null;
		String ipSrcFileLocation = null;
		String ip6revFileLocation = null;
		String ip4revFileLocation = null;

		String PRODUCT = "DnsProvider";
		String VERSION = "1.0";
		String REVISION = "$LastChangedRevision: 1999 $";
		String DATE = "$LastChangedDate: 2013-01-31 16:56:36 +0100 (jeu., 31 janv. 2013) $";
		String AUTHOR = "$LastChangedBy: yplo6403 $";
		int ratioDnsSec = 0;
		int ratioRDflag = 100;
		int ratioUdp = 100;

		logger.warn("Starting {} program at {}", PRODUCT, new Date());
		logger.warn("{} Version: {} ", PRODUCT, VERSION);
		logger.warn(PRODUCT + " {} at {}", REVISION, DATE);
		logger.warn("{} {}", PRODUCT, AUTHOR);

		domainFormat = (String) params.get(PLUGIN_FORMAT_ARG);
		logger.warn("Domain name target format: {}", domainFormat);

		try {
			startDomainIndex = Long.valueOf(params.get(PLUGIN_FIRST_INDEX_DOMAIN_ARG));
		} catch (NumberFormatException e) {
		}
		logger.warn("Start index for Domain name target: {}", startDomainIndex);
		domainIndex = startDomainIndex;

		try {
			endDomainIndex = Long.valueOf(params.get(PLUGIN_LAST_INDEX_DOMAIN_ARG));
		} catch (NumberFormatException e) {
		}
		if (endDomainIndex != -1) {
			if (startDomainIndex < endDomainIndex) {
				logger.warn("Last index for Domain name target: {}", endDomainIndex);
			} else {
				logger.error("Last index for Domain name target: {} mus be greater thant start index: {}", endDomainIndex,
						startDomainIndex);
			}
		}

		try {
			indexLength = Integer.valueOf(params.get(PLUGIN_INDEX_LENGTH_ARG));
		} catch (NumberFormatException e) {
		}
		logger.warn("Index length for Domain name target: {}", indexLength);

		try {
			ratioDnsSec = Integer.valueOf(params.get(PLUGIN_DNSSEC_ARG));
			if (ratioDnsSec < 0 || ratioDnsSec > 100)
				ratioDnsSec = 0;
		} catch (NumberFormatException e) {
		}
		logger.warn("DNSSEC ratio: {}", ratioDnsSec);
		dnsSecTable = initRatioTable(ratioDnsSec, "DNSSEC");
		dnsSecTable.display();

		try {
			ratioRDflag = Integer.valueOf(params.get(PLUGIN_RD_FLAG_ARG));
			if (ratioRDflag < 0 || ratioRDflag > 100)
				ratioRDflag = 0;
		} catch (NumberFormatException e) {
		}
		logger.warn("Recursion Desired (RD flag) ratio: {}", ratioRDflag);

		flagRdTable = initRatioTable(ratioRDflag, "Recursion Desired (RD flag)");
		flagRdTable.display();

		try {
			ratioUdp = Integer.valueOf(params.get(PLUGIN_UDP_ARG));
			if (ratioUdp < 0 || ratioUdp > 100)
				ratioUdp = 100;
		} catch (NumberFormatException e) {
		}
		logger.warn("UDP protocol (vs. TCP) ratio: {}", ratioUdp);
		udpTable = initRatioTable(ratioUdp, "UDP protocol (vs TCP)");

		udpTable.display();

		zoneFileLocation = params.get(PLUGIN_ZONE_ARG);
		if ((zoneFileLocation != null) && (zoneFileLocation.length() != 0)) {
			if (domainFormat.charAt(domainFormat.length() - 1) != '.')
				domainFormat = domainFormat.concat(".");
			Map<String, Integer> map = readWeightedFile(zoneFileLocation, "Zone");
			zoneTable = new WeightedTable<String>(map, "zone");
			zoneTable.display();
		}

		qtypeFileLocation = params.get(PLUGIN_QTYPE_ARG);
		if ((qtypeFileLocation != null) && (qtypeFileLocation.length() != 0)) {
			Map<String, Integer> map = readWeightedFile(qtypeFileLocation, "Query");
			queryTypeTable = new WeightedTable<String>(map, "Query");
			queryTypeTable.display();
		}

		ipSrcFileLocation = params.get(PLUGIN_IP_SRC_ARG);
		if ((ipSrcFileLocation != null) && (ipSrcFileLocation.length() != 0)) {
			Map<String, Integer> map = readWeightedFile(ipSrcFileLocation, "IP source");
			ipSrcTable = new WeightedTable<String>(map, "IP source");
			ipSrcTable.display();
		}

		ip6revFileLocation = params.get(PLUGIN_IP6REVERSE);
		if ((ip6revFileLocation != null) && (ip6revFileLocation.length() != 0)) {
			Map<String, Integer> map = readWeightedFile(ip6revFileLocation, "IPv6 reverse");
			Map<BigInteger, Integer> map2 = new HashMap<BigInteger, Integer>();
			for (String ip6 : map.keySet()) {
				try {
					InetAddress in = InetAddress.getByName(ip6);
					if (in instanceof Inet6Address) {
						byte s[] = ((Inet6Address) in).getAddress();
						map2.put(new BigInteger(s), map.get(ip6));
					} else {
						logger.error("Address '{}' is not IPv6 for IPv6 reverse", ip6);
					}
				} catch (Exception e) {
					logger.error("Fail to load IPv6 addresses '{}' for IPv6 reverse", ip6);
				}
			}
			if (map2.size() != 0) {
				ip6revTable = new WeightedTable<BigInteger>(map2, "IPv6 reverse");
			}
		}

		ip4revFileLocation = params.get(PLUGIN_IP4REVERSE);
		if ((ip4revFileLocation != null) && (ip4revFileLocation.length() != 0)) {
			Map<String, Integer> map = readWeightedFile(ip4revFileLocation, "IPv4 reverse");
			Map<Integer, Integer> map2 = new HashMap<Integer, Integer>();
			for (String ip4 : map.keySet()) {
				try {
					InetAddress in = InetAddress.getByName(ip4);
					if (in instanceof Inet4Address) {
						byte s[] = ((Inet4Address) in).getAddress();
						map2.put(Integer.valueOf(Utils.byteArrayToInt(s)), map.get(ip4));
					} else {
						logger.error("Address '{}' is not IPv4 for IPv4 reverse", ip4);
					}
				} catch (Exception e) {
					logger.error("Fail to load IPv4 addresses '{}' for IPv4 reverse", ip4);
				}
			}
			if (map2.size() != 0) {
				ip4revTable = new WeightedTable<Integer>(map2, "IPv4 reverse");
			}
		}
	}

	/**
	 * Copy constructor (clone specimen object to get session object).
	 * 
	 * @param so
	 *            specimen object to clone
	 */
	private SessionObject(SessionObject so) {
	}

	// //////////////////////////////////////
	// SessionObjectAction implementation //
	// //////////////////////////////////////

	/**
	 * @see org.ow2.clif.scenario.isac.plugin.SessionObjectAction#createNewSessionObject()
	 */
	public Object createNewSessionObject() {
		return new SessionObject(this);
	}

	/**
	 * @see org.ow2.clif.scenario.isac.plugin.SessionObjectAction#close()
	 */
	public void close() {

	}

	/**
	 * @see org.ow2.clif.scenario.isac.plugin.SessionObjectAction#reset()
	 */
	public void reset() {

	}

	// ////////////////////////////////
	// ControlAction implementation //
	// ////////////////////////////////

	/**
	 * @see org.ow2.clif.scenario.isac.plugin.ControlAction#doControl()
	 */
	public void doControl(int number, Map<String, String> params) {
		switch (number) {
			case CONTROL_NEXT :
				this.getNext();
				return;
			default :
				throw new Error("Unable to find this control in ~DnsProvider~ ISAC plugin: " + number);
		}
	}

	/**
	 * 
	 */
	private void getNext() {
		synchronized (syncObject) {
			counterGet++;
			counterGetBI = counterGetBI.add(BigInteger.ONE);
			if (endDomainIndex != -1)
				domainIndex = startDomainIndex + (domainIndex % (endDomainIndex - startDomainIndex + 1));
			else
				domainIndex++;
			String indexStr = getIndexStr(domainIndex, indexLength);
			this.domainName = domainFormat.replaceAll("<index>", indexStr);
			if (zoneTable != null) {
				this.domainName = this.domainName.concat((String) zoneTable.getNext());
			}
			this.queryType = (String) queryTypeTable.getNext();
			this.ipSource = (String) ipSrcTable.getNext();
			this.dnssec = (String) dnsSecTable.getNext();
			this.rd = (String) flagRdTable.getNext();
			this.udp = (String) udpTable.getNext();
			if (ip4revTable != null)
				this.ip4rev = ip4revTable.getNext() + counterGet;
			if (ip6revTable != null)
				this.ip6rev = ip6revTable.getNext().add(counterGetBI);
			if (logger.isTraceEnabled()) {
				logger.trace("DnsProvides domain:'{}' query: '{}' ip-src: '" + this.ipSource + "' DNSSEC: " + this.dnssec
						+ " RD: " + this.rd + " UDP: " + this.udp, this.domainName, this.queryType);
				if (this.ip4rev != null) {
					logger.trace("IPv4 reverse, reverse @:'{}'", ip4toReverseDotted(this.ip4rev));
				}
				if (this.ip6rev != null) {
					logger.trace("IPv6 reverse: reverse @:'{}'", ip6toReverseDotted(this.ip6rev));
					logger.trace("IPv6 reverse: undotted @:'{}'", ip6toForwardUndotted(this.ip6rev));
				}
			}
		}
	}

	/**
	 * Convert an index into a string of 'len' length. 
	 * <p>left padding with '0' is done if required.
	 *  
	 * @param index
	 * @param len
	 * @return
	 */
	private static String getIndexStr(long index, int len) {
		String indexStr = String.valueOf(index);
		if (indexStr.length() > len) {
			indexStr = indexStr.substring(indexStr.length() - len, indexStr.length());
		} else {
			while (indexStr.length() < len) {
				indexStr = "0" + indexStr;
			}
		}
		return indexStr;
	}
	// ///////////////////////////////
	// DataProvider implementation //
	// ///////////////////////////////

	/**
	 * @see org.ow2.clif.scenario.isac.plugin.DataProvider#doGet()
	 */
	public String doGet(String var) {
		if (var.equals("domain")) {
			return this.domainName;
		}
		if (var.equals("query")) {

			return this.queryType;
		}
		if (var.equals("ip")) {
			return this.ipSource;
		}
		if (var.equals("dnssec")) {
			return this.dnssec;
		}
		if (var.equals("rd")) {
			return this.rd;
		}
		if (var.equals("udp")) {
			return this.udp;
		}
		if (var.equals("ip6rev")) {
			if (this.ip6rev != null)
				return ip6toReverseDotted(this.ip6rev);
			else
				throw new IsacRuntimeException("IPv6 address is not defined for reverse DNS");
		}
		if (var.equals("ip6for")) {
			if (this.ip6rev != null)
				return ip6toForwardUndotted(this.ip6rev);
			else
				throw new IsacRuntimeException("IPv6 address is not defined for reverse DNS");
		}
		if (var.equals("ip4rev")) {
			if (this.ip4rev != null)
				return ip4toReverseDotted(this.ip4rev);
			else
				throw new IsacRuntimeException("IPv4 address is not defined for reverse DNS");
		}
		throw new IsacRuntimeException("Unknown parameter value in ~DnsProvider~ ISAC plugin: " + var);
	}

	/**
	 * Load a CSV file into a MAP.
	 * <p>
	 * Each line of the file is formatted as follows: "key,weight"
	 * 
	 * @param fileLocation
	 * @param title
	 * @return
	 */
	public Map<String, Integer> readWeightedFile(String fileLocation, String title) {
		Map<String, Integer> map = new HashMap<String, Integer>();
		try {
			InputStream weightedFileIS;
			if ((weightedFileIS = ClifClassLoader.getClassLoader().getResourceAsStream(fileLocation)) == null) {
				logger.error("Cannot load file: " + fileLocation);
				throw new IsacRuntimeException("Could not instantiate DnsProvider session object:cannot load file "
						+ fileLocation);
			} else {
				BufferedReader br = new BufferedReader(new InputStreamReader(weightedFileIS));
				String strLine;
				// Read File Line By Line
				while ((strLine = br.readLine()) != null) {
					String[] str = strLine.split(",");
					if (str.length == 2) {
						try {
							Integer val = new Integer(str[1]);
							map.put(str[0], val);
						} catch (NumberFormatException e) {
							logger.error("Line '{}' of file is discarded", strLine);
						}
					}
				}
				weightedFileIS.close();
				logger.warn("'{}' File '{}' is loaded", title, fileLocation);
				return map;
			}
		} catch (IOException ioe) {
			logger.error("Cannot open zone file", ioe);
			throw new IsacRuntimeException("Could not instantiate DnsProvider session object:cannot locate file " + fileLocation);
		}
	}

	/**
	 * Create a weighted table with a ratio criteria
	 * 
	 * @param ratio
	 * @param title
	 * @return the wieghted table
	 */
	public WeightedTable<String> initRatioTable(Integer ratio, String title) {
		if (ratio < 0 || ratio > 100)
			throw new IllegalArgumentException("Cannot create weighted table '" + title + "'");
		Map<String, Integer> map = new HashMap<String, Integer>();
		map.put("1", ratio);
		map.put("0", (100 - ratio));
		return new WeightedTable<String>(map, title);
	}

	/**
	 * Create a reverse dotted representation of an IPv4 address
	 * 
	 * @param ip4
	 * @return the reverse dotted representation of ip4
	 */
	private String ip4toReverseDotted(int ip4) {
		byte[] tab = Utils.intToByteArray(ip4);
		StringBuffer sb = new StringBuffer();
		sb.append(tab[3] & 0xFF);
		sb.append(".");
		sb.append(tab[2] & 0xFF);
		sb.append(".");
		sb.append(tab[1] & 0xFF);
		sb.append(".");
		sb.append(tab[0] & 0xFF);
		return sb.toString();
	}

	/**
	 * Create a nibble reverse dotted representation of IPv6 address
	 * 
	 * @param ip6
	 * @return the nibble reverse dotted representation of ip6
	 */
	private String ip6toReverseDotted(BigInteger ip6) {
		byte[] tab = ip6.toByteArray();
		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < 16; i++) {
			sb.append(hex[(tab[15 - i] & 0xF)]);
			sb.append(".");
			sb.append(hex[(tab[15 - i] >> 4) & 0xF]);
			if (i != 15)
				sb.append(".");
		}
		return sb.toString();
	}

	/**
	 * 
	 * Create a representation of IPv6 address without ':' separator
	 * 
	 * @param ip6
	 * @return
	 */
	private String ip6toForwardUndotted(BigInteger ip6) {
		byte[] tab = ip6.toByteArray();
		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < 16; i++) {
			sb.append(hex[(tab[i] >> 4) & 0xF]);
			sb.append(hex[(tab[i] & 0xF)]);
		}
		return sb.toString();
	}

}
