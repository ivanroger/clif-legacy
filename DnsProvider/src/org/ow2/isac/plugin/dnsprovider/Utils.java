/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2013 France Telecom
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * See the "license.txt" provided along with this library or
 * the web site http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html for more details 
 * 
 * Contact : clif@ow2.org
 */
package org.ow2.isac.plugin.dnsprovider;

public class Utils {

	public static final byte[] intToByteArray(int value) {
		return (new byte[] { (byte) (value >>> 24), (byte) (value >>> 16), (byte) (value >>> 8), (byte) value });
	}

	public static final int byteArrayToInt(byte b[]) {
		if (b == null || b.length != 4)
			return 0;
		else
			return (b[0] << 24) + ((b[1] & 0xff) << 16) + ((b[2] & 0xff) << 8) + (b[3] & 0xff);
	}

}
