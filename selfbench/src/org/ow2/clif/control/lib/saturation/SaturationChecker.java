/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2012 France Telecom
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * Contact: clif@ow2.org
 */

package org.ow2.clif.control.lib.saturation;

import java.io.Serializable;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Observable;
import java.util.Observer;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.regex.Pattern;
import javax.jms.Session;
import javax.jms.Topic;
import javax.jms.TopicConnection;
import javax.jms.TopicPublisher;
import javax.jms.TopicSession;
import org.ow2.clif.supervisor.api.TestControl;


/**
 * Object that checks resources saturation according to a saturation policy
 * (basically a set resource usage thresholds). It implements the Observe interface
 * to be aware of blades appearing or disappearing.
 * @see SaturationController
 * @author Bruno Dillenseger
 */
public class SaturationChecker implements Runnable, Observer
{
	static public final String KEY_PROP_NAME = "saturation.key";

	private Properties policy;
	private TestControl tcItf;
	private Map<String,Set<ThresholdChecker>> checkers;
	private BlockingQueue<Map<String,long[]>> stats = new LinkedBlockingQueue<Map<String,long[]>>();
	private String key;
	private TopicSession sessionPub;
	private TopicPublisher saturationPub;
	private Observable configObs, statsObs;

	/**
	 * Creates a new saturation checker object attached to a given
	 * CLIF test, applying a given saturation policy, identified by a key
	 * provided by the policy through the saturation.key property.
	 * The StatPublisher's blade polling period gives the period of this
	 * saturation control loop.
	 * The policy rules are specified through these properties:
	 * <ul>
	 *	<li>saturation.1.ids=cpu sut.*</li> <i>blades of interest are
	 *	specified through regular expressions matched against their
	 *	blade identifiers</i></li>
	 *	<li>saturation.1.index=0</li> <i>index of the metric of interest
	 *	in the array of monitoring statistics for the target blades</i></li>
	 *	<li>saturation.1.max=80</li> <i>saturation high threshold (inclusive);
	 *	'min' instead of 'max' defines a low threshold<i></li>
	 * </ul>
	 * @param supervisor the TestControl interface of the target CLIF
	 * test's supervisor component
	 * @param policy properties that specify the policy key and the
	 * saturation thresholds for the given metrics of given
	 * blades among the deployed test plan.
	 * @param topic the JMS "saturation" topic
	 * @param connection a JMS connection to create a topic session
	 * @param configObs an Observable object bound to the SaturationController
	 * instance, responsible for forwarding reconfiguration messages
	 * @param statsObs an Observable object bound to the SaturationController
	 * instance, responsible for forwarding blade statistics
	 * @see java.util.regex.Pattern
	 */
	public SaturationChecker(
		TestControl supervisor,
		Properties policy,
		Topic topic,
		TopicConnection connection,
		Observable configObs,
		Observable statsObs)
	throws Exception
	{
		tcItf = supervisor;
		this.policy = policy;
		key = policy.getProperty(KEY_PROP_NAME);
	    sessionPub = connection.createTopicSession(false, Session.AUTO_ACKNOWLEDGE);
	    saturationPub = sessionPub.createPublisher(topic);
	    this.configObs = configObs;
	    configObs.addObserver(this);
	    this.statsObs = statsObs;
	    statsObs.addObserver(this);
		update(supervisor.getBladesIds());
	}


	/**
	 * Checks saturation of blades as defined in the policy.
	 * @throws SaturationException when saturation is detected.
	 * This exception gives the saturated blade's identifier
	 * and the value that reached the threshold, as well as
	 * the key identifying the violated saturation policy.
	 */
	private synchronized void check()
	throws SaturationException
	{
		try
		{
			Map<String,long[]> allStats = stats.take();
			for (Map.Entry<String,Set<ThresholdChecker>> entry : checkers.entrySet())
			{
				long[] bladeStats = allStats.get(entry.getKey());
				System.out.println("stats(" + entry.getKey() + ") = " + Arrays.toString(bladeStats));
				if (bladeStats != null)
				{
					for (ThresholdChecker checker : entry.getValue())
					{
						if (! checker.check(bladeStats))
						{
							throw new SaturationException(
								entry.getKey(),
								checker,
								bladeStats[checker.getIndex()],
								key);
						}
					}
				}
			}
		}
		catch (InterruptedException ex)
		{
			// just exit from checking if taking a new stats record was interrupted
		}
	}


	/**
	 * Creates the threshold checkers according to the policy
	 * and the provided blades
	 * @param bladeIds the identifiers of the blades currently
	 * running in the CLIF test plan
	 */
	synchronized void update(String[] bladeIds)
	{
		checkers = new HashMap<String,Set<ThresholdChecker>>();
		for (String key : policy.stringPropertyNames())
		{
			if (key.startsWith("saturation.") && key.endsWith(".ids"))
			{
				Pattern targets = Pattern.compile(policy.getProperty(key));
				String prefix = key.substring(0, key.indexOf(".ids"));
				int index = Integer.parseInt(policy.getProperty(prefix + ".index"));
				String thresholdStr;
				long min;
				thresholdStr = policy.getProperty(prefix + ".min");
				if (thresholdStr == null)
				{
					min = Long.MIN_VALUE;
				}
				else
				{
					min = Long.parseLong(thresholdStr);
				}
				long max;
				thresholdStr = policy.getProperty(prefix + ".max");
				if (thresholdStr == null)
				{
					max = Long.MAX_VALUE;
				}
				else
				{
					max = Long.parseLong(thresholdStr);
				}
				for (String id : bladeIds)
				{
					if (targets.matcher(id).matches())
					{
						Set<ThresholdChecker> checkerSet = checkers.get(id);
						if (checkerSet == null)
						{
							checkerSet = new HashSet<ThresholdChecker>();
							checkers.put(id, checkerSet);
						}
						checkerSet.add(
							new ThresholdChecker(
								index,
								tcItf.getStatLabels(id)[index],
								min,
								max));
					}
				}
			}
		}
	}


	////////////////////////
	// interface Runnable //
	////////////////////////


	/**
	 * Saturation control loop.
	 */
	public void run()
	{
		try
		{
			// the loop is ready for the policy identified by key
			saturationPub.publish(sessionPub.createObjectMessage(key));
			try
			{
				check();
			}
			catch (SaturationException ex)
			{
				// ignore first occurrence (may result from past overload)
			}
			try
			{
				while (true)
				{
					check();
				}
			}
			catch (SaturationException ex)
			{
				System.out.println(ex.toString());
				saturationPub.publish(sessionPub.createObjectMessage(ex));
			}
		}
		catch (Exception ex)
		{
			ex.printStackTrace(System.err);
		}
		System.out.println("End of saturation control loop.");
		configObs.deleteObserver(this);
		statsObs.deleteObserver(this);
	}


	////////////////////////
	// interface Observer //
	////////////////////////


	/**
	 * Must be invoked when the CLIF test's blades have changed
	 * in order to create a new set of threshold checkers
	 */
	@SuppressWarnings("unchecked")
	public void update(Observable obs, Object arg)
	{
		if (obs == configObs && arg instanceof String[])
		{
			update((String[])arg);
		}
		else if (obs == statsObs && arg instanceof HashMap<?,?>)
		{
			stats.add((HashMap<String,long[]>)arg);
		}
	}


	/**
	 * Internal class for checking low and/or high thresholds
	 * for a value at a given index in an array of integers.
	 */
	class ThresholdChecker implements Serializable
	{
		private static final long serialVersionUID = -6987848732396090694L;

		int index;
		String label;
		long min;
		long max;

		ThresholdChecker(
			int statIndex,
			String label,
			long lowThreshold,
			long highThreshold)
		{
			this.index = statIndex;
			this.label = label;
			this.min = lowThreshold;
			this.max = highThreshold;
		}

		boolean check(long[] sample)
		{
			return sample[index] > min && sample[index] < max;
		}

		int getIndex()
		{
			return index;
		}

		public String toString()
		{
			String result = label;
			if (min != Long.MIN_VALUE)
			{
				result = min + " < " + result;
			}
			if (max != Long.MAX_VALUE)
			{
				result = result + " < " + max;
			}
			return result;
		}
	}
}
