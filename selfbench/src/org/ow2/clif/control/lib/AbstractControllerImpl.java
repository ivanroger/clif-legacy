/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2012 France Telecom
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * Contact: clif@ow2.org
 */

package org.ow2.clif.control.lib;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.regex.Pattern;
import javax.jms.TopicConnectionFactory;
import javax.naming.Context;
import org.objectweb.fractal.api.control.BindingController;
import org.ow2.clif.console.lib.batch.JNDI;
import org.ow2.clif.supervisor.api.TestControl;


/**
 * Abstract class for CLIF controllers, used to build autonomic control loops.
 * Can be used either as a Fractal component bound to the supervisor of a
 * CLIF application, or stand-alone without binding. In the latter case, the
 * reference to the supervisor must be provided to the constructor. The typical
 * way is to get the Clif Application reference from the Registry, and to look
 * for the supervisor component and its TestControl interface. The ClifAppFacade
 * utility class provides a valuable support in this case.
 * 
 * @author Bruno Dillenseger
 */
abstract public class AbstractControllerImpl implements BindingController
{
	static protected Context jndi;
	static protected TopicConnectionFactory tcf;

	static {
		jndi = JNDI.init();
		try
		{
			tcf = (TopicConnectionFactory) jndi.lookup("tcf");
		}
		catch (Exception ex)
		{
			throw new Error("Can't get topic connection factory.", ex);
		}
	}


	/** test control interface on the supervisor */
	protected TestControl tcItf;


	/**
	 * This constructor is used when this controller component will
	 * not be bound to the supervisor.
	 * @param tcItf the supervisor's "Test control" interface
	 */
	public AbstractControllerImpl(TestControl tcItf)
	{
		this();
		this.tcItf = tcItf;
	}


	/**
	 * This constructor is used when this controller component will be
	 * bound to the supervisor later on, in order to be operational.
	 */
	public AbstractControllerImpl()
	{
	}


	/**
	 * Get identifiers of all blades bound to the CLIF supervisor
	 * @return blades identifiers
	 */
	public Set<String> getAllBlades()
	{
		return new HashSet<String>(Arrays.asList(tcItf.getBladesIds()));
	}


	/**
	 * Get identifiers of all blades bound to the CLIF supervisor, matching
	 * the given regular expression (see Java regular expressions)
	 * @param regex the regular expression that the blade identifiers must match
	 * @return blades identifiers matching the regular expression
	 * @see java.util.regex.Pattern
	 */
	public Set<String> getBlades(String regex)
	{
		Set<String> blades = new HashSet<String>();
		Pattern selection = Pattern.compile(regex);
		for (String id : tcItf.getBladesIds())
		{
			if (selection.matcher(id).matches())
			{
				blades.add(id);
			}
		}
		return blades;
	}


	/////////////////////////////////
	// interface BindingController //
	/////////////////////////////////


	public Object lookupFc(String clientItfName)
	{
		if (clientItfName.equals(TestControl.TEST_CONTROL))
		{
			return tcItf;
		}
		else
		{
			return null;
		}
	}


	public void bindFc(String clientItfName, Object serverItf)
	{
		if (clientItfName.equals(TestControl.TEST_CONTROL))
		{
			tcItf = (TestControl) serverItf;
		}
	}


	public void unbindFc(String clientItfName)
	{
		if (clientItfName.equals(TestControl.TEST_CONTROL))
		{
			tcItf = null;
		}
	}


	public String[] listFc()
	{
		return new String[] { TestControl.TEST_CONTROL };
	}
}
