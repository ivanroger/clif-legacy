/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2012 France Telecom
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * Contact: clif@ow2.org
 */

package org.ow2.clif.control.lib.srli;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.ObjectMessage;
import javax.jms.Session;
import javax.jms.TextMessage;
import javax.jms.Topic;
import javax.jms.TopicConnection;
import javax.jms.TopicPublisher;
import javax.jms.TopicSession;
import javax.jms.TopicSubscriber;
import org.ow2.clif.control.lib.AbstractControllerImpl;
import org.ow2.clif.control.lib.saturation.SaturationChecker;
import org.ow2.clif.control.lib.saturation.SaturationException;
import org.ow2.clif.control.lib.srli.util.CsvWriter;
import org.ow2.clif.control.lib.srli.util.StringUtil;
import org.ow2.clif.supervisor.api.ClifException;
import org.ow2.clif.supervisor.api.TestControl;

/**
 * Load injection controller component.
 * Its starts its activity when it receives a Properties instance through
 * JMS, on topic "benchmark", stating the load injection policy and the
 * saturation detection policy for the system under test. This policy is
 * forwarded to the "saturation" JMS topic.
 * 
 * @author Bruno Dillenseger
 * @author Colin PUY
 * @author Nabila Salmi
 */
public class InjectionController
	extends AbstractControllerImpl
	implements MessageListener, Runnable
{
	// properties names for self-regulated load injection parameters
	static public final String INJECTORS_PROP_NAME = "injection.ids";
	static public final String BEHAVIOR_PROP_NAME = "injection.behavior";
	static public final String MAXERRORS_PROP_NAME = "injection.maxerrors";
	static public final String MINIMALREQUESTS_PROP_NAME = "injection.minimal";
	static public final String RAMPUPTIME_PROP_NAME = "injection.rampup.time";
	static public final String RAMPUPSTEPS_PROP_NAME = "injection.rampup.steps";
	static public final String STABILIZATIONTIME_PROP_NAME = "injection.stabilization.time";

	// total number of active virtual users
	private int vUsers;

	// step number
	private long step;

	// step stabilization time
	private float stabilizationTime;

	// live-computed necessary number of measures for current step
	private int m_SamplingMeasureNumber;

	// currently assumed Cmax
	private double cMax;

	// initial Cmax
	private double cMax0;

	// currently assumed number of servers K wrt the SUT's queueing model
	private int nServers;

	// per-vUser throughput (req/s) as defined in the behavior
	private double m_NbRequestsPerVuser;

	// number of steps to reach the assumed Cmax
	private int rampupSteps;

	// total test duration
	private long totalTime;

	// ramp-up duration (ms)
	private long rampupTime;

	// minimal number of necessary samples for any step
	private int minimalStepRequests;

	protected BlockingQueue<Map<String,long[]>> statsQ = null;
	protected Set<String> bladeIds;
	protected String behaviorName;
	protected volatile boolean stopped = true; // means no SRLI policy is active
	protected String injectorsRegex;
	protected TopicConnection connection;
	protected TopicSession benchSessionPub, benchSessionSub;
	protected TopicSession satSessionPub, satSessionSub;
	protected TopicSession statsSessionSub;
	protected TopicPublisher saturationPub;
	protected TopicSubscriber saturationSub;
	protected TopicPublisher benchmarkPub;
	protected TopicSubscriber benchmarkSub;
	protected TopicSubscriber statsSub;
	protected String saturationKey;
	protected CsvWriter csvWriter;
	protected int previousVusers;


	/**
	 * Creates a new load injection controller attached to the provided
	 * CLIF test supervisor's TestControl interface
	 * @param tcItf
	 * @throws Exception
	 */
	public InjectionController(TestControl tcItf)
		throws Exception
	{
		super(tcItf);
		connection = tcf.createTopicConnection(SelfbenchConstants.USERNAME, SelfbenchConstants.PASSWORD);
		benchSessionPub = connection.createTopicSession(false, Session.AUTO_ACKNOWLEDGE);
		benchSessionSub = connection.createTopicSession(false, Session.AUTO_ACKNOWLEDGE);
		satSessionPub = connection.createTopicSession(false, Session.AUTO_ACKNOWLEDGE);
		satSessionSub = connection.createTopicSession(false, Session.AUTO_ACKNOWLEDGE);
		statsSessionSub = connection.createTopicSession(false, Session.AUTO_ACKNOWLEDGE);
		Topic benchTopic = (Topic)jndi.lookup(SelfbenchConstants.BENCHMARK_TOPIC);
		Topic satTopic = (Topic)jndi.lookup(SelfbenchConstants.SATURATION_TOPIC);
		Topic statsTopic = (Topic)jndi.lookup(SelfbenchConstants.STATS_TOPIC);
		benchmarkPub = benchSessionPub.createPublisher(benchTopic);
	    saturationPub = satSessionPub.createPublisher(satTopic);
	    benchmarkSub = benchSessionSub.createSubscriber(benchTopic, null, true);
	    benchmarkSub.setMessageListener(this);
	    saturationSub = satSessionSub.createSubscriber(satTopic, null, true);
	    saturationSub.setMessageListener(this);
	    statsSub = statsSessionSub.createSubscriber(statsTopic, null, true);
	    statsSub.setMessageListener(this);
	    connection.start();
	}


	protected void nextStep()
	{
		try
		{
			// first step: initialize load injectors, if any 
			if (step == 0)
			{
				changeInjectorsParameter();
				if (bladeIds.isEmpty())
				{
					return;
				}
			}

			// Next step
			step++;

			System.out.println("");
			System.out.println(SelfbenchConstants.PRINT_SEPARATION_LINE);
			System.out.println("                                        " +
					"Step #" + step + " : " + vUsers + " virtual users...");
			System.out.println(SelfbenchConstants.PRINT_SEPARATION_LINE);

			// Values to write to the CSV file
			List<Object> csvBeginStep = new ArrayList<Object>();
			csvBeginStep.add("Step" + step);
			csvBeginStep.add(vUsers);

			purgeStats();
			csvBeginStep.add(totalTime);
			csvBeginStep.add(stabilizationTime);

			// sleep during the workload ramp-up duration
			System.out.println("Ramp up " + (rampupTime / 1000) + " s...");
			Thread.sleep(rampupTime);

			// dummy call to getStat() to update totalTime
			purgeStats();
			long timeAfterRampUp =  totalTime;

			if (stopped)
			{
				return;
			}

			// sleep during stabilization time
			System.out.println(
				"Start stabilization time " 
				+ StringUtil.formatNumber(stabilizationTime, 2)
				+ " s...");
			Thread.sleep((long) (stabilizationTime * 1000f));

			System.out.println("End of stabilization time");
			System.out.println("Start sampling duration");
			System.out.println(" Minimal number of measures to get = " + minimalStepRequests);

			// initial call to getStats()
			long[][] statsBeforeSampling = getAllStats();
			long nbActionsBeforeSampling = calculateNbActionsBeforeSampling(statsBeforeSampling);
			csvBeginStep.add(nbActionsBeforeSampling);

			// call getStats() as long as the number of requests is not sufficient
			List<long[][]> samplingInjectorStats = new ArrayList<long[][]>();
			samplingInjectorStats.add(getAllStats());

			long sampleNumberAction = 
				numberOfNewActions(samplingInjectorStats.get(0), statsBeforeSampling);

			// compute the actual necessary number of requests
			double meanR = 
				InjectionDriver.calculateMeanResponseTime(samplingInjectorStats, statsBeforeSampling);

			double varR = 
				InjectionDriver.calculateSampleVariance(samplingInjectorStats, statsBeforeSampling);

			m_SamplingMeasureNumber = 
				InjectionDriver.calculateSamplingMeasuresNumber(meanR, varR);

			// As long as the number of requests is not sufficient
			// (and at least 2 for the variance calculus) 
			System.out.println(SelfbenchConstants.PRINT_SEPARATION_LINE);
			printResultTabColumnsTitle();
			System.out.println(SelfbenchConstants.PRINT_SEPARATION_LINE);					

			while (
				(sampleNumberAction < m_SamplingMeasureNumber || sampleNumberAction < minimalStepRequests)
				&& ! stopped)
			{
				printResultLine(sampleNumberAction, 
						sampleNumberAction < minimalStepRequests ?
								"-" : String.valueOf(m_SamplingMeasureNumber), 
								meanR, varR, "No");

				// get latest statistics
				long[][] stats = getAllStats();
				samplingInjectorStats.add(stats);

				// number of requests performed during this sample
				sampleNumberAction = numberOfNewActions(stats, statsBeforeSampling);

				// necessary number of requests
				meanR = InjectionDriver.calculateMeanResponseTime(
						samplingInjectorStats, statsBeforeSampling);

				varR = InjectionDriver.calculateSampleVariance(
						samplingInjectorStats, statsBeforeSampling);

				m_SamplingMeasureNumber = 
					InjectionDriver.calculateSamplingMeasuresNumber(meanR, varR);
			}

			if (stopped)
			{
				return;
			}

			System.out.println(SelfbenchConstants.PRINT_SEPARATION_LINE);
			printResultLine(
				sampleNumberAction,
				String.valueOf(m_SamplingMeasureNumber),
				meanR,
				varR,
				"YES");
			System.out.println(SelfbenchConstants.PRINT_SEPARATION_LINE);
			System.out.println();
			System.out.println("End of sampling duration");

			csvBeginStep.add(sampleNumberAction);
			csvBeginStep.add(meanR);
			csvBeginStep.add(Math.sqrt(varR));
			csvBeginStep.add(nServers);

			// When on the first workload step (1 vUser), compute throughput and Cmax0
			double newCmax = cMax;
			int newNbServers = nServers;
			if (step == 1)
			{
				cMax0 = (double) 1 / meanR;
				newCmax = cMax0;
				cMax = newCmax;

				m_NbRequestsPerVuser = InjectionDriver.calculateActionThroughputPerUser(
					samplingInjectorStats,
					statsBeforeSampling,
					vUsers);
				csvWriter.writeTitleLine();
				csvWriter.flush();
			}
			// otherwise, update Cmax assumption when necessary
			else if (m_NbRequestsPerVuser * vUsers > cMax)
			{
				newNbServers++;
				newCmax = cMax0 * newNbServers;
			}

			// compute injection rate (throughput)
			double current_lambda = (double) vUsers * m_NbRequestsPerVuser * 1000;
			csvBeginStep.add(cMax * 1000);
			csvBeginStep.add(current_lambda);
			System.out.println();
			System.out.println("  Server's parallelism capability : " + nServers);
			System.out.println(
				"  Cmax (server's theoretical maximum throughput) : " 
				+ StringUtil.formatNumber(cMax * 1000, 2)
				+ " request / s");
			System.out.println(
				"  Lambda (injection rate) : " 
				+ StringUtil.formatNumber(current_lambda, 2)
				+ " request / s");
			System.out.println();
			System.out.print("  Computation of next step v-users number : ");
			System.out.flush();

			// Compute the virtual user increment
			int increment = 
				InjectionDriver.calculateVUserIncrement(cMax, m_NbRequestsPerVuser, rampupSteps);
			if (increment == 0)
			{
				System.err.println("Problem: Vusers increment = 0, can't continue");
				return;
			}

			previousVusers = vUsers;
			vUsers += increment;
			cMax = newCmax;
			nServers = newNbServers;

			System.out.println(vUsers);
			System.out.print("  Computation of next step stabilization time: ");
			System.out.flush();

			// compute stabilization time for the next workload step
//
// current implementation of stabilization time is much too time consuming
// as long as a new "quick" implementation is not available, just
// keep the starting value, as set in the properties file
//			double next_lambda = (double) vUsers * m_NbRequestsPerVuser * 1000;
//			stabilizationTime = InjectionDriver.calculateStabilizationTime(
//					next_lambda, previousVusers, vUsers, meanR);

			System.out.println(StringUtil.formatNumber(stabilizationTime, 2) + " s");

			// dummy call to getStats() to update totalTime
			purgeStats();

			csvBeginStep.add(timeAfterRampUp);
			csvWriter.writeLine(csvBeginStep);
			csvWriter.flush();

			// update the load injectors' population of virtual users
			changeInjectorsParameter();
		}
		catch (InterruptedException ex)
		{
			System.err.println(ex + " in " + toString());
		}
		catch (Exception ex)
		{
			ex.printStackTrace(System.err);
		}
	}


	public void initParameters(Properties policy)
	{
		step = 0;
		nServers = 1;
		totalTime = 0;
		vUsers = 1;
		previousVusers = 0;
		rampupTime = Long.valueOf(policy.getProperty(RAMPUPTIME_PROP_NAME));
		stabilizationTime = Long.valueOf(policy.getProperty(STABILIZATIONTIME_PROP_NAME));
		rampupSteps = Integer.valueOf(policy.getProperty(RAMPUPSTEPS_PROP_NAME));
		minimalStepRequests = Integer.valueOf(policy.getProperty(MINIMALREQUESTS_PROP_NAME));
		m_SamplingMeasureNumber = minimalStepRequests;
		behaviorName = policy.getProperty(BEHAVIOR_PROP_NAME);
		injectorsRegex = policy.getProperty(INJECTORS_PROP_NAME);
	}


	private void changeInjectorsParameter() throws ClifException
	{
		bladeIds = getBlades(injectorsRegex);
		if (! bladeIds.isEmpty())
		{
			long population = 1 + vUsers / bladeIds.size();
			int extra = vUsers % bladeIds.size();
			Iterator<String> bladeIter = bladeIds.iterator();
			for (int i=0 ; i < bladeIds.size() ; ++i)
			{
				if (i == extra)
				{
					--population;
				}
				tcItf.changeParameter(
					bladeIter.next(),
					"population", 
					behaviorName + "=" + population);
			}
		}
	}


	/**
	 * Gets load injectors stats and updates the time counter m_TotalTime
	 * @return moving statistical values of all load injectors
	 */
	private long[][] getAllStats()
	{
		long[][] allStats = null;

		try
		{
			Map<String,long[]> stats = statsQ.take();
			if (!stats.isEmpty())
			{
				allStats = new long[bladeIds.size()][];
				int i=0;
				for (String bladeId : bladeIds)
				{
					allStats[i++] = stats.get(bladeId);
				}
				totalTime += allStats[0][0];
			}
		}
		catch (InterruptedException ex)
		{
			return null;
		}
		return allStats;
	}


	private synchronized void purgeStats()
	{
		while (!statsQ.isEmpty())
		{
			Map<String,long[]> stats = statsQ.poll();
			if (!stats.isEmpty())
			{
				totalTime += stats.values().iterator().next()[0];
			}
		}
	}

	/**
	 * Print the results table column title
	 */
	private void printResultTabColumnsTitle()
	{
		System.out.print(StringUtil.formatOutputCenter("Measures", SelfbenchConstants.SPACE_COLUMN_NUMBER));
		System.out.print(StringUtil.formatOutputCenter("Measures", SelfbenchConstants.SPACE_COLUMN_NUMBER));
		System.out.print(StringUtil.formatOutputCenter("Response time (ms)", SelfbenchConstants.SPACE_COLUMN_NUMBER));
		System.out.print(StringUtil.formatOutputCenter("Response time (ms)", SelfbenchConstants.SPACE_COLUMN_NUMBER));
		System.out.print(StringUtil.formatOutputCenter("Convergence", SelfbenchConstants.SPACE_COLUMN_NUMBER));
		System.out.println("|");
		System.out.print(StringUtil.formatOutputCenter("Current number", SelfbenchConstants.SPACE_COLUMN_NUMBER));
		System.out.print(StringUtil.formatOutputCenter("Optimal number", SelfbenchConstants.SPACE_COLUMN_NUMBER));
		System.out.print(StringUtil.formatOutputCenter("Measured", SelfbenchConstants.SPACE_COLUMN_NUMBER));
		System.out.print(StringUtil.formatOutputCenter("Standard deviation", SelfbenchConstants.SPACE_COLUMN_NUMBER));
		System.out.print(StringUtil.formatOutputCenter(" ", SelfbenchConstants.SPACE_COLUMN_NUMBER));
		System.out.println("|");
	}

	/**
	 * Prints a line in result table
	 *
	 * @param p_SampleNumberAction the  sample number action
	 * @param p_SamplingMeasureNumber the sampling measure number
	 * @param p_MeanR the mean response time
	 * @param p_VarR the response time variance
	 */
	private void printResultLine(
		long p_SampleNumberAction,
		String p_SamplingMeasureNumber,
		double p_MeanR, 
		double p_VarR,
		String p_Convergence)
	{
		System.out.print(StringUtil.formatOutput(
			p_SampleNumberAction,
			SelfbenchConstants.SPACE_COLUMN_NUMBER));
		System.out.print(StringUtil.formatOutput(
			p_SamplingMeasureNumber,
			SelfbenchConstants.SPACE_COLUMN_NUMBER));
		System.out.print(StringUtil.formatOutput(
			StringUtil.formatNumber(p_MeanR, 3),
			SelfbenchConstants.SPACE_COLUMN_NUMBER));
		System.out.print(StringUtil.formatOutput(
			StringUtil.formatNumber(Math.sqrt(p_VarR), 3),
			SelfbenchConstants.SPACE_COLUMN_NUMBER));
		System.out.print(StringUtil.formatOutput(
			p_Convergence,
			SelfbenchConstants.SPACE_COLUMN_NUMBER));
		System.out.println("|");
	}

	/**
	 * Calculate the number of requests before sampling.
	 * @param p_StatsBeforeSampling the moving load injectors statistics before sampling
	 * @return the number of actions before sampling
	 */
	private long calculateNbActionsBeforeSampling(long[][] p_StatsBeforeSampling)
	{
		long nbActionsBeforeSampling = 0;

		if (p_StatsBeforeSampling != null)
		{
			for (long[] stats : p_StatsBeforeSampling)
			{
				if (stats != null)
				{
					nbActionsBeforeSampling += stats[4];
				}
			}
		}
		return nbActionsBeforeSampling;
	}

	/**
	 * Calculate the total number of requests performed during a sample.
	 * @param p_CurrentStats the latest statistical values of all injectors
	 * @param p_statsBeforeSampling the statistical values before starting sampling
	 * @return the total number of requests performed during current sample
	 */
	private long numberOfNewActions(
		long[][] p_CurrentStats,
		long[][] p_statsBeforeSampling)
	{
		long result = 0;

		for (int i = 0; i < p_CurrentStats.length; i++)
		{
			if (p_CurrentStats[i] != null && p_statsBeforeSampling[i] != null)
			{
				result += p_CurrentStats[i][4] - p_statsBeforeSampling[i][4];
			}
		}
		return result;
	}


	///////////////////////////////
	// interface MessageListener //
	///////////////////////////////


	@SuppressWarnings("unchecked")
	public synchronized void onMessage(Message msg)
	{
		try
		{
			if (msg instanceof TextMessage)
			{
				System.out.println(((TextMessage)msg).getText());
			}
			else if (
				msg instanceof ObjectMessage
				&& ((ObjectMessage)msg).getObject() instanceof Properties
				&& stopped)
			{
				// start benchmark
				Properties policy = (Properties)((ObjectMessage)msg).getObject();
				stopped = false;
				saturationKey = policy.getProperty(SaturationChecker.KEY_PROP_NAME);
				initParameters(policy);
				saturationPub.publish(satSessionPub.createObjectMessage(policy));
			}
			else if (
				msg instanceof ObjectMessage
				&& ((ObjectMessage)msg).getObject() instanceof HashMap<?,?>)
			{
				if (statsQ != null)
				{
					statsQ.add((HashMap<String,long[]>)((ObjectMessage)msg).getObject());
				}
			}
			else if (
				msg instanceof ObjectMessage
				&& ((ObjectMessage)msg).getObject() instanceof SaturationException
				&& ! stopped)
			{
				// notification: saturation reached
				if (((SaturationException)((ObjectMessage)msg).getObject()).getKey().equals(saturationKey))
				{
					stopped = true;
				}
			}
			else if (
				msg instanceof ObjectMessage
				&& ((ObjectMessage)msg).getObject().equals(saturationKey)
				&& ! stopped)
			{
				// notification: saturation control is ready
				new Thread(this, "Injection control loop").start();
			}
			else
			{
				System.out.println("Ignored message: " + msg);
			}
		}
		catch (JMSException ex)
		{
			ex.printStackTrace(System.err);
		}
	}


	////////////////////////
	// interface Runnable //
	////////////////////////


	public void run()
	{
		try
		{
			csvWriter = CsvWriter.newInstance(SelfbenchConstants.CSV_LOADINJECTION_FILE_NAME);
			csvWriter.setTitleLine(SelfbenchConstants.CSV_LOADINJECTION_TITLES);
			statsQ = new LinkedBlockingQueue<Map<String,long[]>>();
			while (! stopped)
			{
				nextStep();
			}
			synchronized (this)
			{
				statsQ = null;
			}
			csvWriter.close();
			vUsers = 0;
			changeInjectorsParameter();
			benchmarkPub.publish(benchSessionPub.createObjectMessage(previousVusers));
		}
		catch (Exception ex)
		{
			ex.printStackTrace(System.err);
		}
		System.out.println("End of load injection control loop.");
	}
}
