/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2012 France Telecom
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * Contact: clif@ow2.org
 */

package org.ow2.clif.control.lib.srli.util;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import org.ow2.clif.control.lib.srli.SelfbenchConstants;


/**
 * Utility class for writing CSV-formatted files
 * @author Colin PUY
 */
public class CsvWriter
{
	/**
	 * Creates a new CSV file. If the file name already exists, it is renamed
	 * with a "-number" suffix, and a new file is created
	 * @param fileName the new CSV file to create
	 * @return the CsvWriter object associated to the new CSV file
	 */
	static public synchronized CsvWriter newInstance(String fileName)
	{
		File path = new File(fileName);
		File tmp = path;
		int i=0;
		while (tmp.exists())
		{
			tmp = new File(fileName + "-" + ++i);
		}
		if (i != 0)
		{
			path.renameTo(tmp);
		}
		return new CsvWriter(path);
	}

	private PrintWriter m_PrintWriter;
	private String m_ColumnsTitle = null;

	/**
	 * Creates a new, empty CSV-formatted file
	 * @param path the new CSV file's name, relative path or absolute path
	 */
	private CsvWriter(File path)
	{
		try 
		{
			m_PrintWriter =  new PrintWriter(new BufferedWriter(new FileWriter(path)));
		} 
		catch (IOException e) 
		{
			e.printStackTrace(System.err);
		}
	}

	public void setTitleLine(String columnsTitles)
	{
		m_ColumnsTitle = columnsTitles;
	}

	public void writeTitleLine()
	{
		if (m_ColumnsTitle != null)
		{
			m_PrintWriter.println(m_ColumnsTitle);
		}
	}

	/**
	 * Adds a line of values to the CSV file
	 * @param values the list of values	to add to the CSV file as a new line
	 */
	public void writeLine(List<Object> values)
	{
		if (values != null && values.size() > 0)
		{
			StringBuilder sb = new StringBuilder();
			sb.append(values.get(0));
			for (int i = 1; i < values.size(); i++) 
			{
				sb.append(SelfbenchConstants.CSV_SEP);
				sb.append(values.get(i));
			}
			m_PrintWriter.println(sb.toString());
		}
	}

	public void close()
	{
		m_PrintWriter.close();
	}

	public void flush()
	{
		m_PrintWriter.flush();
	}
}
