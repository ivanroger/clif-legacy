/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2012 France Telecom
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * Contact: clif@ow2.org
 */
package org.ow2.clif.control.lib.srli;

/**
 * @author Colin Puy
 * @author Bruno Dillenseger
 */
public abstract class SelfbenchConstants
{
	public static final String USERNAME = "anonymous";
	public static final String PASSWORD = "anonymous";
	public static final String SATURATION_TOPIC = "saturation";
	public static final String CONFIGURATION_TOPIC = "configuration";
	public static final String BENCHMARK_TOPIC = "benchmark";
	public static final String STATS_TOPIC = "stats";
	public static final String SELFBENCH_PROPS_FILE = "selfbench.props";

	/** name of output file where to write the load injection steps */
	public static final String CSV_LOADINJECTION_FILE_NAME = "selfbench.csv";

	public static final String CSV_LOADINJECTION_TITLES =
		"Step;Nb. vusers;Start Date;Stabilisation Time;Nb action before sampling;" +
		"Sampling action number;Mean(R) (ms);StDev;Nb. servers;Current Cmax (req / s);" +
		"Injection rate (Lambda);End ramp up Date";
	
	/** separator character for CSV-formatted files */
	public static final String CSV_SEP = ";";

	public static final double STABILISATION_TIME_EPSILON = Math.pow(10, -8);
	
	/** z=1.96 for 95% confidence interval */
	public static final float SAMPLING_DURATION_Z = 1.96f;

	public static final float SAMPLING_DURATION_EPSILON = 0.1f;

	// constants for fancy printing of load injection steps
	public static final String PRINT_SEPARATION_LINE = 
		"------------------------------------------------------------------------------------------------------------------------------";
	public static final String PRINT_END_TEST =
		"                                                     END OF TEST";
	public static final int SPACE_COLUMN_NUMBER = 24;
}
