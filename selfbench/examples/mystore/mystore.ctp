#CLIF test plan
#Tue Jan 17 15:01:43 CET 2012

blade.0.id=injector1
blade.0.injector=IsacRunner
blade.0.server=clif1
blade.0.argument=mystore.xis jobdelay=500 threads=200
blade.0.comment=

blade.1.id=injector2
blade.1.injector=IsacRunner
blade.1.server=clif2
blade.1.argument=mystore.xis jobdelay=500 threads=200
blade.1.comment=

blade.2.id=jvm sut
blade.2.probe=org.ow2.clif.probe.jmx_jvm.Insert
blade.2.server=local host
blade.2.argument=1000 999999 jmx_jvm.properties
blade.2.comment=

blade.3.id=cpu sut
blade.3.probe=org.ow2.clif.probe.cpu.Insert
blade.3.server=sut
blade.3.argument=1000 999999
blade.3.comment=
