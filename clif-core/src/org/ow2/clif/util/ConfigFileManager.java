/*
* CLIF is a Load Injection Framework
* Copyright (C) 2015-2016 Orange SA
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
* Contact: clif@ow2.org
*/

package org.ow2.clif.util;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.net.InetAddress;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import org.objectweb.fractal.rmi.registry.Registry;

/**
 * Helper class for managing the CLIF configuration file (typically named clif.opts)
 * with regard to network-related properties:
 * <ul>
 *   <li>fractal.registry.host and fractal.registry.port give the network address
 *   of the CLIF/Fractal registry, where all CLIF servers register. When not specified,
 *   the default host is localhost and the default port is 1234</li>
 *   <li>clif.codeserver.host and clif.codeserver.port give the network address of
 *   the CLIF coder server, that serves all classes and necessary resource files during
 *   test plan deployment. When not specified, the default host is localhost and the
 *   default port is 1357</li>
 *   <li>jonathan.connectionfactory.host is the IP address to be chosen among local
 *   configured network interfaces to be published in the registry. For a CLIF server,
 *   this IP address must be reachable from the deployment point (console, command-line
 *   deployment). For a deployment point, this IP address must be reachable by all
 *   CLIF servers. This property is automatically handled accordingly to the other
 *   network settings, using a smart configuration protocol requiring the registry to be
 *   running.</li>
 *   <li>clif.filestorage.host is the IP address to be chosen among local
 *   configured network interfaces of a CLIF server, to be given to the deployment point
 *   and used during the data collection phase. This IP address must be reachable from
 *   the deployment point. This property is automatically handled in the same way as the
 *   previous property.</li>
 * </ul>
 * The {@link #load(boolean)} method may be used to load the configuration file.
 * The {@link #store()} method shall be called to write the properties changes to the file.
 * 
 * @author Bruno Dillenseger
 */
public class ConfigFileManager
{
	/**
	 * Generates or modifies a CLIF configuration file, setting network-related properties. 
	 * @param args configuration_file_path_name [registry_host[:registry_port]]
	 * [codeserver_host[:codeserver_port]]
	 * <br/>With no optional argument, all network-related properties are discarded.
	 * The resulting CLIF configuration will work only on a single host. 
	 * <br/>With registry-only network arguments, registry-related properties are set
	 * as provided, and code server-related properties are set to the registry host and
	 * default code server port.
	 * In any case, the other network-related properties are automatically handled
	 * accordingly to the given parameters. 
	 */
	static public void main(String[] args)
	throws IOException
	{
		// load the configuration file if it exists
		ConfigFileManager conf = new ConfigFileManager(args[0]);
		conf.load(true);
		// configure the Fractal registry network address
		conf.setNetworkAddress(
			"fractal.registry",
			args.length > 1 ? args[1] : null);
		// configure the CLIF code server network address
		conf.setNetworkAddress(
			"clif.codeserver",
			args.length > 2
				? args[2]
				: args.length > 1
					? conf.getProperty("fractal.registry.host", "localhost")
					: null);
		// look for a local IP address that is reachable from the Registry
		InetAddress localAddr = NetConfHelper.tryAddresses(
			conf.getProperty("fractal.registry.host", "localhost"),
			Integer.parseInt(
				conf.getProperty(
					"fractal.registry.port",
					String.valueOf(Registry.DEFAULT_PORT)))
				+ 1,
			5000);
		// set other network-related parameters
		if (localAddr == null)
		{
			String localAddrStr = conf.getProperty("fractal.registry.host", null);
			if (localAddrStr != null
				&& Arrays.asList(Network.getInetAddresses()).contains(InetAddress.getByName(localAddrStr))) 
			{
				conf.setProperty("clif.filestorage.host", localAddrStr);
				conf.setProperty("jonathan.connectionfactory.host", localAddrStr);
			}
			else
			{
				conf.clearProperty("clif.filestorage.host");
				conf.clearProperty("jonathan.connectionfactory.host");
			}
		}
		else
		{
			conf.setProperty("clif.filestorage.host", localAddr.getHostAddress());
			conf.setProperty("jonathan.connectionfactory.host", localAddr.getHostAddress());
		}
		conf.store();
	}

	File conf;
	List<String> options = new LinkedList<String>();

	/**
	 * Creates a new configuration file manager associated
	 * to a given file object.
	 * @param conf the file to manage
	 */
	public ConfigFileManager(File conf)
	{
		this.conf = conf;
	}

	/**
	 * Creates a new configuration file manager to a given
	 * path name.
	 * @param path the path name to the configuration file
	 */
	public ConfigFileManager(String path)
	{
		this(new File(path));
	}

	/**
	 * Loads a configuration file.
	 * @param justTry when false, an exception will be thrown when the file can't
	 * be loaded for any reason; when true, an exception will be thrown only when
	 * the configuration file exists but can't be loaded (either because of
	 * insufficient access rights, or because of I/O errors).
	 * @throws IOException the configuration file could not be read.
	 */
	public void load(boolean justTry)
	throws IOException
	{
		options.clear(); 
		if (conf.exists() || !justTry)
		{
			BufferedReader br = new BufferedReader(new FileReader(conf));
			String line = br.readLine();
			while (line != null)
			{
				options.add(line);
				line = br.readLine();
			}
			br.close();
		}
	}

	/**
	 * Get a property's value.
	 * @return the property's value, or the provided default value
	 * @param key the property name
	 * @param defaultValue the default value to return when the given property
	 * is not defined
	 */
	public String getProperty(String key, String defaultValue)
	{
		key = "-D" + key + "=";
		for (String line : options)
		{
			if (line.startsWith(key))
			{
				return line.substring(line.indexOf("=") + 1);
			}
		}
		return defaultValue;
	}

	/**
	 * Get all the options
	 * @return a list of option strings
	 */
	public List<String> getOptions()
	{
		return options;
	}

	/**
	 * Set a property's value.
	 * @param key the property name
	 * @param value the property value
	 */
	public String setProperty(String key, String value)
	{
		String previousValue = null;
		key = "-D" + key + "=";
		for (int i=0 ; i<options.size() ; ++i)
		{
			if (options.get(i).startsWith(key))
			{
				previousValue = options.set(i, key + value);
			}
		}
		if (previousValue == null)
		{
			options.add(0, key + value);
		}
		return previousValue;
	}

	/**
	 * Discards a property.
	 * @param key the property to discard
	 * @return the property value just before the property was discarded,
	 * or null if this property was not defined.
	 */
	public String clearProperty(String key)
	{
		String oldValue = null;
		key = "-D" + key + "=";
		for (String line : options)
		{
			if (line.startsWith(key))
			{
				oldValue = line;
			}
		}
		if (oldValue != null)
		{
			options.remove(oldValue);
			oldValue = oldValue.substring(oldValue.indexOf("=") + 1);
		}
		return oldValue;
	}

	/**
	 * Writes the properties to the configuration file.
	 * @throws IOException the configuration file could not be written
	 */
	public void store()
	throws IOException
	{
		BufferedWriter bw = new BufferedWriter(new FileWriter(conf));
		for (String line : options)
		{
			bw.write(line + "\n");
		}
		bw.close();
	}

	/**
	 * Generic method for setting network property pairs with form
	 * any.property<em>.host</em> and any.property<em>.port</em>.
	 * @param property property prefix
	 * @param value property value, either:
	 * <ul>
	 *   <li>a simple IP address, in which case the .host property is set and
	 *   the .port property is cleared;</li> 
	 *   <li>or a network address of the form IPaddress:port, in which case both
	 *   properties are set;</li>
	 *   <li>or null, in which case both properties are cleared.</li>
	 * </ul>
	 */
	private void setNetworkAddress(String propPrefix, String value)
	{
		if (value == null)
		{
			clearProperty(propPrefix + ".port");
			clearProperty(propPrefix + ".host");
		}
		else if (value.contains(":"))
		{
			setProperty(
				propPrefix + ".host",
				value.substring(0, value.indexOf(":")));
			setProperty(
				propPrefix + ".port",
				value.substring(value.indexOf(":") + 1));
		}
		else
		{
			setProperty(propPrefix + ".host", value);
			clearProperty(propPrefix + ".port");
		}
	}
}
