/*
* CLIF is a Load Injection Framework
* Copyright (C) 2006-2008, 2011 France Telecom
* Copyright (C) 2014, 2016 Orange SA
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
* Contact: clif@ow2.org
*/
package org.ow2.clif.util;

/**
 * Miscellaneous String utilities
 * @author Fabrice Rivart
 * @author Bruno Dillenseger
 */

public abstract class StringHelper
{
    /**
	 * Returns a valid Java identifier by removing non valid character.
	 * @param string string to convert into a valid Java identifier
	 * @return a valid Java identifier
	 */
	static public String convertJavaIdentifier(String string)
	{	
		StringBuilder sb = new StringBuilder();
		boolean first = true;
		char [] tabs = string.toCharArray();

		for (int i = 0; i < tabs.length; i++)
		{	
			if (first && Character.isJavaIdentifierStart(tabs[i]))
			{
				sb.append(tabs[i]);
				first = false;
			}
			else if (!first)
			{
				if (Character.isJavaIdentifierPart(tabs[i]))
				{
					sb.append(tabs[i]);
				}
			}
		}	
		return sb.toString();
	}

	/**
	 * Java Properties values and keys need escaping when storing them
	 * to conform to the Properties.load() specification.
	 * @param prop a property key or value to escape
	 * @return the escaped key or value ready for storing
	 */
	static public String escapeProperty(String prop)
	{
		return prop
			.replace("\\", "\\\\")
			.replace(":", "\\:")
			.replace("=", "\\=")
			.replace("#", "\\#")
			.replace("!", "\\!");	
	}

	/**
	 * Checks if the given String is one among "true", "yes",
	 * "on" (ignoring case) or "1".
	 * @param value the string to check 
	 * @return true if value equals "true", "yes", "on" (ignoring case)
	 * or "1"; false otherwise.
	 */
	public static boolean isEnabled(String value)
	{
		return
			value.equalsIgnoreCase("yes")
			|| value.equalsIgnoreCase("true")
			|| value.equalsIgnoreCase("on")
			|| value.equals("1");
	}
}
