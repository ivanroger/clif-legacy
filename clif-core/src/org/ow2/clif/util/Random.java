/*
* CLIF is a Load Injection Framework
* Copyright (C) 2006 France Telecom
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
* Contact: clif@ow2.org
*/
package org.ow2.clif.util;

import java.io.InputStream;


/**
 * This class extends Java's basic random features with extra primitives
 * that are likely to be useful to introduce random parameters in requests
 * and testing scenarios...
 * 
 * @author Bruno Dillenseger
 */
public class Random extends java.util.Random
{
	private static final long serialVersionUID = -8940271399087166276L;

	//////////////////////////////////////////////////////
	// static definitions for management of Poisson law //
	//////////////////////////////////////////////////////

	private static final int[][] poissonTable1 = {
		{90484, 81873, 74082, 67032, 60653},
		{99532, 98248, 96306, 93845, 90980},
		{99985, 99885, 99640, 99207, 98561},
		{100000, 99994, 99973, 99922, 99825},
		{100000, 100000, 99998, 99994, 99983},
		{100000, 100000, 100000, 100000, 99999},
		{100000, 100000, 100000, 100000, 100000}
	};

	private static final int[][] poissonTable2 = {
		{36788, 13534, 4979, 1832, 674},
		{73576, 40601, 19915, 9158, 4043},
		{91970, 67668, 42319, 23810, 12465},
		{98101, 85712, 64723, 43347, 26503},
		{99634, 94735, 81526, 62884, 44049},
		{99941, 98344, 91608, 78513, 61596},
		{99992, 99547, 96649, 88933, 76218},
		{99999, 99890, 98810, 94887, 86663},
		{100000, 99976, 99620, 97864, 93191},
		{100000, 99995, 99890, 99187, 96817},
		{100000, 99999, 99971, 99716, 98630},
		{100000, 100000, 99993, 99908, 99455},
		{100000, 100000, 99998, 99973, 99798},
		{100000, 100000, 100000, 99992, 99930},
		{100000, 100000, 100000, 99998, 99977},
		{100000, 100000, 100000, 100000, 99993},
		{100000, 100000, 100000, 100000, 99998},
		{100000, 100000, 100000, 100000, 99999}
	};

	private static final int[][] poissonTable3 = {
		{248, 91, 34, 12, 5},
		{1735, 730, 302, 123, 50},
		{6197, 2964, 1375, 623, 277},
		{15120, 8177, 4238, 2123, 1034},
		{28506, 17299, 9963, 5496, 2925},
		{44568, 30071, 19124, 11569, 6709},
		{60630, 44971, 31337, 20678, 13014},
		{74398, 59871, 45296, 32390, 22022},
		{84724, 72909, 59255, 45565, 33282},
		{91608, 83050, 71662, 58741, 45793},
		{95738, 90148, 81589, 70599, 58304},
		{97991, 94665, 88808, 80301, 69678},
		{99117, 97300, 93620, 87577, 79156},
		{99637, 98719, 96582, 92615, 86446},
		{99860, 99428, 98274, 95853, 91654},
		{99949, 99759, 99177, 97796, 95126},
		{99983, 99904, 99628, 98889, 97296},
		{99994, 99964, 99841, 99468, 98572},
		{99998, 99987, 99935, 99757, 99281},
		{99999, 99996, 99975, 99894, 99655},
		{100000, 99999, 99991, 99956, 99841},
		{100000, 100000, 99997, 99983, 99930},
		{100000, 100000, 99999, 99993, 99970},
		{100000, 100000, 100000, 99998, 99988},
		{100000, 100000, 100000, 99999, 99995},
		{100000, 100000, 100000, 100000, 99998},
		{100000, 100000, 100000, 100000, 99999}
	};

	private static long getFromTable(int[][] table, int p, int x)
	{
		int k, n;

		n = table.length;
		for (k = 0; k < n && x > table[k][p]; k++);
		return k;
	}

	private static long getPoisson(double parameter, double x)
	{
		if (parameter <= 0)
			return 0;
		if (parameter <= 0.5)
			return getFromTable(poissonTable1, (int)(parameter * 10) - 1, (int)(x * 100000));
		if (parameter <= 5)
			return getFromTable(poissonTable2, (int)parameter - 1, (int)(x * 100000));
		return getFromTable(poissonTable3, (int)parameter - 6, (int)(x * 100000));
	}

	private static double normal(double mean, double standardDeviation)
	{
		if (standardDeviation < 0)
			throw new IllegalArgumentException(
					"Standard deviation of a normal distribution can't be negative");

		return mean + standardDeviation
				* Math.sqrt(-2 * Math.log(Math.random()))
				* Math.cos(2 * Math.PI * Math.random());
	}

	//////////////////
	// constructors //
	//////////////////

	/**
	 * @see java.util.Random#Random()
	 */
	public Random()
	{
		super();
	}

	/**
	 * @param seed
	 * @see java.util.Random#Random(long)
	 */
	public Random(long seed)
	{
		super(seed);
	}

	/**
	 * Uniform distribution
	 * @param min minimum value (inclusive)
	 * @param max maximum value (exclusive)
	 * @return a random value according to a uniform distribution
	 */
	public long nextUniform(int min, int max)
	{
		return min + nextInt(max - min);
	}

	/**
	 * Poisson law distribution
	 * @param factor the result is multiplied by this factor
	 * @param parameter both mean and deviation value for the Poisson law
	 * @return a random value according to a Poisson law distribution
	 */
	public long nextPoisson(double parameter, int factor)
	{
		double val1, val2;
		double tmp;

		if (parameter < 0)
			throw new IllegalArgumentException(
					"Parameter of a Poisson distribution can't be negative");

		if (parameter <= 10) {
			if (parameter <= 0.5) {
				tmp = parameter * 10;
				val1 = Math.floor(tmp) / 10;
				val2 = Math.ceil(tmp) / 10;
			}
			else {
				val1 = Math.floor(parameter);
				val2 = Math.ceil(parameter);
			}
			if (val1 == val2)
				return getPoisson(val1, Math.random());
			return Math.random() < (val2 - parameter) / (val2 - val1) ? getPoisson(val1, Math.random()) : getPoisson(val2, Math.random());
		}
		/* For parameter > 10, approximate the Poisson law by the normal law. */
		while ((tmp = Math.round(normal(parameter, Math.sqrt(parameter)))) < 0);
		return (long)tmp;
	}

	/**
	 * Gaussian law distribution
	 * @param min minimum value
	 * @param max maximum value
	 * @param mean values mean
	 * @param deviation values deviation
	 * @return a random value according to a gaussian law distribution
	 */
	public long nextGaussian(int min, int max, int mean, int deviation)
	{
		long rand = mean + (long) nextGaussian() * deviation;
		if (rand < min)
		{
			return min;
		}
		else if (rand > max)
		{
			return max;
		}
		else
		{
			return rand;
		}
	}

	/**
	 * Negative exponential law distribution
	 * @param min minimum value
	 * @param mean values mean
	 * @return a random value according to a negative exponential law distribution
	 */
	public long nextNegativeExponential(int min, int mean)
	{
		double r = nextDouble();
		if (r < 4.54e-5)
		{
			return (long) (r + min);
		}
		else
		{
			return (long) (((- mean) * Math.log(r)) + min);
		}
	}

	/**
	 * @param length the number of characters of the generated string
	 * @return a random character string of the specified length
	 */
	public StringBuilder nextStringBuilder(int length)
	{
		StringBuilder strb = new StringBuilder(length);
		while (length-- > 0)
		{
			strb.append((char)(' ' + this.nextInt('z' - ' ')));
		}
		return  strb;
	}

	/**
	 * Input stream access to the random generator.
	 * @return an input stream generating uniformly distributed random values between 0 and 255 included.
	 * This input stream does not support the mark operation.
	 */
	public InputStream getInputStream()
	{
		return new InputStream() {
			@Override
			public int read()
			{
				return nextInt(256);
			}
		};
	}
}
