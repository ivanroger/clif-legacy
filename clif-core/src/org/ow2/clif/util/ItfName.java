/*
* CLIF is a Load Injection Framework
* Copyright (C) 2003 France Telecom R&D
* Copyright (C) 2003 INRIA
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
* Contact: clif@ow2.org
*/

package org.ow2.clif.util;

import java.math.BigInteger;
import java.util.HashMap;
import java.util.Map;


/**
 * Interface unique name generator for collection client interfaces.
 * @author Bruno Dillenseger
 */
abstract public class ItfName
{
	static protected Map<String,BigInteger> interfaces = new HashMap<String,BigInteger>();

	/**
	 * Unique interface name generation (uniqueness is bound to this class definition in one JVM)
	 * @param itfBaseName interface name to be used as a prefix for unique name generation
	 * @return a new unique name with the given prefix
	 */
	static public String gen(String itfBaseName)
	{
		BigInteger lastId = interfaces.get(itfBaseName);
		if (lastId == null)
		{
			lastId = BigInteger.ZERO;
		}
		else
		{
			lastId = lastId.add(BigInteger.ONE);
		}
		interfaces.put(itfBaseName, lastId);
		return itfBaseName + lastId;
	}
}
