/*
* CLIF is a Load Injection Framework
* Copyright (C) 2005 France Telecom R&D
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
* CLIF $Name: not supported by cvs2svn $
*
* Contact: clif@ow2.org
*/

package org.ow2.clif.storage.api;

import java.io.Serializable;
import java.util.Date;


/**
 * This interface specifies methods to be implemented by a test run descriptor, whose role
 * is to designate a given test run.
 * @author Bruno Dillenseger
 */
public interface TestDescriptor extends Serializable
{
	/**
	 * Gets the test run name of the test run represented by current test descriptor
	 * @return the test run name of the test run represented by current test descriptor
	 */
	public String getName();

	/**
	 * Gets the test run initialization date of the test run represented by current test descriptor
	 * @return the test run initialization date of the test run represented by current test descriptor
	 */
	public Date getDate();
}
