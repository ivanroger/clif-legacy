/*
* CLIF is a Load Injection Framework
* Copyright (C) 2005,2008, 2011-2012 France Telecom
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
* Contact: clif@ow2.org
*/

package org.ow2.clif.storage.lib.filestorage;

import org.ow2.clif.console.lib.ClifDeployDefinition;
import org.ow2.clif.storage.api.BladeDescriptor;
import java.io.File;
import java.io.IOException;
import java.io.FilenameFilter;


/**
 * Gathers description elements of a test blade:
 * blade id, deployment definition, event type labels 
 * @author Bruno Dillenseger
 */
public class BladeDescriptorImpl implements BladeDescriptor
{
	private static final long serialVersionUID = -1870845063920365685L;
	protected String id;
	protected ClifDeployDefinition def;
	protected String[] eventTypeLabels;


	public BladeDescriptorImpl(
		File bladeDir,
		String bladeId,
		ClifDeployDefinition definition)
	throws IOException
	{
		id = bladeId;
		def = definition;
		eventTypeLabels = bladeDir.list(
			new FilenameFilter() {
				@Override
				public boolean accept(File dir, String name)
				{
					return new File(dir, name + FileStorageCommons.CLASSNAME_EXTENSION).exists();
				}
			});
	}


	@Override
	public String toString()
	{
		String result = id
		+ " " + getClassname()
		+ (isProbe() ? " probe" : (isInjector() ? " injector" : ""))
		+ " deployed on " + getServerName();
		if (getComment() != null && getComment().trim().length() > 0)
		{
			result += " (" + getComment() + ")";
		}
		return result;
	}


	///////////////////////////////
	// interface BladeDescriptor //
	///////////////////////////////


	@Override
	public String getServerName()
	{
		return def.getServerName();
	}


	@Override
	public String getArgument()
	{
		return def.getArgument();
	}


	@Override
	public String getClassname()
	{
		return def.getContext().get("insert");
	}


	@Override
	public String getComment()
	{
		return def.getComment();
	}


	@Override
	public String getId()
	{
		return id;
	}


	@Override
	public boolean isProbe()
	{
		return def.isProbe();
	}


	@Override
	public boolean isInjector()
	{
		return !def.isProbe();
	}


	@Override
	public String[] getEventTypeLabels()
	{
		return eventTypeLabels;
	}
}
