/*
* CLIF is a Load Injection Framework
* Copyright (C) 2003-2004, 2012 France Telecom R&D
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
* Contact: clif@ow2.org
*/

package org.ow2.clif.storage.lib.filestorage;

import org.ow2.clif.util.Network;
import org.ow2.clif.util.UniqueKey;
import java.util.HashMap;
import java.util.Map;
import java.util.Stack;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.InetSocketAddress;
import java.io.File;
import java.io.IOException;
import java.io.FileNotFoundException;
import java.io.Serializable;
import java.io.OutputStream;
import java.io.InputStream;
import java.io.FileInputStream;


/**
 * Each instance of this class represents and manages the collect of test data files located in
 * a given directory. Each file is transfered by socket, within a dedicated "collect step".
 * @author Bruno Dillenseger
 */
public class FileStorageCollect
{
	///////////////////////////////////
	// static attributes and methods //
	///////////////////////////////////

	static private final String COLLECT_HOST_PROP = "clif.filestorage.host";
	static public final int BLOCK_SIZE = 4096;
	static protected Map<UniqueKey,FileStorageCollect> active_collects = new HashMap<UniqueKey,FileStorageCollect>();
	static protected Stack<FileStorageCollect> old_collects = new Stack<FileStorageCollect>();

	static public FileStorageCollect newCollect(File dir)
	{
		synchronized (active_collects)
		{
			FileStorageCollect collect = null;
			if (old_collects.isEmpty())
			{
				try
				{
					collect = new FileStorageCollect();
				}
				catch (IOException ex)
				{
					throw new Error("Can't initiate FileStorage data collect", ex);
				}
			}
			else
			{
				collect = old_collects.pop();
			}
			UniqueKey key = new UniqueKey();
			try
			{
				collect.setDirectory(dir);
				collect.setKey(key);
				active_collects.put(key, collect);
			}
			catch (FileNotFoundException ex)
			{
				old_collects.push(collect);
				collect = null;
			}
			return collect;
		}
	}


	static public FileStorageCollect getCollect(UniqueKey key)
	{
		return active_collects.get(key);
	}


	static public Serializable collect(UniqueKey key)
	{
		FileStorageCollect collect = getCollect(key);
		if (collect != null)
		{
			return collect.collect();
		}
		else
		{
			return null;
		}
	}


	static public void close(UniqueKey key)
	{
		FileStorageCollect collect = getCollect(key);
		if (collect != null)
		{
			collect.close();
		}
	}


	public static long getSize(UniqueKey key)
	{
		FileStorageCollect collect = getCollect(key);
		if (collect != null)
		{
			return collect.getSize();
		}
		else
		{
			return -1;
		}
	}


	///////////////////////////////
	// end of static definitions //
	///////////////////////////////


	protected UniqueKey key;
	protected int fileIndex;
	protected File[] files;
	protected FileServer fileServer;
	protected long size;
	protected byte[] buffer = new byte[BLOCK_SIZE];


	private FileStorageCollect()
		throws IOException
	{
		fileServer = new FileServer();
	}


	public UniqueKey getKey()
	{
		return key;
	}


	private void setKey(UniqueKey key)
	{
		this.key = key;
	}


	private void setDirectory(File dir)
		throws FileNotFoundException
	{
		files = dir.listFiles();
		if (files == null)
		{
			throw new FileNotFoundException("directory " + dir + " does not exist");
		}
		fileIndex = -1;
		size = 0;
		for (int i=0 ; i<files.length ; size += files[i++].length());
	}


	private Serializable collect()
	{
		if (++fileIndex < files.length)
		{
			return new FileStorageCollectStep(
				files[fileIndex].getName(),
				fileServer.getLocalSocketAddress());
		}
		else
		{
			close();
			return null;
		}
	}


	private void close()
	{
		synchronized (active_collects)
		{
			if (active_collects.containsKey(key))
			{
				old_collects.push(active_collects.remove(key));
			}
		}
	}


	private long getSize()
	{
		return size;
	}


	/**
	 * Inner class for files download through sockets
	 */
	class FileServer extends ServerSocket implements Runnable
	{
		public FileServer()
			throws IOException
		{
			super();
			InetSocketAddress addr;
			String fixed_address_prop = System.getProperty(COLLECT_HOST_PROP);
			if (fixed_address_prop != null)
			{
				// subnet (form d.d.d.d/n) or host address?
				if (fixed_address_prop.indexOf('/') == -1)
				{
					addr = new InetSocketAddress(InetAddress.getByName(fixed_address_prop), 0);
				}
				else
				{
					addr = new InetSocketAddress(Network.getInetAddress(fixed_address_prop), 0);
				}
			}
			else
			{
				addr = new InetSocketAddress(Network.getInetAddress(null), 0);
			}
			try
			{
				bind(addr);
			}
			catch (IOException ex)
			{
				IOException ex2 = new IOException("Can't bind address " + addr);
				ex2.setStackTrace(ex.getStackTrace());
				throw ex2;
			}
			new Thread(this).start();
		}


		@Override
		public void run()
		{
			Socket sock;
			OutputStream out;
			InputStream in;
			while (true)
			{
				sock = null;
				in = null;
				out = null;
				try
				{
					sock = accept();
					out = sock.getOutputStream();
					in = new FileInputStream(files[fileIndex]);
					int n = in.read(buffer);
					while (n != -1)
					{
						out.write(buffer, 0, n);
						n = in.read(buffer);
					}
				}
				catch (IOException ex)
				{
					ex.printStackTrace(System.err);
				}
				finally
				{
					try
					{
						if (sock != null)
						{
							if (out != null)
							{
								if (in != null)
								{
									in.close();
								}
								out.close();
							}
							sock.close();
						}
					}
					catch (IOException ex)
					{
						ex.printStackTrace(System.err);
					}
				}
			}
		}
	}
}
