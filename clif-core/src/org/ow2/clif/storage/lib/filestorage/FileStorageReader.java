/*
* CLIF is a Load Injection Framework
* Copyright (C) 2012 France Telecom R&D
* Copyright (C) 2013, 2014 Orange
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
* Contact: clif@ow2.org
*/

package org.ow2.clif.storage.lib.filestorage;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.Serializable;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import org.omg.CORBA.LongHolder;
import org.ow2.clif.console.lib.ClifDeployDefinition;
import org.ow2.clif.console.lib.TestPlanReader;
import org.ow2.clif.storage.api.AbstractEvent;
import org.ow2.clif.storage.api.BladeDescriptor;
import org.ow2.clif.storage.api.BladeEvent;
import org.ow2.clif.storage.api.BladeFilter;
import org.ow2.clif.storage.api.EventFactory;
import org.ow2.clif.storage.api.EventFilter;
import org.ow2.clif.storage.api.NoMoreEvent;
import org.ow2.clif.storage.api.StorageRead;
import org.ow2.clif.storage.api.TestDescriptor;
import org.ow2.clif.storage.api.TestFilter;
import org.ow2.clif.supervisor.api.ClifException;
import org.ow2.clif.util.ClifClassLoader;
import org.ow2.clif.util.ExecutionContext;

/**
 * Implementation of the StorageRead interface for the filestorage package.
 * 
 * @author Bruno Dillenseger
 */
public class FileStorageReader implements StorageRead
{
	protected Map<BigInteger,EventIterator> eventIterators = new HashMap<BigInteger,EventIterator>();
	protected BigInteger eventIteratorKey = BigInteger.ZERO;
	protected String testDirbase = null;


	/**
	 * Creates a new access to CLIF storage, using the default
	 * storage directory location.
	 * @throws ClifException if the default directory does
	 * not exist or is not readable
	 * @see ExecutionContext
	 * @see FileStorageCommons
	 */
	public FileStorageReader()
	throws ClifException
	{
		this(null, false);
	}


	/**
	 * Creates a new access to CLIF storage
	 * @param path the target storage directory, either relative to the
	 * CLIF context's default directory, or absolute. When null, the
	 * default storage location is used.
	 * @param create when true, the target storage directory will be
	 * created if it does not exist already. When false, the target
	 * directory must exist.
	 * @throws ClifException the default directory does not exist
	 * and the create parameter is false, or the default directory
	 * does not exist and could not be created.
	 * @see ExecutionContext
	 * @see FileStorageCommons
	 */
	public FileStorageReader(String path, boolean create)
	throws ClifException
	{
		testDirbase = FileStorageCommons.getTestDirbase(path, create);
	}

	protected File getTestDir(String testName)
	{
		return new File(testDirbase, testName);
	}

	protected File getBladeDir(String testName, String bladeId)
	{
		return new File(getTestDir(testName), bladeId);
	}

	protected File getEventFile(String testName, String bladeId, String eventTypeLabel)
	{
		return new File(getBladeDir(testName, bladeId), eventTypeLabel);
	}

	protected File getEventClassFile(String testName, String bladeId, String eventTypeLabel)
	{
		return new File(
			getBladeDir(testName, bladeId),
			eventTypeLabel + FileStorageCommons.CLASSNAME_EXTENSION);
	}

	protected boolean isCommentLine(String line)
	{
		return line != null && line.startsWith(FileStorageCommons.COMMENT_PREFIX);
	}


	///////////////////////////
	// interface StorageRead //
	///////////////////////////

	/**
	 * @param filter only tests matching this filter will be retained
	 * @return a browser object to browse among available test execution results
	 */
	public TestDescriptor[] getTests(final TestFilter filter)
		throws ClifException
	{
		final List<TestDescriptor> descriptors = new ArrayList<TestDescriptor>();
		try
		{
			new File(testDirbase).listFiles(
				new FileFilter() {
					public boolean accept(File path)
					{
						if (path.isDirectory() && new File(path + TestPlanReader.FILE_EXT).exists())
						{
							try
							{
								TestDescriptor desc = new TestDescriptorImpl(path);
								if (filter == null || filter.accept(desc))
								{
									descriptors.add(desc);
									return true;
								}
								else
								{
									return false;
								}
							}
							catch (Exception ex)
							{
								throw new RuntimeException("Cannot get test descriptor for " + path);
							}
						}
						else
						{
							return false;
						}
					}
				});
		}
		catch (Exception ex)
		{
			throw new ClifException("Could not get tests", ex);
		}
		return descriptors.toArray(new TestDescriptor[descriptors.size()]);
	}


	public BladeDescriptor[] getTestPlan(String testName, BladeFilter filter)
		throws ClifException
	{
		List<BladeDescriptor> blades = null;
		try
		{
			Map<String,ClifDeployDefinition> testplan = TestPlanReader.readFromProp(
				new FileInputStream(new File(testDirbase, testName + TestPlanReader.FILE_EXT)));
			blades = new ArrayList<BladeDescriptor>(testplan.size());
			Iterator<Map.Entry<String,ClifDeployDefinition>> iter = testplan.entrySet().iterator();
			File testDir = new File(testDirbase, testName);
			while (iter.hasNext())
			{
				Map.Entry<String,ClifDeployDefinition> entry = iter.next();
				BladeDescriptor desc = new BladeDescriptorImpl(
					new File(testDir, entry.getKey()),
					entry.getKey(),
					entry.getValue());
				if (filter == null || filter.accept(desc))
				{
					blades.add(desc);
				}
			}
		}
		catch (Exception ex)
		{
			throw new ClifException("Could not get test plan for " + testName, ex);
		}
		return blades.toArray(new BladeDescriptor[blades.size()]);
	}


	public Properties getBladeProperties(String testName, String bladeId)
		throws ClifException
	{
		Properties props = new Properties();
		try
		{
			props.load(
				new FileInputStream(
					new File(
						getBladeDir(testName, bladeId),
						FileStorageCommons.JVMPROPS_FILENAME)));
		}
		catch (Exception ex)
		{
			throw new ClifException(
				"Could not get properties for blade " + bladeId + " from test " + testName);
		}
		return props;
	}


	private Class<?> loadEventClass(String testName, String bladeId, String eventTypeLabel)
		throws ClifException
	{
		BufferedReader reader = null;
		try
		{
			reader = new BufferedReader(
				new FileReader(
					getEventClassFile(testName, bladeId, eventTypeLabel)));
			return Class.forName(
				reader.readLine(),
				true,
				ClifClassLoader.getClassLoader());
		}
		catch (Exception ex)
		{
			throw new ClifException(
				"Could not get event class for " + eventTypeLabel + " from " + testName + "/" + bladeId + "/",
				ex);
		}
		finally
		{
			if (reader != null)
			{
				try
				{
					reader.close();
				}
				catch (IOException ex)
				{
					ex.printStackTrace(System.err);
				}
			}
		}
	}


	public String[] getEventFieldLabels(String testName, String bladeId, String eventTypeLabel)
	{
		String[] result = AbstractEvent.getEventFieldLabels(eventTypeLabel);
		if (result == null)
		{
			try
			{
				@SuppressWarnings("unused")
				Class<?> clazz = loadEventClass(testName, bladeId, eventTypeLabel);
				result = AbstractEvent.getEventFieldLabels(eventTypeLabel);
			}
			catch (Exception ex)
			{
				ex.printStackTrace(System.err);
			}
		}
		return result;
	}


	public Serializable getEventIterator(
			String testName,
			String bladeId,
			String eventTypeLabel,
			EventFilter filter)
	throws ClifException
	{
		eventIteratorKey = eventIteratorKey.add(BigInteger.ONE);
		eventIterators.put(
			eventIteratorKey,
			new EventIterator(testName, bladeId, eventTypeLabel, filter));
		return eventIteratorKey;
	}


	public BladeEvent[] getNextEvents(Serializable iteratorKey, int count)
		throws ClifException
	{
		EventIterator iter = eventIterators.get(iteratorKey);
		if (iter != null)
		{
			return iter.next(0, count, null);
		}
		else
		{
			throw new ClifException("Unknown event iterator key " + iteratorKey);
		}
	}


	public void closeEventIterator(Serializable iteratorKey)
	{
		EventIterator iter = eventIterators.remove(iteratorKey);
		if (iter != null)
		{
			iter.close();
		}
	}


	public BladeEvent[] getEvents(
		String testName,
		String bladeId,
		String eventTypeLabel,
		EventFilter filter,
		long fromIndex,
		int count)
	throws ClifException
	{
		EventIterator iter = new EventIterator(testName, bladeId, eventTypeLabel, filter);
		BladeEvent[] result = iter.next(fromIndex, count, null);
		iter.close();
		return result;
	}


	public long countEvents(
		String testName,
		String bladeId,
		String eventTypeLabel,
		EventFilter filter)
	throws ClifException
	{
		EventIterator iter = new EventIterator(testName, bladeId, eventTypeLabel, filter);
		LongHolder count = new LongHolder();
		iter.next(0, -1, count);
		iter.close();
		return count.value;
	}


	class EventIterator
	{
		BufferedReader reader;
		EventFilter filter;
		Class<?> clazz;
		EventFactory factory;


		EventIterator(String testName, String bladeId, String eventTypeLabel, EventFilter filter)
			throws ClifException
		{
			this.filter = filter;
			clazz = loadEventClass(testName, bladeId, eventTypeLabel);
			factory = AbstractEvent.getEventFactory(eventTypeLabel);
			try
			{
				reader = new BufferedReader(
					new FileReader(
						getEventFile(testName, bladeId, eventTypeLabel)));	
			}
			catch (IOException ex)
			{
				throw new ClifException(
					"Unable to find " + eventTypeLabel + " events from blade " + bladeId + " in test " + testName,
					ex);
			}
		}


		BladeEvent[] next(long fromIndex, int n, LongHolder counting)
			throws ClifException
		{
			List<BladeEvent> eventList = null;
			long eventCount = 0;
			if (counting == null)
			{
				eventList = new LinkedList<BladeEvent>();
			}
			try
			{
				String line = reader.readLine();
				boolean isComment = isCommentLine(line);
				while (line != null && (isComment || fromIndex > 0))
				{
					if (! isComment)
					{
						--fromIndex;
					}
					line = reader.readLine();
					isComment = isCommentLine(line);
				}
				while (line != null && (n != 0 || fromIndex < 0 || isComment))
				{
					if (! isComment)
					{
						BladeEvent event = factory.makeEvent(
							AbstractEvent.DEFAULT_SEPARATOR,
							line);
						if (filter == null || filter.accept(event))
						{
							if (counting == null)
							{
								eventList.add(event);
							}
							else
							{
								++eventCount;
							}
							if (fromIndex < 0 && n == 0)
							{
								eventList.remove(0);
							}
							else
							{
								--n;
							}
						}
					}
					if (n != 0 || fromIndex < 0 || isComment)
					{
						line = reader.readLine();
						isComment = isCommentLine(line);
					}
				}
			}
			catch (IOException ex)
			{
				throw new ClifException("Could not get events", ex);
			}
			catch (NoMoreEvent ex)
			{
			}
			if (counting == null)
			{
				return eventList.toArray(new BladeEvent[eventList.size()]);
			}
			else
			{
				counting.value = eventCount;
				return null;
			}
		}


		void close()
		{
			try
			{
				reader.close();
			}
			catch (IOException ex)
			{
				ex.printStackTrace(System.err);
			}
		}
	}
}
