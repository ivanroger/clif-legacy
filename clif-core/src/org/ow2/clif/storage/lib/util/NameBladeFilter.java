/*
* CLIF is a Load Injection Framework
* Copyright (C) 2007 France Telecom
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
* Contact: clif@ow2.org
*/
package org.ow2.clif.storage.lib.util;

import org.ow2.clif.storage.api.BladeDescriptor;
import org.ow2.clif.storage.api.BladeFilter;

/**
 * @author Jordan Brunier
 */
public class NameBladeFilter implements BladeFilter {
	private static final long serialVersionUID = -3987617294965280433L;

	private String name;
	
	/**
	 * Creates a new blade event filter selecting events whose date is between the given bounds
	 * (inclusive).
	 * If the lower bound is negative, then all events prior to the upper bound are accepted.
	 * If the upper bound is negative, then all events after the lower bound are accepted.
	 * If both the upper bound and the lower bound are negative, then all events are accepted.
	 * If the lower bound is greater than the upper bound, then no event is accepted.
	 * @param name the name of the blade
	 */
	public NameBladeFilter(String name)
	{
		this.name = name;
	}
	
	
	///////////////////////////
	// BladeFilter interface //
	///////////////////////////
	
	
	/* (non-Javadoc)
	 * @see org.ow2.clif.storage.api.BladeFilter#accept(org.ow2.clif.storage.api.BladeDescriptor)
	 */
	@Override
	public boolean accept(BladeDescriptor desc) {
		return (this.name.equals(desc.getId()));
	}

}
