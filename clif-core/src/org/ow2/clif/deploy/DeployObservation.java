/*
* CLIF is a Load Injection Framework
* Copyright (C) 2004, 2011 France Telecom R&D
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
* Contact: clif@ow2.org
*/

package org.ow2.clif.deploy;


import org.ow2.clif.util.ThrowableHelper;
import java.io.Serializable;


/**
 * Used by the Observable interface: Observer objects will
 * be invoked with an instance of this class as 2nd argument when
 * a deployment has been completed or has failed.
 * @see java.util.Observer#update(java.util.Observable,java.lang.Object)
 * @author Bruno Dillenseger
 */
public class DeployObservation implements Serializable
{
	private static final long serialVersionUID = 1152714780259576898L;
	boolean successful = false;
	Throwable exception = null;


	public DeployObservation()
	{
		this.successful = true;
	}


	public DeployObservation(boolean successful, Throwable exception)
	{
		this.successful = successful;
		this.exception = exception;
	}


	public boolean isSuccessful()
	{
		return successful;
	}


	public Throwable getException()
	{
		return exception;
	}


	@Override
	public String toString()
	{
		return successful
			? "successful operation"
			: "operation failed:\n" + ThrowableHelper.getMessages(exception);
	}		
}
