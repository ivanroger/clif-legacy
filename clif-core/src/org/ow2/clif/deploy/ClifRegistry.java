/*
* CLIF is a Load Injection Framework
* Copyright (C) 2003,2004,2011 France Telecom R&D
* Copyright (C) 2003 INRIA
* Copyright (C) 2015 Orange SA
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
* Contact: clif@ow2.org
*/

package org.ow2.clif.deploy;

import java.net.InetSocketAddress;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.rmi.registry.NamingService;
import org.objectweb.fractal.rmi.registry.Registry;
import org.ow2.clif.util.NetConfHelper;

/**
 * The clifRegistry is responsible for creating the initial FractalRMI
 * registry if not externally provided.
 * @author Bruno Dillenseger
 * @author Joan Chaumont
 */
public class ClifRegistry
{
	/**
	 * Prefix used for ClifRegistry namespaces : clifApplications or servers
	 */
	static private String CLIFAPP_PREFIX = "clifApp/";
	static private String SERVER_PREFIX = "server/";

	private NamingService registry;
	private String host;
	private int port;


	/**
	 * Creates a new test plan deployer, using the default RMI registry (as specified by
	 * system properties fractal.registry.host and fractal.registry.port)
	 * @param createRegistry if true, a FractalRMI registry is created on localhost using
	 * default port; if false, the registry is supposed to be running already on the default host
	 * and port.
	 * @throws Error 
	 */
	public ClifRegistry(boolean createRegistry) throws Exception
	{
		port = Registry.DEFAULT_PORT;
		String portStr = System.getProperty("fractal.registry.port");
		if (portStr != null)
		{
			try
			{
				port = Integer.parseInt(portStr);
			}
			catch (Exception ex)
			{
				System.err.println("Warning: invalid port number " + portStr + " for RMI registry; using default port number " + port);
			}
		}
		// synchronization to avoid concurrent Fractal RMI stub generations
		synchronized (Registry.class)
		{
			if (createRegistry)
			{
				try
				{
					new NetConfHelper().startServer(
						new InetSocketAddress(
							System.getProperty(
								"jonathan.connectionfactory.host",
								System.getProperty("fractal.registry.host", "localhost")),
							port + 1));
					Registry.createRegistry(port);
					registry = Registry.getRegistry("localhost", port);
					host = "localhost";
				}
				catch (Exception ex)
				{
					throw new Error("Cannot create a Fractal registry", ex);
				}
			}
			else
			{
				host = System.getProperty("fractal.registry.host", "localhost");
				registry = Registry.getRegistry(host, port);
				registry.list();
			}
		}
	}


	/**
	 * Get bound servers in ClifRegistry
	 * @return an array of the names of all available CLIF servers (i.e. registered in the
	 * FractalRMI Registry)
	 */
	public String[] getServers()
	{
		return getListByType(SERVER_PREFIX);
	}


	/**
	 * Gets the list of registered CLIF server names and components
	 * @return a map with server names as keys and Clif server components as values
	 */
	public Map<String,Component> getServerComponents()
	{
		String[] names = getServers();
		Map<String,Component> result = new HashMap<String,Component>();
		for (int i=0 ; i<names.length ; ++i)
		{
			result.put(names[i], lookupServer(names[i]));
		}
		return result;
	}


	private String[] getListByType(String type)
	{
		List<String> servers =  new ArrayList<String>();
		String[] names = registry.list();
		for (int i = 0; i < names.length; i++)
		{
			if(names[i].startsWith(type))
			{
				servers.add(names[i].substring(type.length()));
			}
		}
		names = new String[servers.size()];
		servers.toArray(names);
		return names;
	}

	/**
	 * Bind server component in registry namingService.
	 * Replace if binding allready exists.
	 * @param name name to be associated with the server
	 * @param server server to be associated with the given name
	 */
	public void bindServer(String name, Component server)
	{
		registry.rebind(SERVER_PREFIX+name, server);
	}
	
	/**
	 * Bind clifApp component in registry namingService.
	 * Replace if binding allready exists.
	 * @param name name to be associated with the clifApp
	 * @param clifApp clifApp to be associated with the given name
	 */
	public void bindClifApp(String name, Component clifApp)
	{
		registry.rebind(CLIFAPP_PREFIX+name, clifApp);
	}

	/**
	 * Direct access to CLIF/Fractal registry for binding a component to a name
	 * @param name the component name
	 * @param comp the component
	 */
	public void bind(String name, Component comp)
	{
		registry.bind(name, comp);
	}

	/**
	 * Direct access to CLIF/Fractal registry for rebinding a component to a name
	 * @param name the component name
	 * @param comp the component
	 */
	public void rebind(String name, Component comp)
	{
		registry.rebind(name, comp);
	}

	/**
	 * Direct access to CLIF/Fractal registry for unbinding a component name
	 * @param name the component name
	 */
	public void unbind(String name)
	{
		registry.unbind(name);
	}

	/**
	 * Direct access to CLIF/Fractal registry for finding a component from its name
	 * @param name component name
	 * @return the component bound to the given name
	 */
	public Component lookup(String name)
	{
		return registry.lookup(name);
	}

	/**
	 * Direct access to CLIF/Fractal registry for listing all bound names
	 * @return the list of names bound to components
	 */
	public String[] list()
	{
		return registry.list();
	}

	/**
	 * Looks for a server component.
	 * @param name the name to look for.
	 * @return Component if exists return server component else null
	 */
	public Component lookupServer(String name)
	{
		return registry.lookup(SERVER_PREFIX+name);
	}
	
	/**
	 * Looks for a clifApp component.
	 * @param name the name to look for.
	 * @return Component if exists return clifApp component else null
	 */
	public Component lookupClifApp(String name)
	{
		return registry.lookup(CLIFAPP_PREFIX+name);
	}

	/**
	 * @return string representation in the form registry@host:port_number
	 */
	@Override
	public String toString()
	{		
		return registry == null ? null : "registry@" + host + ":" + port;
	}
}
