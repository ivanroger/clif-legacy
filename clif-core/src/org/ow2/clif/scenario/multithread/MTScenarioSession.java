/*
* CLIF is a Load Injection Framework
* Copyright (C) 2004 France Telecom R&D
* Copyright (C) 2004 INRIA
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
* CLIF $Name: not supported by cvs2svn $
*
* Contact: clif@ow2.org
*/

package org.ow2.clif.scenario.multithread;

import org.ow2.clif.storage.api.ActionEvent;


/**
 * Interface to be implemented by session objects when using a MTScenario-derived scenario.
 * @see MTScenario#newSession(int, String)
 */
public interface MTScenarioSession
{
	/**
	 * Performs an injection action and returns the corresponding test report (response time, etc.).
	 * @param report a pre-filled test report. Fields date, iteration, sessionId and
	 * testId are filled with valid values. Field duration is set to 0, field successful is set to
	 * true, and fields type, comment, result are set to null.
	 * @return the actual, complete test report, or null if this action must not
	 * be taken into account
	 */
	public ActionEvent action(ActionEvent report);
}
