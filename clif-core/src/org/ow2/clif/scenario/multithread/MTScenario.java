/*
* CLIF is a Load Injection Framework
* Copyright (C) 2004, 2011 France Telecom R&D
* Copyright (C) 2004 INRIA
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
* Contact: clif@ow2.org
*/

package org.ow2.clif.scenario.multithread;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Random;
import java.util.StringTokenizer;
import org.objectweb.fractal.api.control.BindingController;
import org.ow2.clif.datacollector.api.DataCollectorWrite;
import org.ow2.clif.datacollector.lib.GenericFilter;
import org.ow2.clif.scenario.isac.util.BooleanHolder;
import org.ow2.clif.server.api.BladeControl;
import org.ow2.clif.server.api.BladeInsertResponse;
import org.ow2.clif.server.util.EventStorageState;
import org.ow2.clif.storage.api.ActionEvent;
import org.ow2.clif.supervisor.api.ClifException;
import org.ow2.clif.util.ClifClassLoader;

/**
 * Abstract implementation of a multi-thread based scenario component.
 * Method newSession() should be implemented by a derived class in order to provide
 * actual action to be performed. Each session loops on calling its action() method, animated
 * by its own thread.
 * MTscenario must be given an argument line (as a single String) beginning with 3 integer parameters:
 * <UL>
 * <LI>the number of threads
 * <LI>the loop time-out in seconds
 * <LI>the ramp-up time in seconds (added to the loop time-out)
 * </UL>
 * The trailing characters of the argument String is passed as argument to the newSession() method.
 * @see #newSession(int, String)
 * @see #setArgument(String)
 * @author Bruno Dillenseger
 */
public abstract class MTScenario
	implements BladeControl, BindingController
{
	static private final String[] interfaceNames = new String[] {
		DataCollectorWrite.DATA_COLLECTOR_WRITE,
		BladeInsertResponse.BLADE_INSERT_RESPONSE };
	protected Serializable testId;
	protected String scenarioId;
	private Object scenario_lock = new Object();
	private Object activities_lock = new Object();
	private BladeInsertResponse sr;
	private Object sr_lock = new Object();
	private DataCollectorWrite dcw;
	private Object dc_lock = new Object();
	private Activity[] threads;
	private int arg_thread_nb = 0;
	private int arg_duration_s = 0;
	private int arg_rampup_duration_ms = 0;
	private String sessionArg = null;
	private volatile int thr_remaining = 0;
	private volatile int thr_waiting = 0;
	private volatile boolean started;
	private volatile boolean suspended;
	private volatile boolean stopped;
	private StopTimer timer = null;
	private Random random = new Random();

	private final BooleanHolder storeLifeCycleEvents = new BooleanHolder(true);
	private final BooleanHolder	storeAlarmEvents = new BooleanHolder(true);
	private final BooleanHolder storeActionEvents= new BooleanHolder(true);
	private final Map eventStorageStatesMap	= new HashMap();

	public MTScenario()
	{
		eventStorageStatesMap.put("store-lifecycle-events", storeLifeCycleEvents);
		eventStorageStatesMap.put("store-alarm-events", storeAlarmEvents);
		eventStorageStatesMap.put("store-" + ActionEvent.EVENT_TYPE_LABEL + "-events", storeActionEvents);
	}


	abstract public MTScenarioSession newSession(int sessionId, String arg) throws ClifException;


	///////////////////////////////////////////////////
	// implementation of interface BindingController //
	///////////////////////////////////////////////////


	public Object lookupFc(String clientItfName)
	{
		if (clientItfName.equals(DataCollectorWrite.DATA_COLLECTOR_WRITE))
		{
			return dcw;
		}
		else if (clientItfName.equals(BladeInsertResponse.BLADE_INSERT_RESPONSE))
		{
			return sr;
		}
		else
		{
			return null;
		}
	}


	public void bindFc(String clientItfName, Object serverItf)
	{
		if (clientItfName.equals(DataCollectorWrite.DATA_COLLECTOR_WRITE))
		{
			synchronized (dc_lock)
			{
				dcw = (DataCollectorWrite) serverItf;
			}
		}
		else if (clientItfName.equals(BladeInsertResponse.BLADE_INSERT_RESPONSE))
		{
			synchronized (sr_lock)
			{
				sr = (BladeInsertResponse) serverItf;
			}
		}
	}


	public void unbindFc(String clientItfName)
	{
		if (clientItfName.equals(DataCollectorWrite.DATA_COLLECTOR_WRITE))
		{
			synchronized (dc_lock)
			{
				dcw = null;
			}
		}
		else if (clientItfName.equals(BladeInsertResponse.BLADE_INSERT_RESPONSE))
		{
			synchronized (sr_lock)
			{
				sr = null;
			}
		}
	}


	public String[] listFc()
	{
		return interfaceNames;
	}


	//////////////////////////////////////////////
	// implementation of interface BladeControl //
	//////////////////////////////////////////////


	/**
	 * initializes a new test, creating and starting the given number of activity threads,
	 * and returns as soon as every thread has been actually started
	 * @param testId unique identifier of the new test
	 */
	public void init(Serializable testId)
		throws ClifException
	{
		synchronized(scenario_lock)
		{
			this.testId = testId;
			started = stopped = suspended = false;
			threads = new Activity[arg_thread_nb];
			thr_waiting = 0;
			Thread.currentThread().setContextClassLoader(ClifClassLoader.getClassLoader());
			for (thr_remaining = 0 ; thr_remaining < arg_thread_nb ; ++thr_remaining)
			{
				threads[thr_remaining] = new Activity(thr_remaining, newSession(thr_remaining, sessionArg));
				threads[thr_remaining].start();
			}
			synchronized (activities_lock)
			{
				if (thr_waiting != thr_remaining)
				{
					try
					{
						activities_lock.wait();
					}
					catch (InterruptedException ex)
					{
						ex.printStackTrace(System.err);
					}
				}
			}
			timer = new StopTimer(arg_duration_s);
		}
	}


	/**
	 * releases every activity thread
	 */
	public void start()
	{
		synchronized (scenario_lock)
		{
			started = true;
			scenario_lock.notifyAll();
		}
		synchronized (activities_lock)
		{
			if (thr_waiting != 0)
			{
				try
				{
					activities_lock.wait();
				}
				catch (InterruptedException ex)
				{
					ex.printStackTrace(System.err);
				}
			}
		}
		timer.start();
	}


	public void stop()
	{
		synchronized (scenario_lock)
		{
			stopped = true;
			if (! started)
			{
				scenario_lock.notifyAll();
			}
		}
		if (suspended)
		{
			resume();
		}
		else if (Thread.currentThread() != timer && timer != null)
		{
			synchronized (timer)
			{
				timer.interrupt();
			}
		}
		synchronized (activities_lock)
		{
			while (thr_remaining != 0)
			{
				try
				{
					activities_lock.wait();
				}
				catch (InterruptedException ex)
				{
					ex.printStackTrace(System.err);
				}
			}
		}
	}


	public void suspend()
	{
		synchronized (scenario_lock)
		{
			suspended = true;
		}
		synchronized (activities_lock)
		{
			if (thr_waiting != thr_remaining)
			{
				try
				{
					activities_lock.wait();
				}
				catch (InterruptedException ex)
				{
					ex.printStackTrace(System.err);
				}
			}
		}
		synchronized (timer)
		{
			timer.interrupt();
		}
	}


	public void resume()
	{
		synchronized (scenario_lock)
		{
			suspended = false;
			scenario_lock.notifyAll();
		}
		synchronized (activities_lock)
		{
			if (thr_waiting != 0)
			{
				try
				{
					activities_lock.wait();
				}
				catch (InterruptedException ex)
				{
					ex.printStackTrace(System.err);
				}
			}
		}
		synchronized(timer)
		{
			timer.notify();
		}
	}


	public void join()
	{
		synchronized (activities_lock)
		{
			if (thr_remaining != 0)
			{
				try
				{
					activities_lock.wait();
				}
				catch (InterruptedException ex)
				{
					ex.printStackTrace(System.err);
				}
			}
		}
	}


	/**
	 * Sets number of threads and test duration parameters
	 * @param arg should begin with 2 integer parameters (separated with usual separators) setting
	 * (1) the number of threads and (2) the test duration (in seconds). The trailing String will
	 * be used as an argument when creating sessions.
	 * @see #newSession(int, String)
	 */
	public void setArgument(String arg)
	{
		StringTokenizer parser = new StringTokenizer(arg);
		try
		{
			arg_thread_nb = Integer.parseInt(parser.nextToken());
			arg_duration_s = Integer.parseInt(parser.nextToken());
			arg_rampup_duration_ms = Integer.parseInt(parser.nextToken()) * 1000;
			try
			{
				sessionArg = parser.nextToken("");
			}
			catch (NoSuchElementException ex)
			{
			}
		}
		catch (Exception ex)
		{
			System.err.println("MTScenario expects 2 arguments: <number_of_threads> <test duration in s>");
		}
	}


	/**
	 * Sets this scenario's unique identifier
	 */
	public void setId(String id)
	{
		scenarioId = id;
	}


	/**
	 * @return the scenario/blade identifier
	 */
	 public String getId()
	{
		return scenarioId;
	}


	 public void changeParameter(String parameter, Serializable value) throws ClifException {
		if (EventStorageState.setEventStorageState(eventStorageStatesMap, parameter, value)) {
			dcw.setFilter(new GenericFilter(storeActionEvents.getBooleanValue(), storeAlarmEvents.getBooleanValue(), storeLifeCycleEvents.getBooleanValue(), false));
			return;
		}
	 }

	 public Map getCurrentParameters() {
		Map parameters = new HashMap();

		EventStorageState.putEventStorageStates(parameters, eventStorageStatesMap);
		return parameters;
	 }

	//////////////////////////////////////////
	// inner timer class for scheduled stop //
	//////////////////////////////////////////


	class StopTimer extends Thread
	{
		long delay;

		public StopTimer(int delay_s)
		{
			super("MTScenario stop timer " + delay_s + "s");
			delay = delay_s*1000;
		}

		public void run()
		{
			long ellapsed_time = 0;
			while (ellapsed_time < delay && ! stopped)
			{
				long start_time = System.currentTimeMillis();
				try
				{
					sleep(delay - ellapsed_time);
					ellapsed_time = delay;
				}
				catch (InterruptedException ex)
				{
					ellapsed_time += System.currentTimeMillis() - start_time;
					synchronized(this)
					{
						if (suspended)
						{
							try
							{
								wait();
							}
							catch (InterruptedException exc)
							{
								exc.printStackTrace(System.err);
							}
						}
					}
				}
			}
			if (! stopped)
			{
				MTScenario.this.stop();
				synchronized (sr_lock)
				{
					if (sr != null)
					{
						sr.completed();
					}
				}
			}
		}
	}


	//////////////////////////////////////
	// inner class for activity threads //
	//////////////////////////////////////


	class Activity extends Thread
	{
		int sessionId;
		MTScenarioSession session;
		long iteration = 0;


		public Activity(int sessionId, MTScenarioSession session)
		{
			super(session + " MTScenarioSession #" + sessionId);
			this.sessionId = sessionId;
			this.session = session;
		}


		public void run()
		{
			synchronized (activities_lock)
			{
				if (++thr_waiting == thr_remaining)
				{
					activities_lock.notify();
				}
			}
			synchronized (scenario_lock)
			{
				if (! started && ! stopped)
				{
					try
					{
						scenario_lock.wait();
					}
					catch (InterruptedException ex)
					{
						ex.printStackTrace(System.err);
					}
				}
			}
			try
			{
				sleep(random.nextInt(arg_rampup_duration_ms));
			}
			catch (InterruptedException ex)
			{
			}
			synchronized (activities_lock)
			{
				if (--thr_waiting == 0)
				{
					activities_lock.notify();
				}
			}
			while (! stopped)
			{
				action();
				synchronized (scenario_lock)
				{
					if (suspended)
					{
						synchronized (activities_lock)
						{
							if (++thr_waiting == thr_remaining)
							{
								activities_lock.notify();
							}
						}
						try
						{
							scenario_lock.wait();
						}
						catch (InterruptedException ex)
						{
							ex.printStackTrace(System.err);
						}
						synchronized (activities_lock)
						{
							if (--thr_waiting == 0)
							{
								activities_lock.notify();
							}
						}
					}
				}
			}
			synchronized (activities_lock)
			{
				if (--thr_remaining == 0)
				{
					activities_lock.notify();
				}
			}
		}


		void action()
		{
			ActionEvent report = new ActionEvent();
			report.sessionId = sessionId;
			report.iteration = iteration++;
			report = session.action(report);
			synchronized (dc_lock)
			{
				if (dcw != null)
				{
					dcw.add(report);
				}
			}
		}
	}
}
