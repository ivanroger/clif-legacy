/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2003 France Telecom R&D
 * Copyright (C) 2003 INRIA
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * CLIF $Name: not supported by cvs2svn $
 *
 * Contact: clif@ow2.org
 *
 * @authors: Julien Buret
 * @authors: Nicolas Droze
 */

package org.ow2.clif.scenario.transitions;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.NoSuchElementException;
import java.util.Properties;
import java.util.Random;
import java.util.Stack;
import java.util.StringTokenizer;

import org.ow2.clif.util.ClifClassLoader;

/**
 * This class provides utilities to design a scenario, which deals with these
 * aspects: <br>- Probability for the virtual user to reach a given link. <br>-
 * Probability for the virtual user to click on the back button. <br>-
 * Probability for the virtual user to end the session. <br>- Think time
 * between each virtual user action. <br>
 * <br>
 * The structure is a matrix that represents the probability for a virtual user
 * to navigate from a specific page to another one. <br>
 * <br>
 * This class need an external text file which has the following structure: <br>
 * <br>- Each value at ligne x and column y represents the probality for the
 * virtual user, to go from the "page y" to the "page x". <br>
 * It must be a value between 0 and 1.<br>
 *
 */
public class TransitionTable implements Cloneable {

	private int nbColumns;

	private int nbRows;

	// The matrix which contains transitions probabilities
	private float transitions[][];

	// The table that contains the transitions think time
	private int transitionsTime[];

	private int beforeStep;

	private Random rand = new Random();

	// The stack of the previous states, useful when the user click on back
	// button
	private Stack previousStates = new Stack();

	// The current state is the initial state (first column of the matrix)
	private int currentState = 0;

	// Use the think time specified in the matrix
	private boolean useMatrixThinkTime;

	private boolean identifyByOrigin;

	// The table that contains all the names of the pages
	private String[] stateNames;

	// The table which makes the association between the state and the action
	private String[] action;

	private String[][] actionByOrigin;

	private boolean isBackClicked;

	private Properties prop = new Properties();

	private Transition trans = new Transition();

	private boolean DEBUG = false;

	/**
	 * Create a transition table with specifics files and give information about
	 * how to handle think time. <br>
	 * In all cases, the scenario can handle its own think time in addition to
	 * one describe above.
	 *
	 * @param filename
	 *            The transition file to load data from.
	 * @param actions_file
	 *            The file containing the actions.
	 * @param useMatrixThinkTime
	 *            true means that think time in the matrix are used.
	 * @param identifyByOrigin
	 *            true means that the actions depends on the previous state.
	 */
	public TransitionTable(String filename, String actions_file,
			boolean useMatrixThinkTime, boolean identifyByOrigin) {

		this.useMatrixThinkTime = useMatrixThinkTime;
		this.identifyByOrigin = identifyByOrigin;
		readMatrixTextFile(filename);
		readActionsTextFile(actions_file);
		trans.setProperties(prop);
	}

	/**
	 * Empty constructor, build a new empty transition table object used in
	 * createnewtransitiontable method
	 */
	private TransitionTable() {
	}

	/**
	 * Create a new transition table, which have the same table than it table
	 *
	 * @return the new transition table
	 */
	public TransitionTable createNewTransitionTable() {
		// create an empty object
		TransitionTable copy = new TransitionTable();
		// clone all values of this transition table to the clone
		copy.nbColumns = this.nbColumns;
		copy.nbRows = this.nbRows;
		copy.transitions = new float[this.transitions.length][transitions[0].length];
		for (int i = 0; i < this.transitions.length; i++) {
			for (int j = 0; j < this.transitions[i].length; j++) {
				copy.transitions[i][j] = this.transitions[i][j];
			}
		}
		copy.transitionsTime = new int[this.transitionsTime.length];
		for (int i = 0; i < this.transitionsTime.length; i++) {
			copy.transitionsTime[i] = this.transitionsTime[i];
		}
		copy.beforeStep = this.beforeStep;
		copy.rand = new Random();
		// copy the stack
		copy.previousStates = new Stack();
		copy.useMatrixThinkTime = this.useMatrixThinkTime;
		copy.identifyByOrigin = this.identifyByOrigin;
		copy.stateNames = new String[this.stateNames.length];
		for (int i = 0; i < this.stateNames.length; i++) {
			copy.stateNames[i] = new String(this.stateNames[i]);
		}
		copy.action = new String[this.action.length];
		for (int i = 0; i < this.action.length; i++) {
			copy.action[i] = new String(this.action[i]);
		}
		if (this.actionByOrigin != null) {
			copy.actionByOrigin = new String[this.actionByOrigin.length][this.actionByOrigin[0].length];
			for (int i = 0; i < this.actionByOrigin.length; i++) {
				if (this.actionByOrigin[i] != null) {
					for (int j = 0; j < this.actionByOrigin[i].length; j++) {
						copy.actionByOrigin[i][j] = this.actionByOrigin[i][j];
					}
				}
			}
		}
		copy.isBackClicked = this.isBackClicked;
		copy.prop = new Properties(this.prop);
		copy.trans = new Transition();
		return copy;
	}

	/**
	 * This method is used to reset all the previous states
	 */
	public void resetPreviousState() {
		this.currentState = 0;
		this.previousStates.clear();
	}

	/**
	 * Fill all the structures reading the file.
	 *
	 * @param filename
	 *            The file to load data from.
	 * @return true if the loading is successful.
	 */
	private boolean readMatrixTextFile(String filename) {
		BufferedReader reader;
		Float f;
		Integer t;

		// Try to open the file
		try {
			reader = new BufferedReader(new FileReader(filename));
		} catch (FileNotFoundException fnf) {
			try {
				reader = new BufferedReader(
					new InputStreamReader(
						ClifClassLoader.getClassLoader().getResourceAsStream(filename)));
			} catch (Exception e) {
				System.err.println("File " + filename + " not found. " + e);
				return false;
			}
		}

		// Now read the file using tab (\t) as field delimiter
		try {

			while (!reader.readLine().startsWith("#")) {
			}

			reader.readLine(); // From >>>

			// Column headers
			StringTokenizer st = new StringTokenizer(reader.readLine(), "\t");

			// Each state + Wait time + 1st column
			nbColumns = st.countTokens();

			// Each state + Back + End Session
			nbRows = nbColumns;

			stateNames = new String[nbRows];
			transitions = new float[nbRows][nbColumns - 2];
			transitionsTime = new int[nbRows];

			// Read the matrix
			for (int i = 0; i < nbRows; i++) {
				st = new StringTokenizer(reader.readLine(), "\t");
				// The first column is the name of the state.
				stateNames[i] = st.nextToken();

				// For all the columns except the last one
				for (int j = 0; j < nbColumns - 2; j++) {
					// We get the value
					f = new Float(st.nextToken());
					// And add the value to the matrix
					transitions[i][j] = f.floatValue();
				}

				// Last column is transition_waiting_time
				t = new Integer(st.nextToken());
				transitionsTime[i] = t.intValue();
			}

			reader.close();

		} catch (IOException ioe) {
			System.err.println("An error occured while reading " + filename
					+ ". (" + ioe.getMessage() + ")");
			return false;
		} catch (NoSuchElementException nsu) {
			System.err.println("File format error in file " + filename
					+ " Reason: " + nsu.getMessage());
			return false;
		} catch (NumberFormatException ne) {
			System.err.println("Number format error in file " + filename
					+ "Reason: " + ne.getMessage());
			return false;
		}

		return true;
	}

	/**
	 * Read the file containing the actions.
	 *
	 * @param filename
	 * @return true if the file was successfully read, false otherwise
	 */
	private boolean readActionsTextFile(String filename) {
		BufferedReader reader;
		StringTokenizer st;
		String line;
		int j;

		// Try to open the file
		try {
			reader = new BufferedReader(new FileReader(filename));
		} catch (FileNotFoundException fnf) {
			try {
				reader = new BufferedReader(new InputStreamReader(this
						.getClass().getClassLoader().getResourceAsStream(
								filename)));
			} catch (Exception e) {
				System.err.println("File " + filename + " not found. " + e);
				return false;
			}
		}

		try {

			while (!(line = reader.readLine()).startsWith("#")) {
			}

			// If the current line is the transition separator
			// We reach the next separator
			if (line.indexOf("Transition") != -1) {
				while (!(line = reader.readLine()).startsWith("#")) {
				}
			}
			// If the current line is the variables separator
			// We load properties until another separator is reached
			if (line.indexOf("Variable") != -1) {
				while (!(line = reader.readLine()).startsWith("#"))
					loadProperty(line);
			}

			if (line.indexOf("Action") != -1) {
				// Read the action matrix

				// This structure contains the association between the name of
				// the page
				// and its action.
				if (identifyByOrigin)
					actionByOrigin = new String[nbColumns - 2][nbColumns - 2];
				else
					action = new String[nbColumns - 2];

				if (identifyByOrigin) {

					for (int i = 0; i < nbColumns - 2; i++) {
						st = new StringTokenizer(reader.readLine(), "\t");
						st.nextToken(); // State name
						j = 0;
						while (st.hasMoreTokens()) {
							actionByOrigin[i][j] = st.nextToken();
							j++;
						}
					}
				} else {

					for (int i = 0; i < nbColumns - 2; i++) {
						st = new StringTokenizer(reader.readLine(), "\t");
						st.nextToken(); // State name
						action[i] = st.nextToken();
					}
				}
			}

			reader.close();

		} catch (IOException ioe) {
			System.err.println("An error occured while reading " + filename
					+ ". (" + ioe.getMessage() + ")");
			return false;
		} catch (NoSuchElementException nsu) {
			System.err.println("File format error in file " + filename
					+ " Reason: " + nsu.getMessage());
			return false;
		} catch (NumberFormatException ne) {
			System.err.println("Number format error in file " + filename
					+ "Reason: " + ne.getMessage());
			return false;
		}

		return true;
	}

	/**
	 * For debugging purpose. Trace all the structures.
	 */
	private void displayMatrix() {

		System.out.println("Listing table states: ");
		System.out.println("--------------------------");
		for (int i = 0; i < stateNames.length; i++) {
			System.out.println(stateNames[i]);
		}

		System.out.println();

		System.out.println("Listing transition table: ");
		System.out.println("------------------------------");
		for (int i = 0; i < transitions.length; i++) {
			for (int j = 0; j < transitions[0].length; j++) {
				System.out.print("(" + i + "," + j + ")" + transitions[i][j]
						+ " ");
			}
			System.out.println();
		}

		if (!identifyByOrigin) {

			System.out.println();

			System.out.println("Listing state action: ");
			System.out.println("------------------------");
			for (int i = 0; i < action.length; i++) {
				System.out.println(action[i]);
			}
		} else {

			System.out.println();

			System.out.println("Listing state action by origin: ");
			System.out.println("-------------------------------------");
			for (int i = 0; i < actionByOrigin.length; i++) {
				for (int j = 0; j < actionByOrigin[0].length; j++) {
					System.out.println("(" + i + "," + j + ")"
							+ actionByOrigin[i][j]);
				}
			}
		}

		System.out.println();

		System.out.println("Listing Wait time: ");
		System.out.println("-------------------");
		for (int i = 0; i < transitionsTime.length; i++) {
			System.out.println(transitionsTime[i]);
		}

		System.out.println();

		System.out.println("Listing variables: ");
		System.out.println("-------------------");
		for (int i = 0; i < prop.size(); i++) {
			System.out.println(prop.keys());
		}

	}

	/**
	 * This method compute the next state calculated with the matrix data.
	 *
	 * @return The value of the next state.
	 */
	private int nextState() {

		beforeStep = currentState;
		float step = rand.nextFloat();
		float cumul = 0;
		int i;

		if (DEBUG) {
			System.out.print("rand = " + step + " --- ");
			System.out.print("previous state = " + beforeStep + " --- ");
		}

		// Determine the next state with the random value
		for (i = 0; i < nbRows; i++) {
			cumul = cumul + transitions[i][currentState];
			if (step < cumul) {
				currentState = i;
				break;
			}
		}

		if (DEBUG)
			System.out.print("current state = " + currentState + " --- ");

		// Deal with Back to previous state
		if (currentState == nbRows - 2) {
			isBackClicked = true;
			if (DEBUG)
				System.out.println("Back...");
			if (previousStates.empty())
				System.out
						.println("Error detected: Trying to go back but no previous state is available (currentState:"
								+ currentState + ", beforeStep:" + beforeStep);
			else {
				try {
					if (useMatrixThinkTime)
						Thread
								.sleep((long) (transitionsTime[currentState]));
				} catch (java.lang.InterruptedException ie) {
					System.err.println("Thread "
							+ Thread.currentThread().getName()
							+ " has been interrupted.");
				}

				Integer previous = (Integer) previousStates.pop();
				currentState = previous.intValue();

				return currentState;
			}
		} else { // Add this state to history (previousStates) if needed
			isBackClicked = false;
			if (!isEndOfSession()) {
				if (DEBUG)
					System.out.print("Not end session --- ");
				// If there is no probability to go back from this state, just
				// empty the stack
				if (transitions[nbRows - 2][currentState] == 0) {
					if (DEBUG)
						System.out.println("no back possibility ");
					previousStates.removeAllElements();
				} else { // else add the previous state to the history just in
					// case we go back !
					if (DEBUG)
						System.out.println();
					previousStates.push(new Integer(beforeStep));
				}
			} else {
				currentState = -1;
				if (DEBUG)
					System.out.println("End session");
			}
		}

		if (currentState != -1) {
			try {

				if (useMatrixThinkTime)
					Thread
							.sleep((long) (transitionsTime[currentState]));

			} catch (java.lang.InterruptedException ie) {
				System.err.println("Thread " + Thread.currentThread().getName()
						+ " has been interrupted.");
			}
		}

		return currentState;
	}

	/**
	 * Tests if the current state is the End of Session state.
	 *
	 * @return true if the current state is the end of session.
	 */
	private boolean isEndOfSession() {
		return currentState == nbRows - 1;
	}

	/**
	 * Return the name of the current state
	 *
	 * @return The String representing the name of the current state in the
	 *         matrix
	 */
	public String getCurrentStateName() {
		return stateNames[currentState];
	}

	/**
	 * Return the time to wait for the current state
	 *
	 * @return the time to wait
	 */
	public long getCurrentWaitingTime() {
		return transitionsTime[currentState];
	}

	/**
	 * @return the action associated to the next state
	 */
	public Transition getNextTransition() {
		trans.setTransition(getStateTransition(nextState()));
		return trans;
	}

	private void resetToInitialState() {
		currentState = 0;
		previousStates.removeAllElements();
	}

	private boolean isBackClicked() {
		return isBackClicked;
	}

	/**
	 * This method transform a state value into its associated action.
	 *
	 * @param state
	 *            The state to transform into action
	 * @return The action of the specified state.
	 */
	private String getStateTransition(int state) {
		// If the current state is an end of session
		if (state == -1) {
			resetToInitialState();
			return null;
		} else if (identifyByOrigin) {
			// If the back action is invoked, then we have to invert the current
			// state with the previous state to get the previous action
			// properly.
			if (isBackClicked)
				return actionByOrigin[beforeStep][state];
			else
				return actionByOrigin[state][beforeStep];
		} else
			return action[state];
	}

	/**
	 * Store the variables into the properties.
	 *
	 * @param line
	 */
	private void loadProperty(String line) {
		int index = line.indexOf("=");
		prop.setProperty(line.substring(0, index), line.substring(index + 1,
				line.length()));
	}

}