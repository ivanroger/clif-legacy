/*
 * CLIF is a Load Injection Framework Copyright (C) 2005 France Telecom R&D
 * 
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option) any
 * later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 * CLIF
 * 
 * Contact: clif@ow2.org
 */
package org.ow2.clif.scenario.isac.engine;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Iterator;
import java.util.HashSet;
import java.util.Set;

/**
 * This class implements a timer based on a Clock object. A Timer can
 * periodically execute actions.
 * 
 * @author Emmanuel Varoquaux
 */
class Timer extends Thread {
	private static class TimerAction {
		private final Method	action;
		private final Object	obj;
		private long			period;
		private long			lastCall	= 0;

		private TimerAction(Method action, Object obj, long period) {
			this.action = action;
			this.obj = obj;
			this.period = period;
		}

		private void doAction() throws IllegalArgumentException,
				IllegalAccessException, InvocationTargetException {
			action.invoke(obj, new Object[0]);
		}
	}

	/* Parameters */
	private final Clock				clock;

	/* Fields */
	private static volatile long	nextId;
	private final Set<TimerAction>			actions		= new HashSet<TimerAction>();
	private volatile boolean		interrupted;
	private volatile long			period;
	private final Object			periodLock	= new Object();

	/**
	 * Initializes a <code>Timer</code> with the given <code>Clock</code>
	 * object. The Timer's thread is set to belong the <code>group</code> ThreadGroup.
	 * 
	 * @param group the thread group.
	 * @param clock the clock of reference of the new timer.
	 */
	protected Timer(ThreadGroup group, Clock clock) {
		super(group, "Timer " + nextId++);
		this.clock = clock;
	}

	private static long gcd(long a, long b) {
		while (b != 0) {
			long c = a % b;
			a = b;
			b = c;
		}
		return Math.abs(a);
	}

	/**
	 * Adds an action.
	 * 
	 * @param obj the object on which to invoke the method.
	 * @param methodName the name of the method to invoke.
	 * @param period the interval of time between two invocations, in
	 *            milliseconds (in the time of the timer's clock).
	 *            period may be negative. In this case, the action will never 
	 *            be executed. 
	 * @exception NoSuchMethodException if the method doesn't exist.
	 */
	protected void add(Object obj, String methodName, long period)
			throws NoSuchMethodException {
		synchronized (actions) {
			actions.add(new TimerAction(obj.getClass().getMethod(methodName,
					new Class[0]), obj, period));
			synchronized (periodLock) {
				this.period = gcd(this.period, period);
			}
		}
	}

	/**
	 * Sets the period of an action.
	 * 
	 * Looks for the action of the given object and method name in the internal
	 * registry. If not found, does nothing. If found, sets the period of the
	 * action to the given value.
	 * 
	 * @param obj the object on which to invoke the method.
	 * @param methodName the name of the method to invoke.
	 * @param period the interval of time between two invocations, in
	 *            milliseconds (in the time of the timer's clock).
	 *            period may be negative. In this case, the action will not 
	 *            be executed. 
	 */
	/* It is certainly not the faster mean to implement setPeriod,
	 * but it works, and fortunately this method is not called very often. */
	protected void setPeriod(Object obj, String methodName, long period) {
		synchronized (periodLock) {
			TimerAction action;
			Iterator<TimerAction> i;
			for (i = actions.iterator(); i.hasNext();) {
				action = i.next();
				if (action.obj == obj
						&& action.action.getName().equals(methodName)) {
					if (action.period == period)
						return;
					action.period = period;
					break;
				}
			}

			if (!i.hasNext())
				return;

			/* Re-compute the period... */
			this.period = 0;
			for (i = actions.iterator(); i.hasNext();)
				this.period = gcd(this.period, i.next().period);
		}
	}

	/**
	 * This method is called by the <code>start</code> method. Don't call it
	 * directly.
	 */
	/* We hope that this implementation is as precise as possible, even for very short periods. */
	@Override
	public void run() {
		long date;

		while (!interrupted) {

			/* get the current date */
			synchronized (clock) {
				date = clock.getDate();
				if (clock.isStopped()) {
					try {
						clock.wait();
					}
					catch (InterruptedException e) {
						return;
					}
				}
			}

			/* execute the actions */
			synchronized (actions) {
				TimerAction action;
				for (Iterator<TimerAction> i = actions.iterator(); i.hasNext()
						&& !interrupted;) {
					if ((action = i.next()).period < 0)
						continue;
					if (action.lastCall + action.period <= date) {
						action.lastCall = date;
						try {
							action.doAction();
						}
						/* Do nothing in case of exception. Simply re-throw it to the upper level. */
						catch (IllegalAccessException e) {
							throw new Error(e);
						}
						catch (InvocationTargetException e) {
							throw new Error(e);
						}
					}
				}
			}

			/* wait until the period elapsed */
			long length = period - (clock.getDate() - date);
			if (length <= 0)
				continue;

			/* Activating these lines may improve performances ! But it is strongly platform-dependent ! */
				/* A threshold of 1 gives better results than 0 for short periods,
				 * because code interpretation itself takes a (short) while. */
//				if (length <= 1)
//					continue;
				/* Ok, this is ugly. But the experience proves that for length == 2,
				 * the sleep method takes more time than 2ms, whereas the following trick
				 * gives the expected result. */
//				if (length == 2) {
//					yield();
//					continue;
//				}
				/* For length >= 3, sleep is suitable. */
			try {
				sleep(length);
			}
			catch (InterruptedException e) {
				return;
			}
		}
	}

	@Override
	public void interrupt() {
		interrupted = true;
		super.interrupt();
	}
}
