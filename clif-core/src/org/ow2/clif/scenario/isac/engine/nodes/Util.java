/*
 * CLIF is a Load Injection Framework Copyright (C) 2005 France Telecom R&D
 * 
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option) any
 * later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 * CLIF
 * 
 * Contact: clif@ow2.org
 */
package org.ow2.clif.scenario.isac.engine.nodes;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.ListIterator;
import java.util.Map;

import org.jdom.Attribute;
import org.jdom.Element;

/**
 * @author Emmanuel Varoquaux
 */
public class Util {

	public static ArrayList<InstructionNode> analyseBlock(ListIterator i)
			throws NodeException {
		ArrayList<InstructionNode> instructions = new ArrayList<InstructionNode>();
		Element e;
		String name;

		while (i.hasNext()) {
			e = (Element)i.next();
			name = e.getName();
			if (name.equals("sample"))
				instructions.add(new SampleNode(e));
			else if (name.equals("control"))
				instructions.add(new ControlNode(e));
			else if (name.equals("timer"))
				instructions.add(new TimerNode(e));
			else if (name.equals("while"))
				instructions.add(new WhileNode(e));
			else if (name.equals("preemptive"))
				instructions.add(new PreemptiveNode(e));
			else if (name.equals("if"))
				instructions.add(new IfNode(e));
			else if (name.equals("nchoice"))
				instructions.add(new NChoiceNode(e));
			else
				throw new UnexpectedElementException(name);
		}

		return instructions;
	}

	private static void analyseParam(Element element, Map<String,String> params)
			throws NodeException {
		ListIterator i;
		Attribute a;
		String name;
		String paramName, paramValue;

		paramValue = paramName = null;
		for (i = element.getAttributes().listIterator(); i.hasNext();) {
			a = (Attribute)i.next();
			name = a.getName();
			if (name.equals("name")) {
				if (paramName != null)
					throw new DuplicatedAttributeException("name");
				paramName = a.getValue();
			}
			else if (name.equals("value")) {
				if (paramValue != null)
					throw new DuplicatedAttributeException("value");
				paramValue = a.getValue();
			}
			else
				throw new UnexpectedAttributeException(name);
		}
		if (paramName == null)
			throw new MissingAttributeException("name");
		if (paramValue == null)
			throw new MissingAttributeException("value");
		params.put(paramName, paramValue);

		i = element.getChildren().listIterator();

		if (i.hasNext())
			throw new UnexpectedElementException(((Element)i.next()).getName());
	}

	protected static Map<String,String> analyseParams(Element element) throws NodeException {
		Map<String,String> params = new HashMap<String,String>();
		ListIterator i;
		Element e;
		String name;

		i = element.getAttributes().listIterator();
		if (i.hasNext())
			throw new UnexpectedAttributeException(((Attribute)i.next())
					.getName());

		i = element.getChildren().listIterator();

		while (i.hasNext()) {
			e = (Element)i.next();
			name = e.getName();
			if (!name.equals("param"))
				throw new BadElementException(name, "param");
			analyseParam(e, params);
		}

		return params;
	}

	protected static StringBuilder stringIndent(int indent, StringBuilder str)
	{
		while (indent-- > 0)
		{
			str.append("  ");
		}
		return str;
	}
}
