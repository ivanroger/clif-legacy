/*
 * CLIF is a Load Injection Framework Copyright (C) 2005 France Telecom R&D
 * 
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option) any
 * later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 * CLIF
 * 
 * Contact: clif@ow2.org
 */
package org.ow2.clif.scenario.isac.engine.nodes;

import java.util.ListIterator;
import java.util.Map;
import java.util.ArrayList;
import java.util.Stack;

import org.jdom.Attribute;
import org.jdom.Element;
import org.ow2.clif.scenario.isac.engine.PlugIn;
import org.ow2.clif.scenario.isac.engine.instructions.GotoInstruction;
import org.ow2.clif.scenario.isac.engine.instructions.Instruction;
import org.ow2.clif.scenario.isac.engine.instructions.TestInstruction;

/**
 * @author Emmanuel Varoquaux
 */
public class IfNode extends ConditionedNode {
	public ArrayList<InstructionNode> instructionsThen;
	public ArrayList<InstructionNode> instructionsElse;

	private ArrayList<InstructionNode> analyseBlock(Element element) throws NodeException {
		ListIterator i;

		i = element.getAttributes().listIterator();

		if (i.hasNext())
			throw new UnexpectedAttributeException(((Attribute)i.next()).getName());
		
		return Util.analyseBlock(element.getChildren().listIterator());
	}

	public IfNode(Element element) throws NodeException {
		ListIterator i;
		Element e;
		String name;

		i = element.getAttributes().listIterator();
		if (i.hasNext())
			throw new UnexpectedAttributeException(((Attribute)i.next())
					.getName());

		i = element.getChildren().listIterator();

		if (i.hasNext()) {
			e = (Element)i.next();
			name = e.getName();
			if (!name.equals("condition"))
				throw new BadElementException(name, "condition");
			condition = new ConditionNode(e);
		}
		else
			throw new MissingElementException("condition");

		if (i.hasNext()) {
			e = (Element)i.next();
			name = e.getName();
			if (!name.equals("then"))
				throw new BadElementException(name, "then");
			instructionsThen = analyseBlock(e);
		}
		else
			throw new MissingElementException("then");

		if (i.hasNext()) {
			e = (Element)i.next();
			name = e.getName();
			if (!name.equals("else"))
				throw new BadElementException(name, "else");
			instructionsElse = analyseBlock(e);
		}
	}

	@Override
	public StringBuilder indentedToString(int indent, StringBuilder str)
	{
		Util.stringIndent(indent, str);
		System.out.print("if: ");
		condition.toString();
		Util.stringIndent(indent, str);
		System.out.println("then:");
		for (int i = 0, n = instructionsThen.size(); i < n; i++)
		{
			instructionsThen.get(i).indentedToString(indent + 1, str);
		}
		if (instructionsElse != null)
		{
			Util.stringIndent(indent, str);
			System.out.println("else:");
			for (int i = 0, n = instructionsElse.size(); i < n; i++)
			{
				instructionsElse.get(i).indentedToString(indent + 1, str);
			}
		}
		return str;
	}

	@Override
	public void compile(Map<String,PlugIn> plugIns, ArrayList<Instruction> code, Stack<TestInstruction> conditions)
			throws Exception {
		TestInstruction t;
		GotoInstruction g;

		condition.checkConditions(code, conditions);
		code
				.add(t = new TestInstruction(condition.use, condition
						.getMethodNumber(plugIns), condition
						.split(condition.params), 0));
		for (int i = 0, n = instructionsThen.size(); i < n; i++)
		{
			instructionsThen.get(i).compile(plugIns, code, conditions);
		}
		if (instructionsElse != null)
		{
			code.add(g = new GotoInstruction(0));
			t.labelFalse = code.size();
			for (int i = 0, n = instructionsElse.size(); i < n; i++)
			{
				instructionsElse.get(i).compile(plugIns, code, conditions);
			}
			g.label = code.size();
		}
		else
			t.labelFalse = code.size();
	}
}
