/*
 * CLIF is a Load Injection Framework Copyright (C) 2005 France Telecom R&D
 * 
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option) any
 * later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 * CLIF
 * 
 * Contact: clif@ow2.org
 */
package org.ow2.clif.scenario.isac.engine.nodes;

import java.util.HashMap;
import java.util.Iterator;
import java.util.ListIterator;
import java.util.Map;

import org.jdom.Attribute;
import org.jdom.Element;

/**
 * @author Emmanuel Varoquaux
 */
public class RampNode {
	public static class Point {
		public int	x;
		public int	y;

		public Point(int x, int y) {
			this.x = x;
			this.y = y;
		}
	}

	private static final String[]	styles	= {"line", "crenel_vh", "crenel_hv"};

	public String style;
	public Map<String,Point> points	= new HashMap<String,Point>();

	private void verify() throws NodeException {
		int i, n;

		for (i = 0, n = styles.length; i < n; i++) {
			if (style.equals(styles[i]))
				break;
		}
		if (i == n)
			throw new NodeException("Style \"" + style
					+ "\" unknown in ramp element");
	}

	private void analysePoint(Element element) throws NodeException {
		ListIterator i;
		Attribute a;
		String name;
		String pointName;
		int x, y;
		boolean xDefined, yDefined;

		pointName = null;
		y = x = 0;
		yDefined = xDefined = false;
		for (i = element.getAttributes().listIterator(); i.hasNext();) {
			a = (Attribute)i.next();
			name = a.getName();
			if (name.equals("name")) {
				if (pointName != null)
					throw new DuplicatedAttributeException("name");
				pointName = a.getValue();
			}
			else if (name.equals("x")) {
				if (xDefined)
					throw new DuplicatedAttributeException("x");
				x = Integer.parseInt(a.getValue());
				xDefined = true;
			}
			else if (name.equals("y")) {
				if (yDefined)
					throw new DuplicatedAttributeException("y");
				y = Integer.parseInt(a.getValue());
				yDefined = true;
			}
			else
				throw new UnexpectedAttributeException(name);
		}
		if (!xDefined)
			throw new MissingAttributeException("x");
		if (!yDefined)
			throw new MissingAttributeException("y");

		i = element.getChildren().listIterator();

		if (i.hasNext())
			throw new UnexpectedElementException(((Element)i.next()).getName());

		if (pointName == null)
			if (points.containsKey("start"))
				pointName = "end";
			else
				pointName = "start";
		points.put(pointName, new Point(x, y));
	}

	private void analysePoints(Element element) throws NodeException {
		ListIterator i;
		Element e;
		String name;

		i = element.getAttributes().listIterator();
		if (i.hasNext())
			throw new UnexpectedAttributeException(((Attribute)i.next())
					.getName());

		i = element.getChildren().listIterator();

		if (!i.hasNext())
			throw new MissingElementException("point");
		e = (Element)i.next();
		name = e.getName();
		if (!name.equals("point"))
			throw new BadElementException(name, "point");
		analysePoint(e);

		if (!i.hasNext())
			throw new MissingElementException("point");
		e = (Element)i.next();
		name = e.getName();
		if (!name.equals("point"))
			throw new BadElementException(name, "point");
		analysePoint(e);

		if (i.hasNext())
			throw new UnexpectedElementException(((Element)i.next()).getName());
	}

	public RampNode(Element element) throws NodeException {
		ListIterator i;
		Attribute a;
		Element e;
		String name;

		for (i = element.getAttributes().listIterator(); i.hasNext();) {
			a = (Attribute)i.next();
			name = a.getName();
			if (name.equals("style")) {
				if (style != null)
					throw new DuplicatedAttributeException("style");
				style = a.getValue();
			}
			else
				throw new UnexpectedAttributeException(name);
		}
		if (style == null)
			throw new MissingAttributeException("style");

		i = element.getChildren().listIterator();

		if (!i.hasNext())
			throw new MissingElementException("points");
		e = (Element)i.next();
		name = e.getName();
		if (!name.equals("points"))
			throw new BadElementException(name, "points");
		analysePoints(e);
		
		if (i.hasNext())
			throw new UnexpectedElementException(((Element)i.next()).getName());
		
		verify();
	}

	public void print() {
		System.out.println("  ramp: style=\"" + style + "\"");
		for (Iterator<Map.Entry<String,Point>> i = points.entrySet().iterator(); i.hasNext();) {
			Map.Entry<String,Point> entry = i.next();
			String name = entry.getKey();
			Point point = entry.getValue();
			System.out.println("    point: name=\"" + name + "\", x=\""
					+ point.x + "\", y=\"" + point.y + "\"");
		}
	}
}
