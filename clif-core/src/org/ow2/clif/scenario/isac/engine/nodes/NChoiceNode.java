/*
 * CLIF is a Load Injection Framework Copyright (C) 2005 France Telecom R&D
 * 
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option) any
 * later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 * CLIF
 * 
 * Contact: clif@ow2.org
 */
package org.ow2.clif.scenario.isac.engine.nodes;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.ListIterator;
import java.util.Map;
import java.util.ArrayList;
import java.util.Stack;

import org.jdom.Attribute;
import org.jdom.Element;
import org.ow2.clif.scenario.isac.engine.PlugIn;
import org.ow2.clif.scenario.isac.engine.instructions.GotoInstruction;
import org.ow2.clif.scenario.isac.engine.instructions.Instruction;
import org.ow2.clif.scenario.isac.engine.instructions.NChoiceInstruction;
import org.ow2.clif.scenario.isac.engine.instructions.TestInstruction;

/**
 * @author Emmanuel Varoquaux
 */
public class NChoiceNode implements InstructionNode {
	public class ChoiceNode {
		public String		proba;
		public ArrayList<InstructionNode>	instructions;

		private ChoiceNode(Element element) throws NodeException {
			ListIterator i;
			Attribute a;
			String name;
			int tmp;

			for (i = element.getAttributes().listIterator(); i.hasNext();) {
				a = (Attribute)i.next();
				name = a.getName();
				if (name.equals("proba")) {
					if (proba != null)
						throw new DuplicatedAttributeException("proba");
					proba = a.getValue();
				}
				else
					throw new UnexpectedAttributeException(name);
			}
			if (proba == null)
				throw new MissingAttributeException("proba");
			tmp = Integer.parseInt(proba);
			if (tmp < 0)
				throw new IllegalAttributeValueException(proba);
			total += tmp;

			instructions = Util.analyseBlock(element.getChildren()
					.listIterator());
		}
	}

	private int			total;

	public Collection<ChoiceNode>	choices	= new HashSet<ChoiceNode>();

	public NChoiceNode(Element element) throws NodeException {
		ListIterator i;
		Element e;
		String name;

		i = element.getAttributes().listIterator();
		if (i.hasNext())
			throw new UnexpectedAttributeException(((Attribute)i.next())
					.getName());

		i = element.getChildren().listIterator();

		if (!i.hasNext())
			throw new MissingElementException("choice");
		do {
			e = (Element)i.next();
			name = e.getName();
			if (!name.equals("choice"))
				throw new BadElementException(name, "choice");
			choices.add(new ChoiceNode(e));
		}
		while (i.hasNext());
		if (total == 0)
			throw new NodeException(
					"Sum of probabilities is zero in \"nchoice\" element");
	}

	@Override
	public StringBuilder indentedToString(int indent, StringBuilder str)
	{
		Util.stringIndent(indent, str);
		System.out.println("nchoice:");
		for (Iterator<ChoiceNode> i = choices.iterator(); i.hasNext();) {
			ChoiceNode choice = i.next();
			Util.stringIndent(indent, str);
			System.out.println("choice: proba=\"" + choice.proba + "\"");
			for (int i1 = 0, n1 = choice.instructions.size(); i1 < n1; i1++)
			{
				choice.instructions.get(i1).indentedToString(indent + 1, str);
			}
		}
		return str;
	}

	@Override
	public void compile(Map<String,PlugIn> plugIns, ArrayList<Instruction> code, Stack<TestInstruction> conditions)
			throws Exception {
		ArrayList<GotoInstruction> gotos = new ArrayList<GotoInstruction>();
		ArrayList<NChoiceInstruction.Choice> choices = new ArrayList<NChoiceInstruction.Choice>();
		int threshold = 0;

		code.add(new NChoiceInstruction(total, choices));
		for (Iterator<ChoiceNode> i = this.choices.iterator(); i.hasNext();) {
			ChoiceNode choice = i.next();
			threshold += Integer.parseInt(choice.proba);
			choices.add(new NChoiceInstruction.Choice(threshold, code.size()));
			for (int i1 = 0, n1 = choice.instructions.size(); i1 < n1; i1++)
			{
				choice.instructions.get(i1).compile(plugIns, code, conditions);
			}
			if (i.hasNext()) {
				GotoInstruction g = new GotoInstruction(0);
				gotos.add(g);
				code.add(g);
			}
		}
		for (int i = 0; i < gotos.size(); i++)
		{
			gotos.get(i).label = code.size();
		}
	}
}
