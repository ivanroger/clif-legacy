/*
 * CLIF is a Load Injection Framework Copyright (C) 2005 France Telecom R&D
 * 
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option) any
 * later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 * CLIF
 * 
 * Contact: clif@ow2.org
 */
package org.ow2.clif.scenario.isac.engine;

import java.util.LinkedList;
import java.util.ListIterator;

/**
 * @author Emmanuel Varoquaux
 */
class LoadProfile {
	
	/* Represents a vertex of the load profile graph. */
	protected static class Vertex {
		static final int	NONE		= 0;
		static final int	LINE		= 1;
		static final int	CRENEL_VH	= 2;
		static final int	CRENEL_HV	= 3;

		long				date;
		int					load;
		int					style;

		Vertex(long date, int load, int style) {
			this.date = date;
			this.load = load;
			this.style = style;
		}
	}

	private final LinkedList<Vertex>	data	= new LinkedList<Vertex>();

	protected void add(Vertex v) throws Exception {
		Vertex previous = null;
		int pos = 0;
		for (ListIterator<Vertex> i = data.listIterator(); i.hasNext() && (previous = i.next()).date < v.date; pos++);
		if (previous != null && previous.date == v.date) {
			if (previous.style != Vertex.NONE)
				throw new Exception("LoadProfile: bad load profile");
			data.set(pos, v);
		}
		else
			data.add(pos, v);		
	}
	
	protected int load(long date) {
		Vertex previous = null;
		for (Vertex v : data)
		{
			if (date <= v.date) {
				if (previous == null)
					return 0;
				switch (previous.style) {
				case Vertex.LINE:
					if (date == v.date)    /* easy short-cut */
						return v.load;
					return previous.load + (int)((v.load - previous.load)
							* (date - previous.date) / (v.date - previous.date));
				case Vertex.CRENEL_VH:
					return date == previous.date ? previous.load : v.load;
				case Vertex.CRENEL_HV:
					return date == v.date ? v.load : previous.load;
				default:
					if (date == previous.date)
						return previous.load;
					if (date == v.date)
						return v.load;
					return 0;
				}
			}
			previous = v;
		}
		return 0;
	}

	protected long endDate() {
		return data.getLast().date;
	}
}
