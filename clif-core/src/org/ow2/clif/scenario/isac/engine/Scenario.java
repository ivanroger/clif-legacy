/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2005, 2006, 2010 France Telecom
 * 
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option) any
 * later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 * CLIF
 * 
 * Contact: clif@ow2.org
 */
package org.ow2.clif.scenario.isac.engine;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;
import org.ow2.clif.scenario.isac.engine.instructions.Instruction;
import org.ow2.clif.scenario.isac.engine.nodes.BadRootElementException;
import org.ow2.clif.scenario.isac.engine.nodes.BehaviorNode;
import org.ow2.clif.scenario.isac.engine.nodes.GroupNode;
import org.ow2.clif.scenario.isac.engine.nodes.NodeException;
import org.ow2.clif.scenario.isac.engine.nodes.PlugInNode;
import org.ow2.clif.scenario.isac.engine.nodes.RampNode;
import org.ow2.clif.scenario.isac.engine.nodes.ScenarioNode;
import org.ow2.clif.scenario.isac.plugin.SessionObjectAction;
import org.ow2.clif.util.ClifClassLoader;
import org.ow2.clif.util.XMLEntityResolver;

/**
 * This class collects data from a scenario XML file. It provides a
 * constructor which initializes the class given the scenario file name.
 * @author Emmanuel Varoquaux
 * @author Bruno Dillenseger
 */
class Scenario {
	/* Root node of the XML tree, used in the constructor, kept only for printing. */
	private final ScenarioNode	scenario;

	 /* maps plug-in ids to a specimen of initialized session objects for this plug-in. */
	private final Map<String,SessionObjectAction> specimenMap = new HashMap<String,SessionObjectAction>();

	/* Maps behavior ids to their compiled pseudo-code */
	private final Map<String,List<Instruction>>			codeMap;

	/* maps plug-in names to PlugIn objects */
	Map<String,PlugIn> plugInsByNames = new HashMap<String,PlugIn>();

	/* maps plug-in ids to PlugIn objects. We will need this later to compile behaviors. */
	Map<String,PlugIn> plugInsByIds = new HashMap<String,PlugIn>();

	private static ScenarioNode JDOMBuild(String fileName)
			throws NodeException, JDOMException, IOException {
		InputStream in;
		String name;
		Element e;
		
		if ((in = ClifClassLoader.getClassLoader()
				.getResourceAsStream(fileName)) == null)
			throw new FileNotFoundException(fileName);
		SAXBuilder parser = new SAXBuilder();
		parser.setEntityResolver(new XMLEntityResolver());
		if (!(name = (e = parser.build(in).getRootElement())
			.getName()).equals("scenario"))
	throw new BadRootElementException(name, "scenario");
		return new ScenarioNode(e);
	}

	/**
	 * Constructs a new <code>Scenario</code> from the given file.
	 * 
	 * @param fileName the scenario XML file.
	 * @throws Exception if an error occurs.
	 */
	protected Scenario(String fileName) throws Exception {

		/* Parse the XML scenario file and build the corresponding tree */
		scenario = JDOMBuild(fileName);
		for (PlugInNode plugInNode : scenario.plugIns)
		{
			/* plug-in configuration */
			String plugInId = plugInNode.id;
			String plugInName = plugInNode.name;

			/* Look for the plug-in plugInName in the plugIns map. If there is none, build it. */
			PlugIn plugIn;
			if ((plugIn = plugInsByNames.get(plugInName)) == null)
			{
				 /* build the PlugIn object plugIn from its name */
				plugInsByNames.put(plugInName, plugIn = new PlugIn(plugInName));
			}
			plugInsByIds.put(plugInId, plugIn);

			/* prefetch class loading */
			ClifClassLoader.fetchClassDependencies(plugIn.sessionObjectClass);
		}

		/* Compile behaviors */
		codeMap = new HashMap<String,List<Instruction>>();
		for (Iterator<BehaviorNode> i = scenario.behaviors.iterator(); i.hasNext();) {
			BehaviorNode behavior = i.next();
			ArrayList<Instruction> code = new ArrayList<Instruction>();

			codeMap.put(behavior.id, code);
			behavior.compile(plugInsByIds, code);
		}
	}

	/* Returns an array containing the ids of the behaviors. */
	protected String[] listBehaviors() {
		Object[] keys = codeMap.keySet().toArray();
		String[] res = new String[keys.length];
		for (int i = 0; i < keys.length; i++)
			res[i] = (String)keys[i];
		return res;
	}

	/*
	 * Returns a group corresponding to the behavior id. If id doesn't
	 * correspond to a behavior, or in case of error, returns null.
	 * 
	 * If several groups are defined for the behavior id in the scenario file,
	 * they are merged according to the following guidelines :
	 * - the load profiles are added ;
	 * - the interruptibility is true if and only if at least one group has an
	 *   interruptibility equal to true.
	 */
	protected Group getGroup(String id, Supervisor supervisor, Clock clock,
			Scheduler scheduler) {
		List<Instruction> code = codeMap.get(id);
		if (code == null)
			return null;
		HashSet<LoadProfile> loadProfiles = new HashSet<LoadProfile>();
		boolean interruptible = false;
		for (Iterator<GroupNode> i = scenario.groups.iterator(); i.hasNext();) {
			GroupNode groupNode = i.next();
			if (!groupNode.behavior.equals(id))
				continue;
			LoadProfile loadProfile = new LoadProfile();
			for (int j = 0, n = groupNode.ramps.size(); j < n; j++) {
				RampNode ramp = groupNode.ramps.get(j);
				RampNode.Point start = ramp.points.get("start"), end = ramp.points
						.get("end");
				int style;
				if (ramp.style.equals("line"))
					style = LoadProfile.Vertex.LINE;
				else if (ramp.style.equals("crenel_vh"))
					style = LoadProfile.Vertex.CRENEL_VH;
				else if (ramp.style.equals("crenel_hv"))
					style = LoadProfile.Vertex.CRENEL_HV;
				else
					style = LoadProfile.Vertex.NONE;
				try {
					loadProfile.add(new LoadProfile.Vertex(start.x * 1000,
							start.y, style));
					loadProfile.add(new LoadProfile.Vertex(end.x * 1000, end.y,
							LoadProfile.Vertex.NONE));
				}
				catch (Exception e) {
					return null;
				}
			}
			loadProfiles.add(loadProfile);
			interruptible |= groupNode.forceStop;
		}
		return new Group(supervisor, clock, scheduler, code, loadProfiles,
				interruptible);
	}

	/**
	 * Creates and returns the session objects specimen (one per plug-in import)
	 * @return the specimen session objects indexed by their plug-in import id
	 * @throws Exception if a session object instantiation threw an exception
	 */
	protected Map<String,SessionObjectAction> getSpecimenMap() throws Exception {
		specimenMap.clear();
		for (PlugInNode plugInNode : scenario.plugIns)
		{
			PlugIn plugIn = plugInsByIds.get(plugInNode.id);
			/* OK, now we create a specimen of session object for the plugin
			 * plugIn with the configuration described in plugInElement.
			 * Constructors of plug-in session objects may use a Map or
			 * a Hashtable argument. Map is tried first.
			 */
			Constructor<?> c;
			Map<String,String> params;
			try {
				try {
					c = plugIn.sessionObjectClass
						.getConstructor(new Class[] {Class
							.forName("java.util.Map")});
					params = plugInNode.params;
				}
				catch (NoSuchMethodException e) {
					c = plugIn.sessionObjectClass
						.getConstructor(new Class[] {Class
							.forName("java.util.Hashtable")});
					params = new Hashtable<String,String>(plugInNode.params);
				}
				ClassLoader contextCL = Thread.currentThread().getContextClassLoader();
				Thread.currentThread().setContextClassLoader(ClifClassLoader.getClassLoader());
				specimenMap.put(plugInNode.id, (SessionObjectAction)c.newInstance(new Object[] {params}));
				Thread.currentThread().setContextClassLoader(contextCL);
			}
			catch (Throwable t) {
				throw new Exception("Failed to load plug-in: " + plugIn.name, t);
			}
		}
		return specimenMap;
	}

	/* Prints the scenario. */
	@Override
	public String toString() {
		return scenario.toString();
	}

	/* Prints the pseudo-code of a behavior. */
	protected void printBehaviorCode(String id) {
		List<Instruction> code;
		if ((code = codeMap.get(id)) == null)
			return;
		System.out.println(id + ":");
		for (int j = 0; j < code.size(); j++)
			System.out.println(j + ":\t" + code.get(j));
		System.out.println();
	}
}
