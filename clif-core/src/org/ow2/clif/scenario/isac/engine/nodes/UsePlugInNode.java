/*
 * CLIF is a Load Injection Framework Copyright (C) 2005 France Telecom R&D
 * 
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option) any
 * later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 * CLIF
 * 
 * Contact: clif@ow2.org
 */
package org.ow2.clif.scenario.isac.engine.nodes;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Stack;

import org.jdom.Attribute;
import org.jdom.Element;
import org.ow2.clif.scenario.isac.engine.instructions.Instruction;
import org.ow2.clif.scenario.isac.engine.instructions.ParamsHolder;
import org.ow2.clif.scenario.isac.engine.instructions.PlugInParamPart;
import org.ow2.clif.scenario.isac.engine.instructions.TestInstruction;
import org.ow2.clif.scenario.isac.util.ParameterParser;

/**
 * @author Emmanuel Varoquaux
 */
/*
 * This is the parent class for SampleElement, TimerElement, ConditionElement.
 */
abstract class UsePlugInNode {
	public String	use;
	public String	name;
	public Map<String,String>		params	= new HashMap<String,String>();

	public UsePlugInNode(Element element) throws NodeException {
		ListIterator i;
		Attribute a;
		Element e;
		String name;

		for (i = element.getAttributes().listIterator(); i.hasNext();) {
			a = (Attribute)i.next();
			name = a.getName();
			if (name.equals("use")) {
				if (use != null)
					throw new DuplicatedAttributeException("use");
				use = a.getValue();
			}
			else if (name.equals("name")) {
				if (this.name != null)
					throw new DuplicatedAttributeException("name");
				this.name = a.getValue();
			}
			else
				throw new UnexpectedAttributeException(name);
		}
		if (use == null)
			throw new MissingAttributeException("use");
		if (this.name == null)
			throw new MissingAttributeException("name");
		
		i = element.getChildren().listIterator();

		if (i.hasNext()) {
			e = (Element)i.next();
			name = e.getName();
			if (!name.equals("params"))
				throw new BadElementException(name, "params");
			params = Util.analyseParams(e);
		}
		else
			params = new HashMap<String,String>();
		
		if (i.hasNext())
			throw new UnexpectedElementException(((Element)i.next()).getName());
	}

	protected PlugInParamPart[] split(String str) {
		List<PlugInParamPart> tmp;
		StringBuilder buf1;
		StringBuilder buf2;
		int state;
		PlugInParamPart part;
		char c;

		tmp = new LinkedList<PlugInParamPart>();
		buf1 = new StringBuilder();
		buf2 = new StringBuilder();
		state = 0;
		loop: for (int i = 0, len = str.length(); i < len; i++) {
			c = str.charAt(i);
			switch (state) {
			case 0: /* Literal string */
				switch (c) {
				case '$':
					if (buf1.length() > 0) {
						part = new PlugInParamPart();
						part.type = PlugInParamPart.STRING;
						part.str = buf1.toString();
						tmp.add(part);
						buf1.setLength(0);
					}
					state = 2;
					continue;
				default:
					buf1.append(c);
					continue;
				}
			// no more state == 1 (irrelevantly used for "escape sequence")
			case 2: /* Context call */
				switch (c) {
				case '#':
					part = new PlugInParamPart();
					part.type = PlugInParamPart.SESSION_ID;
					tmp.add(part);
					state = 0;
					continue;
				case '{':
					state = 3;
					continue;
				case '$' : /* $$ is the escape sequence for $ */
					buf1.append(c);
					state = 0;
					continue;
				default: /* Syntax error */
					break loop;
				}
			case 3: /* plugInId */
				switch (c) {
				case ':':
					state = 4;
					continue;
				case '{':
					break loop;
				case '}': /* no ':' means system property */
					part = new PlugInParamPart();
					part.type = PlugInParamPart.PROPERTY;
					part.plugInId = null;
					part.variable = ParameterParser.unescape(buf1.toString());
					tmp.add(part);
					buf1.setLength(0);
					state = 0;
					continue;
				default:
					buf1.append(c);
					continue;
				}
			case 4: /* Variable */
				switch (c) {
				case '}':
					part = new PlugInParamPart();
					part.type = PlugInParamPart.CONTEXT_CALL;
					part.plugInId = ParameterParser.unescape(buf1.toString());
					part.variable = ParameterParser.unescape(buf2.toString());
					tmp.add(part);
					buf1.setLength(0);
					buf2.setLength(0);
					state = 0;
					continue;
				case '{': /* Syntax error */
					break loop;
				default:
					buf2.append(c);
					continue;
				}
			}
		}
		if (state == 0 && buf1.length() > 0) {
			part = new PlugInParamPart();
			part.type = PlugInParamPart.STRING;
			part.str = buf1.toString();
			tmp.add(part);
		}

		return tmp.toArray(new PlugInParamPart[tmp.size()]);
	}

	protected ParamsHolder split(Map<String,String> params) {
		Map<String,PlugInParamPart[]> tmp = new HashMap<String,PlugInParamPart[]>();

		for (Iterator<Map.Entry<String,String>> i = params.entrySet().iterator(); i.hasNext();) {
			Map.Entry<String,String> entry = i.next();
			tmp.put(entry.getKey(), split(entry.getValue()));
		}
		return new ParamsHolder(tmp);
	}

	protected void checkConditions(ArrayList<Instruction> code, Stack<TestInstruction> conditions) {
		for (int i = 0, n = conditions.size(); i < n; i++)
			code.add(conditions.get(i));
	}

	protected static StringBuilder toString(Map<String,String> params, StringBuilder str)
	{
		str.append("{");
		for (Iterator<Map.Entry<String,String>> i = params.entrySet().iterator(); i.hasNext();)
		{
			Map.Entry<String,String> entry = i.next();
			str.append(entry.getKey() + "=\"" + entry.getValue() + "\"");
			if (i.hasNext())
			{
				str.append(", ");
			}
		}
		str.append("}");
		return str;
	}

	@Override
	public String toString()
	{
		StringBuilder res = new StringBuilder("use=\"" + use + "\", name=\"" + name + "\"" + ", params=");
		return toString(params, res).toString();
	}
}
