/*
 * CLIF is a Load Injection Framework Copyright (C) 2005 France Telecom R&D
 * 
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option) any
 * later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 * CLIF
 * 
 * Contact: clif@ow2.org
 */
package org.ow2.clif.scenario.isac.engine;

/**
 * A clock with its own time. All methods of this class are synchronized.
 *
 * @author Emmanuel Varoquaux
 */
class Clock {
	private boolean	stopped;
	private long	info;

	/**
	 * Initializes a new <code>Clock</code> object in the state <i>stopped</i>.
	 */
	protected Clock() {
		reset();
	}

	/**
	 * Puts the clock in the state <i>stopped</i> and sets its time to 0.
	 */
	protected synchronized void reset() {
		info = 0;
		stopped = true;
	}

	/**
	 * Starts or restarts the clock. Notifies all threads waiting on this clock.
	 * If the clock is already started, does nothing.
	 */
	protected synchronized void start() {
		if (!stopped)
			return;
		info = System.currentTimeMillis() - info;
		stopped = false;
		notifyAll();
	}

	/**
	 * Stops the clock. Doesn't reset its time. If the clock is already stopped,
	 * does nothing.
	 */
	protected synchronized void stop() {
		if (stopped)
			return;
		info = System.currentTimeMillis() - info;
		stopped = true;
	}

	/**
	 * Returns the current date for this clock.
	 */
	protected synchronized long getDate() {
		return stopped ? info : System.currentTimeMillis() - info;
	}

	/**
	 * Tells wether the clock is stopped or not.
	 */
	protected boolean isStopped() {
		return stopped;
	}
}
