/*
 * CLIF is a Load Injection Framework Copyright (C) 2005 France Telecom R&D
 * 
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option) any
 * later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 * CLIF
 * 
 * Contact: clif@ow2.org
 */
package org.ow2.clif.scenario.isac.engine.instructions;

/**
 * @author Emmanuel Varoquaux
 */
public class ControlInstruction extends Instruction {
	public String		plugInId;
	public int			actionNumber;
	public ParamsHolder	paramsHolder;

	public ControlInstruction(String plugInId, int actionNumber,
			ParamsHolder paramsHolder) {
		this.plugInId = plugInId;
		this.actionNumber = actionNumber;
		this.paramsHolder = paramsHolder;
	}

	@Override
	public int getType() {
		return CONTROL;
	}

	@Override
	public String toString() {
		return "CONTROL\t\"" + plugInId + "\", " + actionNumber + ", "
				+ paramsHolder.toString();
	}
}
