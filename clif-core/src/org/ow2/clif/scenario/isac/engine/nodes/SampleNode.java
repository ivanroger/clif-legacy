/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2005,2012 France Telecom R&D
 * 
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option) any
 * later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 * Contact: clif@ow2.org
 */
package org.ow2.clif.scenario.isac.engine.nodes;

import java.util.Map;
import java.util.ArrayList;
import java.util.Stack;
import org.jdom.Element;
import org.ow2.clif.scenario.isac.engine.PlugIn;
import org.ow2.clif.scenario.isac.engine.instructions.Instruction;
import org.ow2.clif.scenario.isac.engine.instructions.SampleInstruction;
import org.ow2.clif.scenario.isac.engine.instructions.TestInstruction;

/**
 * @author Emmanuel Varoquaux
 * @author Bruno Dillenseger
 */
public class SampleNode extends UsePlugInNode implements InstructionNode {
	
	public SampleNode(Element element) throws NodeException {
		super(element);
	}
	
	protected int getMethodNumber(Map<String,PlugIn> plugIns) throws Exception {
		if (!plugIns.containsKey(use))
			throw new Exception("No plug-in imported as \"" + use + "\" for sample \"" + name + "\".");
		PlugIn plugIn = (PlugIn)plugIns.get(use);
		if (!plugIn.sampleConversionMap.containsKey(name))
			throw new Exception("Sample \"" + name + "\" is not defined by plug-in \"" + use + "\".");
		return plugIn.sampleConversionMap.get(name);
	}

	@Override
	public StringBuilder indentedToString(int indent, StringBuilder str)
	{
		Util.stringIndent(indent, str);
		str.append("sample: " + super.toString() + "\n");
		return str;
	}

	@Override
	public void compile(Map<String,PlugIn> plugIns, ArrayList<Instruction> code, Stack<TestInstruction> conditions) throws Exception {
		checkConditions(code, conditions);
		code.add(new SampleInstruction(use, getMethodNumber(plugIns), split(params)));
	}
}
