/*
* CLIF is a Load Injection Framework
* Copyright (C) 2004 France Telecom R&D
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
* CLIF 
*
* Contact: clif@ow2.org
*/
package org.ow2.clif.scenario.isac.exception;

/**
 * This class implements runtime exception for the Isac module
 * 
 * @author JC Meillaud
 * @author A Peyrard
 */
public class IsacRuntimeException extends RuntimeException {
	/**
	 * 
	 */
	private static final long serialVersionUID = -3755096846421262659L;
	
	/**
	 * Constructor, build a new runtime exception with a prdefined message
	 * @param cause The cause of the exception
	 */
	public IsacRuntimeException(String cause) {
		// build a new runtime exception 
		super(cause) ;
	}
	
	/**
	 * Send an isac runtime exception
	 * @param cause The cause of the exception
	 * @param e The previous exception
	 */
	public IsacRuntimeException(String cause, Exception e) {
		// build a new runtime exception
		super(cause,e);
	}
}
