/*
* CLIF is a Load Injection Framework
* Copyright (C) 2003,2004,2005,2006,2010-2012 France Telecom
* Copyright (C) 2003 INRIA
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
* Contact: clif@ow2.org
*/

package org.ow2.clif.supervisor.lib;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Observable;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.Interface;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.BindingController;
import org.objectweb.fractal.util.Fractal;
import org.ow2.clif.console.lib.ClifDeployDefinition;
import org.ow2.clif.datacollector.api.DataCollectorAdmin;
import org.ow2.clif.server.api.BladeControl;
import org.ow2.clif.storage.api.AlarmEvent;
import org.ow2.clif.storage.api.CollectListener;
import org.ow2.clif.storage.api.StorageAdmin;
import org.ow2.clif.supervisor.api.BladeState;
import org.ow2.clif.supervisor.api.ClifException;
import org.ow2.clif.supervisor.api.SupervisorInfo;
import org.ow2.clif.supervisor.api.TestControl;


/**
 * Supervisor implementation, used to control a deployed test plan (i.e. a set of blades).
 * This class extends the Observable class in order to provide feedback information about the state
 * of the test plan's blades and the occurrence of alarms.
 * @see AlarmEvent
 * @see BladeObservation
 * @author Julien Buret
 * @author Nicolas Droze
 * @author Bruno Dillenseger
 * @author Joan Chaumont
 */

public class SupervisorImpl
	extends
		Observable
	implements
		TestControl,
		SupervisorInfo,
		BindingController
{
	/** blades' states indexed by their corresponding blade id */
	private Map<String,BladeState> bladesStates = new HashMap<String,BladeState>();

	/** blades' states indexed by their corresponding blade id */
	private Map<String,String> bladeIdByName = new HashMap<String,String>();

	/** blades' BladeControl interfaces, indexed by their blade id */
	private Map<String,BladeControl> bladesById = new HashMap<String,BladeControl>();

	/** test plan definition */
	private Map<String,ClifDeployDefinition> definitions;


	////////////////////////////////
	// fields for client bindings //
	////////////////////////////////


	/** contains BladeControl Fractal interfaces indexed by their interface names */
	private Map<String,BladeControl> bladesItf = new HashMap<String,BladeControl>();

	/** contains DataCollectorAdmin Fractal interfaces indexed by their names */
	private Map<String,DataCollectorAdmin> collectorsItf = new HashMap<String,DataCollectorAdmin>();

	/** Fractal interface for storage component administration */
	private StorageAdmin storageItf;

	/** Fractal interface for Console component information interface */
	private SupervisorInfo infoItf;

	/** keeps a cache of all bound client interfaces names (null value means invalidation) */
	private String[] interfaceNamesCache = null;
	
	/** The current test ID */
	protected Serializable currentTestId;

    /**
     * Gets the collection of states of some blades. 
     * @param selBladesId the blades identifiers, or null to
     * include all deployed blades
     * @return the blades states. When a requested blade identifier
     * is not known, its state is represented by null.
     */
	private Collection<BladeState> getBladesStates(String[] selBladesId)
	{
		Collection<BladeState> states;
		if (selBladesId == null)
		{
			states = bladesStates.values();
		}
		else
		{
			states = new ArrayList<BladeState>(selBladesId.length);
			for (String bladeId : selBladesId)
			{
				states.add(bladesStates.get(bladeId));
			}
		}
		return states;
	}


	/////////////////////////////////
	// interface BindingController //
	/////////////////////////////////


	public Object lookupFc(String clientItfName)
	{
		if (clientItfName.equals(StorageAdmin.STORAGE_ADMIN))
		{
			return storageItf;
		}
		else if (clientItfName.startsWith(DataCollectorAdmin.DATA_COLLECTOR_ADMIN))
		{
			return collectorsItf.get(clientItfName);
		}
		else if (clientItfName.startsWith(BladeControl.BLADE_CONTROL))
		{
			return bladesItf.get(clientItfName);
		}
		else if (clientItfName.equals(SupervisorInfo.SUPERVISOR_INFO))
		{
			return infoItf;
		}
		else
		{
			return null;
		}
	}


	public synchronized void bindFc(String clientItfName, Object serverItf)
	{
		if (clientItfName.equals(StorageAdmin.STORAGE_ADMIN))
		{
			storageItf = (StorageAdmin) serverItf;
			interfaceNamesCache = null;
		}
		else if (clientItfName.startsWith(DataCollectorAdmin.DATA_COLLECTOR_ADMIN))
		{
			collectorsItf.put(clientItfName, (DataCollectorAdmin)serverItf);
			interfaceNamesCache = null;
		}
		else if (clientItfName.startsWith(BladeControl.BLADE_CONTROL))
		{
			BladeControl bladeCtl = (BladeControl)serverItf;
			bladesItf.put(clientItfName, bladeCtl);
			try
			{
				Component bladeComp = ((Interface)bladeCtl).getFcItfOwner();
				String bladeId = Fractal.getNameController(bladeComp).getFcName();
				bladesById.put(bladeId, bladeCtl);
				bladeIdByName.put(clientItfName, bladeId);
				bladesStates.put(bladeId, BladeState.UNDEPLOYED);
			}
			catch (NoSuchInterfaceException ex)
			{
				throw new Error("Fractal configuration error: blades should have a NameController.", ex);
			}
			interfaceNamesCache = null;
		}
		else if (clientItfName.equals(SupervisorInfo.SUPERVISOR_INFO))
		{
			infoItf = (SupervisorInfo) serverItf;
			interfaceNamesCache = null;
		}
	}


	public synchronized void unbindFc(String clientItfName)
	{
		if (clientItfName.equals(StorageAdmin.STORAGE_ADMIN))
		{
			storageItf = null;
			interfaceNamesCache = null;
		}
		else if (clientItfName.startsWith(DataCollectorAdmin.DATA_COLLECTOR_ADMIN))
		{
			collectorsItf.remove(clientItfName);
			interfaceNamesCache = null;
		}
		else if (clientItfName.startsWith(BladeControl.BLADE_CONTROL))
		{
			bladesItf.remove(clientItfName);
			String bladeId = bladeIdByName.remove(clientItfName);
			bladesById.remove(bladeId);
			bladesStates.remove(bladeId);
			interfaceNamesCache = null;
		}
	}


	public synchronized String[] listFc()
	{
		if (interfaceNamesCache == null)
		{
			int i = 0;
			interfaceNamesCache = new String[
				(storageItf == null ? 0 : 1)
				+ (infoItf == null ? 0 : 1)
				+ bladesItf.size()
				+ collectorsItf.size()];
			if (storageItf != null)
			{
				interfaceNamesCache[i++] = StorageAdmin.STORAGE_ADMIN;
			}
			if (infoItf != null)
			{
				interfaceNamesCache[i++] = SupervisorInfo.SUPERVISOR_INFO;
			}
			i = fillInterfaceArray(i, interfaceNamesCache, bladesItf.keySet());
			i = fillInterfaceArray(i, interfaceNamesCache, collectorsItf.keySet());
		}
		return interfaceNamesCache;
	}


	/**
	 * utility method used by listFc() to build an array of interfaces names
	 * @param i starting index
	 * @param array the array to fill with names
	 * @param values names
	 * @return last index plus one (in other words, next starting index)
	 */
	private int fillInterfaceArray(int i, String[] array, Collection<String> nameSet)
	{
		for (String name : nameSet)
		{
			array[i++] = name;
		}
		return i;
	}


	///////////////////////////
	// interface TestControl //
	///////////////////////////


	/**
	 * Get the identifiers of all blades that are currently
	 * under control (i.e. deployed).
	 * @return an array containing the identifiers of all blades bound
	 * to this supervisor.
	 */
	public String[] getBladesIds()
	{
		return bladesById.keySet().toArray(new String[bladesById.size()]);
	}


	/**
	 * Retrieve the execution statistics of a blade.
	 * @param bladeId The blade identifier to get the statistics from
	 * @return An array containing execution statistics for the given blade
	 * @see #getStatLabels(String)
	 */
	public long[] getStats(String bladeId)
	{
		Interface blade = (Interface)bladesById.get(bladeId);
		if (blade != null)
		{
			Thread.currentThread().setContextClassLoader(this.getClass().getClassLoader());
			try
			{
				return ((DataCollectorAdmin)blade.getFcItfOwner().getFcInterface(
					DataCollectorAdmin.DATA_COLLECTOR_ADMIN)).getStat();
			}
			catch (NoSuchInterfaceException ex)
			{
				throw new Error("Blade " + bladeId + " does not have a DataCollectorAdmin interface.");
			}
		}
		else
		{
			return null;
		}
	}


	/**
	 * Gets labels describing statistics delivered by a blade
	 * @param bladeId the blade identifier
	 * @return An array of string labels describing the statistics delivered
	 * by the designated blade
	 * @see #getStats(String)
	 */
	public String[] getStatLabels(String bladeId)
	{
		Interface blade = (Interface)bladesById.get(bladeId);
		if (blade != null)
		{
			try
			{
				return ((DataCollectorAdmin)blade.getFcItfOwner().getFcInterface(
					DataCollectorAdmin.DATA_COLLECTOR_ADMIN)).getLabels();
			}
			catch (NoSuchInterfaceException ex)
			{
				throw new Error("Blade " + bladeId + " does not have a DataCollectorAdmin interface.");
			}
		}
		else
		{
			return null;
		}
	}


	/**
	 * Gets a Map of blade parameters and their associated values.
	 * @param bladeId the blade identifier
	 * @return parameters of the designated blade if found, null otherwise 
	 */
	public Map<String,Serializable> getCurrentParameters(String bladeId)
	{
		BladeControl bladeCtl = bladesById.get(bladeId);
		if (bladeCtl != null)
		{
			return bladeCtl.getCurrentParameters();
		}
		else
		{
			return null;
		}
	}


	/**
	 * Changes a blade parameter value.
	 * @param bladeId the blade identifier
	 * @param name the parameter name
	 * @param value the new parameter value
	 */
	public void changeParameter(String bladeId, String name, Serializable value) 
    	throws ClifException
    {
		BladeControl bladeCtl = bladesById.get(bladeId);
		if (bladeCtl != null)
		{
			bladeCtl.changeParameter(name, value);
        }
		else
		{
			throw new ClifException("Cannot change parameter value of unknown blade " + bladeId);
		}
    }


    /**
	 * Collects latest test data for selected blades
	 * @param selBladesId selected blades identifiers as a String[]
	 */
	public void collect(String[] selBladesId, CollectListener listener)
	{
	    storageItf.collect(selBladesId, listener);
	}


    /**
	 * Initializes a new test for every blade in the deployed test plan.
	 * {@link #setDefinitions(Map)} must be called before to test
	 * the currently deployed test plan definition.
	 * @param testId the new test's name
	 */
	public void init(Serializable testId)
		throws ClifException
	{
		currentTestId = testId;
        storageItf.newTest(testId, definitions);
        for (BladeControl bladeCtl : bladesItf.values())
        {
            bladeCtl.init(testId);
        }
	}


	/**
	 * Starts all blades of the currently deployed test plan.
	 */
	public void start()
	{
		for (BladeControl bladeCtl : bladesItf.values())
		{
			bladeCtl.start();
		}
	}


	/**
	 * Starts a selection of blades of currently deployed test plan.
	 * Does not complain if any blade identifier is not known.
	 * @param selBladesId identifiers of blades to be started,
	 * or null for starting all blades.
	 */
	public void start(String[] selBladesId)
	{
		if (selBladesId == null)
		{
			start();
		}
		else
		{
			for (String bladeId : selBladesId)
			{
				BladeControl bladeCtl = bladesById.get(bladeId);
				if (bladeId != null)
				{
					bladeCtl.start();
				}
			}
		}
	}


	/**
	 * Stops all blades of currently deployed test plan.
	 */
	public void stop()
	{
		for (BladeControl bladeCtl : bladesItf.values())
		{
			bladeCtl.stop();
		}
	}


	/**
	 * Stops a selection of blades from currently deployed test plan.
	 * Does not complain if any blade identifier is not known.
	 * @param selBladesId array of target blades identifiers,
	 * or null for designating all blades
	 */
	public void stop(String[] selBladesId)
	{
		if(selBladesId == null)
		{
			stop();
		}
		else
		{
			for (String bladeId : selBladesId)
			{
				BladeControl bladeCtl = bladesById.get(bladeId);
				if (bladeId != null)
				{
					bladeCtl.stop();
				}
			}
		}
	}


	/**
	 * Suspends all blades of currently deployed test plan.
	 */
	public void suspend()
	{
		for (BladeControl bladeCtl : bladesItf.values())
		{
			bladeCtl.suspend();
		}
	}


	/**
	 * Suspends a selection of blades from currently deployed test plan.
	 * Does not complain if any blade identifier is not known.
	 * @param selBladesId array of target blades identifiers,
	 * or null for designating all blades
	 */
	public void suspend(String[] selBladesId)
	{
		if(selBladesId == null)
		{
			suspend();
		}
		else
		{
			for (String bladeId : selBladesId)
			{
				BladeControl bladeCtl = bladesById.get(bladeId);
				if (bladeId != null)
				{
					bladeCtl.suspend();
				}
			}
		}
	}


	/**
	 * Resumes all blades of currently deployed test plan.
	 */
	public void resume()
	{
		for (BladeControl bladeCtl : bladesItf.values())
		{
			bladeCtl.resume();
		}
	}


	/**
	 * Resumes a selection of blades from currently deployed test plan.
	 * Does not complain if any blade identifier is not known.
	 * @param selBladesId array of target blades identifiers,
	 * or null for designating all blades
	 */
	public void resume(String[] selBladesId)
	{
		if(selBladesId == null)
		{
			resume();
		}
		else
		{
			for (String bladeId : selBladesId)
			{
				BladeControl bladeCtl = bladesById.get(bladeId);
				if (bladeId != null)
				{
					bladeCtl.resume();
				}
			}
		}
	}


	/**
	 * Waits for the end of activity of all blades of currently deployed test plan.
	 */
	public void join()
	{
		for (BladeControl bladeCtl : bladesItf.values())
		{
			bladeCtl.join();
		}
	}


	/**
	 * Waits for the end of execution of a selection of blades
	 * from currently deployed test plan.
	 * Does not complain if any blade identifier is not known.
	 * @param selBladesId array of target blades identifiers,
	 * or null for designating all blades
	 */
	public void join(String[] selBladesId)
	{
		if(selBladesId == null)
		{
			join();
		}
		else
		{
			for (String bladeId : selBladesId)
			{
				BladeControl bladeCtl = bladesById.get(bladeId);
				if (bladeId != null)
				{
					bladeCtl.join();
				}
			}
		}
	}


	//////////////////////////////
	// interface SupervisorInfo //
	//////////////////////////////


	/**
	 * Forwards the alarm event to observers
	 */
	public void alarm(String bladeId, AlarmEvent alarm)
	{
		setChanged();
		notifyObservers(new AlarmObservation(bladeId, alarm));
	}


    /**
     * Inform that the state of a blade has changed.
     * @param id the globally unique blade identifier
     * @param state The new state of the blade
     */
    public synchronized void setBladeState(String id, BladeState state)
    {
    	bladesStates.put(id, state);
    	setChanged();
    	notifyObservers(new BladeObservation(id, state));
        notifyAll();
    }


    /**
     * Waits until blades are all in a stationary state.
     * @param selBladesId array of identifiers of target blades,
     * or null for designating all blades of currently deployed test plan.
     */
    public synchronized void waitStationaryState(String[] selBladesId)
    	throws InterruptedException
    {
        while (! BladeState.isStationaryState(getBladesStates(selBladesId)))
        {
        	wait();
        } 
    }


    /**
     * Waits until blades are all in a given state.
     * @param selBladesId array of identifiers of target blades,
     * or null for designating all blades of currently deployed test plan.
     * @param state the requested blade state
     * @return true if current state is actually the requested state,
     * false otherwise.
     */
	public synchronized boolean waitForState(String[] selBladesId, BladeState state)
		throws InterruptedException
	{
    	BladeState currentState = BladeState.getGlobalState(getBladesStates(selBladesId));
    	while (
            ! currentState.equals(BladeState.ABORTED)
            && ! currentState.equals(BladeState.COMPLETED)
            && ! currentState.equals(BladeState.STOPPED)
            && ! currentState.equals(state))
        {
        	wait();
        	currentState = BladeState.getGlobalState(getBladesStates(selBladesId));
        }
        return currentState.equals(state);
    }


    /**
	 * Get global state for some blades.
	 * @param selBladesId identifiers of target blades,
	 * or null for designating all blades
	 * @return the global state.
	 */
	public BladeState getGlobalState(String[] selBladesId) 
	{
		return BladeState.getGlobalState(getBladesStates(selBladesId));
	}


	/**
	 * Wait until blades are stopped, completed or aborted.
	 * @param selBladesId identifiers of target blades,
	 * or null for designating all blades
	 */
    public synchronized void waitEndOfRun(String[] selBladesId)
    	throws InterruptedException
    {
      	while (BladeState.isRunning(getBladesStates(selBladesId)))
       	{
       		wait();
       	}
    }


    /**
     * Sets the test plan definition. Must be set before
     * calling {@link #init(Serializable)}.
     */
    public void setDefinitions(Map<String,ClifDeployDefinition> definitions)
    {
        this.definitions = definitions;
    }


    /**
     * Gets current test plan definition
     * @return current test plan definition
     */
    public Map<String,ClifDeployDefinition> getDefinitions()
    {
        return definitions;
    }
    
	/**
	 * Retrieves current test identifier.
	 * @return A string composed of the test name,
	 * and the date and time of its initialization
	 */
	public String getCurrentTestId() {
	    return currentTestId.toString();
    }
}
