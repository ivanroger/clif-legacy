/*
* CLIF is a Load Injection Framework
* Copyright (C) 2005, 2011 France Telecom R&D
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
* Contact: clif@ow2.org
*/

package org.ow2.clif.supervisor.lib;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.io.Serializable;
import org.ow2.clif.storage.api.AlarmEvent;

/**
 * Class for alarm objects dedicated to the Observer interface,
 * encapsulating an AlarmEvent instance with the originating blade id.
 * 
 * @author Bruno Dillenseger
 */
public class AlarmObservation implements Serializable, Comparable<AlarmObservation>
{
	private static final long serialVersionUID = 8742113826239693732L;
	String bladeId;
    AlarmEvent alarm;
    
    public static final int COMPARE_TIME = 0;
    public static final int COMPARE_ID = 1;
    public static final int COMPARE_SEVERITY = 2;
    public static final int COMPARE_MESSAGE = 3;
    
    private int comparator;

    public AlarmObservation(String bladeId, AlarmEvent alarm)
    {
        this.bladeId = bladeId;
        this.alarm = alarm;
        comparator = COMPARE_ID;
    }


    public String getBladeId()
    {
        return bladeId;
    }


    public AlarmEvent getAlarm()
    {
        return alarm;
    }

    public long getDate() {
        return alarm.getDate();
    }
    
    public int getSeverity() {
        return alarm.severity;
    }
    
    public String getMessage() {
        String message = "";
        if (alarm.argument instanceof Throwable)
        {
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            ((Throwable)alarm.argument).printStackTrace(new PrintStream(out));
            message = out.toString();
        }
        else
        {
            message = alarm.argument.toString();
        }
        return message;
    }
    
    public void compareBy (int comparator) {
        this.comparator = comparator;
    }

    public int compareTo(AlarmObservation o) {
        switch(comparator) {
        case COMPARE_ID :
            return (o.getBladeId().compareTo(bladeId) < 0)?1:-1;
        case COMPARE_TIME :
            return (o.getDate() <= alarm.getDate())?1:-1;
        case COMPARE_SEVERITY :
            return (o.getSeverity() <= alarm.severity)?1:-1;
        case COMPARE_MESSAGE :
            return (o.getMessage().compareTo(getMessage()) < 0)?1:-1;
        }
        return 0;
    }

    public String toString()
    {
    	return "alarm from " + bladeId + ": " + alarm;
    }
}
