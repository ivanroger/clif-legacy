/*
* CLIF is a Load Injection Framework
* Copyright (C) 2003, 2008, 2010, 2011 France Telecom R&D
* Copyright (C) 2003 INRIA
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
* Contact: clif@ow2.org
*/

package org.ow2.clif.supervisor.api;

import java.util.Map;
import org.ow2.clif.console.lib.ClifDeployDefinition;
import org.ow2.clif.storage.api.AlarmEvent;


/**
 * Interface used to send blade state information from a CLIF server to the Supervisor
 *
 * @author Julien Buret
 * @author Nicolas Droze
 * @author Bruno Dillenseger
 * @author Joan Chaumont
 */
public interface SupervisorInfo
{
	static public final String SUPERVISOR_INFO =  "Supervisor information";

	/**
	 * Informs that the state of a host has changed.
	 * @param id the scenario globally unique identifier object
	 * @param state The new state of the scenario
	 */
	public void setBladeState(String id, BladeState state);
	
	public void setDefinitions(Map<String,ClifDeployDefinition> definitions);
	public Map<String,ClifDeployDefinition> getDefinitions();

	/**
	 * Informs that an alarm occurred
	 * @param id blade identifier of the event source
	 * @param alarm alarm event
	 */
	public void alarm(String id, AlarmEvent alarm);

	/**
	 * Get global state for these blades.
	 * @param selBladesId blades of interest's identifiers, or null for every blade
	 * @return the global state.
	 */
	public BladeState getGlobalState(String[] selBladesId);

	/**
	 * Test if these blades are stationary or not.
	 * @param selBladesId blades of interest's identifiers, or null for every blade
	 */
	public void waitStationaryState(String[] selBladesId) throws InterruptedException;

	/**
	 * Test if these blades are Running or not.
	 * @param selBladesId blades of interest's identifiers, or null for every blade
	 */
    public void waitEndOfRun(String[] selBladesId) throws InterruptedException;

    /**
     * Wait until a target state is reached.
     * @param selBladesId blades of interest's identifiers, or null for every blade
     * @param state target state to wait for
     */
    public boolean waitForState(String[] selBladesId, BladeState state) throws InterruptedException;
}
