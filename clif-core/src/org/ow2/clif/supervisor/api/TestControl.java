/*
* CLIF is a Load Injection Framework
* Copyright (C) 2004, 2012 France Telecom R&D
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
* Contact: clif@ow2.org
*/

package org.ow2.clif.supervisor.api;

import java.io.Serializable;
import java.util.Map;
import java.util.Observer;
import org.ow2.clif.server.api.ActivityControl;
import org.ow2.clif.storage.api.CollectListener;

/**
 * Interface for controlling a (distributed) test plan : 
 * deployment, initialization, start, stop...
 * @author Bruno Dillenseger
 * @author Joan Chaumont
 */
public interface TestControl extends ActivityControl
{
	static public final String TEST_CONTROL = "Test control";

	/**
	 * Get the identifiers of all blades that are currently
	 * under control (i.e. deployed).
	 * @return an array of blade identifiers
	 */
	public String[] getBladesIds();

	/**
	 * Retrieve the ActionStat of a specific host.
	 * @param bladeId The blade identifier to get the statistical data from
	 * @return An array containing some informations about the test for this host
	 */
	public long[] getStats(String bladeId);

	/**
	 * Retrieve the ActionStat of a specific host.
	 * @param bladeId The blade identifier to get the statistics labels from
	 * @return An array containing some informations about the test for this host
	 */
	public String[] getStatLabels(String bladeId);
    
    /**
     * Return current changeable parameters
     * @param bladeId The identifier of the target blade
     */
    public Map<String,Serializable> getCurrentParameters(String bladeId);

    /**
	 * Start selected blades
	 * @param selBladesId String[] ids of selected blades 
	 * (selBladesId = null means all blades)
	 */
	public void start(String[] selBladesId);
	
	/**
	 * Stop selected blades
	 * @param selBladesId String[] ids of selected blades
	 * (selBladesId = null means all blades)
	 */
	public void stop(String[] selBladesId);
	
	/**
	 * Suspend selected blades
	 * @param selBladesId String[] ids of selected blades
	 * (selBladesId = null means all blades)
	 */
	public void suspend(String[] selBladesId);
	
	/**
	 * Resume selected blades
	 * @param selBladesId String[] ids of selected blades
	 */
	public void resume(String[] selBladesId);
	
	/**
	 * Wait for selected blades to end
	 * @param selBladesId String[] ids of selected blades
	 * (selBladesId = null means all blades)
	 */
	public void join(String[] selBladesId);
	
	/**
	 * Collect test results from all or selected blades
	 * @param selBladesId String[] of selected blades ids
	 * or null to collect from all blades.
	 * @param listener the listener receives progress
	 * information and it is given the opportunity to
	 * cancel the collection. Ignored if null.
	 */
	public void collect(String[] selBladesId, CollectListener listener);

    public void addObserver(Observer obs);

    public void deleteObservers();

    public void changeParameter(String bladeId, String name, Serializable text)
    throws ClifException;

    /**
	 * Retrieves current test identifier.
	 * @return A string composed of the test name,
	 * and the date and time of its initialization
	 */
	public String getCurrentTestId();
}
