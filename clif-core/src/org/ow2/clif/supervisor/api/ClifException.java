/*
* CLIF is a Load Injection Framework
* Copyright (C) 2004, 2011 France Telecom R&D
* Copyright (C) 2003 INRIA
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
* Contact: clif@ow2.org
*/

package org.ow2.clif.supervisor.api;

/**
 * Class for CLIF exceptions
 *
 * @author Julien Buret
 * @author Bruno Dillenseger
 */
public class ClifException extends Exception
{
	private static final long serialVersionUID = -1336406590769691916L;

	/**
	 * @param msg exception message
	 * @param t embedded exception
	 */
	public ClifException(String msg, Throwable t)
	{
		super(msg, t);
	}

	/**
	 * @param t embedded exception
	 */
	public ClifException(Throwable t)
	{
		super(t);
	}

	/**
	 * @param msg exception message
	 */
	public ClifException(String msg)
	{
		super(msg);
	}
}
