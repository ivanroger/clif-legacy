/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2009 France Telecom
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * CLIF
 *
 * Contact: clif@ow2.org
 */

package org.ow2.clif.probe.rtp;

import org.ow2.clif.probe.rtp.RTCPPacket;

/**
 * This class is used to create a RTP packet.
 * 
 * @author Remi Druilhe
 */
public class RTPPacket 
{
	private Integer version = 2;	// Version (-) : 2 bits
	private Integer padding = 0;	// Padding (-) : 1 bit
	private Integer extension = 0;	// Extension (-) : 1 bit
	private Integer csrcCount = 1;	// CSRC Count (-) : 4 bits
	private Integer marker = 0;		// Marker (-) : 1 bit
	private Integer payloadType;	// Payload Type (*) : 7 bits
	private Integer sequenceNumber;	// Sequence Number (-) : 16 bits (should be randomly generate for the first packet)
	private Long timestamp;			// Timestamp (-) : 32 bits (should be randomly generate for the first packet)
	private Long ssrc;				// SSRC (-) : 32 bits (should be randomly generate for the session)
	private byte[] payload;			// Payload (*) : depends on the type of payload
	
	// Sampling * 0.001 (to convert in ms)
	// The payload type must be include between 0 and 18 (see RFC 3551 p28)
	private static Double[] samplingTable = {8.0, 1.0, 1.0, 8.0, 8.0, 8.0, 16.0, 8.0, 8.0, 8.0, 
			44.1, 44.1, 8.0, 8.0, 90.0, 80., 11.025, 22.05, 8.0};
	
	/**
	 * @param payloadType the payload type of the RTP packet.
	 * @param sequenceNumber the sequence number of the RTP packet. 
	 * @param timestamp the timestamp of the RTP packet.
	 * @param ssrc the SSRC of the RTP packet.
	 * @param payload the payload of the RTP packet.
	 */
	public RTPPacket(Integer payloadType, Integer sequenceNumber, Long timestamp, Long ssrc, byte[] payload)
	{
		this.payloadType = payloadType;
		this.sequenceNumber = sequenceNumber;
		this.timestamp = timestamp;
		this.ssrc = ssrc;
		this.payload = payload;
	}
	
	/**
	 * Create a RTP packet in bytes.
	 * 
	 * @return the packet formatted in bytes.
	 */
	public byte[] createRtpPacket()
	{
		byte[] tempPacket = new byte[payload.length + 12];
		byte[] headerPacket = createHeader();
		
		for(int i=0; i<12; i++)
		{
			tempPacket[i] = headerPacket[i];
		}
		
		for(int i=0; i<payload.length; i++)
		{
			tempPacket[i+12] = payload[i];
		}
		
		return tempPacket;
	}
	
	/**
	 * Convert every information of the header in bytes.
	 * 
	 * @return the header formatted in bytes.
	 */
	public byte[] createHeader() 
	{	
		Integer header = new Integer(0);
		byte[] rtpHeader = new byte[12];
		
		header = (version << 6) & 0xC0;
		header |= (padding << 5) & 0x20;
		header |= (extension << 4) & 0x10;
		header |= csrcCount & 0x0F;
		rtpHeader[0] = header.byteValue();

		header = (marker << 7) & 0x80;
		header |= payloadType & 0x7F;
		rtpHeader[1] = header.byteValue();
		
		header = sequenceNumber >> 8;
		rtpHeader[2] = header.byteValue();
		header = sequenceNumber & 0xFF;
		rtpHeader[3] = header.byteValue();
		
		rtpHeader[4] = new Long(timestamp >> 24).byteValue();
		rtpHeader[5] = new Long((timestamp >> 16) & 0xFF).byteValue();
		rtpHeader[6] = new Long((timestamp >> 8) & 0xFF).byteValue();
		rtpHeader[7] = new Long(timestamp & 0xFF).byteValue();
		
		rtpHeader[8] = new Long(ssrc >> 24).byteValue();
		rtpHeader[9] = new Long((ssrc >> 16) & 0xFF).byteValue();
		rtpHeader[10] = new Long((ssrc >> 8) & 0xFF).byteValue();
		rtpHeader[11] = new Long(ssrc & 0xFF).byteValue();

		return rtpHeader;
	}
	
	/**
	 * Get the payload type from the RTP packet
	 * 
	 * @param data : the packet
	 * @return the PayloadType of the packet
	 */
	public static Integer getPayloadType(byte[] data)
	{
		return new Integer(RTCPPacket.decodeTwoComplement(new Byte(data[1]).intValue()) & 0x7F);
	}
	
	/**
	 * Get the sequence number from the RTP packet
	 * 
	 * @param data : the packet
	 * @return the Sequence Number of the packet
	 */
	public static Integer getSequenceNumber(byte[] data)
	{
		return new Integer((RTCPPacket.decodeTwoComplement(new Byte(data[2]).intValue()) << 8) 
				+ RTCPPacket.decodeTwoComplement(new Byte(data[3]).intValue()));
	}
	
	/**
	 * Get the timestamp from the RTP packet
	 * 
	 * @param data : the packet
	 * @return the Timestamp of the packet
	 */
	public static Long getTimestamp(byte[] data)
	{
		return new Long((RTCPPacket.decodeTwoComplement(new Byte(data[4]).longValue()) << 24)
				+ (RTCPPacket.decodeTwoComplement(new Byte(data[5]).longValue()) << 16)
				+ (RTCPPacket.decodeTwoComplement(new Byte(data[6]).longValue()) << 8)
				+ RTCPPacket.decodeTwoComplement(new Byte(data[7]).longValue()));
	}
	
	/**
	 * Get the ssrc from the RTP packet
	 * 
	 * @param data : the packet
	 * @return the SSRC of the packet
	 */
	public static Long getSsrc(byte[] data)
	{
		return new Long((RTCPPacket.decodeTwoComplement(new Byte(data[8]).longValue()) << 24)
				+ (RTCPPacket.decodeTwoComplement(new Byte(data[9]).longValue()) << 16)
				+ (RTCPPacket.decodeTwoComplement(new Byte(data[10]).longValue()) << 8) 
				+ RTCPPacket.decodeTwoComplement(new Byte(data[11]).longValue()));
	}
	
	/**
	 * Returns the frequency associated to this payload type
	 * 
	 * @param payloadType : the payload type
	 * @return the frequency
	 */
	public static Double getSampling(Integer payloadType)
	{
		return samplingTable[payloadType];
	}
	
	// GET Methods
	public Integer getVersion()
	{
		return version;
	}
	
	public Integer getPadding()
	{
		return padding;
	}
	
	public Integer getExtension()
	{
		return extension;
	}
	
	public Integer getCsrcCount()
	{
		return csrcCount;
	}
	
	public Integer getMarker()
	{
		return marker;
	}
	
	public Integer getPayloadType()
	{
		return payloadType;
	}
	
	public Integer getSequenceNumber()
	{
		return sequenceNumber;
	}
	
	public Long getTimestamp()
	{
		return timestamp;
	}
	
	public Long getSsrc()
	{
		return ssrc;
	}
	
	public byte[] getPayload()
	{
		return payload;
	}
	
	// SET Methods
	public void setVersion(Integer version)
	{
		this.version = version;
	}
	
	public void setPadding(Integer padding)
	{
		this.padding = padding;
	}
	
	public void setExtension(Integer extension)
	{
		this.extension = extension;
	}
	
	public void setCsrcCount(Integer csrcCount)
	{
		this.csrcCount= csrcCount;
	}
	
	public void setMarker(Integer marker)
	{
		this.marker = marker;
	}
	
	public void setPayloadType(Integer payloadType)
	{
		this.payloadType = payloadType;
	}
	
	public void setSequenceNumber(Integer sequenceNumber)
	{
		this.sequenceNumber= sequenceNumber;
	}
	
	public void setTimestamp(Long timestamp)
	{
		this.timestamp= timestamp;
	}
	
	public void setSSRC(Long ssrc)
	{
		this.ssrc = ssrc;
	}
	
	public void setPayload(byte[] tempPayload)
	{
		this.payload = tempPayload;
	}
}