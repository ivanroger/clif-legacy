/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2009 France Telecom
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * CLIF
 *
 * Contact: clif@ow2.org
 */

package org.ow2.clif.probe.rtp;

import java.util.LinkedList;

/**
 * Create a RTCP packet.
 * 
 * Packet type available for CLIF : 200, 201, 202, 203, 204.
 * 
 * @author Rémi Druilhe
 */
public abstract class RTCPPacket 
{
	// Packet type available for CLIF. 
	private static Integer[] packetTypeAvailable = {200, 201, 202, 203, 204};
	
	/**
	 * Create a RTCP packet.
	 * 
	 * @return the packet in bytes.
	 */
	public abstract byte[] createPacket();
	
	/**
	 * Returns the packet type of the RTCP packet.
	 * 
	 * @return the packet type.
	 */
	public abstract Integer getPacketType();
	
	/**
	 * Create a RTCP packet with each reports created previously.
	 * 
	 * @param reports : the report to add to the packet.
	 * @return the RTCP packet with each reports.
	 */
	public byte[] createRtcpPacket(LinkedList<byte[]> reports) 
	{
		Integer length = 0;
		byte[] rtcpPacket;
		
		for(int i=0; i<reports.size(); i++)
			length = length + reports.get(i).length;
		
		rtcpPacket = new byte[length];
		
		Integer offset = 0;
		
		while(!reports.isEmpty())
		{
			for(int i=0; i<reports.size(); i++)
			{
				rtcpPacket[i + offset] = reports.getFirst()[i];
			}
			
			offset = offset + reports.removeFirst().length;
		}
		
		return rtcpPacket;
	}
	
	/**
	 * Method to separate RTCP report.
	 * 
	 * @param data : the data to decode.
	 * @param time : the arrival time of the packet.
	 * @return a list of RTCP reports.
	 */
	public static LinkedList<RTCPInformation> getRtcpPackets(byte[] data, Long time)
	{
		LinkedList<RTCPInformation> rtcpPackets = new LinkedList<RTCPInformation>();
		
		Integer offset = 0;
		boolean available = true;
		
		while(available)
		{
			Integer packetType = decodePacketType(data, offset);
			
			if(!packetTypeAvailable(packetType))
				break;
			
			Integer count = decodeCount(data, offset);
			Integer length = decodeLength(data, offset);
			
			if(length.compareTo(data.length) >= 0)
				break;
			
			offset = offset + length;
			
			rtcpPackets.add(new RTCPInformation(packetType, count, length, time));
		}
		
		return rtcpPackets;
	}
	
	/**
	 * Returns the version.
	 * 
	 * @param data : the raw RTCP packet.
	 * @param offset : the offset to start to decode.
	 * @return the version.
	 */
	public static Integer decodeVersion(byte[] data, Integer offset)
	{
		return new Integer(decodeTwoComplement(new Byte(data[offset + 0]).intValue() & 0xC0));
	}
	
	/**
	 * Returns the padding.
	 * 
	 * @param data : the raw RTCP packet.
	 * @param offset : the offset to start to decode.
	 * @return the padding.
	 */
	public static Integer decodePadding(byte[] data, Integer offset)
	{
		return new Integer(decodeTwoComplement(new Byte(data[offset + 0]).intValue() & 0x20));
	}
	
	/**
	 * Returns the count header (RC, SC or Subtype).
	 * 
	 * @param data : the raw RTCP packet.
	 * @param offset : the offset to start to decode.
	 * @return the count.
	 */
	public static Integer decodeCount(byte[] data, Integer offset)
	{
		return new Integer(decodeTwoComplement(new Byte(data[offset + 0]).intValue() & 0x1F));
	}
	
	/**
	 * Returns the packet type header.
	 * 
	 * @param data : the raw RTCP packet.
	 * @param offset : the offset to start to decode.
	 * @return the packet type.
	 */
	public static Integer decodePacketType(byte[] data, Integer offset)
	{
		return new Integer(decodeTwoComplement(new Byte(data[offset + 1]).intValue()));
	}
	
	/**
	 * Returns the length header.
	 * 
	 * @param data : the raw RTCP packet.
	 * @param offset : the offset to start to decode.
	 * @return the length.
	 */
	public static Integer decodeLength(byte[] data, Integer offset)
	{
		return new Integer(((decodeTwoComplement(new Byte(data[offset + 2]).intValue()) << 8) 
				+ decodeTwoComplement(new Byte(data[offset + 3]).intValue()) + 1) << 2);
	}
	
	/**
	 * Decode the value (RTP use two's complement) 
	 * 
	 * @param value : the value to convert (include between -128 and 127)
	 * @return a value include between 0 and 256
	 */
	public static Long decodeTwoComplement(Long value)
	{
		if(value.compareTo(0L) < 0)
			value = 256 + value;
		
		return value;
	}
	
	/**
	 * Decode the value (RTP use two's complement) 
	 * 
	 * @param value : the value to convert (include between -128 and 127)
	 * @return a value include between 0 and 256
	 */
	public static Integer decodeTwoComplement(Integer value)
	{
		if(value.compareTo(0) < 0)
			value = 256 + value;
		
		return value;
	}
	
	/**
	 * Tells if the packet type is supported by CLIF.
	 * 
	 * @param packetType : the packet type to test.
	 * @return true if packet type is supported by CLIF.
	 */
	public static boolean packetTypeAvailable(Integer packetType)
	{
		boolean available = false;
		
		for(int i=0; i<5; i++)
			if(packetType.compareTo(packetTypeAvailable[i]) == 0)
				available = true;
		
		return available;
	}
}
