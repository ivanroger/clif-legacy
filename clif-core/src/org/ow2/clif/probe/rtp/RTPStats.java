/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2009 France Telecom
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * CLIF
 *
 * Contact: clif@ow2.org
 */

package org.ow2.clif.probe.rtp;

import java.util.ArrayList;
import java.util.LinkedList;

import org.ow2.clif.scenario.isac.exception.IsacRuntimeException;

/**
 * Thread to return statistics about RTP.
 * 
 * To calculate jitter, please refer to RFC 3550.
 * 
 * @author Rémi Druilhe
 */
public class RTPStats extends Thread
{
	private static RTPStats instance;
	
	private RTPListener rtpListener;
	private boolean endThread = false;
	private Boolean locked = false;
	private Long startTime = System.currentTimeMillis();
	private Long stopTime = System.currentTimeMillis();;
	
	private ArrayList<ParticipantStats> partStatsTable = new ArrayList<ParticipantStats>();
	private ArrayList<Integer> rtcpPortList = new ArrayList<Integer>();
	private ArrayList<ParticipantStats> byeList = new ArrayList<ParticipantStats>();
	
	// For time jitter
	private Long minTimeJitter = 0L;
	private Long maxTimeJitter = 0L;
	
	// Jump and inversion for every sessions joined
	private Long jump = 0L;
	private Long inversion = 0L;
	
	/**
	 * Constructor.
	 */
	private RTPStats()
	{
		rtpListener = RTPListener.getInstance();
	}
	
	/**
	 * Create or get the single instance of the thread.
	 * 
	 * @return the single instance of the thread
	 */
	public static synchronized RTPStats getInstance()
	{
		if(instance == null)
			instance = new RTPStats();

		return instance;
	}
	
	/**
	 * Open sockets using RTPListener.java
	 * 
	 * @param listeningPorts : a list of ports to open.
	 */
	public void openStats(ArrayList<Integer> listeningPorts)
	{
		rtpListener.openSocket(null, listeningPorts);
		rtpListener.startListener();
	}
	
	/**
	 * Start the thread of the singleton if it hasn't been started yet.
	 */
	public synchronized void startStats()
	{
		if(!instance.isAlive())
			instance.start();
	}
	
	/**
	 * @see java.lang.Thread#run()
	 */
	@Override
	public void run()
	{
		ArrayList<Long> rtpSessions = new ArrayList<Long>(100);
		LinkedList<RTPInformation> table = new LinkedList<RTPInformation>();
		
		int index;
		Integer sequenceNumber;
		Long ssrc;
		Long timestamp;
		Long time;
		Integer diffJpIn;

		while(!endThread)
		{
			table = rtpListener.getPackets(20);
			
			while(!table.isEmpty())
			{
				RTPInformation rtpListPack = table.removeFirst();

				if(!rtcpPortList.contains(new Integer(rtpListPack.getPort())))
				{
					ssrc = RTPPacket.getSsrc(rtpListPack.getData());
					timestamp = RTPPacket.getTimestamp(rtpListPack.getData());
					sequenceNumber = RTPPacket.getSequenceNumber(rtpListPack.getData());
					time = rtpListPack.getTime();
					
					if(!rtpSessions.contains(ssrc))
					{
						rtpSessions.add(ssrc);
						
						index = rtpSessions.indexOf(ssrc);
						
						ParticipantStats partInfo = new ParticipantStats(ssrc, sequenceNumber, rtpListPack.getPort());
						
						partInfo.setPreviousTime(time);
						partInfo.setPreviousTimestamp(timestamp);
						partInfo.setPreviousSeqNum(sequenceNumber);
						partInfo.incPacketSum();
						partInfo.setSampling(new Double(RTPPacket.getSampling(RTPPacket.getPayloadType(rtpListPack.getData()).intValue())));
	
						synchronized(partStatsTable)
						{
							partStatsTable.add(index, partInfo);
							
							partStatsTable.notify();
						}
					}
					else
					{
						index = rtpSessions.indexOf(ssrc);
						
						ParticipantStats partInfo = null;
						
						synchronized(partStatsTable)
						{
							partInfo = partStatsTable.get(index);
							
							partStatsTable.notify();
						}

						diffJpIn = sequenceNumber - partInfo.getSeqNumMax();
						
						partInfo.calculateTimeJitter(time, timestamp);
	
						// For next packet
						partInfo.calculateSeqNumMax(sequenceNumber);
						partInfo.setPreviousTime(time);
						partInfo.setPreviousTimestamp(timestamp);
						partInfo.setPreviousSeqNum(sequenceNumber);
						partInfo.incPacketSum();
	
						synchronized(partStatsTable)
						{
							partStatsTable.set(index, partInfo);
							
							partStatsTable.notify();
						}
						
						synchronized(locked)
						{
							locked = true;
							
							// Jump and inversion
							if(diffJpIn.compareTo(0) < 0)
								inversion++;
							else if(diffJpIn.compareTo(0) > 1)
								jump++;
							
							minTimeJitter = Math.min(minTimeJitter, partInfo.getTimeJitter());
							maxTimeJitter = Math.max(maxTimeJitter, partInfo.getTimeJitter());
							
							locked = false;
							locked.notify();
						}
					}
				}
				else
				{
					Integer offset = 0;
					LinkedList<RTCPInformation> rtcpInfoList = RTCPPacket.getRtcpPackets(rtpListPack.getData(), rtpListPack.getTime());
					
					while(!rtcpInfoList.isEmpty())
					{
						RTCPInformation rtcpInfo = rtcpInfoList.removeFirst();

						if(rtcpInfo.getPacketType().compareTo(200) == 0)
						{
							index = rtpSessions.indexOf(ReportPacket.decodeSsrc(rtpListPack.getData(), offset));
							
							ParticipantStats partInfo = null;
							
							synchronized(partStatsTable)
							{
								partInfo = partStatsTable.get(index);
								
								partStatsTable.notify();
							}
							
							Long MswNtpTimestamp = ReportPacket.decodeSrMswNtpTimestamp(rtpListPack.getData(), offset);
							Long LswNtpTimestamp = ReportPacket.decodeSrLswNtpTimestamp(rtpListPack.getData(), offset);

							partInfo.rtpSender();
							partInfo.setLsr(partInfo.calculateLsr(MswNtpTimestamp, LswNtpTimestamp));
							partInfo.setDlsr(rtcpInfo.getTime());
							partInfo.setFirstReport();
							
							synchronized(partStatsTable)
							{
								partStatsTable.set(index, partInfo);
								
								partStatsTable.notify();
							}
						}
						
						if(rtcpInfo.getPacketType().compareTo(201) == 0)
						{
							index = rtpSessions.indexOf(ReportPacket.decodeSsrc(rtpListPack.getData(), offset));
							
							ParticipantStats partInfo = null;
							
							synchronized(partStatsTable)
							{
								partInfo = partStatsTable.get(index);
								
								partStatsTable.notify();
							}

							partInfo.rtpReceiver();
							partInfo.setFirstReport();
							
							synchronized(partStatsTable)
							{
								partStatsTable.set(index, partInfo);
								
								partStatsTable.notify();
							}
						}
						
						if(rtcpInfo.getPacketType().compareTo(203) == 0)
						{
							ArrayList<Long> ssrcList = ByePacket.decodeSsrc(rtpListPack.getData(), offset);
							
							for(int i=0; i<ssrcList.size(); i++)
							{
								index = rtpSessions.indexOf(ssrcList.get(i));
									
								rtpSessions.remove(index);
								
								synchronized(partStatsTable)
								{
									byeList.add(partStatsTable.remove(index));
									
									partStatsTable.notify();
								}
							}
						}
						
						offset = offset + rtcpInfo.getLength();
					}
				}
			}
			
			table.clear();
		}
		
		rtcpPortList.clear();
		
		instance = null;
	}

	/**
	 * Generate statistics for monitoring every seconds.
	 * 
	 * 0 : Packets number
	 * 1 : Cumulative number of packets lost
	 * 2 : Minimum of time jitter.
	 * 3 : Maximum of time jitter.
	 * 4 : Average of time jitter.
	 * 5 : Standard deviation of time jitter.
	 * 6 : Number of jumped packets per second.
	 * 7 : Number of inverted packets per second.
	 * 
	 * @return the results
	 */	
	public long[] getResults()
	{
		long[] results = new long[8];
		Long diff = (startTime - stopTime) / 1000;
		
		if(diff.compareTo(0L) == 0)
			diff = 1L;
		
		Integer participantsNumber = 0;
		
		Long totalPacket = 0L;
		Long tempAvgTimeJitter = 0L;
		Long tempStDevTimeJitter = 0L;
		Integer cumulativePacketsLost = 0;
		
		ArrayList<ParticipantStats> partStats = getParticipantsStats();

		if(!partStats.isEmpty())
		{
			for(int i=0; i<partStats.size(); i++)
			{	
				ParticipantStats partInfo = partStats.get(i);
				
				totalPacket = totalPacket + partInfo.getLastPacketSum();
				
				tempAvgTimeJitter = tempAvgTimeJitter + partInfo.getLastTimeJitterSum();
				tempStDevTimeJitter = tempStDevTimeJitter + partInfo.getLastTimeJitterSumSquare();
				
				cumulativePacketsLost = partInfo.getCumulativePacketLost();
			}
			
			participantsNumber = partStats.size();
		}
		
		results[0] = totalPacket / diff;
		
		if(participantsNumber.compareTo(0) == 0)
			participantsNumber = 1;
		
		results[1] = cumulativePacketsLost / participantsNumber;
		
		synchronized(locked)
		{
			if(locked)
			{
				try
				{
					locked.wait(100);
				}
				catch(Exception e)
				{
					throw new IsacRuntimeException("Unable to calculate statistics : " + e);
				}
			}

			if(!locked)
			{	
				
				if(totalPacket.compareTo(0L) != 0)
				{
					results[2] = minTimeJitter;
					results[3] = maxTimeJitter;
				}
				else
				{
					results[2] = 0;
					results[3] = 0;
					totalPacket = 1L;
				}

				// Time jitter
				minTimeJitter = maxTimeJitter;
				maxTimeJitter = results[2];

				// Jump and inversion Sequence Number
				results[6] = jump / diff;
				jump = 0L;

				results[7] = inversion / diff;
				inversion = 0L;
				
				locked.notify();
			}
		}

		Long avgTimeJitter = tempAvgTimeJitter / totalPacket;
		results[4] = avgTimeJitter;

		results[5] = (new Double(Math.sqrt((tempStDevTimeJitter / totalPacket) - (avgTimeJitter * avgTimeJitter)))).longValue();
			
		stopTime = startTime;
		startTime = System.currentTimeMillis();
		
		return results;
	}
	
	/**
	 * Update statistics for each participants
	 * 
	 * @return the statistics for each participants
	 */
	public ArrayList<ParticipantStats> getParticipantsStats()
	{
		ArrayList<ParticipantStats> tempTable = new ArrayList<ParticipantStats>();
		
		synchronized(partStatsTable)
		{
			if(partStatsTable.isEmpty())
			{
				try
				{
					partStatsTable.wait(20);
				}
				catch(Exception e)
				{
					throw new IsacRuntimeException("Unable to get statistics : " + e);
				}
			}
			
			for(int i=0; i<partStatsTable.size(); i++)
			{
				ParticipantStats partStats = partStatsTable.get(i);
				
				tempTable.add(i, partStats);
				
				partStats.newMeasure();
				partStats.calculateFractionLost();
				partStats.calculateCumulativePacketLost();
				
				partStatsTable.set(i, partStats);
			}
		}
		
		return tempTable;
	}
	
	/**
	 * Close RTPStats by stopping thread
	 */
	public void close()
	{
		endThread = true;
		
		rtpListener.close();
	}
	
	/**
	 * Add a port to a list to avoid decoding UDP packets from this port as RTP packets.
	 * 
	 * @param port : the port to add to the RtcpPortList
	 */
	public void addRtcpPortToList(Integer port)
	{
		rtcpPortList.add(port);
	}
	
	/**
	 * Returns the list of participants which have sent a BYE packet.
	 * 
	 * @return the list of participants.
	 */
	public ArrayList<ParticipantStats> getByeList()
	{
		return byeList;
	}
}
