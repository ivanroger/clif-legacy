/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2009 France Telecom
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * Contact: clif@ow2.org
 */

package org.ow2.clif.probe.rtp;

import java.util.LinkedList;

/**
 * Create a RTCP SR/RR packet.
 * 
 * Cf. RFC 3550 for details about construction of SR/RR packet.
 * 
 * @author Remi Druilhe
 */
public class ReportPacket extends RTCPPacket
{
	// For sender and receiver report
	private Integer version = 2;		// Version (-) : 2 bits
	private Integer padding = 0;		// Padding (-) : 1 bit
	private Integer packetType;			// Packet Type (-) : 8 bits
	private Long ssrc;					// SSRC (*) : 32 bits (must be the same ssrc as the RTP session)
	
	// Only for sender report
	private Long MswNtpTimestamp;		// MSW NTP Timestamp (-) : 32 bits
	private Long LswNtpTimestamp;		// MSW NTP Timestamp (-) : 32 bits
	private Long rtpTimestamp;			// RTP Timestamp (*) : 32 bits (must be the same timestamp as the last RTP packet)
	private Long packetCount;			// Sender's packet count (*) : 32 bit
	private Long octetCount;			// Sender's octet count (*) : 32 bits
	
	private LinkedList<byte[]> reportBlock = new LinkedList<byte[]>();
	
	/**
	 * Constructor for a receiver report.
	 * 
	 * @param ssrc
	 */
	public ReportPacket(Long ssrc)	
	{
		this.ssrc = ssrc;
		this.packetType = 201;
	}
	
	/**
	 * Contructor for a sender report.
	 */
	public ReportPacket(Long ssrc, Long rtpTimestamp, Long packetCount, Long octetCount)
	{
		Long time = System.currentTimeMillis();
		
		this.ssrc = ssrc;
		this.MswNtpTimestamp = ReportPacket.generateMswNtpTimestamp(time);
		this.LswNtpTimestamp = ReportPacket.generateLswNtpTimestamp(time);
		this.rtpTimestamp = rtpTimestamp;
		this.packetCount = packetCount;
		this.octetCount = octetCount; 
		
		this.packetType = 200;
	}
	
	/**
	 * Convert every information of the sender's report header in bytes.
	 * 
	 * @return the header formatted in bytes for sender's report.
	 */
	@Override
	public byte[] createPacket()
	{
		Integer length;
		Integer offset = 8;
		
		if(packetType.compareTo(200) == 0)
			length = 28 + 24 * reportBlock.size();
		else
			length = 8 + 24 * reportBlock.size();
		
		byte[] report = new byte[length];
		
		Integer header = (version << 6) & 0xC0;
		header |= (padding << 5) & 0x20;
		
		Integer count = reportBlock.size();
		
		header |= count & 0x1F;
		report[0] = header.byteValue();

		header = packetType & 0xFF;
		report[1] = header.byteValue();

		Integer tempLength = (length >> 2) - 1;
		
		header = tempLength >> 8;
		report[2] = header.byteValue();
		header = tempLength & 0xFF;
		report[3] = header.byteValue();
		
		report[4] = new Long(ssrc >> 24).byteValue();
		report[5] = new Long((ssrc >> 16) & 0xFF).byteValue();
		report[6] = new Long((ssrc >> 8) & 0xFF).byteValue();
		report[7] = new Long(ssrc & 0xFF).byteValue();
		
		if(packetType.compareTo(200) == 0)
		{			
			report[8] = new Long(MswNtpTimestamp >> 24).byteValue();
			report[9] = new Long((MswNtpTimestamp >> 16) & 0xFF).byteValue();
			report[10] = new Long((MswNtpTimestamp >> 8) & 0xFF).byteValue();
			report[11] = new Long(MswNtpTimestamp & 0xFF).byteValue();
			
			report[12] = new Long(LswNtpTimestamp >> 24).byteValue();
			report[13] = new Long((LswNtpTimestamp >> 16) & 0xFF).byteValue();
			report[14] = new Long((LswNtpTimestamp >> 8) & 0xFF).byteValue();
			report[15] = new Long(LswNtpTimestamp & 0xFF).byteValue();
			
			report[16] = new Long(rtpTimestamp >> 24).byteValue();
			report[17] = new Long((rtpTimestamp >> 16) & 0xFF).byteValue();
			report[18] = new Long((rtpTimestamp >> 8) & 0xFF).byteValue();
			report[19] = new Long(rtpTimestamp & 0xFF).byteValue();
			
			report[20] = new Long(packetCount >> 24).byteValue();
			report[21] = new Long((packetCount >> 16) & 0xFF).byteValue();
			report[22] = new Long((packetCount >> 8) & 0xFF).byteValue();
			report[23] = new Long(packetCount & 0xFF).byteValue();
			
			report[24] = new Long(octetCount >> 24).byteValue();
			report[25] = new Long((octetCount >> 16) & 0xFF).byteValue();
			report[26] = new Long((octetCount >> 8) & 0xFF).byteValue();
			report[27] = new Long(octetCount & 0xFF).byteValue();
			
			offset = 28;
		}

		int numberOfReport = 0;
		
		while(!reportBlock.isEmpty())
		{
			byte[] tempReport = reportBlock.removeFirst();
			offset = offset + 24 * numberOfReport;
			
			for(int i=0; i<24; i++)
			{
				report[i+offset] = tempReport[i];
			}
			
			numberOfReport++;
		}
		
		return report;
	}
	
	/**
	 * Add an item to the report packet.
	 * 
	 * @param ssrc the SSRC associated to the statistics.
	 * @param fractionLost the fraction lost (cf. RFC 3550 p32).
	 * @param packetsLost the cumulative number packets lost (cf.RFC 3550 p32).
	 * @param cycle the number of cycle of the sequence number (cf.RFC 3550 p33).
	 * @param maxSeqNum the maximum sequence number read (cf.RFC 3550 p33).
	 * @param jitter the jitter (cf.RFC 3550 p33).
	 * @param lsr last sender report (cf.RFC 3550 p33).
	 * @param dlsr delay since last sender report (cf.RFC 3550 p33).
	 */
	public void addReportBlock(Long ssrc, Integer fractionLost, Integer packetsLost, Integer cycle, Integer maxSeqNum, Long jitter, Long lsr, Long dlsr)
	{
		byte[] report = new byte[24];
		
		report[0] = new Long(ssrc >> 24).byteValue();
		report[1] = new Long((ssrc >> 16) & 0xFF).byteValue();
		report[2] = new Long((ssrc >> 8) & 0xFF).byteValue();
		report[3] = new Long(ssrc & 0xFF).byteValue();

		Integer header = fractionLost & 0xFF;
		report[4] = header.byteValue();

		header = (packetsLost >> 16) & 0xFF;
		report[5] = header.byteValue();
		header = (packetsLost >> 8) & 0xFF;
		report[6] = header.byteValue();
		header = packetsLost & 0xFF;
		report[7] = header.byteValue();

		header = (cycle >> 8) & 0xFF;
		report[8] = header.byteValue();
		header = cycle & 0xFF;
		report[9] = header.byteValue();

		header = (maxSeqNum >> 8) & 0xFF;
		report[10] = header.byteValue();
		header = maxSeqNum & 0xFF;
		report[11] = header.byteValue();

		report[12] = new Long(jitter >> 24).byteValue();
		report[13] = new Long((jitter >> 16) & 0xFF).byteValue();
		report[14] = new Long((jitter >> 8) & 0xFF).byteValue();
		report[15] = new Long(jitter & 0xFF).byteValue();
		
		report[16] = new Long(lsr >> 24).byteValue();
		report[17] = new Long((lsr >> 16) & 0xFF).byteValue();
		report[18] = new Long((lsr >> 8) & 0xFF).byteValue();
		report[19] = new Long(lsr & 0xFF).byteValue();
		
		dlsr = dlsr << 6;
		
		report[20] = new Long(dlsr >> 24).byteValue();
		report[21] = new Long((dlsr >> 16) & 0xFF).byteValue();
		report[22] = new Long((dlsr >> 8) & 0xFF).byteValue();
		report[23] = new Long(dlsr & 0xFF).byteValue();

		if(new Integer(reportBlock.size()).compareTo(31) != 0)
			reportBlock.addLast(report);
	}
	
	/**
	 * Change type of report (201 to 200)
	 */
	public void receiverToSender()
	{
		this.packetType = 200;
	}
	
	/**
	 * Change type of report (200 to 201)
	 */
	public void senderToReceiver()
	{
		this.packetType = 201;
	}
	
	/**
	 * Floor part of the NTP timestamp.
	 * 
	 * Calculation of the time since Jan 01, 1900 at midnight.
	 * 
	 * MSW = (24 * 60 * 60) * ((70 * 365) + 17) + time / 1000
	 * 
	 * @return the time in seconds.
	 */
	public static Long generateMswNtpTimestamp(Long time)
	{
		Long offset = (24L * 60L * 60L) * ((70L * 365L) + 17L);

		return (time / 1000 + offset);
	}
	
	/**
	 * Decimal part of the NTP Timestamp.
	 * 
	 * LSW = E(2^32 * (time & 0xF) / 1000)
	 * 
	 * @return the time in seconds.
	 */
	public static Long generateLswNtpTimestamp(Long time)
	{
		Double microseconds = new Double(time - (time / 1000) * 1000) / 1000;

		return new Double(Math.pow(2, 32) * microseconds).longValue();
	}
	
	/**
	 * /**
	 * Returns the packet type. Here it is 200 or 201.
	 */
	@Override
	public Integer getPacketType()
	{
		return packetType;
	}
	
	/**
	 * Decode SSRC from a SR/RR report.
	 * 
	 * @param data : the data to decode.
	 * @param offset : the beginning of the report.
	 * @return SSRC of the report.
	 */
	public static Long decodeSsrc(byte[] data, Integer offset)
	{
		return new Long((decodeTwoComplement(new Byte(data[offset + 4]).longValue()) << 24)
				+ (decodeTwoComplement(new Byte(data[offset + 5]).longValue()) << 16)
				+ (decodeTwoComplement(new Byte(data[offset + 6]).longValue()) << 8) 
				+ decodeTwoComplement(new Byte(data[offset + 7]).longValue()));
	}
	
	/**
	 * Decode MSW Timestamp from a SR report.
	 * 
	 * @param data : the data to decode.
	 * @param offset : the beginning of the report.
	 * @return MSW Timestamp of the report.
	 */
	public static Long decodeSrMswNtpTimestamp(byte[] data, Integer offset)
	{
		return new Long((decodeTwoComplement(new Byte(data[offset + 8]).longValue()) << 24)
				+ (decodeTwoComplement(new Byte(data[offset + 9]).longValue()) << 16)
				+ (decodeTwoComplement(new Byte(data[offset + 10]).longValue()) << 8) 
				+ decodeTwoComplement(new Byte(data[offset + 11]).longValue()));
	}
	
	/**
	 * Decode LSW Timestamp from a SR report.
	 * 
	 * @param data : the data to decode.
	 * @param offset : the beginning of the report.
	 * @return LSW Timestamp of the report.
	 */
	public static Long decodeSrLswNtpTimestamp(byte[] data, Integer offset)
	{
		return new Long((decodeTwoComplement(new Byte(data[offset + 12]).longValue()) << 24)
				+ (decodeTwoComplement(new Byte(data[offset + 13]).longValue()) << 16)
				+ (decodeTwoComplement(new Byte(data[offset + 14]).longValue()) << 8) 
				+ decodeTwoComplement(new Byte(data[offset + 15]).longValue()));
	}
	
	/**
	 * To add new value to the SR report.
	 * 
	 * @param rtpTimestamp : the RTP timestamp of the last RTP packet (cf. RFC 3550 p31).
	 * @param packetCount : the number of packets since the beginning of the RTP session (cf. RFC 3550 p32).
	 * @param octetCount : the number of octets since the beginning of the RTP session (cf. RFC 3550 p32).
	 */
	public void setNewValues(Long rtpTimestamp, Long packetCount, Long octetCount)
	{
		Long time = System.currentTimeMillis();
		
		this.MswNtpTimestamp = ReportPacket.generateMswNtpTimestamp(time);
		this.LswNtpTimestamp = ReportPacket.generateLswNtpTimestamp(time);
		this.rtpTimestamp = rtpTimestamp;
		this.packetCount = packetCount;
		this.octetCount = octetCount; 
	}
}
