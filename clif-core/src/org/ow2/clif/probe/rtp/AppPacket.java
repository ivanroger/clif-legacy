/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2009 France Telecom
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * CLIF
 *
 * Contact: clif@ow2.org
 */

package org.ow2.clif.probe.rtp;

/**
 * Create a RTCP APP packet.
 * 
 *  0                   1                   2                   3
 *  0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
 * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 * |V=2|P| subtype |    PT=APP=204 |              length           |
 * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 * |                            SSRC/CSRC                          |
 * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 * |                           name (ASCII)                        |
 * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 * |                   application-dependent data                ...
 * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 * 
 * Cf. RFC 3550 for details about construction of APP packet.
 * 
 * @author Rémi Druilhe
 */
public class AppPacket extends RTCPPacket
{
	private Integer version = 2;		// Version (-) : 2 bits
	private Integer padding = 0;		// Padding (-) : 1 bit
	private Integer packetType = 204;	// Packet Type (-) : 8 bits
	
	private String name;				// Name (*) : 32 bits
	private String data;				// Data (*) : 4000 bits max
	private Integer subtype;			// Subtype (*) : 5 bits
	private Long ssrc;					// SSRC (*) : 32 bits
	
	/**
	 * Constructor
	 * 
	 * @param subtype : the subtype of the packet. 
	 * @param ssrc : the SSRC of the packet.
	 * @param name : the name of the packet.
	 * @param data : the data to transmit in the packet (must be less than 500 bytes).
	 */
	public AppPacket(Integer subtype, Long ssrc, String name, String data)
	{
		this.subtype = subtype;
		this.ssrc = ssrc;
		this.name = name;
		this.data = data;
	}
	
	/**
	 * Method to create a APP packet.
	 * 
	 * @return the APP packet in bytes.
	 */
	@Override
	public byte[] createPacket()
	{
		Integer length = 8 + 4 + data.length();
		byte[] app = new byte[length];
		
		Integer header = (version << 6) & 0xC0;
		header |= (padding << 5) & 0x20;

		header |= subtype & 0x1F;
		app[0] = header.byteValue();

		header = packetType & 0xFF;
		app[1] = header.byteValue();
		
		Integer tempLength = (length >> 2) - 1;
		
		header = tempLength >> 8;
		app[2] = header.byteValue();
		header = tempLength & 0xFF;
		app[3] = header.byteValue();
		
		app[4] = new Long(ssrc >> 24).byteValue();
		app[5] = new Long((ssrc >> 16) & 0xFF).byteValue();
		app[6] = new Long((ssrc >> 8) & 0xFF).byteValue();
		app[7] = new Long(ssrc & 0xFF).byteValue();
		
		for(int i=0; i<4; i++)
			app[i+8] = name.getBytes()[i];
		
		byte[] tempData = data.getBytes();
		
		for(int i=0; i<tempData.length; i++)
			app[i+12] = tempData[i];
		
		return app;
	}
	
	/**
	 * Returns the packet type. Here it is 204.
	 */
	@Override
	public Integer getPacketType()
	{
		return packetType;
	}
	
	/**
	 * Decode SSRC from a APP packet.
	 * 
	 * @param data : the data to decode.
	 * @param offset : the beginning of the report.
	 * @return SSRC of the packet.
	 */
	public Long decodeSsrc(byte[] data, Integer offset)
	{
		return new Long((decodeTwoComplement(new Byte(data[offset + 4]).longValue()) << 24)
				+ (decodeTwoComplement(new Byte(data[offset + 5]).longValue()) << 16)
				+ (decodeTwoComplement(new Byte(data[offset + 6]).longValue()) << 8) 
				+ decodeTwoComplement(new Byte(data[offset + 7]).longValue()));
	}
	
	/**
	 * Decode Name from a APP packet.
	 * 
	 * @param data : the data to decode.
	 * @param offset : the beginning of the report.
	 * @return name of the packet.
	 */
	public Long decodeName(byte[] data, Integer offset)
	{
		return new Long((decodeTwoComplement(new Byte(data[offset + 8]).longValue()) << 24)
				+ (decodeTwoComplement(new Byte(data[offset + 9]).longValue()) << 16)
				+ (decodeTwoComplement(new Byte(data[offset + 10]).longValue()) << 8) 
				+ decodeTwoComplement(new Byte(data[offset + 11]).longValue()));
	}
}
