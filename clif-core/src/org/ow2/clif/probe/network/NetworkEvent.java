/*
* CLIF is a Load Injection Framework
* Copyright (C) 2004, 2005 France Telecom R&D
* Copyright (C) 2014 Orange
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
* Contact: clif@ow2.org
*/

package org.ow2.clif.probe.network;

import org.ow2.clif.storage.api.AbstractEvent;
import org.ow2.clif.storage.api.BladeEvent;
import org.ow2.clif.storage.api.EventFactory;
import org.ow2.clif.storage.api.ProbeEvent;
import org.ow2.clif.supervisor.api.ClifException;


/**
 * @author Francois AHIBA
 * @author Bruno Dillenseger
 */
public class NetworkEvent extends ProbeEvent
{
	private static final long serialVersionUID = 3656496024046927972L;
	static public final String EVENT_TYPE_LABEL = "network";
	static final String[] EVENT_FIELD_LABELS = new String[] {
		"date",
		"receive throughput (bit/s)",
		"packets received",
		"transmit throughput (bit/s)",
		"packets transmitted",
		"receive errors",
		"receive overruns",
		"receive drops",
		"transmit errors",
		"transmit overruns",
		"transmit drops",
		"transmit collisions"
	};

	static
	{
		AbstractEvent.registerEventFieldLabels(
			EVENT_TYPE_LABEL,
			EVENT_FIELD_LABELS,
			new EventFactory() {
				@Override
				public BladeEvent makeEvent(String separator, String line)
					throws ClifException
				{	
					return fillEvent(separator, line, new NetworkEvent());
				}
			});
	}


	private NetworkEvent()
	{
		super();
	}


	NetworkEvent(long date, long[] values)
	{
		super(date, values);
	}

	//////////////////////////
	// BladeEvent interface //
	//////////////////////////


	@Override
	public String getTypeLabel()
	{
		return EVENT_TYPE_LABEL;
	}


	@Override
	public String[] getFieldLabels()
	{
		return EVENT_FIELD_LABELS;
	}
}
