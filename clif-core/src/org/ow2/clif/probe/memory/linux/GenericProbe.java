/*
* CLIF is a Load Injection Framework
* Copyright (C) 2010 France Telecom R&D
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
* Contact: clif@ow2.org
*/

package org.ow2.clif.probe.memory.linux;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Linux generic memory probe intended to be more impervious to /proc file format changes.
 * Reads data from /proc/meminfo, supposed to be formatted as a list of
 * "attribute: value [unit]" lines. 
 * @author Bruno Dillenseger
 */
public class GenericProbe
{
	// data source file
	static private final File MEM_FILE = new File("/proc/meminfo");
	// full ordered list of attributes names (index in list = line number in file)
	static private List<String> attributes = null;

	// class initialization: get list of attributes names for once
	static
	{
		try
		{
			BufferedReader reader = new BufferedReader(new FileReader(MEM_FILE));
			attributes = new ArrayList<String>();
			String line;
			while ((line = reader.readLine()) != null)
			{
				attributes.add(line.substring(0,line.indexOf(':')).trim());
			}
			reader.close();
		}
		catch (Exception ex)
		{
			throw new Error("Could not initialize Linux Generic Probe", ex);
		}
	}

	// contains the line numbers of attributes of interest for this probe
	private short[] lines;
	// contains the values of the list of available attributes
	// declared here for once to avoid useless object creation
	private long[] values = new long[attributes.size()];


	/**
	 * Creates a new memory probe that will return only the mentioned
	 * attributes' values, in the very same order.
	 * @param fields names of attributes of interest.
	 * @throws Exception an attribute name does not exist
	 * @see #getData()
	 */
	public GenericProbe(String[] fields)
		throws Exception
	{
		lines = new short[fields.length];
		int i = 0;
		for (String field : fields)
		{
			lines[i] = (short)attributes.indexOf(field);
			if (lines[i] == -1)
			{
				throw new Exception("Unknown field in " + MEM_FILE.getPath());
			}
			++i;
		}
	}


	/**
	 * Reads the system data source file for memory usage, and returns
	 * the values of attributes of interest.
	 * @return values of memory attributes of interest,
	 * as specified at probe creation (in the same order).
	 * @throws IOException for some reason, the system data source file
	 * could not be read, or more probably the file format is not
	 * compatible with this probe.
	 * @see #GenericProbe(String[])
	 */
	public long[] getData()
		throws IOException
	{
		long[] data = new long[lines.length];
		InputStream ins = new FileInputStream(MEM_FILE);
		short lineNumber = 0;
		int c;
		short state = 0;

		while ((c = ins.read()) != -1)
		{
			switch (state)
			{
			case 0: // reading field name
				switch (c)
				{
				case '\n': // ignore blank line (?)
					++lineNumber;
					break;
				case ':':
					state = 1;
					break;
				}
				break;
			case 1: // reading blank characters
				switch (c)
				{
				case '\n': // blank value (?)
					values[lineNumber] = -1;
					++lineNumber;
					state = 0;
					break;
				case ' ':
				case '\t':
					break;
				default:
					state = 2;
					values[lineNumber] = c - '0';
				}
				break;
			case 2: // reading digits
				switch (c)
				{
				case '\n':
					++lineNumber;
					state = 0;
					break;
				case ' ':
				case '\t':
					state = 3;
					break;
				default:
					values[lineNumber] = 10 * values[lineNumber] + c - '0';
				}
				break;
			case 3: // reading trailing characters (unit)
				switch (c)
				{
				case '\n':
					++lineNumber;
					state = 0;
					break;
				}
				break;
			}
		}
		ins.close();
		for (int i=0 ; i<data.length ; ++i)
		{
			data[i] = values[lines[i]];
		}
		return data;
	}
}
