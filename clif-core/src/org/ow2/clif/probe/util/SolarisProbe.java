/*
* CLIF is a Load Injection Framework
* Copyright (C) 2011 France Telecom R&D
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
* Contact: clif@ow2.org
*/

package org.ow2.clif.probe.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;
import java.util.regex.MatchResult;
import java.util.regex.Pattern;

/**
 * Probe for Solaris, designed to produce a variety of system usage
 * measures as a single, background activity, and shared by CLIF probes
 * of several kinds, namely cpu and memory as of current version.
 * This probe is based on the following command lines to get measures:
 * <ul>
 *   <li> "swap -s" to get the total amount of swap configured for the system
 *   <li> "prtconf" to get the total RAM installed in the system
 *   <li> "vmstat 1" to get RAM and swap usage every second.
 * </ul>
 * This probe shall be used as a singleton, and preferably getting measures
 * each second, because the singleton produces 1 measure per second and does
 * not buffer, nor aggregate, measures in case they are not queried (they are
 * simply discarded).
 * 
 * @author Bruno Dillenseger
 */
public class SolarisProbe implements Runnable
{
	static private final String SWAP_CMD = "swap -s";
	static private final String PRTCONF_CMD = "prtconf";
	static private final String VMSTAT_CMD = "vmstat 1";

	static private SolarisProbe singleton = null;
	/** Counts the number of current singleton's clients;
	 * when zero, the possibly running singleton is stopped
	 * and discarded. A new singleton will be created on next
	 * singleton client.
	 * The counter is incremented on each call to getSingleton()
	 * method, and decremented on each call to stop() method.
	 * Calling stop() is important to enable singleton renewing, and
	 * killing the background vmstat process when it becomes useless.
	 */
	static private int clientCount = 0;

	/**
	 * Gets the SolarisProbe singleton, creating a new one when the
	 * client counter is zero.
	 * @return the SolarisProbe singleton
	 * @throws Exception the singleton could not be created.
	 * @see #clientCount
	 * @see #SolarisProbe()
	 */
	static public synchronized SolarisProbe getSingleton() throws Exception
	{
		if (clientCount == 0)
		{
			singleton = new SolarisProbe();
		}
		++clientCount;
		return singleton;
	}

	// total memory kB, free memory kB, total swap kB, free swap kB, user cpu %, sys cpu %
	private volatile long[] latestMeasures = {0, 0, 0, 0, 0, 0};
	private Process jobProcess;
	private BufferedReader jobOutput;
	private int freeMemoryIndex;
	private int freeSwapIndex;
	private int userCpuIndex;
	private int sysCpuIndex;
	private volatile boolean stopped = false;


	/**
	 * Gets the total RAM and swap sizes for the current system, in kB. Then, starts
	 * the resource usage polling thread, consisting in a "vmstat 1" background process.
	 * @throws Exception the singleton could not be created, for this version of the
	 * SolarisProbe is not compatible with the SunOS/Solaris version or set-up. Check
	 * for availability and output format of commands "swap -s", "prtconf" and "vmstat 1".
	 */
	private SolarisProbe()
		throws Exception
	{
		Process proc;
		Scanner scan;

		// get total swap size
		proc = execAndReportException(SWAP_CMD);
		scan = new Scanner(proc.getInputStream()).useDelimiter("\\D+");
		scan.nextLong(); // skip allocated
		scan.nextLong(); // skip reserved
		latestMeasures[2] = (scan.nextLong() + scan.nextLong()); // used + available in kB
		scan.close();

		// get total physical memory size
		proc = execAndReportException(PRTCONF_CMD);
		BufferedReader reader = new BufferedReader(new InputStreamReader(proc.getInputStream()));
		String line;
		Pattern regex = Pattern.compile("Memory size: (\\d*) Megabytes");
		while ((line = reader.readLine()) != null && latestMeasures[0] == 0)
		{
			scan = new Scanner(line);
			if (scan.findInLine(regex) != null)
			{
				MatchResult result = scan.match();
				latestMeasures[0] = Long.parseLong(result.group(1)) << 10;
			}
		}
		proc.getInputStream().close();
		proc.waitFor();
		jobProcess = Runtime.getRuntime().exec(VMSTAT_CMD);
		jobOutput = new BufferedReader(new InputStreamReader(jobProcess.getInputStream()));
		line = jobOutput.readLine();
		if (line.contains("cpu"))
		{
			line = jobOutput.readLine();
		}
		scan = new Scanner(line);
		int index = 0;
		while (scan.hasNext())
		{
			String field = scan.next();
			if (field.equals("swap"))
			{
				freeSwapIndex = index;
			}
			else if (field.equals("free"))
			{
				freeMemoryIndex = index;
			}
			else if (field.equals("id"))
			{
				userCpuIndex = index - 2;
				sysCpuIndex = index -1;
			}
			++index;
		}
		new Thread(this, "Solaris probe").start();
	}


	/**
	 * Runs a command line and waits for its completion.
	 * In case of non-zero exit value, the standard error output
	 * is read and returned in a thrown exception.
	 * @param command the command line to run
	 * @return the process object for possible further operations
	 * (such as reading its standard output).
	 * @throws Exception the command line exited with non-zero value.
	 * The possible error message is included in the exception.
	 */
	private Process execAndReportException(String command)
		throws Exception
	{
		Process proc = Runtime.getRuntime().exec(command);
		if (proc.waitFor() != 0)
		{
			BufferedReader reader = new BufferedReader(
				new InputStreamReader(proc.getErrorStream()));
			StringBuilder message = new StringBuilder();
			String line;
			while ((line = reader.readLine()) != null)
			{
				message.append(line + "\n");
			}
			reader.close();
			throw new Exception("Can't execute command " + command + ": " + message.toString());
		}
		else
		{
			return proc;
		}
	}


	/**
	 * Gets the latest measures from the resource usage polling process.
	 * @return an array of long with the following values (in this order):
	 * <ul>
	 *   <li> total memory kB
	 *   <li> free memory kB
	 *   <li> total swap kB
	 *   <li> free swap kB
	 *   <li> user cpu %
	 *   <li> sys cpu %
	 * </ul>
	 */
	public long[] getData()
	{
		return latestMeasures.clone();
	}


	/**
	 * Decreases the number of clients for this SolarisProbe,
	 * and stops the resource usage polling process when the
	 * client counter becomes zero.
	 * @throws InterruptedException
	 */
	public synchronized void stop() throws InterruptedException
	{
		if (--clientCount == 0 && ! stopped)
		{
			stopped = true;
			wait();
		}
	}

	
	// Runnable interface


	/**
	 * Resource usage polling activity.
	 * Reads the output of command "vmstat 1" until
	 * boolean "stopped" becomes true.
	 * @see #getSingleton()
	 * @see #stop()
	 */
	public void run()
	{
		try
		{
			while (! stopped)
			{
				String line = jobOutput.readLine();
				long[] measures = new long[6];
				while (line.contains("cpu") || line.contains("swap"))
				{
					line = jobOutput.readLine();
				}
				Scanner scan = new Scanner(line);
				int index = 0;
				while (scan.hasNext())
				{
					if (index == freeMemoryIndex)
					{
						measures[1] = scan.nextLong();
					}
					else if (index == freeSwapIndex)
					{
						measures[3] = scan.nextLong();
					}
					else if (index == userCpuIndex)
					{
						measures[4] = scan.nextLong();
					}
					else if (index == sysCpuIndex)
					{
						measures[5] = scan.nextLong();
					}
					else
					{
						scan.next();
					}
					++index;
				}
				measures[0] = latestMeasures[0];
				measures[2] = latestMeasures[2];
				latestMeasures = measures;
			}
		}
		catch (IOException ex)
		{
			latestMeasures = null;
		}
		jobProcess.destroy();
		synchronized (this)
		{
			stopped = true;
			notify();
		}
	}
}
