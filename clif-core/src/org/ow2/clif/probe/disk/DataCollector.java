/*
* CLIF is a Load Injection Framework
* Copyright (C) 2004 France Telecom R&D
* Copyright (C) 2014 Orange
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
* Contact: clif@ow2.org
*/

package org.ow2.clif.probe.disk;
 
import java.io.Serializable;
import org.ow2.clif.datacollector.lib.AbstractProbeDataCollector;

/**
 * Data collector for disk probe
 * @author Jean-Francois AHIBA
 * @author Bruno Dillenseger
 */
public class DataCollector extends AbstractProbeDataCollector
{
	static public final String[] LABELS = new String[DiskEvent.EVENT_FIELD_LABELS.length - 1];
	static {
		for (int i=1 ; i<DiskEvent.EVENT_FIELD_LABELS.length ; ++i)
		{
			LABELS[i-1] = DiskEvent.EVENT_FIELD_LABELS[i];
		}
	}

    @Override
	public void init(Serializable testId, String probeId)
	{      
		stats = new long[LABELS.length];
		super.init(testId, probeId);
	}


	//////////////////////////////////
	// DataCollectorAdmin interface //
	//////////////////////////////////


	@Override
	public String[] getLabels()
	{  
		
		return LABELS;
	}
}
