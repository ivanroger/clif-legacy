/*
* CLIF is a Load Injection Framework
* Copyright (C) 2014 Orange
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
* Contact: clif@ow2.org
*/

package org.ow2.clif.probe.disk;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import org.hyperic.sigar.FileSystem;
import org.hyperic.sigar.FileSystemMap;
import org.hyperic.sigar.FileSystemUsage;
import org.hyperic.sigar.Sigar;
import org.hyperic.sigar.SigarException;
import org.ow2.clif.probe.util.AbstractDumbInsert;
import org.ow2.clif.probe.util.SigarUtil;
import org.ow2.clif.storage.api.ProbeEvent;
import org.ow2.clif.supervisor.api.ClifException;

/**
 * Insert for disk probe based on SIGAR
 * @author Bruno Dillenseger
 */
public class Insert extends AbstractDumbInsert 
{
	private FileSystemUsage probe = new FileSystemUsage();
	private String diskName;
	private long prevReads = -1;
	private long prevReadBytes = -1;
	private long prevWrites = -1;
	private long prevWriteBytes = -1;
	private long prevTime = -1;


	public Insert()
	throws ClifException
	{
		super();
		eventStorageStatesMap.put("store-lifecycle-events", storeLifeCycleEvents);
		eventStorageStatesMap.put("store-alarm-events", storeAlarmEvents);
		eventStorageStatesMap.put("store-" + DiskEvent.EVENT_TYPE_LABEL + "-events", storeProbeEvents);
	}


	@Override
	public String getHelpMessage()
	{
		return super.getHelpMessage()
			+ " <partition path>\n"
			+ "Available partitions: " + getAvailablePartitions();
	}


	private List<String> getAvailablePartitions()
	{
		List<String> partitions = new LinkedList<String>();
		try
		{
			for (FileSystem candidate : SigarUtil.getSigar().getFileSystemList())
	   		{
	   			if (candidate.getType() == FileSystem.TYPE_LOCAL_DISK)
	   			{
	   				partitions.add(candidate.getDirName());
	   			}
	   		}
		}
		catch (SigarException ex)
		{
			ex.printStackTrace(System.err);
			partitions = null;
		}
		return partitions;
	}


	@Override
	protected void setExtraArguments(List<String> args)
	throws ClifException
	{ 
        if (! args.isEmpty())
        {
            String dir = args.get(0);
           	Sigar sigar = SigarUtil.getSigar();
           	try
           	{
           		FileSystemMap fsm = sigar.getFileSystemMap();
               	FileSystem fs = fsm.getFileSystem(dir);
               	if (fs == null || fs.getType() != FileSystem.TYPE_LOCAL_DISK)
               	{
               		throw new ClifException(
               			"Disk probe error: " + dir + " is not a valid partition path."
               			+ "\nValid paths: " + getAvailablePartitions());
               	}
               	diskName = fs.getDirName();
           	}
           	catch (SigarException ex)
           	{
           		throw new ClifException("Disk probe error: " + ex.getMessage(), ex);
           	}
        }
        else
        {
        	throw new ClifException("Disk probe error: the target partition path argument is missing.");
        }
    }


	@Override
	public ProbeEvent doProbe()
	{
		DiskEvent event = null;
		try
		{
			long currentTime = System.currentTimeMillis();
			probe.gather(SigarUtil.getSigar(), diskName);
	    	if (prevTime != -1)
	    	{
				long elapsedTime_s = (currentTime - prevTime) / 1000;
				long[] values = new long[10];
				values[0] = probe.getDiskReads() - prevReads;
				values[1] = ((probe.getDiskReadBytes() - prevReadBytes) / elapsedTime_s) >> 10;
				values[2] = probe.getDiskWrites() - prevWrites;
				values[3] = ((probe.getDiskWriteBytes() - prevWriteBytes) / elapsedTime_s) >> 10;
				values[4] = Math.round(probe.getDiskServiceTime() * 1000);
				values[5] = Math.round(probe.getDiskQueue());
				values[6] = probe.getFree() >> 10;
				values[7] = Math.round(100 * probe.getUsePercent());
				values[8] = probe.getFreeFiles();
				values[9] = (100 * (probe.getFiles() - probe.getFreeFiles())) / probe.getFiles();
				event = new DiskEvent(currentTime, values);
	    	}
	    	prevTime = currentTime;
	    	prevReads = probe.getDiskReads();
	    	prevReadBytes = probe.getDiskReadBytes();
	    	prevWrites = probe.getDiskWrites();
	    	prevWriteBytes = probe.getDiskWriteBytes();
		}
		catch (Exception ex)
		{
			ex.printStackTrace(System.err);
		}
		return event;
	}


	static public void main(String args[])
	throws Exception
	{
		Insert ins = new Insert();
		ins.setExtraArguments(Arrays.asList(args));
		while (true)
		{
			ProbeEvent measurement = ins.doProbe();
			System.out.println(measurement);
			Thread.sleep(1000);
		}
	}
}
