/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2011 France Telecom
 * Copyright (C) 2016 Orange SA
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * Contact: clif@ow2.org
 */

package org.ow2.clif.console.lib.batch;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.ow2.clif.console.lib.ClifDeployDefinition;
import org.ow2.clif.console.lib.TestPlanReader;
import org.ow2.clif.deploy.ClifRegistry;
import org.ow2.clif.util.ExecutionContext;


/**
 * Batch command to wait for the CLIF registry to be ready, and,
 * optionally, for CLIF servers used in a test plan to be ready
 * (i.e. to be registered in the registry). The default "local host"
 * CLIF server is not considered since it is supposed to be created
 * by the deploy command. 
 * 
 * @author Bruno Dillenseger
 */
public class WaitServersCmd
{
	static public final int sleepTimeMs = 1000;

	/**
	 * @see #run(String)
	 * @param args args[0] (optional) the file name of the test plan
	 * whose CLIF servers to wait for.
	 */
	static public void main(String[] args)
	{
		if (System.getSecurityManager() == null)
		{
			System.setSecurityManager(new SecurityManager());
		}
		ExecutionContext.init("./");
		int res = BatchUtil.ERR_ARGS;
		if (args.length == 0)
		{
			res = run(null);
		}
		else if (args.length == 1)
		{
			res = run(args[0]);
		}
		else
		{
			BatchUtil.usage("optional argument: <test plan definition file>");
		}
		System.exit(res);
	}


	/**
	 * Wait for the registry to be ready, and then for all CLIF servers
	 * in a given test plan file to be registered in the registry. Note:
	 * the default CLIF server named "local host" is ignored, since it is
	 * supposed to be created by the deploy command.
	 * @param fileName (optional) file name of the test plan definition
	 * @return command status code (@see BatchUtil)
	 */
	static public int run(String fileName)
	{
		try
		{
			ClifRegistry clifReg = null;
			System.out.println("Trying to connect to the CLIF Registry...");
			// wait for the CLIF registry to be reachable
			while (clifReg == null)
			{
				try
				{
					clifReg = new ClifRegistry(false);
				}
				catch (Exception ex)
				{
					Thread.sleep(sleepTimeMs);
				}
			}
			System.out.println("Connected to " + clifReg);
			// wait for CLIF servers
			if (fileName != null)
			{
				// get list of necessary CLIF servers
				File ctpFile = new File(fileName);
				Collection<ClifDeployDefinition> definitions = TestPlanReader.readFromProp(
					new FileInputStream(ctpFile)).values();
				Set<String> required = new HashSet<String>();
				for (ClifDeployDefinition def : definitions)
				{
					required.add(def.getServerName());
				}
				required.remove(ExecutionContext.DEFAULT_SERVER);
				System.out.println("Waiting for CLIF servers...");
				// loop until all CLIF servers are registered
				while (! required.isEmpty())
				{
					List<String> available = Arrays.asList(clifReg.getServers());
					for (String name : available)
					{
						if (required.contains(name))
						{
							System.out.println("CLIF server " + name + " is available.");
							required.remove(name);
						}
					}
					if (! required.isEmpty())
					{
						Thread.sleep(sleepTimeMs);
					}
				}
				System.out.println("Required CLIF servers are ready.");
				return BatchUtil.SUCCESS;
			}
		}
		catch (FileNotFoundException fileEx)
		{
			System.err.println("Test plan file " + fileName + " not found");
			return BatchUtil.ERR_ARGS;
		}
		catch (IOException ioEx)
		{
			System.err.println("Invalid test plan file " + fileName);
			ioEx.printStackTrace(System.err);
			return BatchUtil.ERR_ARGS;
		}
		catch (Exception ex)
		{
			System.err.println("Execution problem while waiting for CLIF registry and servers");
			ex.printStackTrace(System.err);
			return BatchUtil.ERR_EXEC;
		}
		return BatchUtil.SUCCESS;
	}
}
