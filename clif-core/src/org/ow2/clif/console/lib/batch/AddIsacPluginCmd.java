/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2005, 2006 France Telecom
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * CLIF $Name: not supported by cvs2svn $
 *
 * Contact: clif@ow2.org
 */

package org.ow2.clif.console.lib.batch;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

/**
 * This class add isac-plugins into Manifest.mf file in order to be abble to use 
 * these isac-plugins with the CLIF console or with CLIF eclipse plugins.
 * @param args
 */
public class AddIsacPluginCmd {

	private static File[] files;
	private static ArrayList<String> filesPresentInManifest = new ArrayList<String>();
	
	/**
	 * Add isac-plugins name at the end of Manifest.mf file if the isac-plugins
	 *  path is not yet present
	 * @param args contains the path where libraries and the manifest are located 
	 */
	
	public static void main(String[] args) {
		String dirName = args[0];
		files = new File(dirName).listFiles();
		
		BufferedReader br = null;
		BufferedWriter bw = null;
		
		try {

			br = new BufferedReader(new FileReader(args[1]));
			bw = new BufferedWriter(new FileWriter(args[2]));

			String line;
			while ((line = br.readLine()) != null) {
				if (line.contains("Bundle-ClassPath:")) {
					bw.append(line+"\n");
					//System.out.println(line);					
					addFilesPathInManifest(bw, br);
				} else {
					bw.append(line+"\n");
					//System.out.println(line);
				}	
			}
			// dans tous les cas, on ferme nos flux
			br.close();
			bw.close();
		
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		
		
	}
	
	private static void addFilesPathInManifest(BufferedWriter bw, BufferedReader br) {
		String line;
		
		try {
			while ((line = br.readLine()) != null) {
				if (!line.endsWith(",")) {
					String[] splittedLine = line.split("/");
					String strToAdd = splittedLine[splittedLine.length-1].substring(0, splittedLine[splittedLine.length-1].length()-1);
					filesPresentInManifest.add(strToAdd);
					
					line = line + ",";
					//System.out.println(line);
					bw.append(line+"\n");
					
					for (int k = 0; k < files.length-1; k++) {
						if (!fileExistInManifest(files[k].getName())) {
							
							//System.out.println(" lib/ext/"+files[k].getName()+",");
							bw.append(" lib/ext/"+files[k].getName()+",\n");							
						}
					}

					//System.out.println(" lib/ext/"+files[files.length-1].getName());
					bw.append(" lib/ext/"+files[files.length-1].getName()+"\n");	
					
					break;
					
				} else {
					String[] splittedLine = line.split("/");
					String strToAdd = splittedLine[splittedLine.length-1].substring(0, splittedLine[splittedLine.length-1].length()-2);
					filesPresentInManifest.add(strToAdd);
					
					//System.out.println(line);
					bw.append(line+"\n");
				}
			}

		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private static boolean fileExistInManifest(String fileName) {
		boolean result = false;
		for (int j = 0; j < filesPresentInManifest.size(); j++) {
			if (fileName.contains(filesPresentInManifest.get(j))) {
				result = true;
				break;
			} else {
				result = false;
			}
		}
		return result;
	}

}
