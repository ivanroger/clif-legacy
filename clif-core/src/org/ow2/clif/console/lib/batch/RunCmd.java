/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2005, 2006 France Telecom
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * CLIF $Name: not supported by cvs2svn $
 *
 * Contact: clif@ow2.org
 */

package org.ow2.clif.console.lib.batch;

import org.ow2.clif.util.ExecutionContext;

/**
 * Batch command to initialize and start a deployed test plan,
 * or just a subset of its blades, and then collect data after termination.
 * 
 * @author Joan Chaumont
 * @author Bruno Dillenseger
 */
public class RunCmd
{
	/**
	 * Initializes and starts a deployed test plan, or just a subset of its blades,
	 * and then collect data after termination.
	 * @param args args[0] is the name of the deployed test plan name to use,
	 * args[1] is the identifier string for this new test run,
	 * args[2] is optional and may contain a list of blade identifiers,
	 * separated by : character (bladeId1:bladeId2:...bladeIdn).
	 */
	public static void main(String[] args)
	{
		if (System.getSecurityManager() == null) {
			System.setSecurityManager(new SecurityManager());
		}
		if (args.length < 2)
		{
			BatchUtil.usage("expected arguments: <name of deployed test plan> <new test run identifier> [<bladeId1:bladeId2:...bladeIdn>]");
		}
		ExecutionContext.init("./");
		System.exit(run(args[0], args[1], BatchUtil.getSelBlades(args, 2)));
	}


	/**
	 * Initializes and starts a deployed test plan, or just a subset of its blades,
	 * and then collect data after termination.
	 * @param testPlanName the name of the deployed test plan name to use
	 * @param testRunId the identifier string for this new test run
	 * @param blades array of target blade identifiers. A null value means all blades
	 * in the test plan are concerned.
	 * @return execution status code
	 */
	static public int run(String testPlanName, String testRunId, String[] blades)
	{
		try
		{
			int retcode;
			retcode = InitCmd.run(testPlanName, testRunId);
			if (retcode != BatchUtil.SUCCESS)
			{
				return retcode;
			}
			retcode = StartCmd.run(testPlanName, blades);
			if (retcode != BatchUtil.SUCCESS)
			{
				return retcode;
			}
			retcode = CollectCmd.run(testPlanName, blades);
			if (retcode != BatchUtil.SUCCESS)
			{
				return retcode;
			}
			System.out.println("test run " + testPlanName + " complete");
			return BatchUtil.SUCCESS;
		}
		catch (Exception ex)
		{
			ex.printStackTrace(System.err);
			return BatchUtil.ERR_EXEC;
		}
	}
}
