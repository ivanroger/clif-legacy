/*
* CLIF is a Load Injection Framework
* Copyright (C) 2012 France Telecom
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
* Contact: clif@ow2.org
*/

package org.ow2.clif.console.lib.batch;

import org.ow2.clif.analyze.lib.quickstat.TestStats;
import org.ow2.clif.storage.api.StorageRead;
import org.ow2.clif.storage.lib.filestorage.FileStorageReader;
import org.ow2.clif.util.ExecutionContext;

/**
 * Command for printing a simple statistical report on load injectors
 * of the latest test execution.
 * @see TestStats
 * @author Bruno Dillenseger
 */
public class QuickstatsCmd
{
	/**
	 * Prints a simple statistical report on load injection for the latest
	 * test execution.
	 * @param args optional: args[0] is the path to the file storage root directory.
	 * If omitted, the default file storage root directory will be used, according
	 * to the CLIF environment's settings.
	 */
	public static void main(String[] args)
	{
		if (System.getSecurityManager() == null)
		{
			System.setSecurityManager(new SecurityManager());
		}
		ExecutionContext.init("./");
		String reportDir = null;
		if (args.length > 0)
		{
			reportDir = args[0];
		}
		System.exit(run(reportDir));
	}


	/**
	 * Prints a simple statistical report on load injection for the latest
	 * test execution.
	 * @param path the path to the file storage root directory. If null, the
	 * default file storage root directory will be used, according to the
	 * CLIF environment's settings.
	 * @return command status code (@see BatchUtil)
	 */
	public static int run(String path)
	{	
		try
		{
			StorageRead sr = new FileStorageReader(path, false);
			TestStats ts = new TestStats(sr);
			ts.report(System.out);
			System.out.println("Quick statistics report done");
			return BatchUtil.SUCCESS;
		}
		catch (Exception ex)
		{
			System.out.println("Error: execution problem while generating the quick statistics report");
			ex.printStackTrace(System.err);
			return BatchUtil.ERR_EXEC;
		}
	}
}
