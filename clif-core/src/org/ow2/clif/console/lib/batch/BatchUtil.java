/*
* CLIF is a Load Injection Framework
* Copyright (C) 2005, 2006 France Telecom
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
* CLIF $Name: not supported by cvs2svn $
*
* Contact: clif@ow2.org
*/

package org.ow2.clif.console.lib.batch;

import org.objectweb.fractal.api.Component;
import org.ow2.clif.deploy.ClifAppFacade;
import org.ow2.clif.deploy.ClifRegistry;

/**
 * Static methods used by all batch commands.
 * 
 * This class also defines a set of code return for each
 * kind of error.
 * 
 * @author Joan Chaumont
 * @author Bruno Dillenseger
 */
public class BatchUtil
{
	/** Fractal ADL definition file of the CLIF application to deploy */
	static final String CLIF_APPLICATION = "org.ow2.clif.console.lib.batch.ClifApp";

	// status codes of command execution

	/** success status code */
	static public final int SUCCESS = 0; 

	/** error status code for missing args */
	static public final int ERR_ARGS = -1;

	/** error status code for execution problem */
	static public final int ERR_EXEC = -2;

	/** error status code for lifecycle violation */
	static public final int ERR_LIFECYCLE = -3;

	/** error status code for an undeployed test plan */
	static public final int ERR_DEPLOY = -4;

	/** error status code for missing registry */
	static public final int ERR_REGISTRY = -5;

	/** error status code for a problem creating a shared code server */
	static public final int ERR_CODESERVER = -6;

	static private final String[] ERR_MESSAGES = {
		"OK",
		"invalid arguments",
		"execution error",
		"lifecycle violation attempt",
		"test plan is not deployed",
		"Registry is unreachable",
		"shared code server error"};

    /**
	 * Return a clifApplication object with a clifApp component. This component
	 * is find by his name in the ClifRegistry bindings.
	 * 
	 * @param name the name of the clifApp component
	 * @return a new clifApplication made with clifApp component
	 */
    static ClifAppFacade getClifAppFacade (String name)
    {
        ClifRegistry clifReg;
        Component clifApp = null;
        try {
            clifReg = new ClifRegistry(false);
            clifApp =  clifReg.lookupClifApp(name);
            if (clifApp == null)
            {
                return null;
            }
        } catch (Throwable e) {
            e.printStackTrace(System.err);
            System.exit(ERR_REGISTRY);
        }
        return new ClifAppFacade(clifApp, name);
    }


    /**
	 * Splits a string of blade identifiers separated by : character into an
	 * array of individual blade identifier strings. The source string is taken
	 * from an array of strings at a given index. Returns null if the given
	 * index is greater than the array size.
	 * 
	 * @param args the array of strings containing the string of blade identifiers to split into substrings
	 * @param index the index of the target string in the given array
	 * @return an array of blade identifiers if the size of the source array is compatible with the given index,
	 * or null if the index is out of the source array bounds. 
	 */
	static String[] getSelBlades(String[] args, int index)
	{
		return args.length > index ? args[index].split(":") : null;
	}


	/**
	 * Usage method for incorrect args
	 * 
	 * @param message usage help message
	 */
	static void usage(String cmd)
	{
		System.out.println(cmd);
		System.exit(ERR_ARGS);
	}


	/**
	 * Gets a message describing the provided command execution status code
	 * @param statusCode the command execution status code
	 * @return the status message
	 */
	static String getMessage(int statusCode)
	{
		return ERR_MESSAGES[-statusCode];
	}
}
