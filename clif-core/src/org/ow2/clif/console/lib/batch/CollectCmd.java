/*
* CLIF is a Load Injection Framework
* Copyright (C) 2005, 2006 France Telecom
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
* CLIF $Name: not supported by cvs2svn $
*
* Contact: clif@ow2.org
*/

package org.ow2.clif.console.lib.batch;

import org.ow2.clif.deploy.ClifAppFacade;
import org.ow2.clif.util.ExecutionContext;

/**
 * Batch command to collect results of a test plan. The test plan must be
 * running or terminated (i.e. stopped, completed or aborted state). If the test
 * plan is running, then collection will wait for its termination before
 * occuring (in conformance with the blade lifecycle definition).
 * 
 * @author Joan Chaumont
 * @author Bruno Dillenseger
 */
public class CollectCmd
{
	/**
	 * @see #run(String, String[])
	 * @param args args[0] is the name of the target test plan,
	 * optional args[1] is a list of identifiers of blades to be collected, separated with : character
	 * (bladeId1:bladeId2:...bladeId3).
	 */
	public static void main(String[] args)
	{
		if (System.getSecurityManager() == null)
		{
			System.setSecurityManager(new SecurityManager());
		}
		if (args.length < 1)
		{
			BatchUtil.usage("arguments expected: <test plan name> [<bladeId1:bladeId2:...bladeIdn>]");
		}
		ExecutionContext.init("./");
		System.exit(run(args[0], BatchUtil.getSelBlades(args, 1)));
	}


	/**
	 * @param testPlanName the name of the target initialized test plan
	 * @param bladeIds array of blade identifiers that must be started in the given test plan. If null,
	 * all blades are started.
	 * @return command status code (@see BatchUtil)
	 */
	public static int run(String testPlanName, String[] bladeIds)
	{	
		try
		{
			ClifAppFacade clifApp = BatchUtil.getClifAppFacade(testPlanName);
			System.out.println("Waiting for test termination...");
			clifApp.join(bladeIds);
			System.out.println("Collecting data from test plan " + testPlanName + "...");
			int res = clifApp.collect(bladeIds, null);
			if (res == BatchUtil.ERR_LIFECYCLE)
			{
				System.err.println("Error: blades are not stopped nor completed");
				System.exit(BatchUtil.ERR_LIFECYCLE);
			}
			System.out.println("Collection done");
			return BatchUtil.SUCCESS;
		}
		catch (Exception ex)
		{
			System.err.println("Error: execution problem while collecting data from blades");
			ex.printStackTrace(System.err);
			return BatchUtil.ERR_EXEC;
		}
	}
}
