/*
* CLIF is a Load Injection Framework
* Copyright (C) 2003, 2004 France Telecom R&D
* Copyright (C) 2003 INRIA
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
* CLIF $Name: not supported by cvs2svn $
*
* Contact: clif@ow2.org
*/

package org.ow2.clif.datacollector.api;


/**
 * @author Julien Buret
 * @author Bruno Dillenseger
 */
public interface DataCollectorAdmin
{
	public final String DATA_COLLECTOR_ADMIN = "Data collector administration";


	/**
	 * Get latest blade statistics
	 * @return the latest blade statistics in the form of an array of integer values.
	 * @see #getLabels()
	 */
	public long[] getStat();


	/**
	 * Get labels describing the statistics
	 * @return the statistics labels, in the same order as the values returned by getStat()
	 * @see #getStat()
	 */
	public String[] getLabels();
}
