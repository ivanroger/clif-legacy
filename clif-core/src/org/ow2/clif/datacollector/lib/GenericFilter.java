/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * CLIF
 *
 * Contact: clif@ow2.org
 */
package org.ow2.clif.datacollector.lib;

import org.ow2.clif.datacollector.api.DataCollectorFilter;
import org.ow2.clif.storage.api.ActionEvent;
import org.ow2.clif.storage.api.AlarmEvent;
import org.ow2.clif.storage.api.LifeCycleEvent;
import org.ow2.clif.storage.api.ProbeEvent;

/**
 * @author Emmanuel Varoquaux
 */
public class GenericFilter implements DataCollectorFilter {
	private static final long serialVersionUID = -8354193368431682968L;
	private final boolean	acceptActionEvents;
	private final boolean	acceptAlarmEvents;
	private final boolean	acceptLifeCycleEvents;
	private final boolean	acceptProbeEvents;

	public GenericFilter(boolean acceptActionEvents, boolean acceptAlarmEvents,
			boolean acceptLifeCycleEvents, boolean acceptProbeEvents) {
		this.acceptActionEvents = acceptActionEvents;
		this.acceptAlarmEvents = acceptAlarmEvents;
		this.acceptLifeCycleEvents = acceptLifeCycleEvents;
		this.acceptProbeEvents = acceptProbeEvents;
	}

	public boolean accept(ActionEvent action) {
		return acceptActionEvents;
	}

	public boolean accept(AlarmEvent alarm) {
		return acceptAlarmEvents;
	}

	public boolean accept(LifeCycleEvent event) {
		return acceptLifeCycleEvents;
	}

	public boolean accept(ProbeEvent measure) {
		return acceptProbeEvents;
	}
}
