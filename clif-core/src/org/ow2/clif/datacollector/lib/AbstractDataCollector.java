/*
* CLIF is a Load Injection Framework
* Copyright (C) 2003,2004,2010 France Telecom R&D
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
* Contact: clif@ow2.org
*/

package org.ow2.clif.datacollector.lib;

import java.io.Serializable;
import org.objectweb.fractal.api.control.BindingController;
import org.ow2.clif.datacollector.api.DataCollectorAdmin;
import org.ow2.clif.datacollector.api.DataCollectorFilter;
import org.ow2.clif.datacollector.api.DataCollectorWrite;
import org.ow2.clif.storage.api.ActionEvent;
import org.ow2.clif.storage.api.AlarmEvent;
import org.ow2.clif.storage.api.LifeCycleEvent;
import org.ow2.clif.storage.api.ProbeEvent;
import org.ow2.clif.storage.api.StorageWrite;
import org.ow2.clif.supervisor.api.ClifException;

/**
 * Abstract implementation of a Data Collector component.
 * @author Bruno Dillenseger
 */
abstract public class AbstractDataCollector
	implements
		DataCollectorWrite,
		DataCollectorAdmin,
		BindingController
{
	static final String[] interfaceNames = new String[] { StorageWrite.STORAGE_WRITE };

	protected StorageWrite sws;
	private DataCollectorFilter filter = null;


	public AbstractDataCollector()
	{
	}


	/////////////////////////////////
	// interface BindingController //
	/////////////////////////////////


	public Object lookupFc(String clientItfName)
	{
		if (clientItfName.equals(StorageWrite.STORAGE_WRITE))
		{
			return sws;
		}
		else
		{
			return null;
		}
	}


	public void bindFc(String clientItfName, Object serverItf)
	{
		if (clientItfName.equals(StorageWrite.STORAGE_WRITE))
		{
			sws = (StorageWrite) serverItf;
		}
	}


	public void unbindFc(String clientItfName)
	{
		if (clientItfName.equals(StorageWrite.STORAGE_WRITE))
		{
			sws = null;
		}
	}


	public String[] listFc()
	{
		return sws == null ? new String[0] : interfaceNames;
	}


	//////////////////////////////////
	// interface DataCollectorWrite //
	//////////////////////////////////


	/**
	 * Does nothing.
	 * @param testId test identifier
	 * @param bladeId blade identifier
	 */
	public void init(Serializable testId, String bladeId)
	{
	}


	/**
	 * Does nothing.
	 */
	public void terminate()
	{
	}


	/**
	 * Add a new lifecycle event - simply forwards it to the Storage Proxy component
	 * unless it is rejected by current DataCollectorFilter.
	 * @param event new lifecycle event
	 */
	public void add(LifeCycleEvent event)
	{
		try
		{
			if (filter != null && !filter.accept(event))
			{
				return;
			}
		}
		catch (Exception ex)
		{
			return;
		}
		try
		{
			sws.write(event);
		}
		catch (ClifException ex)
		{
			ex.printStackTrace(System.err);
		};
	}


	/**
	 * Add a new measure - simply transmit it to the Storage Proxy component
	 * unless it is rejected by current DataCollectorFilter.
	 * @param action the new action event
	 */
	public void add(ActionEvent action)
	{
		try
		{
			if (filter != null && !filter.accept(action))
			{
				return;
			}
		}
		catch (Exception ex)
		{
			return;
		}
		try
		{
			sws.write(action);
		}
		catch (ClifException ex)
		{
			ex.printStackTrace(System.err);
		};
	}


	/**
	 * Add a new alarm - simply transmit it to the Storage Proxy component
	 * unless it is rejected by current DataCollectorFilter.
	 * @param alarm new alarm event
	 */
	public void add(AlarmEvent alarm)
	{
		try
		{
			if (filter != null && !filter.accept(alarm))
			{
				return;
			}
		}
		catch (Exception ex)
		{
			return;
		}
		try
		{
			sws.write(alarm);
		}
		catch (ClifException ex)
		{
			ex.printStackTrace(System.err);
		}
	}


	/**
	 * Add a new measure - simply transmit it to the Storage Proxy component
	 * unless it is null or it is rejected by current DataCollectorFilter.
	 * @param measure the new measure
	 */
	public void add(ProbeEvent measure)
	{
		try
		{
			if (measure == null || filter != null && !filter.accept(measure))
			{
				return;
			}
		}
		catch (Exception ex)
		{
			return;
		}
		if (measure != null)
		{
			try
			{
				sws.write(measure);
			}
			catch (ClifException ex)
			{
				ex.printStackTrace(System.err);
			}
		}
	}

	
	/////////////////////////////////////////////////////////
	// DataCollectorAdmin interface partial implementation //
	/////////////////////////////////////////////////////////
	
	
	public void setFilter(DataCollectorFilter filter)
	{
		this.filter = filter;
	}
}
