/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2005, 2011 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * CLIF
 *
 * Contact: clif@ow2.org
 */
package org.ow2.clif.server.util;

import java.io.Serializable;
import java.util.Map;
import org.ow2.clif.scenario.isac.util.BooleanHolder;
import org.ow2.clif.supervisor.api.ClifException;

/**
 * This class provides helpful utilities to manage the storage of events.
 * 
 * Each type of event have a storage state, which tells whether or not this
 * type of event has to be stored (via the StorageProxy). This storage state is a boolean, held by a
 * BooleanHolder.
 * 
 * @see BooleanHolder
 * @author Emmanuel Varoquaux
 * @author Bruno Dillenseger
 */
public class EventStorageState {

	/**
	 * Utility for GetCurrentParameters(). Puts the storage states of each
	 * type of event into the parameters Map.
	 * 
	 * @param parameters the parameter Map
	 * @param eventStorageStatesMap a Map between parameter names and BooleanHolders. 
	 */
	public static void putEventStorageStates(
		Map<String,Serializable> parameters,
		Map<String,BooleanHolder> eventStorageStatesMap)
	{
		for (Map.Entry<String,BooleanHolder> entry : eventStorageStatesMap.entrySet())
		{
			parameters.put(entry.getKey(), entry.getValue().getBooleanValue() ? "true" : "false");
		}
	}

	/**
	 * Utility for changeParameter(). Tries to set a BooleanHolder from a parameter.
	 * 
	 * @param eventStorageStatesMap a Map between parameter names and BooleanHolders.
	 * @param parameter the name of the parameter.
	 * @param value the value of the parameter.
	 * @return <code>true</code> in case of success (a BooleanHolder has been set),
	 * <code>false</code> otherwise.
	 */
	public static boolean setEventStorageState(
		Map<String,BooleanHolder> eventStorageStatesMap,
		String parameter,
		Serializable value)
	throws ClifException
	{
		if (!(value instanceof String))
			return false;
		for (Map.Entry<String,BooleanHolder> entry : eventStorageStatesMap.entrySet())
		{
			if (parameter.equals(entry.getKey())) {
				entry.getValue().setBooleanValue((String)value);
				return true;
			}
		}
		return false;
	}
}
