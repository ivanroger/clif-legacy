/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2011-2014 France Telecom R&D
 * Copyright (C) 2016-2017 Orange SA
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * Contact: clif@ow2.org
 */

package org.ow2.clif.analyze.lib.report;


import java.io.File;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import org.jdom.Attribute;
import org.jdom.Content;
import org.jdom.Element;
import org.jfree.chart.JFreeChart;
import org.ow2.clif.analyze.lib.report.Dataset.ChartType;
import org.ow2.clif.analyze.lib.report.Report.imageFormats;


/**
 * @author Tomas Perez Segovia
 * @author Bruno Dillenseger
 */
public class Section {

	public enum ChartRepresentation {
		TIME_BASED, HISTOGRAM, QUANTILE;
	};

	protected String title;
	protected String comment;
	protected int sectionId;
	protected int sectionOrder;	// page number // section order
	private static int nextSectionId = 1; 
	//	private int slicesNb;
	//	private int quantilesNb;
	//	private chartRepresentation representation ;
	private Chart chart;
	protected ArrayList<Dataset> datasets = new ArrayList<Dataset>();
	//	private long drawStartTime;
	//	private long drawEndTime;


	// constructors
	public Section(int _order, String _title, String _comment)
	{
		sectionId = generateId();
		sectionOrder = _order;
		this.setTitle("Section " + String.valueOf(sectionId));
		this.setComment(Report.EMPTY_COMMENT);
	}

	public Section() {
		this(nextSectionId, "Section " + nextSectionId, "<empty section comment>");
	}


	public Section(int _order) {
		this(_order, "Section " + nextSectionId, "<empty section comment>");
	}

	public Section(String _title) {
		this(nextSectionId, _title, "<empty section comment>");
	}

	public Section(String _title, String _comment) {
		this(nextSectionId, _title, _comment);
	}


	// getters and setters


	/**
	 * @return section's title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param sectionTitle
	 */
	public void setTitle(String sectionTitle) {
		this.title = sectionTitle;
	}

	/**
	 * @return returns section's comment
	 */
	public String getComment() {
		return comment;
	}

	/**
	 * @param sectionComment
	 */
	public void setComment(String sectionComment) {
		this.comment = sectionComment;
	}


	public int getSectionOrder() {
		return sectionOrder;
	}

	public void setSectionOrder(int sectionOrder) {
		this.sectionOrder = sectionOrder;
	}

	public int getSectionId() {
		return sectionId;
	}

	public void setSectionId(int currentId) {
		this.sectionId = currentId;
	}

	public Chart getChart()
	{
		return chart;
	}

	public void setChart(Chart chart)
	{
		this.chart = chart;
	}

	public ArrayList<Dataset> getDatasets() {
		return datasets;
	}

	public void setDatasets(ArrayList<Dataset> datasets) {
		this.datasets = datasets;
	}


	public void setChartRepresentation(ChartRepresentation representation)
	{
		if (representation != null && chart == null)
		{
			chart = new Chart(
				this,
				title,
				representation,
				-1,
				-1,
				20,
				20);
		}
		if (null != chart)
		{
			chart.setRepresentation(representation);
		}
		for (Dataset dataset : getDatasets())
		{
			if (dataset.isPerformDraw())
			{
				switch (representation)
				{
					case TIME_BASED:
						dataset.setPerformTimeChart(true);
						dataset.setPerformHistogram(false);
						dataset.setPerformQuantiles(false);
						break;
					case HISTOGRAM:
						dataset.setPerformTimeChart(false);
						dataset.setPerformHistogram(true);
						dataset.setPerformQuantiles(false);
						break;
					case QUANTILE:
						dataset.setPerformTimeChart(false);
						dataset.setPerformHistogram(false);
						dataset.setPerformQuantiles(true);
						break;
				}
			}
		}
	}

	public ChartRepresentation getChartRepresentation() {
		if (null != chart){
			return chart.getRepresentation();
		}
		return null;
	}


	public long getDrawStartTime() {
		long time = -1;
		if (chart != null)
		{
			time = chart.getStartTime();
		}
		return time;
	}


	public void setDrawStartTime(long l) {
		if (chart != null)
		{
			chart.setStartTime(l);
		}
	}


	public long getDrawEndTime() {
		long time = -1;
		if (chart != null)
		{
			time = chart.getEndTime();
		}
		return time;
	}


	public void setDrawEndTime(long l) {
		if (chart != null)
		{
			chart.setEndTime(l);
		}
	}


	/**
	 * if not set, computes (min) chart's (draw) start time from all dataset's drawStartTime
	 * and stores it in chart's data
	 */
	public void updateDrawStartTime()
	{
		if (datasets.size() > 0 && chart != null)
		{
			if (chart.getStartTime() == -1)
			{
				setDrawStartTime(chart.getDefaultStartTime());
			}
		}
	}


	/**
	 * if not set, computes (max) chart's (draw) end time from all dataset's drawStartTime
	 * and stores it in chart's data
	 */
	public void updateDrawEndTime()
	{
		if (datasets.size() > 0 && chart != null)
		{
			setDrawEndTime(chart.getDefaultEndTime());
		}
	}


	//	other methods

	private int generateId(){
		int id = nextSectionId  ++;
		return id;
	}


	public boolean addDatasetToSection(Dataset dataset) {
		boolean ret = datasets.add(dataset);
		if (dataset.isPerformDraw())
		{
			ChartRepresentation chartType = ChartRepresentation.TIME_BASED;
			if (dataset.isPerformQuantiles())
			{
				chartType = ChartRepresentation.QUANTILE;
			}
			else if (dataset.isPerformHistogram())
			{
				chartType = ChartRepresentation.HISTOGRAM;
			}
			chart = new Chart(
				this,
				title,
				chartType,
				-1,
				-1,
				dataset.getSlicesNb(),
				dataset.getQuantiles());
		}
		return ret;
	}


	public boolean removeDataset(Dataset dataset){
		if (datasets.contains(dataset)){
			datasets.remove(dataset);
			return true;
		}
		return false;
	}

	//  chart related methods

	// create a new chart

	public void updateChart() {
		if (chart != null)
		{
			chart.clearChartDataValues();
			for(Dataset dataset : datasets)
			{
				if (dataset.isPerformDraw()){  // don't draw if datasetperformDraw is not set.
					List<ChartDataValues> dataValList = dataset.createChartDataValues();
					chart.addChartDataValues(dataValList);
				}
			}
			chart.generateGraphics();
		}
	}


	public JFreeChart getJfChart() {
		return chart == null ? null : chart.getJfChart();
	}


	// other methods


	public boolean addDatasetModel(Dataset dataset){
		return addDatasetToSection(dataset);
	}

	public boolean addSimpleDataset (String testName, String bladeId, String eventType){
		return addDatasetToSection(new SimpleDataset(testName, bladeId, eventType));
	}

	public boolean addMultipleDataset (String testName, String bladeId, String eventType){
		return addDatasetToSection(new MultipleDataset(testName, bladeId, eventType));
	}

	public boolean addAggregateDataset (String testName, String bladeId, String eventType){
		return addDatasetToSection(new AggregateDataset(testName, bladeId, eventType));
	}

	public String toString() {
		String ret = title;
		//		String ret = "  - " + title + " - " + comment + "\n";
		//		for(Dataset dataset : this.datasets){
		//			ret += dataset.toString();
		//		}
		return ret;
	}

	public String toc(){
		String ret = title +" TOC: ";
		String sep = "";
		ret += "[";
		for(Dataset dataset : datasets){
			ret += sep + dataset.getName();
			sep = ", ";
		}
		return ret + "]";
	}


	public String exportToHtml(int tabsMem, File resourceFolder, imageFormats imageFormat) {
		StringBuffer htmlText = new StringBuffer("");
		LogAndDebug.htmlWriteNP(htmlText, "<h2>" + title + "</h2>");
		LogAndDebug.htmlWriteNP(htmlText, LogAndDebug.comment2html(comment));
		// datasets
		switch (datasets.size()){
		case 0:
			LogAndDebug.htmlWrite(htmlText, "No dataset.");
			break;
		case 1:
			LogAndDebug.htmlWrite(htmlText, "1 dataset.");
			break;
		default:
			LogAndDebug.htmlWrite(htmlText, datasets.size() + " datasets.");
		}
		for(Dataset dataset : datasets)
		{
			LogAndDebug.htmlWriteNP(htmlText, "<section class=\"dataset\">");
			LogAndDebug.htmlTabsp();
			htmlText.append(dataset.exportToHtml(tabsMem+1));
			LogAndDebug.htmlTabsm();
			LogAndDebug.htmlWriteNP(htmlText, "</section>");
		}
		LogAndDebug.htmlWriteNP(htmlText, "<section class=\"chart\">");
		LogAndDebug.htmlTabsp();
		if (chart == null)
		{
			LogAndDebug.htmlWriteNP(htmlText, "<span class=\"notice\">No chart.</span>");
		}
		else
		{
			if (resourceFolder.canWrite() || resourceFolder.mkdir())
			{
				htmlText.append(chart.exportToHtml(tabsMem+1, resourceFolder, title , imageFormat));
			}
			else
			{
				htmlText.append("<span class=\"error\">Error: could not write to directory " + resourceFolder + " for generating charts.</span>");
			}
		}
		LogAndDebug.htmlTabsm();
		LogAndDebug.htmlWriteNP(htmlText, "</section><hr>");
		return htmlText.toString();
	}

/*
 * Tomas version
 * 
	public StringBuffer exportToXml(int nbTabs, File mainFile, imageFormats imageFormat) {
		String tabs = "";
		for (int i = 0 ; i<= nbTabs;i++){
			tabs += "\t";
		}
		StringBuffer text = new StringBuffer(tabs + "Section:\n");

		text.append(tabs + "  title:          \"" + title + "\"\n");
		text.append(tabs + "  comment:        \"" + comment + "\"\n");
		text.append(tabs + "  sectionId:      "   + sectionId + "\n");
		text.append(tabs + "  sectionOrder:   "   + sectionOrder + "\n");
		switch (chart.getRepresentation()){
		case TIME_BASED:
			text.append(tabs + "  TIME_BASED\n");
			break;
		case HISTOGRAM:
			text.append("HISTOGRAM "+ chart.getNumberOfSlices() + " slices.");
			break;
		case QUANTILE:
			text.append("QUANTILE "+ chart.getNumberOfQuantils() + " quantiles.");
			break;
		case BOXPLOT:
			text.append("BOXPLOT");
			break;
		default:
			;
		}

		text.append(tabs + "  "+ datasets.size()+" datasets:\n");
		for(Dataset dataset : datasets){
			text.append(dataset.exportToTxt(nbTabs+1));
		}

		text.append(tabs + "  chart:\n");

		String mainFileName = mainFile.getName();
		File chartDirectory = new File(
				mainFile.getParentFile(),
				mainFileName.substring(0, mainFileName.lastIndexOf('.')));
		if (chartDirectory.canWrite() || chartDirectory.mkdir())
		{
			text.append(chart.exportToXml(nbTabs+1, chartDirectory, title , imageFormat));
		}
		else
		{
			text.append("Error: could not write to directory " + chartDirectory + " for generating charts.");
		}

		return text;
	}
*/
	
	public StringBuffer exportToTxt(int nbTabs, File mainFile, imageFormats imageFormat) {
		String tabs = "";
		for (int i = 0 ; i<= nbTabs;i++){
			tabs += "\t";
		}
		StringBuffer text = new StringBuffer(tabs + "Section:\n");

		text.append(tabs + "  title:          \"" + title + "\"\n");
		text.append(tabs + "  comment:        \"" + comment + "\"\n");
		text.append(tabs + "  sectionId:      "   + sectionId + "\n");
		text.append(tabs + "  sectionOrder:   "   + sectionOrder + "\n");
		text.append(tabs + "  "+ datasets.size()+" datasets:\n");
		for(Dataset dataset : datasets)
		{
			text.append(dataset.exportToTxt(nbTabs+1));
		}
		if (chart == null)
		{
			text.append("Notice: no chart for this section.");
		}
		else
		{
			switch (chart.getRepresentation()){
			case TIME_BASED:
				text.append(tabs + "  TIME_BASED\n");
				break;
			case HISTOGRAM:
				text.append(tabs + "  HISTOGRAM with "+ chart.getNumberOfSlices() + " slices.\n");
				break;
			case QUANTILE:
				text.append(tabs + "  QUANTILE with "+ chart.getNumberOfQuantils() + " quantiles.\n");
				break;
			}
			text.append(tabs + "  chart: " + mainFile + "\n");
			chartExport(nbTabs, mainFile, imageFormat, text);
		}
		return text;
	}

	private void chartExport(int nbTabs, File mainFile, imageFormats imageFormat,
			StringBuffer text) {
		if (chart != null)
		{
			String mainFileName = mainFile.getName();
			File chartDirectory = new File(
					mainFile.getParentFile(),
					mainFileName.substring(0, mainFileName.lastIndexOf('.')));
			if (chartDirectory.canWrite() || chartDirectory.mkdir())
			{
				text.append(chart.exportToTxt(nbTabs+1, chartDirectory, title , imageFormat));
			}
			else
			{
				text.append("Error: could not write to directory " + chartDirectory + " for generating charts.");
			}
		}
	}


	public int compareToByOrder(Section _sectionB){
		return sectionOrder - _sectionB.getSectionOrder();
	}


	public Set<ChartType> getChartTypes(){
		Set<ChartType>  ret = new HashSet<ChartType>();
		for(Dataset dataset : datasets){
			Set<ChartType> current = dataset.getChartTypes();
			ret.addAll(current);
		}
		return ret;
	}


	public String getEventFieldLabel() {
		String eventFieldLabel = "";
		boolean error = false;
		for(Dataset dataset : datasets){
			String field = dataset.getEventFieldLabel();
			if (eventFieldLabel.equals("")){
				eventFieldLabel = field;
			}else if (! eventFieldLabel.equals(field)){
				error = true;
				LogAndDebug.trace("ERROR Section.getEventFieldLabel: different fields found "+
						eventFieldLabel + " and " + field + ".");
			}
		}
		if (error){
			return null;
		}
		return eventFieldLabel;
	}


	public long getFilterStartTime() {
		long ret = -1 ;
		if (null != datasets){
			for(Dataset dataset : datasets){
				if (-1 == ret){
					ret = dataset.getFilterStartTime();
				}else{
					ret = Math.min(ret, dataset.getFilterStartTime());
				}
			}
		}
		return ret;
	}


	public long getFilterEndTime() {
		long ret = -1 ;
		if (null != datasets){
			for(Dataset dataset : datasets){
				if (-1 == ret){
					ret = dataset.getFilterEndTime();
				}else{
					ret = Math.max(ret, dataset.getFilterEndTime());
				}
			}
		}
		return ret;
	}



	public String getYAxisLabel() {
		return "YAxisLabel";
	}

	public List<Statistics> getStatistics() {
		List<Statistics>  ret = new ArrayList<Statistics>();
		for (Dataset dataset : datasets){
			ret.addAll(dataset.getStatistics());
		}
		return ret;
	}

	public int getId() {
		return sectionId;
	}

	public void setSlicesNb(int i) {
		if (chart != null)
		{
			chart.setNumberOfSlices(i);
		}
	}

	public int getSlicesNb() {
		int slices = 20;
		if (chart != null)
		{
			slices = chart.getNumberOfSlices();
		}
		return slices;
	}

	public int getQuantilesNb() {
		int quantiles = 20;
		if (chart != null)
		{
			quantiles = chart.getNumberOfQuantils();
		}
		return quantiles;
	}

	public void setQuantilesNb(int i) {
		if (chart != null)
		{
			chart.setNumberOfQuantils(i);
		}
	}

	public StringBuffer dump(int nbTabs) {
		String tabs = "";
		for (int i = 0 ; i< nbTabs;i++){
			tabs += "\t";
		}
		StringBuffer text = new StringBuffer (tabs + "Section\n");
		text.append(tabs + "  title: \"" + title + "\"\n");
		text.append(tabs + "  comment: \"" + comment + "\"\n");
		text.append(tabs + "  sectionId: " + sectionId + "\n");
		text.append(tabs + "  sectionOrder: " + sectionOrder + "\n");
		text.append(tabs + "  slicesNb: " + getSlicesNb() + "\n");
		text.append(tabs + "  quantilesNb: " + getQuantilesNb() + "\n");
		text.append(tabs + "  representation: " + getChartRepresentation() + "\n");
		if (chart == null)
		{
			text.append(tabs + "  no chart.\n");
		}
		else
		{
			text.append(tabs + "  chart:\n" + chart.dump());
		}
		text.append(tabs + datasets.size()+" datasets:\n");
		for(Dataset dataset : datasets){
			text.append(dataset.dump());
		}
		return text;
	}


	public Content saveToXml(int i) {

		int nbTabs = 0;

		Element section =new Element("section");		
		Element titleXml =new Element("title");
		Element commentXml =new Element("comment");

		section.addContent(titleXml);
		section.addContent(commentXml);
	//	chart.exportToXML(nbTabs, chartDirectory, title , imageFormat)

		for(Dataset dat : getDatasets()){	
			Content newSection= dat.saveMacro(nbTabs+1);		
			section.addContent(newSection);			
		}
		
		
		return section;
	}


	public Content saveMacro(int i) {
		Element section =new Element("section");		
		Element titleXml =new Element("title");
		Element commentXml =new Element("comment");


		// chart type attributes	
		Element chartType ;
		Attribute SlicesNumber;

		section.addContent(titleXml);
		section.addContent(commentXml);

		// add dataset element  
		int nbTabs = 0;

		
	
		// to correct writing //simple|multiple|aggregate
		for(Dataset dat : getDatasets()){
			// no graph selected
			if (!dat.isPerformDraw()){
				chartType = new Element("nochart");
			}
			
			// graph selected
			else if (chart.representation.name().equals("TIME_BASED")){
				chartType = new Element("timechart");	
				
			}
			else if (chart.representation.name().equals("HISTOGRAM")){
				chartType = new Element("histogram");
				SlicesNumber=new Attribute("slices", String.valueOf(getSlicesNb()));	
				chartType.setAttribute(SlicesNumber);
			}
			else if (chart.representation.name().equals("QUANTILE")){
				chartType =new Element("quantiles");
				SlicesNumber=new Attribute("slices", String.valueOf(getQuantilesNb()));	
				chartType.setAttribute(SlicesNumber);
			}		
			else {
				chartType = new Element("null") ;
			}
			section.addContent(chartType);	

			Content newSection= dat.saveMacro(nbTabs+1);		
			section.addContent(newSection);			
		}

		return section;
	}

	public Content saveReportToXML(int i) {

		Element section =new Element("section");

		Element titleXml =new Element("title");
		titleXml.setText(this.title);

		Element commentXml =new Element("comment");
		if (Report.EMPTY_COMMENT.equals(comment)){
		}
		else {commentXml.setText(this.comment);}


		// chart type attributes	
		Element chartType ;
		Attribute SlicesNumber;

		section.addContent(titleXml);
		section.addContent(commentXml);

		// add dataset element  
		int nbTabs = 0;

		// to correct writing
		for(Dataset dat : getDatasets()){
			// no graph selected
			if (!dat.isPerformDraw()){
				chartType = new Element("nochart");
			}
			// graph selected
			else if (chart.representation.name().equals("TIME_BASED")){
				chartType = new Element("timechart");	

				Attribute beginAttribut = new Attribute("begin",String.valueOf(getFilterStartTime()));
				chartType.setAttribute(beginAttribut);

				Attribute endAttribut = new Attribute("end",String.valueOf(getFilterEndTime()));
				chartType.setAttribute(endAttribut);

			}
			else if (chart.representation.name().equals("HISTOGRAM")){
				chartType = new Element("histogram");
				SlicesNumber=new Attribute("slices", String.valueOf(getSlicesNb()));	
				chartType.setAttribute(SlicesNumber);
			}
			else if (chart.representation.name().equals("QUANTILE")){
				chartType =new Element("quantiles");
				SlicesNumber=new Attribute("slices", String.valueOf(getQuantilesNb()));	
				chartType.setAttribute(SlicesNumber);
			}		
			else {
				chartType = new Element("null") ;
			}
			section.addContent(chartType);	

			Content newDataset= dat.saveToXML(nbTabs+1);		
			section.addContent(newDataset);			
		}

		return section;

	}
	
	
	private void chartExportXml(int nbTabs, File mainFile, imageFormats imageFormat ) {
		String mainFileName = mainFile.getName();
		File chartDirectory = new File(
				mainFile.getParentFile(),
				mainFileName.substring(0, mainFileName.lastIndexOf('.')));
		if (chart != null)
		{
			if (chartDirectory.canWrite() || chartDirectory.mkdir())
			{
				chart.exportToXML(nbTabs+1, chartDirectory, title , imageFormat);
			}
			else
			{
			System.out.println	("Error: could not write to directory " + chartDirectory + " for generating charts.");
			}
		}
	}
	
	
	public void loadReport(Element sectionLoded) {
		long startTime = -1;
		long endTime = -1;
		int numberOfSlices=10; 
		int numberOfQuantiles=10;
		Element rootload = sectionLoded;

		if (rootload==null){
			System.out.println("empty file");
		}
		else {
			List<?> elements = sectionLoded.getChildren();
			Iterator<?> i = elements.iterator();		      
			while(i.hasNext())
			{									
				Element courant = (Element)i.next();
				String name = courant.getName();
				if(name=="title")
				{
					setTitle( courant.getValue());        	 		        	  
				}
				else if (name=="comment"){
					setComment(courant.getValue());
				}
				else if(name=="dataset"){
					Dataset dataset = new Dataset();
					dataset.loadReport(courant);					
					this.addDatasetToSection(dataset);
					updateDrawStartTime();
					updateDrawEndTime();
				}
				else if(name=="timechart")
				{
					this.chart = new Chart(this, title, ChartRepresentation.TIME_BASED, startTime,
							endTime, numberOfSlices, numberOfQuantiles);
						System.out.println( "timechart"+ (courant.getValue()));						
				}
				else if(name=="nochart"){	
					// TODO : nochart XML implement
					
					System.out.println("No chart");
				}
				
				else if(name=="histogram"){	
					String representation = courant.getValue();
					System.out.println("representation : " +representation);
					this.chart = new Chart(this, title, ChartRepresentation.HISTOGRAM, startTime,
							endTime, numberOfSlices, numberOfQuantiles);
					
					// TODO : histogram XML implement					
					
				}
				else if(name=="quantiles"){					
					// TODO : quantiles XML implement
				}
				else {
					System.out.println("Not section element");
				}
				updateChart();
				nextSectionId ++;
			}
		}	

	}

	
	public String XMLToString(Element current){

		String elementName= current.getName();
		List<?> childrens = current.getChildren();

		Iterator<?> it= childrens.iterator();
		if(!it.hasNext()){
			return elementName;
		}
		else
		{	current =(Element)it.next() ;
		String nextName= XMLToString(current);

		while( it.hasNext())
		{
			nextName += ",";
			current =(Element)it.next() ;
			nextName += XMLToString(current);
		}
		return elementName + "(" + nextName + ")";
		}		

	}

	public Content saveReportToXML(int i, File mainFile,imageFormats imageFormat) {
		
		Element section =new Element("section");

		Element titleXml =new Element("title");
		titleXml.setText(this.title);

		Element commentXml =new Element("comment");
		if (Report.EMPTY_COMMENT.equals(comment)){
		}
		else {commentXml.setText(this.comment);}


		// chart type attributes	
		Element chartType ;
		Attribute SlicesNumber;

		section.addContent(titleXml);
		section.addContent(commentXml);

		// add dataset element  
		int nbTabs = 0;

		// to correct writing
		for(Dataset dat : getDatasets()){
			// no graph selected
			if (!dat.isPerformDraw()){
				chartType = new Element("nochart");
			}
			// graph selected
			else if (chart.representation.name().equals("TIME_BASED")){
				chartType = new Element("timechart");	

				Attribute beginAttribut = new Attribute("begin",String.valueOf(getFilterStartTime()));
				chartType.setAttribute(beginAttribut);

				Attribute endAttribut = new Attribute("end",String.valueOf(getFilterEndTime()));
				chartType.setAttribute(endAttribut);	
				
				chartExportXml( nbTabs,  mainFile,  imageFormat ) ;
				
			}
			else if (chart.representation.name().equals("HISTOGRAM")){
				chartType = new Element("histogram");
				SlicesNumber=new Attribute("slices", String.valueOf(getSlicesNb()));	
				chartType.setAttribute(SlicesNumber);
		//		chartExportXml( nbTabs,  mainFile,  imageFormat ) ;
			}
			else if (chart.representation.name().equals("QUANTILE")){
				chartType =new Element("quantiles");
				SlicesNumber=new Attribute("slices", String.valueOf(getQuantilesNb()));	
				chartType.setAttribute(SlicesNumber);
		//		chartExportXml( nbTabs,  mainFile,  imageFormat ) ;
			}		
			else {
				chartType = new Element("null") ;
			}
			section.addContent(chartType);	

			Content newDataset= dat.saveToXML(nbTabs+1);		
			section.addContent(newDataset);			
		}

		return section;

	}

	public Content saveReportToXML(int i, imageFormats imageFormat) {
		// TODO Auto-generated method stub
		return null;
	}


}

class SectionComparatorByOrder implements Comparator<Object>{

	public int compare(Object sA, Object sB){
		int a = ((Section)sA).getSectionOrder();        
		int b = ((Section)sB).getSectionOrder();

		LogAndDebug.trace("  comparing section's order " + a + " to " + b);
		if(a > b){
			LogAndDebug.trace(" after.");
			return 1;
		}else if(a < b){
			LogAndDebug.trace(" before.");
			return -1;
		}else{
			LogAndDebug.trace(" equal.");
			return 0;
		}
	}
}	

