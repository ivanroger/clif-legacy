/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2011-2014 France Telecom R&D
 * Copyright (C) 2017 Orange SA
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * Contact: clif@ow2.org
 */

package org.ow2.clif.analyze.lib.report;

import java.util.Iterator;
import java.util.List;
import org.jdom.Attribute;
import org.jdom.Element;
import org.ow2.clif.analyze.lib.report.Dataset.DatasetType;
import org.ow2.clif.analyze.lib.report.Report.imageFormats;


public class Datasource  {
	protected String testName  = null;
	protected String bladeId   = null;
	protected String eventType = null;
	protected String field     = null;
	protected Dataset dataset  = null;
	protected String alias = null;

	Statistics statistics = null; 	// used only for  MULTIPLE_DATASET 
	// 	SIMPLE_DATASET and AGGREGATE_DATASET statistics are stored in Dataset


	// constructors
	public Datasource(String testName, String bladeId, String eventType,
			String field, Dataset dataset) {
		this.testName = testName;
		this.bladeId = bladeId;
		this.eventType =  eventType;
		this.field = field;
		this.dataset = dataset;
	}



	public Datasource(String testName, String bladeId, String eventType, String field) {
		this(testName, bladeId, eventType, field, null);
	}


	public Datasource(String _testName, String _bladeId, String _eventType) {
		this(_testName, _bladeId, _eventType, (String) null);
	}

	public Datasource(String _testName, String _bladeId) {
		this(_testName, _bladeId, (String) null);
	}

	public Datasource(String _testName) {
		this(_testName, "undefinedBladeId");
	}

	public Datasource() {
		this("undefinedTestName");
	}

	// getters and setters


	/**
	 * @return the testName
	 */
	public String getTestName() {
		return testName;
	}

	/**
	 * @param testName the testName to set
	 */
	public void setTestName(String testName) {
		this.testName = testName;
	}

	/**
	 * @return the bladeId
	 */
	public String getBladeId() {
		return bladeId;
	}

	/**
	 * @param bladeId the bladeId to set
	 */
	public void setBladeId(String bladeId) {
		this.bladeId = bladeId;
	}

	/**
	 * @return the eventType
	 */
	public String getEventType() {
		return eventType;
	}

	/**
	 * @param eventType the eventType to set
	 */
	public void setEventType(String eventType) {
		this.eventType = eventType;
	}

	/**
	 * @return the field
	 */
	public String getField() {
		return field;
	}

	/**
	 * @param field the field to set
	 */
	public void setField(String field) {
		this.field = field;
	}

	public Statistics getStatistics()
	{
		return statistics;
	}

	public void setStatistics(Statistics statistics)
	{
		this.statistics = statistics;
	}


	//	other methods
	public void ExportToHtml(){
		LogAndDebug.unimplemented();
	}

	public void ExportToXml(){
		LogAndDebug.unimplemented();
	}

	public void ExportToTxt(){
		LogAndDebug.unimplemented();
	}

	public void ExportRoutineToXml(){
		LogAndDebug.unimplemented();
	}

	public String exportToHtml(int tabsMem) {
		StringBuffer htmlText;
		htmlText = new StringBuffer("");

		// datasource definition
		LogAndDebug.htmlWriteNP(htmlText, "<table class=\"datasource\">");
		LogAndDebug.htmlTabsp();
		LogAndDebug.htmlWriteNP(htmlText, "<caption>Datasource " + getName() + "</caption>");
		LogAndDebug.htmlWriteNP(htmlText, "<tr><th>Test Name</th><th>Blade Id</th><th>Event Type</th><th>Field</th></tr>");
		LogAndDebug.htmlWriteNP(htmlText, "<tr><td>" + testName  + "</td><td>" + bladeId + "</td><td>" + eventType + "</td><td>" + field + "</td></tr>");
		LogAndDebug.htmlTabsm();
		LogAndDebug.htmlWriteNP(htmlText, "</table>");
		// statistical analysis
		if (null != statistics)
		{
			htmlText.append(statistics.exportToHtml(tabsMem));
		}
		else if (dataset.getDatasetType().equals(DatasetType.SIMPLE_DATASET))
		{
			htmlText.append(dataset.getStatistics().get(0).exportToHtml(tabsMem));
		}
		return htmlText.toString();
	}


	public String exportToTxt(int nbTabs) {
		String tabs = "";
		for (int i = 0 ; i<= nbTabs;i++){
			tabs += "\t";
		}
		String text = new String();
		text += tabs + "datasource:\n";
		text += tabs + "  test name: "  + this.getTestName()  + "\n";
		text += tabs + "  blade id: "   + this.getBladeId()   + "\n";
		text += tabs + "  event type: " + this.getEventType() + "\n";
		text += tabs + "  field: "      + this.getField()     + "\n";
		if (null != statistics){
			text += statistics.exportToTxt(nbTabs+1);
		}
		return text;
	}


	public String getName()
	{
		if (alias == null)
		{
			return getCanonicalName();
		}
		else
		{
			return alias;
		}
	}


	public String getCanonicalName()
	{
		return getTestName() + "/" + getBladeId() + ":" + getEventType() + "." + getField();
	}


	public String getAlias()
	{
		return alias;
	}


	public void setAlias(String _alias)
	{
		this.alias = _alias;
	}


	public Dataset getDataset() {
		return dataset;
	}



	public void setDataset(Dataset dataset) {
		this.dataset = dataset;
	}


	public StringBuffer dump() {
		StringBuffer text = new StringBuffer("");
		String tabs = "\t\t\t\t";

		text.append(tabs + "test name: \""  + this.getTestName()  + "\"\n");
		text.append(tabs + "blade id: \""   + this.getBladeId()   + "\"\n");
		text.append(tabs + "event type: \"" + this.getEventType() + "\"\n");
		text.append(tabs + "field: \""      + this.getField()     + "\"\n");
		if (null != statistics){
			text.append(tabs + statistics.dump());
		}
		return text;
	}


	public int getOriginalEventsNumber() {
		return statistics.getOriginalEventsNumber();
	}


	public int getFinalEventsNumber() {
		return statistics.getFinalEventsNumber();
	}


	public Element saveToXML(int i) {
		Element datasrc = new Element("source");
		Attribute testNameAttribute=new Attribute("test",this.getTestName());
		datasrc.setAttribute(testNameAttribute);
		Attribute bladeNameAttribute=new Attribute("blade",this.getBladeId());
		datasrc.setAttribute(bladeNameAttribute);
		Attribute aliasNameAttribute=new Attribute("alias",this.getName());
		datasrc.setAttribute(aliasNameAttribute);
		return datasrc;
	}

	public void loadReport(Element datasourceLoaded){		
		Element rootload =datasourceLoaded;

		String bladeAttribut;
		String testAttribut;
		String aliasAttribut;

		if (rootload==null){
			System.out.println("empty datasource file");
		}
		else if (rootload!=null){
			List<?> attributes = rootload.getAttributes();
			Iterator<?> j = attributes.iterator();
			while(j.hasNext())
			{
				Attribute courantAttribute = (Attribute)j.next();
				String nameAttribute = courantAttribute.getName();
				System.out.println("____datasource nameAttribute "+nameAttribute);
		//		courantAttribute=rootload.getAttribute("test");	
				
				if(nameAttribute==("test")){			
					testAttribut=courantAttribute.getValue();	
					this.testName=testAttribut;
					System.out.println("   datasource testAttribut "+testAttribut);
				}
				else if(nameAttribute==("blade")){
					 bladeAttribut=courantAttribute.getValue();
						System.out.println("   datasource bladeAttribut "+bladeAttribut);
				}
				else if(nameAttribute==("alias")){
					 aliasAttribut=courantAttribute.getValue();
						System.out.println("   datasource aliasAttribut "+aliasAttribut);
				}
				else {
					System.out.println("datasource : " +nameAttribute+" is not a datasource attribute");
				}
			}


			List<?> elements = rootload.getChildren();
			Iterator<?> i = elements.iterator();		      
			while(i.hasNext())
			{	
				Element datasourceCourant = (Element)i.next();
				String datasourceElementName = datasourceCourant.getName();

				if(datasourceElementName=="source")
				{ 
					System.out.println("datasource element  : " +XMLToString(datasourceCourant));
				}
				else{
					System.out.println("NOT datasource   element  : " +XMLToString(datasourceCourant));
				}
				
			}
		}
	}

	public String XMLToString(Element current){

		String elementName= current.getName();
		List<?> childrens = current.getChildren();

		Iterator<?> it= childrens.iterator();
		if(!it.hasNext()){
			return elementName;
		}
		else
		{	current =(Element)it.next() ;
		String nextName= XMLToString(current);

		while( it.hasNext())
		{
			nextName += ",";
			current =(Element)it.next() ;
			nextName += XMLToString(current);
		}
		return elementName + "(" + nextName + ")";
		}		

	}

	
	public Element saveToXML(int i, imageFormats imageFormat) {
		// TODO Auto-generated method stub
		return null;
	}


	public Object exportToXml(int i) {
		// TODO Auto-generated method stub
		return null;
	}
}