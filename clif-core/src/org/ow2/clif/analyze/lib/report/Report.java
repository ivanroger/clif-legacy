/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2011-2014 France Telecom R&D
 * Copyright (C) 2016-2017 Orange SA
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * Contact: clif@ow2.org
 */

package org.ow2.clif.analyze.lib.report;


import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import org.jdom.output.Format;
import org.jdom.output.XMLOutputter;
import org.jdom.Content;
import org.jdom.Document;
import org.jdom.Element;


/**
 * @author Tomas Perez Segovia
 * @author Bruno Dillenseger
 */

public class Report {
	static final String EMPTY_COMMENT = "No comment";

	public enum exportFormats {
		HTML_FORMAT, XML_FORMAT, TXT_FORMAT;
	}

	public enum imageFormats {
		PNG_FORMAT, JPG_FORMAT, SVG_FORMAT;
	}

	protected String title;
	protected String comment;
	protected exportFormats exportFormat = exportFormats.HTML_FORMAT;
	protected imageFormats  imageFormat = imageFormats.PNG_FORMAT;

	protected List<Section> sections = new ArrayList<Section>();

	// constructors

	/**
	 * @param _title : report's title
	 * @param _comment : report's general comment
	 */
	public Report(String _title, String _comment) {
		LogAndDebug.tracep("(\"" + _title + ", \"" + _comment + "\")");
		this.title = _title;
		this.comment = _comment;
		LogAndDebug.tracem("(\"" + _title + ", \"" + _comment + "\")");
	}


	public Report(String reportTitle) {
		this(reportTitle, EMPTY_COMMENT);
	}

	public Report() {
		this("CLIF Report");
	}



	// getters and setters

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}
	
	public String getTitleMacro() {
		return title;
	}

	public void setTitleMacro(String title) {
		this.title = title;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public exportFormats getExportFormat() {
		return exportFormat;
	}


	public void setExportFormat(exportFormats exportFormat) {
		LogAndDebug.trace("("+ exportFormat +")");
		this.exportFormat = exportFormat;
	}


	public imageFormats getImageFormat() {
		return imageFormat;
	}


	public void setImageFormat(imageFormats imageFormat) {
		LogAndDebug.trace("("+ imageFormat +")");
		this.imageFormat = imageFormat;
	}


	// other methods

	public String toString() {
		String ret = title;
		return ret;
	}


	public String toc(){	// Table Of Contents
		String ret = "- " + this.getTitle() +" TOC: ";
		String sep = "";
		ret += "[";
		for(Section st : getSections()){
			ret += sep + st.toc();
			sep = ", ";
		}
		return ret + "]";
	}


	public Section getSectionByTitle(String title) {
		for(Section st : getSections()){
			if (title == st.getTitle()){
				return st;
			}
		}
		return null;
	}


	public String exportToHtml(int indent, File resourceFolder){
		StringBuffer htmlText = new StringBuffer("");
		LogAndDebug.htmlTabs(indent);
		LogAndDebug.htmlWriteNP(htmlText, "<h1>" + title + "</h1>");
		LogAndDebug.htmlWrite(htmlText, LogAndDebug.comment2html(comment));
		LogAndDebug.htmlWrite(htmlText, getSections().size() + " sections:");
		LogAndDebug.htmlWriteNP(htmlText, "<hr>");		
		for(Section sec : getSections()){
			htmlText.append(sec.exportToHtml(LogAndDebug.htmlTabsMem, resourceFolder, imageFormat));
		}
		LogAndDebug.htmlWrite(htmlText, "<span class=\"notice\">End of report.</span>");

		return htmlText.toString();
	}


	public String saveMacroXml() {
		// for now generates a macro description. modify to generate XML
		String stringResult="essai string";
		int nbTabs = 0;

		Element root =new Element("report");

		// document JDOM
		Document document = new Document(root);

		Element titleXml =new Element("title");
		Element commentXml =new Element("comment");
		root.addContent(titleXml);
		root.addContent(commentXml);

		for(Section sec : getSections()){	
			Content newSection= sec.saveMacro(nbTabs+1);		
			root.addContent(newSection);			
		}

		XMLOutputter sortie = new XMLOutputter(Format.getPrettyFormat());
		stringResult=sortie.outputString(document);		
		return (stringResult);
	}
	

	public String exportToTxt(File mainFile){
		String tabs = "";
		int nbTabs = 0;
		for (int i = 0 ; i<= nbTabs;i++){
			tabs += "\t";
		}
		StringBuffer text = new StringBuffer("CLIF text Report\n"); 
		text.append(tabs + "  title: \"" + title + "\"\n"); 
		text.append(tabs + "  comment: \"" + comment + "\"\n");

		text.append(tabs + "  exportFormat: \"" + exportFormat + "\"\n");
		text.append(tabs + "  imageFormat:  \"" + imageFormat + "\"\n");

		for(Section sec : getSections()){
			text.append(sec.exportToTxt(nbTabs+1, mainFile, imageFormat));
		}
		return text.toString();
	}


	// if section[order] exists, return it; create a new one otherwise
	// returned section.order may be renumbered to have a continuous ordering
	private Section addSection(int _order) {
		LogAndDebug.tracep("(" + _order + ")");
		Section section;
		if (sections.contains(_order)){
			section = getSectionByOrder(_order);
		}else{
			LogAndDebug.trace(" adding section " + _order + "...");
			section = new Section(_order);
			addSectionToReport(section);
			Collections.sort(sections, new SectionComparatorByOrder());
			renumberSections();
		}
		LogAndDebug.trace(" added section("+ section.getSectionOrder()+ ").");
		LogAndDebug.tracem("(" + _order + ")");
		return section;
	}


	private void addSectionToReport(Section section) {
		LogAndDebug.tracep("(" + section + ")");
		sections.add(section);
		LogAndDebug.tracem("(" + section + ")");
	}


	// add dataset to report (in a new section)
	public Section addDatasetToReport(Dataset _dataset) {
		LogAndDebug.tracep("(" + LogAndDebug.toText(_dataset) + ")");
		Section section = addSection(sections.size()+1);
		section.addDatasetToSection(_dataset);
		LogAndDebug.tracem("(" + LogAndDebug.toText(_dataset) + ")");
		return section;
	}

	// add dataset to section(order). If section(order) does not exists, it is created.
	// returned section.order may be renumbered to have a continuous ordering
	public Section addDataset(Dataset _dataset, int _order) {
		LogAndDebug.tracep("(" + LogAndDebug.toText(_dataset) + ", " + _order + ")");
		LogAndDebug.trace(" Report: adding dataset \"" + _dataset.getName() + "\" to section(" + _order + ")");
		Section section = addSection(_order);
		section.addDatasetToSection(_dataset);
		LogAndDebug.tracem("(" + LogAndDebug.toText(_dataset) + ", " + _order + ")");
		return section;
	}

	// renumbers sections to have a continuous ordering
	private void renumberSections() {
		LogAndDebug.tracep();
		LogAndDebug.trace(" renumbering sections");
		String msg  = " order: [";
		String pre = "";
		for(Section section : getSections()){
			msg +=pre + section.getSectionOrder();
			pre = ", ";
		}
		int i = 1;		// section(order) starts at 1 !!
		for(Section section : getSections()){
			section.setSectionOrder(i++);
		}
		msg += "] -> [";
		pre = "";
		for(Section section : getSections()){
			msg += pre + section.getSectionOrder();
			pre = ", ";
		}
		LogAndDebug.trace(msg + "]");
		LogAndDebug.tracem();
	}

	public boolean removeSection(Section sec) {
		LogAndDebug.tracep("("+LogAndDebug.toText(sec)+")");
		LogAndDebug.trace("  report has "+sections.size()+" sections.");
		boolean ret = sections.remove(sec);
		LogAndDebug.trace("  report has now "+sections.size()+" sections.");
		LogAndDebug.tracem("("+LogAndDebug.toText(sec)+")");
		return ret;
	}



	//	public void removeSection(int _order){
	//		LogAndDebug.tracep("(" + _order + ")");
	//		for(Section section : getSections()){
	//			if (_order == section.getSectionOrder()){
	//				LogAndDebug.trace(" removing section (" + _order + ")");
	//				sections.remove(section);
	//				break;
	//			}
	//			renumberSections();
	//		}
	//		LogAndDebug.tracem("(" + _order + ")");
	//	}


	public List<Section> getSections() {
		return sections;
	}


	public void setSectionsList(ArrayList<Section> sections) {
		this.sections = sections;
	}


	private Section getSectionByOrder(int order) {
		for (Section sec : getSections()){
			if (order == sec.getSectionOrder()){
				return sec;
			}
		}
		return null;
	}


	public void saveRoutine() {
		LogAndDebug.unimplemented();
		// TODO Auto-generated method stub
	}


	public void loadRoutine() {
		LogAndDebug.unimplemented();
		// TODO Auto-generated method stub
	}


	public void removeDataset(int sectionId, int datasetId) {	// called by RMAnalysView
		LogAndDebug.unimplemented();
		// TODO Auto-generated method stub
	}


	public void clearAll() {
		sections.clear();
	}


	public void dump() {
		String tabs = "";
		int nbTabs = 1;
		for (int i = 0 ; i< nbTabs;i++){
			tabs += "\t";
		}
		StringBuffer text = new StringBuffer("CLIF text dump\n"); 
		text.append(tabs + "title: \"" + title + "\"\n"); 
		text.append(tabs + "comment: \"" + comment + "\"\n");

		text.append(tabs + "exportFormat: \"" + exportFormat + "\"\n");
		text.append(tabs + "imageFormat:  \"" + imageFormat + "\"\n");

		text.append(tabs + getSections().size() +" sections:\n");
		for(Section sec : getSections()){
			text.append(sec.dump(nbTabs+1));
		}
		System.out.println(text);
	}


	public void loadReport(Element root) {
		
		Element rootload =root;
		int order = 1;

		if (rootload==null){
			System.out.println("empty file");
		}		
		else {
			List<?> elements = root.getChildren();
			Iterator<?> i = elements.iterator();		      
			while(i.hasNext())
			{		    	  
				Element courant = (Element)i.next();
				String name = courant.getName();
				if(name=="title")
				{
					setTitle(courant.getValue()); 	        	 		        	  
				}
				else if (name=="comment"){
					setComment(courant.getValue());
				}
				else if(name=="section")
				{					
					String titleSection = "title Section";
					String commentSection = "comment Section";
					Section sec = new Section(order, titleSection, commentSection);
					sec.getDrawEndTime();
					sec.loadReport(courant);					
		//			addSection(order);		// 	ajoute des panel de graph :  mais si on mmet aussi la ligne suivante on a deux fois trop de panel§
					addSectionToReport(sec);
					Collections.sort(sections, new SectionComparatorByOrder());
					renumberSections();
					order++;
				}
				else {
					
				}

			}
		}	
	}

	public Report loadReportFromXLM(Element root) {
		Report rp=new Report();
		Element rootload =root;
		int order = 1;

		if (rootload==null){
			System.out.println("empty file");
		}		
		else {
			List<?> elements = root.getChildren();
			Iterator<?> i = elements.iterator();		      
			while(i.hasNext())
			{		    	  
				Element courant = (Element)i.next();
				String name = courant.getName();
				if(name=="title")
				{
					rp.title= courant.getValue(); 	        	 		        	  
				}
				else if (name=="comment"){
					rp.comment = courant.getValue();
				}
				else if(name=="section")
				{					
					String titleSection = "title Section";
					String commentSection = "comment Section";					
					Section sec = new Section(order, titleSection, commentSection);
					sec.loadReport(courant);					
		//			addSection(order);		// 	ajoute des panel de graph :  mais si on mmet aussi la ligne suivante on a deux fois trop de panel§
					rp.addSectionToReport(sec);
					order++;
				}
				else {
					
				}

			}
		}
		return rp;	
	}
	


	public void loadMacro(Element root) {
		// TODO Auto-generated method stub
		
	}


	public String exportXML(File mainFile) {
		int nbTabs = 0;
	String stringResult=" ";
	Element root =new Element("report");		
	org.jdom.Document document = new org.jdom.Document(root);

	Element titleXml =new Element("title");
	titleXml.setText(this.title);		
	root.addContent(titleXml);		

	// report commments		
	Element commentXml =new Element("comment");				

	if (Report.EMPTY_COMMENT.equals(comment)){
	}
	else {
		commentXml.setText(this.comment);
		}
	
	root.addContent(commentXml);


	for(Section sec : getSections()){		
		Content newSection= sec.saveReportToXML(nbTabs+1,mainFile, imageFormat);	
		root.addContent(newSection);	
	}
	

	XMLOutputter sortie = new XMLOutputter(Format.getPrettyFormat());
	stringResult=sortie.outputString(document);				
	return stringResult;
	}


	

}