/*
* CLIF is a Load Injection Framework
* Copyright (C) 2011-2014 France Telecom R&D
* Copyright (C) 2016-2017 Orange SA
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
* Contact: clif@ow2.org
*/
package org.ow2.clif.analyze.lib.report;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import org.jdom.Attribute;
import org.jdom.Content;
import org.jdom.Element;
import org.ow2.clif.analyze.lib.report.FieldsValuesFilter.LogicalEnum;
import org.ow2.clif.analyze.statistics.util.data.StatOnLongs;


/**
 *  Implementation of a dataset as viewed by the report generator
 *  @author Colette Vincent
 *  @author Bruno Dillenseger
 */
public class Dataset {
	static final String NO_FILTER = "No filter.";

	// Dataset Type
	public enum DatasetType {
		SIMPLE_DATASET, MULTIPLE_DATASET, AGGREGATE_DATASET;

		public String toText() {
			switch (this) {
			case SIMPLE_DATASET:
				return "Simple";
			case MULTIPLE_DATASET:
				return "Multiple";
			case AGGREGATE_DATASET:
				return "Aggregate";
			default:
				return "Unknown";
			}
		}
	}

	public enum ChartType{
		RAW, MOVING_MIN, MOVING_MAX, MOVING_AVERAGE, MOVING_MEDIAN,MOVING_STDDEV, MOVINGTHROUGHPUT}


	// Dataset Attributes
	protected DatasetType datasetType = DatasetType.SIMPLE_DATASET;
	private int datasetId = -1;
	private String initialName = "undefined initial name";
	private String name = "undefined name";
	private String comment = "<empty comment>";
	private Section section = null;

	protected List<Datasource> datasources = new ArrayList<Datasource>();

	// filtering options
	private boolean performStatAnalysis;

	private long filterStartTime;
	private long filterEndTime;
	private boolean startFilterAtFirstEvent;
	private boolean endFilterAtLastEvent;

	private FieldsValuesFilter fieldsValuesFilter;

	private double kParam;						// cleaning K value
	private double percentPointsKeptParam;		// cleaning nb points kept

	// draw options
	private boolean performDraw; 

	//representation (should be in Class "Chart")
	private boolean performTimeChart, performHistogram, performQuantile;

	// draw
	Set<ChartType> chartTypesSet = new HashSet<ChartType>();

	// - dataset options
	private int maxPointsNb;
	private int timeWindow;
	private int step;
	private int slicesNb;
	private int quantilesNb;


	// time/dates
	private long firstEventTime, lastEventTime;

	// statistics
	Statistics statistics = null; 	// used only for SIMPLE_DATASET and AGGREGATE_DATASET
	// when MULTIPLE_DATASET Statistics are stored in DataSource

	// other attributes
	private static int nextId = 1;


	////////////////////////////
	// constructors
	public Dataset(DatasetType dsType, String _name, String _comment){
		super();
		LogAndDebug.tracep("(" + dsType + ", \"" + _name + "\", \"" + _comment + "\")");
		datasetId = generateId();
		initialName = "dataset_" + String.valueOf(datasetId);

		datasetType = dsType;
		if ((null != name) || _name.equals("")){
			name = new String(initialName);
		}else{
			name = _name;
		}

		if ((null ==_comment) || _comment.equals("")){
			comment = Report.EMPTY_COMMENT;
		}else{
			comment = _comment;
		}

		// default options
		fieldsValuesFilter = new FieldsValuesFilter();
		performStatAnalysis = true;
		startFilterAtFirstEvent = true;
		endFilterAtLastEvent = true;
		performDraw = true;
		LogAndDebug.tracem("(" + dsType + ", \"" + _name + "\", \"" + _comment + "\")");
	}

	public Dataset(){
		this(DatasetType.SIMPLE_DATASET, null, null);
	}

	public Dataset(String _name){
		this(DatasetType.SIMPLE_DATASET, _name, null);
	}


	// AggregateDataset
	public Dataset(String _name, String _testName, String _bladeId){
		this(DatasetType.SIMPLE_DATASET, _name, null);
		Datasource ds = new Datasource(_testName, _bladeId, null, null, this);
		datasources.add(ds);
	}

	//SimpleDataset //MultipleDataset
	public Dataset(String _name, String _testName, String _bladeId, String _eventType){
		this(DatasetType.SIMPLE_DATASET, _name, null);
		Datasource ds = new Datasource(_testName, _bladeId, _eventType, null, this);
		datasources.add(ds);
	}

	public Dataset(DatasetType datasetType, String name, String testName,
			String bladeId, String eventType) {
		this(name, testName, bladeId, eventType);
		this.datasetType = datasetType;
	}


	public Dataset(String _name, String _testName, String _bladeId, String _eventType, String field){
		Datasource ds = new Datasource(_testName, _bladeId, _eventType, field, this);
		datasources.add(ds);
	}

	public Dataset(DatasetType dsType) {
		this(dsType, null, null);
	}

	////////////////////////////
	////////////////////////////
	////////////////////////////

	public DatasetType fromString(String st){
		if(0 ==(st.compareToIgnoreCase("Simple Dataset"))){
			return DatasetType.SIMPLE_DATASET;
		} else if(0 ==(st.compareToIgnoreCase("Simple"))){
			return DatasetType.SIMPLE_DATASET;
		} else if (0 ==(st.compareToIgnoreCase("Multiple Dataset"))){
			return DatasetType.MULTIPLE_DATASET;
		} else if (0 ==(st.compareToIgnoreCase("Multiple"))){
			return DatasetType.MULTIPLE_DATASET;
		} else if (0 ==(st.compareToIgnoreCase("Aggregate Dataset"))){
			return DatasetType.AGGREGATE_DATASET;
		} else if (0 ==(st.compareToIgnoreCase("Aggregate"))){
			return DatasetType.AGGREGATE_DATASET;
		} else {
			return DatasetType.SIMPLE_DATASET;
		}
	}


	////////////////////////////
	// getters and setters

	public int getId() {
		return datasetId;
	}


	public List<Datasource> getDatasources() {
		return datasources;
	}


	public Set<ChartType> getChartTypesSet() {
		return chartTypesSet;
	}


	public void setChartTypesSet(Set<ChartType> cts) {
		LogAndDebug.trace("(" + LogAndDebug.toText(cts) +")");
		this.chartTypesSet = cts;
	}


	public int getDatasetId() {
		return datasetId;
	}


	public void setId(int id) {
		this.datasetId = id;
	}


	public DatasetType getDatasetType() {
		return datasetType;
	}


	public void setDatasetType(DatasetType d) {
		LogAndDebug.trace("(" + d +")");
		datasetType = d;
	}

	public String getComment() {
		return comment;
	}


	public void setComment(String str) {
		LogAndDebug.trace("(\"" + str +"\")");
		comment = str;
	}


	public String getTitle() {
		return name;
	}


	public String getName() {
		return name;
	}

	public String getNames() {
		StringBuffer ret = new StringBuffer();
		String pre = "";
		String post = "";
		if (datasources.size() > 1){
			pre = "[";
			post = "";
		}
		for(Datasource ds : datasources){
			ret.append(pre + ds.getName());
			pre = ", ";
			post = "]";
		}
		ret.append(post);
		return ret.toString();
	}


	public void setName(String str) {
		LogAndDebug.trace("(\"" + str +"\")");
		name = str;
	}


	public String getInitialName() {
		return initialName;
	}


	public void setInitialName(String str) {
		initialName = str;
	}


	public boolean isPerformStatAnalysis() {
		return performStatAnalysis;
	}


	public void setPerformStatAnalysis(boolean b) {
		LogAndDebug.trace("(" + b +")");
		performStatAnalysis = b;
	}


	public long getFilterStartTime() {
		return filterStartTime;
	}


	public void setFilterStartTime(long l) {
		LogAndDebug.trace("(" + l +")");
		filterStartTime = l;
	}


	public boolean isStartFilterAtFirstEvent() {
		return startFilterAtFirstEvent;
	}


	public void setStartFilterAtFirstEvent(boolean b) {
		LogAndDebug.trace("(" + b +")");
		startFilterAtFirstEvent = b;
	}


	public long getFilterEndTime() {
		return filterEndTime;
	}


	public void setFilterEndTime(long l) {
		LogAndDebug.trace("(" + l +")");
		filterEndTime = l;
	}


	public boolean isEndFilterAtLastEvent() {
		return endFilterAtLastEvent;
	}


	public void setEndFilterAtLastEvent(boolean b) {
		LogAndDebug.trace("(" + b +")");
		endFilterAtLastEvent = b;
	}


	public double getkParam() {
		return kParam;
	}


	public void setkParam(double d) {
		LogAndDebug.trace("(" + d +")");
		kParam = d;
	}


	public double getPercentPointsKeptParam() {
		return percentPointsKeptParam;
	}


	public void setPercentPointsKeptParam(double d) {
		LogAndDebug.trace("(" + d +")");
		percentPointsKeptParam = d;
	}


	public boolean isPerformDraw() {
		return performDraw;
	}


	public void setPerformDraw(boolean b) {
		LogAndDebug.trace("(" + b +")");
		performDraw = b;
	}


	public boolean isPerformTimeChart() {
		return performTimeChart;
	}


	public void setPerformTimeChart(boolean b) {
		LogAndDebug.trace("(" + b +")");
		performTimeChart = b;
	}


	public boolean isPerformHistogram() {
		return performHistogram;
	}


	public void setPerformHistogram(boolean b) {
		LogAndDebug.trace("(" + b +")");
		performHistogram = b;
	}


	public boolean isPerformQuantiles() {
		return performQuantile;
	}


	public void setPerformQuantiles(boolean b) {
		LogAndDebug.trace("(" + b +")");
		performQuantile = b;
	}


	public boolean isDrawRaw(){
		return chartTypesSet.contains(ChartType.RAW);
	}


	public void setDrawRaw(boolean b) {
		LogAndDebug.trace("(" + b +")");
		if (b){	chartTypesSet.add(ChartType.RAW);
		}else{	chartTypesSet.remove(ChartType.RAW);}
	}


	public boolean isDrawMovingMin() {
		return chartTypesSet.contains(ChartType.MOVING_MIN);
	}


	public void setDrawMovingMin(boolean b) {
		LogAndDebug.trace("(" + b +")");
		if (b){	chartTypesSet.add(ChartType.MOVING_MIN);
		}else{	chartTypesSet.remove(ChartType.MOVING_MIN);}
	}


	public boolean isDrawMovingMax() {
		return chartTypesSet.contains(ChartType.MOVING_MAX);
	}


	public void setDrawMovingMax(boolean b) {
		LogAndDebug.trace("(" + b +")");
		if (b){	chartTypesSet.add(ChartType.MOVING_MAX);
		}else{	chartTypesSet.remove(ChartType.MOVING_MAX);}
	}


	public boolean isDrawMovingAverage() {
		return chartTypesSet.contains(ChartType.MOVING_AVERAGE);
	}


	public void setDrawMovingAverage(boolean b) {
		LogAndDebug.trace("(" + b +")");
		if (b){	chartTypesSet.add(ChartType.MOVING_AVERAGE);
		}else{	chartTypesSet.remove(ChartType.MOVING_AVERAGE);}
	}


	public boolean isDrawMovingMedian() {
		return chartTypesSet.contains(ChartType.MOVING_MEDIAN);
	}


	public void setDrawMovingMedian(boolean b) {
		LogAndDebug.trace("(" + b +")");
		if (b){	chartTypesSet.add(ChartType.MOVING_MEDIAN);
		}else{	chartTypesSet.remove(ChartType.MOVING_MEDIAN);}
	}


	public boolean isDrawMovingStdDev() {
		return chartTypesSet.contains(ChartType.MOVING_STDDEV);
	}


	public void setDrawMovingStdDev(boolean b) {
		LogAndDebug.trace("(" + b +")");
		if (b){	chartTypesSet.add(ChartType.MOVING_STDDEV);
		}else{	chartTypesSet.remove(ChartType.MOVING_STDDEV);}
	}


	public boolean isDrawMovingThroughput() {
		return chartTypesSet.contains(ChartType.MOVINGTHROUGHPUT);
	}


	public void setDrawMovingThroughput(boolean b) {
		LogAndDebug.trace("(" + b +")");
		if (b){	chartTypesSet.add(ChartType.MOVINGTHROUGHPUT);
		}else{	chartTypesSet.remove(ChartType.MOVINGTHROUGHPUT);}
	}


	public int getMaxPointsNb() {
		return maxPointsNb;
	}


	public void setMaxPointsNb(int i) {
		LogAndDebug.trace("(" + i +")");
		maxPointsNb = i;
	}


	public int getTimeWindow() {
		return timeWindow;
	}


	public void setTimeWindow(int i) {
		LogAndDebug.trace("(" + i +")");
		timeWindow = i;
	}


	public int getStep() {
		return step;
	}


	public void setStep(int i) {
		LogAndDebug.trace("(" + i +")");
		step = i;
	}


	public int getSlicesNb() {
		return slicesNb;
	}


	public void setSlicesNb(int i) {
		LogAndDebug.trace("(" + i +")");
		slicesNb = i;
	}


	public int getQuantiles()
	{
		return quantilesNb;
	}


	public void setQuantiles(int n)
	{
		quantilesNb = n;
	}


	public long getFirstEventTime() {
		return firstEventTime;
	}


	public void setFirstEventTime(long l) {
		LogAndDebug.trace("(" + l +")");
		firstEventTime = l;
	}


	public long getLastEventTime() {
		return lastEventTime;
	}


	public void setLastEventTime(long l) {
		LogAndDebug.trace("(" + l +")");
		lastEventTime = l;
	}


	public long getFirstEventTimeAfterFilter()
	{
		long result = -1;
		if (isStartFilterAtFirstEvent())
		{
			result = getFirstEventTime();
		}
		else if (getFilterStartTime() < getFirstEventTime())
		{
			result = getFirstEventTime();
		}
		else
		{
			result = getFilterStartTime();
		}
		return result;
	}


	public long getLastEventTimeAfterFilter()
	{
		long result = -1;
		if (isEndFilterAtLastEvent())
		{
			result = getLastEventTime();
		}
		else if (getFilterEndTime() > getLastEventTime())
		{
			result = getLastEventTime();
		}
		else
		{
			result = getFilterEndTime();
		}
		return result;
	}


	public void setStatistics(Statistics statistics)
	{
		LogAndDebug.tracep("(statistics)");
		switch (datasetType){
		case SIMPLE_DATASET:
		case AGGREGATE_DATASET:
			this.statistics = statistics;
			break;
		default:
			this.statistics = null;
			System.err.println("ERROR Dataset.setStatistics: for MULTIPLE_DATASET should call setStatistics(List<Statistics> _statistics)");
		}
		LogAndDebug.tracem("(statistics)");
	}



	public void setStatistics(List<Statistics> statistics)
	{
		LogAndDebug.tracep("(List<Statistics> statistics)");
		switch (datasetType){
		case SIMPLE_DATASET:
		case AGGREGATE_DATASET:
			if (1 == statistics.size()){
				this.statistics = statistics.get(0);
			}
			break;
		case MULTIPLE_DATASET:
			this.statistics = null;		// sould never acces
			int i = 0;
			for (Statistics stat : statistics){
				Datasource datasource = datasources.get(i++);
				datasource.setStatistics(stat);
			}
		default:
			this.statistics = null;
			System.err.println("ERROR Dataset.setStatistics: for MULTIPLE_DATASET shoould call setStatistics(List<Statistics> _statistics)");
		}
		LogAndDebug.tracem("(List<Statistics> statistics)");
	}


	public List<Statistics> getStatistics()
	{
		List<Statistics> ret = new ArrayList<Statistics>();
		switch (datasetType){
		case SIMPLE_DATASET:
		case AGGREGATE_DATASET:
			if (null == statistics){
				LogAndDebug.err("ERROR [Dataset]getStaistics():  Dataset.getStatistics: statistics have not been set.\n");
				ret = null;
			}else{
				ret.add(statistics);
			}
			break;

		case MULTIPLE_DATASET:
			for(Datasource datasource : getDatasources()){
				ret.add(datasource.getStatistics());
			}
			break;
		default:
			break;
		}
		return ret;
	}



	public FieldsValuesFilter getFieldsValuesFilter()
	{
		return fieldsValuesFilter;
	}


	public void setFieldsValuesFilter(FieldsValuesFilter fieldsValuesFilter)
	{
		LogAndDebug.trace("("+fieldsValuesFilter+ ") for " + LogAndDebug.toText(this));
		this.fieldsValuesFilter = fieldsValuesFilter;
	}


	public Section getSection() {
		return section;
	}


	public void setSection(Section section) {
		LogAndDebug.trace("(" + section +")");
		this.section = section;
	}




	// public methods

	/**
	 * adds <field> to all Datasources in this Dataset
	 * @param field
	 */
	public void addField(String field) {
		for(Datasource ds : getDatasources()){
			ds.setField(field);
		}
	}


	public String exportToHtml(int tabsMem) {
		StringBuffer htmlText = new StringBuffer("");
		LogAndDebug.htmlWriteNP(htmlText, "<h3>" + getName() + "</h3>");
		LogAndDebug.htmlWrite(htmlText, LogAndDebug.comment2html(comment));
		LogAndDebug.htmlWriteNP(htmlText, "<ul>");
		LogAndDebug.htmlTabsp();
		LogAndDebug.htmlWriteNP(htmlText, "<li>First sample time: " + firstEventTime + " ms.</li>");
		LogAndDebug.htmlWriteNP(htmlText, "<li>Last sample time: "  + lastEventTime  + " ms.</li>");
		LogAndDebug.htmlTabsm();
		LogAndDebug.htmlWriteNP(htmlText, "</ul>");
		// filters definition
		LogAndDebug.htmlWriteNP(htmlText, "<table class=\"filters\">");
		LogAndDebug.htmlTabsp();
		LogAndDebug.htmlWriteNP(htmlText, "<caption>Filters</caption>");
		String filterDescription;
		filterDescription = NO_FILTER;
		if (getFilterStartTime() != getFirstEventTime())
		{
			filterDescription = getFilterStartTime() + " ms";
		}
		LogAndDebug.htmlWriteNP(htmlText, "<tr><th>Start time</th><td>" + filterDescription + "</td></tr>");
		filterDescription =  NO_FILTER;
		if (getFilterEndTime() != getLastEventTime())
		{
			filterDescription = getFilterEndTime() + " ms";
		}
		LogAndDebug.htmlWriteNP(htmlText, "<tr><th>End time</th><td>" + filterDescription + "</td></tr>");
		filterDescription =  NO_FILTER;
		if (! getFieldsValuesFilter().isEmpty())
		{
			filterDescription = getFieldsValuesFilter().toString();
		}
		LogAndDebug.htmlWriteNP(htmlText, "<tr><th>Fields values</th><td>" + filterDescription + "</td></tr>");
// TODO		LogAndDebug.htmlWriteNP(htmlText, "<tr><th>Statistical cleaning</th><td>" + dataset.getStatisticalFilteringParamsToHtml() + "</td></tr>");
		LogAndDebug.htmlTabsm();
		LogAndDebug.htmlWriteNP(htmlText, "</table>");
	
		switch (datasources.size()){
		case 0:
			LogAndDebug.htmlWrite(htmlText, "This " + datasetType.toText() + " dataset has no datasource.");
			break;
		case 1:
			LogAndDebug.htmlWrite(htmlText, "This " + datasetType.toText() + " dataset has one datasource:");
			break;
		default:
			LogAndDebug.htmlWrite(htmlText, "This " + datasetType.toText() + " dataset has " + datasources.size() + " datasources:");
		}
		// datasources
		for(Datasource ds : getDatasources())
		{
			LogAndDebug.htmlWriteNP(htmlText, "<section class=\"datasource\">");
			LogAndDebug.htmlTabsp();
			htmlText.append(ds.exportToHtml(tabsMem+1));
			LogAndDebug.htmlTabsm();
			LogAndDebug.htmlWriteNP(htmlText, "</section>");
		}
		//	global statistics for aggregate dataset only
		if (getDatasetType().equals(DatasetType.AGGREGATE_DATASET))
		{
			LogAndDebug.htmlWriteNP(htmlText, "<section class=\"statistics\">");
			LogAndDebug.htmlTabsp();
			htmlText.append(statistics.exportToHtml(tabsMem));
			LogAndDebug.htmlTabsm();
			LogAndDebug.htmlWriteNP(htmlText, "</section>");
		}
		return htmlText.toString();
	}


/* not used, but could be useful...
	private int getOriginalEventsNumber() {
		int ret = 0;
		switch (datasetType){
		case SIMPLE_DATASET:
		case AGGREGATE_DATASET:
			ret = statistics.getOriginalEventsNumber();
			break;
		case MULTIPLE_DATASET:
		default:
			for(Datasource ds : datasources){
				ret += ds.getOriginalEventsNumber();
			}
		}
		return ret;
	}

	private int getFinalEventsNumber() {
		int ret = 0;
		switch (datasetType){
		case SIMPLE_DATASET:
		case AGGREGATE_DATASET:
			ret = statistics.getFinalEventsNumber();
			break;
		case MULTIPLE_DATASET:
		default:
			for(Datasource ds : datasources){
				ret += ds.getFinalEventsNumber();
			}
		}
		return ret;
	}
*/

	public Content saveToXML(int i) {
		Element dataset =new Element("dataset");

		Attribute graphTypeAttribut = new Attribute("type",this.getDatasetType().toText());		
		dataset.setAttribute(graphTypeAttribut);


		Element titleXml =new Element("title");
		titleXml.addContent(this.getTitle());
		dataset.addContent(titleXml);

		Element commentXml =new Element("comment");
		if (Report.EMPTY_COMMENT.equals(comment)){	
		}
		else {
			commentXml.addContent(this.getComment());
		}
		dataset.addContent(commentXml);


		Element chartXml =new Element("chart");

		int maxpoints = this.getMaxPointsNb();
		if (maxpoints!=-1){
			Element rawXml =new Element("raw");
			Attribute maxAttribute = new Attribute("max",String.valueOf(this.getMaxPointsNb()));
			rawXml.setAttribute(maxAttribute);			
			chartXml.addContent(rawXml);
		}

		Element movingXml =new Element("moving");
		String stats ="Draw : ";
		if (!this.isDrawRaw()){			
			stats=stats+ "no raw";
		}

		if (this.isDrawMovingAverage()){
			if (stats !="Draw : ") {stats=stats+"||";}
			stats=stats+"Moving average";
		}
		if (this.isDrawMovingMin()){
			if (stats !="Draw : ") {stats=stats+"||";}
			stats=stats+"Moving min";
		}
		if (this.isDrawMovingMax()){
			if (stats !="Draw : ") {stats=stats+"||";}
			stats=stats+"Moving Max";
		}
		if (this.isDrawMovingMedian()){
			if (stats !="Draw : ") {stats=stats+"||";}
			stats=stats+"Moving Median";
		}
		if (this.isDrawMovingStdDev()){
			if (stats !="Draw : ") {stats=stats+"||";}
			stats=stats+"Moving StandardDeviation";
		}
		if (this.isDrawMovingThroughput()){
			if (stats !="Draw : ") {stats=stats+"||";}
			stats=stats+"Moving Throughput";
		}

		if (stats !="Draw : "||stats.contentEquals("Draw : no raw")){
			movingXml.addContent(stats);}

		if (isDrawMovingAverage()||isDrawMovingMin()||isDrawMovingMax()||isDrawMovingMedian()||this.isDrawMovingThroughput()||this.isDrawMovingStdDev()){

			Element elementwindow =new Element("window");
			elementwindow.addContent(String.valueOf(this.getTimeWindow()));
			movingXml.addContent(elementwindow);

			Element elementstep =new Element("step");
			elementstep.addContent(String.valueOf(this.getStep()));
			movingXml.addContent(elementstep);
		}

		if (movingXml.getContentSize() != 0){
			chartXml.addContent(movingXml);	}			 

		//		if (!this.performDraw){
		//			dataset.removeContent(movingXml);
		//		}

		if (this.isDrawRaw()||chartXml.getContentSize()!=0){			
			dataset.addContent(chartXml);
		}


		Element events =new Element("event");
		Attribute typedAttribut=new Attribute("type",this.getEventType());
		events.setAttribute(typedAttribut);

		Attribute fieldAttribut=new Attribute("field",this.getFields());
		events.setAttribute(fieldAttribut);

		if (this.getFieldsValuesFilter() !=null){
			// List filterList + logicalOp	
			fieldsValuesFilter =this.getFieldsValuesFilter();
			int index=fieldsValuesFilter.getFilterList().size();

			
			String logicOperatordAttributString = fieldsValuesFilter.getLogicalOp().toString();
			if (logicOperatordAttributString=="AND_OP"){
				logicOperatordAttributString="and";
			}
			else if (logicOperatordAttributString=="OR_OP"){
				logicOperatordAttributString="or";
			}
			else{logicOperatordAttributString=null;}
			
			
			Element selector =new Element("selection_operator");
			Attribute logicOperatordAttribut=new Attribute("operator",logicOperatordAttributString);			
			selector.setAttribute(logicOperatordAttribut);
			
			for (int j=0; j<index ;j++)
			{	
				Element filter = new Element("filter");
				Attribute fieldAttributfilter=new Attribute("fieldFilter",this.getFields());
				filter.setAttribute(fieldAttributfilter);
				Attribute relationalOperatorAttribut=new Attribute("operator",fieldsValuesFilter.getFilterList().get(j).getOperator().toString());
				filter.setAttribute(relationalOperatorAttribut);
				Attribute valueAttribut=new Attribute("value",fieldsValuesFilter.getFilterList().get(j).getValue().toString());
				filter.setAttribute(valueAttribut);
				selector.addContent(filter);
			}

			events.addContent(selector);

		}	

		int nbTabs=0;	

		for(Datasource datasrc : getDatasources()){				
			Element newDatasource= datasrc.saveToXML(nbTabs+1);	
			if (datasetType.toText()=="Multiple"){			
				newDatasource.addContent(datasrc.statistics.statisticXml());	
			}
			events.addContent(newDatasource);
		}

		if (null != statistics){
			if (datasetType.toText()=="Simple"||datasetType.toText()=="Aggregate"){
				Element statistics =new Element("statistics");			
				statistics= statisticXml(dataset, nbTabs);
				events.addContent(statistics);	}
		}
		dataset.addContent(events);

		return dataset;
	}


	//	public Content saveToXML(int i, imageFormats imageFormat) {
	//		Element dataset =new Element("dataset");
	//
	//		Attribute graphTypeAttribut = new Attribute("type",this.getDatasetType().toText());		
	//		dataset.setAttribute(graphTypeAttribut);
	//
	//
	//		Element titleXml =new Element("title");
	//		titleXml.addContent(this.getTitle());
	//		dataset.addContent(titleXml);
	//
	//		Element commentXml =new Element("comment");
	//		if (EMPTY_DATASET_COMMENT.equals(comment)){	
	//		}
	//		else {
	//			commentXml.addContent(this.getComment());
	//		}
	//		dataset.addContent(commentXml);
	//
	//
	//		Element chartXml =new Element("chart");
	//
	//		int maxpoints = this.getMaxPointsNb();
	//		if (maxpoints!=-1){
	//			Element rawXml =new Element("raw");
	//			Attribute maxAttribute = new Attribute("max",String.valueOf(this.getMaxPointsNb()));
	//			rawXml.setAttribute(maxAttribute);			
	//			chartXml.addContent(rawXml);
	//		}
	//				
	//		Element movingXml =new Element("moving");
	//			String stats ="Draw : ";
	//			if (!this.isDrawRaw()){			
	//				stats=stats+ "no raw";
	//			}
	//				
	//			 if (this.isDrawMovingAverage()){
	//				 if (stats !="Draw : ") {stats=stats+"||";}
	//				stats=stats+"Moving average";
	//			}
	//			 if (this.isDrawMovingMin()){
	//				 if (stats !="Draw : ") {stats=stats+"||";}
	//				stats=stats+"Moving min";
	//			}
	//			 if (this.isDrawMovingMax()){
	//				 if (stats !="Draw : ") {stats=stats+"||";}
	//				stats=stats+"Moving Max";
	//			}
	//			 if (this.isDrawMovingMedian()){
	//				 if (stats !="Draw : ") {stats=stats+"||";}
	//				stats=stats+"Moving Median";
	//			}
	//			 if (this.isDrawMovingStdDev()){
	//				 if (stats !="Draw : ") {stats=stats+"||";}
	//				stats=stats+"Moving StandardDeviation";
	//			}
	//			 if (this.isDrawMovingThroughput()){
	//				 if (stats !="Draw : ") {stats=stats+"||";}
	//				stats=stats+"Moving Throughput";
	//			}
	//			
	//			if (stats !="Draw : "||stats.contentEquals("Draw : no raw")){
	//				movingXml.addContent(stats);}
	//				
	//			if (isDrawMovingAverage()||isDrawMovingMin()||isDrawMovingMax()||isDrawMovingMedian()||this.isDrawMovingThroughput()||this.isDrawMovingStdDev()){
	//	
	//				Element elementwindow =new Element("window");
	//				elementwindow.addContent(String.valueOf(this.getTimeWindow()));
	//				movingXml.addContent(elementwindow);
	//	
	//				Element elementstep =new Element("step");
	//				elementstep.addContent(String.valueOf(this.getStep()));
	//				movingXml.addContent(elementstep);
	//			}
	//
	//		if (movingXml.getContentSize() != 0){
	//			chartXml.addContent(movingXml);	}			 
	//
	////		if (!this.performDraw){
	////			dataset.removeContent(movingXml);
	////		}
	//		
	//		if (this.isDrawRaw()||chartXml.getContentSize()!=0){			
	//			dataset.addContent(chartXml);
	//		}
	//
	//		
	//		Element events =new Element("event");
	//		Attribute typedAttribut=new Attribute("type",this.getEventType());
	//		events.setAttribute(typedAttribut);
	//
	//		Attribute fieldAttribut=new Attribute("field",this.getFields());
	//		events.setAttribute(fieldAttribut);
	//
	//		Element selector =new Element("selection_operator");
	//		Attribute operatordAttribut=new Attribute("operator","an^^or");
	//		selector.setAttribute(operatordAttribut);	
	//
	//		Element filter = new Element("filter");
	//		Attribute fieldAttributfilter=new Attribute("fieldFilter",this.getFields());
	//		filter.setAttribute(fieldAttributfilter);
	//		Attribute operatorAttribut=new Attribute("operator","");
	//		filter.setAttribute(operatorAttribut);
	//		Attribute valueAttribut=new Attribute("value","");
	//		filter.setAttribute(valueAttribut);
	//		selector.addContent(filter);
	//		events.addContent(selector);		
	//
	//		int nbTabs=0;	
	//
	//		for(Datasource datasrc : getDatasources()){				
	//			Element newDatasource= datasrc.saveToXML(nbTabs+1);	
	//			//		Element newDatasource= datasrc.saveToXML(nbTabs+1,imageFormat);	
	//			if (datasetType.toText()=="Multiple"){			
	//				newDatasource.addContent(datasrc.statistics.statisticXml());	
	//			}
	//			events.addContent(newDatasource);
	//		}
	//
	//		if (null != statistics){
	//			if (datasetType.toText()=="Simple"||datasetType.toText()=="Aggregate"){
	//				Element statistics =new Element("statistics");			
	//				statistics= statisticXml(dataset, nbTabs);
	//				events.addContent(statistics);	}
	//		}
	//		dataset.addContent(events);
	//
	//		return dataset;
	//	}

	private Element statisticXml(Element dataset, int nbTabs) {

		if (datasetType.toText()=="Simple"||datasetType.toText()=="Aggregate"){
			Element statis =new Element("statistics");

			Element statsminimum = new Element("minimun");
			statsminimum.setText(String.valueOf(statistics.getMinimum()));
			statis.addContent(statsminimum);

			Element statsmaximum = new Element("maximum");
			statsmaximum.setText(String.valueOf(statistics.getMaximum()));
			statis.addContent(statsmaximum);

			Element statsmean = new Element("mean");
			statsmean.setText(String.valueOf(statistics.getAverage()));
			statis.addContent(statsmean);

			Element statsmedian = new Element("median");
			statsmedian.setText(String.valueOf(statistics.getMedian()));
			statis.addContent(statsmedian);

			Element statsstandardeviation = new Element("standarddeviation");
			statsstandardeviation.setText(String.valueOf(statistics.getStandardDeviation()));
			statis.addContent(statsstandardeviation);

			Element statspopulation = new Element("population");
			statspopulation.setText(String.valueOf(statistics.getOriginalEventsNumber()));
			statis.addContent(statspopulation);

			//			Element statsrejected = new Element("rejected");			
			//			int rejectedNumber=statistics.getOriginalEventsNumber()-statistics.getFinalEventsNumber();					
			//			statsrejected.setText(String.valueOf(rejectedNumber));					
			//			statis.addContent(statsrejected);

			Element statsthroughput = new Element("throughput");					
			statsthroughput.setText(String.valueOf(statistics.getThroughput())+ " (events per second)");				
			statis.addContent(statsthroughput);

			return statis;
		}			

		else {
			return null;				
		}

	}


	public String toString() {
		String ret = name;
		return ret;
	}


	private int generateId(){
		int id = nextId   ++;
		return id;
	}


	public Set<ChartType> getChartTypes(){
		return chartTypesSet;
	}

	public String getEventFieldLabel() {
		String eventFieldLabel = "";
		boolean error = false;
		for(Datasource ds : getDatasources()){
			String field = ds.getField();
			if (eventFieldLabel.equals("")){
				eventFieldLabel = field;
			}else if (! eventFieldLabel.equals(field)){
				error = true;
				System.err.println("ERROR Dataset: different fields found "+
						eventFieldLabel + " and " + field + ".");
			}
		}
		if (error){
			return null;
		}
		return eventFieldLabel;
	}

	public StringBuffer exportToTxt(int nbTabs) {
		String tabs = "";
		for (int i = 0 ; i<= nbTabs;i++){
			tabs += "\t";
		}
		StringBuffer text = new StringBuffer(tabs + "Dataset\n");
		text.append(tabs + "  name:    \"" + this.getName() + "\"\n");
		text.append(tabs + "  dataset type: " + this.getDatasetType().toText() + "\n");
		text.append(tabs + "  id:      " + this.getId() + "\n");
		text.append(tabs + "  comment: " + comment + "\n");
		text.append(tabs + "  first event time: " + firstEventTime + " ms\n");
		text.append(tabs + "  last Event time:  " + lastEventTime + " ms\n");
		text.append(tabs + "  datasources:\n");
		for(Datasource ds : getDatasources()){
			text.append(ds.exportToTxt(nbTabs+1));
		}
		text.append(tabs + "  statistics:\n");
		if (null != statistics){
			text.append(statistics.exportToTxt(nbTabs+1));
		}

		// filtering options
		text.append(tabs + "  performStatAnalysis:    " + performStatAnalysis + "\n");
		text.append(tabs + "  filterStartTime: " + filterStartTime + " ms.\n");
		text.append(tabs + "  filterEndTime:   " + filterEndTime + " ms.\n");
		text.append(tabs + "  startFilterAtFirstEvent:  " + startFilterAtFirstEvent + "\n");
		text.append(tabs + "  endFilterAtLastEvent:     " + endFilterAtLastEvent + "\n");
		text.append(tabs + "  fieldsValuesFilter: \n");
		text.append(fieldsValuesFilter.exportToTXT(nbTabs+1));
		text.append(tabs + "  kParam:                 " + kParam + "\n");
		text.append(tabs + "  percentPointsKeptParam: " + percentPointsKeptParam + "\n");
		text.append(tabs + "  performDraw:    " + performDraw + "\n");
		text.append(tabs + "  performTimeChart: " + performTimeChart + "\n");
		text.append(tabs + "  performHistogram: " + performHistogram + "\n");
		text.append(tabs + "  performQuantile:  " + performQuantile + "\n");
		text.append(tabs + "  chartTypesSet: " + chartTypesSet + "\n");
		text.append(tabs + "  maxPointsNb: " + maxPointsNb + "\n");
		text.append(tabs + "  timeWindow:  " + timeWindow + " ms.\n");
		text.append(tabs + "  step:        " + step + " ms.\n");
		text.append(tabs + "  slicesNb:    " + slicesNb + "\n");
		text.append(tabs + "  firstEventTime: " + firstEventTime + " ms.\n");
		text.append(tabs + "  lastEventTime:  " + lastEventTime + " ms.\n");
		text.append(tabs + "  nextId: " + nextId + "\n");

		return text;
	}

	public List<ChartDataValues> createChartDataValues() {
		LogAndDebug.tracep();
		List<ChartDataValues> retList = new ArrayList<ChartDataValues>();
		if (null != statistics){
			retList.add(createChartDataVal(statistics));
		}else{
			for(Datasource datasource : getDatasources()){
				ChartDataValues chartData = createChartDataVal(datasource.statistics);				// TODO add datasource name to parameters
				chartData.setMaxPointsNb(maxPointsNb);
				retList.add(chartData);
			}
		}
		LogAndDebug.tracem();
		return retList;
	}


	private ChartDataValues createChartDataVal(Statistics statistics) {
		String xLabel = "ms.";
		String yLabel = name;		

		String datasourceName="";
		if (datasetType == DatasetType.MULTIPLE_DATASET){
			datasourceName = statistics.getname();
		}
		else{
			datasourceName=name;
		}


		StatOnLongs stolDate;
		StatOnLongs stol;

		if (null == statistics){
			stolDate = null;
			stol = null;
		}else{
			stolDate = statistics.getStolDate();
			stol = statistics.getStol();
		}

		ChartDataValues ret = new ChartDataValues(this, xLabel, yLabel, chartTypesSet, step, timeWindow, stol, stolDate, datasourceName);
		String fieldName = datasources.get(0).getField();
		for(Datasource source : datasources){
			if (! fieldName.equals(source.getField())){
				LogAndDebug.trace("() *** Found different field names: \""+fieldName+"\" and \""+ source.getField()+"\"");
				fieldName += "+"+source.getField();
			}
		}

		ret.setyLabel(fieldName);
		return ret;
	}


	public void clearDatasources() {
		datasources.clear();
	}


	public void addDatasource(Datasource datasource) {
		datasources.add(datasource);
		datasource.setDataset(this);
	}


	public String getBladeId() {
		String bladesId = getDatasources().get(0).getBladeId();
		return bladesId;
	}


	/**
	 * @return a String with all bladeIds
	 */
	public String getBladeIds(){
		StringBuffer ret = new StringBuffer();
		String pre = "";
		String post = "";
		if (datasources.size() > 1){
			pre = "[";
			post = "";
		}
		for(Datasource ds : datasources){
			ret.append(pre + ds.getBladeId());
			pre = ", ";
			post = "]";
		}
		ret.append(post);
		return ret.toString();
	}


	/**
	 * @return a String with all bladeIds, but without duplicates
	 */
	public String getUniqueBladeIds() {
		StringBuffer ret = new StringBuffer();
		String pre = "";
		String post = "";
		Set<String> bladeIdsSet = new HashSet<String>();
		for(Datasource ds : datasources){
			bladeIdsSet.add(ds.getBladeId());
		}
		if (bladeIdsSet.size() > 1){
			pre = "[";
			post = "";
		}
		for (String s : bladeIdsSet){
			ret.append(pre + s);
			pre = ", ";
			post = "]";
		}
		if (bladeIdsSet.size() > 1){
			ret.append(post);
		}
		return ret.toString();
	}

	public String dump() {
		String tabs = "\t\t\t";
		StringBuffer text = new StringBuffer("\t\t\tDataset:\n");
		text.append(tabs + "dataset type: \"" + this.getDatasetType().toText() + "\"\n");
		text.append(tabs + "name: \"" + this.getName() + "\"\n");
		text.append(tabs + "id: " + this.getId() + "\n");
		text.append(tabs + "comment: \"" + comment + "\"\n");

		text.append(tabs + "datasetId: " + datasetId + "\n");
		text.append(tabs + "initialName: \"" + initialName + "\"\n");
		text.append(tabs + "section: \"" + section + "\"\n");

		text.append(tabs + "performStatAnalysis: " + performStatAnalysis + "\n");

		text.append(tabs + "startFilterAtFirstEvent: " + startFilterAtFirstEvent + "\n");
		text.append(tabs + "filterStartTime: " + filterStartTime + "\n");
		text.append(tabs + "endFilterAtLastEvent: " + endFilterAtLastEvent + "\n");
		text.append(tabs + "filterEndTime: " + filterEndTime + "\n");

		text.append(tabs + "kParam: " + kParam + "\n");
		text.append(tabs + "percentPointsKeptParam: " + percentPointsKeptParam + "\n");

		text.append(tabs + "performDraw: " + performDraw + "\n");

		text.append(tabs + "chartTypesSet: " + chartTypesSet + "\n");

		text.append(tabs + "maxPointsNb: " + maxPointsNb + "\n");
		text.append(tabs + "timeWindow: " + timeWindow + "\n");
		text.append(tabs + "step: " + step + "\n");
		text.append(tabs + "slicesNb: " + slicesNb + "\n");

		text.append(tabs + "firstEventTime: " + firstEventTime + "\n");
		text.append(tabs + "lastEventTime: " + lastEventTime + "\n");


		text.append(tabs + "nextId: " + nextId + "\n");

		text.append(tabs + "fieldsValuesFilter:\n");
		text.append(fieldsValuesFilter.dump());

		text.append(tabs + "statistics: \n");
		if (null != statistics){
			text.append(statistics.dump());
		}

		text.append(tabs + "datasources: \n");
		for(Datasource ds : getDatasources()){
			text.append(ds.dump());
		}
		return text.toString();
	}


	public void setLogicalOp(LogicalEnum logicalOp) {
		LogAndDebug.tracep("("+logicalOp+") "+ LogAndDebug.toText(this));
		fieldsValuesFilter.setLogicalOp(logicalOp);
		LogAndDebug.tracem("("+logicalOp+") "+ LogAndDebug.toText(this)+ " = " + getLogicalOp());
	}


	private String getLogicalOp() {
		return fieldsValuesFilter.getLogicalOp().toString();
	}


	public String getEventType() {
		String eventType = getDatasources().get(0).getEventType();
		for(Datasource ds : getDatasources()){
			if (eventType.equals(ds.getEventType())){
				;
			}else{
				LogAndDebug.err("() *** more than one eventTypes ("+eventType+","+ds.getEventType()+") in " + LogAndDebug.toText(this));
			}
		}
		return eventType;
	}


	public String getUniqueEventTypes() {
		StringBuffer ret = new StringBuffer();
		String pre = "";
		String post = "";
		Set<String> eventTypesSet = new HashSet<String>();
		for(Datasource ds : datasources){
			eventTypesSet.add(ds.getEventType());
		}
		if (eventTypesSet.size() > 1){
			pre = "[";
			post = "";
		}
		for (String s : eventTypesSet){
			ret.append(pre + s);
			pre = ", ";
			post = "]";
		}
		if (eventTypesSet.size() > 1){
			ret.append(post);
		}
		return ret.toString();
	}



	public String getFields() {
		StringBuffer fields = new StringBuffer("");

		String pre = "";
		String post = "";
		if (datasources.size()>1){
			pre = "[";
			post = "]";
		}
		for(Datasource ds : getDatasources()){
			fields.append(pre + ds.getField());
			pre = ", ";
		}
		fields.append(post);
		return fields.toString();
	}


	public String[] getArrayFields(){
		List<String> fieldsList = new ArrayList<String>();
		for(Datasource ds : getDatasources()){
			fieldsList.add(ds.getField());
		}
		String[] ret = (String[]) fieldsList.toArray(new String[fieldsList.size()]);
		return ret;
	}


	public String[] getUniqueFields(){
		//		List<String> fieldsList = new ArrayList<String>();
		HashMap<String, Integer> map = new HashMap<String, Integer>();
		for(Datasource ds : getDatasources()){
			String field = ds.getField();
			//			fieldsList.add(field);
			if (map.containsKey(field)){
				map.put(field,map.get(field)+1);
			}else{
				map.put(field, new Integer(1));
			}
		}
		ArrayList<String> ret = new ArrayList<String>();
		for(String s : map.keySet()){
			ret.add(s);
		}
		return (String[]) ret.toArray(new String[ret.size()]);
	}


	public String getStatisticalFilteringParamsToString() {
		return "K: "+ kParam + "  keep at least "+percentPointsKeptParam+" points.\n";
	}

	public String getStatisticalFilteringParamsToHtml() {
		if (kParam == 0.0){
			return NO_FILTER;
		}
		return "K: "+ kParam + "<br />  keep at least "+percentPointsKeptParam+" points.\n";
	}

	public String getTimeFilterEventsDiscardedNumber() {
		// TODO Auto-generated method stub
		return null;
	}


	public Content saveMacro(int i) {
		Element dataset =new Element("dataset");


		// case graph Type attribut
		Attribute graphTypeAttribut = new Attribute("type","Unknown" );
		if (datasetType.equals("Simple"))
		{
			graphTypeAttribut.setName("Simple");
		}
		else if (datasetType.equals("Multiple"))
		{
			graphTypeAttribut.setName("Multiple");
		}
		else if (datasetType.equals("Aggregate"))
		{
			graphTypeAttribut.setName("Aggregate");
		}
		else
		{
			graphTypeAttribut.setName("Unknown");
		}

		dataset.setAttribute(graphTypeAttribut);

		Attribute beginAttribut = new Attribute("begin_event","  (ms)");
		dataset.setAttribute(beginAttribut);

		Attribute endAttribut = new Attribute("end_event","  (ms)");
		dataset.setAttribute(endAttribut);		

		Element titleXml =new Element("title");
		dataset.addContent(titleXml);

		Element commentXml =new Element("comments");
		dataset.addContent(commentXml);	

		Element raw = new Element("raw");
		Attribute rawAttribut=new Attribute("max","");
		raw.setAttribute(rawAttribut);
		dataset.addContent(raw);


		Element eventType =new Element("events");
		Attribute fieldAttribut=new Attribute("field","");
		eventType.setAttribute(fieldAttribut);	
		Element selector =new Element("selection_operator");
		Attribute operatordAttribut=new Attribute("operator","and^^or");
		selector.setAttribute(operatordAttribut);	
		Element filter = new Element("filter");
		Attribute fieldfiletrAttribut=new Attribute("field_filter","");
		filter.setAttribute(fieldfiletrAttribut);
		Attribute operatorAttribut=new Attribute("operator","");
		filter.setAttribute(operatorAttribut);
		Attribute valueAttribut=new Attribute("value","");
		filter.setAttribute(valueAttribut);
		selector.addContent(filter);
		eventType.addContent(selector);		

		Element datasource = new Element("datasource");		
		Attribute testdAttribut=new Attribute("test","");
		datasource.setAttribute(testdAttribut);
		Attribute bladeAttribut=new Attribute("blade","");
		datasource.setAttribute(bladeAttribut);
		Attribute aliasAttribut=new Attribute("alias","");
		datasource.setAttribute(aliasAttribut);
		eventType.addContent(datasource);

		dataset.addContent(eventType);


		return dataset;
	}

	public void loadReport(Element datasetLoaded) {	

		String	field="";
		String  type = "" ; 
		Element rootload =datasetLoaded;

		if (rootload==null){
			System.out.println("empty file");
		}		
		else if (rootload!=null){
			List<?> attributes = rootload.getAttributes();
			Iterator<?> j = attributes.iterator();
			while(j.hasNext())
			{
				Attribute courantAttribute = (Attribute)j.next();
				String nameAttribute = courantAttribute.getName();
				if(nameAttribute=="dataset_type")
				{					
					fromString(courantAttribute.getValue());
				}
				else {
					;
				}

			}

			List<?> elements = rootload.getChildren();
			Iterator<?> i = elements.iterator();		      
			while(i.hasNext())
			{		    	  
				Element courant = (Element)i.next();
				String name = courant.getName();
				if(name=="title")
				{ 
					System.out.println("title + +  : " +XMLToString(courant));	
					setName( courant.getValue());     	
				}

				else if (name=="comment"){
					System.out.println("comment + +  : " +XMLToString(courant));	
					comment=courant.getValue();						
				}

				else if(name=="raw"){
					setPerformDraw(true);
					setPerformStatAnalysis(true);					
					Attribute courantrawAttribute=courant.getAttribute("max");						
					String rawNameAttribute = courantrawAttribute.getName();
					System.out.println("raw + rawNameAttribute+  : " +rawNameAttribute);					

				}

				else if(name=="moving"){				
					System.out.println("||" +XMLToString(courant));
				}					

				else if(name=="events"){
					Element eventTypeRoot =courant;					
					String eventTypeAttribut="";

					List<?> attributesEvent = eventTypeRoot.getAttributes();
					Iterator<?> m = attributesEvent.iterator();
					while(m.hasNext())
					{
						Attribute courantAttribute = (Attribute)m.next();
						eventTypeAttribut = courantAttribute.getName();
						System.out.println("dataset eventTypeValue "+eventTypeAttribut);
						if(eventTypeAttribut=="type")
						{
							type=courantAttribute.getValue();							
							System.out.println("dataset eventTypeValue type "+type);
						}

						else if(eventTypeAttribut=="field")
						{
							field=courantAttribute.getValue();							
							System.out.println("dataset eventTypeValue field "+field);
						}

						else {;
						}

					}					

					List<?> eventElements = eventTypeRoot.getChildren();
					Iterator<?> l = eventElements.iterator();		      
					while(l.hasNext())
					{		    	  
						Element eventCourant = (Element)l.next();
						String eventElementName = eventCourant.getName();
						if(eventElementName=="datasource")
						{ 
							System.out.println("datasource + eventCourant +  : " +XMLToString(eventCourant));
							System.out.println("datasource + field  +  : " +field );
							Datasource datasource = loadDatasourceReport(eventCourant,type,field);								
							addDatasource(datasource);

							//	statistics = datasource.getStatistics();
							this.statistics  = datasource.getStatistics();

							setPerformStatAnalysis(true);
							//createChartDataVal(statistics);
							createChartDataValues();

							setDrawRaw(true);
							setPerformTimeChart(true);
							setPerformDraw(true);

						}
						else if (eventElementName=="selection_operator"){
							System.out.println("selection_operator + +  : " +XMLToString(eventCourant));	

						}
					}
				}

				else {						
					System.out.println("Not dataset element : "+courant.getValue());
				}					 
			}			

		}	
	}

	public Datasource loadDatasourceReport(Element eventCourant,String eventType,String field) {

		Element rootload =eventCourant;

		String bladeAttribut = null;
		String testAttribut= null;
		String aliasAttribut= null;

		if (rootload==null){
			System.out.println("empty datasource file");
		}		

		else if (rootload!=null){
			List<?> attributes = rootload.getAttributes();
			Iterator<?> j = attributes.iterator();
			while(j.hasNext())
			{
				Attribute courantAttribute = (Attribute)j.next();
				String nameAttribute = courantAttribute.getName();

				System.out.println("   datasource nameAttribute "+nameAttribute);
				//		courantAttribute=rootload.getAttribute("test");	

				if(nameAttribute==("test")){			
					testAttribut=courantAttribute.getValue();	
					System.out.println("   datasource testAttribut "+testAttribut);
				}
				else if(nameAttribute==("blade")){
					bladeAttribut=courantAttribute.getValue();
					System.out.println("   datasource bladeAttribut "+bladeAttribut);
				}
				else if(nameAttribute==("alias")){
					aliasAttribut=courantAttribute.getValue();
					System.out.println("   datasource aliasAttribut "+aliasAttribut);
				}
				else {
					System.out.println("datasource : " +nameAttribute+" is not a datasource attribute");
				}
			}

		}	
		Datasource ns = new Datasource (testAttribut,bladeAttribut,eventType,field);
		return ns;
	}


	public String XMLToString(Element current){

		String elementName= current.getName();
		List<?> childrens = current.getChildren();

		Iterator<?> it= childrens.iterator();
		if(!it.hasNext()){
			return elementName;
		}
		else
		{	current =(Element)it.next() ;
		String nextName= XMLToString(current);

		while( it.hasNext())
		{
			nextName += ",";
			current =(Element)it.next() ;
			nextName += XMLToString(current);
		}
		return elementName + "(" + nextName + ")";
		}		

	}



}