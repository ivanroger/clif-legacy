/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * CLIF $Name:  $
 *
 * Contact: clif@ow2.org
 */
package org.ow2.clif.analyze.statistics;

import org.ow2.clif.storage.api.*;
import org.ow2.clif.supervisor.api.ClifException;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

/**
 * This class implements the methods to browse test runs and access
 * to test measurements/results.
 * 
 * @author Guy Vachet
 */
public class BladeStoreReader implements Constants {
	// verbose mode for verification
	private static final boolean VERBOSE = false;
	private BladeDescriptor bladeDescriptor = null;
	private File bladeDir = null;

	private static Map<String, EventFactory> eventFactories = null;
	private static Map<String, String> eventTypes = null;

	static {
		String eventTypeLabel;
		EventFactory factory;
		eventTypes = new HashMap<String, String>();
		eventFactories = new HashMap<String, EventFactory>();
		eventTypes.put(ACTION_EVENT_TYPE_LABEL, ACTION_EVENT_CLASS);
		eventTypes.put(ALARM_EVENT_TYPE_LABEL, ALARM_EVENT_CLASS);
		eventTypes.put(CPU_EVENT_TYPE_LABEL, CPU_EVENT_CLASS);
		eventTypes.put(JVM_EVENT_TYPE_LABEL, JVM_EVENT_CLASS);
		// eventTypes.put(JMX_JVM_EVENT_TYPE_LABEL, JMX_JVM_EVENT_CLASS);
		eventTypes.put(LIFECYCLE_EVENT_TYPE_LABEL, LIFECYCLE_EVENT_CLASS);
		eventTypes.put(MEMORY_EVENT_TYPE_LABEL, MEMORY_EVENT_CLASS);
		eventTypes.put(NETWORK_EVENT_TYPE_LABEL, NETWORK_EVENT_CLASS);
		for (Iterator<String> iter = eventTypes.keySet().iterator(); iter
				.hasNext();) {
			eventTypeLabel = iter.next();
			try {
				// initialize the corresponding class
				Class.forName(eventTypes.get(eventTypeLabel));
				factory = AbstractEvent.getEventFactory(eventTypeLabel);
				eventFactories.put(eventTypeLabel, factory);
				if (VERBOSE) {
					StringBuffer sb = new StringBuffer("Get factory ")
							.append(factory);
					System.out.println(sb.append(" for ")
							.append(eventTypeLabel));
				}
			} catch (Exception e) {
				StringBuffer sb = new StringBuffer(
						"Cannot initialize event class for ");
				throw new Error(sb.append(eventTypeLabel).toString(), e);
			}
		}
	}

	/**
	 * empty constructor
	 */
	public BladeStoreReader() {
	}

	/**
	 * constructor
	 */
	public BladeStoreReader(BladeDescriptor bladeDescriptor, File bladeDir) {
		setBladeDescriptor(bladeDescriptor);
		setBladeDir(bladeDir);
		if (VERBOSE) {
			StringBuffer sb = new StringBuffer("Event reader for: ")
					.append(bladeDir);
			sb.append(" \t(test: ").append(bladeDir.getParent()).append(")");
			String[] list = new File(bladeDir.getPath()).list();
			for (int i = 0; i < list.length; i++)
				sb.append("\n\t").append(list[i]);
			System.out.println(sb.append("\n"));
		}
	}

	public static String getEventClassName(String eventTypeLabel) {
		return eventTypes.get(eventTypeLabel);
	}

	public static String[] getEventFieldLabels(String eventTypeLabel) {
		// don't need strange code any more due to static initializing
		return AbstractEvent.getEventFieldLabels(eventTypeLabel);
	}

	public static EventFactory getEventFactory(String eventTypeLabel)
			throws ClifException {
		EventFactory factory = null;
		try {
			factory = eventFactories.get(eventTypeLabel);
			if (factory == null)
				throw new Exception("Any matching event type label.");
		} catch (Exception e) {
			StringBuffer sb = new StringBuffer("Cannot get event class for ");
			throw new ClifException(sb.append(eventTypeLabel).toString(), e);
		}
		return factory;
	}

	public BladeDescriptor getBladeDescriptor() {
		return bladeDescriptor;
	}

	public void setBladeDescriptor(BladeDescriptor bladeDescriptor) {
		this.bladeDescriptor = bladeDescriptor;
	}

	public File getBladeDir() {
		return bladeDir;
	}

	public void setBladeDir(File bladeDir) {
		this.bladeDir = bladeDir;
	}

	public final static boolean isCommentLine(String line) {
		return line.startsWith(COMMENT_PREFIX);
	}

	public List<BladeEvent> getEvents(String eventTypeLabel, EventFilter filter)
			throws ClifException {
		List<BladeEvent> eventList = new ArrayList<BladeEvent>();
		BufferedReader reader = null;
		EventFactory factory = null;
		BladeEvent event;
		StringBuffer sb;
		try {
			factory = getEventFactory(eventTypeLabel);
			reader = new BufferedReader(new FileReader(new File(bladeDir,
					eventTypeLabel)));
			String line = reader.readLine();
			while (line != null) {
				if (!isCommentLine(line)) {
					event = factory.makeEvent(AbstractEvent.DEFAULT_SEPARATOR,
							line);
					if (filter == null || filter.accept(event)) {
						eventList.add(factory.makeEvent(
								AbstractEvent.DEFAULT_SEPARATOR, line));
					}
				}
				line = reader.readLine();
			}
		} catch (ClifException e) {
			throw e;
		} catch (NoMoreEvent e) {
			// silent
		} catch (IOException e) {
			sb = new StringBuffer("Error when getting ");
			if (factory == null)
				sb = new StringBuffer("Unable to get factory for ");
			else if (reader == null)
				sb = new StringBuffer("Unable to find  ");
			sb.append(eventTypeLabel).append(" events from blade ");
			throw new ClifException(sb.append(bladeDir).toString(), e);
		} finally {
			try {
				if (reader != null)
					reader.close();
			} catch (IOException ex) {
				ex.printStackTrace(System.err);
			}
		}
		if (VERBOSE) {
			sb = new StringBuffer("Get ").append(eventList.size());
			System.out.println(sb.append(" ").append(eventTypeLabel).append(
					" events"));
		}
		return eventList;
	}

	public List<BladeEvent> getAllEvents(String eventTypeLabel)
			throws ClifException {
		return getEvents(eventTypeLabel, null);
	}

	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer("blade # ");
		sb.append(getBladeDescriptor().getId()).append((" manage"));
		String[] eventTypeLabels = getBladeDescriptor().getEventTypeLabels();
		for (int l = 0; l < eventTypeLabels.length; l++) {
			sb.append(" ").append(eventTypeLabels[l]);
			if (VERBOSE) {
				sb.append(" (");
				String[] labels = getEventFieldLabels(eventTypeLabels[l]);
				for (int m = 0; m < labels.length; m++) {
					sb.append("\"").append(labels[m]).append("\";");
				}
				sb.append(")");
			}
		}
		return sb.toString();
	}

}
