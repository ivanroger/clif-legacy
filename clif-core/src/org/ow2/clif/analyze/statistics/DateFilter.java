/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * CLIF $Name:  $
 *
 * Contact: clif@ow2.org
 */
package org.ow2.clif.analyze.statistics;

import org.ow2.clif.storage.api.BladeEvent;
import org.ow2.clif.storage.api.EventFilter;

/**
 * Helper class for filtering blade events according their date.
 * 
 * @author Guy Vachet
 */
public class DateFilter implements EventFilter {

	private static final long serialVersionUID = -4894494433208669386L;
	private long minTime, maxTime;

	/**
	 * Creates a new blade event filter selecting events whose date is between
	 * the given bounds (inclusive). Any bound can be negative and at least the
	 * upper bound is greater than the lower one.
	 * 
	 * @param lower
	 *            the lower date bound (inclusive)
	 * @param upper
	 *            the upper date bound (inclusive)
	 */
	public DateFilter(long lower, long upper) {
		minTime = lower;
		maxTime = upper;
	}

	public long getMinTime() {
		return minTime;
	}

	public long getMaxTime() {
		return maxTime;
	}

	/**
	 * reduce the elapsed time window to be analyzed
	 * 
	 * @param start
	 *            gap from the lower bound
	 * @param end
	 *            in order to define the range with the start parameter
	 */
	public void updateBounds(long start, long end) {
		minTime += start;
		if (end < (maxTime - minTime + start))
			maxTime = minTime + end - start;
	}

	// /////////////////////////
	// EventFilter interface //
	// /////////////////////////

	@Override
	public boolean accept(BladeEvent event) {
		return (event.getDate() >= minTime) && (event.getDate() <= maxTime);
	}

	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer("Time filter (elapsed time range ");
		sb.append("to analyze):\n\t(range = [").append(minTime).append("..");
		return sb.append(maxTime).append("] -CLIF date in milliseconds !-)").toString();
	}

}
