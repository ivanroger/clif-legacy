/*
 * Copyright (C) 2005 - France Telecom R&D
 */
package org.ow2.clif.analyze.statistics.profiling;

/**
 * This generic object is comparable to AbstractEvent. This object
 * represents a result of the profiling which is a measurement of
 * the long type.
 * 
 * @author Guy Vachet
 * @see org.ow2.clif.storage.api.AbstractEvent
 * @see org.ow2.clif.analyze.statistics.profiling.ActionDatum
 * @see org.ow2.clif.analyze.statistics.profiling.ProbeDatum
 */
public abstract class Datum implements Comparable<Datum> {

	private long date = -1;

	public void setDate(long date) {
		this.date = date;
	}

	public long getDate() {
		return date;
	}

	public abstract long getResult();

	// ////////////////////////
	// interface Comparable //
	// ////////////////////////

	/**
	 * The order is based on dates. In case of same dates, an arbitrary order is
	 * applied based on hashcodes.
	 */
	@Override
	public int compareTo(Datum obj) throws ClassCastException {
		int diff = (int) (date - obj.getDate());
		return diff == 0 ? hashCode() - obj.hashCode() : diff;
	}

}
