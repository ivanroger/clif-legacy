/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2005 France Telecom R&D
 * Copyright (C) 2017 Orange SA
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * Contact: clif@ow2.org
 */
package org.ow2.clif.analyze.statistics.util.data;

import java.util.Collections;
import java.util.logging.Logger;
import java.util.logging.Level;

/**
 * Class for performances statistical calculation (data stored in ListOfLong) with
 * median, average value, standard deviation ...
 * 
 * @author Guy Vachet
 * @author Bruno Dillenseger
 */
public class StatOnLongs {
	// verbose mode for verification
	private static final boolean VERBOSE = false;
	protected Logger logger = null;
	// data storage in order as values are supplied (chronological order)
	private ListOfLong data;
	// min number of values to use (to avoid drastic statistical rejection)
	public static final int MIN_SIZE_OF_STATISTICAL_DATA = 30;
	public static final double DEFAULT_PERCENT_MIN = 95.4;
	public static final double DEFAULT_FACTOR = 2.0;
	// sorted list based on original data
	private ListOfLong sortedData;
	// tells if sort list based on original data has been defined
	private boolean isSorted;
	// tells if statistical rejection has been applied (and is consistant ?)
	private boolean isStatisticalRejected;
	// the factor of statistical rejection (based on mean +/- factor*std)
	private double statisticalSortFactor = DEFAULT_FACTOR;
	// the target percentage of kept measures after statistical rejection
	private double statisticalSortPercentage = DEFAULT_PERCENT_MIN;
	// private int statNb; // number of values after statistical rejection.
	private double statMean; // mean after statistical rejection.
	private double statStd; // standard deviation after statistical rejection.
	// min and max indexes of sorted, statistical sort data, inclusive.
	private int minStatIndex;
	private int maxStatIndex;

	/**
	 * longStat empty constructor
	 */
	public StatOnLongs() {
		data = new ListOfLong();
	}

	/**
	 * Constructs a LongStatistics with an empty list of long contructed with
	 * the specified initial capacity.
	 * 
	 * @param initialCapacity
	 */
	public StatOnLongs(int initialCapacity) {
		data = new ListOfLong(initialCapacity);
	}

	/**
	 * longStat constructor based on existing ListOfLong.
	 * 
	 * @param listOfLong
	 *            the ListOfLong as initial value.
	 */
	public StatOnLongs(ListOfLong listOfLong) {
		data = new ListOfLong(listOfLong);
	}

	/**
	 * to specify a logger when you want to log something.
	 */
	public void setLogger(Logger log) {
		logger = log;
	}

	/**
	 * resets the environment
	 */
	public void reset() {
		data.reset();
		isSorted = false;
		isStatisticalRejected = false;
	}

	/**
	 * gets data as a list of long
	 * 
	 * @return ListOfLong in order as values are supplied (chronological order)
	 */
	public ListOfLong getData() {
		return new ListOfLong(data);
	}

	public ListOfLong getSortedData()
	{
		sort();
		return new ListOfLong(sortedData); 
	}

	/**
	 * adds a long value in data
	 * 
	 * @param value
	 *            to be added to data
	 */
	public void addLong(long value) {
		data.addLong(value);
		isSorted = false;
		isStatisticalRejected = false;
	}

	/**
	 * adds a copy of ListOfLong managed by the supplied StatOnLongs in the
	 * current one managed by this LongStatistics
	 * 
	 * @param ls
	 *            the LongStatistics to be added in the current one
	 */
	public void addAll(StatOnLongs ls) {
		data.addAll(ls.getData());
		isSorted = false;
		isStatisticalRejected = false;
	}

	/**
	 * adds a ListOfLong in the current one managed by this LongStatistics
	 * 
	 * @param lol
	 *            the ListOfLong to be added in the current one
	 */
	public void addAll(ListOfLong lol) {
		data.addAll(lol);
		isSorted = false;
		isStatisticalRejected = false;
	}

	/**
	 * remove long value at the specified position in this list.
	 * 
	 * @param index
	 *            the position
	 */
	public void removeLongAt(int index) {
		data.removeLongAt(index);
		isSorted = false;
		isStatisticalRejected = false;
	}

	/**
	 * get long value at the specified position in this list (ith value, in
	 * chronological order)
	 * 
	 * @param index
	 *            the position
	 * @return long value
	 */
	public long getLongAt(int index) {
		return data.longAt(index);
	}

	/**
	 * gets the data size
	 * 
	 * @return size
	 */
	public int size() {
		return data.size();
	}

	/**
	 * gets the factor of statistical rejection (mean +/- factor*std)
	 * 
	 * @return factor
	 */
	public double getStatisticalSortFactor() {
		return statisticalSortFactor;
	}

	/**
	 * sets the factor of statistical sort (rejection based on mean +/-
	 * factor*std).
	 * 
	 * @param factor
	 *            that must be greater than 0.
	 * @throws java.lang.IllegalArgumentException
	 *             if an input parameter is illegal
	 */
	public void setStatisticalSortFactor(double factor)
			throws IllegalArgumentException {
		if (factor > 0) {
			statisticalSortFactor = factor;
			isStatisticalRejected = false;
		} else
			throw new IllegalArgumentException("factor must be greater than 0");
	}

	/**
	 * gets the target percentage of kept measures after statistical rejection
	 * 
	 * @return rate
	 */
	public double getStatisticalSortPercentage() {
		return statisticalSortPercentage;
	}

	/**
	 * sets the target percentage of kept measures after statistical rejection
	 * in order to avoid to clear data during statistical rejection.
	 * 
	 * @param percentage
	 *            (0.0 < rate <= 100.0)
	 * @throws java.lang.IllegalArgumentException
	 *             if an input parameter is illegal
	 */
	public void setStatisticalSortPercentage(double percentage)
			throws IllegalArgumentException {
		if ((percentage > 0) && (percentage <= 100)) {
			statisticalSortPercentage = percentage;
			isStatisticalRejected = false;
		} else
			throw new IllegalArgumentException("rate must be in range 0..100");
	}

	/**
	 * outputData the original data as one value per line
	 */
	public void outputData() {
		System.out.println(data.toString());
	}

	/**
	 * outputData the original data as an Array of "number" columns.
	 * 
	 * @param numberOfColumn
	 *            to format the values.
	 */
	public void outputData(int numberOfColumn) {
		System.out.println(data.toString(numberOfColumn));
	}

	/**
	 * sort the list
	 */
	private void sort() {
		if (!isSorted) {
			sortedData = new ListOfLong(data);
			Collections.sort(sortedData);
			isSorted = true;
		}
	}
	
	/**
	 * outputSortedData the sorted data as one value per line
	 */
	public void outputSortedData() {
		sort();
		System.out.println(sortedData.toString());
	}

	/**
	 * outputSortedData the sorted data as an Array of "number" columns.
	 * 
	 * @param numberOfColumn
	 *            to format the values.
	 */
	public void outputSortedData(int numberOfColumn) {
		sort();
		System.out.println(sortedData.toString(numberOfColumn));
	}

	/**
	 * gets the min value of raw data
	 * 
	 * @return long min value
	 */
	public Long getMin() {
		if (data.size() == 0)
			return null;
		else if (data.size() == 1)
			return data.longAt(0);
		else {
			sort();
			return sortedData.longAt(0);
		}
	}

	/**
	 * gets the max value of raw data
	 * 
	 * @return long max value
	 */
	public Long getMax() {
		if (data.size() == 0)
			return null;
		else if (data.size() == 1)
			return data.longAt(0);
		else {
			sort();
			return sortedData.longAt(data.size() - 1);
		}
	}

	/**
	 * Returns median of data part. The sub-data begins at the specified
	 * beginIndex and ends at endIndex. Thus the size of the sub-data is
	 * (endIndex-beginIndex+1).
	 * 
	 * @param beginIndex -
	 *            the beginning index, inclusive.
	 * @param endIndex -
	 *            the ending index, inclusive.
	 * @return median of value within [beginIndex..endIndex]
	 */
		
	public Long getMedian(int beginIndex, int endIndex) {
		if ((beginIndex >= 0) && (endIndex < size())
				&& (beginIndex <= endIndex)) {
			sort();
			if (beginIndex == endIndex)
				return sortedData.longAt(beginIndex);
			else {
				int size = endIndex - beginIndex + 1;
				int i = beginIndex + size / 2;
				sort();
				if ((size % 2) == 0) {
					// median is the mean of 2 values
					return (sortedData.longAt(i - 1) + sortedData.longAt(i)) / 2;
				} else
					return sortedData.longAt(i);
			}
		} else
			return null;
	}


	
	
	/**
	 * median calculation of raw data
	 * 
	 * @return long median
	 */
	public Long getMedian() {
		return getMedian(0, data.size() - 1);
	}

	/**
	 * gets average of raw data
	 * 
	 * @return double average
	 */
	public double getMean() {
		if (data.size() == 0)
			return Double.NaN;
		else if (data.size() == 1)
			return data.longAt(0);
		else
			return data.getSum() / data.size();
	}

	/**
	 * gets the standard deviation of raw data
	 * 
	 * @return double standard deviation
	 */
	public double getStd() {
		if (data.size() < 2)
			return Double.NaN;
		else {
			double var = data.getSquareSum();
			var -= data.getSum() * data.getSum() / data.size();
			return Math.sqrt(var / (data.size() - 1));
		}
	}

	/**
	 * the list of the sorted values is divided in m equal parts the mean of the
	 * n_th part of the best values is returned
	 * 
	 * @return double n_th average
	 * @param n
	 *            n_th part
	 * @param m
	 *            m equal parts
	 */
	public double subMean(int n, int m) throws Exception {
		if (m > data.size())
			throw new Exception("not enough values for subMean function,"
					+ " calculation not allowed.");
		if (m <= 0 || n < 1 || n > m)
			throw new Exception("value out of bounds for subMean function,"
					+ " calculation not allowed.");
		sort();
		int i0 = ((n - 1) * data.size()) / m;
		int i1 = (n * data.size()) / m;
		if (i0 == i1)
			return Long.MIN_VALUE;
		double subSum = 0;
		for (int i = i0; i < i1; i++)
			subSum += sortedData.longAt(i);
		return subSum / (i1 - i0);
	}

	/**
	 * the list of the sorted values is divided in m equal parts (m>0) mean of
	 * the sets [n1..n2] (n1 till n2, 0 < n1 < n2 <= m )
	 * 
	 * @param n1
	 *            start part
	 * @param n2
	 *            end part
	 * @param m
	 *            m equal parts
	 */
	public double subMean(int n1, int n2, int m) throws Exception {
		if (m > data.size())
			throw new Exception("not enough values for subMean function,"
					+ " calculation not allowed.");
		if ((m <= 0 || n1 < 1 || n1 > m) || (m <= 0 || n2 <= n1 || n2 > m))
			throw new Exception("value out of bounds for subMean function,"
					+ " calculation not allowed.");
		sort();
		int i0 = ((n1 - 1) * data.size()) / m;
		int i1 = (n2 * data.size()) / m;
		if (i0 == i1)
			return Long.MIN_VALUE;
		double subSum = 0;
		for (int i = i0; i < i1; i++)
			subSum += sortedData.longAt(i);
		return subSum / (i1 - i0);
	}

	/**
	 * the list of the sorted values is divided in m equal parts, the median
	 * from the best value (minimal) till the n_th part (included) is returned
	 * 
	 * @return long subMin
	 * @param n
	 *            n_th part
	 * @param m
	 *            equal parts
	 */
	public long subMin(int n, int m) throws Exception {
		if (m > data.size())
			throw new Exception("not enough values for subMin function,"
					+ " calculation not allowed.");
		if (m <= 0 || n < 1 || n > m)
			throw new Exception("value out of bounds for subMin function,"
					+ " calculation not allowed.");
		sort();
		return sortedData.longAt((n - 1) * data.size() / m);
	}

	/**
	 * the list of the sorted values is divided in m equal parts, the median
	 * from the best value (minimal) till the n_th part (included) is returned
	 * 
	 * @return long subMax
	 * @param n
	 *            n_th part
	 * @param m
	 *            equal parts
	 */
	public long subMax(int n, int m) throws Exception {
		if (m > data.size())
			throw new Exception("not enough values for subMax function,"
					+ " calculation not allowed.");
		if (m <= 0 || n < 1 || n > m)
			throw new Exception("value out of bounds for subMax function,"
					+ " calculation not allowed.");
		sort();
		return sortedData.longAt((n * data.size() / m) - 1);
	}

	/**
	 * the list of the sorted values is divided in m equal parts, the median
	 * from the best value (minimal) till the n_th part (included) is returned
	 * 
	 * @return long submedian
	 * @param n
	 *            n_th part
	 * @param m
	 *            equal parts
	 */
	public long subMedian(int n, int m) throws Exception {
		if (m > data.size())
			throw new Exception("not enough values for subMedian function,"
					+ " calculation not allowed.");
		if (m <= 0 || n < 1 || n > m)
			throw new Exception("value out of bounds for subMedian function,"
					+ " calculation not allowed.");
		sort();
		if ((data.size() / m % 2) == 0) {
			// median is mean of 2 values
			int upIx = data.size() * (2 * n - 1) / 2 / m;
			return (sortedData.longAt(upIx - 1) + sortedData.longAt(upIx)) / 2;
		} else
			return sortedData.longAt((data.size() * (2 * n - 1) - m) / 2 / m);
	}

	/**
	 * statistical rejection based on mean +/- factor * standard deviation in
	 * respect of the percentage criteria (as 95% when factor is 2, or 99.7%
	 * when factor is 3, and so on, see bell distribution characteristics)
	 */
	private void statisticalRejection() {
		int statNb, count, minSize /* to stop statistical rejecting */;
		long minVal, maxVal, lowerOutOfRange, upperOutOfRange;
		double statSum, statDev;
		if (isStatisticalRejected)
			return;
		else
			isStatisticalRejected = true;
		statMean = getMean();
		statStd = getStd();
		statNb = data.size();
		minStatIndex = 0;
		maxStatIndex = statNb - 1;
		if (statNb < 3) {
			return;
		}
		sort();
		// apply statistical rejection only if the number of values is
		// greater than statisticalSortPercentage % of initial data number and
		// in respect of MIN_SIZE_OF_STATISTICAL_DATA
		minSize = (int) Math.ceil((statisticalSortPercentage * statNb / 100));
		if (minSize < MIN_SIZE_OF_STATISTICAL_DATA)
			minSize = MIN_SIZE_OF_STATISTICAL_DATA;
		// ready to reject value(s) out of statistical range
		statSum = data.getSum();
		statDev = data.getSquareSum();
		if (VERBOSE) {
			StringBuffer sb = new StringBuffer(
					"Performs statistical sort thru ");
			sb.append(statisticalSortFactor).append(
					" factor according to min data size = ");
			sb.append(minSize).append(
					", as:\n\tmin | max index\t\tAvg\tStd\n\t");
			sb.append(minStatIndex).append("\t").append(maxStatIndex);
			sb.append("\t\t").append(Math4Long.displayDouble(statMean));
			sb.append("\t").append(Math4Long.displayDouble(statStd));
			logger.log(Level.FINEST, sb.toString());
		}
		while (statNb > minSize) {
			minVal = sortedData.longAt(minStatIndex);
			maxVal = sortedData.longAt(maxStatIndex);
			lowerOutOfRange = Math.round(Math.ceil(statMean
					- statisticalSortFactor * statStd))
					- minVal;
			upperOutOfRange = maxVal
					- Math.round(Math.floor(statMean + statisticalSortFactor
							* statStd));
			// checks if statistical rejection must be applied
			if (lowerOutOfRange > upperOutOfRange) {
				if (lowerOutOfRange > 0) {
					// statistical rejection can be applied
					// try to include all-same value in the current removing
					for (count = 1, minStatIndex++; sortedData
							.longAt(minStatIndex) == minVal; minStatIndex++) {
						count++;
					}
					// check if count doesn't pass the min size
					if (count > (statNb - minSize)) {
						minStatIndex -= count + minSize - statNb;
						count = statNb - minSize;
					}
					statSum -= count * minVal;
					statDev -= count * minVal * minVal;
					statNb -= count;
				} else
					break;
			} else {
				if (upperOutOfRange > 0) {
					// statistical rejection can be applied
					// try to include all-same value in the current removing
					for (count = 1, maxStatIndex--; sortedData
							.longAt(maxStatIndex) == maxVal; maxStatIndex--) {
						count++;
					}
					// check if count doesn't pass the min size
					if (count > (statNb - minSize)) {
						maxStatIndex += count + minSize - statNb;
						count = statNb - minSize;
					}
					statSum -= count * maxVal;
					statDev -= count * maxVal * maxVal;
					statNb -= count;
				} else
					break;
			}
			// then (if no break) compute new statistical values for next loop
			statMean = statSum / statNb;
			statStd = (statDev - (statSum * statSum / statNb)) / (statNb - 1);
			statStd = Math.sqrt(statStd);
			if (VERBOSE) {
				StringBuffer sb = new StringBuffer("OK\t").append(minStatIndex);
				sb.append("\t").append(maxStatIndex).append("\t\t");
				sb.append(Math4Long.displayDouble(statMean)).append("\t");
				sb.append(Math4Long.displayDouble(statStd));
				logger.log(Level.FINEST, sb.toString());
			}
		}
	}

	/**
	 * computes the statistical median (could be more robust in case of non-bell
	 * distribution).
	 * 
	 * @return the statistical median
	 */
	public long getStatSortMedian() {
		statisticalRejection();
		return getMedian(minStatIndex, maxStatIndex);
	}

	/**
	 * computes the statistical mean
	 * 
	 * @return the statistical average
	 */
	public double getStatSortMean() {
		statisticalRejection();
		return statMean;
	}

	/**
	 * computes the standard deviation through statistical rejection
	 * 
	 * @return the standard deviation
	 */
	public double getStatSortStd() {
		statisticalRejection();
		return statStd;
	}

	/**
	 * keptRate the percent rate of kept values as one digit after dot
	 * 
	 * @return rate of nonRejection
	 */
	public double keptRate() {
		double tenrate = 1000.0 * getStatSortDataNumber() / data.size();
		return Math.round(tenrate) / 10;
	}

	public int getMinStatSortIndex() {
		statisticalRejection();
		return minStatIndex;
	}

	public int getMaxStatSortIndex() {
		statisticalRejection();
		return maxStatIndex;
	}

	public int getStatSortDataNumber() {
		statisticalRejection();
		return maxStatIndex - minStatIndex + 1;
	}

	public long getMinStatSortValue() {
		statisticalRejection();
		return sortedData.longAt(minStatIndex);
	}

	public long getMaxStatSortValue() {
		statisticalRejection();
		return sortedData.longAt(maxStatIndex);
	}

	public long getSortedValue(int index) {
		if ((index < 0) || (index >= size()))
			return Long.MIN_VALUE;
		else {
			sort();
			return sortedData.longAt(index);
		}
	}

	/**
	 * Get the number of values in interval
	 * @param data
	 * @param timeStartCurrent
	 * @param timeEndCurrent
	 * @return number of values in interval
	 */
	public double getNumberOfValues(	// TODO  TPS: add this method on ListOfLong class.
			ListOfLong data,
			int timeStartCurrent,
			int timeEndCurrent) {
		double numberOfValues = 0;
		for(Long longTmp : data){
			if((longTmp >= timeStartCurrent) && (longTmp < timeEndCurrent)){
				numberOfValues ++;
			}
			if(longTmp >= timeEndCurrent){
				break;
			}
		}
		return numberOfValues;
	}



}
