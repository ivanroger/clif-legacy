/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * CLIF $Name:  $
 *
 * Contact: clif@ow2.org
 */
package org.ow2.clif.analyze.statistics.util.data;

import java.util.Collections;

/**
 * summarize long data set (durations) measured within a elapsed time slice
 * @author Guy Vachet
 */
public class SliceSummary {
    private long minTimestamp;
    private long maxTimestamp;
    private int dataNb;
    private long minData = Long.MIN_VALUE;
    private long maxData = Long.MIN_VALUE;
    private long dataMedian = Long.MIN_VALUE;
    private double dataMean = Double.NaN;
    private double dataStd = Double.NaN;

    /**
     * @param minTime the min time stamp of elapsed time (included)
     * @param maxTime the max time stamp of elapsed time (excluded)
     * @param data is a set of times (response times) to be summarize
     */
    public SliceSummary(long minTime, long maxTime, ListOfLong data) {
        minTimestamp = minTime;
        maxTimestamp = maxTime;
        summarizeData(data);
    }

    /**
     * compute data in order to get min, max, median and..
     */
    private void summarizeData(ListOfLong data) {
        dataNb = data.size();
        if (dataNb == 0)
            return;
        else if (dataNb == 1) {
            minData = data.longAt(0);
            maxData = minData;
            dataMedian = minData;
            dataMean = minData;
        } else {
            Collections.sort(data);
            minData = data.longAt(0);
            maxData = data.longAt(dataNb - 1);
            if ((dataNb % 2) == 0) {  // median is the mean of lower and upper
                dataMedian = data.longAt((dataNb / 2) - 1);
                dataMedian = (dataMedian + data.longAt(dataNb / 2)) / 2;
            } else
                dataMedian = data.longAt((dataNb - 1) / 2);
            dataMean = data.getSum() / dataNb;
            double sq = data.getSum() * data.getSum() / dataNb;
            dataStd = Math.sqrt((data.getSquareSum() - sq) / (dataNb - 1));
        }
    }

    /**
     * @return the lower timestamp within the slice (included)
     */
    public long getMinTimestamp() {
        return minTimestamp;
    }

    /**
     * @return the upper timestamp of within the slice (excluded)
     */
    public long getMaxTimestamp() {
        return maxTimestamp;
    }

    /**
     * @return the data (that is durations) number within the slice
     */
    public int getDataNb() {
        return dataNb;
    }

    /**
     * @return min value of durations
     */
    public long getMinData() {
        return minData;
    }

    /**
     * @return max value of durations
     */
    public long getMaxData() {
        return maxData;
    }

    /**
     * @return median of durations
     */
    public long getDataMedian() {
        return dataMedian;
    }

    /**
     * @return average of durations
     */
    public double getDataMean() {
        return dataMean;
    }

    /**
     * @return standard deviation of durations
     */
    public double getDataStd() {
        return dataStd;
    }

    public String displayStatistics() {
        StringBuffer sb = new StringBuffer("[").append(minTimestamp / 1000);
        sb.append("..[").append(maxTimestamp / 1000).append("\t").append(dataNb);
        sb.append("\t").append(dataMedian).append("\t");
        sb.append(Math4Long.round(dataMean, dataNb)).append("\t");
        sb.append(Math4Long.round(dataStd, dataNb)).append("\t");
        return sb.append(minData).append("\t").append(maxData).toString();
    }

    @Override
	public String toString() {
        StringBuffer sb = new StringBuffer("[").append(minTimestamp / 1000);
        sb.append("..[").append(maxTimestamp / 1000).append("\t").append(dataNb);
        return sb.append("\t").append(dataMedian).toString();
    }
}
