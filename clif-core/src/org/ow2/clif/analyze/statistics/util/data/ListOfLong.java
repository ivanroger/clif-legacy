/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * CLIF $Name:  $
 *
 * Contact: clif@ow2.org
 */
package org.ow2.clif.analyze.statistics.util.data;

import java.util.ArrayList;

/**
 * ListOfLong : a list of long with convenient management tools and stuff for
 * statistics as sum, squareSum. Should be more up to date than LongVector.
 * @author Guy Vachet
 */
public class ListOfLong extends ArrayList<Long> {
	
	private static final long serialVersionUID = 2240927757154209233L;
	// statistics stuff
    private double sum;
    private double squareSum;

    /**
     * constructor of an empty ListOfLong.
     */
    public ListOfLong() {
        super();
    }

    /**
     * Constructs an empty list of long with the specified initial capacity.
     * @param initialCapacity
     */
    public ListOfLong(int initialCapacity) {
        super(initialCapacity);
    }

    /**
     * Constructs a list of long containing the elements of the specified
     * collection, in the order they are returned by the collection's iterator.
     * @param listOfLong
     */
    public ListOfLong(ListOfLong listOfLong) {
        super(listOfLong);
        setSum(listOfLong.getSum());
        setSquareSum(listOfLong.getSquareSum());
    }

    /**
     * resets ListOfLong
     */
    public void reset() {
        clear();
        sum = 0;
        squareSum = 0;
    }

    /**
     * adds a long element
     * @param longValue the long to addLong
     */
    public void addLong(long longValue) {
        add(new Long(longValue));
        sum += longValue;
        squareSum += longValue * longValue;
    }

    /**
     * adds all element
     * @param listOfLong the ListOfLong to addLong
     */
    public void addAll(ListOfLong listOfLong) {
        super.addAll(listOfLong);
        sum += listOfLong.getSum();
        squareSum += listOfLong.getSquareSum();
    }

    /**
     * remove long value at the specified position in this list.
     * @param index the position
     */
    public void removeLongAt(int index) {
        long longValue = (remove(index)).longValue();
        sum -= longValue;
        squareSum -= longValue * longValue;
    }

    /**
     * long value at the specified position in this list.
     * @param index the position
     * @return long at this position
     */
    public long longAt(int index) {
        return (get(index)).longValue();
    }

    /**
     * gets the list sum
     * @return double sum
     */
    public double getSum() {
        return sum;
    }

    public void setSum(double sum) {
        this.sum = sum;
    }

    /**
     * gets the list squareSum
     * @return double squareSum
     */
    public double getSquareSum() {
        return squareSum;
    }

    public void setSquareSum(double squareSum) {
        this.squareSum = squareSum;
    }

    /**
     * <b>toString</b> defines the string representative of all long values
     * as an Array of supplied number of columns.
     * @param numberOfColumn the number of columns to format the values.
     * @return string representative of all values as an Array of "n" columns.
     */
    public String toString(int numberOfColumn) {
        if (numberOfColumn > 1) {
            int cr = numberOfColumn - 1;
            StringBuffer sb = new StringBuffer();
            for (int i = 0; i < size(); i++) {
                sb.append(longAt(i));
                if ((i % numberOfColumn) == cr)
                    sb.append("\n");
                else
                    sb.append("\t");
            }
            if ((size() % numberOfColumn) > 0)
                sb.append("\n");
            return sb.toString();
        } else
            return toString();
    }

    /**
     * <b>toString</b> defines the string representative of all long values.
     * @return string representative of all values (one value per line).
     */
    @Override
	public String toString() {
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < size(); i++)
            sb.append(longAt(i)).append("\n");
        return sb.toString();
    }
    

}
