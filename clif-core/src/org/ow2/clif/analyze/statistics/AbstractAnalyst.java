/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * CLIF $Name:  $
 *
 * Contact: clif@ow2.org
 */
package org.ow2.clif.analyze.statistics;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;


import org.ow2.clif.analyze.statistics.profiling.Datum;
import org.ow2.clif.analyze.statistics.util.data.ListOfLong;
import org.ow2.clif.analyze.statistics.util.data.LongStatistics;
import org.ow2.clif.analyze.statistics.util.data.SliceSummary;
import org.ow2.clif.storage.api.EventFilter;
import org.ow2.clif.supervisor.api.ClifException;

/**
 * Generic class dedicated to analyze data of CLIF blades
 * 
 * @author Guy Vachet
 */
public abstract class AbstractAnalyst implements Constants {

	private String label = null;
	private Map<String, List<Datum>> resultsOfBlades = null;
	private double statSortFactor = DEFAULT_STATISTICAL_SORT_FACTOR;
	private double statSortRatio = DEFAULT_STATISTICAL_SORT_RATIO;

	public void setLabel(String label) {
		this.label = label;
	}

	public String getLabel() {
		return label;
	}

	public void setStatSortFactor(double sortFactor) {
		statSortFactor = sortFactor;
	}

	public double getStatSortFactor() {
		return statSortFactor;
	}

	public void setStatSortRatio(double sortRatio) {
		statSortRatio = sortRatio;
	}

	public double getStatSortRatio() {
		return statSortRatio;
	}

	public void addBladeData(String bladeId, List<Datum> results) {
		if (resultsOfBlades == null) {
			resultsOfBlades = new TreeMap<String, List<Datum>>();
		}
		resultsOfBlades.put(bladeId, results);
	}

	public boolean isEmpty() {
		if (resultsOfBlades == null)
			return true;
		return resultsOfBlades.isEmpty();
	}

	public String[] getBladeIdentifiers() {
		if (resultsOfBlades != null) {
			Set<String> probeIds = resultsOfBlades.keySet();
			return probeIds.toArray(new String[probeIds.size()]);
		}
		return new String[0];
	}

	public void removeBladeData(String bladeId) {
		resultsOfBlades.remove(bladeId);
	}

	public ProfilingStatistics getProfilingStatistics(String bladeId) {
		if ((resultsOfBlades != null) && resultsOfBlades.containsKey(bladeId)) {
			return new ProfilingStatistics(resultsOfBlades.get(bladeId));
		}
		return null;
	}

	abstract public void addProfilingData(BladeStoreReader r, EventFilter f)
			throws ClifException;

	abstract public void outputAnalysis(boolean detailed, long sliceSize);

	/**
	 * Cette classe stocke les mesures (de type long) de CLIF pour les analysts
	 * et offre les calculs statistiques pour ce type de données.
	 */
	protected class ProfilingStatistics {
		// Profiling data (extracted by a reader) to be analyzed
		private List<Datum> profilingData = null;
		private double sortFactor = 0;
		private double sortRatio = 0;

		/**
		 * default constructor
		 */
		protected ProfilingStatistics() {
			setSortFactor(getStatSortFactor());
			setSortRatio(getStatSortRatio());
		}

		/**
		 * constructor
		 */
		protected ProfilingStatistics(List<Datum> profilingData) {
			this();
			setProfilingData(profilingData);
		}

		public void setProfilingData(List<Datum> data) {
			profilingData = data;
		}

		public void addProfilingData(List<Datum> data) {
			if (profilingData == null) {
				setProfilingData(data);
			} else {
				profilingData.addAll(data);
			}
		}

		public List<Datum> getProfilingData() {
			return profilingData;
		}

		public boolean isEmpty() {
			if (profilingData == null)
				return true;
			return profilingData.isEmpty();
		}

		public int size() {
			return profilingData.size();
		}

		public void setSortFactor(double statSortFactor) {
			this.sortFactor = statSortFactor;
		}

		public double getSortFactor() {
			return sortFactor;
		}

		public void setSortRatio(double statSortRatio) {
			this.sortRatio = statSortRatio;
		}

		public double getSortRatio() {
			return sortRatio;
		}

		public long getMinTimestamp() {
			if ((profilingData == null) || profilingData.isEmpty())
				return -1;
			return (profilingData.get(0)).getDate();
		}

		public long getMaxTimestamp() {
			if ((profilingData == null) || profilingData.isEmpty())
				return -1;
			return (profilingData.get(profilingData.size() - 1))
					.getDate();
		}

		/**
		 * get LongStatistics of durations to perform statistical analysis
		 * 
		 * @return LongStatistics of durations measured during loading test
		 */
		public LongStatistics getLongResults() {
			final List<Datum> data = getProfilingData();
			final int dataSize = data.size();
			LongStatistics longResults = new LongStatistics(dataSize);
			for (int index = 0; index < dataSize; index++) {
				longResults.addLong((data.get(index)).getResult());
			}
			longResults.setStatisticalSortFactor(sortFactor);
			longResults.setStatisticalSortPercentage(sortRatio);
			return longResults;
		}

		/**
		 * list of the max index for time slices (times are timestamps).
		 * 
		 * @return all last indexes of each elapsed time slice
		 */
		public int[] getMaxSliceIndexesOfProfilingData(long sliceSize) {
			List<Datum> data = getProfilingData();
			List<Integer> maxSliceIndexes = new ArrayList<Integer>();
			long timeThreshold = sliceSize;
			int size = data.size();
			for (int index = 0; index < size; index++) {
				if ((data.get(index)).getDate() >= timeThreshold) {
					maxSliceIndexes.add(new Integer(index - 1));
					timeThreshold += sliceSize;
				}
			}
			// and add the last but not least index
			maxSliceIndexes.add(new Integer(size - 1));
			size = maxSliceIndexes.size();
			int[] sliceIndexes = new int[size];
			for (int i = 0; i < size; i++)
				sliceIndexes[i] = (maxSliceIndexes.get(i)).intValue();
			return sliceIndexes;
		}

		// //////////////////////////////////////
		// Slicing of statistical-sorted data //
		// //////////////////////////////////////

		/**
		 * analyze profile long results function of elapsed time.
		 */
		public List<SliceSummary> getHistoryOfStatLongResults(
				LongStatistics longRes, long sliceSize) {
			int sliceId = 0, size;
			long duration, minDuration, maxDuration, minTime = 0, maxTime;
			int[] maxIndexes = null;
			List<SliceSummary> history = null;
			ListOfLong subDurations = new ListOfLong();
			maxTime = sliceSize;
			size = longRes.size();
			maxIndexes = getMaxSliceIndexesOfProfilingData(sliceSize);
			history = new ArrayList<SliceSummary>(maxIndexes.length);
			minDuration = longRes.getMinStatSortValue();
			maxDuration = longRes.getMaxStatSortValue();
			for (int index = 0; index < size; index++) {
				duration = longRes.getLongAt(index);
				if ((duration >= minDuration) && (duration <= maxDuration))
					subDurations.addLong(duration);
				if (index == maxIndexes[sliceId]) {
					// got the last index of the current slice, so summarize it
					history
							.add(new SliceSummary(minTime, maxTime,
									subDurations));
					minTime = maxTime;
					maxTime += sliceSize;
					subDurations.reset();
					sliceId++;
				}
			}
			return history;
		}

		// ////////////////////////////////////////////
		// Slicing of Data without statistical sort //
		// ////////////////////////////////////////////

		/**
		 * analyze raw profile long results function of elapsed time.
		 */
		public List<SliceSummary> getHistoryOfRawLongResults(
				LongStatistics longRes, long sliceSize) {
			int sliceId = 0, size;
			long minTime = 0, maxTime = sliceSize;
			int[] maxIndexes = null;
			List<SliceSummary> history = null;
			ListOfLong subDurations = new ListOfLong();
			maxIndexes = getMaxSliceIndexesOfProfilingData(sliceSize);
			history = new ArrayList<SliceSummary>(maxIndexes.length);
			size = longRes.size();
			for (int index = 0; index < size; index++) {
				subDurations.addLong(longRes.getLongAt(index));
				if (index == maxIndexes[sliceId]) {
					// got the last index of the current slice, so summarize it
					history.add(new SliceSummary(minTime, maxTime, subDurations));
					minTime = maxTime;
					maxTime += sliceSize;
					subDurations.reset();
					sliceId++;
				}
			}
			return history;
		}

	}
}
