/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * CLIF $Name:  $
 *
 * Contact: clif@ow2.org
 */
package org.ow2.clif.analyze.statistics;

import org.ow2.clif.analyze.statistics.profiling.ActionDatum;
import org.ow2.clif.analyze.statistics.profiling.Datum;
import org.ow2.clif.analyze.statistics.util.data.LongStatistics;
import org.ow2.clif.analyze.statistics.util.data.Math4Long;
import org.ow2.clif.analyze.statistics.util.data.SliceSummary;
import org.ow2.clif.storage.api.BladeEvent;
import org.ow2.clif.storage.api.EventFilter;
import org.ow2.clif.supervisor.api.ClifException;

import java.util.*;
import java.util.regex.Pattern;

/**
 * Analyze one or more CLIF injectors: first of all, agglomerate action blades ;
 * check success of any action ; compute data in order to get statistical stuff
 * 
 * @author Guy Vachet
 */
public class InjectorAnalyst extends AbstractAnalyst {
	// verbose mode for verification
	private static final boolean VERBOSE = false;
	public static final String TYPE_LABEL = ACTION_EVENT_TYPE_LABEL;
	public static final String FIELD_LABEL = DURATION_EVENT_FIELD_LABEL;
	public static final String CTRL_FIELD = SUCCESS_EVENT_FIELD_LABEL;
	public static final String RESULT_FIELD = RESULT_EVENT_FIELD_LABEL;
	// regular expression to check results
	private String regexCtrl = DEFAULT_REGEX_CTRL_ACTION;

	/**
	 * default constructor
	 */
	public InjectorAnalyst() {
		setLabel(TYPE_LABEL);
		StringBuffer sb = new StringBuffer(getLabel());
		System.out.println(sb.append(" analysis:"));
	}

	public InjectorAnalyst(String analyzeRange) {
		setLabel(TYPE_LABEL);
		StringBuffer sb = new StringBuffer(getLabel());
		System.out.println(sb.append(" analysis").append(analyzeRange));
	}

	@Override
	public void addProfilingData(BladeStoreReader reader, EventFilter filter)
			throws ClifException {
		String bladeId = reader.getBladeDescriptor().getId();
		List<Datum> actionData = new ActionReader(reader).getProfilingData(filter);
		addBladeData(bladeId, actionData);
		if (VERBOSE) {
			StringBuffer sb = new StringBuffer("\tadd ");
			sb.append(actionData.size()).append(" data from ");
			System.out.println(sb.append(bladeId));
		}
	}

	public void setRegexCtrl(String regexCtrl) {
		this.regexCtrl = regexCtrl;
	}

	public String getRegexCtrl() {
		return regexCtrl;
	}

	/**
	 * join profiling data from several CLIF injectors
	 * 
	 * @return flattened data sorted by date
	 */
	public ProfilingStatistics flattenProfilingStatistics() {
		String[] ids = getBladeIdentifiers();
		List<Datum> flatData = new ArrayList<Datum>();
		for (int i = 0; i < ids.length; i++) {
			flatData.addAll(getProfilingStatistics(ids[i]).getProfilingData());
			if (VERBOSE) {
				StringBuffer sb = new StringBuffer("\tflatten ");
				sb.append(getProfilingStatistics(ids[i]).getProfilingData().size());
				System.out.println(sb.append(" data from ").append(ids[i]));
			}
			removeBladeData(ids[i]);
		}
		// now sort data by date because of several CLIF injection
		Collections.sort(flatData);
		return new ProfilingStatistics(flatData);
	}

	/**
	 * keep data that has not raised an CLIF error during load injection
	 */
	public void checkProfilingData(ProfilingStatistics profilingStat) {
		List<Datum> profilingData, checkedData;
		ActionDatum datum = null;
		int size;
		profilingData = profilingStat.getProfilingData();
		size = profilingData.size();
		checkedData = new ArrayList<Datum>(size);
		for (int index = 0; index < size; index++) {
			datum = (ActionDatum) profilingData.get(index);
			if (datum.isOk()) {
				checkedData.add(datum);
			}
		}
		if (checkedData.size() < size) {
			if (checkedData.size() > 0) {
				StringBuffer sb = new StringBuffer("WARNING: Request failures = \t");
				sb.append(Math4Long.displayDouble(100.0 * (size - checkedData.size()) / size, 1));
				System.out.println(sb.append("\t%"));
			} else
				System.out.println("PANIC: All requests failed during load");
		}
		profilingStat.setProfilingData(checkedData);
	}

	private List<Long> getHistoryOfLoadRate(ProfilingStatistics profStat,
			LongStatistics durations, long sliceSize) {
		List<Long> loadRates = new ArrayList<Long>();
		List<SliceSummary> histories;
		histories = profStat.getHistoryOfRawLongResults(durations, sliceSize);
		double rate;
		for (Iterator<SliceSummary> iter = histories.iterator(); iter.hasNext();) {
			rate = (iter.next()).getDataNb() * 1000.0 / sliceSize;
			loadRates.add(new Long(Math.round(rate)));
		}
		return loadRates;
	}

	/**
	 */
	public void outputHistoryOfActionResults(ProfilingStatistics profStat,
			LongStatistics durations, long sliceSize) {
		Iterator<SliceSummary> sliceIterator;
		Iterator<Long> rateIterator;
		SliceSummary slice;
		long midDate;
		StringBuffer sb = new StringBuffer("Statistical-sort data vs elapsed time");
		sb.append(" (slicing size: ").append(sliceSize / 1000).append(" s) and load rate");
		sb.append("\nRange(s)\tNumber\tMedian(ms)\t\tdate(s)\tload(rq/s)\n");
		sliceIterator = 
			profStat.getHistoryOfStatLongResults(durations, sliceSize).iterator();
		rateIterator = 
			getHistoryOfLoadRate(profStat, durations, sliceSize).iterator();
		for (; sliceIterator.hasNext();) {
			slice = sliceIterator.next();
			midDate = slice.getMinTimestamp() + slice.getMaxTimestamp();
			sb.append(slice).append("\t-\t").append(midDate / 2000);
			if (rateIterator.hasNext())
				sb.append("\t").append((rateIterator.next()).longValue());
			sb.append("\n");
		}
		System.out.println(sb);
	}

	/**
	 * 
	 * @param isDetailed
	 *            if true display more analysis
	 * @param sliceSize
	 *            size of elapsed time in order to analyze sub-population
	 */
	@Override
	public void outputAnalysis(boolean isDetailed, long sliceSize) {
		ProfilingStatistics profilingStat = null;
		LongStatistics durations;
		// stage period should be part of elapsed time
		long stagePeriod;
		StringBuffer sb;
		// first of all, flatten data from all injection blades
		profilingStat = flattenProfilingStatistics();
		if (profilingStat.isEmpty()) {
			System.out.println("No action data to analyze..");
			return;
		}
		// check data consistency (successful requests)
		checkProfilingData(profilingStat);
		if (profilingStat.isEmpty()) {
			System.out.println("\nNo successful request to analyze, so ..");
			return;
		}
		sb = new StringBuffer("Analyze ").append(profilingStat.size());
		sb.append(" requests that are sent within ");
		stagePeriod = profilingStat.getMaxTimestamp() - profilingStat.getMinTimestamp();
		sb.append(Math4Long.displayMillisecTime(stagePeriod));
		System.out.println(sb.append("\n"));
		// build a statistical long list in order to analyze durations
		durations = profilingStat.getLongResults();
		if (isDetailed) {
			System.out.println("Duration statistics (in milliseconds):");
			durations.outputStatistics("time", isDetailed);
			durations.outputStatisticalSortDataFrequency(INJECTOR_FREQUENCY_CLASS_NUMBER);
			durations.outputStatisticalSortDataQuantiles(INJECTOR_QUANTILE_CLASS_NUMBER);
			outputHistoryOfActionResults(profilingStat, durations, sliceSize);
		} else {
			sb = new StringBuffer("Request duration median: \t");
			sb.append(durations.getStatSortMedian());
			sb.append("\tmilliseconds (based on ");
			sb.append(durations.getStatSortDataNumber());
			System.out.println(sb.append(" data)."));
		}
	}

	/**
	 */
	class ActionReader extends BladeDatumReader {

		private Pattern pattern = null;

		public ActionReader(BladeStoreReader bladeReader) {
			setBladeStoreReader(bladeReader);
			setEventTypeLabel(TYPE_LABEL);
			pattern = Pattern.compile(getRegexCtrl());
		}

		/**
		 * @param event
		 * @param minTime
		 * @return
		 */
		@Override
		public Datum convert2Datum(BladeEvent event, long minTime) {
			long duration;
			boolean ctrl = false;
			String result = null;
			duration = ((Integer) event.getFieldValue(FIELD_LABEL)).intValue();
			// At least, checks the success field BUT some application 
			// errors can be detected thru the result field
			if (((Boolean) event.getFieldValue(CTRL_FIELD)).booleanValue()) {
				// A dummy control of action result can be:
				//result = event.getFieldValue(RESULT_FIELD).toString();
				//ctrl = result.getClass().getName()
				//				.equalsIgnoreCase("java.lang.String");
				result = event.getFieldValue(RESULT_FIELD).toString();
				if (pattern.matcher(result).matches()) {
					ctrl = true;
				} else {
					ctrl = pattern.split(result, -1).length > 1;
				}
			}
			return new ActionDatum(event.getDate() - minTime, duration, ctrl);
		}

	}

}
