/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2017 Orange SA
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * Contact: clif@ow2.org
 */
package org.ow2.isac.plugin.jsonhandler;

import org.ow2.clif.scenario.isac.plugin.SessionObjectAction;
import java.util.ArrayDeque;
import java.util.Deque;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.ow2.clif.scenario.isac.plugin.ControlAction;
import org.ow2.clif.scenario.isac.exception.IsacRuntimeException;
import org.ow2.clif.scenario.isac.plugin.DataProvider;
import com.jayway.jsonpath.Configuration;
import com.jayway.jsonpath.JsonPath;
import com.jayway.jsonpath.Option;
import com.jayway.jsonpath.PathNotFoundException;
import org.ow2.clif.scenario.isac.plugin.TestAction;

/**
 * Implementation of a session object for plugin ~JsonHandler~,
 * an ISAC plug-in supporting JSON text parsing using JSON path
 * expressions.
 * @see <a href="http://goessner.net/articles/JsonPath/">Json Path reference article</a>
 */
public class SessionObject implements SessionObjectAction, ControlAction, DataProvider, TestAction {

	static final int TEST_ISDEFINED = 3;
	static final String TEST_ISDEFINED_RESULTSET = "resultset";

	static final int TEST_ISNOTDEFINED = 4;
	static final String TEST_ISNOTDEFINED_RESULTSET = "resultset";

	static final int CONTROL_PARSE = 0;
	static final String CONTROL_PARSE_RESULTSET = "resultset";
	static final String CONTROL_PARSE_JSONPATH = "jsonpath";

	static final int CONTROL_SETJSONCONTENT = 1;
	static final String CONTROL_SETJSONCONTENT_JSONCONTENT = "jsoncontent";

	static final int CONTROL_CLEARALL = 2;

	static final int CONTROL_CLEARRESULTSET = 5;
	static final String CONTROL_CLEARRESULTSET_RESULTSET = "resultset";

	static final int CONTROL_NEXTRESULT = 8;
	static final String CONTROL_NEXTRESULT_RESULTSET = "resultset";

	String jsonContent = "";
	Map<String,Deque<String>> parseResults = new HashMap<String,Deque<String>>();


	//////////////////
	// Constructors //
	//////////////////


	/**
	 * Constructor for specimen object: does nothing
	 *
	 * @param params this plug-in takes no import parameter
	 */
	public SessionObject(Map<String,String> params)
	{
	}

	/**
	 * Copy constructor (clone specimen object to get session object):
	 * does nothing.
	 *
	 * @param so specimen object to clone
	 */
	private SessionObject(SessionObject so)
	{
	}


	////////////////////////////////////////
	// SessionObjectAction implementation //
	////////////////////////////////////////


	/**
	 * Just calls copy constructor.
	 * @see org.ow2.clif.scenario.isac.plugin.SessionObjectAction#createNewSessionObject()
	 */
	public Object createNewSessionObject()
	{
		return new SessionObject(this);
	}

	/**
	 * Sets JSON content and the map of parse result sets to null
	 * @see org.ow2.clif.scenario.isac.plugin.SessionObjectAction#close()
	 */
	public void close()
	{
		jsonContent = null;
		parseResults = null;
	}

	/**
	 * Resets JSON content to empty string and discards parse result sets. 
	 * @see org.ow2.clif.scenario.isac.plugin.SessionObjectAction#reset()
	 */
	public void reset()
	{
		jsonContent = "";
		parseResults.clear();
	}


	//////////////////////////////////
	// ControlAction implementation //
	//////////////////////////////////


	/**
	 * @see org.ow2.clif.scenario.isac.plugin.ControlAction#doControl()
	 */
	public void doControl(int number, Map<String, String> params)
	{
		switch (number)
		{
			case CONTROL_NEXTRESULT:
				doControlNextResult(params);
				break;
			case CONTROL_CLEARRESULTSET:
				parseResults.remove(params.get(CONTROL_CLEARRESULTSET_RESULTSET));
				break;
			case CONTROL_CLEARALL:
				reset();
				break;
			case CONTROL_SETJSONCONTENT:
				jsonContent = params.get(CONTROL_SETJSONCONTENT_JSONCONTENT);
				break;
			case CONTROL_PARSE:
				doControlParse(params);
				break;
			default:
				throw new Error(
					"Unable to find this control in ~JsonHandler~ ISAC plugin: "
					+ number);
		}
	}


	private void doControlNextResult(Map<String, String> params)
	{
		Deque<String> resultSet = parseResults.get(params.get(CONTROL_NEXTRESULT_RESULTSET));
		if (resultSet == null)
		{
			throw new IsacRuntimeException("Result set " + params.get(CONTROL_NEXTRESULT_RESULTSET) + " is not defined.");
		}
		resultSet.pollFirst();
		if (resultSet.isEmpty())
		{
			parseResults.remove(params.get(CONTROL_NEXTRESULT_RESULTSET));
		}
		
	}


	private void doControlParse(Map<String, String> params)
	{
		String jsonPath = params.get(CONTROL_PARSE_JSONPATH);
		JsonPath parser = JsonPath.compile(jsonPath);
		Configuration conf = Configuration.builder().options(Option.ALWAYS_RETURN_LIST).build();
		ArrayDeque<String> resultSet = null;
		String resultSetName = params.get(CONTROL_PARSE_RESULTSET);
		try
		{
			List<Object> resultObjects = parser.read(jsonContent, conf);
			resultSet = new ArrayDeque<String>(resultObjects.size());
			for (Object oneResultObj : resultObjects)
			{
				resultSet.add(oneResultObj.toString());
			}
		}
		catch (PathNotFoundException ex)
		{
			// no result: nothing special to do
		}
		if (resultSetName != null && !resultSetName.isEmpty())
		{
			if (resultSet == null || resultSet.isEmpty())
			{
				parseResults.remove(resultSetName);
			}
			else
			{
				parseResults.put(resultSetName, resultSet);
			}
		}
	}


	/////////////////////////////////
	// DataProvider implementation //
	/////////////////////////////////


	/**
	 * @see org.ow2.clif.scenario.isac.plugin.DataProvider#doGet()
	 */
	public String doGet(final String var)
	{
		String result = null;
		Deque<String> resultSet = null;
		if (var.startsWith("#"))
		{
			 // return the number of results in the result set
			resultSet = parseResults.get(var.substring(1));
		}
		else
		{
			resultSet = parseResults.get(var);
		}
		if (resultSet != null)
		{
			if (var.startsWith("#"))
			{
				result = String.valueOf(resultSet.size());
			}
			else
			{
				result = resultSet.peek();
			}
		}
		if (result == null)
		{
			throw new IsacRuntimeException(
				"Undefined variable in ~JsonHandler~ ISAC plugin: " + var);
		}
		return result;
	}


	///////////////////////////////
	// TestAction implementation //
	///////////////////////////////


	/**
	 * @see org.ow2.clif.scenario.isac.plugin.TestAction#doTest()
	 */
	public boolean doTest(int number, Map<String, String> params)
	{
		String resultSetName;
		switch (number)
		{
			case TEST_ISNOTDEFINED:
				resultSetName = params.get(TEST_ISNOTDEFINED_RESULTSET);
				return !parseResults.containsKey(resultSetName);
			case TEST_ISDEFINED:
				resultSetName = params.get(TEST_ISDEFINED_RESULTSET);
				return parseResults.containsKey(resultSetName);
			default:
				throw new Error(
					"Unable to find this test in ~JsonHandler~ ISAC plugin: "
					+ number);
		}
	}
}
