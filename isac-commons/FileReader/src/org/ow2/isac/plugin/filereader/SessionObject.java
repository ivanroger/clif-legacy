/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2006, 2012 France Telecom
 * Copyright (C) 2015 Orange
 * 
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option) any
 * later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 * Contact: clif@ow2.org
 */

package org.ow2.isac.plugin.filereader;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.Map;
import org.ow2.clif.scenario.isac.exception.IsacRuntimeException;
import org.ow2.clif.scenario.isac.plugin.DataProvider;
import org.ow2.clif.scenario.isac.plugin.SessionObjectAction;
import org.ow2.clif.scenario.isac.util.ParameterParser;
import org.ow2.clif.util.ClifClassLoader;
import org.ow2.clif.scenario.isac.plugin.ControlAction;
import org.ow2.clif.scenario.isac.plugin.TestAction;

/**
 * @author Bruno Dillenseger
 */
public class SessionObject implements DataProvider, SessionObjectAction, ControlAction, TestAction
{
	static final int TEST_ISSET = 1;
	static final int TEST_ISNOTSET = 2;
	static final int CONTROL_LOAD = 0;
	static final String CONTROL_LOAD_CHARSET = "charset";
	static final String CONTROL_LOAD_FILENAME = "filename";
	static final int CONTROL_CLEAR = 3;
	static final String PLUGIN_CHARSET = "charset";
	static final String PLUGIN_FILENAME = "filename";


	static private String readData(String filename, String charset)
	throws IsacRuntimeException
	{
		String data;
		InputStream in = ClifClassLoader.getClassLoader().getResourceAsStream(filename);
		if (in == null)
		{
			throw new IsacRuntimeException("Plug-in ~FileReader~ can't find file " + filename);
		}
		else
		{
			ByteArrayOutputStream out = new ByteArrayOutputStream();
			try
			{
				int c;
				while ((c = in.read()) != -1)
				{
					out.write(c);
				}
			}
			catch (IOException ex)
			{
				throw new IsacRuntimeException("Plug-in ~FileReader~ can't read file " + filename, ex);
			}
			if (charset == null || charset.trim().isEmpty() || charset.equals("default"))
			{
				data = out.toString();
			}
			else
			{
				try
				{
					data = out.toString(charset);
				}
				catch (UnsupportedEncodingException ex)
				{
					throw new IsacRuntimeException("Plug-in ~FileReader~ can't apply character encoding " + charset);
				}
			}
			return data;
		}
	}


	private final String importData;
	private String data = null;


	public SessionObject(Map<String,String> params)
	throws IsacRuntimeException
	{
		String filename = params.get(PLUGIN_FILENAME).trim();
		if (filename == null || filename.isEmpty())
		{
			importData = null;
		}
		else
		{
			importData = readData(filename, ParameterParser.getCombo(params.get(PLUGIN_CHARSET)));
		}
	}


	private SessionObject(final SessionObject toClone)
	{
		data = importData = toClone.importData;
	}


	////////////////////////////
	// DataProvider interface //
	////////////////////////////


	public String doGet(String dummy)
	throws IsacRuntimeException
	{
		if (data != null)
		{
			return data;
		}
		else
		{
			throw new IsacRuntimeException("This instance of plug-in ~FileReader~ is not set.");
		}
	}


	///////////////////////////////////
	// SessionObjectAction interface //
	///////////////////////////////////


	public Object createNewSessionObject()
	{
		return new SessionObject(this);
	}

	
	public void close()
	{
	}

	public void reset()
	{
		data = importData;
	}

	
	//////////////////////////////////
	// ControlAction implementation //
	//////////////////////////////////


	/**
	 * @see org.ow2.clif.scenario.isac.plugin.ControlAction#doControl()
	 */
	public void doControl(int number, Map<String, String> params)
	throws IsacRuntimeException
	{
		switch (number)
		{
			case CONTROL_CLEAR:
				data = null;
				break;
			case CONTROL_LOAD:
				data = null;
				data = readData(
					params.get(CONTROL_LOAD_FILENAME),
					ParameterParser.getCombo(params.get(CONTROL_LOAD_CHARSET)));
				break;
			default:
				throw new Error(
					"Unable to find this control in ~FileReader~ ISAC plugin: "
					+ number);
		}
	}

	
	///////////////////////////////
	// TestAction implementation //
	///////////////////////////////


	/**
	 * @see org.ow2.clif.scenario.isac.plugin.TestAction#doTest()
	 */
	public boolean doTest(int number, Map<String, String> params)
	{
		switch (number)
		{
			case TEST_ISNOTSET:
				return data == null;
			case TEST_ISSET:
				return data != null;
			default:
				throw new Error(
					"Unable to find this test in ~FileReader~ ISAC plugin: "
					+ number);
		}
	}
}
