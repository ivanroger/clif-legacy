/*
* CLIF is a Load Injection Framework
* Copyright (C) 2016 Orange SA
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 3 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
* Contact: clif@ow2.org
*/

package org.ow2.isac.plugin.filehandler;

import org.ow2.clif.scenario.isac.plugin.SessionObjectAction;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.NoSuchFileException;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.Map;
import org.ow2.clif.scenario.isac.plugin.TestAction;
import org.ow2.clif.scenario.isac.exception.IsacRuntimeException;
import org.ow2.clif.scenario.isac.plugin.ControlAction;
import org.ow2.clif.scenario.isac.util.ParameterParser;
import org.ow2.clif.scenario.isac.plugin.DataProvider;

/**
 * Implementation of a session object for plugin ~FileHandler~.
 * FileHandler session objects are stateless.
 * @author Bruno Dillenseger
 */
public class SessionObject implements SessionObjectAction, TestAction, ControlAction, DataProvider
{
	static final String VAR_SIZE = "size";
	static final String VAR_CTIME = "ctime";
	static final String VAR_MTIME = "mtime";
	static final int TEST_EXISTS = 0;
	static final String TEST_EXISTS_PATH = "path";
	static final int TEST_EXISTSNOT = 1;
	static final String TEST_EXISTSNOT_PATH = "path";
	static final int TEST_READABLE = 2;
	static final String TEST_READABLE_PATH = "path";
	static final int TEST_WRITEABLE = 3;
	static final String TEST_WRITEABLE_PATH = "path";
	static final int TEST_NOTREADABLE = 4;
	static final String TEST_NOTREADABLE_PATH = "path";
	static final int TEST_NOTWRITEABLE = 5;
	static final String TEST_NOTWRITEABLE_PATH = "path";
	static final int TEST_ISDIR = 6;
	static final String TEST_ISDIR_PATH = "path";
	static final int TEST_ISFILE = 7;
	static final String TEST_ISFILE_PATH = "path";
	static final int TEST_ISLINK = 8;
	static final String TEST_ISLINK_PATH = "path";
	static final int TEST_ISNOTLINK = 9;
	static final String TEST_ISNOTLINK_PATH = "path";
	static final int CONTROL_DELETEFILES = 10;
	static final String CONTROL_DELETEFILES_PATHS = "paths";
	static final int CONTROL_DELETEEMPTYDIR = 11;
	static final String CONTROL_DELETEEMPTYDIR_PATH = "path";

	/**
	 * Constructor for specimen object.
	 * Does nothing (no import parameter).
	 *
	 * @param params key-value pairs for plugin parameters
	 */
	public SessionObject(Map<String,String> params)
	{
	}

	/**
	 * Copy constructor (clone specimen object to get session object).
	 * Does nothing.
	 * @param so specimen object to clone
	 */
	private SessionObject(SessionObject so)
	{
	}

	/////////////////////////////////////////////////
	// back-end business methods relative to files //
	/////////////////////////////////////////////////

	private BasicFileAttributes getAttributes(String path)
	throws IOException
	{
		return Files.readAttributes(new File(path).toPath(), BasicFileAttributes.class, LinkOption.NOFOLLOW_LINKS);
	}

	private boolean isLink(String path)
	throws IOException
	{
		try
		{
			return getAttributes(path).isSymbolicLink();
		}
		catch (NoSuchFileException ex)
		{
			return false;
		}
	}

	private boolean isFile(String path)
	throws IOException
	{
		try
		{
			return getAttributes(path).isRegularFile();
		}
		catch (NoSuchFileException ex)
		{
			return false;
		}
	}

	private boolean isDir(String path)
	throws IOException
	{
		try
		{
			return getAttributes(path).isDirectory();
		}
		catch (NoSuchFileException ex)
		{
			return false;
		}
	}

	private boolean isWriteable(String path)
	{
		return Files.isWritable(new File(path).toPath());
	}

	private boolean isReadable(String path)
	{
		return Files.isReadable(new File(path).toPath());
	}

	private boolean exists(String path)
	{
		return Files.exists(new File(path).toPath());
	}

	private void delete(String path)
	throws IOException
	{
		Files.delete(new File(path).toPath());
	}

	private long getMTime(String path)
	throws IOException
	{
		return getAttributes(path).lastModifiedTime().toMillis();			
	}

	private long getCTime(String path)
	throws IOException
	{
		return getAttributes(path).creationTime().toMillis();
	}

	private long getSize(String path)
	throws IOException
	{
		return getAttributes(path).size();
	}


	////////////////////////////////////////
	// SessionObjectAction implementation //
	////////////////////////////////////////

	/**
	 * @see org.ow2.clif.scenario.isac.plugin.SessionObjectAction#createNewSessionObject()
	 */
	public Object createNewSessionObject()
	{
		return new SessionObject(this);
	}

	/**
	 * @see org.ow2.clif.scenario.isac.plugin.SessionObjectAction#close()
	 */
	public void close()
	{
	}

	/**
	 * @see org.ow2.clif.scenario.isac.plugin.SessionObjectAction#reset()
	 */
	public void reset()
	{
	}


	///////////////////////////////
	// TestAction implementation //
	///////////////////////////////

	/**
	 * @see org.ow2.clif.scenario.isac.plugin.TestAction#doTest()
	 */
	public boolean doTest(int number, Map<String, String> params)
	{
		String path = null;
		try
		{
			switch (number)
			{
			case TEST_ISNOTLINK:
				path = params.get(TEST_ISNOTLINK_PATH);
				return !isLink(path);
			case TEST_ISLINK:
				path = params.get(TEST_ISLINK_PATH);
				return isLink(path);
			case TEST_ISFILE:
				path = params.get(TEST_ISFILE_PATH);
				return isFile(path);
			case TEST_ISDIR:
				path = params.get(TEST_ISDIR_PATH);
				return isDir(path);
			case TEST_NOTWRITEABLE:
				path = params.get(TEST_NOTWRITEABLE_PATH);
				return !isWriteable(path);
			case TEST_NOTREADABLE:
				path = params.get(TEST_NOTREADABLE_PATH);
				return !isReadable(path);
			case TEST_WRITEABLE:
				path = params.get(TEST_WRITEABLE_PATH);
				return isWriteable(path);
			case TEST_READABLE:
				path = params.get(TEST_NOTWRITEABLE_PATH);
				return isReadable(path);
			case TEST_EXISTSNOT:
				path = params.get(TEST_EXISTSNOT_PATH);
				return !exists(path);
			case TEST_EXISTS:
				path = params.get(TEST_EXISTSNOT_PATH);
				return exists(path);
			default:
				throw new Error("Undefined test identifier " + number + " in ~FileHandler~ ISAC plugin.");
			}
		}
		catch (IOException ex)
		{
			throw new IsacRuntimeException("Exception while examining path " + path, ex);
		}
	}

	
	//////////////////////////////////
	// ControlAction implementation //
	//////////////////////////////////

	/**
	 * @see org.ow2.clif.scenario.isac.plugin.ControlAction#doControl()
	 */
	public void doControl(int number, Map<String, String> params)
	{
		String path = null;
		try
		{
			switch (number)
			{
			case CONTROL_DELETEEMPTYDIR:
				path = params.get(CONTROL_DELETEEMPTYDIR_PATH);
				delete(path);
				break;
			case CONTROL_DELETEFILES:
				for (String onePath : ParameterParser.getNField(params.get(CONTROL_DELETEFILES_PATHS)))
				{
					path = onePath;
					delete(onePath);
				}
				break;
			default:
				throw new Error("Undefined control identifier " + number + " in ~FileHandler~ ISAC plugin.");
			}
		}
		catch (IOException ex)
		{
			throw new IsacRuntimeException("Exception while trying to delete " + path, ex);
		}
	}

	
	/////////////////////////////////
	// DataProvider implementation //
	/////////////////////////////////

	/**
	 * @see org.ow2.clif.scenario.isac.plugin.DataProvider#doGet()
	 */
	public String doGet(String var)
	{
		String[] split = var.split(";");
		String path = split[0];		
		var = split[1];
		try
		{
			if (var.equalsIgnoreCase(VAR_CTIME))
			{
				return String.valueOf(getCTime(path));
			}
			else if (var.equalsIgnoreCase(VAR_MTIME))
			{
				return String.valueOf(getMTime(path));
			}
			else if (var.equalsIgnoreCase(VAR_SIZE))
			{
				return String.valueOf(getSize(path));
			}
			else
			{
				throw new IsacRuntimeException("Unknown variable in ~FileHandler~ ISAC plugin: " + var);
			}
		}
		catch (IOException ex)
		{
			throw new IsacRuntimeException(
				"Exception while getting attribute " + var + " for " + path,
				ex);
		}
	}
}
