/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2005, 2006, 2008, 2013 France Telecom
 * Copyright (C) 2017 Orange SA
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * Contact: clif@ow2.org
 */
package org.ow2.isac.plugin.common;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.TimeZone;
import org.ow2.clif.scenario.isac.exception.IsacRuntimeException;
import org.ow2.clif.scenario.isac.plugin.ControlAction;
import org.ow2.clif.scenario.isac.plugin.DataProvider;
import org.ow2.clif.scenario.isac.plugin.TestAction;
import org.ow2.clif.scenario.isac.plugin.SessionObjectAction;
import org.ow2.clif.storage.api.ActionEvent;
import org.ow2.clif.scenario.isac.plugin.SampleAction;
import org.ow2.clif.scenario.isac.util.ParameterParser;

/**
 * This plug-in provides <code>true</code> and <code>false</code> constant conditions,
 * as well as printing primitives for stdout and stderr. It also provides a timestamp
 * facility through a variable, as well as a log primitive which creates a pseudo request
 * report. At last, it supports setting Java system properties.
 * 
 * @author Emmanuel Varoquaux
 * @author Bruno Dillenseger
 */
public class SessionObject implements
	SessionObjectAction,
	TestAction,
	DataProvider,
	ControlAction,
	SampleAction
{
	static final int SAMPLE_LOG = 3;
	static final String SAMPLE_LOG_ITERATION = "iteration";
	static final String SAMPLE_LOG_SUCCESSFUL = "successful";
	static final String SAMPLE_LOG_RESULT = "result";
	static final String SAMPLE_LOG_COMMENT = "comment";
	static final String SAMPLE_LOG_DURATION = "duration";
	static final int CONTROL_PRINTOUT = 0;
	static final String CONTROL_PRINTOUT_STRING = "string";
	static final int CONTROL_PRINTERR = 1;
	static final String CONTROL_PRINTERR_STRING = "string";
	static final int CONTROL_ALARM = 2;
	static final String CONTROL_ALARM_MESSAGE = "message";
	static final int CONTROL_SETPROPERTY = 4;
	static final String CONTROL_SETPROPERTY_VALUE = "value";
	static final String CONTROL_SETPROPERTY_NAME = "name";
	static final int CONTROL_SETTIMEZONE = 5;
	static final String CONTROL_SETTIMEZONE_TIMEZONE = "timezone";
	static final int TEST_TRUE = 0;
	static final int TEST_FALSE = 1;

	static final String DATE_MS_GET = "date_ms";
	static final String DATE_PREFIX = "date!";

	TimeZone tz;

	//////////////////
	// constructors //
	//////////////////

	public SessionObject()
	{
		tz = TimeZone.getDefault();
	}

	public SessionObject(Map<String,String> params)
	{
		this();
	}

	///////////////////////////////////
	// SessionObjectAction interface //
	///////////////////////////////////

	public Object createNewSessionObject()
	{
		return new SessionObject();
	}

	public void reset() {}

	public void close() {}

	//////////////////////////
	// TestAction interface //
	//////////////////////////

	public boolean doTest(int number, Map<String,String> params)
	{
		switch (number)
		{
			case TEST_FALSE:
				return false;
			case TEST_TRUE:
				return true;
			default:
				throw new Error("Fatal error in ISAC's Common plug-in: unknown test identifier " + number);
		}
	}

	/////////////////////////////
	// ControlAction interface //
	/////////////////////////////

	public void doControl(int number, Map<String,String> params)
	{
		switch (number)
		{
			case CONTROL_SETTIMEZONE:
				String tzStr = params.get(CONTROL_SETTIMEZONE_TIMEZONE);
				if (tzStr == null || tzStr.isEmpty())
				{
					tz = TimeZone.getDefault();
				}
				else
				{
					tz = TimeZone.getTimeZone(tzStr);
				}
				break;
			case CONTROL_SETPROPERTY:
				System.setProperty(
					params.get(CONTROL_SETPROPERTY_NAME),
					params.get(CONTROL_SETPROPERTY_VALUE));
				break;
			case CONTROL_PRINTOUT:
				System.out.println(params.get(CONTROL_PRINTOUT_STRING));
				break;
			case CONTROL_PRINTERR:
				System.err.println(params.get(CONTROL_PRINTERR_STRING));
				break;
			case CONTROL_ALARM:
				throw new IsacRuntimeException(params.get(CONTROL_ALARM_MESSAGE));
			default:
				throw new Error("Fatal error in ISAC's Common plug-in: unknown control identifier " + number);
		}
	}


	////////////////////////////
	// DataProvider interface //
	////////////////////////////


	public String doGet(String var)
	{
		if (var.equals(DATE_MS_GET))
		{
			return String.valueOf(System.currentTimeMillis());
		}
		else if (var.startsWith(DATE_PREFIX))
		{
			SimpleDateFormat format = new SimpleDateFormat(var.substring(DATE_PREFIX.length()));
			format.setTimeZone(tz);
			return format.format(new Date(System.currentTimeMillis()));
		}
		else
		{
			throw new IsacRuntimeException("Unknown variable name in Common plug-in: " + var);
		}
	}

	
	/////////////////////////////////
	// SampleAction implementation //
	/////////////////////////////////


	/**
	 * @see org.ow2.clif.scenario.isac.plugin.SampleAction#doSample()
	 */
	public ActionEvent doSample(int number, Map<String,String> params, ActionEvent report) {
		switch (number) {
			case SAMPLE_LOG:
				report.setDate(System.currentTimeMillis());
				report.duration = Integer.parseInt(params.get(SAMPLE_LOG_DURATION));
				report.comment = params.get(SAMPLE_LOG_COMMENT);
				report.result = params.get(SAMPLE_LOG_RESULT);
				report.type = "LOG";
				report.iteration = Integer.parseInt(params.get(SAMPLE_LOG_ITERATION));
				report.successful =
					ParameterParser.getRadioGroup(params.get(SAMPLE_LOG_SUCCESSFUL)).equals("yes");
				return report;
			default:
				throw new Error(
					"Unable to find this sample in ~Common~ ISAC plugin: "
							+ number);
		}
	}
}
