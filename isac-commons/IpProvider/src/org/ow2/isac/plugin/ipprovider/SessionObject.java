package org.ow2.isac.plugin.ipprovider;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.lang.Error;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.util.Map;
import org.ow2.clif.scenario.isac.exception.IsacRuntimeException;
import org.ow2.clif.scenario.isac.plugin.DataProvider;
import org.ow2.clif.scenario.isac.plugin.ControlAction;
import org.ow2.clif.scenario.isac.plugin.SessionObjectAction;
import org.ow2.clif.scenario.isac.util.ParameterParser;
import org.ow2.clif.util.Network;

/**
 * Implementation of a session object for plugin ~IPprovider~
 */
public class SessionObject
	implements SessionObjectAction, DataProvider, ControlAction
{
	static final String PLUGIN_UNIQUE = "unique";
	static final String PLUGIN_FILTERS = "filters";
	static final String PLUGIN_POLICY = "policy";
	static final int CONTROL_RELEASE = 1;
	static final int CONTROL_NEXT = 2;
	static final int POLICY_RANDOM = 0;
	static final String POLICY_RANDOM_STR = "random";
	static final int POLICY_ROUNDROBIN = 1;
	static final String UNIQUE_YES = "yes"; 

	static Random rand = new Random();

	private List<String> addresses;
	private String[] filters = null;
	private int allocationPolicy = POLICY_ROUNDROBIN;
	private boolean unique = false;
	private int rr_index = 0;
	private String myIP = null;

	/**
	 * Constructor for specimen object.
	 *
	 * @param params key-value pairs for plugin parameters
	 */
	public SessionObject(Map<String,String> params)
	{
		String policyStr = ParameterParser.getRadioGroup((String)params.get(PLUGIN_POLICY));
		if (policyStr.equals(POLICY_RANDOM_STR))
		{
			allocationPolicy = POLICY_RANDOM;
		}
		else
		{
			allocationPolicy = POLICY_ROUNDROBIN;
		}
		List<String>filterList = ParameterParser.getNField((String)params.get(PLUGIN_FILTERS));
		if (filterList.isEmpty())
		{
			filters = null;
		}
		else
		{
			filters = filterList.toArray(new String[filterList.size()]);
		}
		unique = ParameterParser.getRadioGroup((String)params.get(PLUGIN_UNIQUE)).equals(UNIQUE_YES);
		InetAddress[] inetAddresses = Network.getInetAddresses();
		addresses = new ArrayList<String>(inetAddresses.length);
		for (int i=0 ; i<inetAddresses.length ; ++i)
		{
			if (filters == null)
			{
				addresses.add(inetAddresses[i].getHostAddress());
			}
			else
			{
				for (int j=0 ; j<filters.length ; ++j)
				{
					if (Network.belongsToSubnet((Inet4Address)inetAddresses[i],	filters[j]))
					{
						addresses.add(inetAddresses[i].getHostAddress());
					}
				}
			}
		}
	}


	/**
	 * Copy constructor (clone specimen object to get session object).
	 *
	 * @param so specimen object to clone
	 */
	private SessionObject(SessionObject so)
	{
		filters = so.filters;
		allocationPolicy = so.allocationPolicy;
		unique = so.unique;
		addresses = so.addresses;
	}


	////////////////////////////////////////
	// SessionObjectAction implementation //
	////////////////////////////////////////


	/**
	 * @see org.ow2.clif.scenario.isac.plugin.SessionObjectAction#createNewSessionObject()
	 */
	public Object createNewSessionObject() {
		return new SessionObject(this);
	}

	/**
	 * @see org.ow2.clif.scenario.isac.plugin.SessionObjectAction#close()
	 */
	public void close()
	{
		releaseIP();
	}


	///////////////////////////////////
	// miscellaneous utility methods //
	///////////////////////////////////


	private void releaseIP()
	{
		if (unique && myIP != null)
		{
			synchronized(addresses)
			{
				addresses.add(myIP);
			}
		}
		myIP = null;
	}


	private void getIP()
	{
		releaseIP();
		if (addresses.isEmpty())
		{
			throw new IsacRuntimeException("No more available IP address!");
		}
		if (allocationPolicy == POLICY_RANDOM)
		{
			myIP = addresses.get(rand.nextInt(addresses.size()));
		}
		else if (allocationPolicy == POLICY_ROUNDROBIN)
		{
			myIP = addresses.get(rr_index++);
			if (rr_index == addresses.size())
			{
				rr_index = 0;
			}
		}
	}


	private void acquireIP()
	{
		if (unique)
		{
			synchronized(addresses)
			{
				rr_index = 0; // round robin + unique => get the first IP address from the list 
				getIP();
				addresses.remove(myIP);
			}
		}
		else
		{
			getIP();
		}
	}


	/**
	 * @see org.ow2.clif.scenario.isac.plugin.SessionObjectAction#reset()
	 */
	public void reset()
	{
		releaseIP();
	}

	
	/////////////////////////////////
	// DataProvider implementation //
	/////////////////////////////////

	/**
	 * @see org.ow2.clif.scenario.isac.plugin.DataProvider#doGet()
	 */
	public String doGet(String var)
	{
		return myIP;
	}


	//////////////////////////////////
	// ControlAction implementation //
	//////////////////////////////////

	/**
	 * @see org.ow2.clif.scenario.isac.plugin.ControlAction#doControl()
	 */
	public void doControl(int number, Map<String,String> params) {
		switch (number) {
			case CONTROL_NEXT:
				acquireIP();
				break;
			case CONTROL_RELEASE:
				releaseIP();
				break;
			default:
				throw new Error("Unable to find this control in ~IPprovider~ ISAC plugin: " + number);
		}
	}
}
