/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2004,2013 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * Contact: clif@ow2.org
 */
package org.ow2.isac.plugin.constanttimer;


import java.util.Map;
import org.ow2.clif.scenario.isac.exception.IsacRuntimeException;
import org.ow2.clif.scenario.isac.plugin.SampleAction;
import org.ow2.clif.scenario.isac.plugin.TestAction;
import org.ow2.clif.scenario.isac.plugin.TimerAction;
import org.ow2.clif.scenario.isac.plugin.SessionObjectAction;
import org.ow2.clif.storage.api.ActionEvent;


/**
 * This class is the implementation of the ConstantTimer session object.
 * Includes time-out facilities.
 *
 * @author Bruno Dillenseger
 */
public class SessionObject implements SampleAction, TestAction, TimerAction, SessionObjectAction
{
	// plug-in parameter: duration parameter
	static public final String DURATION_ARG = "duration_arg";

	// timer parameters
	static public final String PERIOD_ARG = "period_arg";

	// tests identifiers
	static public final int RUNNING_TEST = 0;
	static public final int PASSED_TEST = 1;

	// pseudo-samples identifiers
	static public final int RESET_SAMPLE = 0;
	static public final int SET_SAMPLE = 1;

	// timer identifiers
	static public final int SLEEP_TIMER = 0;
	static public final int PERIOD_BEGIN = 1;
	static public final int PERIOD_END = 2;

	// timer default duration
	protected int defaultDuration;
	// current timer duration in ms
	protected int duration;
	// time origin for current time out
	protected long startTime;
	// time origin for period
	protected long startPeriod = -1;


	/**
	 * Build a new SessionObject for this plugin
	 * @param params The table containing all the parameters
	 */
	public SessionObject(Map<String,String> params)
	{
		startTime = -1;
		String value = (String)params.get(DURATION_ARG);
		if (value != null && value.length() > 0)
		{
			duration = defaultDuration = Integer.parseInt(value);
		}
		else
		{
			duration = defaultDuration = 0;
		}
	}


	/**
	 * This constructor is used to clone the specified session object
	 *
	 * @param toClone
	 *            The session object to clone
	 */
	private SessionObject(SessionObject toClone)
	{
		startTime = -1;
		startPeriod = -1;
		defaultDuration = toClone.defaultDuration;
		duration = toClone.defaultDuration;
	}


	////////////////////////////
	// SampleAction interface //
	////////////////////////////


	public ActionEvent doSample(int number, Map<String,String> params, ActionEvent dummy)
	{
		switch (number)
		{
			case RESET_SAMPLE:
				startTime = System.currentTimeMillis();
				break;
			case SET_SAMPLE:
				startTime = System.currentTimeMillis();
				duration = Integer.parseInt((String)params.get(DURATION_ARG));
				break;
		}
		return null;
	}


	///////////////////////////
	// TestAction interface //
	///////////////////////////


	public boolean doTest(int number, Map<String,String> params)
		throws IsacRuntimeException
	{
		switch (number)
		{
			case RUNNING_TEST:
				return duration > (int)(System.currentTimeMillis() - startTime);
			case PASSED_TEST:
				return duration <= (int)(System.currentTimeMillis() - startTime);
		}
		throw new Error("Fatal error in ISAC's ConstantTimer plug-in: unknown test identifier " + number);
	}


	///////////////////////////
	// TimerAction interface //
	///////////////////////////


	public long doTimer(int number, Map<String,String> params)
		throws IsacRuntimeException
	{
		switch (number)
		{
			case SLEEP_TIMER:
				String durationStr = (String)params.get(DURATION_ARG);
				if (durationStr != null && durationStr.length() > 0)
				{
					try
					{
						return Integer.parseInt(durationStr);
					}
					catch (Exception ex)
					{
						throw new IsacRuntimeException("Invalid timer duration " + durationStr, ex);
					}
				}
				else
				{
					return duration;
				}
			case PERIOD_BEGIN:
				startPeriod = System.currentTimeMillis();
				return 0;
			case PERIOD_END:
				String periodStr = (String)params.get(PERIOD_ARG);
				int sleepTime;
				if (periodStr != null && periodStr.length() > 0)
				{
					try
					{
						sleepTime = (int)
							(Integer.parseInt(periodStr)
							- System.currentTimeMillis()
							+ startPeriod);
					}
					catch (Exception ex)
					{
						throw new IsacRuntimeException("Invalid period " + periodStr, ex);
					}
				}
				else
				{
					sleepTime = (int)
						(duration
						- System.currentTimeMillis()
						+ startPeriod);
				}
				if (startPeriod < 0 || sleepTime <= 0)
				{
					return 0;
				}
				else
				{
					return sleepTime;
				}
			default:
				throw new Error("Unknown timer identifier in " + getClass());
		}
	}


	///////////////////////////////////
	// SessionObjectAction interface //
	///////////////////////////////////


	public Object createNewSessionObject()
	{
		return new SessionObject(this);
	}


	public void close()
	{
	}


	public void reset()
	{
		startTime = System.currentTimeMillis();
		duration = defaultDuration;
	}
}
