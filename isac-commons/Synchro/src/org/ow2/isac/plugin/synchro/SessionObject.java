/*
* CLIF is a Load Injection Framework
* Copyright (C) 2011 France Telecom R&D
* Copyright (C) 2016 Orange SA
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
* Contact: clif@ow2.org
*/

package org.ow2.isac.plugin.synchro;

import java.util.Map;
import org.ow2.clif.deploy.ClifRegistry;
import org.ow2.clif.scenario.isac.plugin.SessionObjectAction;
import org.ow2.clif.scenario.isac.plugin.ControlAction;
import org.ow2.clif.scenario.isac.exception.IsacRuntimeException;
import org.ow2.clif.scenario.isac.plugin.TestAction;
import org.ow2.clif.scenario.isac.util.ParameterParser;
import org.ow2.clif.scenario.isac.plugin.DataProvider;
import org.ow2.clif.server.api.Synchronizer;
import org.ow2.clif.server.lib.SynchronizerImpl;

/**
 * Implementation of a session object for plugin ~Synchro~
 * When distributed synchronization is enabled, a domain name
 * must be specified. This domain name, prefixed with
 * Synchronizer.SYNCHRONIZER is looked up in the CLIF registry
 * to find a Synchronizer implementation to which all calls are
 * delegated.
 * 
 * @author Bruno Dillenseger
 */
public class SessionObject
	implements SessionObjectAction, ControlAction, TestAction, DataProvider
{
	static final String CONTROL_SETRENDEZVOUS_TIMES = "times";
	static final String CONTROL_RENDEZVOUS_TIMEOUT = "timeout";
	static final String CONTROL_RENDEZVOUS_LOCK = "lock";
	static final String CONTROL_SETRENDEZVOUS_LOCK = "lock";
	static final String PLUGIN_DOMAIN = "domain";
	static final String PLUGIN_DISTRIBUTED = "distributed";
	static final int TEST_WASNOTIFIED = 2;
	static final String TEST_WASNOTIFIED_LOCK = "lock";
	static final String TEST_WASNOTIFIED_OPTIONS = "options";
	static final int TEST_WASNOTIFIEDN = 3;
	static final String TEST_WASNOTIFIEDN_LOCK = "lock";
	static final String TEST_WASNOTIFIEDN_OPTIONS = "options";
	static final String TEST_WASNOTIFIEDN_TIMES = "times";
	static final int CONTROL_NOTIFY = 0;
	static final String CONTROL_NOTIFY_LOCK = "lock";
	static final int CONTROL_WAIT = 1;
	static final String CONTROL_WAIT_TIMEOUT = "timeout";
	static final String CONTROL_WAIT_LOCK = "lock";
	static final int CONTROL_WAITN = 4;
	static final String CONTROL_WAITN_TIMEOUT = "timeout";
	static final String CONTROL_WAITN_LOCK = "lock";
	static final String CONTROL_WAITN_TIMES = "times";
	static final int CONTROL_SETRENDEZVOUS = 5;
	static final int CONTROL_RENDEZVOUS = 6;
	protected volatile Synchronizer locks;


	/**
	 * Constructor for specimen object.
	 *
	 * @param params key-value pairs for plugin parameters
	 */
	public SessionObject(Map<String,String> params)
	{
		if (ParameterParser.getRadioGroup(params.get(PLUGIN_DISTRIBUTED)).equals("yes"))
		{
			// delegate calls to the remote synchronizer implementation
			String domain = params.get(PLUGIN_DOMAIN);
			try
			{
				locks = (Synchronizer)
					new ClifRegistry(false)
					.lookup(Synchronizer.SYNCHRONIZER + "/" + domain)
					.getFcInterface(Synchronizer.SYNCHRONIZER);
			}
			catch (Exception e)
			{
				throw new IsacRuntimeException("Can't connect to Synchronizer for domain " + domain, e);
			}
		}
		else
		{
			// local synchronizer implementation
			locks = new SynchronizerImpl();
		}
	}


	////////////////////////////////////////
	// SessionObjectAction implementation //
	////////////////////////////////////////


	/**
	 * @see org.ow2.clif.scenario.isac.plugin.SessionObjectAction#createNewSessionObject()
	 */
	public Object createNewSessionObject()
	{
		// No need for a dedicated session object per virtual user: the specimen object is sufficient
		// (by nature, locks are shared between vUsers)
		return this;
	}


	/**
	 * @see org.ow2.clif.scenario.isac.plugin.SessionObjectAction#close()
	 */
	public void close()
	{
	}


	/**
	 * @see org.ow2.clif.scenario.isac.plugin.SessionObjectAction#reset()
	 */
	public void reset()
	{
	}


	//////////////////////////////////
	// ControlAction implementation //
	//////////////////////////////////


	/**
	 * @see org.ow2.clif.scenario.isac.plugin.ControlAction#doControl()
	 */
	public void doControl(int number, Map<String,String> params)
	{
		String lockName;
		switch (number)
		{
			case CONTROL_RENDEZVOUS:
				lockName = params.get(CONTROL_RENDEZVOUS_LOCK);
				try
				{
					locks.notify(lockName);
					locks.getRendezVous(
						lockName,
						Long.parseLong((String)params.get(CONTROL_RENDEZVOUS_TIMEOUT)));
				}
				catch (InterruptedException ex)
				{
					throw new IsacRuntimeException("Interrupted rendez-vous on lock " + lockName);
				}
				break;
			case CONTROL_SETRENDEZVOUS:
				lockName = params.get(CONTROL_SETRENDEZVOUS_LOCK);
				locks.setRendezVous(
					lockName,
					Long.parseLong((String)params.get(CONTROL_SETRENDEZVOUS_TIMES)));
				break;
			case CONTROL_WAITN:
				lockName = params.get(CONTROL_WAITN_LOCK);
				try
				{
					locks.wait(
						lockName,
						Long.parseLong((String)params.get(CONTROL_WAIT_TIMEOUT)),
						Long.parseLong(params.get(CONTROL_WAITN_TIMES)));
				}
				catch (InterruptedException ex)
				{
					throw new IsacRuntimeException("Interrupted wait on lock " + lockName);
				}
				break;
			case CONTROL_WAIT:
				lockName = params.get(CONTROL_WAIT_LOCK);
				try
				{
					locks.wait(lockName, Long.parseLong((String)params.get(CONTROL_WAIT_TIMEOUT)));
				}
				catch (InterruptedException ex)
				{
					throw new IsacRuntimeException("Interrupted wait on lock " + lockName);
				}
				break;
			case CONTROL_NOTIFY:
				lockName = params.get(CONTROL_NOTIFY_LOCK);
				locks.notify(lockName);
				break;
			default:
				throw new Error(
					"Unable to find this control in ~Synchro~ ISAC plugin: "
					+ number);
		}
	}

	
	///////////////////////////////
	// TestAction implementation //
	///////////////////////////////


	/**
	 * @see org.ow2.clif.scenario.isac.plugin.TestAction#doTest()
	 */
	public boolean doTest(int number, Map<String,String> params)
	{
		String lockName;
		boolean response;
		switch (number)
		{
			case TEST_WASNOTIFIEDN:
				lockName = params.get(TEST_WASNOTIFIEDN_LOCK);
				response = locks.wasNotified(
					lockName,
					Long.parseLong((String)params.get(TEST_WASNOTIFIEDN_TIMES)));
				if (ParameterParser.getCheckBox(params.get(TEST_WASNOTIFIEDN_OPTIONS)).contains("not"))
				{
					response = ! response;
				}
				break;
			case TEST_WASNOTIFIED:
				lockName = params.get(TEST_WASNOTIFIED_LOCK);
				response = locks.wasNotified(lockName);
				if (ParameterParser.getCheckBox(params.get(TEST_WASNOTIFIED_OPTIONS)).contains("not"))
				{
					response = ! response;
				}
				break;
			default:
				throw new Error(
					"Unable to find this test in ~Synchro~ ISAC plugin: " + number);
		}
		return response;
	}


	/////////////////////////////////
	// DataProvider implementation //
	/////////////////////////////////


	/**
	 * @see org.ow2.clif.scenario.isac.plugin.DataProvider#doGet()
	 */
	public synchronized String doGet(String var)
	{
		if (var.equals(""))
		{
			return locks.toString();
		}
		else
		{
			return String.valueOf(locks.getCount(var));
		}
	}
}
