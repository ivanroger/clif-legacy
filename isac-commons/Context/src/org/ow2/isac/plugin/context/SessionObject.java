/*
* CLIF is a Load Injection Framework
* Copyright (C) 2005, 2009 France Telecom R&D
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
* Contact: clif@ow2.org
*/

package org.ow2.isac.plugin.context;

import org.ow2.clif.scenario.isac.plugin.ControlAction;
import org.ow2.clif.scenario.isac.plugin.DataProvider;
import org.ow2.clif.scenario.isac.plugin.TestAction;
import org.ow2.clif.scenario.isac.plugin.SessionObjectAction;
import org.ow2.clif.scenario.isac.util.ParameterParser;
import org.ow2.clif.scenario.isac.exception.IsacRuntimeException;
import org.ow2.clif.util.ClifClassLoader;
import java.io.IOException;
import java.io.InputStream;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.lang.Error;


/**
 * ISAC plug-in providing a context holder to define and use arbitrary variables/constants
 * @author Bruno Dillenseger
 */
public class SessionObject
	implements DataProvider, ControlAction, TestAction, SessionObjectAction
{
	static final String PLUGIN_FILES = "files";
	static final String PLUGIN_VARIABLES = "variables";
	static final int TEST_ISDEF = 0;
	static final String TEST_ISDEF_VARIABLE = "variable";
	static final int TEST_ISNOTDEF = 1;
	static final String TEST_ISNOTDEF_VARIABLE = "variable";
	static final int CONTROL_SET = 0;
	static final String CONTROL_SET_VALUE = "value";
	static final String CONTROL_SET_VARIABLE = "variable";
	static final int CONTROL_LOAD = 2;
	static final String CONTROL_LOAD_FILE = "file";
	static final int CONTROL_CLEAR = 3;

	static private final char SET_CHAR = '=';

	static private void parseVariables(String varstr, Properties result)
	{
		Iterator<String> variableIter = ParameterParser.getNField(varstr).iterator();
		while (variableIter.hasNext())
		{
			String variable = variableIter.next();
			int splitIndex = variable.indexOf(SET_CHAR);
			String key = variable.substring(0, splitIndex);
			String value;
			if (splitIndex > 0)
			{
				value = variable.substring(splitIndex + 1);
			}
			else
			{
				value = "";
			}
			result.put(key, value);
		}
	}

	private Properties variables;
	private Properties defaultVariables;

	/**
	 * Build a new SessionObject for this plugin
	 * @param params The table containing all the parameters
	 */
	public SessionObject(Map<String,String> params)
	{
		variables = new Properties();
		parseVariables(params.get(PLUGIN_VARIABLES), variables);
		List<String> fileNames = ParameterParser.getNField(params.get(PLUGIN_FILES));
		for (String file : fileNames)
		{
			if (file.length() > 0)
			{
				try
				{
					InputStream input = ClifClassLoader.getClassLoader().getResourceAsStream(file);
					if (input == null)
					{
						throw new IsacRuntimeException("Plug-in Context could not find property file " + file);
					}
					variables.load(input);
					input.close();
				}
				catch (IOException ex)
				{
					throw new IsacRuntimeException("Plug-in Context could not load property file " + file, ex);
				}
			}
		}
	}


	/**
	 * This constructor is used to clone the specified session object
	 *
	 * @param toClone
	 *            The session object to clone
	 */
	private SessionObject(SessionObject toClone)
	{
		defaultVariables = toClone.variables;
		variables = (Properties)toClone.variables.clone();
	}


	///////////////////////////
	// TestAction interface //
	///////////////////////////


	public boolean doTest(int number, Map<String,String> params)
		throws IsacRuntimeException
	{
		switch (number)
		{
			case TEST_ISDEF:
				return variables.containsKey(params.get(TEST_ISDEF_VARIABLE));
			case TEST_ISNOTDEF:
				return ! variables.containsKey(params.get(TEST_ISNOTDEF_VARIABLE));
			default:
				throw new Error("Fatal error in ISAC's ~Context~ plug-in: unknown test identifier " + number);
		}
	}


	////////////////////////////
	// DataProvider interface //
	////////////////////////////

	
	public String doGet(String var)
	{
		return variables.getProperty(var);
	}


	////////////////////////////
	// ControlAction interface //
	////////////////////////////


	public void doControl(int number, Map<String,String> params)
	{
		switch (number)
		{
			case CONTROL_CLEAR:
				variables.clear();
				break;
			case CONTROL_LOAD:
				String fileName = params.get(CONTROL_LOAD_FILE);
				try
				{
					InputStream input = ClifClassLoader.getClassLoader().getResourceAsStream(fileName);
					variables.load(input);
					input.close();
				}
				catch (IOException ex)
				{
					throw new IsacRuntimeException("Context could not load property file " + fileName, ex);
				}
				break;
			case CONTROL_SET:
				variables.put(params.get(CONTROL_SET_VARIABLE), params.get(CONTROL_SET_VALUE));
				break;
			default:
				throw new Error("Fatal error in ISAC's ~Context~ plug-in: unknown control identifier " + number);
		}
	}


	///////////////////////////////////
	// SessionObjectAction interface //
	///////////////////////////////////


	public Object createNewSessionObject()
	{
		return new SessionObject(this);
	}


	public void close()
	{
	}


	public void reset()
	{
		variables = (Properties)defaultVariables.clone();
	}
}
