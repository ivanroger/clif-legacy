/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2007-2013 France Telecom
 * Copyright (C) 2016 Orange SA
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * Contact: clif@ow2.org
 */

package org.ow2.isac.plugin.ldapinjector;

import org.ow2.clif.scenario.isac.plugin.SessionObjectAction;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;
import java.util.HashMap;
import java.util.List;
import org.ow2.clif.scenario.isac.plugin.DataProvider;
import org.ow2.clif.scenario.isac.exception.IsacRuntimeException;
import java.lang.Error;
import java.util.Map;
import org.ow2.clif.storage.api.ActionEvent;
import org.ow2.clif.scenario.isac.plugin.SampleAction;
import org.ow2.clif.scenario.isac.util.ParameterParser;
import com.novell.ldap.LDAPAttribute;
import com.novell.ldap.LDAPAttributeSet;
import com.novell.ldap.LDAPConnection;
import com.novell.ldap.LDAPEntry;
import com.novell.ldap.LDAPException;
import com.novell.ldap.LDAPJSSESecureSocketFactory;
import com.novell.ldap.LDAPMessage;
import com.novell.ldap.LDAPModification;
import com.novell.ldap.LDAPSearchConstraints;
import com.novell.ldap.LDAPSearchQueue;
import com.novell.ldap.LDAPSearchResult;
import com.novell.ldap.LDAPSocketFactory;
import org.ow2.clif.scenario.isac.plugin.TestAction;

/**
 * Implementation of a session object for plugin ~LdapInjector~
 * 
 * @author Thomas Escalle
 * @author Bruno Dillenseger
 */
public class SessionObject implements SessionObjectAction, DataProvider, SampleAction, TestAction
{
	static final String PLUGIN_LDAPSSLPORT = "ldapSSLPort";
	static final String PLUGIN_LDAPPORT = "ldapPort";
	static final String PLUGIN_LDAPHOST = "ldapHost";
	static final String PLUGIN_LDAPVERSION = "ldapVersion";

	static final int SAMPLE_BIND = 0;
	static final String SAMPLE_BIND_COMMENT = "comment";
	static final String SAMPLE_BIND_ACTIONTYPE = "actiontype";
	static final String SAMPLE_BIND_WITHSSL = "withSSL";
	static final String SAMPLE_BIND_PASSWORD = "password";
	static final String SAMPLE_BIND_LOGIN = "login";

	static final int SAMPLE_SEARCH = 2;
	static final String SAMPLE_SEARCH_SEARCHLIMIT = "searchLimit";
	static final String SAMPLE_SEARCH_BUFFER = "buffer";
	static final String SAMPLE_SEARCH_COMMENT = "comment";
	static final String SAMPLE_SEARCH_ACTIONTYPE = "actiontype";
	static final String SAMPLE_SEARCH_SEARCHSCOPE = "searchScope";
	static final String SAMPLE_SEARCH_SEARCHFILTER = "searchFilter";
	static final String SAMPLE_SEARCH_SEARCHBASE = "searchBase";
	
	static final int SAMPLE_CLOSECONNECTION = 3;

	static final int SAMPLE_ADDENTRY = 4;
	static final String SAMPLE_ADDENTRY_ENTRYNODETYPE = "entryNodeType";
	static final String SAMPLE_ADDENTRY_ENTRYNODENAME = "entryNodeName";
	static final String SAMPLE_ADDENTRY_ATTRIBUTESLIST = "AttributesList";
	static final String SAMPLE_ADDENTRY_LDAPDN = "ldapDN";

	static final int SAMPLE_DELETEENTRY = 5;
	static final String SAMPLE_DELETEENTRY_LDAPDN = "ldapDN";

	static final int SAMPLE_ADDATTRIBUTE = 6;
	static final String SAMPLE_ADDATTRIBUTE_ATTRIBUTESNAMEVALUES = "attributesNameValues";
	static final String SAMPLE_ADDATTRIBUTE_DN = "dn";

	static final int SAMPLE_DELETEATTRIBUTE = 7;
	static final String SAMPLE_DELETEATTRIBUTE_ATTRIBUTESTODELETE = "attributesToDelete";
	static final int TEST_ISCONNECTED = 8;
	static final int TEST_ISNOTCONNECTED = 9;
	static final int TEST_HASMOREENTRIES = 10;
	static final String TEST_HASMOREENTRIES_BUFFER = "buffer";
	static final int TEST_HASNOMOREENTRIES = 11;
	static final String TEST_HASNOMOREENTRIES_BUFFER = "buffer";
	static final String SAMPLE_DELETEATTRIBUTE_DN = "dn";

	static final String CONNECT_TYPE = "CONNECT";
	static final String CONNECT_SSL_TYPE = "SSL_CONNECT";
	static final String DISCONNECT_TYPE = "DISCONNECT";
	static final String SEARCH_TYPE = "SEARCH";
	static final String ADDENTRY_TYPE = "ADD_ENTRY";
	static final String DELETEENTRY_TYPE = "DELETE_ENTRY";
	static final String ADDATTRIBUTE_TYPE = "ADD_ATTRIBUTE";
	static final String DELETEATTRIBUTE_TYPE = "DELETE_ENTRY";

	/**
	 * Utility method for possibly null or empty Strings.
	 * @param value the String to check for nullness or emptiness
	 * @param defaultValue the String to return in case parameter 'value' 
	 * is null or empty
	 * @return value if it is non-null and non-empty, defaultValue otherwise
	 */
	static protected String defaultIfEmpty(String value, final String defaultValue)
	{
		String result = defaultValue;
		if (value != null)
		{
			value = value.trim();
			if (! value.isEmpty())
			{
				result = value;
			}
		}
		return result;
	}

	// LDAP server host name or IP address
	private String hostname;
	// LDAP server port
	private int port;
	// DLAPS server port
	private int sslPort;
	// LDAP version (... should always be 3)
	private int ldapVersion;
	// current connection with an LDAP server, null means no connection
	private LDAPConnection ldapConnection = null;
	// LDAP entries resulting from search operations, indexed by a buffer/variable name
	private Map<String,Deque<LDAPEntry>> searchBuffers = new HashMap<String,Deque<LDAPEntry>>();

	/**
	 * Constructor for specimen object: sets LDAP server's
	 * network address and LDAP protocol version number. 
	 *
	 * @param params plug-in parameters: key-value pairs holding:
	 * <ul>
	 *   <li>host name or address,</li>
	 *   <li>port number,</li>
	 *   <li>SSL port number,</li>
	 *   <li>LDAP version number</li>
	 * </ul> 
	 */
	public SessionObject(Map<String,String> params)
	{
		hostname = params.get(PLUGIN_LDAPHOST);
		if (hostname == null || hostname.isEmpty())
		{
			throw new IsacRuntimeException(hostname + " is not a valid host name or address for ~LdapInjector~.");
		}
		try
		{
			port = Integer.parseInt(params.get(PLUGIN_LDAPPORT));
			sslPort = Integer.parseInt(params.get(PLUGIN_LDAPSSLPORT));
		}
		catch (Exception ex)
		{
			throw new IsacRuntimeException(
				params.get(PLUGIN_LDAPPORT)
				+ " is not a valid port number for ~LdapInjector~.");
		}
		try
		{
			ldapVersion = Integer.parseInt(params.get(PLUGIN_LDAPVERSION));
		}
		catch (Exception ex)
		{
			throw new IsacRuntimeException(
				params.get(PLUGIN_LDAPVERSION)
				+ " is not a valid LDAP version number for ~LdapInjector~.");
		}
	}

	/**
	 * Copy constructor (clone specimen object to get session object).
	 *
	 * @param so specimen object to clone
	 */
	private SessionObject(SessionObject so) {
		this.hostname = so.hostname;
		this.port = so.port;
		this.sslPort = so.sslPort;
		this.ldapVersion = so.ldapVersion;
	}


	////////////////////////////////////////
	// SessionObjectAction implementation //
	////////////////////////////////////////

	/**
	 * @see org.ow2.clif.scenario.isac.plugin.SessionObjectAction#createNewSessionObject()
	 */
	@Override
	public Object createNewSessionObject()
	{
		return new SessionObject(this);
	}

	/**
	 * @see org.ow2.clif.scenario.isac.plugin.SessionObjectAction#close()
	 */
	@Override
	public void close()
	{
		searchBuffers = null;
		ldapConnection = null;
	}

	/**
	 * @see org.ow2.clif.scenario.isac.plugin.SessionObjectAction#reset()
	 */
	@Override
	public void reset()
	{
		ldapConnection = null;
		searchBuffers = new HashMap<String,Deque<LDAPEntry>>();
	}

	
	/////////////////////////////////
	// DataProvider implementation //
	/////////////////////////////////

	/**
	 * @see org.ow2.clif.scenario.isac.plugin.DataProvider#doGet()
	 */
	@Override
	public String doGet(String variable)
	{
		String response = null;
		if (variable != null && !variable.isEmpty())
		{
			try
			{
				Deque<LDAPEntry> results = searchBuffers.get(variable);
				if (results != null)
				{
					// ${ldap:buffer} => return all attributes name-value pairs
					response = results.pollFirst().toString();
				}
				else
				{
					String bufferName = variable.substring(0, variable.indexOf(':'));
					String attributeName = variable.substring(variable.indexOf(':') + 1);
					results = searchBuffers.get(bufferName);
					if (results != null)
					{
						if (attributeName.equals("#"))
						{
							// ${ldap:buffer:#} => return the number of entries left to read
							response = String.valueOf(results.size());
						}
						else
						{
							// ${ldap:buffer:attribute} => return the attribute's value
							response = results.pollFirst().getAttribute(attributeName).getStringValue();
						}
					}
					else
					{
						throw new IsacRuntimeException("No such search results buffer: " + variable);
					}
				}
			}
			catch (Exception ex)
			{
				throw new IsacRuntimeException("~LdapInjector could not retrieve search results for " + variable, ex);
			}
		}
		return response;
	}

	
	/////////////////////////////////
	// SampleAction implementation //
	/////////////////////////////////

	/**
	 * @see org.ow2.clif.scenario.isac.plugin.SampleAction#doSample()
	 */
	@Override
	public ActionEvent doSample(int number, Map<String,String> params, ActionEvent report)
	{
		switch (number)
		{
			case SAMPLE_DELETEATTRIBUTE:
				return doDeleteAttributes(params,report);
			case SAMPLE_ADDATTRIBUTE:
				return doAddAttributes(params,report);
			case SAMPLE_DELETEENTRY:
				return doDeleteEntry(params,report);
			case SAMPLE_ADDENTRY:
				return doAddEntry(params,report);
			case SAMPLE_BIND:
				return doBind(params,report);
			case SAMPLE_SEARCH:
				return doSearch(params,report);
			case SAMPLE_CLOSECONNECTION:
				return doCloseConnection(params,report);				
			default:
				throw new Error("Unable to find this sample in ~LdapInjector~ ISAC plugin: " + number);
		}
	}

	/**
	 * Does a connection to a LDAP directory (bind).
	 *
	 * @param report The action report to update.
	 * @param params Connection parameters provided as key-value pairs:
	 * <ul>
	 *   <li>user DN</li>
	 *   <li>password</li>
	 *   <li>SSL switch</li>
	 *   <li>custom action type</li>
	 *   <li>custom comment</li>
	 * </ul>
	 * @return the provided action report object
	 */
	protected ActionEvent doBind(Map<String,String> params, ActionEvent report)
	{
		String loginDn = params.get(SAMPLE_BIND_LOGIN);
		String password = params.get(SAMPLE_BIND_PASSWORD);
		String enableSslStr = params.get(SAMPLE_BIND_WITHSSL);
		boolean withSSL =
			enableSslStr != null
			&& ParameterParser.getCheckBox(enableSslStr).contains("enable");
		String actionType = params.get(SAMPLE_BIND_ACTIONTYPE);
		String comment = params.get(SAMPLE_BIND_COMMENT);
		report.type = defaultIfEmpty(actionType, CONNECT_TYPE);
		report.comment = defaultIfEmpty(
			comment, 
			loginDn + "@" + hostname + ":"
			+ (withSSL ? String.valueOf(sslPort) : String.valueOf(port)));
		report.setDate(System.currentTimeMillis());
		try
		{
			if (withSSL)
			{
				LDAPSocketFactory ssf = new LDAPJSSESecureSocketFactory();
				ldapConnection = new LDAPConnection(ssf);
				ldapConnection.connect(hostname, sslPort);
			}
			else
			{
				ldapConnection = new LDAPConnection();
				ldapConnection.connect(hostname, port);
			}
			ldapConnection.bind(ldapVersion, loginDn, password.getBytes("UTF8"));
			report.successful = true;
			report.result = "bound";
			if (withSSL)
			{
				report.result = "bind with SSL OK";
			}
			else
			{
				report.result = "bind OK";
			}
		}
		catch (Exception e)
		{
			ldapConnection = null;
			report.successful = false;
			report.result = e.toString();
		}
		finally
		{
			report.duration = (int) (System.currentTimeMillis() - report.getDate());
		}
		return report;
	}


	/**
	 * Closes the connection to an LDAP directory (bind).
	 * @param params this sample takes no parameter.
	 * @param report the action report to update.
	 *
	 * @return the action report.
	 */
	protected ActionEvent doCloseConnection(Map<String,String> params, ActionEvent report)
	{
		if (ldapConnection == null)
		{
			throw new IsacRuntimeException("~LdapInjector~ disconnect attempt while not connected.");
		}
		report.type = DISCONNECT_TYPE;
		report.setDate(System.currentTimeMillis());
		try
		{
            ldapConnection.disconnect();
            report.duration = (int)(System.currentTimeMillis() - report.getDate());
			ldapConnection = null;
			report.successful = true;
			report.comment = "disconnected from " + hostname;
			report.result = "disconnected";
			return report;
		}
		catch (LDAPException e)
		{
			report.successful = false;
			report.comment = "disconnection from " + hostname+ " failed";
			report.result = e.toString();
			return report;
		}
	}

	/**
	 * Does search into a LDAP directory.
	 * @param params Parameters for search operation, provided as key-value pairs:
	 * <ul>
	 *   <li>search base</li>
	 *   <li>search filter</i>
	 *   <li>search scope</li>
	 *   <li>search limit (maximum number of entries to retrieve)</li>
	 *   <li>buffer name for keeping search results</li>
	 * </ul>
	 * @param report the action report to update
	 *
	 * @return the action report
	 */
	protected ActionEvent doSearch(Map<String,String> params, ActionEvent report)
	{
		if (ldapConnection == null)
		{
			throw new IsacRuntimeException("~LdapInjector~ search attempt while not connected.");
		}
		String base = params.get(SAMPLE_SEARCH_SEARCHBASE);
		String filter = params.get(SAMPLE_SEARCH_SEARCHFILTER);
		String scopeStr = ParameterParser.getCombo(params.get(SAMPLE_SEARCH_SEARCHSCOPE));
		String bufferName = params.get(SAMPLE_SEARCH_BUFFER);
		if (bufferName != null && bufferName.trim().isEmpty())
		{
			bufferName = null;
		}
		String limitStr = params.get(SAMPLE_SEARCH_SEARCHLIMIT);
		int limit = 0;
		try
		{
			limit = Integer.parseInt(limitStr);
			if (limit < 0)
			{
				throw new IsacRuntimeException("Search limit can't be less than zero.");
			}
		}
		catch (Exception ex)
		{
			throw new IsacRuntimeException("Invalid search limit in ~LdapInjector~: " + limitStr, ex);
		}
		int scope;
		if (scopeStr.equals("base"))
		{
			scope = LDAPConnection.SCOPE_BASE;
		}
		else if (scopeStr.equals("one"))
		{
			scope = LDAPConnection.SCOPE_ONE;
		}
		else if (scopeStr.equals("sub"))
		{
			scope = LDAPConnection.SCOPE_SUB;
		}
		else
		{
			throw new IsacRuntimeException("Unrecognized LDAP search scope: " + scopeStr);
		}
		LDAPSearchConstraints constraints = new LDAPSearchConstraints();
		constraints.setMaxResults(limit);
		report.type = defaultIfEmpty(params.get(SAMPLE_SEARCH_ACTIONTYPE), SEARCH_TYPE);
		report.comment = defaultIfEmpty(params.get(SAMPLE_SEARCH_COMMENT), filter);
		try
		{
			report.setDate(System.currentTimeMillis());
			LDAPSearchQueue searchQ = ldapConnection.search(
				base, scope, filter, null, false, null, constraints);
			LDAPMessage message;
			Deque<LDAPEntry> searchEntries = null;
			long numberOfEntries = 0;
			if (bufferName != null)
			{
				if (limit > 0)
				{
					searchEntries = new ArrayDeque<LDAPEntry>(limit);
				}
				else
				{
					searchEntries = new ArrayDeque<LDAPEntry>();
				}
			}
			while ((message = searchQ.getResponse()) != null)
			{
				if (message instanceof LDAPSearchResult)
				{
					LDAPEntry entry = ((LDAPSearchResult)message).getEntry();
					++numberOfEntries;
					if (bufferName != null)
					{
						searchEntries.addLast(entry);
					}
				}
			}
    	    report.successful = true;
    	    if (bufferName != null)
    	    {
    	    	searchBuffers.put(bufferName, searchEntries);
    	    }
    	    report.result = "got " + numberOfEntries + " entries"; 
	    }
		catch(LDAPException e)
		{
         	report.successful = false;
        	report.result = e.toString();
	    }
		finally
		{
			report.duration = (int)(System.currentTimeMillis() - report.getDate());
		}
		return report;
	}

	/**
	 * Add an entry into a LDAP directory. The connection to the LDAP directory must be established before doing the doAddEntry.
	 * @param params : A Map containing all the useful variable.
	 * @param report : The ActionReport to update.
	 *
	 * @return the report.
	 */	
	public ActionEvent doAddEntry(Map<String,String> params, ActionEvent report) {
		String dn = params.get(SAMPLE_ADDENTRY_LDAPDN);
		String insertNodeNameValue =
			ParameterParser.getCombo(params.get(SAMPLE_ADDENTRY_ENTRYNODETYPE))
			+ "="
			+ params.get(SAMPLE_ADDENTRY_ENTRYNODENAME);
		List<String> attributesList = ParameterParser.getNField(
			params.get(SAMPLE_ADDENTRY_ATTRIBUTESLIST));
		
		LDAPAttributeSet attributeSet = new LDAPAttributeSet();
		
		// Test to know if the entry node belongs to the attributesList. If no, then we put it in this List.
		if (!attributesList.contains(insertNodeNameValue))
			attributesList.add(insertNodeNameValue);

		// Test to know if the entry node belongs to the dn. If no, then we add it at the beginning of the dn.
		if (dn.indexOf(insertNodeNameValue) == -1)
			dn = insertNodeNameValue+","+dn;
		
		String name = null;
		String[] values = null;
		
		String[] tabParam_NameValue = attributesList.toArray(new String[attributesList.size()]);
		for (int i = 0; i<tabParam_NameValue.length; i++) {
			name = tabParam_NameValue[i].substring(0, tabParam_NameValue[i].indexOf("="));
			values = (tabParam_NameValue[i].substring(tabParam_NameValue[i].indexOf("=")+1, tabParam_NameValue[i].length())).split(",");

			attributeSet.add( new LDAPAttribute( name, values ));
		}
		LDAPEntry newEntry = new LDAPEntry( dn, attributeSet );

		report.type = ADDENTRY_TYPE;
		report.setDate(System.currentTimeMillis());
		
        try {
			ldapConnection.add( newEntry );
			
			report.duration = (int)(System.currentTimeMillis() - report.getDate()); 
	        report.successful = true;
	        report.comment = "Added object: " + dn + " successfully.";
			// System.out.println( "Added object: " + dn + " successfully." );				
			
		} catch (LDAPException e) {
			report.successful = false;
			report.result = e.toString();
			report.comment = "ISAC Ldap Injector can't add object "+dn+" to the ldap directory.";
            // System.out.println("ISAC Ldap Injector can't add object "+dn+" to the ldap directory. "+e.toString());
		} catch (Exception e) {
			report.successful = false;
			report.result = e.toString();
			report.comment = "ISAC Ldap Injector can't add object "+dn+" to the ldap directory.";
            // System.out.println("ISAC Ldap Injector can't add object "+dn+" to the ldap directory. "+e.toString());			
		}

		return report;
	}

	/**
	 * Delete an entry from a LDAP directory. The connection to the LDAP directory must be established before doing the doDeleteEntry.
	 * @param params : A Map containing all the useful variable.
	 * @param report : The ActionReport to update.
	 *
	 * @return the report.
	 */	
	public ActionEvent doDeleteEntry(Map<String,String> params,ActionEvent report) {
		String dn = params.get(SAMPLE_DELETEENTRY_LDAPDN);
		
		report.type = DELETEENTRY_TYPE;
		report.setDate(System.currentTimeMillis());
		
		try {
			
			ldapConnection.delete(dn);

			report.duration = (int)(System.currentTimeMillis() - report.getDate()); 
			report.successful = true;
			report.comment = "Entry: " + dn + " was deleted." ;
			// System.out.println("Entry: " + dn + " was deleted.");
			
		} catch (LDAPException e) {
			report.successful = false;
			report.comment = "ISAC Ldap Injector can't delete dn : "+dn;
            if ( e.getResultCode() == LDAPException.NO_SUCH_OBJECT ) {
            	report.result = "Error: No such object";
                // System.out.println( "Error: No such object" );
			} else if ( e.getResultCode() == LDAPException.INSUFFICIENT_ACCESS_RIGHTS ) {
				report.result = "Error: Insufficient rights";
                // System.out.println( "Error: Insufficient rights" );
			} else {
				report.comment = "Error: " + e.toString();
                // System.out.println( "Error: " + e.toString() );
			}    
		}
		
		return report;
	}

	/**
	 * Add attributes into an entry of LDAP directory. The connection to the LDAP directory must be established before doing the doAddAttributes.
	 * @param params : A Map containing all the useful variable.
	 * @param report : The ActionReport to update.
	 *
	 * @return the report.
	 */	
	public ActionEvent doAddAttributes(Map<String,String> params, ActionEvent report) {
        String dn = params.get(SAMPLE_ADDATTRIBUTE_DN);
        String[] attributesNameValue = (String[])
        	ParameterParser.getNField(params.get(SAMPLE_ADDATTRIBUTE_ATTRIBUTESNAMEVALUES)).toArray();

        ArrayList<LDAPModification> modList = new ArrayList<LDAPModification>();
		LDAPAttribute attribute = null;
  
		String name = null;
		String[] values = null;      		
		
		for (int i = 0; i<attributesNameValue.length; i++) {
 			name = attributesNameValue[i].substring(0, attributesNameValue[i].indexOf("="));
 			values =
 				(attributesNameValue[i].substring(
 					attributesNameValue[i].indexOf("=")+1, attributesNameValue[i].length())
 				).split(",");		
 			attribute = new LDAPAttribute(name, values);
	        modList.add( new LDAPModification(LDAPModification.ADD,attribute));
		}   		

        LDAPModification[] mods = new LDAPModification[modList.size()]; 
        mods = (LDAPModification[])modList.toArray(mods);	

		report.type = ADDATTRIBUTE_TYPE;
		report.setDate(System.currentTimeMillis());        
        
        try {
            ldapConnection.modify(dn, mods);

			report.duration = (int)(System.currentTimeMillis() - report.getDate());             
            report.successful = true;
            report.comment = "Successfully add the attribute.";
            // System.out.println("Successfully add the attribute.");        
        
        } catch( LDAPException ex ) {
        	
        	report.successful = false;
        	
            if(ex.getResultCode()== LDAPException.ATTRIBUTE_OR_VALUE_EXISTS){ 
            	report.result = ex.toString();
            	report.comment = "Failed to add existing attribute.";
                // System.out.println("Failed to add existing attribute : "+ex.toString());
            } else {
            	report.result = ex.toString();
            	report.comment = "Failed to add attribute.";            	
            	// System.out.println("Failed to add attribute : "+ex.toString());
            }
        } catch ( Exception e) {
        	report.result = e.toString();
        	report.comment = "Error : Failed to add attribute.";            	
        	// System.out.println("Error : Failed to add attribute : "+e.toString());        	
        }
        
        return report;
	}

	/**
	 * Delete an attributes of an entry of LDAP directory.
	 * The connection to the LDAP directory must be established before doing the doDeleteAttributes.
	 * @param params : A Map containing all the useful variable.
	 * @param report : The ActionReport to update.
	 *
	 * @return the report.
	 */	
	public ActionEvent doDeleteAttributes(Map<String,String> params, ActionEvent report) {

        String dn = params.get(SAMPLE_DELETEATTRIBUTE_DN);
        String[] attributesToDelete = (String[])ParameterParser.getNField(
        	params.get(SAMPLE_DELETEATTRIBUTE_ATTRIBUTESTODELETE)).toArray();

        ArrayList<LDAPModification> modList = new ArrayList<LDAPModification>();
 
		LDAPAttribute attribute = null;		

		for (int i = 0; i<attributesToDelete.length; i++) {
			attribute = new LDAPAttribute(attributesToDelete[i]);
	        modList.add( new LDAPModification(LDAPModification.DELETE,attribute));
		}    
		
        // Create modifications array
        LDAPModification[] modsdel = new LDAPModification[modList.size()]; 
        modsdel = (LDAPModification[])modList.toArray(modsdel);	

		report.type = DELETEATTRIBUTE_TYPE;
		report.setDate(System.currentTimeMillis());          
        
        try {
			ldapConnection.modify(dn, modsdel);

			report.duration = (int)(System.currentTimeMillis() - report.getDate()); 			
	        report.successful = true;
	        report.comment = "Successfully delete the attribute.";	
	        // System.out.println("Successfully delete the attribute.");			
		} catch( LDAPException ex ) {
        	report.successful = false;
        	report.result = ex.toString();
        	report.comment = "Failed to delete attribute";            	
        	// System.out.println("Failed to delete attribute : "+ex.toString());
		}    

		return report;
	}
	
	///////////////////////////////
	// TestAction implementation //
	///////////////////////////////

	/** 
	 * @see org.ow2.clif.scenario.isac.plugin.TestAction#doTest()
	 */
	@Override
	public boolean doTest(int number, Map<String,String> params)
	{
		switch (number)
		{
			case TEST_HASNOMOREENTRIES:
				return ! hasMoreEntries(params.get(TEST_HASNOMOREENTRIES_BUFFER));
			case TEST_HASMOREENTRIES:
				return hasMoreEntries(params.get(TEST_HASMOREENTRIES_BUFFER));
			case TEST_ISCONNECTED:
				return ldapConnection != null;
			case TEST_ISNOTCONNECTED:
				return ldapConnection == null;
		}
		throw new IsacRuntimeException("Unimplemented condition in ~LdapInjector~ ISAC plugin: " + number);
	}

	/**
	 * Checks if a search results buffer contains at least one entry 
	 * @param bufferName the name of the search results buffer
	 * @return true if the buffer holds at least one entry, false if it is empty
	 * @throws IsacRuntimeException there is no search results buffer
	 * with the provided name, or the provided name is null or empty.
	 */
	protected boolean hasMoreEntries(String bufferName)
	throws IsacRuntimeException
	{
		if (bufferName != null & !bufferName.isEmpty())
		{
			Deque<LDAPEntry> searchResults = searchBuffers.get(bufferName);
			if (searchResults != null)
			{
				return searchResults.size() > 0;
			}
			else
			{
				throw new IsacRuntimeException("No search results buffer named " + bufferName + " in ~LdapInjector~.");
			}
		}
		else
		{
			throw new IsacRuntimeException("Missing search results buffer name in ~LdapInjector~ condition.");
		}
	}
}
