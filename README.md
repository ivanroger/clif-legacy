Welcome to CLIF, the highly adaptable and high power performance testing software
=================================================================================

![clif_100](dist/icons/common/logo_clif_100px.gif) ***CLIF is a Load Injection Framework***

Load injectors to measure server response times over a variety of protocols, probes to measure computing and networking resources usage, embedded and automated performance analysis and reporting tools...

Care about performance with CLIF!
* scenarios with 1 virtual user for integration and functional testing, or Quality of Experience monitoring;
* several virtual users for performance testing (possibly in continuous integration), endurance testing...
* up to hundreds of load injectors and millions of virtual users for load testing.

Documentation
-------------
* [CLIF main documentation site](http://clif.ow2.io/)
* [latest CLIF distributions](http://clif.ow2.io/clif-legacy/download/)
* [latest ISAC plug-ins reference manual](http://clif.ow2.io/clif-legacy/idoc/) for writing scenarios
* [CLIF Performance testing Plug-in for Jenkins](https://plugins.jenkins.io/clif-performance-testing)
* [latest CLIF API documentation](http://clif.ow2.io/clif-legacy/jdoc/) for Java developers
